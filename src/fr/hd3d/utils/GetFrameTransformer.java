package fr.hd3d.utils;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IShot;


/**
 * If object is a shot, the number of frames of this shot is returned. Else it
 * 
 * @author HD3D
 * 
 * @param <T>
 *            Type of transformed object.
 */
public class GetFrameTransformer<T extends IBase> implements Transformer<T, Long>
{
    public Long transform(T object)
    {
        if (object instanceof IShot)
        {
            return new Long(((IShot) object).getNbFrame());
        }

        return null;
    }
}
