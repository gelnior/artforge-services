package fr.hd3d.utils;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.impl.CriteriaImpl;
import org.hibernate.impl.SessionImpl;
import org.hibernate.impl.CriteriaImpl.Subcriteria;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.impl.hibernate.interceptor.Hd3dInterceptor;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.services.security.utils.AuthenticationUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public final class HibernateUtil
{
    static Bench b;// DEBUG
    final public static Configuration CFG;
    private static SessionFactory sessionFactory;
    public static final int JDBC_BATCHSIZE;

    static
    {
        try
        {
            CFG = new Configuration().configure();

            // org.hibernate.event.SaveOrUpdateEventListener[] saveOrUpdateStack = { new
            // fr.hd3d.model.persistence.impl.hibernate.eventListener.SaveOrUpdateEventListener() };
            // org.hibernate.event.MergeEventListener[] mergeStack = { new
            // fr.hd3d.model.persistence.impl.hibernate.eventListener.MergeEventListener() };

            // new fr.hd3d.model.persistence.impl.hibernate.eventListener.UpdateEventListener(),
            // new fr.hd3d.model.persistence.impl.hibernate.eventListener.SaveEventListener() };
            // PersistEventListener[] persiststack = { new
            // fr.hd3d.model.persistence.impl.hibernate.eventListener.PersistEventListener() };
            // FlushEntityEventListener[] flushStack = { new
            // fr.hd3d.model.persistence.impl.hibernate.eventListener.EntityCheckDirtyListener() };
            //
            // CFG.getEventListeners().setSaveOrUpdateEventListeners(saveOrUpdateStack);
            // CFG.getEventListeners().setMergeEventListeners(mergeStack);
            // CFG.getEventListeners().setPersistEventListeners(persiststack);
            // CFG.getEventListeners().setFlushEntityEventListeners(flushStack);

            sessionFactory = CFG.buildSessionFactory();
            final SchemaUpdate schemaUpdate = new SchemaUpdate(CFG);
            schemaUpdate.execute(false, true);

            /* Read Hibernate Settings */
            SessionFactoryImplementor sessionFactoryImplementor = (SessionFactoryImplementor) sessionFactory;
            JDBC_BATCHSIZE = sessionFactoryImplementor.getSettings().getJdbcBatchSize();
        }
        catch (Exception e)
        {
            throw new ExceptionInInitializerError(e);
        }
    }

    private HibernateUtil()
    {}

    public static void clear()
    {
        b = null;
    }

    /**
     * the hibernate session bound to the current thread. Note: a restlet thread seems to be able to handle several
     * request, thus the hibernate session may be reused.
     */
    private static final ThreadLocal<org.hibernate.Session> SESSION = new ThreadLocal<org.hibernate.Session>();

    /**
     * returns the current hibernate session and creates a new one if needed.
     * 
     * @return
     * @throws HibernateException
     */
    public static synchronized Session currentSession() throws HibernateException
    {
        Session s = (Session) SESSION.get();
        if (s == null || !s.isOpen())
        {
            SessionFactory sf = getSessionFactory();
            s = sf.openSession(new Hd3dInterceptor());
            SESSION.set(s);
        }
        return s;
    }

    /**
     * flush the hibernate session but does not close it
     * 
     * @throws HibernateException
     */
    public static synchronized void flushSession() throws HibernateException
    {
        Session s = (Session) SESSION.get();
        if (s != null && s.isOpen())
        {
            s.flush();
        }
    }

    /**
     * flush the hibernate session and close it
     * 
     * @throws HibernateException
     */
    public static synchronized void closeSession()
    {
        AuthenticationUtil.currentUser.set(null);
        AuthenticationUtil.currentUser.remove();
        AuthenticationUtil.currentUserRoles.set(null);
        AuthenticationUtil.currentUserRoles.remove();

        /* close session */
        Session s = (Session) SESSION.get();

        if (s == null || !s.isOpen())
            return;

        // try
        // {
        // if (s.getFlushMode() == FlushMode.MANUAL)
        // {
        // s.flush();
        // }
        // }
        // catch (HibernateException e)
        // {
        // /* do it even exceptions occur */
        // }
        // finally
        // {
        s.clear();
        /*
         * "session.clear() is not effective when use_query_cache is true".
         * Cf.http://opensource.atlassian.com/projects/hibernate/browse/HHH-3195
         */
        ((SessionImpl) s).getActionQueue().executeActions();// avoid memory leaks
        s.close();
        s = null;
        // }

        SESSION.set(null);// avoid memory leaks
        SESSION.remove();
    }

    public static synchronized SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }

    /**
     * shut down the session factory
     */
    public static void shutdown()
    {
        closeSession();
        if (sessionFactory != null)
        {
            sessionFactory.close();
        }
        sessionFactory = null;
        clear();
    }

    public static void safeQuery(IHibernateQuery cmd) throws Hd3dException
    {
        final Session session = currentSession();
        session.setFlushMode(FlushMode.MANUAL);// if read-only, better perf

        cmd.execute(session);
    }

    public static void safeTransaction(IHibernateTransaction cmd) throws Hd3dPersistenceException, HibernateException
    {
        final Session session = currentSession();
        session.setFlushMode(FlushMode.AUTO);
        try
        {
            cmd.execute(session);
            // session.flush();
        }
        catch (StaleObjectStateException e)
        {
            Log.LOGGER
                    .error("AN ERROR OCCURED during TRANSACTION: the entity being updated must be updated by another transaction.");
            Log.LOGGER.error(ExceptionUtils.getMessage(e));
            throw e;
        }
        // catch (HibernateException e)
        // {
        // Log.LOGGER.error("AN ERROR OCCURED during TRANSACTION:");
        // Log.LOGGER.error(ExceptionUtils.getMessage(e));
        // }
        catch (Exception e)
        {
            // Note: do not try to implement retry here, once an exception occurs Hibernate
            // session has to be closed (inconsistent state) => useless to retry
            Log.LOGGER.error("AN ERROR OCCURED during TRANSACTION:");
            Log.LOGGER.error(ExceptionUtils.getMessage(e));
            session.getTransaction().rollback();
            throw new Hd3dPersistenceException(e);
        }
    }

    public static void indexCollection(final Iterable<?> objects) throws Hd3dException
    {
        CollectUtils.removeNull((Collection<?>) objects);

        FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
        int counter = 0;
        for (Object object : objects)
        {
            fullTextSession.index(object);
            counter++;
            if (counter % HibernateUtil.JDBC_BATCHSIZE == 0)
            {
                fullTextSession.flushToIndexes();
            }
        }
    }

    public static boolean aliasExist(Criteria criteria, String alias)
    {
        Iterator<Subcriteria> iter = ((CriteriaImpl) criteria).iterateSubcriteria();
        while (iter.hasNext())
        {
            Subcriteria sub = iter.next();
            if (alias.equals(sub.getAlias()))
            {
                return true;
            }
        }
        return false;
    }

    public static void cleanupThreadLocals(Thread thread, final String classFilter, final ClassLoader classLoader)
    {
        if (classLoader != null)
            Log.LOGGER.debug("@@ cleanupThreadLocals for classLoader={} @{}", classLoader.getClass().getName(), Integer
                    .toHexString(classLoader.hashCode()));

        Thread[] threadList;
        if (thread != null)
        {
            threadList = new Thread[1];
            threadList[0] = thread;
        }
        else
        {
            // Every thread
            threadList = new Thread[Thread.activeCount()];
            Thread.enumerate(threadList);
        }

        for (int iThreadList = 0; iThreadList < threadList.length; iThreadList++)
        {
            Thread t = threadList[iThreadList];

            Field field;
            try
            {
                Class<?> c;
                c = t.getClass();
                while ((c != null) && (c != java.lang.Thread.class))
                    c = c.getSuperclass();
                if (c != null)
                {
                    field = c.getDeclaredField("threadLocals");
                    field.setAccessible(true);

                    Object threadLocals = field.get(t);
                    if (threadLocals != null)
                    {
                        Field entries = threadLocals.getClass().getDeclaredField("table");
                        entries.setAccessible(true);
                        Object entryList[] = (Object[]) entries.get(threadLocals);
                        for (int iEntry = 0; iEntry < entryList.length; iEntry++)
                        {
                            if (entryList[iEntry] != null)
                            {
                                Field fValue = entryList[iEntry].getClass().getDeclaredField("value");
                                if (fValue != null)
                                {
                                    fValue.setAccessible(true);
                                    Object value = fValue.get(entryList[iEntry]);
                                    if (value != null)
                                    {
                                        boolean flag = true;
                                        Log.LOGGER.debug("found entry: value={} @{}", value.getClass().getName(),
                                                Integer.toHexString(value.hashCode()));
                                        if ((classFilter != null)
                                                && (value.getClass().getName().indexOf(classFilter) == -1))
                                            flag = false;
                                        if ((classLoader != null) && (value.getClass().getClassLoader() != classLoader))
                                            flag = false;

                                        if (flag)
                                        {
                                            Log.LOGGER.debug("@@ Entry for {} @{} cleared.",
                                                    value.getClass().getName(), Integer.toHexString(value.hashCode()));
                                            entryList[iEntry] = null;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                        Log.LOGGER.debug("@@ no threadLocals for '{}'", t.getName());
                }
            }
            catch (IllegalAccessException ex)
            {
                // Globals.reportException(ex);
                Log.LOGGER.error(ExceptUtils.format(ex));
            }
            catch (SecurityException ex)
            {
                // Globals.reportException(ex);
                Log.LOGGER.error(ExceptUtils.format(ex));
            }
            catch (NoSuchFieldException ex)
            {
                // Globals.reportException(ex);
                Log.LOGGER.error(ExceptUtils.format(ex));
                Field fields[] = t.getClass().getDeclaredFields();
                Log.LOGGER.debug("Fields available for {}: ", t.getClass().getName());
                for (int ii = 0; ii < fields.length; ii++)
                    Log.LOGGER.debug(fields[ii].getName());
                Log.LOGGER.debug(SystemUtils.LINE_SEPARATOR);
            }
        }
    }
}
