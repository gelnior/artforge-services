package fr.hd3d.utils;

import org.apache.commons.lang.SystemUtils;

import fr.hd3d.model.persistence.IFileRevision;


public final class FilenameMaker
{
    final static long MAX_NB_FILES_IN_DIR = 10000;
    final static String TMP_DIR = Conf.getTmpDir();
    final static String UPLOAD_DIR = Conf.getUploadDir();

    /**
     * return <temporary directory>/<system time in ms>. Ex: TMP/12545484455
     * 
     * @return
     */
    public static String getTemporaryFileFullPath()
    {
        String uniquId = Long.toString(System.currentTimeMillis());
        return TMP_DIR + SystemUtils.FILE_SEPARATOR + uniquId;
    }

    public static String buildFileLocation(IFileRevision file)
    {
        if (file == null)
            return null;

        Long id = file.getId();
        Long min = (id / MAX_NB_FILES_IN_DIR) * MAX_NB_FILES_IN_DIR;
        Long max = ((id / MAX_NB_FILES_IN_DIR) + 1) * MAX_NB_FILES_IN_DIR;
        String intDir = min + "-" + max;
        return UPLOAD_DIR + SystemUtils.FILE_SEPARATOR + intDir;
    }

    public static String buildFileRelativePath(IFileRevision file)
    {
        if (file == null)
            return null;

        return file.getId() + "_" + file.getKey();
    }

    public static String getFullPath(IFileRevision file)
    {
        if (file == null)
            return null;

        return file.getLocation() + SystemUtils.FILE_SEPARATOR + file.getId() + "_" + file.getKey();
    }

    public static String getImagePreviewFromPath(String path)
    {
        return addSuffix(path, "_thumb", "jpg");
    }

    public static String getImagePreviewFromPath(IFileRevision pf)
    {
        return getImagePreviewFromPath(getFullPath(pf));
    }

    public static String getMoviePreviewFromPath(String path)
    {
        return addSuffix(path, "_preview", "flv");
    }

    public static String getMoviePreviewFromPath(IFileRevision pf)
    {
        return getMoviePreviewFromPath(getFullPath(pf));
    }

    private static String addSuffix(String path, String suffix, String newExt)
    {
        String ext = "";

        // TODO get last index of
        int extPos = path.indexOf(".");
        if (extPos != -1)
        {
            ext = path.substring(extPos);
            path = path.replaceFirst(ext, suffix + "." + newExt);
        }
        else
        {
            path = path + suffix + "." + newExt;
        }
        return path;
    }

}
