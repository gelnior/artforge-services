package fr.hd3d.utils.closure;

import static org.apache.commons.collections15.CollectionUtils.collect;
import static org.apache.commons.collections15.CollectionUtils.select;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.Closure;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ChainedTransformer;

import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.predicate.MatchParentIdPredicate;
import fr.hd3d.utils.transformer.GetTagNameTransformer;
import fr.hd3d.utils.transformer.GetTagTransformer;


public class LoadTagNamesClosure implements Closure<ILBase>
{
    List<TagParent> tagParents;

    public LoadTagNamesClosure(List<TagParent> tagParents)
    {
        this.tagParents = tagParents;
    }

    public void execute(ILBase light)
    {
        if (light != null && CollectUtils.isNotEmpty(tagParents))
        {
            /* collect the TagParents matching the current entity and bind them. */
            Collection<TagParent> filtered = select(tagParents, new MatchParentIdPredicate(light.getId()));

            if (CollectUtils.isNotEmpty(filtered))
            {
                List<String> tagNames = new ArrayList<String>(collect(filtered,
                        new ChainedTransformer<TagParent, String>(new Transformer[] { new GetTagTransformer(),
                                new GetTagNameTransformer() })));
                tagParents.removeAll(filtered);

                if (CollectUtils.isNotEmpty(tagNames))
                {
                    light.setTagNames(tagNames);
                }
            }
        }
    }
}
