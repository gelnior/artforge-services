package fr.hd3d.utils.closure;

import static org.apache.commons.collections15.CollectionUtils.collect;
import static org.apache.commons.collections15.CollectionUtils.select;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.Closure;

import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.predicate.MatchParentIdPredicate;
import fr.hd3d.utils.transformer.GetTagTransformer;


public class LoadTagsClosure implements Closure<ILBase>
{
    List<TagParent> tagParents;

    public LoadTagsClosure(List<TagParent> tagParents)
    {
        this.tagParents = tagParents;
    }

    public void execute(ILBase light)
    {
        /* collect the TagParents matching the current entity and bind them. */
        if (light != null && CollectUtils.isNotEmpty(tagParents))
        {
            Collection<TagParent> filtered = select(tagParents, new MatchParentIdPredicate(light.getId()));

            if (CollectUtils.isNotEmpty(filtered))
            {
                List<Long> tags = CollectUtils.getIds(collect(filtered, new GetTagTransformer()));

                if (CollectUtils.isNotEmpty(tags))
                {
                    light.setTags(tags);
                }
            }
        }
    }
}
