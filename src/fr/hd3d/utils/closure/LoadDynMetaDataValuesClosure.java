package fr.hd3d.utils.closure;

import static org.apache.commons.collections15.CollectionUtils.select;

import java.util.List;

import org.apache.commons.collections15.Closure;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.predicate.MatchDynMetaDataValueIdPredicate;


public class LoadDynMetaDataValuesClosure implements Closure<ILBase>
{
    List<IDynMetaDataValue> dyns;
    ResourceContext<?, ?> context;

    public LoadDynMetaDataValuesClosure(List<IDynMetaDataValue> dyns, ResourceContext<?, ?> context)
    {
        this.dyns = dyns;
        this.context = context;
    }

    public void execute(ILBase light)
    {
        if (light != null && CollectUtils.isNotEmpty(dyns))
        {
            /* collect the DynMetaDataValues matching the current entity and bind them. */
            List<IDynMetaDataValue> values = (List<IDynMetaDataValue>) select(dyns,
                    new MatchDynMetaDataValueIdPredicate(light.getId()));
            try
            {
                if (CollectUtils.isNotEmpty(values))
                {
                    light.setDynMetaDataValues(Translators.dynmetadatavalue.toLightweightCollection(values, context));
                    dyns.removeAll(values);
                }
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
        }
    }
}
