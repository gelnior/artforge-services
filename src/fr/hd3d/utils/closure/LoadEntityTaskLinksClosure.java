package fr.hd3d.utils.closure;

import static org.apache.commons.collections15.CollectionUtils.select;

import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections15.Closure;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.lightweight.ILEntityTaskLink;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.predicate.MatchEntityTaskLinkIdPredicate;


public class LoadEntityTaskLinksClosure implements Closure<ILBase>
{
    List<IEntityTaskLink> entityTaskLinks;
    ResourceContext<?, ?> context;

    public LoadEntityTaskLinksClosure(List<IEntityTaskLink> approvalNotes, ResourceContext<?, ?> context)
    {
        this.entityTaskLinks = approvalNotes;
        this.context = context;
    }

    public void execute(ILBase light)
    {
        if (light != null && CollectUtils.isNotEmpty(entityTaskLinks))
        {
            /* collect the DynMetaDataValues matching the current entity and bind them. */
            final List<IEntityTaskLink> values = (List<IEntityTaskLink>) select(entityTaskLinks,
                    new MatchEntityTaskLinkIdPredicate(light.getId()));
            try
            {
                if (CollectUtils.isNotEmpty(values))
                {
                    light.setEntityTaskLinks(new HashSet<ILEntityTaskLink>(Translators.entitytasklink
                            .toLightweightCollection(values, context)));
                }
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
        }
    }
}
