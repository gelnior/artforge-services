package fr.hd3d.utils.closure;

import static org.apache.commons.collections15.CollectionUtils.select;

import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections15.Closure;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.predicate.MatchApprovalNoteIdPredicate;


public class LoadApprovalNotesClosure implements Closure<ILBase>
{
    List<IApprovalNote> approvalNotes;
    ResourceContext<?, ?> context;

    public LoadApprovalNotesClosure(List<IApprovalNote> approvalNotes, ResourceContext<?, ?> context)
    {
        this.approvalNotes = approvalNotes;
        this.context = context;
    }

    public void execute(ILBase light)
    {
        if (light != null && CollectUtils.isNotEmpty(approvalNotes))
        {
            /* collect the DynMetaDataValues matching the current entity and bind them. */
            List<IApprovalNote> values = (List<IApprovalNote>) select(approvalNotes, new MatchApprovalNoteIdPredicate(
                    light.getId()));
            try
            {
                if (CollectUtils.isNotEmpty(values))
                {
                    light.setApprovalNotes(new HashSet<ILApprovalNote>(Translators.approvalnote
                            .toLightweightCollection(values, context)));
                    approvalNotes.removeAll(values);
                }
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
        }
    }
}
