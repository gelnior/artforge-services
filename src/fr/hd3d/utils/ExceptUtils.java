package fr.hd3d.utils;

import org.apache.commons.lang.exception.ExceptionUtils;


public final class ExceptUtils
{
    private ExceptUtils()
    {}

    public static String format(Throwable e)
    {
        // if (e == null)
        // return null;

        return ExceptionUtils.getStackTrace(e);

        // Throwable cause = ExceptionUtils.getCause(e);
        // Throwable rootCause = ExceptionUtils.getRootCause(e);
        //
        // StringBuilder error = null;
        // if (cause != null)
        // {
        // String[] causeStackFrame = ExceptionUtils.getStackFrames(cause);
        // String causeStr = StringUtils.join(causeStackFrame, LINE_SEP, 0, 50);
        // error = new StringBuilder(causeStr.length() * 2);// optim
        // error.append(causeStr).append(LINE_SEP).append("...").append(LINE_SEP);
        // }
        //
        // if (rootCause != null)
        // {
        // String[] rootStackFrame = ExceptionUtils.getStackFrames(rootCause);
        // String rootStr = StringUtils.join(rootStackFrame, LINE_SEP, 0, 50);
        // if (error == null)
        // error = new StringBuilder(rootStr.length() + 50);// optim
        // error.append("Root Cause: ").append(LINE_SEP);
        // error.append(rootStr).append(LINE_SEP).append("...");
        // }
        // else
        // {
        // e.setStackTrace(new StackTraceElement[0]);
        // // e.printStackTrace();
        // error = new StringBuilder();
        // String message = e.getMessage();
        // if (message == null)
        // message = "/!\\ no error message /!\\";
        //
        // error.append(message);
        // }
        //
        // return error.toString();
    }
}
