package fr.hd3d.utils;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.functors.NotNullPredicate;
import org.apache.commons.collections15.Closure;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.IteratorUtils;
import org.apache.commons.collections15.MultiMap;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.StringValueTransformer;
import org.apache.commons.collections15.iterators.EmptyIterator;
import org.apache.commons.lang.ArrayUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.impl.ILPersistent;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.serialization.EntityDescriptor;
import fr.hd3d.model.translator.utils.TransitionDifferences;
import fr.hd3d.utils.predicate.MatchClassPredicate;
import fr.hd3d.utils.transformer.DateValueTransformer;
import fr.hd3d.utils.transformer.GetIdTransformer;
import fr.hd3d.utils.transformer.GetLightIdTransformer;
import fr.hd3d.utils.transformer.IntValueTransformer;
import fr.hd3d.utils.transformer.LongValueTransformer;
import fr.hd3d.utils.transformer.ObjectToEntityDescriptorTransformer;
import fr.hd3d.utils.transformer.ObjectToEntityTransformer;


/**
 * Named this way to avoid collision with CollectionUtils
 * 
 * @author Try LAM
 */
public final class CollectUtils
{
    private CollectUtils()
    {}

    public static boolean isEmpty(final Iterable<?> col)
    {
        return col == null || !col.iterator().hasNext();
    }

    public static boolean isEmpty(final Map<?, ?> map)
    {
        return map == null || map.isEmpty();
    }

    public static boolean isEmpty(final MultiMap<?, ?> map)
    {
        return map == null || map.isEmpty();
    }

    public static boolean isNotEmpty(final Iterable<?> col)
    {
        return !isEmpty(col);
    }

    public static boolean isNotEmpty(final Map<?, ?> map)
    {
        return !isEmpty(map);
    }

    public static boolean isNotEmpty(final MultiMap<?, ?> map)
    {
        return !isEmpty(map);
    }

    public static <T> Iterable<T> nullSafe(final Iterable<T> col)
    {
        if (col == null)
        {
            return new Iterable<T>() {
                public Iterator<T> iterator()
                {
                    return EmptyIterator.INSTANCE;
                }

                @Override
                public String toString()
                {
                    return "[]";
                }
            };
        }

        return col;
    }

    public static <K, V> Map<K, V> nullSafe(final Map<K, V> map)
    {
        if (map == null)
        {
            return Collections.emptyMap();
        }
        return map;
    }

    public static void safeClear(Collection<?> col)
    {
        if (col != null)
        {
            col.clear();
        }
    }

    public static void safeClear(Map<?, ?> map)
    {
        if (map != null)
        {
            map.clear();
        }
    }

    public static void safeClear(MultiMap<?, ?> map)
    {
        if (map != null)
        {
            map.clear();
        }
    }

    public static <T extends IBase> boolean allEnabled(final Iterable<T> col)
    {
        for (T object : col)
        {
            if (object != null && !object.enabled())
            {
                return false;
            }
        }
        return true;
    }

    public static <T extends Persistent> List<Long> getIds(final Iterable<T> col)
    {
        final ArrayList<Long> ret = new ArrayList<Long>();/* an immutable list cannot be cleared later */

        if (col != null)
        {
            ret.addAll(CollectionUtils.collect((Collection<T>) col, new GetIdTransformer<T>()));
        }

        return ret;
    }

    public static void cleanUpArray(Object[][][] array)
    {
        if (array != null)
        {
            for (int i = 0; i < array.length; i++)
            {
                for (int j = 0; j < array[i].length; j++)
                {
                    Arrays.fill(array[i][j], null);
                }
            }
        }
    }

    public static Object[][] getColFromArray(Object[][][] array, int colIndex)
    {
        Object[][] ret = new Object[array.length][];

        for (int i = 0; i < array.length; i++)
        {
            ret[i] = array[i][colIndex];
        }

        return ret;
    }

    public static Object[] getColFromArray(Object[][] array, int colIndex)
    {
        Object[] ret = new Object[array.length];

        for (int i = 0; i < array.length; i++)
        {
            ret[i] = array[i][colIndex];
        }

        return ret;
    }

    /**
     * Given objects implementing Persistent, returns a Map with KEY=id, VALUE=object
     * 
     * @param <T>
     * @param col
     * @return
     */
    public static <T extends Persistent> Map<Long, T> to_Id_EntityMap(Iterable<T> col)
    {
        Map<Long, T> map = new HashMap<Long, T>();
        for (T obj : col)
        {
            map.put(obj.getId(), obj);
        }

        return map;
    }

    // public static <T> T getLastOrNull(List<T> list)
    // {
    // T ret = null;
    //
    // if (isNotEmpty(list))
    // {
    // ret = list.get(list.size() - 1);
    // }
    //
    // return ret;
    // }

    public static <T> T getLastOrNull(Iterable<T> iterable)
    {
        T ret = null;

        if (isNotEmpty(iterable))
        {
            T[] array = IteratorUtils.toArray(iterable.iterator());
            ret = array[array.length - 1];
        }

        return ret;
    }

    public static <T> T getFirstOrNull(Iterable<T> iterable)
    {
        T ret = null;

        if (isNotEmpty(iterable))
        {
            ret = IteratorUtils.toArray(iterable.iterator())[0];
        }

        return ret;
    }

    public static <T> List<T> getAllExceptLast(List<T> list)
    {
        List<T> ret = null;

        if (isNotEmpty(list))
        {
            ret = list.subList(0, list.size() - 1);
        }

        return ret;
    }

    public static <T> boolean addIfNotNull(Collection<T> collection, T object)
    {
        boolean ret = false;

        if (collection != null && object != null)
        {
            ret = collection.add(object);
        }

        return ret;
    }

    public static <T> boolean addAllIfNotEmpty(Collection<T> collection, Collection<T> objects)
    {
        boolean ret = false;

        if (collection != null && isNotEmpty(objects))
        {
            ret = collection.addAll(objects);
        }

        return ret;
    }

    public static <O, I> List<O> convertTo(final Iterable<I> input, final Transformer<I, O> transformer)
    {
        final List<O> ret = new ArrayList<O>();

        if (input != null)
        {
            ret.addAll(CollectionUtils.collect((Collection<I>) input, transformer));
        }

        return ret;
    }

    public static List<? extends IBase> toEntities(final Iterable<Object> objects)
    {
        return convertTo(objects, new ObjectToEntityTransformer<IBase>());
    }

    public static List<? extends EntityDescriptor> toEntityDescriptors(final Iterable<Object> objects)
    {
        return convertTo(objects, new ObjectToEntityDescriptorTransformer<EntityDescriptor>());
    }

    public static List<Long> toLong(final Iterable<String> idStrings)
    {
        return convertTo(idStrings, new LongValueTransformer());
    }

    public static List<String> toString(final Iterable<Long> ids)
    {
        return convertTo(ids, StringValueTransformer.<Long> getInstance());
    }

    public static List<Integer> toInt(final Iterable<String> idStrings)
    {
        return convertTo(idStrings, new IntValueTransformer());
    }

    public static List<Date> toDate(final Iterable<String> dateStrings)
    {
        return convertTo(dateStrings, new DateValueTransformer());
    }

    public static void removeNull(final Collection<?> col)
    {
        org.apache.commons.collections.CollectionUtils.filter(col, NotNullPredicate.INSTANCE);
    }

    public static Collection<?> select(final Collection<?> col, final Class<?> clazz)
    {
        return CollectionUtils.select(col, new MatchClassPredicate(clazz));
    }

    @SuppressWarnings("unchecked")
    public static <T> Collection<T> selectNotNull(final Collection<T> col)
    {
        return CollectionUtils.<T> select(col, org.apache.commons.collections15.functors.NotNullPredicate.INSTANCE);
    }

    public static <T extends ILPersistent> List<Long> getLightIds(final Iterable<T> col)
    {
        final List<Long> ret = new ArrayList<Long>();

        if (col != null)
        {
            ret.addAll(CollectionUtils.collect((Collection<T>) col, new GetLightIdTransformer<T>()));
        }

        return ret;
    }

    public static Long nullSafeId(final Persistent object)
    {
        return object == null ? null : object.getId();
    }

    public static <T extends Persistent> List<T> reorderById(final List<Long> ids, final List<T> objects)
    {
        return reorder(ids, objects, new GetIdTransformer<T>());
    }

    /**
     * Note: since there is no easy and safe way to find out the Type given the parameter, better pass it as an argument
     * 
     * @throws Hd3dPersistenceException
     */
    public static <T extends IBase> void merge(final Collection<T> coll, final Collection<Long> ids,
            final Class<T> clazz) throws Hd3dException
    {
        if (coll != null && ids != null && clazz != null)
        {
            final String className = clazz.getCanonicalName();
            final IPersist<T> persistor = Persistors.getPersistor(className);

            final TransitionDifferences diff = TransitionDifferences.findTransition(getIds(coll), ids);

            final List<T> toAdd = CollectUtils.reorderById(new ArrayList(diff.toAdd), persistor.getByIds(null,
                    diff.toAdd));

            final List<T> toRemove = CollectUtils.reorderById(new ArrayList(diff.toRemove), persistor.getByIds(null,
                    diff.toRemove));

            coll.removeAll(toRemove);
            coll.addAll(toAdd);
        }
    }

    public static <KEY, VALUE extends Persistent> List<VALUE> reorder(final List<KEY> orderedKeys,
            final List<VALUE> objectsToTransform, final Transformer<VALUE, KEY> objectToKeyTransformer)
    {
        List<VALUE> ret;

        boolean mapStrategy = new HashSet<KEY>(orderedKeys).size() == new HashSet<Long>(CollectUtils
                .getIds(objectsToTransform)).size();

        if (mapStrategy)
        {
            Map<KEY, VALUE> orderedMap = new LinkedHashMap<KEY, VALUE>();
            for (KEY key : orderedKeys)
            {
                orderedMap.put(key, null);
            }

            MapUtils.addToMap(orderedMap, objectsToTransform, objectToKeyTransformer);

            ret = new ArrayList<VALUE>(orderedMap.values());
        }
        else
        {
            ret = new ArrayList<VALUE>(orderedKeys.size());

            for (KEY id : orderedKeys)
            {
                boolean found = false;
                for (VALUE object : objectsToTransform)
                {
                    if (id.equals(objectToKeyTransformer.transform(object)))
                    {
                        ret.add(object);
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    ret.add(null);
                }
            }
        }

        return ret;
    }

    public static List asList(final Object value, final int size)
    {
        final Object[] array = new Object[size];
        Arrays.fill(array, value);
        return Arrays.asList(array);
    }

    public static List<?> union(final List<List<?>> lists)
    {
        List<?> ret = null;

        if (CollectUtils.isNotEmpty(lists))
        {
            if (lists.size() == 1)
            {
                ret = lists.get(0);
            }
            else if (lists.size() == 2)
            {
                ret = ListUtils.union(lists.get(0), lists.get(1));
            }
            else
            {
                ret = ListUtils.union(lists.get(0), union(lists.subList(1, lists.size() - 1)));
            }
        }

        return ret;
    }

    public static List<Integer> partition(final int number, final int nbSlices)
    {
        List<Integer> ret = new ArrayList<Integer>();

        if (number > 0)
        {
            if (nbSlices <= 1)
            {
                ret.add(number);
            }
            else if (Math.abs(number / nbSlices) > 1)
            {
                for (int i = number; i >= Math.abs(number / nbSlices); i = i - Math.abs(number / nbSlices))
                {
                    ret.add(Math.abs(number / nbSlices));
                }
                /* remaining */
                if (number % nbSlices > 0)
                {
                    ret.add(number % nbSlices);
                }
            }
            else
            {
                ret.add(number);
                for (int i = 0; i < nbSlices - 1; i++)
                {
                    ret.add(0);
                }
            }
        }

        return ret;
    }

    public static void main(String[] args)
    {
        System.out.println(partition(2000, 3));
    }

    public static <T> List<List<T>> partition(final List<T> list, int size)
    {
        final List<List<T>> ret = new ArrayList<List<T>>();

        if (isEmpty(list))
        {
            ret.add(list);
        }
        else
        {
            final int listSize = list.size();
            final int nbPartitions = listSize / size;
            int end = 0;

            for (int i = 0; i < nbPartitions; i++)
            {
                final int start = i * size;
                end = Math.min(start + size, listSize);
                ret.add(list.subList(start, end));
            }
            /* remaining */
            ret.add(list.subList(Math.min(end, listSize), listSize));
        }

        return ret;
    }

    /**
     * Note: this code is from : http://www.vogella.de/articles/JavaAlgorithmsPartitionCollection/article.html
     * 
     * Returns consecutive {@linkplain List#subList(int, int) sublists} of a list, each of the same size (the final list
     * may be smaller). For example, partitioning a list containing {@code [a, b, c, d, e]} with a partition size of 3
     * yields {@code [[a, b, c], [d, e]]} -- an outer list containing two inner lists of three and two elements, all in
     * the original order.
     * 
     * <p>
     * The outer list is unmodifiable, but reflects the latest state of the source list. The inner lists are sublist
     * views of the original list, produced on demand using {@link List#subList(int, int)}, and are subject to all the
     * usual caveats about modification as explained in that API.
     * 
     * * Adapted from http://code.google.com/p/google-collections/
     * 
     * @param list
     *            the list to return consecutive sublists of
     * @param size
     *            the desired size of each sublist (the last may be smaller)
     * @return a list of consecutive sublists
     * @throws IllegalArgumentException
     *             if {@code partitionSize} is nonpositive
     * 
     */
    public static <T> List<List<T>> splitIntoXparts(List<T> list, int size)
    {
        if (list == null)
            throw new NullPointerException("'list' must not be null");
        if (!(size > 0))
            throw new IllegalArgumentException("'size' must be greater than 0");

        return new Partition<T>(list, size);
    }

    private static class Partition<T> extends AbstractList<List<T>>
    {

        final List<T> list;
        final int size;

        Partition(List<T> list, int size)
        {
            this.list = list;
            this.size = size;
        }

        @Override
        public List<T> get(int index)
        {
            int listSize = size();
            if (listSize < 0)
                throw new IllegalArgumentException("negative size: " + listSize);
            if (index < 0)
                throw new IndexOutOfBoundsException("index " + index + " must not be negative");
            if (index >= listSize)
                throw new IndexOutOfBoundsException("index " + index + " must be less than size " + listSize);
            int start = index * size;
            int end = Math.min(start + size, list.size());
            return list.subList(start, end);
        }

        @Override
        public int size()
        {
            return (list.size() + size - 1) / size;
        }

        @Override
        public boolean isEmpty()
        {
            return list.isEmpty();
        }
    }

    /**
     * A Map value may contain a collection. Splits the collection by value size. Ex:key=A, value=[x,x,x]. If size=2,
     * result will be key=A, value=[[x,x],[x]]
     * 
     * @param multiMap
     * @param size
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Map partitionValue(final Map map, int size)
    {
        Map ret = null;

        if (map != null)
        {
            Entry ent;
            ret = new HashMap(map.size());
            for (Object entry : map.entrySet())
            {
                ent = (Entry) entry;

                Object val;
                if (ent.getValue() instanceof Collection)
                {
                    val = partition(new ArrayList((Collection) ent.getValue()), size);
                }
                else
                {
                    val = ent.getValue();
                }

                ret.put(ent.getKey(), val);
            }
        }

        return ret;
    }

    public static <K, V> Map<K, List<List<V>>> partitionValue(final MultiMap<K, V> multimap, int size)
    {
        Map<K, List<List<V>>> ret = null;

        if (multimap != null)
        {
            ret = new HashMap<K, List<List<V>>>(multimap.size());
            for (Entry<K, Collection<V>> entry : multimap.entrySet())
            {
                ret.put(entry.getKey(), (List<List<V>>) partition(new ArrayList<V>(entry.getValue()), size));
            }
        }

        return ret;
    }

    public static <T> void doByPartition(final List<T> col, int size, final Closure<List<T>> closure)
    {
        final List<List<T>> partitions = partition(col, size);
        CollectionUtils.forAllDo(partitions, closure);
        partitions.clear();
    }

    public static Object[] tail(Object[] array)
    {
        if (array == null)
            return array;

        if (array.length == 1)
            return array;

        return ArrayUtils.subarray(array, 1, array.length);
    }

}
