package fr.hd3d.utils;

import static fr.hd3d.common.client.ScriptConst.CREATE;
import static fr.hd3d.common.client.ScriptConst.DELETE;
import static fr.hd3d.common.client.ScriptConst.PRE_CREATE;
import static fr.hd3d.common.client.ScriptConst.PRE_DELETE;
import static fr.hd3d.common.client.ScriptConst.PRE_UPDATE;
import static fr.hd3d.common.client.ScriptConst.UPDATE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.Map.Entry;

import net.sf.sojo.interchange.json.JsonParser;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.criterion.Restrictions;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Encoding;
import org.restlet.engine.application.EncodeRepresentation;
import org.restlet.representation.Representation;

import fr.hd3d.common.client.Constraint;
import fr.hd3d.common.client.Persist;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.audit.Audit;
import fr.hd3d.model.exec.ScriptExec;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataValueH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataValuesToCreate;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.ScriptH;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.query.Queries;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.resources.ResourceContext;


public final class ServerResourceUtils<L extends ILBase, P extends IBase>
{
    private ServerResourceUtils()
    {}

    // Key=event, value=operation
    private static final Map<String, String> EVENT_OPERATION_MAP = Collections
            .unmodifiableMap(new HashMap<String, String>() {
                {
                    put(Persist.PREPERSIST, PRE_CREATE);
                    put(Persist.PREUPDATE, PRE_UPDATE);
                    put(Persist.PREDELETE, PRE_DELETE);
                    put(Persist.POSTPERSIST, CREATE);
                    put(Persist.POSTUPDATE, UPDATE);
                    put(Persist.POSTDELETE, DELETE);
                }
            });

    @SuppressWarnings("unchecked")
    private static <L extends ILBase> Object getLightWeight(final Object object, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        Object ret = null;

        if (object != null)
        {
            if (IBase.class.isInstance(object))
            {
                final IBase baseObj = (IBase) object;
                final IBaseTranslator translator = baseObj.defaultTranslator();
                if (translator != null)
                {
                    ret = (L) translator.toLightweight(baseObj, context);
                }
            }
            else
            {
                ret = object;
            }
        }

        return ret;
    }

    /**
     * A object property may be a Hibernate CGLIB enhanced class, thus making Json unserialization fail. If it is an
     * enhanced class, returns the lightweight representation.
     * 
     * @param value
     * @return
     * @throws Hd3dException
     */
    @SuppressWarnings("unchecked")
    public static Object dynaBeanPropSafeConvert(final Object value, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        Object ret = null;

        if (value != null)
        {
            if (java.util.Collection.class.isInstance(value))
            {
                final Collection values = (Collection) value;
                CollectUtils.removeNull(values);

                if (CollectUtils.isNotEmpty(values))
                {
                    ret = getLightWeight(values, context);
                }
                else
                {
                    ret = value;
                }
            }
            else if (IBase.class.isInstance(value))
            {
                ret = getLightWeight(value, context);
            }
            else if (value instanceof Enum)
            {
                ret = ((Enum<?>) value).name();
            }
            else if (value instanceof Object[])
            {
                ret = getArrayLightWeight((Object[]) value, context);
            }
            else
            {
                ret = value;
            }
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    private static Object getLightWeight(final Collection<Object> objects, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        Collection<Object> ret = null;
        try
        {
            Class<?> clazz = getMappedHibernateCollection(objects);
            if (clazz != null)
            {
                ret = (Collection<Object>) clazz.newInstance();
            }
            else
            {
                throw new Hd3dException("Error: unable to obtain a collection from :" + objects);
            }
        }
        catch (InstantiationException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
        catch (IllegalAccessException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }

        CollectUtils.removeNull(objects);
        if (CollectUtils.isNotEmpty(objects))
        {
            for (Object obj : objects)
            {
                if (java.util.Collection.class.isInstance(obj))
                {
                    final Collection values = (Collection) obj;
                    CollectUtils.removeNull(values);

                    if (CollectUtils.isNotEmpty(values))
                    {
                        ret.add(getLightWeight(values, context));
                    }
                    else
                    {
                        ret.add(obj);
                    }
                }
                else if (IBase.class.isInstance(obj))
                {
                    final IBase baseObj = (IBase) obj;
                    final IBaseTranslator translator = baseObj.defaultTranslator();
                    if (translator != null)
                    {
                        ret.add(translator.toLightweight(baseObj, context));
                    }
                }
                else
                {
                    ret.add(obj);
                }
            }
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    private static Object getArrayLightWeight(final Object[] objects, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        List ret = new ArrayList();

        if (!ArrayUtils.isEmpty(objects))
        {
            for (Object obj : objects)
            {
                if (java.util.Collection.class.isInstance(obj))
                {
                    final Collection values = (Collection) obj;
                    CollectUtils.removeNull(values);

                    if (CollectUtils.isNotEmpty(values))
                    {
                        ret.add(getLightWeight(values, context));
                    }
                    else
                    {
                        ret.add(obj);
                    }
                }
                else if (IBase.class.isInstance(obj))
                {
                    final IBase baseObj = (IBase) obj;
                    final IBaseTranslator translator = baseObj.defaultTranslator();
                    if (translator != null)
                    {
                        ret.add(translator.toLightweight(baseObj, context));
                    }
                }
                else
                {
                    ret.add(objects);
                }
            }
        }

        return ret.toArray();
    }

    public static Representation encodeRepresentation(final Representation representation)
    {
        Representation ret;

        if (fr.hd3d.utils.Conf.compressResponse())
        {
            ret = new EncodeRepresentation(Encoding.GZIP, representation);
        }
        else
        {
            ret = representation;
        }

        return ret;
    }

    public static void save(final Audit audit)
    {
        if (audit == null)
            return;

        audit.flushModification();
        HibernateUtil.currentSession().persist(audit);
    }

    public static void save(final List<Audit> audits) throws Hd3dException
    {
        CollectUtils.removeNull(audits);
        if (CollectUtils.isNotEmpty(audits))
        {
            for (Audit audit : audits)
            {
                audit.flushModification();
            }
            HibernatePersist.persistRawCollection(audits);
        }
    }

    public static String getScriptTrigger(final IBase object, final String event)
    {
        return new StringBuilder(50).append(object.entityName()).append('.').append(event).toString();
    }

    /**
     * Run the scripts triggered by the event. Note: must be called AFTER the the response identifier (url of the
     * resource) has been set, since it is passed to the scripts to retrieve the object
     * 
     * @param object
     * @param event
     *            that triggered the script. Possible values are: "prePersist", "postPersist", "preUpdate",
     *            "postUpdate", "preDelete", "postDelete"
     * @param identifier
     *            is the uri identifying the resource passed to the script as parameter
     * @return a message about script execution
     * @throws Hd3dPersistenceException
     */
    public static String executeScripts(final IBase object, final String event, final String identifier)
            throws Hd3dPersistenceException
    {
        if (object == null || event == null || identifier == null)
            return null;

        final String trigger = getScriptTrigger(object, event);
        if (!ScriptH.scripts().containsKey(trigger))// not an existing trigger
            return null;

        final Map<String, String> params = new HashMap<String, String>(3);
        params.put(ScriptExec.OPERATION, EVENT_OPERATION_MAP.get(event));
        params.put(ScriptExec.ENTITYNAME, object.entityName());
        // Make sure that the identifier has been properly set previously
        params.put(ScriptExec.URL, identifier);

        return new ScriptExec(trigger, params).exec();
    }

    public static final String info(final Request request)
    {
        return StringUtils.join(new Object[] { request.getClientInfo().getAddress(),
                request.getClientInfo().getAgent(), request.getMethod(), request.getOriginalRef() }, ' ');
    }

    public static String info(final Response response)
    {
        return StringUtils.join(new Object[] { info(response.getRequest()), response.getStatus() }, ' ');
    }

    public static String info(final Response response, Bench b)
    {
        return StringUtils.join(new Object[] { info(response.getRequest()), response.getStatus(), b.e() + "ms" }, ' ');
    }

    public static String[] buildErrorMessage(final Throwable excep, final String message)
    {
        String[] errorMessage = new String[2];
        if (excep != null)
        {
            errorMessage[0] = message + ": " + excep.getMessage();
            errorMessage[1] = ExceptUtils.format(excep);
        }
        return errorMessage;
    }

    public static synchronized void createDynMetaDataValuesFor(List<? extends IBase> list, final String entityName)
            throws Hd3dException
    {
        if (CollectUtils.isNotEmpty(list) && BaseH.DYNVALUES_TO_CREATE_SET.contains(entityName))
        {
            for (List<Long> idsPart : CollectUtils.partition(CollectUtils.getIds(list), Conf.getMaxRecords()))
            {
                List<DynMetaDataValuesToCreate> toCreate = Queries.getDynMetaDataValuesToCreate(entityName, idsPart);

                ServerResourceUtils.createDynMetaDataValues(toCreate, entityName);
                toCreate.clear();
            }
        }
    }

    public static synchronized <X extends IBase> void createDynMetaDataValueFor(final X object, final String entityName)
            throws Hd3dException
    {
        if (object != null && BaseH.DYNVALUES_TO_CREATE_SET.contains(entityName))
        {
            List<DynMetaDataValuesToCreate> toCreate = Queries.getDynMetaDataValuesToCreate(entityName, object.getId());

            ServerResourceUtils.createDynMetaDataValues(toCreate, entityName);
            toCreate.clear();
        }
    }

    /**
     * Creates DynMetaDataValues for the given entity
     * 
     * @param toCreate
     * @param entityName
     * @throws Hd3dPersistenceException
     */
    public static synchronized void createDynMetaDataValues(final List<DynMetaDataValuesToCreate> toCreate,
            final String entityName) throws Hd3dException
    {
        CollectUtils.removeNull(toCreate);

        if (CollectUtils.isNotEmpty(toCreate) && EntitiesMaps.isEntity(entityName))
        {
            /* create dynmetadatavalues */
            createDynMetaDataValues(toCreate);

            /* delete corresponding records in DynMetaDataValuesToCreate table */
            /* still exist ? */
            cleanDynMetaDataValuesToCreate(toCreate);

            /* update in memory */
            if (CollectUtils.isEmpty(Queries.getDynMetaDataValuesToCreate(entityName)))
            {
                BaseH.DYNVALUES_TO_CREATE_SET.remove(entityName);
            }
        }
    }

    private static void createDynMetaDataValues(final List<DynMetaDataValuesToCreate> toCreate) throws Hd3dException
    {
        List<List<DynMetaDataValuesToCreate>> partitions = CollectUtils.partition(toCreate, Conf.getMaxRecords());
        for (List<DynMetaDataValuesToCreate> part : partitions)
        {
            final List<IDynMetaDataValue> dynValuesToCreate = new ArrayList<IDynMetaDataValue>(part.size());
            for (DynMetaDataValuesToCreate x : part)
            {
                dynValuesToCreate.add(new DynMetaDataValueH(x.getClassDynMetaDataType(), x.getParent(), x
                        .getParentType()));
            }

            Persistors.dynmetadatavalue.persistCollection(dynValuesToCreate);
            /* delete corresponding records in DynMetaDataValuesToCreate table */
            /* still exist ? */
            cleanDynMetaDataValuesToCreate(part);
        }
    }

    @SuppressWarnings("unchecked")
    private static void cleanDynMetaDataValuesToCreate(final List<DynMetaDataValuesToCreate> toCreate)
            throws Hd3dException
    {
        final List<String> idsToCreate = new ArrayList<String>(toCreate.size());
        for (DynMetaDataValuesToCreate d : toCreate)
        {
            idsToCreate.add(d.getId());
        }

        if (CollectUtils.isNotEmpty(idsToCreate))
        {
            final Criteria criteria = HibernateUtil.currentSession().createCriteria(DynMetaDataValuesToCreate.class)
                    .add(Restrictions.in(Const.ID, idsToCreate)).setLockMode(LockMode.OPTIMISTIC);
            final List<DynMetaDataValuesToCreate> list = criteria.list();

            if (!list.isEmpty())
            { /* if so, delete */
                HibernatePersist.deleteRawCollection(toCreate);
            }
        }
    }

    public static void addIdEqRestriction(final ResourceContext<?, ?> context, final Criteria query,
            final String attribute, final Long id)
    {
        if (context != null && query != null && attribute != null)
        {
            if (context.getFilter() != null && context.getFilter().containsCreateAliasModifier(attribute))
            {
                query.add(Restrictions.eq(attribute + ".id", id));
            }
            else
            {
                /*
                 * Do not check whether attribute is part of context.getPersistedClass or not, because query may have
                 * declared a .createCriteria, thus changing the persisted class
                 */
                query.createCriteria(attribute).add(Restrictions.idEq(id));
            }
        }
    }

    public static void addIdEqRestriction(final ResourceContext<?, ?> context, final Criteria query,
            final String attribute, final Long id, Class<?> clazz)
    {
        if (context != null && query != null && attribute != null)
        {
            if (context.getFilter() != null && context.getFilter().containsCreateAliasModifier(attribute))
            {
                query.add(Restrictions.eq(attribute + ".id", id));
            }
            else if (clazz != null && EntitiesMaps.withClass(clazz).isAssociation(attribute))
            {
                /*
                 * Do not check whether attribute is part of context.getPersistedClass or not, because query may have
                 * declared a .createCriteria, thus changing the persisted class
                 */
                query.createCriteria(attribute).add(Restrictions.idEq(id));
            }
            else
            {
                query.add(Restrictions.idEq(id));
            }
        }
    }

    public static Class<?> getMappedHibernateCollection(Collection<Object> col)
    {
        Class<?> ret = null;

        if (col != null)
        {
            if (List.class.isInstance(col))
            {
                ret = ArrayList.class;
            }
            else if (Set.class.isInstance(col))
            {
                if (HashSet.class.isInstance(col))
                {
                    ret = HashSet.class;
                }
                else if (SortedSet.class.isInstance(col))
                {
                    ret = SortedSet.class;
                }
                else
                {
                    ret = HashSet.class;
                }
            }
        }

        return ret;
    }

    public static Pair<Integer, Integer> getStartAndEndIndex(Map<String, String> queryMap, int collectionSize)
            throws Hd3dQueryFilterException
    {
        Pair<Integer, Integer> ret;

        /* Parse query, get the attributes FIRST and QUANTITY. */
        final String query = (String) queryMap.get(Constraint.PAGINATION);
        final Map<String, Long> paginationMap = query == null ? null : (Map<String, Long>) new JsonParser()
                .parse(query);

        if (paginationMap != null && collectionSize > 0)
        {
            final Set<Entry<String, Long>> entrySet = paginationMap.entrySet();
            CollectUtils.removeNull(entrySet);

            int first = 0;
            int qty = 50;
            String key;
            Long value;
            for (Entry<String, Long> entry : entrySet)
            {
                key = entry.getKey();
                value = entry.getValue();
                if (Constraint.FIRST_MAP_FIELD.equals(key))
                {
                    first = value.intValue();
                }
                else if (Constraint.QUANTITY_MAP_FIELD.equals(key))
                {
                    qty = value.intValue();
                }
                else
                {
                    throw new Hd3dQueryFilterException(query);
                }
            }

            final int startIndex = Math.min(first >= 0 ? first : 0, collectionSize);

            ret = new Pair<Integer, Integer>(startIndex, Math.min(startIndex + qty, collectionSize));
        }
        else
        {
            ret = new Pair<Integer, Integer>(0, collectionSize);
        }

        return ret;
    }
}
