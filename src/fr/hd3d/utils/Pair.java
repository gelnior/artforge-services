package fr.hd3d.utils;

public class Pair<C1, C2>
{
    C1 field_1;
    C2 field_2;

    public Pair(C1 c1, C2 c2)
    {
        this.field_1 = c1;
        this.field_2 = c2;
    }

    public C1 getField_1()
    {
        return field_1;
    }

    public C2 getField_2()
    {
        return field_2;
    }

    public void setField_1(C1 c1)
    {
        this.field_1 = c1;
    }

    public void setField_2(C2 c2)
    {
        this.field_2 = c2;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((field_1 == null) ? 0 : field_1.hashCode());
        result = prime * result + ((field_2 == null) ? 0 : field_2.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pair<C1, C2> other = (Pair<C1, C2>) obj;
        if (field_1 == null)
        {
            if (other.field_1 != null)
                return false;
        }
        else if (!field_1.equals(other.field_1))
            return false;
        if (field_2 == null)
        {
            if (other.field_2 != null)
                return false;
        }
        else if (!field_2.equals(other.field_2))
            return false;
        return true;
    }
}
