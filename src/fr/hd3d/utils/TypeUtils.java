package fr.hd3d.utils;

import java.util.ArrayList;
import java.util.List;


public final class TypeUtils
{

    /**
     * Used to convert an Object to Integer. Useful when retrieving numeric value from JSON, since only Long is used in
     * JSON
     * 
     * @param value
     * @return
     */
    public static Integer toInt(Object value)
    {
        Integer val;
        if (Long.class.isInstance(value))
            val = ((Long) value).intValue();
        else
            val = (Integer) value;
        return val;
    }

    public static List<Integer> toInt(final List<Integer> values)
    {
        List<Integer> ret = new ArrayList<Integer>(values.size());

        for (java.util.Iterator<Integer> it = values.iterator(); it.hasNext();)
            ret.add(toInt(it.next()));

        return ret;
    }
}
