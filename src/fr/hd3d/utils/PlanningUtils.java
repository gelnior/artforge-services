/**
 * 
 */
package fr.hd3d.utils;

import java.util.Calendar;
import java.util.Date;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;


/**
 * @author thomas-eskenazi
 * 
 */
public final class PlanningUtils
{
    /**
     * Iterates over the tasks of a constituent to find the first start date and the last end date for the given
     * baseOobject.
     * 
     * SHALL remain private, to ensure on which objects this method is called.
     * 
     * @param baseObject
     * @return a Date[] where Date[0] is the start date and Date [1] the end date. If no start or end date is found,
     *         return an array of null.
     */
    private static Date[] getStartandEndDateForBaseObject(IBase baseObject)
    {
        Date[] result = new Date[2];
        boolean hasBeenStartDateSetted = false;
        boolean hasBeenEndDateSetted = false;
        Calendar farFarWaway = Calendar.getInstance();
        farFarWaway.add(Calendar.YEAR, 1000);
        Date begin = farFarWaway.getTime();
        Date end = new Date(0);
        for (IEntityTaskLink taskLink : baseObject.getEntityTaskLinks())
        {

            if (taskLink.getTask().getStartDate() != null && begin.after(taskLink.getTask().getStartDate()))
            {
                begin = taskLink.getTask().getStartDate();
                hasBeenStartDateSetted = true;
            }
            if (taskLink.getTask().getEndDate() != null && end.before(taskLink.getTask().getEndDate()))
            {
                end = taskLink.getTask().getEndDate();
                hasBeenEndDateSetted = true;
            }
        }
        if (hasBeenStartDateSetted && hasBeenEndDateSetted)
        {
            result[0] = begin;
            result[1] = end;
        }
        return result;
    }

    public static Date[] getStartandEndDateForCategory(Long categoryID)
    {
        ICategory category = null;
        try
        {
            category = Persistors.category.getById(categoryID);
        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
            return new Date[] {};
        }
        return getStartandEndDateForCategory(category);
    }

    /**
     * Iterates over the tasks of a constituent to find the first start date and the last end date for the given
     * category.
     * 
     * @param category
     * @return a Date[] where Date[0] is the start date and Date [1] the end date. If no start or end date is found,
     *         return an array of null.
     */
    public static Date[] getStartandEndDateForCategory(ICategory category)
    {
        Date[] result = new Date[2];
        boolean hasBeenStartDateSetted = false;
        boolean hasBeenEndDateSetted = false;
        Calendar farFarWaway = Calendar.getInstance();
        farFarWaway.add(Calendar.YEAR, 1000);
        Date begin = farFarWaway.getTime();
        Date end = new Date(0);

        for (ICategory category2 : category.getChildren())
        {
            Date[] tempDate = getStartandEndDateForCategory(category2);
            if (tempDate[0] != null && tempDate[1] != null)
            {
                if (begin.after(tempDate[0]))
                {
                    begin = tempDate[0];
                    hasBeenStartDateSetted = true;
                }
                if (end.before(tempDate[1]))
                {
                    end = tempDate[01];
                    hasBeenEndDateSetted = true;
                }
            }
        }
        for (IConstituent constituent : category.getConstituents())
        {
            Date[] tempDate = getStartandEndDateForConstituent(constituent);
            if (tempDate[0] != null && tempDate[1] != null)
            {
                if (begin.after(tempDate[0]))
                {
                    begin = tempDate[0];
                    hasBeenStartDateSetted = true;
                }
                if (end.before(tempDate[1]))
                {
                    end = tempDate[01];
                    hasBeenEndDateSetted = true;
                }
            }
        }
        if (hasBeenStartDateSetted && hasBeenEndDateSetted)
        {
            result[0] = begin;
            result[1] = end;
        }
        return result;
    }

    public static Date[] getStartandEndDateForSequence(Long sequenceID)
    {
        ISequence sequence = null;
        try
        {
            sequence = Persistors.sequence.getById(sequenceID);
        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
            return new Date[] {};
        }
        return getStartandEndDateForSequence(sequence);
    }

    public static Date[] getStartandEndDateForSequence(ISequence rootSequence)
    {
        Date[] result = new Date[2];
        boolean hasBeenStartDateSetted = false;
        boolean hasBeenEndDateSetted = false;
        Calendar farFarWaway = Calendar.getInstance();
        farFarWaway.add(Calendar.YEAR, 1000);
        Date begin = farFarWaway.getTime();
        Date end = new Date(0);
        for (ISequence sequence : rootSequence.getChildren())
        {
            Date[] tempDate = getStartandEndDateForSequence(sequence);
            if (tempDate[0] != null && tempDate[1] != null)
            {
                if (begin.after(tempDate[0]))
                {
                    begin = tempDate[0];
                    hasBeenStartDateSetted = true;
                }
                if (end.before(tempDate[1]))
                {
                    end = tempDate[01];
                    hasBeenEndDateSetted = true;
                }
            }
        }
        for (IShot shot : rootSequence.getShots())
        {
            Date[] tempDate = getStartandEndDateForShot(shot);
            if (tempDate[0] != null && tempDate[1] != null)
            {
                if (begin.after(tempDate[0]))
                {
                    begin = tempDate[0];
                    hasBeenStartDateSetted = true;
                }
                if (end.before(tempDate[1]))
                {
                    end = tempDate[01];
                    hasBeenEndDateSetted = true;
                }
            }
        }
        if (hasBeenStartDateSetted && hasBeenEndDateSetted)
        {
            result[0] = begin;
            result[1] = end;
        }
        return result;

    }

    public static Date[] getStartandEndDateForShot(Long shotID)
    {
        IShot shot;
        try
        {
            shot = Persistors.shot.getById(shotID);
        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
            return new Date[] {};
        }
        return getStartandEndDateForShot(shot);
    }

    public static Date[] getStartandEndDateForShot(IShot shot)
    {
        return getStartandEndDateForBaseObject(shot);
    }

    public static Date[] getStartandEndDateForConstituent(Long constituentID)
    {
        IConstituent constituent = null;
        try
        {
            constituent = Persistors.constituent.getById(constituentID);
        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
            return new Date[] {};
        }
        return getStartandEndDateForConstituent(constituent);
    }

    public static Date[] getStartandEndDateForConstituent(IConstituent constituent)
    {
        return getStartandEndDateForBaseObject(constituent);
    }
}
