package fr.hd3d.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.math.NumberUtils;

import fr.hd3d.common.client.enums.EAssetStatus;
import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.common.client.enums.EFileStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionH;
import fr.hd3d.services.resources.InitializeBankResource;
import fr.hd3d.services.security.utils.AuthenticationUtil;


public final class FileRevisionUtils
{
    /**
     * Renvoi l'ID de la category ROOT pour le projet projectId. si -1 est passé, renvoit le category ROOT de la Reuse
     * Bank
     * 
     * @param projectId
     * @return
     * @throws Exception
     */
    public static Long getCategoryRootId(Long projectId) throws Hd3dException
    {
        IProject project;

        if (projectId.equals(NumberUtils.LONG_MINUS_ONE))
        {
            String bankProject = InitializeBankResource.REUSEBANK_PROJECT_STRING;
            project = Persistors.project.getObjectByValue(Const.NAME, bankProject);
            if (project == null)
            {
                Log.LOGGER.error("*** Error: could not find Bank Project : {}", bankProject);
                throw new Hd3dException(MessageFormat.format("*** Error: could not find Bank Project : {0}",
                        bankProject));
            }
            return project.getId();
        }
        else
        {
            project = Persistors.project.getById(projectId);
            return project.rootCategory().getId();
        }
    }

    // Renvoi l'ID de la sequence ROOT pour le projet projectId
    // si -1 est passé, renvoit le sequence ROOT de la Reuse Bank
    public static Long getTemporalRootId(Long projectId) throws Hd3dException
    {
        IProject project;

        if (projectId.equals(NumberUtils.LONG_MINUS_ONE))
        {
            String bankProject = InitializeBankResource.REUSEBANK_PROJECT_STRING;
            project = Persistors.project.getObjectByValue(Const.NAME, bankProject);
            if (project == null)
            {
                Log.LOGGER.error("*** Error: could not find Bank Project : {}", bankProject);
                throw new Hd3dException(MessageFormat.format("*** Error: could not find Bank Project : {0}",
                        bankProject));
            }
            return project.getId();
        }
        else
        {
            project = Persistors.project.getById(projectId);
            return project.rootSequence().getId();
        }
    }

    /**
     * Creates a temporary file from uploaded file then creates the file revision finally copies the file to the right
     * directory.
     * 
     * @param destDir
     *            the directory where it will be the file, if == "", it creates to the default directory to upload.
     * @param fileItem
     *            the file item containing the file to create.
     * @param key
     *            the key of the file revision
     * @param variation
     *            the variation of the file revision
     * @param revision
     *            the revision of the file revision
     * @param state
     *            the state of the file revision
     * @return the file revision
     */
    public static IFileRevision persistUploadedFile(String mountPoint, String destDir, FileItem fileItem, String key,
            String variation, Integer revision, EFileState state, IAssetRevision asset)
    {
        IFileRevision fileRevision = null;
        File tmpFile = null;
        File tmpDir = null;
        try
        {
            key = key.replaceAll("[\\\\]+", "/");
            String[] pathSegments = key.split("/");
            if (pathSegments != null && pathSegments.length > 0)
            {
                key = pathSegments[pathSegments.length - 1];
            }

            tmpDir = new File(FilenameMaker.getTemporaryFileFullPath());
            if (tmpDir.mkdirs() == false)
            {
                Log.LOGGER.error("*** Error: could not create temporary directory '{}'", tmpDir.getAbsolutePath());
                throw new Hd3dException("*** Error: could not create temporary directory '" + tmpDir.getAbsolutePath()
                        + "'");
            }
            /* create a UNIQUE temporary directory */
            tmpFile = createTemporaryFile(tmpDir, key, fileItem);

            if (revision == null)
            {
                revision = 1;
            }
            if (variation == null)
            {
                if (asset != null)
                {
                    variation = asset.getVariation();
                }
            }
            /* At this point the temporary file has been successfully created */
            fileRevision = createFileRevision(mountPoint, tmpFile, key, variation, revision, state, true, asset);

            /* Copy the tmp file to the good directory */
            if (mountPoint == null)
            {
                copyFileToDirectory(fileRevision, destDir, tmpFile);
            }

            return fileRevision;

        }
        catch (Exception e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            return null;
        }
        finally
        {
            if (tmpFile != null)
            {
                tmpFile.delete();
            }
            if (tmpDir != null)
            {
                /* delete temporary files */
                FileUtils.deleteQuietly(tmpDir);
            }
        }
    }

    /**
     * Copy the file to the destination folder.
     * 
     * @param fileRevision
     *            the file revision corresponding to the file
     * @param destFolder
     *            the destination folder
     * @param tmpFile
     *            the file to copy
     * @throws Hd3dException
     */
    private static void copyFileToDirectory(IFileRevision fileRevision, String destFolder, File tmpFile)
            throws Hd3dException
    {

        File destFile;
        File destDir;
        if (destFolder.equals(""))
        {
            destFolder = FilenameMaker.getFullPath(fileRevision);
            destFile = new File(destFolder);
            destDir = new File(fileRevision.getLocation());
        }
        else
        {
            destFolder = Conf.getFileTreeDir() + destFolder;
            destFile = new File(destFolder + '/' + fileRevision.getRelativePath());
            destDir = new File(destFolder);
        }

        /* On crée le répertoire de destination si nécessaire */

        if (!destDir.exists())
        {
            if (destDir.mkdirs() == false)
            {
                Log.LOGGER.error("*** Error: could not create directory '{}'", destDir.getAbsolutePath());
                throw new Hd3dException("*** Error: could not create directory '" + destDir.getAbsolutePath() + "'");
            }
        }
        try
        {
            FileUtils.copyFile(tmpFile, destFile);
        }
        catch (IOException ioe)
        {
            Log.LOGGER.error("*** Error: could not rename file from '{}' to '{}'", tmpFile.getPath(), destFile
                    .getPath());
            throw new Hd3dException("*** Error: could not rename file from '" + tmpFile.getPath() + "' to '"
                    + destFile.getPath() + "'");
        }
        fileRevision.setLocation(destDir.getPath());
        Persistors.filerevision.merge(fileRevision);

    }

    /**
     * Creates the file revision.
     * 
     * @param fileToStore
     *            the file which link to the file revision
     * @param key
     *            the key of the file revision
     * @param variation
     *            the variation of the file revision
     * @param revision
     *            the revision of the file revision
     * @param state
     *            the state of the file revision
     * @param generatedDestinationPath
     *            if true generate a default path
     * @return the file revision
     * @throws Hd3dException
     */
    public static IFileRevision createFileRevision(String mountPoint, File fileToStore, String key, String variation,
            int revision, EFileState state, boolean generatedDestinationPath, IAssetRevision asset)
            throws Hd3dException
    {
        try
        {
            String filename = fileToStore.getName();
            String ext = StringUtils.substringAfterLast(filename, ".");

            IFileRevision fileRevision = new FileRevisionH(new Date(), key, variation, revision, getCreator(), ext,
                    fileToStore.length(), String.valueOf(FileUtils.checksumCRC32(fileToStore)), fileToStore
                            .getAbsolutePath(), filename, EFileStatus.UNLOCKED, new Date(), getCreator(), "", "prod",
                    null, state == null ? EFileState.getDefault() : state, mountPoint);

            fileRevision.setAssetRevision(asset);
            if (fileRevision == null)
            {
                Log.LOGGER.error("*** Error: Could not put file into database ! '{}'", fileToStore.getPath());
                throw new Hd3dException("*** Error: Could not put file into database ! '" + fileToStore.getPath() + "'");
            }
            boolean nok = true;
            while (nok)
            {
                try
                {
                    fileRevision.doPrePersist();
                    nok = false;
                }
                catch (Hd3dException e)
                {
                    if (e.getMessage().startsWith("A FileRevision with triplet(key,variation,revision)="))
                    {

                        fileRevision.setRevision(++revision);
                    }
                }
            }
            Persistors.filerevision.persist(fileRevision);
            fileRevision.doPostPersist();

            if (generatedDestinationPath)
            {
                /* On modifie les chemins de destination en fonction de l'ID */
                String newLocation = FilenameMaker.buildFileLocation(fileRevision);
                String newRelativePath = FilenameMaker.buildFileRelativePath(fileRevision);
                fileRevision.setLocation(newLocation);
                fileRevision.setRelativePath(newRelativePath);
                /* On sauve le tout. */
                Persistors.filerevision.merge(fileRevision);
            }

            return fileRevision;
        }
        catch (Exception e)
        {
            Log.LOGGER.error("*** Error: Could not put file into database ! '{}'", fileToStore.getPath());
            Log.LOGGER.error(ExceptUtils.format(e));
            throw new Hd3dException("*** Error: Could not put file into database ! '" + fileToStore.getPath() + "'");
        }
    }

    /**
     * Creates the temporary file from the uploaded file.
     * 
     * @param tmpDir
     *            the temporary directory to create the file
     * @param fileName
     *            the file name
     * @param fileItem
     *            the file item containing the uploaded file.
     * @return the temporary file.
     * @throws Hd3dException
     */
    private static File createTemporaryFile(File tmpDir, String fileName, FileItem fileItem) throws Hd3dException
    {

        /* On sauve le fichier dans un espace temporaire (pour calculer son checksum) */
        final String tempFilePath = tmpDir.getAbsoluteFile() + SystemUtils.FILE_SEPARATOR + fileName;
        File tmpFile = new File(tempFilePath);

        boolean isCreated;
        try
        {
            isCreated = tmpFile.createNewFile();
            if (!isCreated)
            {
                Log.LOGGER.error("*** Error: could not create temporary file '{}'", tempFilePath);
                throw new Hd3dException("*** Error: could not create temporary file '" + tempFilePath + "'");
            }

            fileItem.write(tmpFile);
            if (tmpFile.length() == 0)
            {
                Log.LOGGER.error("*** Warning: uploaded file is empty... '{}'", tmpFile.getAbsolutePath());
                throw new Hd3dException("*** Warning: uploaded file is empty... '" + tmpFile.getAbsolutePath() + "'");
            }
            return tmpFile;

        }
        catch (IOException e)
        {
            Log.LOGGER.error("*** Error: could not create temporary file '{}'", tempFilePath);
            throw new Hd3dException("*** Error: could not create temporary file '" + tempFilePath + "'");
        }
        catch (Exception e)
        {
            Log.LOGGER.error("*** Error: could not write temporary file '{}'", tmpFile.getAbsolutePath());
            throw new Hd3dException("*** Error: could not write temporary file '" + tmpFile.getAbsolutePath() + "'");
        }

    }

    /**
     * Attache un filerevision à une categorie/constituant/sequence/shot passé en paramètre. Dans un but de généricité,
     * il faut passer le project, ou bien changer l'interface de IBaseObject avec un getProject()
     */
    public static boolean attachFileToParent(IFileRevision filerevision, IBase parent, IProject project)
    {
        if (filerevision == null)
        {
            Log.LOGGER.error("*** Error: File is null (attachFileToParent)");
            return false;
        }
        if (parent == null)
        {
            Log.LOGGER.error("*** Error: Parent not found to attach file : {}", filerevision.getId());
            return false;
        }
        if (project == null)
        {
            Log.LOGGER.error("*** Error: Project not found to attach file : {}", filerevision.getId());
            return false;
        }

        try
        {
            /* On crée un asset... */
            final IAssetRevision asset = getOrCreateAsset(filerevision, project);
            if (asset != null)
            {
                // /* ... qu'on rattache au fichier */
                filerevision.setAssetRevision(asset);
                // getOrCreateAssetFileLink(filerevision);

                /* On crée un groupe rattaché au parent indiqué */
                IAssetRevisionGroup assetGroup = getOrCreateAssetGroupLinkedToParent(parent, project);
                if (assetGroup == null)
                {
                    return false;
                }
                /* assetGroup.addAssetRevision(asset); */
                asset.addAssetRevisionGroup(assetGroup);

                /* Puis on y ajoute l'asset (relié au fichier), simple, non ? :) */
                // assetGroup.addAssetRevision(asset);
                assetGroup.doPrePersist();
                Persistors.assetrevisiongroup.persist(assetGroup);
                assetGroup.doPostPersist();
                // System.out.println("CREATION GROUP: " + assetGroup.getId());
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            return false;
        }

    }

    /**
     * Gets or Creates an Asset corresponding to the given Filerevision
     * 
     * @param filerevision
     * @param project
     * @return
     * @throws Exception
     */
    private static IAssetRevision getOrCreateAsset(final IFileRevision filerevision, final IProject project)
            throws Exception
    {
        if (filerevision == null)
            return null;

        /* lookup the AssetRevision */
        final String key = filerevision.getKey();
        final String variation = filerevision.getVariation();
        final Integer revision = filerevision.getRevision();

        String[] properties = new String[] { Const.KEY, Const.VARIATION, Const.REVISION };
        Object[] values = new Object[] { key, variation, revision };
        List<IAssetRevision> assets = Persistors.assetrevision.getByValues(null, properties, values, "AND");

        /* AssetRevision found */
        if (assets != null)
        {
            if (assets.size() == 1)
            {
                return assets.get(0);
            }
            /* more than one AssetRevision found */
            else if (assets.size() > 1)
            {
                throw new Exception("Several assets found with same key/variation/revision!");
            }
        }
        /* No AssetRevision found: create one */
        else
        {
            IAssetRevision asset = new AssetRevisionH();
            {
                asset.setProject(project);
                asset.setCreationDate(new Date());
                asset.setKey(key);
                asset.setVariation(variation);
                asset.setRevision(revision);
                asset.setCreator(filerevision.getCreator());
                // setType();
                asset.setName("Asset_" + filerevision.getId());
                asset.setStatus(EAssetStatus.UNLOCKED);
            }

            asset.doPrePersist();
            Persistors.assetrevision.persist(asset);
            asset.doPostPersist();

            // System.out.println("CREATION ASSET: " + "Asset_" + filerevision.getId() + " ( " + asset.getId() + " )");
            return asset;
        }
        return null;
    }

    /**
     * Gets or creates an AssetGroup linked to a given category. Return the AssetGroup object or null if the category id
     * given does not exist
     * 
     * @param parent
     * @param project
     * @return
     */
    public static IAssetRevisionGroup getOrCreateAssetGroupLinkedToParent(final IBase parent, final IProject project)
    {
        final String groupName = MessageFormat.format("Group_{0}_{1}", parent.entityName(), parent.getId());

        try
        {
            IAssetRevisionGroup group = Persistors.assetrevisiongroup.getObjectByValue(Const.NAME, groupName);

            if (group != null)
            {
                /* AssetRevisionGroup found */
                return group;
            }
            else
            {
                /* AssetRevisionGroup not found: create one */
                final AssetRevisionGroupH newgroup = new AssetRevisionGroupH(groupName, null, parent.entityName(),
                        String.valueOf(parent.getId()), project);
                // newgroup.doPrePersist();
                // PersistedObjectProviders.assetrevisiongroup.persistObject(newgroup);
                return newgroup;
            }
        }
        catch (Exception e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            return null;
        }
    }

    private static IPerson getCreator() throws Hd3dPersistenceException
    {
        return AuthenticationUtil.getCurrentUser();
    }

    public static boolean copyFile(File in, File out)
    {
        boolean ret = true;
        try
        {
            FileInputStream fis = new FileInputStream(in);
            FileOutputStream fos = new FileOutputStream(out);
            byte[] buf = new byte[1024];
            int i = 0;
            while ((i = fis.read(buf)) != -1)
            {
                fos.write(buf, 0, i);
            }
            fis.close();
            fos.close();
        }
        catch (Exception e)
        {
            ret = false;
        }
        return ret;
    }

    /**
     * Creates the temporary file, the file revision and copy in the default directory.
     * 
     * @param fileItem
     *            the file item containing the uploaded file
     * @param name
     *            the name of the file.
     * @param variation
     *            the variation of the file revision
     * @param revision
     *            the revision of the file revision
     * @param state
     *            the state of the file revision
     * @return the file revision linked to the file
     */
    public static IFileRevision persistUploadedFile(FileItem fileItem, String name, String variation, int revision,
            EFileState state)
    {
        return persistUploadedFile(null, "", fileItem, name, variation, revision, state, null);
    }

    public static void copyFile(FileItem fileItem, String destDirPath, String filename) throws Hd3dException
    {
        File file = new File(destDirPath + "/" + filename);
        File destDir = new File(destDirPath);
        if (!destDir.exists())
        {
            if (destDir.mkdirs() == false)
            {
                Log.LOGGER.error("*** Error: could not create directory '{}'", destDir.getAbsolutePath());
                throw new Hd3dException("*** Error: could not create directory '" + destDir.getAbsolutePath() + "'");
            }
        }

        try
        {
            fileItem.write(file);
        }
        catch (Exception e)
        {
            Log.LOGGER.error("*** Error: could not create file '{}'", file.getPath());
            throw new Hd3dException("*** Error: could not create file '" + file.getPath() + "'");
        }
    }
}
