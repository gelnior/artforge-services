package fr.hd3d.utils;

/**
 * Simple class for bench purpose
 * 
 * @author Try LAM
 */
public class Bench
{
    long start, end;

    public Bench()
    {
        b();
    }

    /**
     * "start"
     */
    public void b()
    {

        start = System.currentTimeMillis();
    }

    /**
     * "end"
     * 
     * @return duration
     */
    public long e()
    {

        end = System.currentTimeMillis();
        return end - start;
    }
}
