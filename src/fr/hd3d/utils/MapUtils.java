package fr.hd3d.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.MultiMap;
import org.apache.commons.collections15.Transformer;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.utils.transformer.GetIdTransformer;


public final class MapUtils
{
    private MapUtils()
    {}

    /**
     * Given 2 List of SAME SIZE, return a Map with first list as KEYS and second list as VALUES
     * 
     * @param keys
     * @param values
     * @return
     * @throws Hd3dException
     */
    @SuppressWarnings("unchecked")
    public static <M extends Map> Map<?, ?> toMap(final List<?> keys, final List<?> values, final Class<M> mapType)
            throws Hd3dException
    {
        Map map = null;

        /* KEYS cannot be null */
        if (CollectUtils.isNotEmpty(keys))
        {
            map = getMap(mapType);
            if (map != null)
            {
                for (int i = 0; i < keys.size(); i++)
                {
                    map.put(keys.get(i), values.get(i));
                }
            }
        }

        return map;
    }

    private static <K, V, M extends Map<K, V>> Map<?, ?> getMap(final Class<M> mapType)
    {
        Map<K, V> map = null;

        if (mapType != null)
        {
            try
            {
                map = mapType.newInstance();
            }
            catch (InstantiationException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
            catch (IllegalAccessException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
        }

        return map;
    }

    public static <T extends Persistent> Map<Long, T> toIdKeyMap(final List<T> objects)
    {
        return toMap(objects, new GetIdTransformer<T>(), HashMap.class);
    }

    public static <T extends Persistent> Map<Long, T> toOrderedIdKeyMap(final List<T> objects)
    {
        return toMap(objects, new GetIdTransformer<T>(), LinkedHashMap.class);
    }

    public static <X, T extends Persistent, M extends Map> Map<X, T> toMap(final List<T> objects,
            final Transformer<T, X> transformer, final Class<M> mapType)
    {
        Map<X, T> ret = null;

        if (CollectUtils.isNotEmpty(objects))
        {
            ret = getMap(mapType);
            if (ret != null && transformer != null)
            {
                for (T object : objects)
                {
                    ret.put(transformer.transform(object), object);
                }
            }
        }

        return ret;
    }

    /**
     * Given a list of Entities, for each of them, transform it with (chained) transformer, the transformation output is
     * used as the key of the map, and the value is the entity.
     * 
     * @param <X>
     * @param <T>
     * @param objectsToTransform
     * @param transformerToKey
     * @param map
     */
    public static <X, T extends Persistent> void addToMap(final Map<X, T> map, final List<T> objectsToTransform,
            final Transformer<T, X> transformerToKey)
    {
        if (CollectUtils.isNotEmpty(objectsToTransform) && map != null && transformerToKey != null)
        {
            for (T object : objectsToTransform)
            {
                map.put(transformerToKey.transform(object), object);
            }
        }
    }

    public static <X, T extends Persistent> void addToMap(final MultiMap<X, T> map, final List<T> objectsToTransform,
            final Transformer<T, X> transformerToKey)
    {
        if (CollectUtils.isNotEmpty(objectsToTransform) && map != null && transformerToKey != null)
        {
            for (T object : objectsToTransform)
            {
                map.put(transformerToKey.transform(object), object);
            }
        }
    }

}
