package fr.hd3d.utils;

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.ObjectUtils;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.lightweight.ILDynMetaDataValue;
import fr.hd3d.model.lightweight.ILEntityTaskLink;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.model.translator.impl.BaseTranslator;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.utils.AuthenticationUtil;


/**
 * @author Try LAM
 */
public final class TranslatorUtil<L extends ILBase, P extends IBase>
{
    BaseTranslator<L, P> translator;
    P object;
    L lObject;
    String objectField;
    String lObjectField;
    boolean acceptNull = false;

    public TranslatorUtil(BaseTranslator<L, P> t)
    {
        this.translator = t;
    }

    /**
     * update the common fields
     * 
     * @throws Hd3dPersistenceException
     */
    public static void updateCommonFields(final IBase object, final ILBase light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        /* Note: DO NOT ALLOW ID UPDATE AND VERSION UPDATE */
        if (object != null && light != null)
        {
            /* Do NOR test for empty list (delete all tags) */
            object.mergeTags(light.getTags());

            Map<String, String> map = context.getQueryMap();

            if (map != null)
            {
                /* DynMetaData requested */
                if (Const.TRUE.equals(map.get(Const.DYN)))
                {
                    merge(object, object.getDynMetaDataValues(), light.getDynMetaDataValues(), IDynMetaDataValue.class);
                }

                /* ApprovalNotes requested */
                if (Const.TRUE.equals(map.get(Const.APPROVALS)))
                {
                    merge(object, object.getApprovalNotes(), light.getApprovalNotes(), IApprovalNote.class);
                }

                /* EntityTaskLinks requested */
                if (Const.TRUE.equals(map.get(Const.ENTITYTASKLINKS)))
                {
                    merge(object, object.getEntityTaskLinks(), light.getEntityTaskLinks(), IEntityTaskLink.class);
                }
            }
        }
    }

    private static <L extends ILBase, T extends IBase> void merge(final IBase object, final Collection<T> entities,
            final Collection<L> lights, final Class<T> clazz) throws Hd3dException
    {
        if (CollectUtils.isNotEmpty(lights))
        {
            object.merge(entities, CollectUtils.getLightIds(lights), clazz);
        }
    }

    public static void toLightWeightCommonFields(final IBase object, final ILBase light, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object != null && light != null)
        {
            if (syncMode(context.getQueryMap()))
            {
                light.setInternalUUID(object.getInternalUUID());
            }
            /* can update / can delete */
            light.setUserCanUpdate(object.canUpdate());
            light.setUserCanDelete(object.canDelete());

            /* default path */
            final String path = object.defaultPath();
            if (path != null)
            {
                light.setDefaultPath(path.substring(1, path.length()));
            }
        }
    }

    public static void fromLightWeightCommonFields(final IBase object, final ILBase light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        if (object != null && light != null)
        {
            Map<String, String> map = context == null ? null : context.getQueryMap();

            /* version, uuid */
            if (syncMode(map))
            {
                object.setVersion(light.getVersion());
                object.setInternalUUID(light.getInternalUUID());
            }

            /* DynMetaDataValues */
            final Collection<ILDynMetaDataValue> lDynMetaDataValues = light.getDynMetaDataValues();
            List<IDynMetaDataValue> dynMetaDataValues = Translators.dynmetadatavalue.fromLightweightCollection(
                    lDynMetaDataValues, context);
            for (IDynMetaDataValue dynValue : dynMetaDataValues)
            {
                dynValue.doPrePersist();
            }
            Persistors.dynmetadatavalue.persistCollection(dynMetaDataValues);
            for (IDynMetaDataValue dynValue : dynMetaDataValues)
            {
                dynValue.doPostPersist();
            }

            /* ApprovalNotes */
            final Set<ILApprovalNote> approvalNotes = light.getApprovalNotes();
            if (CollectUtils.isNotEmpty(approvalNotes))
            {
                object.setApprovalNotes(Translators.approvalnote.fromLightweightCollection(approvalNotes, context));
            }

            /* EntityTasklinks */
            final Set<ILEntityTaskLink> lEntityTaskLinks = light.getEntityTaskLinks();
            if (CollectUtils.isNotEmpty(lEntityTaskLinks))
            {
                object.setEntityTaskLinks(Translators.entitytasklink.fromLightweightCollection(lEntityTaskLinks,
                        context));
            }
        }
    }

    public static boolean syncMode(final Map<String, String> map)
    {
        return AuthenticationUtil.isCurrentSuperUser() && map != null && Const.TRUE.equals(map.get(Const.SYNC));
    }

    public TranslatorUtil<L, P> withObject(P anObject)
    {
        this.object = anObject;
        this.lObject = null;
        return this;
    }

    public TranslatorUtil<L, P> withLightObject(L anLObject)
    {
        this.lObject = anLObject;
        return this;
    }

    public TranslatorUtil<L, P> acceptNull()
    {
        this.acceptNull = true;
        return this;
    }

    public TranslatorUtil<L, P> dontAcceptNull()
    {
        this.acceptNull = false;
        return this;
    }

    public TranslatorUtil<L, P> fromField(String field) throws Hd3dException
    {
        this.lObjectField = field;
        return this;
    }

    public TranslatorUtil<L, P> updateField(String field) throws Hd3dException
    {
        this.objectField = field;
        return this;
    }

    public void update() throws Hd3dException
    {
        if (translator != null && object != null && lObject != null && objectField != null)
        {
            Class<?> objectFieldClass = EntitiesMaps.withClass(object.getClass()).getTypeOf(objectField);
            if (objectFieldClass != null)
            {
                if (IBase.class.isAssignableFrom(objectFieldClass))
                {
                    updateFieldEntity();
                }
                else if (objectFieldClass.isEnum())
                {
                    updateFieldEnum();
                }
                else if (Collection.class.isAssignableFrom(objectFieldClass))
                {
                    updateFieldCollection();
                }
                else
                {
                    updateSimpleField();
                }
                this.lObjectField = null;
                this.acceptNull = false;
            }
        }
    }

    private void updateSimpleField() throws Hd3dException
    {
        if (translator != null && object != null && lObject != null && objectField != null)
        {
            try
            {
                Object objectProperty = PropertyUtils.getProperty(object, objectField);

                Object lObjectProperty = lObjectField == null ? PropertyUtils.getProperty(lObject, objectField)
                        : PropertyUtils.getProperty(lObject, lObjectField);

                if (!acceptNull && lObjectProperty == null)
                {
                    return;
                }

                if (!ObjectUtils.equals(objectProperty, lObjectProperty))
                {
                    PropertyUtils.setProperty(object, objectField, lObjectProperty);
                }
            }
            catch (IllegalAccessException e)
            {
                throw new Hd3dException(e);
            }
            catch (InvocationTargetException e)
            {
                throw new Hd3dException(e);
            }
            catch (NoSuchMethodException e)
            {
                throw new Hd3dException(e);
            }
        }
    }

    private TranslatorUtil<L, P> updateFieldEntity() throws Hd3dException
    {
        if (translator != null && object != null && lObject != null && objectField != null)
        {
            try
            {
                IBase objectProperty = (IBase) PropertyUtils.getProperty(object, objectField);

                Long lObjectProperty = (Long) (lObjectField == null ? PropertyUtils.getProperty(lObject, objectField)
                        : PropertyUtils.getProperty(lObject, lObjectField));

                /* TODO: -1L is a special case (cf.Composition) */
                if ((!acceptNull && lObjectProperty == null) || (lObjectProperty != null && lObjectProperty == -1L))
                {
                    return this;
                }

                if (!ObjectUtils.equals(CollectUtils.nullSafeId(objectProperty), lObjectProperty))
                {
                    if (acceptNull && lObjectProperty == null)
                    {
                        PropertyUtils.setProperty(object, objectField, null);
                    }
                    else
                    {
                        Class<?> objectFieldClass = EntitiesMaps.withClass(object.getClass()).getTypeOf(objectField);
                        IPersist<?> persistor = Persistors.getPersistor(objectFieldClass.getCanonicalName());

                        if (persistor != null)
                        {
                            IBase entityFromLight = persistor.getById(lObjectProperty);
                            if (entityFromLight == null)
                            {
                                throw new Hd3dTranslationException(MessageFormat.format(BaseTranslator.ERRMSG_0,
                                        objectProperty.entityName(), lObjectProperty));
                            }
                            PropertyUtils.setProperty(object, objectField, entityFromLight);
                        }
                    }
                }
            }
            catch (IllegalAccessException e)
            {
                throw new Hd3dException(e);
            }
            catch (InvocationTargetException e)
            {
                throw new Hd3dException(e);
            }
            catch (NoSuchMethodException e)
            {
                throw new Hd3dException(e);
            }
        }
        return this;
    }

    private <T extends IBase> TranslatorUtil<L, P> updateFieldCollection() throws Hd3dException
    {
        if (translator != null && object != null && lObject != null && objectField != null)
        {
            List<Class<?>> clazzes = (List<Class<?>>) EntitiesMaps.withClass(object.getClass())
                    .getCollectionParameterTypeOf(objectField);

            if (CollectUtils.isNotEmpty(clazzes) && IBase.class.isAssignableFrom(clazzes.get(0)))
            {
                try
                {
                    Collection<T> objectProperty = (Collection<T>) PropertyUtils.getProperty(object, objectField);

                    Collection<Long> lObjectProperty = (Collection<Long>) (lObjectField == null ? PropertyUtils
                            .getProperty(lObject, objectField) : PropertyUtils.getProperty(lObject, lObjectField));

                    if (!acceptNull && lObjectProperty == null)
                    {
                        return this;
                    }
                    if (CollectUtils.isEmpty(lObjectProperty) && CollectUtils.isEmpty(objectProperty))
                    {
                        return this;
                    }

                    object.merge(objectProperty, lObjectProperty, (Class<T>) clazzes.get(0));

                }
                catch (IllegalAccessException e)
                {
                    throw new Hd3dException(e);
                }
                catch (InvocationTargetException e)
                {
                    throw new Hd3dException(e);
                }
                catch (NoSuchMethodException e)
                {
                    throw new Hd3dException(e);
                }
            }
        }
        return this;
    }

    private TranslatorUtil<L, P> updateFieldEnum() throws Hd3dException
    {
        if (translator != null && object != null && lObject != null && objectField != null)
        {
            try
            {
                Class<? extends Enum> objectFieldClass = (Class<? extends Enum>) EntitiesMaps.withClass(
                        object.getClass()).getTypeOf(objectField);

                Enum<?> objectProperty = (Enum<?>) PropertyUtils.getProperty(object, objectField);

                String lObjectProperty = (String) (lObjectField == null ? PropertyUtils.getProperty(lObject,
                        objectField) : PropertyUtils.getProperty(lObject, lObjectField));

                if (lObjectProperty != null && !lObjectProperty.equalsIgnoreCase("undefined"))
                {
                    Enum<?> enumFromLightWeight = objectFieldClass == null ? null : EnumUtils.getEnumValueIgnoreCase(
                            objectFieldClass, lObjectProperty);

                    if (!acceptNull && lObjectProperty == null)
                    {
                        return this;
                    }

                    if (objectProperty != enumFromLightWeight)
                    {
                        PropertyUtils.setProperty(object, objectField, enumFromLightWeight);
                    }
                }
            }
            catch (IllegalAccessException e)
            {
                throw new Hd3dException(e);
            }
            catch (InvocationTargetException e)
            {
                throw new Hd3dException(e);
            }
            catch (NoSuchMethodException e)
            {
                throw new Hd3dException(e);
            }
        }
        return this;
    }
}
