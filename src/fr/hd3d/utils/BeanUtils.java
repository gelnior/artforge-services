package fr.hd3d.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaProperty;


public final class BeanUtils
{
    private BeanUtils()
    {}

    /**
     * given an object with only properties (not getter/setter), turn it into a java bean. attributesNames and objects
     * arrays must have the same length Note: unused for the moment but do not remove yet
     * 
     * 
     * @param name
     *            Name of the DynaClass to be instantiated, which the DynaBean is based on
     * @param attributesNames
     * @param types
     * @param attributesValues
     * @return
     */
    public static DynaBean toBean(String name, String[] attributesNames, Class<?>[] types, Object[] attributesValues)
    {
        // properties
        List<DynaProperty> props = new ArrayList<DynaProperty>();

        for (int i = 0; i < attributesNames.length; i++)
        {
            props.add(new DynaProperty(attributesNames[i], types[i]));
        }
        //

        DynaProperty[] propsArray = new DynaProperty[props.size()];
        BasicDynaClass dynaClass = new BasicDynaClass(name, null, props.toArray(propsArray));
        try
        {
            DynaBean bean = dynaClass.newInstance();
            DynaProperty[] dynaProps = dynaClass.getDynaProperties();
            for (int i = 0; i < dynaProps.length; i++)
            {
                bean.set(dynaProps[i].getName(), attributesValues[i]);
            }
            // test
            // Map m = ((BasicDynaBean) bean).getMap();
            //
            return bean;
        }
        catch (IllegalAccessException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
        catch (InstantiationException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
        return null;
    }

    public static DynaBean toBean(BasicDynaClass dynaClass, String[] attributesNames, Object[] attributesValues)
    {
        try
        {
            DynaBean bean = dynaClass.newInstance();
            DynaProperty[] dynaProps = dynaClass.getDynaProperties();
            for (int i = 0; i < dynaProps.length; i++)
            {
                bean.set(dynaProps[i].getName(), attributesValues[i]);
            }
            // test
            // Map m = ((BasicDynaBean) bean).getMap();
            //
            return bean;
        }
        catch (IllegalAccessException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
        catch (InstantiationException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
        return null;
    }

    /**
     * create DynaBean with SINGLE property Note: unused for the moment but do not remove yet
     * 
     * @param attributeName
     * @param type
     * @param attributeValue
     * @return
     */
    public static DynaBean toBean(String name, String attributeName, Class<?> type, Object attributeValue)
    {
        DynaProperty[] props = new DynaProperty[] { new DynaProperty(attributeName, type) };
        BasicDynaClass dynaClass = new BasicDynaClass(name, null, props);
        try
        {
            DynaBean bean = dynaClass.newInstance();
            bean.set(attributeName, attributeValue);
            return bean;
        }
        catch (IllegalAccessException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
        catch (InstantiationException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }

        return null;
    }

    public static Map<String, Object> toMap(String[] attributesNames, Object[] values)
    {
        Map<String, Object> m = new org.apache.commons.collections15.map.ListOrderedMap<String, Object>();
        for (int i = 0; i < attributesNames.length; i++)
        {
            m.put(attributesNames[i], values[i]);
        }
        return m;
    }

    public static Map<String, Object> toMap(String attributeName, Object value)
    {
        Map<String, Object> m = new org.apache.commons.collections15.map.ListOrderedMap<String, Object>();
        m.put(attributeName, value);
        return m;
    }
}
