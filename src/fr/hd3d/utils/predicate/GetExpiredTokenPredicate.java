package fr.hd3d.utils.predicate;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections15.Predicate;

import fr.hd3d.services.security.customs.IAuthcToken;
import fr.hd3d.services.security.utils.AuthenticationUtil;


public class GetExpiredTokenPredicate implements Predicate<Map.Entry<String, IAuthcToken>>
{
    public boolean evaluate(Entry<String, IAuthcToken> entry)
    {
        return entry != null && entry.getValue() != null
                && (entry.getValue().getDate() + AuthenticationUtil.TIME_TO_LIVE - new Date().getTime()) < 0;
    }
}
