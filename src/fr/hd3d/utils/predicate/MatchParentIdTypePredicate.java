package fr.hd3d.utils.predicate;

import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.ObjectUtils;

import fr.hd3d.model.persistence.impl.hibernate.TagParent;


public class MatchParentIdTypePredicate implements Predicate<TagParent>
{
    Long parentId;
    String parentType;

    public MatchParentIdTypePredicate(Long id, String type)
    {
        this.parentId = id;
        this.parentType = type;
    }

    public boolean evaluate(TagParent tagparent)
    {
        return tagparent != null && tagparent.getParent() != null
                && ObjectUtils.equals(parentId, tagparent.getParent())
                && ObjectUtils.equals(parentType, tagparent.getParentType());
    }
}
