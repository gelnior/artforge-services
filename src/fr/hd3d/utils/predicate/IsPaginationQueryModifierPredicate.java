package fr.hd3d.utils.predicate;

import org.apache.commons.collections15.Predicate;

import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.FirstResultQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.MaxResultQueryModifier;


public class IsPaginationQueryModifierPredicate<T extends IQueryModifier> implements Predicate<T>
{
    public boolean evaluate(T object)
    {
        return object != null
                && (object instanceof FirstResultQueryModifier || object instanceof MaxResultQueryModifier);
    }
}
