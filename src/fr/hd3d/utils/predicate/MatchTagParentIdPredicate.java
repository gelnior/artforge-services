package fr.hd3d.utils.predicate;

import org.apache.commons.collections15.Predicate;

import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;


public class MatchTagParentIdPredicate implements Predicate<TagParent>
{
    Long id;

    public MatchTagParentIdPredicate(Long id)
    {
        if (id == null)
        {
            throw new Hd3dRuntimeException("MatchTagParentIdPredicate: id cannot be null");
        }
        this.id = id;
    }

    public boolean evaluate(TagParent object)
    {
        return object != null && object.getTag() != null && object.getTag().getId().equals(id);
    }
}
