package fr.hd3d.utils.predicate;

import org.apache.commons.collections15.Predicate;

import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;


public class MatchClassDynMetaDataTypePredicate implements Predicate<IDynMetaDataValue>
{
    IClassDynMetaDataType type;

    public MatchClassDynMetaDataTypePredicate(IClassDynMetaDataType type)
    {
        this.type = type;
    }

    public boolean evaluate(IDynMetaDataValue dynMetaDataValue)
    {
        /* method BaseH.classDynMetaDataTypes() never returns null */
        return dynMetaDataValue != null && dynMetaDataValue.classDynMetaDataTypes().contains(type);
    }
}
