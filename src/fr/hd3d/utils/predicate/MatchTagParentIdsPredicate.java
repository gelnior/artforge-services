package fr.hd3d.utils.predicate;

import java.util.Collection;

import org.apache.commons.collections15.Predicate;

import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;


public class MatchTagParentIdsPredicate implements Predicate<TagParent>
{
    Collection<Long> ids;

    public MatchTagParentIdsPredicate(Collection<Long> ids)
    {
        if (ids == null)
        {
            throw new Hd3dRuntimeException("MatchTagParentIdsPredicate: ids cannot be null");
        }
        this.ids = ids;
    }

    public boolean evaluate(TagParent object)
    {
        return object != null && object.getTag() != null && ids.contains(object.getTag().getId());
    }
}
