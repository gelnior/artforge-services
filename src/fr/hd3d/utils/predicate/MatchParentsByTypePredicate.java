package fr.hd3d.utils.predicate;

import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.ObjectUtils;

import fr.hd3d.model.persistence.impl.hibernate.TagParent;


public class MatchParentsByTypePredicate implements Predicate<TagParent>
{
    String type;

    public MatchParentsByTypePredicate(String type)
    {
        this.type = type;
    }

    public boolean evaluate(TagParent tagparent)
    {
        return tagparent != null && ObjectUtils.equals(type, tagparent.getParentType());
    }
}
