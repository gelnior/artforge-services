package fr.hd3d.utils.predicate;

import org.apache.commons.collections15.Predicate;

import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.persistence.IEntityTaskLink;


public class MatchEntityTaskLinkIdPredicate implements Predicate<IEntityTaskLink>
{
    Long id;

    public MatchEntityTaskLinkIdPredicate(Long id)
    {
        if (id == null)
        {
            throw new Hd3dRuntimeException("MatchEntityTaskLinkIdPredicate: id cannot be null");
        }
        this.id = id;
    }

    public boolean evaluate(IEntityTaskLink object)
    {
        return object != null && object.getBoundEntity() != null && object.getBoundEntity().equals(id);
    }
}
