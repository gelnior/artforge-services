package fr.hd3d.utils.predicate;

import org.apache.commons.collections15.Predicate;

import fr.hd3d.exception.Hd3dRuntimeException;


public class MatchClassPredicate implements Predicate<Object>
{
    Class<?> clazz;

    public MatchClassPredicate(Class<?> clazz)
    {
        if (clazz == null)
        {
            throw new Hd3dRuntimeException("MatchClassPredicate: class parameter cannot be null");
        }
        this.clazz = clazz;
    }

    public boolean evaluate(Object object)
    {
        return object != null && clazz.isAssignableFrom(object.getClass());
    }
}
