package fr.hd3d.utils.predicate;

import org.apache.commons.collections15.Predicate;

import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.persistence.IDynMetaDataValue;


public class MatchDynMetaDataValueIdPredicate implements Predicate<IDynMetaDataValue>
{
    Long id;

    public MatchDynMetaDataValueIdPredicate(Long id)
    {
        if (id == null)
        {
            throw new Hd3dRuntimeException("MatchDynMetaDataValuesIdPredicate: id cannot be null");
        }
        this.id = id;
    }

    public boolean evaluate(IDynMetaDataValue object)
    {
        return object != null && object.getParent() != null && object.getParent().equals(id);
    }
}
