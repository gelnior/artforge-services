package fr.hd3d.utils.predicate;

import org.apache.commons.collections15.Predicate;

import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.persistence.IApprovalNote;


public class MatchApprovalNoteIdPredicate implements Predicate<IApprovalNote>
{
    Long id;

    public MatchApprovalNoteIdPredicate(Long id)
    {
        if (id == null)
        {
            throw new Hd3dRuntimeException("MatchApprovalNoteIdPredicate: id cannot be null");
        }
        this.id = id;
    }

    public boolean evaluate(IApprovalNote object)
    {
        return object != null && object.getBoundEntity() != null && object.getBoundEntity().equals(id);
    }
}
