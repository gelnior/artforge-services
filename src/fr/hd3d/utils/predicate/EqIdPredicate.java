package fr.hd3d.utils.predicate;

import org.apache.commons.collections15.Predicate;

import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.persistence.IBase;


public class EqIdPredicate<T extends IBase> implements Predicate<T>
{
    Long id;

    public EqIdPredicate(Long id)
    {
        if (id == null)
        {
            throw new Hd3dRuntimeException("MatchIdPredicate: id cannot be null");
        }
        this.id = id;
    }

    public boolean evaluate(T object)
    {
        return object != null && object.getId().equals(id);
    }
}
