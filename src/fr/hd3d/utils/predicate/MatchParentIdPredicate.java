package fr.hd3d.utils.predicate;

import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.ObjectUtils;

import fr.hd3d.model.persistence.impl.hibernate.TagParent;


public class MatchParentIdPredicate implements Predicate<TagParent>
{
    Long id;

    public MatchParentIdPredicate(Long id)
    {
        this.id = id;
    }

    public boolean evaluate(TagParent tagparent)
    {
        return tagparent != null && ObjectUtils.equals(id, tagparent.getParent());
    }
}
