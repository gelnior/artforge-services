package fr.hd3d.utils.predicate;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections15.Predicate;

import fr.hd3d.services.security.customs.IAuthcToken;


public class GetPrincipalPredicate implements Predicate<Map.Entry<String, IAuthcToken>>
{
    String principal;

    public GetPrincipalPredicate(String principal)
    {
        this.principal = principal;
    }

    public boolean evaluate(Entry<String, IAuthcToken> entry)
    {
        return entry != null && entry.getValue() != null && entry.getValue().getPrincipal().equals(principal);
    }
}
