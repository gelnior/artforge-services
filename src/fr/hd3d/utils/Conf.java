/**
 * 
 */
package fr.hd3d.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.math.NumberUtils;

import fr.hd3d.exception.Hd3dRuntimeException;


/**
 * @author Thomas ESKENAZI
 */
public final class Conf
{
    public final static String CONF_FILE = "services.properties";
    private final static String BENCH = "hd3d.server.bench";
    private final static String MIGRATION = "hd3d.server.migration";
    private final static String PRODUCTION_STATE = "hd3d.server.production";
    private final static String AUTH_COOKIE = "hd3d.auth.cookie";
    private final static String AUTH_ENABLE = "hd3d.auth.enable";
    private final static String GZIP = "hd3d.gzip";
    private final static String MAXRECORDS = "hd3d.maxrecords";
    private final static String SCRIPT_TIMEOUT = "hd3d.script.timeout";
    private final static String SCRIPT_LOCATION = "hd3d.script.location";
    private final static String SCRIPT_ENGINE = "hd3d.script.engine";
    private final static String UPLOAD_DIR = "hd3d.upload.dir";
    private final static String TMP_DIR = "hd3d.tmp.dir";
    private final static String ALLOWED_ROOTPATHS = "hd3d.filesystem.rootpaths";
    private final static String CREATE_TASK_FOR_STEP = "hd3d.step.createtask";
    private final static String CREATE_ASSET_FOR_STEP = "hd3d.step.createasset";
    private final static String ICES_PATH = "hd3d.ices.path";
    private final static String ICES_UPLOAD_DIR = "hd3d.ices.upload.dir";
    private final static String ICES_OUTPUT_DIR = "hd3d.ices.output.dir";
    private final static String AUDIT_PURGE_SEPARATOR = "hd3d.audit.purge.separator";
    private final static String AUDIT_PURGE_DIR = "hd3d.audit.purge.dir";
    private final static String AUDIT_PURGE_PERIOD = "hd3d.audit.purge.period";
    private final static String UPDATESSTACK_ON = "hd3d.updatesstack.on";
    private final static String UPDATESSTACK_PURGE_DIR = "hd3d.updatesstack.purge.dir";
    private final static String UPDATESSTACK_PURGE_PERIOD = "hd3d.updatesstack.purge.period";
    private final static String ANNOTATION_ON = "annotation.on";
    private final static String GRAPHICAL_ANNOTATION_ON = "hd3d.graphicalannotation.on";
    private final static String BASEURI = "hd3d.baseuri";
    private final static String ANNOTATION_SERVER_URI = "annotation.server.uri";

    private final static String IMAGES_PATH = "hd3d.images.path";
    private final static String RLM_SERVER = "hd3d.rlm.server";
    private final static String RLM_LICENSE_PATH = "hd3d.rlm.license.path";

    private final static String FILE_TREE_DIR = "hd3d.asset.dir";
    //
    private final static String VERSIONFILE = "version";
    private final static String VERSION = "version";

    public final static Properties PROPS = new Properties();
    public final static Properties VERSION_FILE = new Properties();

    static
    {
        init(PROPS, CONF_FILE);
        init(VERSION_FILE, VERSIONFILE);
    }

    private Conf()
    {}

    public static void init(final Properties props, final String fileName)
    {
        InputStream stream = readProperties(fileName);
        if (stream == null)
        {
            stream = readProperties("/" + fileName);
        }
        if (stream == null)
        {
            throw new Hd3dRuntimeException(fileName + " not found");
        }

        try
        {
            props.load(stream);
        }
        catch (IOException exception)
        {
            Log.LOGGER.error(ExceptUtils.format(exception));
        }
        finally
        {
            try
            {
                if (stream != null)
                {
                    stream.close();
                }
            }
            catch (IOException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
        }
    }

    public static InputStream readProperties(final String path)
    {
        // String stripped = path.startsWith("/") ? path.substring(1) : path;
        InputStream stream = null;
        final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if (classLoader != null)
        {
            stream = classLoader.getResourceAsStream(path);
        }
        if (stream == null)
        {
            stream = Conf.class.getResourceAsStream(path);
        }
        if (stream == null)
        {
            stream = Conf.class.getClassLoader().getResourceAsStream(path);
        }
        return stream;
    }

    public static boolean isInProductionState()
    {
        return BooleanUtils.toBoolean(StringUtils.trim(getProperty(PROPS, PRODUCTION_STATE)));
    }

    public static boolean useBench()
    {
        return BooleanUtils.toBoolean(StringUtils.trim(getProperty(PROPS, BENCH)));
    }

    public static boolean isMigration()
    {
        return BooleanUtils.toBoolean(StringUtils.trim(getProperty(PROPS, MIGRATION)));
    }

    public static boolean isAuthEnable()
    {
        return BooleanUtils.toBoolean(StringUtils.trim(getProperty(PROPS, AUTH_ENABLE)));
    }

    public static String getAuthCookie()
    {
        return getProperty(PROPS, AUTH_COOKIE);
    }

    public static boolean compressResponse()
    {
        return BooleanUtils.toBoolean(StringUtils.trim(getProperty(PROPS, GZIP)));
    }

    public static int getMaxRecords()
    {
        int maxRecord = NumberUtils.toInt(getProperty(PROPS, MAXRECORDS));
        return maxRecord == 0 ? 300 : maxRecord;
    }

    public static int getScriptTimeOut()
    {
        return NumberUtils.toInt(getProperty(PROPS, SCRIPT_TIMEOUT));
    }

    public static String getScriptLocation()
    {
        return getProperty(PROPS, SCRIPT_LOCATION);
    }

    public static String getScriptEngine()
    {
        return getProperty(PROPS, SCRIPT_ENGINE);
    }

    public static String getICESPath()
    {
        return getProperty(PROPS, ICES_PATH);
    }

    public static String getICESUploadDir()
    {
        return getProperty(PROPS, ICES_UPLOAD_DIR);
    }

    public static String getICESOutputDir()
    {
        return getProperty(PROPS, ICES_OUTPUT_DIR);
    }

    public static String getUploadDir()
    {
        return getProperty(PROPS, UPLOAD_DIR);
    }

    public static String getAuditPurgeDir()
    {
        return getProperty(PROPS, AUDIT_PURGE_DIR);
    }

    public static boolean isUpdatesStackOn()
    {
        return BooleanUtils.toBoolean(StringUtils.trim(getProperty(PROPS, UPDATESSTACK_ON)));
    }

    public static String getUpdatesStackPurgeDir()
    {
        return getProperty(PROPS, UPDATESSTACK_PURGE_DIR);
    }

    public static String getAuditPurgeSeparator()
    {
        return getProperty(PROPS, AUDIT_PURGE_SEPARATOR);
    }

    public static String getAuditPurgePeriod()
    {
        return getProperty(PROPS, AUDIT_PURGE_PERIOD);
    }

    public static String getUpdatesStackPurgePeriod()
    {
        return getProperty(PROPS, UPDATESSTACK_PURGE_PERIOD);
    }

    public static boolean isAnnotationOn()
    {
        return BooleanUtils.toBoolean(StringUtils.trim(getProperty(PROPS, ANNOTATION_ON)));
    }

    public static boolean isGraphicalAnnotationOn()
    {
        return BooleanUtils.toBoolean(StringUtils.trim(getProperty(PROPS, GRAPHICAL_ANNOTATION_ON)));
    }

    public static String getBaseUri()
    {
        return getProperty(PROPS, BASEURI);
    }

    public static String getAnnotationServerUri()
    {
        return getProperty(PROPS, ANNOTATION_SERVER_URI);
    }

    public static String getVersion()
    {
        return getProperty(VERSION_FILE, VERSION);
    }

    public static String getRLMServer()
    {
        return getProperty(PROPS, RLM_SERVER);
    }

    public static String getRLMLicensePath()
    {
        return getProperty(PROPS, RLM_LICENSE_PATH);
    }

    public static boolean createAssetForStep()
    {
        return BooleanUtils.toBoolean(StringUtils.trim(getProperty(PROPS, CREATE_ASSET_FOR_STEP)));
    }

    public static boolean createTaskForStep()
    {
        return BooleanUtils.toBoolean(StringUtils.trim(getProperty(PROPS, CREATE_TASK_FOR_STEP)));
    }

    public static String getProperty(final Properties props, final String propertyName)
    {
        if (props == null)
        {
            throw new RuntimeException("No server properties found!");
        }
        final String result = props.getProperty(propertyName);
        if (result == null)
        {
            throw new Hd3dRuntimeException("No " + propertyName + " property found!");
        }
        return result;
    }

    public static List<String> getPropertyValues(final String propName)
    {
        if (PROPS == null)
        {
            throw new RuntimeException(propName + " property not found!");
        }
        final String result = PROPS.getProperty(propName);
        if (result == null)
        {
            throw new Hd3dRuntimeException("No " + propName + " property found!");
        }
        return Arrays.asList(StringUtils.split(result, ','));

    }

    public static String getTmpDir()
    {
        return getProperty(PROPS, TMP_DIR);
    }

    public static String getAllowedRootPaths()
    {
        return getProperty(PROPS, ALLOWED_ROOTPATHS);
    }

    public static String getImagePath()
    {
        String path = getProperty(PROPS, IMAGES_PATH);
        if (!path.endsWith(SystemUtils.FILE_SEPARATOR))
        {
            path += SystemUtils.FILE_SEPARATOR;
        }

        return path;
    }

    public static String getFileTreeDir()
    {
        return getProperty(PROPS, FILE_TREE_DIR);
    }

}
