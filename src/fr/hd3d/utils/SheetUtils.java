package fr.hd3d.utils;

import java.util.Collection;

import org.apache.commons.lang.ArrayUtils;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ISheet;


public final class SheetUtils
{
    private SheetUtils()
    {}

    public static Pair<IItem, Integer> getColumnItemAndItemIndex(final Object[][][] values, final Long itemId)
            throws Hd3dPersistenceException
    {
        Pair<IItem, Integer> ret = null;

        if (itemId != null && ArrayUtils.isNotEmpty(values) && values[0] != null)
        {
            final Object[][] firstRow = values[0];
            IItem item;
            for (int i = 0; i < firstRow.length; i++)
            {
                item = (IItem) firstRow[i][2];

                if (item != null && itemId.equals(item.getId()))
                {
                    ret = new Pair<IItem, Integer>(item, Integer.valueOf(i));
                    break;
                }
            }
            // if (ret == null)
            // {
            // throw new Hd3dPersistenceException("Cannot find item with id=" + itemId);
            // }
        }

        return ret;
    }

    public static int nbColumns(final ISheet sheet)
    {
        if (sheet == null)
            return 0;

        Collection<IItemGroup> groups = CollectUtils.<IItemGroup> selectNotNull(sheet.getItemGroups());

        if (CollectUtils.isEmpty(groups))
            return 0;

        /* compute total number of items */
        int nbColums = 0;
        for (final IItemGroup itemGroup : groups)
        {
            if (CollectUtils.isNotEmpty(itemGroup.getItems()))
            {
                nbColums += CollectUtils.selectNotNull(itemGroup.getItems()).size();
            }
        }
        return nbColums;
    }
}
