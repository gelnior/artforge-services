package fr.hd3d.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.hd3d.common.client.DateFormat;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.services.EntitiesMaps;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public final class StringUtils
{
    /* hook only accepts alphanum and "_" and "-" */
    public static Pattern hookForbiddenPattern = Pattern.compile("[^!\"`@\\s#$%&'()*+,:;<=>?^{}|]*");

    private StringUtils()
    {}

    public static boolean isValid(final String string)
    {
        return string != null && !"".equals(string.trim());
    }

    public static boolean isValidColor(final String color)
    {
        return color != null && (color.matches("#[\\p{Alnum}]{6}") || color.matches("#[\\p{Alnum}]{3}"));
    }

    public static boolean isNotValidColor(final String color)
    {
        return !isValidColor(color);
    }

    /**
     * For numeric values, JSON format is Long, and may cause ClassCastException for Integer property type. So,
     * narrowing it to Integer when appropriate is needed.
     * 
     * @param persisted
     *            the business entity
     * @param object
     *            to narrow
     * @param column
     *            the attribute to narrow the object into
     * @return
     */
    public static Object narrow(final Class<?> persisted, final Object object, final String column)
    {
        Object ret = object;

        if (persisted != null && column != null)
        {
            /*
             * column may be dot noted: attr1.attr2... Must recursively narrow until the last attribute
             */
            /* get the object property's type */
            final Class<?> type = EntitiesMaps.lastAttribbuteClass(persisted, column);

            if (type != null)
            {
                /* if property type is Integer, narrow value(Long) to Integer, because JSON only uses Long, not Integer */
                if (Integer.class.isAssignableFrom(type))
                {
                    ret = ((Long) object).intValue();
                }
                else if (Float.class.isAssignableFrom(type))
                {
                    ret = parseFloat((Double) object);
                }
                else if (Date.class.isAssignableFrom(type))
                {
                    /* sometimes, the JSON deserialize a date as a String */
                    if (String.class.isInstance(object))
                    {
                        ret = parseDate((String) object);
                    }
                    else
                    {
                        ret = (Date) object;
                    }
                }
                /* if the property is a business entity */
                else if (EntitiesMaps.isEntity(type.getCanonicalName()))
                {
                    try
                    {
                        ret = Persistors.getPersistor(type.getCanonicalName()).getById((Long) object);
                    }
                    catch (Hd3dException e)
                    {
                        Log.LOGGER.error(ExceptUtils.format(e));
                    }
                }
            }
        }

        return ret;
    }

    private static Float parseFloat(final Double object)
    {
        return object == null ? null : object.floatValue();
    }

    public static Date parseDate(final String date)
    {
        Date ret = null;

        if (date != null)
        {
            try
            {
                ret = new SimpleDateFormat(DateFormat.DATE_TIME_STRING).parse(date);
            }
            catch (ParseException e)
            {
                Log.LOGGER.error("Error occured when parsing date: {}", e);
            }
        }

        return ret;
    }

    public static String format(Date date)
    {
        return new SimpleDateFormat(DateFormat.DATE_TIME_STRING).format(date);
    }

    public static String formatIso(Date date)
    {
        return new SimpleDateFormat(DateFormat.DATE_TIME_ISO_8607_STRING).format(date);
    }

    public static String nullSafeString(final Object object)
    {
        return object == null ? "null" : object.toString();
    }

    public static String nullSafeIdString(final IBase object)
    {
        return object == null ? "null" : String.valueOf(object.getId());
    }

    public static boolean isProperHook(String hook)
    {
        Matcher m = hookForbiddenPattern.matcher(hook);
        return m.matches();
    }

    public static List<String> asList(String string, char sep)
    {
        return Arrays.asList(org.apache.commons.lang.StringUtils.split(string, sep));
    }
}
