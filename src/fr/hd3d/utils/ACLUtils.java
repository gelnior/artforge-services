package fr.hd3d.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.acl.ACLInfo;
import fr.hd3d.model.persistence.impl.hibernate.acl.ACLInfo.EACL_Permission;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.security.dao.Hd3dACLH;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.services.security.utils.AuthenticationUtil;


public final class ACLUtils
{

    private ACLUtils()
    {}

    public static List<IHd3dACL> getACLs(List<Long> roleIds, String entityName)
    {
        /* Note: cannot use stateless session, does not handle collection */
        Session session = HibernateUtil.currentSession();
        final Criteria crit = session.createCriteria(Hd3dACLH.class, "acl");
        crit.createAlias("acl.roles", "rls");
        crit.add(Restrictions.in("rls.id", roleIds));
        crit.add(Restrictions.eq("entityName", entityName));
        /* No projection: used later to check update and delete */
        // statelessSessionCrit.setProjection(Projections.property("allowedToRead"));
        crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return crit.list();
    }

    // public static Set<String> getPermissionTokens(Collection<IHd3dACL> acls, EACL_Permission operation)
    // {
    // Set<String> permissionTokens = new HashSet<String>();
    // Set<String> permissions = getOperationPermissions(acls, operation);
    // for (String permission : permissions)
    // {
    // permissionTokens.addAll(Arrays.asList(StringUtils.split(permission, '|')));
    // }
    // return permissionTokens;
    // }

    // public static Triplet<Boolean, List<Long>, List<Long>> getReadPermissionIds(String readPermission)
    // {
    // Boolean allPermitted = false;
    // List<Long> notPermittedIds = new ArrayList<Long>();
    // List<Long> permittedIds = new ArrayList<Long>();
    //
    // if ("*".equals(readPermission))
    // {
    // allPermitted = true;
    // }
    // else
    // {
    // final String prefix = StringUtils.substringBefore(readPermission, ":");
    // if (prefix != null)
    // {
    // /* a beginning "-" means not allowed */
    // final boolean isNotAllowed = prefix.startsWith("-");
    // final String method = StringUtils.stripStart(prefix, "-");
    // final String methodValues = StringUtils.substringAfter(readPermission, ":");
    // final Collection<Long> ids;
    // if ("id".equalsIgnoreCase(method))
    // {
    // ids = CollectUtils.toLong(Arrays.asList(StringUtils.split(methodValues, ',')));
    // if (isNotAllowed)
    // {
    // notPermittedIds.addAll(ids);
    // }
    // else
    // {
    // permittedIds.addAll(ids);
    // }
    // }
    // }
    // }
    //
    // return new Triplet<Boolean, List<Long>, List<Long>>(allPermitted, notPermittedIds, permittedIds);
    // }

    // public static Triplet<Boolean, List<Criterion>, List<Criterion>> getReadCriterionListInfo(List<IHd3dACL> acls)
    // throws Hd3dException
    // {
    // Boolean allPermitted = false;
    // List<Criterion> readNotPermittedCriterionList = new ArrayList<Criterion>();
    // List<Criterion> readPermittedCriterionList = new ArrayList<Criterion>();
    //
    // Collection<Long> permittedIds = new ArrayList<Long>();
    // Collection<Long> notPermittedIds = new ArrayList<Long>();
    //
    // Triplet<Boolean, List<Long>, List<Long>> readPermissionIds;
    //
    // /* handle different ways to obtain allowed/not allowed ids */
    // for (String token : getPermissionTokens(acls, "READ"))
    // {
    // readPermissionIds = getReadPermissionIds(token);
    //
    // allPermitted |= readPermissionIds.getField_1();
    // notPermittedIds.addAll(readPermissionIds.getField_2());
    // permittedIds.addAll(readPermissionIds.getField_3());
    // }
    //
    // if (CollectUtils.isNotEmpty(permittedIds))
    // {
    // readPermittedCriterionList.add(Restrictions.in(Const.ID, permittedIds));
    // }
    // else if (CollectUtils.isNotEmpty(notPermittedIds))
    // {
    // readNotPermittedCriterionList.add(Restrictions.not(Restrictions.in(Const.ID, notPermittedIds)));
    // }
    // // else if ("association".equalsIgnoreCase(method))
    // // {
    // // /* TODO */
    // //
    // // }
    // // else if ("hibernatequerycmd".equalsIgnoreCase(method))
    // // {
    // // /* TODO */
    // // String[] methodValuesTokens = StringUtils.split(methodValues, ',');
    // // final String queryCmdName = methodValuesTokens[0];
    // // try
    // // {
    // // Class<?> queryCmdClass = Class.forName(queryCmdName);
    // // ListResultQuery<?, ?> cmd = (ListResultQuery) queryCmdClass.newInstance();
    // // cmd.setPermissionOn(true);
    // // cmd.setIdOnly(true);
    // // for (int i = 1; i < methodValuesTokens.length; i++)
    // // {
    // // try
    // // {
    // // org.apache.commons.beanutils.BeanUtils.setProperty(cmd, StringUtils.substringBefore(
    // // methodValuesTokens[i], "="), (Object) StringUtils.substringAfter(
    // // methodValuesTokens[i], "="));
    // // }
    // // catch (InvocationTargetException e)
    // // {
    // // Log.LOGGER.error(ExceptUtils.format(e));
    // // }
    // // }
    // //
    // // HibernateUtil.safeQuery((IHibernateQuery) cmd);
    // //
    // // if (isNotAllowed)
    // // {
    // // notPermittedIds.addAll(cmd.allIds);
    // // }
    // // else
    // // {
    // // permittedIds.addAll(cmd.allIds);
    // // }
    // //
    // // if (CollectUtils.isNotEmpty(permittedIds))
    // // {
    // // readPermittedCriterionList.add(Restrictions.in(Const.ID, permittedIds));
    // // }
    // // else if (CollectUtils.isNotEmpty(notPermittedIds))
    // // {
    // // readNotPermittedCriterionList.add(Restrictions.not(Restrictions.in(Const.ID,
    // // notPermittedIds)));
    // // }
    // // }
    // // catch (ClassNotFoundException e)
    // // {
    // // Log.LOGGER.error(ExceptUtils.format(e));
    // // }
    // // catch (InstantiationException e)
    // // {
    // // Log.LOGGER.error(ExceptUtils.format(e));
    // // }
    // // catch (IllegalAccessException e)
    // // {
    // // Log.LOGGER.error(ExceptUtils.format(e));
    // // }
    // // }
    // return new Triplet<Boolean, List<Criterion>, List<Criterion>>(allPermitted, readNotPermittedCriterionList,
    // readPermittedCriterionList);
    // }

    public static Set<String> getOperationPermissions(Collection<IHd3dACL> acls, EACL_Permission operation)
    {
        Set<String> permissions = new HashSet<String>();
        for (IHd3dACL acl : acls)
        {
            String permission;
            if (operation == EACL_Permission.READ_PERMITTED)
            {
                permission = acl.getReadPermitted();
            }
            else if (operation == EACL_Permission.READ_BANS)
            {
                permission = acl.getReadBans();
            }
            else if (operation == EACL_Permission.CREATE_PERMITTED)
            {
                permission = acl.getCreatePermitted();
            }
            else if (operation == EACL_Permission.UPDATE_PERMITTED)
            {
                permission = acl.getUpdatePermitted();
            }
            else if (operation == EACL_Permission.UPDATE_BANS)
            {
                permission = acl.getUpdateBans();
            }
            else if (operation == EACL_Permission.DELETE_PERMITTED)
            {
                permission = acl.getDeletePermitted();
            }
            else if (operation == EACL_Permission.DELETE_BANS)
            {
                permission = acl.getDeleteBans();
            }
            else
            {
                continue;
            }
            if (StringUtils.isNotBlank(permission))
            {
                permissions.add(permission);
            }
        }
        return permissions;
    }

    // private static Collection<Long> getPermitted(Set<Long> ids)
    // {
    // return org.apache.commons.collections15.CollectionUtils.select(ids,
    // new org.apache.commons.collections15.Predicate<Long>() {
    //
    // @Override
    // public boolean evaluate(Long id)
    // {
    // return id > 0;
    // }
    // });
    // }
    //
    // private static Collection<Long> getNotPermitted(Set<Long> ids)
    // {
    // return org.apache.commons.collections15.CollectionUtils.collect(
    // org.apache.commons.collections15.CollectionUtils.select(ids,
    // new org.apache.commons.collections15.Predicate<Long>() {
    //
    // @Override
    // public boolean evaluate(Long id)
    // {
    // return id < 0;
    // }
    // }), new Transformer<Long, Long>() {
    // /* reverse the sign */
    // @Override
    // public Long transform(Long id)
    // {
    // return -id;
    // }
    // });
    // }

    public static ACLInfo getACLInfo(final Class<?> clazz, List<Long> roleIds) throws Hd3dException
    {
        return getACLInfo(EntitiesMaps.withClass(clazz).getEntityName(), roleIds);
    }

    public static ACLInfo getACLInfo(final String entityName, List<Long> roleIds) throws Hd3dException
    {
        final List<Long> ids;
        if (CollectUtils.isEmpty(roleIds))
        {
            ids = CollectUtils.getIds(AuthenticationUtil.getCurrentUserRoles());
        }
        else
        {
            ids = roleIds;
        }

        if (CollectUtils.isEmpty(ids))
            return new ACLInfo();

        return new ACLInfo(getACLs(ids, entityName));
    }

}
