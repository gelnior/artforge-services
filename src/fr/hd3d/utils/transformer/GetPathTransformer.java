package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPath;


public class GetPathTransformer<T extends IBase> implements Transformer<T, String>
{
    public String transform(T object)
    {
        String path = null;
        if (IPath.class.isInstance(object))
        {
            path = ((IPath) object).getPath();
        }

        return path;
    }
}
