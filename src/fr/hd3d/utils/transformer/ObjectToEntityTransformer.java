package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.IBase;


public class ObjectToEntityTransformer<T extends IBase> implements Transformer<Object, T>
{
    public T transform(Object object)
    {
        T ret = null;
        if (object != null && IBase.class.isInstance(object))
        {
            ret = (T) object;
        }
        return ret;
    }
}
