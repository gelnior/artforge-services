package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;


public class GetTagTransformer implements Transformer<TagParent, ITag>
{
    public ITag transform(TagParent tagparent)
    {
        return tagparent == null ? null : tagparent.getTag();
    }
}
