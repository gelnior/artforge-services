package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.IStep;


public class GetStepBoundEntityTransformer implements Transformer<IStep, Long>
{
    public Long transform(IStep object)
    {
        Long boundEntity = null;
        if (IStep.class.isInstance(object))
        {
            boundEntity = ((IStep) object).getBoundEntity();
        }

        return boundEntity;
    }
}
