package fr.hd3d.utils.transformer;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.services.resources.ResourceContext;


/**
 * 
 * Given an IItemGroup, returns a Map containing:<br>
 * -key="id"/value=item group id<br>
 * -key="items"/value=Collection of Maps returned by GetItemTransformer.
 * 
 * @see GetItemTransformer
 * 
 */
public class GetItemGroupTransformer implements Transformer<IItemGroup, Map<String, Object>>
{
    Object entity;
    ResourceContext<?, ?> context;
    /* TODO ugly to pass the entire list here */
    List<?> result;

    public GetItemGroupTransformer(Object entity, ResourceContext<?, ?> context, List<?> result)
    {
        this.entity = entity;
        this.context = context;
        this.result = result;
    }

    public Map<String, Object> transform(final IItemGroup itemgroup)
    {
        Map<String, Object> ret = null;

        if (itemgroup != null)
        {
            ret = new HashMap<String, Object>(2);
            ret.put(fr.hd3d.utils.Const.ID, itemgroup.getId());
            ret.put(fr.hd3d.utils.Const.ITEMS, getTransformedItems(itemgroup.getItems(), entity, context, result));
        }

        return ret;
    }

    private Collection<Map<String, Object>> getTransformedItems(List<IItem> items, Object entity,
            ResourceContext<?, ?> context, List<?> result)
    {
        return CollectionUtils.collect(items, new GetItemTransformer(entity, true, context, result));
    }
}
