package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.lightweight.impl.LTag;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;


public class GetTagParentsIdTypeTransformer implements Transformer<TagParent, LTag.LTagParentIdType>
{
    public LTag.LTagParentIdType transform(TagParent tagparent)
    {
        return tagparent == null ? null : new LTag.LTagParentIdType(tagparent.getParent(), tagparent.getParentType());
    }
}
