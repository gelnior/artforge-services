package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.ITaskType;


public class GetApprovalNoteTaskTypeTransformer implements Transformer<IApprovalNote, ITaskType>
{
    public ITaskType transform(IApprovalNote approvalNote)
    {
        ITaskType ret = null;

        if (approvalNote != null && approvalNote.getApprovalNoteType() != null)
        {
            ret = approvalNote.getApprovalNoteType().getTaskType();
        }

        return ret;
    }
}
