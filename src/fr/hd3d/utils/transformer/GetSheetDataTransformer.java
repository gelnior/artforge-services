package fr.hd3d.utils.transformer;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.services.resources.ResourceContext;


/**
 * Given an Entity instance, returns a Map(key=Entity instance id ,value=Collection of Maps returned by
 * GetItemGroupTransformer
 * 
 * @see GetItemGroupTransformer
 */
public class GetSheetDataTransformer implements Transformer<Object, Map<String, Object>>
{
    private final ISheet sheet;
    private final ResourceContext<?, ?> context;
    /* TODO ugly to pass the entire list here */
    List result;

    public GetSheetDataTransformer(ISheet sheet, ResourceContext<?, ?> context, List result)
    {
        this.sheet = sheet;
        this.context = context;
        this.result = result;
    }

    public Map<String, Object> transform(final Object entity)
    {
        Map<String, Object> ret = null;

        if (entity != null)
        {
            ret = new HashMap<String, Object>(2);
            ret.put(fr.hd3d.utils.Const.ID, ((IBase) entity).getId());
            ret.put(fr.hd3d.utils.Const.GROUPS,
                    getTransformedItemGroups(sheet.getItemGroups(), entity, context, result));
        }

        return ret;
    }

    private Collection<Map<String, Object>> getTransformedItemGroups(List<IItemGroup> itemGroups, Object entity,
            ResourceContext<?, ?> context, List result)
    {
        return CollectionUtils.collect(itemGroups, new GetItemGroupTransformer(entity, context, result));
    }
}
