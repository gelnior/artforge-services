package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.common.client.Const;
import fr.hd3d.model.persistence.IDynMetaDataValue;


public class GetDynValueParentTransformer implements Transformer<IDynMetaDataValue, Long>
{
    public Long transform(IDynMetaDataValue object)
    {
        return object == null ? Const.ID_NULL : object.getParent();
    }
}
