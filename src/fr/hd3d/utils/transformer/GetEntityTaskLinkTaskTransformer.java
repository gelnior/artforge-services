package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.ITask;


public class GetEntityTaskLinkTaskTransformer implements Transformer<IEntityTaskLink, ITask>
{
    public ITask transform(IEntityTaskLink entityTaskLink)
    {
        return entityTaskLink == null ? null : (entityTaskLink.getTask() == null ? null : (entityTaskLink.getTask()
                .enabled() ? entityTaskLink.getTask() : null));
    }
}
