package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


public class GetParentsTransformer<T extends IBase> implements Transformer<TagParent, T>
{
    @SuppressWarnings("unchecked")
    public T transform(TagParent tagparent)
    {
        T ret = null;

        if (tagparent != null)
        {
            try
            {
                ret = (T) Persistors.getPersistor(tagparent.getParentType()).getById(tagparent.getParent());
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
        }

        return ret;
    }
}
