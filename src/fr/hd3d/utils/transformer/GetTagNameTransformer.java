package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.ITag;


public class GetTagNameTransformer implements Transformer<ITag, String>
{
    public String transform(ITag tag)
    {
        return tag == null ? null : tag.getName();
    }
}
