package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.common.client.Const;
import fr.hd3d.model.persistence.Persistent;


public class GetIdTransformer<T extends Persistent> implements Transformer<T, Long>
{
    public Long transform(T object)
    {
        return object == null ? Const.ID_NULL : object.getId();
    }
}
