package fr.hd3d.utils.transformer;

import java.util.Date;

import org.apache.commons.collections15.Transformer;


public class DateValueTransformer implements Transformer<String, Date>
{
    public Date transform(String object)
    {
        return fr.hd3d.utils.StringUtils.parseDate(object);
    }
}
