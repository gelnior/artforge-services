package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.IBase;


public class GetThumbnailTransformer<T extends IBase> implements Transformer<T, String>
{
    public String transform(T object)
    {
        return object == null ? "null|null" : object.entityName() + '|' + object.getId();
    }
}
