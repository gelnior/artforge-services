package fr.hd3d.utils.transformer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.impl.hibernate.ItemTypeHandler;
import fr.hd3d.services.resources.ItemGroupResource;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.resources.transformer.ItemTransformer;
import fr.hd3d.services.resources.transformer.TransformersMap;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.ServerResourceUtils;


/**
 * 
 * Given an IItem, returns a Map containing:<br>
 * -key="id"/value=item id<br>
 * -key="name"/value=item name<br>
 * -key="value"/value=item value, depending on item type
 * 
 * 
 * @param simpleDisplay
 * 
 */
public class GetItemTransformer implements Transformer<IItem, Map<String, Object>>
{
    Object entityInstance;
    /* TODO ugly to pass the entire list here */
    List<?> result;
    /* if simpleDisplay=true, returns only the id of the item, the "name" and "value" fields */
    boolean simpleDisplay;
    ResourceContext<?, ?> context;

    public GetItemTransformer(Object entityInstance, boolean simpleDisplay, ResourceContext<?, ?> context,
            List<?> result)
    {
        this.entityInstance = entityInstance;
        this.simpleDisplay = simpleDisplay;
        this.context = context;
        this.result = result;
    }

    public Map<String, Object> transform(final IItem item)
    {
        Map<String, Object> ret = null;

        if (item != null)
        {
            ret = new HashMap<String, Object>(5);
            ret.put(ItemGroupResource.ID, item.getId());
            ret.put(ItemGroupResource.NAME, item.getName());
            ret.put(ItemGroupResource.VALUE, getValue(item));
            if (!simpleDisplay)
            {
                ret.put(ItemGroupResource.CONTROLCONTENT, item.getControlContent());
                ret.put(ItemGroupResource.METATYPE, item.getMetaType());
            }
        }

        return ret;
    }

    private Object getValue(IItem item)
    {
        /*
         * if item is to get dynmetadataValue, item.query contains classDynMetaDataType id. In this case, return the
         * entity id (to keep), the dynmetadataValues will be retrieved in one time and re-attached properly using the
         * previous entity id. Otherwise, return the real value
         */
        final Object value = new ItemTypeHandler(entityInstance, item, null).getValue();

        /* apply the bound transformer on the the value */
        final Object transformedValue = applyTransformer(item, value);

        Object ret;
        try
        {
            ret = ServerResourceUtils.dynaBeanPropSafeConvert(transformedValue, this.context);
        }
        catch (Hd3dTranslationException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            ret = e;
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            ret = e;
        }

        return ret;
    }

    private Object applyTransformer(IItem item, Object value)
    {
        Object ret;

        if (item != null)
        {
            /* apply the bound transformer on the the value */
            final String transformerName = item.getTransformer();
            if (StringUtils.isNotBlank(transformerName))
            {
                /* get the transformer from the cache map, if not found try to instantiate it */
                ItemTransformer transformer = getTransformer(transformerName);
                if (transformer == null)
                {
                    ret = value;
                }
                else
                {
                    ret = transformer.transform(entityInstance, value, item.getTransformerParameters());
                }
            }
            else
            {
                ret = value;
            }
        }
        else
        {
            ret = value;
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    private static ItemTransformer getTransformer(final String transformerName)
    {
        ItemTransformer trans = TransformersMap.getTransformerInstance(transformerName);

        if (trans == null)
        {
            Class<ItemTransformer> clazz;
            try
            {
                clazz = (Class<ItemTransformer>) Class.forName(transformerName);
                trans = clazz.newInstance();
            }
            catch (ClassNotFoundException e)
            {
                Log.LOGGER.error("unable to instanciate {} class", transformerName);
            }
            catch (InstantiationException e)
            {
                Log.LOGGER.error("unable to instanciate {} class", transformerName);
            }
            catch (IllegalAccessException e)
            {
                Log.LOGGER.error("unable to instanciate {} class", transformerName);
            }
        }
        return trans;
    }
}
