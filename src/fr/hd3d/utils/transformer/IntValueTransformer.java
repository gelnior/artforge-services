package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.math.NumberUtils;


public class IntValueTransformer implements Transformer<String, Integer>
{
    public Integer transform(String object)
    {
        return NumberUtils.toInt(object);
    }
}
