package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskType;


public class GetTaskTaskTypeTransformer implements Transformer<ITask, ITaskType>
{
    public ITaskType transform(ITask task)
    {
        ITaskType ret = null;

        if (task != null)
        {
            ret = task.getTaskType();
        }

        return ret;
    }
}
