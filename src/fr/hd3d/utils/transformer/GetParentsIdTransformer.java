package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.persistence.impl.hibernate.ParentHandler;


public class GetParentsIdTransformer<T extends IBase> implements Transformer<T, Long>
{
    public Long transform(T object)
    {
        Long ret = null;

        if (object != null)
        {
            Persistent parent = ParentHandler.getParent(object);
            if (parent != null)
            {
                ret = parent.getId();
            }
        }

        return ret;
    }
}
