package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.model.persistence.impl.hibernate.serialization.EntityDescriptor;


public class ObjectToEntityDescriptorTransformer<T extends EntityDescriptor> implements Transformer<Object, T>
{
    public T transform(Object object)
    {
        return (T) object;
    }
}
