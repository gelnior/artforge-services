package fr.hd3d.utils.transformer;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.math.NumberUtils;


public class LongValueTransformer implements Transformer<String, Long>
{
    public Long transform(String object)
    {
        return NumberUtils.toLong(object);
    }
}
