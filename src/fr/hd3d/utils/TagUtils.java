package fr.hd3d.utils;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.TagH;


public final class TagUtils
{
    private TagUtils()
    {}

    static public Long getIdFromName(String name)
    {
        try
        {
            Criteria criteria = HibernateUtil.currentSession().createCriteria(TagH.class);
            criteria.add(Restrictions.eq("name", name));
            ITag tag = (ITag) criteria.uniqueResult();
            return tag.getId();
        }
        catch (Exception e)
        {
            return Long.valueOf(0);
        }
    }

    static public Long getOrCreateTagIdFromName(String name)
    {
        Long ret = getIdFromName(name);
        if (BaseH.isValidId(ret))
        {
            return ret;
        }
        else
        {
            ITag tag = new TagH(name);
            try
            {
                tag.doPrePersist();
                Persistors.tag.persist(tag);
                tag.doPostPersist();
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
                return Long.valueOf(0);
            }

            return tag.getId();
        }
    }

    static public boolean tagExists(Long id)
    {
        try
        {
            ITag tag = Persistors.tag.getById(id);
            return (tag != null && BaseH.isValidId(tag.getId()));
        }
        catch (Exception e)
        {
            Log.LOGGER.error(e.getMessage());
            return false;
        }
    }

}
