package fr.hd3d.utils;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;


/**
 * @author Thomas ESKENAZI
 */
public final class EnumUtils
{
    private EnumUtils()
    {}

    /**
     * returns the Enum as a List of Strings values
     * 
     * @param <E>
     *            Enum to be returned as List of String
     * @param enumClass
     *            Enum to be returned as List of String
     * @return List of Strings
     */
    public static <E extends Enum<E>> List<String> getEnumStringValues(final Class<E> enumClass)
    {
        List<String> ret = null;

        if (enumClass != null)
        {
            ret = new ArrayList<String>();
            for (Enum<E> oneEnum : EnumSet.allOf(enumClass))
            {
                ret.add(oneEnum.toString());
            }
        }

        return ret;
    }

    public static <E extends Enum<E>> E getEnumValueIgnoreCase(final Class<E> enumClass, final String value)
    {
        E ret = null;

        if (enumClass != null && value != null)
        {
            try
            {
                /* try lower case */
                ret = Enum.valueOf(enumClass, value.toLowerCase());
            }
            catch (IllegalArgumentException e)
            {
                try
                {
                    /* try upper case */
                    ret = Enum.valueOf(enumClass, value.toUpperCase());
                }
                catch (IllegalArgumentException e1)
                {
                    try
                    {
                        /* try as is */
                        ret = Enum.valueOf(enumClass, value);
                    }
                    catch (IllegalArgumentException e2)
                    {
                        Log.LOGGER.error(ExceptUtils.format(e2));
                    }
                }
            }
        }

        return ret;
    }
}
