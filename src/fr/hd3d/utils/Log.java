package fr.hd3d.utils;

import org.slf4j.LoggerFactory;


public final class Log
{
    private Log()
    {}

    public final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger("hd3d");
}
