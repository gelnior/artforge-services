package fr.hd3d.model.exec;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.PrintStream;


public abstract class ScriptMessageStream extends FilterOutputStream
{
    private final StringBuilder message = new StringBuilder();

    public ScriptMessageStream(PrintStream stream)
    {
        super(stream);
    }

    @Override
    public void write(int b) throws IOException
    {
        message.append(Character.toString((char) b));
        out.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException
    {
        for (int i = off; i < off + len; i++)
        {
            this.write(b[i]);
        }
    }

    public String getMessage()
    {
        return message.toString();
    }
}
