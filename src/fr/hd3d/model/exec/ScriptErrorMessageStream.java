/**
 * 
 */
package fr.hd3d.model.exec;

final class ScriptErrorMessageStream extends ScriptMessageStream
{
    public ScriptErrorMessageStream()
    {
        super(System.err);
    }
}
