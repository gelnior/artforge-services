/**
 * 
 */
package fr.hd3d.model.exec;

final class ScriptOutputMessageStream extends ScriptMessageStream
{
    public ScriptOutputMessageStream()
    {
        super(System.out);
    }
}
