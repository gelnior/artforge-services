package fr.hd3d.model.exec;

import org.apache.commons.exec.PumpStreamHandler;


public class ScriptStreamHandler extends PumpStreamHandler
{
    // private ScriptErrorMessageStream myFilter;

    public ScriptStreamHandler(ScriptErrorMessageStream myFilter, ScriptOutputMessageStream output)
    {
        super(output, myFilter);
        // this.myFilter = myFilter;
    }

    public String getErrorMessage()
    {
        return ((ScriptErrorMessageStream) getErr()).getMessage();
        // return myFilter.getMessage();
    }

    public String getOutputMessage()
    {
        return ((ScriptOutputMessageStream) getOut()).getMessage();
    }

}
