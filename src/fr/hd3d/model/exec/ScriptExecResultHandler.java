package fr.hd3d.model.exec;

import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;

import fr.hd3d.utils.Log;


public class ScriptExecResultHandler implements ExecuteResultHandler
{
    private static final String SUCCESS = "Script {} completed successfully.";
    private static final String FAIL = "Script {} failed!";

    public String scriptLocation;
    public int exitCode;
    public String errorMessage = null;
    public String outputMessage = null;

    private boolean terminated = false;
    private final ScriptStreamHandler streamHandler;

    public ScriptExecResultHandler(String scriptLocation, ScriptStreamHandler streamHandler)
    {
        this.scriptLocation = scriptLocation;
        this.streamHandler = streamHandler;
    }

    public boolean isTerminated()
    {
        return this.terminated;
    }

    public boolean isNotTerminated()
    {
        return !isTerminated();
    }

    public void onProcessComplete(int exitcode)
    {
        this.terminated = true;
        Log.LOGGER.info(SUCCESS, scriptLocation);
        this.exitCode = exitcode;
        this.outputMessage = streamHandler.getOutputMessage();
    }

    public void onProcessFailed(ExecuteException e)
    {
        this.terminated = true;
        Log.LOGGER.error(FAIL, scriptLocation);
        this.exitCode = e.getExitValue();
        // this.errorMessage = e.getMessage();
        this.outputMessage = streamHandler.getOutputMessage();
        this.errorMessage = streamHandler.getErrorMessage();
    }
}
