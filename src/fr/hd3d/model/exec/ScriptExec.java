package fr.hd3d.model.exec;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

import fr.hd3d.common.client.ScriptMessageGenerator;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.impl.hibernate.ScriptH;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


public class ScriptExec
{
    public final static String TIMEOUT_IS = "script timeout is {}";
    public final static String EXECUTING_SCRIPT = "Executing script : {}";
    public final static String STOPPED = "Execution of {} has been stopped due to timeout.";
    public final static CharSequence RUN = " run, exit code is ";
    public final static CharSequence RUN_WITH_ERROR = " run with Error, exit code is ";
    public final static CharSequence ERROR_IS = ", error message is ";

    public final static String OPERATION = "operation";
    public final static String ENTITYNAME = "entityName";
    public final static String PARAMS = "params";
    public final static String URL = "url";

    private final String trigger;
    /* key="operation", value=[CREATE|UPDATE|DELETE] */
    /* key="entityName", value=the entity name of the object the operation performed on */
    /* key="url", value=the url of the object after operation. */
    private final Map<String, String> params;

    private final static String SCRIPT_LOCATION;

    static
    {
        final String rootDir = Conf.getScriptLocation();
        if (StringUtils.isBlank(rootDir))
        {
            SCRIPT_LOCATION = "";
        }
        else if (!rootDir.endsWith(SystemUtils.FILE_SEPARATOR))
        {
            SCRIPT_LOCATION = rootDir + SystemUtils.FILE_SEPARATOR;
        }
        else
        {
            SCRIPT_LOCATION = rootDir;
        }
    }

    public ScriptExec(final String trigger, final Map<String, String> params)
    {
        this.trigger = trigger;
        this.params = params;
    }

    public ScriptExec(final Map<String, String> params)
    {
        this.trigger = null;
        this.params = params;
    }

    public String exec() throws Hd3dPersistenceException
    {
        final StringBuilder exitValues = new StringBuilder(50);

        if (StringUtils.isNotEmpty(this.trigger))
        {
            /* collect all the scripts triggered by the event and execute them */
            final Collection<IScript> scripts = new CopyOnWriteArrayList<IScript>(ScriptH.scripts().getCollection(
                    this.trigger));
            CollectUtils.removeNull(scripts);

            String[] parameters = new String[3];
            for (IScript script : scripts)
            {
                parameters[0] = params.get(OPERATION);
                parameters[1] = params.get(ENTITYNAME);
                parameters[2] = params.get(URL);
                Log.LOGGER.info("executing scripts " + script.getName() + " triggered by " + trigger
                        + " with parameters " + params);
                execute(script, parameters, exitValues);
                // PersistedObjectProviders.script.updateObject(s);
            }

            exitValues.trimToSize();
        }

        return exitValues.toString();
    }

    public String exec(final IScript script) throws Hd3dPersistenceException
    {
        final StringBuilder exitValues = new StringBuilder(50);

        if (script != null)
        {
            execute(script, new String[] { params.get(PARAMS) }, exitValues);

            // PersistedObjectProviders.script.updateObject(script);
            exitValues.trimToSize();
        }

        return exitValues.toString();

    }

    private IScript execute(final IScript script, final String[] params, final StringBuilder exitValues)
    {
        if (script != null)
        {
            /* configure script executor */
            final DefaultExecutor executor = new DefaultExecutor();
            ScriptStreamHandler scriptStreamHandler = new ScriptStreamHandler(new ScriptErrorMessageStream(),
                    new ScriptOutputMessageStream());
            executor.setStreamHandler(scriptStreamHandler);
            executor.setExitValue(script.getExitCode());

            /* configure watchdog */
            final int timeout = Conf.getScriptTimeOut();
            Log.LOGGER.info(TIMEOUT_IS, timeout);
            final ExecuteWatchdog watchdog = new ExecuteWatchdog(timeout);
            executor.setWatchdog(watchdog);

            final String scriptLocation = Conf.getScriptEngine() + " " + SCRIPT_LOCATION + script.getLocation();

            /* handler */
            final ScriptExecResultHandler handler = new ScriptExecResultHandler(scriptLocation, scriptStreamHandler);

            ScriptMessageGenerator messageGenerator = null;

            try
            {
                /* create the command line and parameters */
                final CommandLine commandLine = getCommandLine(scriptLocation, params);

                Log.LOGGER.info(EXECUTING_SCRIPT, commandLine.toString());

                /* EXECUTION: ScriptHandler is called after command line execution */
                executor.execute(commandLine, handler);// asynchro

                /* if timeout occurs */
                if (watchdog.killedProcess())
                {
                    Log.LOGGER.error(STOPPED, scriptLocation);
                }

                /* POLLING to check script execution */
                while (handler.isNotTerminated())
                {
                    try
                    {
                        Thread.sleep(2000L);// one check every 2s
                    }
                    catch (InterruptedException e)
                    {
                        Log.LOGGER.error(ExceptUtils.format(e));
                    }
                }

                if (handler.errorMessage == null)
                {
                    messageGenerator = new ScriptMessageGenerator(handler.exitCode, "", handler.outputMessage);
                }
                else
                {
                    messageGenerator = new ScriptMessageGenerator(handler.exitCode, URLEncoder.encode(
                            handler.errorMessage, "UTF-8"), "");
                }

                if (StringUtils.isNotEmpty(messageGenerator.getOutputMessage()))
                {
                    exitValues.append(messageGenerator.outputMessageToJsonString());
                }
                else
                {
                    exitValues.append(messageGenerator.errorMessageToJsonString());
                }

                /* update the script object with exit code and execution date */
                script.setLastRun(new Date());
                script.setLastExitCode(handler.exitCode);
                script.setLastRunMessage(handler.errorMessage);
            }
            catch (ExecuteException e)
            {
                // If CommandLine is called Asynchroniously, the exception is redirected to the handler
                // exitValue = e.getExitValue();
                // exitValues += s.getName() + " run with Error, exit code is " + exitValue + ", exeption is "
                // + e.getMessage() + "\r\n";
            }
            catch (IOException e)
            {
                // If CommandLine is called Asynchroniously, the exception is redirected to the handler
                // exitValue = handler.exitCode;
                // exitValues += s.getName() + " run with Error, exit code is " + exitValue + ", exeption is "
                // + e.getMessage() + "\r\n";
            }
        }

        return script;
    }

    private CommandLine getCommandLine(final String scriptLocation, final String[] params)
    {
        final CommandLine commandLine = CommandLine.parse(scriptLocation);

        if (!ArrayUtils.isEmpty(params))
        {
            for (String param : params)
            {
                commandLine.addArgument(param);
            }
        }
        return commandLine;
    }
}
