package fr.hd3d.model.preferences;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.Parameter;

import fr.hd3d.model.persistence.Name;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.utils.StringUtils;


/**
 * 
 * @author try.lam
 * 
 */

@Entity
@Table(name = "hd3d_sheetfilter")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class SheetFilter implements Serializable, Persistent, Name
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    protected java.sql.Timestamp version;
    private Long sheet;
    private String filter;
    private String name;

    public SheetFilter()
    {}

    public SheetFilter(final Long sheet, final String filter, final String name)
    {
        super();
        this.sheet = sheet;
        this.filter = filter;
        this.name = name;
    }

    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_sheetfilter"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    @Version
    public java.sql.Timestamp getVersion()
    {
        return version;
    }

    @Column(name = "sheetfilter_sheetid")
    public Long getSheet()
    {
        return sheet;
    }

    public void setSheet(final Long sheet)
    {
        this.sheet = sheet;
    }

    @Column(columnDefinition = "TEXT", name = "sheetfilter_filter")
    public String getFilter()
    {
        return filter;
    }

    @Column(name = "sheetfilter_name")
    public String getName()
    {
        return name;
    }

    public void setFilter(final String filter)
    {
        this.filter = filter;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public void setVersion(final java.sql.Timestamp version)
    {
        this.version = version;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((filter == null) ? 0 : filter.hashCode());
        result = prime * result + ((sheet == null) ? 0 : sheet.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SheetFilter other = (SheetFilter) obj;
        if (filter == null)
        {
            if (other.filter != null)
                return false;
        }
        else if (!filter.equals(other.filter))
            return false;
        if (sheet == null)
        {
            if (other.sheet != null)
                return false;
        }
        else if (!sheet.equals(other.sheet))
            return false;
        if (version == null)
        {
            if (other.version != null)
                return false;
        }
        else if (!version.equals(other.version))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

    public String toString()
    {
        return new ToStringBuilder(this).append("sheet", StringUtils.nullSafeString(sheet)).append("version", version)
                .append("filter", StringUtils.nullSafeString(filter)).append("name", StringUtils.nullSafeString(name))
                .toString();
    }
}
