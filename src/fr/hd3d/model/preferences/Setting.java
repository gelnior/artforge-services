package fr.hd3d.model.preferences;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.Parameter;

import fr.hd3d.model.persistence.Persistent;


/**
 * 
 * @author try.lam
 * 
 */

@Entity
@Table(name = "hd3d_settings")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class Setting implements Serializable, Persistent
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    protected java.sql.Timestamp version;
    private String login;
    private Long person;// use id instead of entity for decoupling
    private String key;
    private String value;

    public Setting()
    {}

    public Setting(final String login, final Long person, final String key, final String value)
    {
        super();
        this.login = login;
        this.person = person;
        this.key = key;
        this.value = value;
    }

    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_settings"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    @Version
    public java.sql.Timestamp getVersion()
    {
        return version;
    }

    @Column(name = "settings_login")
    public String getLogin()
    {
        return login;
    }

    @Column(name = "settings_person")
    public Long getPerson()
    {
        return person;
    }

    @Column(name = "settings_key")
    public String getKey()
    {
        return key;
    }

    @Column(columnDefinition = "TEXT", name = "settings_value")
    public String getValue()
    {
        return value;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setVersion(final java.sql.Timestamp version)
    {
        this.version = version;
    }

    public void setLogin(final String login)
    {
        this.login = login;
    }

    public void setPerson(final Long person)
    {
        this.person = person;
    }

    public void setKey(final String key)
    {
        this.key = key;
    }

    public void setValue(final String value)
    {
        this.value = value;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((login == null) ? 0 : login.hashCode());
        result = prime * result + ((person == null) ? 0 : person.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Setting other = (Setting) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (key == null)
        {
            if (other.key != null)
                return false;
        }
        else if (!key.equals(other.key))
            return false;
        if (login == null)
        {
            if (other.login != null)
                return false;
        }
        else if (!login.equals(other.login))
            return false;
        if (person == null)
        {
            if (other.person != null)
                return false;
        }
        else if (!person.equals(other.person))
            return false;
        if (value == null)
        {
            if (other.value != null)
                return false;
        }
        else if (!value.equals(other.value))
            return false;
        if (version == null)
        {
            if (other.version != null)
                return false;
        }
        else if (!version.equals(other.version))
            return false;
        return true;
    }

    public String toString()
    {
        return new ToStringBuilder(this).append("id", id).append("version", version).append("login", login).append(
                "key", key).append("value", value).toString();
    }
}
