package fr.hd3d.model.persistence;

public interface IProxy extends IBase
{
    IFileRevision getFileRevision();

    IProxyType getProxyType();

    String getLocation();

    String getDescription();

    String getMountPoint();

    String getFilename();

    Integer getFramerate();

    void setFileRevision(IFileRevision fileRevision);

    void setProxyType(IProxyType proxyType);

    void setFilename(String filename);

    void setLocation(String location);

    void setDescription(String description);

    void setMountPoint(String mountPoint);

    void setFramerate(Integer framerate);
}
