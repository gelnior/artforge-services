package fr.hd3d.model.persistence;

public interface Name
{
    String getName();

    void setName(String name);
}
