package fr.hd3d.model.persistence;

import java.util.Date;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("DynMetaDataValue")
public interface IDynMetaDataValue extends IBase
{

    IClassDynMetaDataType getClassDynMetaDataType();

    Long getParent();

    String getParentType();

    String getValue();

    Object getObjectValue();

    Boolean getBooleanValue();

    Date getDateValue();

    Long getLongValue();

    void setClassDynMetaDataType(IClassDynMetaDataType classDynMetaDataType);

    void setParent(Long parent);

    void setValue(String value);

    void setObjectValue(IBase objectValue);

    void setDateValue(Date dateValue);

    void setParentType(String parentType);

    void setBooleanValue(Boolean booleanValue);

    void setLongValue(Long longValue);

    String getDynMetaDataType_Type();

    boolean isEntity();
}
