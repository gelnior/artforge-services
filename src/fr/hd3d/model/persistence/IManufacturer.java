package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for computer pools needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Manufacturer")
public interface IManufacturer extends ISimple
{
    String getName();

    void setName(String name);
}
