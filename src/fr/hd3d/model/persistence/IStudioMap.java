package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for studio map informations needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Studiomap")
public interface IStudioMap extends IBase, Name
{

    String getImagePath();

    void setImagePath(String imagePath);

    Integer getSizeX();

    void setSizeX(Integer sizeX);

    Integer getSizeY();

    void setSizeY(Integer sizeY);

    Set<IComputer> getComputers();

    void setComputers(Set<IComputer> computers);
}
