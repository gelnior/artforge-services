package fr.hd3d.model.persistence;

import fr.hd3d.common.client.enums.ESkillLevel;
import fr.hd3d.common.client.enums.ESkillMotivation;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for skill level informations needed by the human resources application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("SkillLevel")
public interface ISkillLevel extends IBase
{
    ESkillLevel getLevel();

    void setLevel(ESkillLevel level);

    ESkillMotivation getMotivation();

    void setMotivation(ESkillMotivation motivation);

    IPerson getPerson();

    void setPerson(IPerson person);

    ISkill getSkill();

    void setSkill(ISkill skill);
}
