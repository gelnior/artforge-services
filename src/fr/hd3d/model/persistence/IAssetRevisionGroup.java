package fr.hd3d.model.persistence;

import java.util.List;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("AssetRevisionGroup")
public interface IAssetRevisionGroup extends IBase, Project, Name
{

    Set<IAssetRevision> getAssetRevisions();

    String getCriteria();

    String getValue();

    IProject getProject();

    void setAssetRevisions(Set<IAssetRevision> list);

    void setCriteria(String criteria);

    void setValue(String value);

    void addAssetRevision(IAssetRevision assetRevision);

    void removeAssetRevision(IAssetRevision assetRevision);

    void setProject(IProject project);

    IBase getAssociatedEntity();

    String getAssociatedObjectId();

    String getAssociatedEntityName();

    String getFileRevisionsIds() throws Hd3dException;

    List<IFileRevision> getFileRevisions() throws Hd3dException;

}
