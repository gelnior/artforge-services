package fr.hd3d.model.persistence;

import java.util.Date;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Try LAM
 * 
 */
@Hd3dEntityName("WorkingTime")
public interface IWorkingTime extends IBase, IDurable
{
    IPersonDay getPersonDay();

    Date getEndDate();

    Date getStartDate();

    void setPersonDay(IPersonDay personDay);

    void setEndDate(Date endDate);

    void setStartDate(Date startDate);

}
