package fr.hd3d.model.persistence;

import java.util.Date;

import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("GraphicalAnnotation")
public interface IGraphicalAnnotation extends IBase
{
    String getAnnotationSvg();

    Long getMarkIn();

    Long getMarkOut();

    String getComment();

    IPerson getAuthor();

    IProxy getProxy();

    IApprovalNote getApprovalNote();

    Date getDate();

    void setAnnotationSvg(String annotationSvg);

    void setMarkIn(Long markIn);

    void setMarkOut(Long markOut);

    void setAuthor(IPerson author);

    void setDate(Date date);

    void setComment(String comment);

    void setProxy(IProxy proxy);

    void setApprovalNote(IApprovalNote approvalNote);
}
