package fr.hd3d.model.persistence;

import java.util.Date;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for an inventory item such as a computer, a device, a software or a screen.
 * 
 * @author HD3D
 */
@Hd3dEntityName("InventoryITem")
public interface IInventoryItem extends IResource, Name
{

    String getSerial();

    String getBillingReference();

    Date getPurchaseDate();

    Date getWarrantyEnd();

    EInventoryStatus getInventoryStatus();

    void setSerial(String serial);

    void setBillingReference(String billingReference);

    void setPurchaseDate(Date purchaseDate);

    void setWarrantyEnd(Date purchaseDate);

    void setInventoryStatus(EInventoryStatus inventoryStatus);
}
