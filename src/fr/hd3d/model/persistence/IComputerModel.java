package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for CPU informations needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("ComputerModel")
public interface IComputerModel extends IModel
{
    Set<IComputer> getComputers();

    void setComputers(Set<IComputer> computers);
}
