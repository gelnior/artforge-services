package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for computer pools needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Pool")
public interface IPool extends IBase, Name
{

    Boolean getIsDispatcherAllowed();

    void setIsDispatcherAllowed(Boolean isDispatcherAllowed);

    Set<IComputer> getComputers();

    void setComputers(Set<IComputer> computers);
}
