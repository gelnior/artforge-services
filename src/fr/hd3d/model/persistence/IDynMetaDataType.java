package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("DynMetaDataType")
public interface IDynMetaDataType extends IBase, Name
{

    String getNature();

    String getType();

    String getDefaultValue();

    String getStructName();

    Long getStructId();

    void setNature(String nature);

    void setType(String type);

    void setDefaultValue(String defaultValue);

    void setStructName(String structName);

    void setStructId(Long structId);

    IListValues getListValues();
}
