package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILItemGroup;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Table(name = "hd3d_itemgroup")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ItemGroupH extends BaseH implements IItemGroup
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Integer index;// display order
    private String name;
    private List<IItem> items;
    private ISheet sheet;
    private String hook;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_itemgroup"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ItemGroupH()
    {}

    public ItemGroupH(Long id, Timestamp version, String name, List<IItem> items, ISheet sheet, String hook)
    {
        super(id, version);
        this.name = name;
        setItems(items);
        this.sheet = sheet;
        if (sheet != null)
            sheet.addItemGroup(this);
        this.hook = hook;
    }

    public ItemGroupH(String name, List<IItem> items, ISheet sheet, String hook)
    {
        this(null, null, name, items, sheet, hook);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "itemgroup_index")
    public Integer getIndex()
    {
        return index;
    }

    public void setIndex(Integer index)
    {
        if (index == null)
            this.index = NumberUtils.INTEGER_ZERO;
        else
            this.index = index;
    }

    @Column(name = "itemgroup_name", nullable = false)
    @NotEmpty
    public String getName()
    {
        return name;
    }

    @OneToMany(targetEntity = ItemH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "itemGroup")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.MERGE,
            org.hibernate.annotations.CascadeType.LOCK })
    @IndexColumn(name = "item_index", base = 0)
    @org.hibernate.annotations.OrderBy(clause = "item_index")
    @Fetch(value = FetchMode.SELECT)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public List<IItem> getItems()
    {
        if (items == null)
            items = new ArrayList<IItem>();
        return items;
    }

    @ManyToOne(targetEntity = SheetH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "itemgroup_sheet_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public ISheet getSheet()
    {
        return sheet;
    }

    @Column(name = "itemgroup_hook")
    public String getHook()
    {
        return hook;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setItems(List<IItem> items)
    {
        if (items == null)
            this.items = new ArrayList<IItem>();

        // must set the index manually (seems hibernate does not work well)
        // note: do this first...
        if (items != null)
        {
            for (IItem item : items)
            {
                if (item == null)
                    continue;
                if (item.getIndex() == null || item.getIndex().equals(0))
                {
                    item.setIndex(items.indexOf(item));
                }
                item.setItemGroup(this);
            }
            // ...and let hibernate synchronize
            this.items = items;
        }
    }

    public void setSheet(ISheet sheet)
    {
        this.sheet = sheet;
    }

    public void setHook(String hook)
    {
        this.hook = hook;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((hook == null) ? 0 : hook.hashCode());
        result = prime * result + ((index == null) ? 0 : index.hashCode());
        // result = prime * result + ((items == null) ? 0 : items.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((sheet == null) ? 0 : sheet.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ItemGroupH other = (ItemGroupH) obj;
        if (hook == null)
        {
            if (other.hook != null)
                return false;
        }
        else if (!hook.equals(other.hook))
            return false;
        if (index == null)
        {
            if (other.index != null)
                return false;
        }
        else if (!index.equals(other.index))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (sheet == null)
        {
            if (other.sheet != null)
                return false;
        }
        else if (!sheet.equals(other.sheet))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    public void completePrePersistOrUpdate()
    {
        if (StringUtils.isBlank(getHook()))
        {
            setHook(HookHandler.getHook(this));
        }
    }

    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    public void addItem(IItem item)
    {
        if (!getItems().contains(item) && item != null)
        {
            getItems().add(item);
            // must set the index manually (seems hibernate does not work well)
            item.setIndex(getItems().indexOf(item));
        }
    }

    public IItem itemByName(String name)
    {
        IItem item = null;
        if (StringUtils.isBlank(name))
            return item;

        for (IItem aItem : getItems())
        {
            if (aItem != null && name.equals(aItem.getName()))
            {
                item = aItem;
                break;
            }
        }
        return item;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILItemGroup, IItemGroup> defaultTranslator()
    {
        return Translators.itemGroup;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("index", index).append("name", name);
    // b.append("itemIds", StringUtils.join(CollectUtils.getIds(getItems()), ','));
    // b.append("sheetId", sheet == null ? "null" : sheet.getId());
    // b.append("hook", hook);
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
