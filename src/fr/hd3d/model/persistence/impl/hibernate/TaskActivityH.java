package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Table(name = "hd3d_task_activity")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class TaskActivityH extends ActivityH implements ITaskActivity
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private ITask task;

    /*-------------
     * Constructors
     -------------*/
    public TaskActivityH()
    {}

    /**
     * @param id
     * @param version
     * @param startingDate
     * @param duration
     * @param comment
     */
    public TaskActivityH(Long id, java.sql.Timestamp version, ITask task, IPersonDay day, Long duration,
            String comment, IPerson filledBy, Date filledDate)
    {
        super(id, version, day, duration, comment, filledBy, filledDate);
        this.task = task;
    }

    public TaskActivityH(ITask task, IPersonDay day, Long duration, String comment, IPerson filledBy, Date filledDate)
    {
        this(null, null, task, day, duration, comment, filledBy, filledDate);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToOne(targetEntity = TaskH.class, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "task_activity_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public ITask getTask()
    {
        return task;
    }

    public void setTask(ITask task)
    {
        this.task = task;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    /**
     * Equality definition What's make a TaskActivity unique is its task
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((getTask() == null) ? 0 : getTask().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof TaskActivityH))
            return false;
        TaskActivityH other = (TaskActivityH) obj;
        if (getTask() == null)
        {
            if (other.getTask() != null)
                return false;
        }
        else if (!getTask().equals(other.getTask()))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    public String taskName()
    {
        return getTask().getName();
    }

    // Note: cannot rename to project() because it's a bean attribute method in SimpleActivityHibernateImpl
    // It inherits from ActivityHibernateImpl which is an abstract class.
    @Transient
    public IProject getProject()
    {
        return getTask().getProject();
    }

    // Note: cannot rename to worker() because it's a bean attribute method in SimpleActivityHibernateImpl
    // It inherits from ActivityHibernateImpl which is an abstract class.
    @Transient
    public IPerson getWorker()
    {
        return getTask().getWorker();
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILTaskActivity, ITaskActivity> defaultTranslator()
    {
        return Translators.taskActivity;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("taskId", task == null ? "null" : task.getId());

        return b.toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
