package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.common.client.enums.EFileStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;


/**
 * @author Try LAM
 */
@Entity
@Indexed
@Table(name = "hd3d_filerevision")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class FileRevisionH extends BaseH implements IFileRevision
{
    private static final long serialVersionUID = 1L;

    private static final String TIMESTAMP_FIELD = "creationDate";
    private static final String LASTOPERATIONDATE_FIELD = "lastOperationDate";
    private static final String STATUS_FIELD = "status";
    private static final String STATE_FIELD = "state";

    private String uriScheme;// file,http...
    private Date creationDate;
    private String key;// Customer dependent
    private String variation;// Customer dependent
    private Integer revision;// Customer dependent
    private IPerson creator;
    private String format;
    private Long size;
    private String checksum;// Customer dependent
    private String location;// Customer dependent
    private String relativePath;// Customer dependent
    private EFileStatus status;
    private Date lastOperationDate;
    private IPerson lastUser;
    private String comment;
    private String validity;// Enum ?
    private EFileState state;

    private String mountPoint;

    /* Sequence: null if standalone file; (start, step, stop) if sequence file */
    private String sequence;// Customer dependent

    private Set<IFileAttribute> attributes;

    private IAssetRevision assetRevision;

    private Set<IProxy> proxies;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_filerevision"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public FileRevisionH()
    {}

    public FileRevisionH(Long id, java.sql.Timestamp version, Date creationDate, String key, String variation,
            Integer revision, IPerson creator, String format, Long size, String checksum, String location,
            String relativePath, EFileStatus status, Date lastOperationDate, IPerson lastUser, String comment,
            String validity, String sequence, EFileState state, String mountPoint)
    {
        super(id, version);
        this.creationDate = creationDate;
        this.key = key;
        this.variation = variation;
        this.revision = revision;
        this.creator = creator;
        this.format = format;
        this.size = size;
        this.checksum = checksum;
        this.location = location;
        this.relativePath = relativePath;
        this.status = status;
        this.lastOperationDate = lastOperationDate;
        this.lastUser = lastUser;
        this.comment = comment;
        this.validity = validity;
        this.sequence = sequence;
        this.state = state;
        this.mountPoint = mountPoint;
    }

    public FileRevisionH(Date creationDate, String key, String variation, Integer revision, IPerson creator,
            String format, Long size, String checksum, String location, String relativePath, EFileStatus status,
            Date lastOperationDate, IPerson lastUser, String comment, String validity, String sequence,
            EFileState state, String mountPoint)
    {
        this(null, null, creationDate, key, variation, revision, creator, format, size, checksum, location,
                relativePath, status, lastOperationDate, lastUser, comment, validity, sequence, state, mountPoint);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "filerevision_urischeme")
    public String getUriScheme()
    {
        return uriScheme;
    }

    @Column(name = "filerevision_creationdate")
    public Date getCreationDate()
    {
        return creationDate;
    }

    @Column(name = "filerevision_key")
    @NotEmpty
    public String getKey()
    {
        return key;
    }

    @Column(name = "filerevision_variation")
    public String getVariation()
    {
        return variation;
    }

    @Column(name = "filerevision_revision")
    public Integer getRevision()
    {
        return revision;
    }

    @OneToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "filerevision_creator")
    @Fetch(FetchMode.JOIN)
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getCreator()
    {
        return creator;
    }

    @Column(name = "filerevision_format")
    public String getFormat()
    {
        return format;
    }

    @Column(name = "filerevision_size")
    public Long getSize()
    {
        return size;
    }

    @Column(name = "filerevision_checksum")
    public String getChecksum()
    {
        return checksum;
    }

    @Column(name = "filerevision_location")
    public String getLocation()
    {
        return location;
    }

    @Column(name = "filerevision_relativepath")
    public String getRelativePath()
    {
        return relativePath;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "filerevision_status", nullable = false)
    public EFileStatus getStatus()
    {
        return status;
    }

    @Column(name = "filerevision_lastoperationdate")
    public Date getLastOperationDate()
    {
        return lastOperationDate;
    }

    @OneToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "filerevision_lastuser")
    @Fetch(FetchMode.JOIN)
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getLastUser()
    {
        return lastUser;
    }

    @Column(name = "filerevision_comment")
    public String getComment()
    {
        return comment;
    }

    @Column(name = "filerevision_validity")
    public String getValidity()
    {
        return validity;
    }

    @Column(name = "filerevision_sequence")
    public String getSequence()
    {
        return sequence;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "filerevision_state")
    public EFileState getState()
    {
        return state;
    }

    @OneToMany(targetEntity = FileAttributeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "fileRevision")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    // @JoinColumn(name = "fileattribute_file_id")
    @BatchSize(size = 50)
    @Fetch(FetchMode.JOIN)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IFileAttribute> getAttributes()
    {
        if (attributes == null)
            return new HashSet<IFileAttribute>();
        return attributes;
    }

    @OneToMany(targetEntity = ProxyH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "fileRevision")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @Fetch(value = FetchMode.JOIN)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @BatchSize(size = 50)
    public Set<IProxy> getProxies()
    {
        if (proxies == null)
        {
            return new HashSet<IProxy>();
        }
        return proxies;
    }

    @Column(name = "filerevision_mountpoint")
    public String getMountPoint()
    {
        return mountPoint;
    }

    public void setMountPoint(String mountPoint)
    {
        this.mountPoint = mountPoint;
    }

    @ManyToOne(targetEntity = AssetRevisionH.class)
    @JoinColumn(name = "filerevision__assetrevision_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IAssetRevision getAssetRevision()
    {
        return assetRevision;
    }

    public void setAssetRevision(IAssetRevision assetRevision)
    {
        this.assetRevision = assetRevision;
    }

    public void setProxies(Set<IProxy> proxies)
    {
        this.proxies = proxies;
    }

    public void setUriScheme(String uriScheme)
    {
        this.uriScheme = uriScheme;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public void setVariation(String variation)
    {
        this.variation = variation;
    }

    public void setRevision(Integer revision)
    {
        this.revision = revision;
    }

    public void setCreator(IPerson creator)
    {
        this.creator = creator;
    }

    public void setFormat(String format)
    {
        this.format = format;
    }

    public void setSize(Long size)
    {
        this.size = size;
    }

    public void setChecksum(String checksum)
    {
        this.checksum = checksum;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public void setRelativePath(String relativePath)
    {
        this.relativePath = relativePath;
    }

    public void setStatus(EFileStatus status)
    {
        this.status = status;
    }

    public void setLastOperationDate(Date lastOperationDate)
    {
        this.lastOperationDate = lastOperationDate;
    }

    public void setLastUser(IPerson lastUser)
    {
        this.lastUser = lastUser;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setValidity(String validity)
    {
        this.validity = validity;
    }

    public void setSequence(String sequence)
    {
        this.sequence = sequence;
    }

    public void setAttributes(Set<IFileAttribute> attributes)
    {
        this.attributes = attributes;
    }

    public void setState(EFileState state)
    {
        this.state = state;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((checksum == null) ? 0 : checksum.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + ((creator == null) ? 0 : creator.hashCode());
        result = prime * result + ((format == null) ? 0 : format.hashCode());
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((lastOperationDate == null) ? 0 : lastOperationDate.hashCode());
        result = prime * result + ((lastUser == null) ? 0 : lastUser.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((relativePath == null) ? 0 : relativePath.hashCode());
        result = prime * result + ((revision == null) ? 0 : revision.hashCode());
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((validity == null) ? 0 : validity.hashCode());
        result = prime * result + ((variation == null) ? 0 : variation.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        FileRevisionH other = (FileRevisionH) obj;
        if (checksum == null)
        {
            if (other.checksum != null)
                return false;
        }
        else if (!checksum.equals(other.checksum))
            return false;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (creationDate == null)
        {
            if (other.creationDate != null)
                return false;
        }
        else if (!creationDate.equals(other.creationDate))
            return false;
        if (creator == null)
        {
            if (other.creator != null)
                return false;
        }
        else if (!creator.equals(other.creator))
            return false;
        if (format == null)
        {
            if (other.format != null)
                return false;
        }
        else if (!format.equals(other.format))
            return false;
        if (key == null)
        {
            if (other.key != null)
                return false;
        }
        else if (!key.equals(other.key))
            return false;
        if (lastOperationDate == null)
        {
            if (other.lastOperationDate != null)
                return false;
        }
        else if (!lastOperationDate.equals(other.lastOperationDate))
            return false;
        if (lastUser == null)
        {
            if (other.lastUser != null)
                return false;
        }
        else if (!lastUser.equals(other.lastUser))
            return false;
        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (relativePath == null)
        {
            if (other.relativePath != null)
                return false;
        }
        else if (!relativePath.equals(other.relativePath))
            return false;
        if (revision == null)
        {
            if (other.revision != null)
                return false;
        }
        else if (!revision.equals(other.revision))
            return false;
        if (size == null)
        {
            if (other.size != null)
                return false;
        }
        else if (!size.equals(other.size))
            return false;
        if (status == null)
        {
            if (other.status != null)
                return false;
        }
        else if (!status.equals(other.status))
            return false;
        if (validity == null)
        {
            if (other.validity != null)
                return false;
        }
        else if (!validity.equals(other.validity))
            return false;
        if (variation == null)
        {
            if (other.variation != null)
                return false;
        }
        else if (!variation.equals(other.variation))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Transient
    public Set<IFileRevisionLink> getFileRevisionLinks_ThisAsInput() throws Hd3dException
    {
        List<IFileRevisionLink> fileRevisionLinks = Persistors.filerevisionlink.getByValue(null, "inputFileRevision",
                this);

        if (fileRevisionLinks == null)
            return new HashSet<IFileRevisionLink>();

        return new HashSet<IFileRevisionLink>(fileRevisionLinks);
    }

    @Transient
    public Set<IFileRevisionLink> getFileRevisionLinks_ThisAsOutput() throws Hd3dException
    {
        List<IFileRevisionLink> fileRevisionLinks = Persistors.filerevisionlink.getByValue(null, "outputFileRevision",
                this);

        if (fileRevisionLinks == null)
            return new HashSet<IFileRevisionLink>();

        return new HashSet<IFileRevisionLink>(fileRevisionLinks);
    }

    @SuppressWarnings("serial")
    @Transient
    public Set<IFileRevisionLink> getFileRevisionLinks() throws Hd3dException
    {
        return new HashSet<IFileRevisionLink>() {
            {
                addAll(getFileRevisionLinks_ThisAsInput());
                addAll(getFileRevisionLinks_ThisAsOutput());
            }
        };
    }

    // @Transient
    // public Set<IAssetRevisionFileRevision> getAssetRevisionFileRevisions() throws Hd3dException
    // {
    // String[] properties = { "fileKey", "fileVariation", "fileRevision" };
    // Object[] values = { this.getKey(), this.getVariation(), this.getRevision() };
    //
    // List<IAssetRevisionFileRevision> assetRevisionFileRevisions = Persistors.assetrevisionfilerevision.getByValues(
    // null, properties, values, "AND");
    //
    // if (assetRevisionFileRevisions == null)
    // return new HashSet<IAssetRevisionFileRevision>();
    //
    // return new HashSet<IAssetRevisionFileRevision>(assetRevisionFileRevisions);
    // }

    @Transient
    @Field(index = Index.UN_TOKENIZED, store = Store.YES, name = "fullpath")
    public String getFullPath()
    {
        return getLocation() + getRelativePath();
    }

    public void addAttribute(IFileAttribute attribute)
    {
        getAttributes().add(attribute);
    }

    public void removeAttribute(IFileAttribute attribute)
    {
        getAttributes().remove(attribute);
    }

    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        // cascade disable
        Collection<IFileAttribute> fileAttributes = CollectUtils.selectNotNull(getAttributes());
        Persistors.fileattribute.disableCollection(fileAttributes);

        Collection<IFileRevisionLink> fileRevisionLinks = CollectUtils.selectNotNull(getFileRevisionLinks());
        Persistors.filerevisionlink.disableCollection(fileRevisionLinks);

        IAssetRevision assetRevision = getAssetRevision();
        if (assetRevision != null && assetRevision.getFileRevisions() != null)
        {
            assetRevision.getFileRevisions().remove(this);
        }
        // Collection<IAssetRevisionFileRevision> assetRevisionFileRevisions = CollectUtils
        // .selectNotNull(getAssetRevisionFileRevisions());
        // Persistors.assetrevisionfilerevision.disableCollection(assetRevisionFileRevisions);
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILFileRevision, IFileRevision> defaultTranslator()
    {
        return Translators.filerevision;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("uriScheme", uriScheme).append("creationDate", creationDate).append("key", key).append("variation",
                variation).append("revision", revision);
        b.append("creatorId", creator == null ? "null" : creator.getId());
        b.append("format", format).append("size", size).append("checksum", checksum).append("location", location)
                .append("relativePath", relativePath).append("status", status).append("lastOperationDate",
                        lastOperationDate);
        b.append("lastUserId", lastUser == null ? "null" : lastUser.getId());
        b.append("validity", validity).append("sequence", sequence);
        b.append("attributeIds", StringUtils.join(CollectUtils.getIds(getAttributes()), ','));

        return b.toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class FileComplexTypeProvider implements IComplexTypeProvider
    {
        public List<String> getValidEnum(String field)
        {
            if (STATUS_FIELD.equals(field))
                return EnumUtils.getEnumStringValues(EFileStatus.class);
            if (STATE_FIELD.equals(field))
                return EnumUtils.getEnumStringValues(EFileState.class);
            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            Object ret = null;

            if (isValidTypeField(field))
            {
                if (STATUS_FIELD.equals(field))
                {
                    ret = EnumUtils.getEnumValueIgnoreCase(EFileStatus.class, value);
                }
                else if (STATE_FIELD.equals(field))
                {
                    ret = EnumUtils.getEnumValueIgnoreCase(EFileState.class, value);
                }
                else if (TIMESTAMP_FIELD.equals(field) || LASTOPERATIONDATE_FIELD.equals(field))
                {
                    ret = fr.hd3d.utils.StringUtils.parseDate(value);
                }
            }

            return ret;
        }

        public boolean isValidTypeField(String field)
        {
            return STATE_FIELD.equals(field) || STATUS_FIELD.equals(field) || TIMESTAMP_FIELD.equals(field)
                    || LASTOPERATIONDATE_FIELD.equals(field);
        }
    }

}
