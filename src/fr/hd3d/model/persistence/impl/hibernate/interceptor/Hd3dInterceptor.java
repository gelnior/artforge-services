package fr.hd3d.model.persistence.impl.hibernate.interceptor;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.collection.PersistentCollection;
import org.hibernate.type.Type;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.audit.Audit;
import fr.hd3d.model.audit.UpdatesStack;
import fr.hd3d.model.persistence.Hook;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.persistence.impl.hibernate.visitor.update.PreUpdateVisitor;
import fr.hd3d.model.preferences.Setting;
import fr.hd3d.model.preferences.SheetFilter;
import fr.hd3d.ontology.annotation.AnnotationHandler;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.ServerResourceUtils;
import fr.hd3d.utils.StringUtils;


public final class Hd3dInterceptor extends EmptyInterceptor
{
    private static final long serialVersionUID = 1L;
    public static CharSequence FIELD = "field '";
    public static CharSequence WAS = "' was ";
    public static CharSequence NOW = ", now ";

    Set<Object> alreadyProcessed = new HashSet<Object>();

    List<UpdatesStack> stack = new CopyOnWriteArrayList<UpdatesStack>();

    /* keeps entities which will produce annotations for the anntotation server */
    Set<Persistent> entitiesForAnnotation = new CopyOnWriteArraySet<Persistent>();

    /****************
     * ON ENTITY SAVE
     ****************/
    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types)
            throws CallbackException
    {
        auditPost(entity, id);

        updateStack(entity, id, "CREATE;");

        completeHookableEntity(entity, id);

        if (Conf.isAnnotationOn())
        {
            if (entity instanceof IBase)
                entitiesForAnnotation.add((IBase) entity);
        }

        return true;
    }

    private void auditPost(Object entity, Serializable id)
    {
        if (entity != null && entity instanceof IBase)
        {
            IBase ent = (IBase) entity;
            Audit audit = newAudit(Audit.CREATE);
            audit.setDateMs(new Date().getTime());
            audit.setEntityName(ent.entityName());
            audit.setEntityId(ent.getId());// done after persistence to have id
            ServerResourceUtils.save(audit);
        }
    }

    private Audit newAudit(final String operation)
    {
        final IPerson person = AuthenticationUtil.getCurrentUser();
        final Audit audit = new Audit();
        if (person != null)
        {
            audit.setPerson(person.getId());
            audit.setLogin(person.getLogin());
            audit.setFirstName(person.getFirstName());
            audit.setLastName(person.getLastName());
        }
        audit.setOperation(operation);

        return audit;
    }

    private synchronized void updateStack(Object entity, Serializable id, String operation)
    {
        if (Conf.isUpdatesStackOn())
        {
            String _operation = operation;
            if (entity instanceof IBase)
            {
                IBase ent = (IBase) entity;
                if ("UPDATE;".equals(operation))
                {
                    _operation = ent.enabled() ? "UPDATE;" : "DELETE;";
                }
                stack
                        .add(new UpdatesStack(updateStackMessage(_operation, ent.entityName(), id, ent
                                .getInternalUUID())));
            }
            else if (entity instanceof Setting || entity instanceof SheetFilter)
            {
                stack.add(new UpdatesStack(updateStackMessage(_operation, entity.getClass().getName(), id, null)));
            }
        }
    }

    private void completeHookableEntity(Object entity, Serializable id)
    {
        if (entity instanceof IBase && entity instanceof Hook)
        {
            Hook ent = (Hook) entity;
            ent.setHook(ent.getHook() + id);
        }
    }

    /*******************
     * ON ENTITY UPDATE
     *******************/
    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState,
            String[] propertyNames, Type[] types)
    {
        boolean ret = false;

        if (!alreadyProcessed.contains(entity.hashCode()))
        {
            auditPut(entity, id, currentState, previousState, propertyNames, types);

            updateStack(entity, id, "UPDATE;");

            ret = doPreUpdate(entity, id, currentState, previousState, propertyNames, types);
        }
        return ret;
    }

    private boolean doPreUpdate(Object entity, Serializable id, Object[] currentState, Object[] previousState,
            String[] propertyNames, Type[] types)
    {
        boolean ret = false;

        if (entity instanceof IBase)
        {
            try
            {
                if (!alreadyProcessed.contains(entity.hashCode()))
                {
                    alreadyProcessed.add(entity.hashCode());// must be done before any processing (which might loop)
                    ret = new PreUpdateVisitor().visit(entity, id, currentState, previousState, propertyNames, types);
                }
            }
            catch (Hd3dException e)
            {
                throw new CallbackException(e);
            }
            alreadyProcessed.remove(entity.hashCode());
        }

        return ret;
    }

    private String updateStackMessage(final String operation, final String entityName, final Serializable id,
            final String internalUUID)
    {
        return new StringBuilder(operation).append(entityName).append(';').append(id).append(';').append(internalUUID)
                .toString();
    }

    private boolean isDirty(Object entity, Object currentState, Object previousState, Type type)
    {
        boolean isDirty;
        if (type.isCollectionType() && currentState != null)
        {
            isDirty = ((PersistentCollection) currentState).isDirty();
        }
        else
        {
            isDirty = !org.apache.commons.lang.ObjectUtils.equals(previousState, currentState);
        }
        return isDirty;
    }

    private void auditPut(Object entity, Serializable id, Object[] currentState, Object[] previousState,
            String[] propertyNames, Type[] types)
    {
        if (entity != null && entity instanceof IBase)
        {
            IBase ent = (IBase) entity;

            Audit audit = newAudit(Audit.UPDATE);
            audit.setDateMs((new Date()).getTime());
            audit.setEntityName(ent.entityName());
            audit.setEntityId(ent.getId());

            for (int i = 0; i < currentState.length; i++)
            {
                if (isDirty(entity, currentState[i], previousState == null ? null : previousState[i],
                        types == null ? null : types[i]))
                {
                    StringBuilder modif = new StringBuilder();
                    if (IBase.class.isAssignableFrom(types[i].getReturnedClass()))
                    {
                        modif.append(FIELD).append(propertyNames[i]).append(WAS).append(
                                StringUtils.nullSafeIdString(previousState == null ? null : (IBase) previousState[i]))
                                .append(NOW).append(StringUtils.nullSafeIdString((IBase) currentState[i])).append(";");
                    }
                    else if (types[i].getReturnedClass().isEnum())
                    {
                        modif.append(FIELD).append(propertyNames[i]).append(WAS).append(
                                StringUtils.nullSafeString(previousState == null ? null : previousState[i]))
                                .append(NOW).append(StringUtils.nullSafeString(currentState[i])).append(";");
                    }
                    else if (types[i].isCollectionType())
                    {
                        // TODO use PersistentCollection.getStoredSnapshot() and test for Set, List, Map...
                        List currentCol = new ArrayList((Collection) currentState[i]);

                        if (previousState != null && previousState[i] instanceof PersistentCollection)
                        {
                            Object previousCol = ((PersistentCollection) previousState[i]).getStoredSnapshot();
                            if (previousCol instanceof Map)
                            {
                                /* storedSnapShot is stored as a Map */
                                List<Object> previousCollection = new ArrayList<Object>(((Map) previousCol).values());
                                Object firstNotNullObj = null;
                                for (Object obj : currentCol)
                                {
                                    if (obj != null)
                                    {
                                        firstNotNullObj = obj;
                                        break;
                                    }
                                }
                                if (firstNotNullObj == null)
                                {
                                    for (Object obj : previousCollection)
                                    {
                                        if (obj != null)
                                        {
                                            firstNotNullObj = obj;
                                            break;
                                        }
                                    }
                                }

                                if (firstNotNullObj != null && IBase.class.isInstance(firstNotNullObj))
                                {
                                    List<? extends IBase> entities = CollectUtils.toEntities(previousCollection);
                                    modif.append(FIELD).append(propertyNames[i]).append(WAS).append(
                                            org.apache.commons.lang.StringUtils
                                                    .join(CollectUtils.getIds(entities), ',')).append(NOW).append(
                                            org.apache.commons.lang.StringUtils.join(CollectUtils.getIds(currentCol),
                                                    ','));
                                }
                                else
                                {
                                    modif.append(FIELD).append(propertyNames[i]).append(WAS).append(
                                            org.apache.commons.lang.StringUtils.join(previousCollection, ',')).append(
                                            NOW).append(org.apache.commons.lang.StringUtils.join(currentCol, ','));
                                }
                            }
                        }
                        else
                        {
                            modif.append(FIELD).append(propertyNames[i]).append(WAS).append(
                                    previousState == null ? "null" : org.apache.commons.lang.StringUtils.join(
                                            (Collection) previousState[i], ',')).append(NOW).append(
                                    org.apache.commons.lang.StringUtils.join(currentCol, ','));
                        }
                    }
                    else
                    {
                        modif.append(FIELD).append(propertyNames[i]).append(WAS).append(
                                StringUtils.nullSafeString(previousState == null ? null : previousState[i]))
                                .append(NOW).append(StringUtils.nullSafeString(currentState[i])).append(";");
                    }
                    audit.addModification(modif.toString());
                }
            }

            ServerResourceUtils.save(audit);
        }
    }

    /****************
     * BEFORE COMMIT
     ****************/
    // @Override
    // public void beforeTransactionCompletion(Transaction tx)
    // {
    // persistUpdateStack(tx);
    // }
    @Override
    public synchronized void afterTransactionCompletion(Transaction tx)
    {
        if (!tx.wasCommitted())
            return;

        persistUpdateStack();

        if (Conf.isAnnotationOn())
        {
            try
            {
                /* send annotation to annotation server */
                AnnotationHandler handler = new AnnotationHandler(entitiesForAnnotation);
                handler.buildAnnotations();
                handler.sendAnnotations();
            }
            catch (IOException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
            finally
            {
                entitiesForAnnotation.clear();
            }
        }
    }

    private void persistUpdateStack()
    {
        if (Conf.isUpdatesStackOn())
        {
            if (!stack.isEmpty())
            {
                StatelessSession updateStackSession = null;
                Transaction updateStackTx;
                try
                {
                    updateStackSession = HibernateUtil.getSessionFactory().openStatelessSession();
                    updateStackTx = updateStackSession.beginTransaction();

                    for (UpdatesStack obj : stack)
                    {
                        updateStackSession.insert(obj);
                    }

                    updateStackTx.commit();
                }
                finally
                {
                    stack.clear();
                    if (updateStackSession != null)
                        updateStackSession.close();
                }
            }
        }
    }
}
