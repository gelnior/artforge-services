package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILComposition;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IFact;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Indexed
@Table(name = "hd3d_composition")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class CompositionH extends BaseH implements IComposition
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String name;
    private String description;
    private IConstituent constituent;
    private IShot shot;
    private ISequence sequence;

    private List<IFact> facts;
    private Integer nbOccurence = 1;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_composition"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public CompositionH()
    {}

    public CompositionH(Long id, Timestamp version, String name, String description, IConstituent constituent,
            IShot shot, ISequence sequence, Integer nbOccurence)
    {
        super(id, version);
        this.name = name;
        this.description = description;
        this.constituent = constituent;
        this.shot = shot;
        this.sequence = sequence;
        if (nbOccurence != null)
            this.nbOccurence = nbOccurence;
    }

    public CompositionH(Long id, Timestamp version, String name, String description, IConstituent constituent,
            IShot shot, ISequence sequence)
    {
        this(id, version, name, description, constituent, shot, sequence, 1);
    }

    public CompositionH(String name, String description, IConstituent constituent, IShot shot, ISequence sequence,
            Integer nbOccurence)
    {
        this(null, null, name, description, constituent, shot, sequence, nbOccurence);
    }

    public CompositionH(String name, String description, IConstituent constituent, IShot shot, ISequence sequence)
    {
        this(null, null, name, description, constituent, shot, sequence, 1);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "composition_name")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    public String getName()
    {
        return name;
    }

    @Column(name = "composition_description")
    @Field(index = Index.TOKENIZED, store = Store.YES)
    public String getDescription()
    {
        return description;
    }

    @ManyToOne(targetEntity = ConstituentH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "constituent_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IConstituent getConstituent()
    {
        return this.constituent;
    }

    @Transient
    @Field(name = "constituent_id", index = Index.UN_TOKENIZED, store = Store.YES)
    public Long getConstituentId()
    {
        return CollectUtils.nullSafeId(getConstituent());
    }

    @ManyToOne(targetEntity = ShotH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "shot_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IShot getShot()
    {
        return shot;
    }

    @ManyToOne(targetEntity = SequenceH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "sequence_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public ISequence getSequence()
    {
        return sequence;
    }

    @OneToMany(targetEntity = FactH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "composition")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public List<IFact> getFacts()
    {
        if (facts == null)
            facts = new ArrayList<IFact>();
        return facts;
    }

    @Column(name = "composition_nboccurence")
    public Integer getNbOccurence()
    {
        return nbOccurence;
    }

    public void setNbOccurence(Integer nbOccurence)
    {
        this.nbOccurence = nbOccurence;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setConstituent(IConstituent constituent)
    {
        this.constituent = constituent;
    }

    public void setShot(IShot shot)
    {
        this.shot = shot;
        /* shot mutually excludes sequence */
        if (shot != null)
            this.sequence = null;
    }

    public void setSequence(ISequence sequence)
    {
        this.sequence = sequence;
        /* sequence mutually excludes shot */
        if (sequence != null)
            this.shot = null;
    }

    public void setFacts(List<IFact> facts)
    {
        this.facts = facts;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((constituent == null) ? 0 : constituent.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((nbOccurence == null) ? 0 : nbOccurence.hashCode());
        result = prime * result + ((sequence == null) ? 0 : sequence.hashCode());
        result = prime * result + ((shot == null) ? 0 : shot.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        CompositionH other = (CompositionH) obj;
        if (constituent == null)
        {
            if (other.constituent != null)
                return false;
        }
        else if (constituent.getId() != null && other.constituent != null
                && !constituent.getId().equals(other.constituent.getId()))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (nbOccurence == null)
        {
            if (other.nbOccurence != null)
                return false;
        }
        else if (!nbOccurence.equals(other.nbOccurence))
            return false;
        if (sequence == null)
        {
            if (other.sequence != null)
                return false;
        }
        else if (sequence.getId() != null && other.sequence != null && !sequence.getId().equals(other.sequence.getId()))
            return false;
        if (shot == null)
        {
            if (other.shot != null)
                return false;
        }
        else if (shot.getId() != null && other.shot != null && !shot.getId().equals(other.shot.getId()))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Transient
    @Field(name = "facts_id", index = Index.TOKENIZED, store = Store.YES)
    public String getFactsIdString()
    {
        return StringUtils.join(CollectUtils.getIds(getFacts()), ' ');
    }

    @Transient
    @Field(name = "project_id", index = Index.TOKENIZED, store = Store.YES)
    public String getProjectIdString()
    {
        String ret = null;

        if (getConstituent() != null && getConstituent().getCategory() != null)
        {
            IProject project = getConstituent().getCategory().getProject();
            if (project != null)
            {
                ret = String.valueOf(project.getId());
            }
        }
        else if (getSequence() != null)
        {
            IProject project = getSequence().getProject();
            if (project != null)
            {
                ret = String.valueOf(project.getId());
            }

        }
        else if (getShot() != null && getShot().getSequence() != null)
        {
            IProject project = getShot().getSequence().getProject();
            if (project != null)
            {
                ret = String.valueOf(project.getId());
            }
        }

        return ret;
    }

    public IBaseTranslator<ILComposition, IComposition> defaultTranslator()
    {
        return Translators.composition;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public void postDisable() throws Hd3dException
    {
        super.postDisable();

        // cascade disable
        Collection<IFact> facts = CollectUtils.selectNotNull(getFacts());
        Persistors.fact.disableCollection(facts);
    }

    @Override
    public void completePrePersistOrUpdate()
    {
        super.completePrePersistOrUpdate();
        if (StringUtils.isBlank(getName()))
        {
            if (getConstituent() != null)
            {
                name += getConstituent().getLabel();
            }
            if (getShot() != null)
            {
                name += '-' + getShot().getLabel();
            }
            if (getSequence() != null)
            {
                name += '-' + getSequence().getName();
            }
        }
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }
    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("name", name).append("description", description);
    // b.append("constituentId", constituent == null ? "null" : constituent.getId());
    // b.append("shotId", shot == null ? "null" : shot.getId());
    // b.append("sequenceId", sequence == null ? "null" : sequence.getId());
    // b.append("factIds", StringUtils.join(CollectUtils.getIds(getFacts()), ','));
    // b.append("nbOccurence", nbOccurence);
    //
    // return b.toString();
    // }
    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
