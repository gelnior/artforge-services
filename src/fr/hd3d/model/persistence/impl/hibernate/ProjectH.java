package fr.hd3d.model.persistence.impl.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;


/**
 * A34 time sheet implementation https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Indexed
@Table(name = "hd3d_project")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ProjectH extends BaseH implements IProject
{
    private static final long serialVersionUID = 1L;

    private static final String STATUS_FIELD = "status";
    private static final String START_DATE_FIELD = "actualStartDate";
    private static final String END_DATE_FIELD = "actualEndDate";

    private String name;
    private Date startDate;
    private Date endDate;
    private String color;
    private EProjectStatus status;

    private Set<IResourceGroup> resourceGroups;
    private Set<ISimpleActivity> simpleActivities;

    private Set<ICategory> categories;
    private ICategory rootCategory;
    private Set<ISequence> sequences;
    private ISequence rootSequence;
    private List<ISheet> sheets;
    private String hook;
    private IProjectType projectType;

    private Set<ITaskType> taskTypes;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_project"), @Parameter(name = "initial_value", value = "1"),
            @Parameter(name = "increment_size", value = "1"), @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ProjectH()
    {}

    /**
     * @param id
     * @param version
     * @param name
     * @param color
     * @param status
     * @param staff
     */
    public ProjectH(Long id, java.sql.Timestamp version, String name, Date startDate, Date endDate, String color,
            EProjectStatus status, Set<IResourceGroup> resourceGroups, String hook)
    {
        super(id, version);
        this.setName(name);
        this.startDate = startDate;
        this.endDate = endDate;
        this.color = color;
        this.status = status;
        this.resourceGroups = resourceGroups;
        this.hook = hook;
        this.createRootCategory();
        this.createRootSequence();
    }

    public ProjectH(String name, Date startDate, Date endDate, String color, EProjectStatus status,
            Set<IResourceGroup> resourceGroups, String hook)
    {
        this(null, null, name, startDate, endDate, color, status, resourceGroups, hook);
    }

    private final void createRootCategory()
    {
        ICategory rootCategory = new CategoryH(Const.CATEGORY_ROOT, null, this);
        this.rootCategory = rootCategory;
        getCategories().add(rootCategory);
    }

    private final void createRootSequence()
    {
        ISequence rootSequence = new SequenceH(Const.SEQUENCE_ROOT, null, null, this, null);
        this.rootSequence = rootSequence;
        getSequences().add(rootSequence);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "project_name", nullable = false)
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    @NotEmpty
    public String getName()
    {
        return name;
    }

    @Column(name = "project_start_date")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    public Date getStartDate()
    {
        return startDate;
    }

    @Column(name = "project_end_date")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    public Date getEndDate()
    {
        return endDate;
    }

    @Column(name = "project_color")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    public String getColor()
    {
        return color;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "project_status", nullable = false)
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    public EProjectStatus getStatus()
    {
        return status;
    }

    @ManyToMany(targetEntity = ResourceGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_resourcegroup__project", joinColumns = @JoinColumn(name = "project_id"), inverseJoinColumns = @JoinColumn(name = "resourcegroup_id"))
    @BatchSize(size = 50)
    @Fetch(value = FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IResourceGroup> getResourceGroups()
    {
        if (resourceGroups == null)
            resourceGroups = new HashSet<IResourceGroup>();
        return resourceGroups;
    }

    @OneToMany(targetEntity = SimpleActivityH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "project")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @Fetch(value = FetchMode.SELECT)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<ISimpleActivity> getSimpleActivities()
    {
        if (simpleActivities == null)
            simpleActivities = new HashSet<ISimpleActivity>();
        return simpleActivities;
    }

    @OneToMany(targetEntity = CategoryH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "project")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @Fetch(value = FetchMode.SELECT)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @ContainedIn
    public Set<ICategory> getCategories()
    {
        if (categories == null)
            categories = new HashSet<ICategory>();
        return categories;
    }

    @OneToMany(targetEntity = SequenceH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "project")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @Fetch(value = FetchMode.SELECT)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @ContainedIn
    public Set<ISequence> getSequences()
    {
        if (sequences == null)
            sequences = new HashSet<ISequence>();
        return sequences;
    }

    @OneToMany(targetEntity = SheetH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "project")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @Fetch(value = FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @BatchSize(size = 50)
    public List<ISheet> getSheets()
    {
        if (sheets == null)
            sheets = new ArrayList<ISheet>();
        return this.sheets;
    }

    @Column(name = "project_hook")
    public String getHook()
    {
        return hook;
    }

    @ManyToOne(targetEntity = ProjectTypeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "project__projecttype_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProjectType getProjectType()
    {
        return projectType;
    }

    public void setProjectType(IProjectType projectType)
    {
        this.projectType = projectType;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public void setStatus(EProjectStatus status)
    {
        this.status = status;
    }

    public void setResourceGroups(Set<IResourceGroup> resourceGroups)
    {
        this.resourceGroups = resourceGroups;
    }

    public void setSimpleActivities(Set<ISimpleActivity> simpleActivities)
    {
        this.simpleActivities = simpleActivities;
    }

    // A12
    public void setCategories(Set<ICategory> categories)
    {
        this.categories = categories;
    }

    public void setSequences(Set<ISequence> sequences)
    {
        this.sequences = sequences;
    }

    public void setHook(String hook)
    {
        this.hook = hook;
    }

    public void setSheets(List<ISheet> sheets)
    {
        this.sheets = sheets;
    }

    @ManyToMany(targetEntity = TaskTypeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_project__tasktype", joinColumns = @JoinColumn(name = "project_id"), inverseJoinColumns = @JoinColumn(name = "tasktype_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<ITaskType> getTaskTypes()
    {
        if (this.taskTypes == null)
        {
            this.taskTypes = new HashSet<ITaskType>();
        }
        return this.taskTypes;
    }

    public void setTaskTypes(Set<ITaskType> taskTypes)
    {
        this.taskTypes = taskTypes;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((hook == null) ? 0 : hook.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProjectH other = (ProjectH) obj;
        if (color == null)
        {
            if (other.color != null)
                return false;
        }
        else if (!color.equals(other.color))
            return false;
        if (hook == null)
        {
            if (other.hook != null)
                return false;
        }
        else if (!hook.equals(other.hook))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (status == null)
        {
            if (other.status != null)
                return false;
        }
        else if (!status.equals(other.status))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    public void completePrePersistOrUpdate()
    {
        if (StringUtils.isBlank(getHook()))
        {
            setHook(HookHandler.getHook(this));
        }
    }

    @Transient
    public Set<IPerson> getStaff()
    {
        Set<IPerson> staff = new HashSet<IPerson>();
        for (IResourceGroup rg : getResourceGroups())
        {
            staff.addAll(rg.getStaff());
        }
        return staff;
    }

    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    public void addResourceGroup(IResourceGroup resourceGroup)
    {
        getResourceGroups().add(resourceGroup);
    }

    public void addCategory(ICategory category)
    {
        if (category.isRoot())
            throw new Hd3dRuntimeException("Cannot add another Root Category (already exists)");
        getCategories().add(category);
    }

    public void addSheet(ISheet sheet)
    {
        this.sheets.add(sheet);
    }

    /**
     * return the root category of this project
     */
    public ICategory rootCategory()
    {
        if (this.rootCategory != null)
            return this.rootCategory;

        for (ICategory c : getCategories())
            if (c.isRoot())
            {
                this.rootCategory = c;
                return c;
            }

        return null;
    }

    public void addSequence(ISequence sequence)
    {
        if (sequence.isRoot())
            throw new Hd3dRuntimeException("Cannot add another Root Sequence (already exists)");
        getSequences().add(sequence);
    }

    /**
     * return the root sequence of this project
     */
    public ISequence rootSequence()
    {
        if (this.rootSequence != null)
            return this.rootSequence;

        for (ISequence s : getSequences())
            if (s.isRoot())
            {
                this.rootSequence = s;
                return s;
            }

        return null;
    }

    public IBaseTranslator<ILProject, IProject> defaultTranslator()
    {
        return Translators.project;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();

        for (IResourceGroup g : getResourceGroups())
        {
            g.getProjects().remove(this);
        }

        // cascade disable
        Collection<ISimpleActivity> simpleActivities = CollectUtils.selectNotNull(getSimpleActivities());
        Persistors.simpleActivity.disableCollection(simpleActivities);

        Collection<ICategory> categories = CollectUtils.selectNotNull(getCategories());
        Persistors.category.disableCollection(categories);

        Collection<ISequence> sequences = CollectUtils.selectNotNull(getSequences());
        Persistors.sequence.disableCollection(sequences);

        Collection<ISheet> sheets = CollectUtils.selectNotNull(getSheets());
        Persistors.sheet.disableCollection(sheets);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("name", name).append("color", color).append("status", status);
    //
    // b.append("resourceGroupIds", StringUtils.join(CollectUtils.getIds(getResourceGroups()), ','));
    // b.append("simpleActivitieIds", StringUtils.join(CollectUtils.getIds(getSimpleActivities()), ','));
    // b.append("categorieIds", StringUtils.join(CollectUtils.getIds(getCategories()), ','));
    // b.append("rootCategory", rootCategory == null ? "null" : rootCategory.getId());
    // b.append("sequenceIds", StringUtils.join(CollectUtils.getIds(getSequences()), ','));
    // b.append("rootSequence", rootSequence == null ? "null" : rootSequence.getId());
    // b.append("sheetIds", StringUtils.join(CollectUtils.getIds(getSheets()), ','));
    // b.append("hook", hook);
    // b.append("projectTypeId", projectType == null ? "null" : projectType.getId());
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class ProjectComplexTypeProvider implements IComplexTypeProvider
    {
        public Object getValue(String field, String value)
        {
            if (STATUS_FIELD.equals(field))
                return EnumUtils.getEnumValueIgnoreCase(EProjectStatus.class, value);
            else if (START_DATE_FIELD.equals(field) || END_DATE_FIELD.equals(field))
                return fr.hd3d.utils.StringUtils.parseDate(value);
            else
                return null;
        }

        public List<String> getValidEnum(String field)
        {
            if (STATUS_FIELD.equals(field) || START_DATE_FIELD.equals(field) || END_DATE_FIELD.equals(field))
                return EnumUtils.getEnumStringValues(EProjectStatus.class);
            return Collections.emptyList();
        }

        public boolean isValidTypeField(String field)
        {
            return STATUS_FIELD.equals(field);
        }
    }
}
