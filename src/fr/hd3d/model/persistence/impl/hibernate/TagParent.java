package fr.hd3d.model.persistence.impl.hibernate;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;

import fr.hd3d.model.persistence.ITag;


/**
 * This is a joint class between TagHibernateImpl and the other business classes implementing IBaseObject. So no point
 * to extend IBaseObject
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */

@Entity
@Table(name = "tagparent")
public class TagParent implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Long id;
    protected java.sql.Timestamp version;
    private Long parent;
    private String parentType;
    private ITag tag;

    public TagParent()
    {}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId()
    {
        return id;
    }

    @Version
    public java.sql.Timestamp getVersion()
    {
        return version;
    }

    public Long getParent()
    {
        return parent;
    }

    public String getParentType()
    {
        return parentType;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setVersion(java.sql.Timestamp version)
    {
        this.version = version;
    }

    public void setParent(Long parent)
    {
        this.parent = parent;
    }

    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    @ManyToOne(targetEntity = TagH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "tag_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public ITag getTag()
    {
        return tag;
    }

    public void setTag(ITag tag)
    {
        this.tag = tag;
    }
}
