package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.model.lightweight.ILComputerModel;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IComputerModel;
import fr.hd3d.model.persistence.IManufacturer;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * Computer model hibernate object needed for database persistance.
 * 
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_computer_model")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ComputerModelH extends SimpleH implements IComputerModel
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Set<IComputer> computers;
    private IManufacturer manufacturer;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_computermodel"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ComputerModelH()
    {}

    public ComputerModelH(Long id, Timestamp version, String name, IManufacturer manufacturer)
    {
        super(id, version, name);

        this.manufacturer = manufacturer;
    }

    public ComputerModelH(String name, IManufacturer manufacturer)
    {
        this(null, null, name, manufacturer);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @OneToMany(targetEntity = ComputerH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "computerModel")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IComputer> getComputers()
    {
        if (this.computers == null)
        {
            this.computers = new HashSet<IComputer>();
        }
        return this.computers;
    }

    public void setComputers(Set<IComputer> computers)
    {
        this.computers = computers;
    }

    public IBaseTranslator<ILComputerModel, IComputerModel> defaultTranslator()
    {
        return Translators.computerModel;
    }

    @ManyToOne(targetEntity = ManufacturerH.class)
    @JoinColumn(name = "manufacturer_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IManufacturer getManufacturer()
    {
        return manufacturer;
    }

    public void setManufacturer(IManufacturer manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    /*------------------
     * equal & hashcode
     ------------------*/

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }
    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
