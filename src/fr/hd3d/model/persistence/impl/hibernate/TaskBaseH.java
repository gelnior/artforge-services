package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.ITaskBase;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;


/**
 * @author michael.guiral
 * 
 */
@Entity
@Table(name = "hd3d_task_base")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public abstract class TaskBaseH extends BaseH implements ITaskBase
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String START_DATE_FIELD = "startDate";
    private static final String END_DATE_FIELD = "endDate";
    private Date startDate;
    private Date endDate;
    private Long duration;// estimated, not real
    private ITaskGroup taskGroup;// the taskGroup this belongs to
    private IExtraLine extraLine;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_taskbase"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public TaskBaseH()
    {
        super();
    }

    public TaskBaseH(Long id, java.sql.Timestamp version, Date startDate, Date endDate, Long duration,
            ITaskGroup taskGroup)
    {
        super(id, version);
        setStartDate(startDate);
        setEndDate(endDate);
        setTaskGroup(taskGroup);
        setDuration(duration);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "task_start_date")
    @org.hibernate.annotations.Index(name = "start_date_idx")
    public Date getStartDate()
    {
        return startDate;
    }

    @Column(name = "task_end_date")
    @org.hibernate.annotations.Index(name = "end_date_idx")
    public Date getEndDate()
    {
        return endDate;
    }

    @Column(name = "duration")
    @Min(value = 0)
    public Long getDuration()
    {
        return duration;
    }

    @ManyToOne(targetEntity = TaskGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "task_group_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public ITaskGroup getTaskGroup()
    {
        return taskGroup;
    }

    @ManyToOne(targetEntity = ExtraLineH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "task_extra_line_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IExtraLine getExtraLine()
    {
        return extraLine;
    }

    public void setExtraLine(IExtraLine extraLine)
    {
        this.extraLine = extraLine;
    }

    public void setStartDate(final Date startDate)
    {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public void setDuration(final Long duration)
    {
        this.duration = duration;
    }

    public void setTaskGroup(ITaskGroup taskGroup)
    {
        this.taskGroup = taskGroup;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((taskGroup == null) ? 0 : taskGroup.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        TaskBaseH other = (TaskBaseH) obj;
        if (endDate == null)
        {
            if (other.endDate != null)
                return false;
        }
        else if (!endDate.equals(other.endDate))
            return false;
        if (startDate == null)
        {
            if (other.startDate != null)
                return false;
        }
        else if (!startDate.equals(other.startDate))
            return false;
        if (taskGroup == null)
        {
            if (other.taskGroup != null)
                return false;
        }
        else if (!taskGroup.equals(other.taskGroup))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("startDate", startDate).append("endDate", endDate).append("duration", duration);
        b.append("taskGroupId", taskGroup == null ? "null" : taskGroup.getId());
        b.append("extraLineId", extraLine == null ? "null" : extraLine.getId());

        return b.toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class TaskBaseComplexTypeProvider implements IComplexTypeProvider
    {
        public Object getValue(String field, String value)
        {
            if (START_DATE_FIELD.equals(field) || END_DATE_FIELD.equals(field))
            {
                return fr.hd3d.utils.StringUtils.parseDate(value);
            }
            else
                return null;
        }

        public List<String> getValidEnum(String field)
        {
            return Collections.emptyList();
        }

        public boolean isValidTypeField(String field)
        {
            if (START_DATE_FIELD.equals(field) || END_DATE_FIELD.equals(field))
                return true;
            return false;
        }
    }
}
