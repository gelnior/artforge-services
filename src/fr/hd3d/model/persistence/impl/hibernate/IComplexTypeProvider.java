package fr.hd3d.model.persistence.impl.hibernate;

import java.util.List;


/**
 * Implemented by Persisted Objects to retrieve a concrete complex type (such as Enum, Date, etc.) value from a string.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 * 
 */
public interface IComplexTypeProvider
{
    /**
     * checks whether a field is a valid complex type
     * 
     * @param field
     *            field to check
     * @return true is the field is a valid complex type.
     */
    public boolean isValidTypeField(String field);

    /**
     * 
     * @param field
     *            complex type
     * @param value
     *            value of the complex type
     * @return complex type Enum
     */
    public Object getValue(String field, String value);

    /**
     * as a List of String
     * 
     * @param field
     * @return the values of the Enum corresponding to <i>field</i>
     */
    public List<String> getValidEnum(String field);
}
