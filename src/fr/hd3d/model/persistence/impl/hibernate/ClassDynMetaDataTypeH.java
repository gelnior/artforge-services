package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILClassDynMetaDataType;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Table(name = "hd3d_classdynmetadatatype")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ClassDynMetaDataTypeH extends BaseH implements IClassDynMetaDataType
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String name;

    private IDynMetaDataType dynMetaDataType;

    private String predicate = "*";// by default, apply to all

    private String className;// The class on which this DynMetaDataType applies on

    // additional field to store extra info to be used by caller in the way he wants (ex: store
    // project Id to reduce the scope of the metadatavalues)
    private String extra;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_classdynmetadatatype"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ClassDynMetaDataTypeH()
    {}

    public ClassDynMetaDataTypeH(Long id, Timestamp version, String name, IDynMetaDataType dynMetaDataType,
            String predicate, String className, String extra)
    {
        super(id, version);
        this.name = name;
        this.dynMetaDataType = dynMetaDataType;
        if (predicate != null)
        {
            this.predicate = predicate;
        }
        this.className = className;
        this.extra = extra;
    }

    public ClassDynMetaDataTypeH(String name, IDynMetaDataType dynMetaDataType, String predicate, String className,
            String extra)
    {
        this(null, null, name, dynMetaDataType, predicate, className, extra);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "classdynmetadata_name")
    public String getName()
    {
        return name;
    }

    @ManyToOne(targetEntity = DynMetaDataTypeH.class, fetch = FetchType.LAZY)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // @NotNull//TODO may cause exception when erasing DB ?
    @NotFound(action = NotFoundAction.IGNORE)
    public IDynMetaDataType getDynMetaDataType()
    {
        return dynMetaDataType;
    }

    @Column(name = "classdynmetadata_classname")
    public String getClassName()
    {
        return className;
    }

    @Column(name = "classdynmetadata_predicate")
    public String getPredicate()
    {
        return predicate;
    }

    @Column(name = "classdynmetadata_extra")
    public String getExtra()
    {
        return extra;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setDynMetaDataType(IDynMetaDataType dynmetadatetype)
    {
        this.dynMetaDataType = dynmetadatetype;
    }

    public void setClassName(String className)
    {
        this.className = className;
    }

    public void setPredicate(String predicate)
    {
        if (predicate != null)
            this.predicate = predicate;
    }

    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((className == null) ? 0 : className.hashCode());
        result = prime * result + ((dynMetaDataType == null) ? 0 : dynMetaDataType.hashCode());
        result = prime * result + ((extra == null) ? 0 : extra.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((predicate == null) ? 0 : predicate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ClassDynMetaDataTypeH other = (ClassDynMetaDataTypeH) obj;
        if (className == null)
        {
            if (other.className != null)
                return false;
        }
        else if (!className.equals(other.className))
            return false;
        if (dynMetaDataType == null)
        {
            if (other.dynMetaDataType != null)
                return false;
        }
        else if (!dynMetaDataType.equals(other.dynMetaDataType))
            return false;
        if (extra == null)
        {
            if (other.extra != null)
                return false;
        }
        else if (!extra.equals(other.extra))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (predicate == null)
        {
            if (other.predicate != null)
                return false;
        }
        else if (!predicate.equals(other.predicate))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    public IBaseTranslator<ILClassDynMetaDataType, IClassDynMetaDataType> defaultTranslator()
    {
        return Translators.classdynmetadata;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }
    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("name", name);
    // b.append("dynMetaDataTypeId", dynMetaDataType == null ? "null" : dynMetaDataType.getId());
    // b.append("predicate", predicate).append("className", className).append("extra", extra);
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
