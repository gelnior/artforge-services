package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;


/**
 * A34 time sheet implementation https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */

@Entity
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public abstract class ActivityH extends BaseH implements IActivity
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String FILLED_DATE_FIELD = "filledDate";
    private IPersonDay day;
    private String comment;
    private Long duration;// REAL DURATION, not estimated
    private Date filledDate;
    private IPerson filledBy;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_activity"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    ActivityH()
    {}

    /**
     * @param id
     * @param version
     * @param day
     * @param duration
     * @param comment
     */
    ActivityH(Long id, java.sql.Timestamp version, IPersonDay day, Long duration, String comment, IPerson filledBy,
            Date filledDate)
    {
        super(id, version);
        this.day = day;
        this.duration = duration;
        this.comment = comment;
        this.filledBy = filledBy;
        this.filledDate = filledDate;
    }

    ActivityH(IPersonDay day, Long duration, String comment, IPerson filledBy, Date filledDate)
    {
        this(null, null, day, duration, comment, filledBy, filledDate);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "activity_comment")
    @NotEmpty
    public String getComment()
    {
        if (StringUtils.isBlank(comment))
            return " ";
        return comment;
    }

    @Column(name = "activity_duration", nullable = false)
    @Min(value = 0)
    public Long getDuration()
    {
        return duration;
    }

    @ManyToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "activity_filled_by_id")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getFilledBy()
    {
        return this.filledBy;
    }

    @Column(name = "activity_filled_date", nullable = false)
    @Index(name = "activity_filled_date_idx")
    public Date getFilledDate()
    {
        return this.filledDate;
    }

    @ManyToOne(targetEntity = PersonDayH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "activity_day_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IPersonDay getDay()
    {
        return day;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setDuration(Long duration)
    {
        this.duration = duration;
    }

    public void setDay(IPersonDay day)
    {
        this.day = day;
    }

    public void setFilledBy(IPerson filledBy)
    {
        this.filledBy = filledBy;
    }

    public void setFilledDate(Date filledDate)
    {
        this.filledDate = filledDate;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((day == null) ? 0 : day.hashCode());
        result = prime * result + ((duration == null) ? 0 : duration.hashCode());
        result = prime * result + ((filledBy == null) ? 0 : filledBy.hashCode());
        result = prime * result + ((filledDate == null) ? 0 : filledDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ActivityH other = (ActivityH) obj;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (day == null)
        {
            if (other.day != null)
                return false;
        }
        else if (!day.equals(other.day))
            return false;
        if (duration == null)
        {
            if (other.duration != null)
                return false;
        }
        else if (!duration.equals(other.duration))
            return false;
        if (filledBy == null)
        {
            if (other.filledBy != null)
                return false;
        }
        else if (!filledBy.equals(other.filledBy))
            return false;
        if (filledDate == null)
        {
            if (other.filledDate != null)
                return false;
        }
        else if (!filledDate.equals(other.filledDate))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("dayId", day == null ? "null" : day.getId());
        b.append("duration", duration).append("filledDate", filledDate);
        b.append("filledById", filledBy == null ? "null" : filledBy.getId());
        return b.toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

    public static class ActivityComplexTypeProvider implements IComplexTypeProvider
    {
        public List<String> getValidEnum(String field)
        {
            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            return isValidTypeField(field) ? fr.hd3d.utils.StringUtils.parseDate(value) : null;
        }

        public boolean isValidTypeField(String field)
        {
            return FILLED_DATE_FIELD.equals(field);
        }
    }
}
