package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IResource;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.utils.CollectUtils;


/**
 * Resource hibernate object needed for database persistence.
 * 
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_resource")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public abstract class ResourceH extends BaseH implements IResource
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Set<IResourceGroup> resourceGroups;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_resource"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ResourceH()
    {}

    public ResourceH(Long id, Timestamp version, Set<IResourceGroup> resourceGroups)
    {
        super(id, version);
        this.resourceGroups = resourceGroups;
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToMany(targetEntity = ResourceGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_resourcegroup__resource", joinColumns = @JoinColumn(name = "resource_id"), inverseJoinColumns = @JoinColumn(name = "resourcegroup_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IResourceGroup> getResourceGroups()
    {
        if (this.resourceGroups == null)
        {
            this.resourceGroups = new HashSet<IResourceGroup>();
        }
        return this.resourceGroups;
    }

    public void setResourceGroups(Set<IResourceGroup> resourceGroups)
    {
        this.resourceGroups = resourceGroups;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        int result = super.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        for (IResourceGroup g : getResourceGroups())
        {
            g.getResources().remove(this);
        }
    }

    @Transient
    public Set<IProject> getProjects()
    {
        Set<IProject> projects = new HashSet<IProject>();
        for (IResourceGroup g : getResourceGroups())
            projects.addAll(g.getProjects());
        return projects;
    }

    @Transient
    public abstract String getResourceName();

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("resourceGroupIds", StringUtils.join(CollectUtils.getIds(getResourceGroups()), ','));

        return b.toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
