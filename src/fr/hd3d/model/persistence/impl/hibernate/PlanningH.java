package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


/**
 * @author michael.guiral
 * 
 */
@Entity
@Table(name = "hd3d_planning")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class PlanningH extends BaseH implements IPlanning
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String START_DATE_FIELD = "startDate";
    private static final String END_DATE_FIELD = "endDate";

    private String name;
    private IProject project;
    private List<ITaskGroup> taskGroups;
    private Boolean master = Boolean.FALSE;
    private Date startDate;
    private Date endDate;
    private List<ITaskChanges> taskChanges;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_planning"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public PlanningH()
    {}

    /**
     * @param id
     * @param version
     * @param name
     * @param project
     */
    public PlanningH(Long id, Timestamp version, String name, IProject project, Date startDate, Date endDate)
    {
        super(id, version);
        this.name = name;
        this.project = project;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public PlanningH(IPlanning planning)
    {
        super(planning.getId(), planning.getVersion());
        this.name = planning.getName();
        this.project = planning.getProject();
        this.endDate = planning.getEndDate();
        this.startDate = planning.getStartDate();
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "task_start_date")
    public Date getStartDate()
    {
        return startDate;
    }

    @Column(name = "task_end_date")
    public Date getEndDate()
    {
        return endDate;
    }

    @OneToMany(targetEntity = TaskChangesH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "planning")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @IndexColumn(name = "taskchanges_index", base = 0)
    @org.hibernate.annotations.OrderBy(clause = "taskchanges_index")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public List<ITaskChanges> getTaskChanges()
    {
        if (taskChanges == null)
            taskChanges = new ArrayList<ITaskChanges>();
        return taskChanges;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public void setTaskChanges(List<ITaskChanges> taskChanges)
    {
        if (taskChanges == null)
            this.taskChanges = new ArrayList<ITaskChanges>();

        // must set the index manually (seems hibernate does not work well)
        // note: do this first...
        else
        {
            for (ITaskChanges tc : taskChanges)
            {
                if (tc == null)
                    continue;
                if (tc.getIndex() == null)
                {
                    tc.setIndex(taskChanges.indexOf(tc));
                }
                tc.setPlanning(this);
            }
            // ...and let hibernate synchronize
            this.taskChanges = taskChanges;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.persistence.IPlanning#getName()
     */
    @Column(name = "planning_name")
    public String getName()
    {
        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.persistence.IPlanning#setName(java.lang.String)
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.persistence.IPlanning#getProject()
     */
    @ManyToOne(targetEntity = ProjectH.class, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "planning_project_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.hd3d.model.persistence.IPlanning#setProject(fr.hd3d.model.persistence.IProject)
     */
    public void setProject(IProject project)
    {
        this.project = project;
    }

    @OneToMany(targetEntity = TaskGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "planning")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    // @IndexColumn(name = "taskgroup_index", base = 0)
    @org.hibernate.annotations.OrderBy(clause = "taskgroup_index")
    @Fetch(value = FetchMode.SELECT)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public List<ITaskGroup> getTaskGroups()
    {
        if (taskGroups == null)
            taskGroups = new ArrayList<ITaskGroup>();
        return taskGroups;
    }

    public void setTaskGroups(List<ITaskGroup> taskGroups)
    {
        if (taskGroups == null)
            this.taskGroups = new ArrayList<ITaskGroup>();

        // must set the index manually (seems hibernate does not work well)
        // note: do this first...
        else
        {
            for (ITaskGroup tg : taskGroups)
            {
                if (tg == null)
                    continue;
                if (tg.getIndex() == null)
                {
                    tg.setIndex(taskGroups.indexOf(tg));
                }
                tg.setPlanning(this);
            }
            // ...and let hibernate synchronize
            this.taskGroups = taskGroups;
        }
    }

    public IBaseTranslator<ILPlanning, IPlanning> defaultTranslator()
    {
        return Translators.planning;
    }

    @Column(name = "planning_ismaster")
    public Boolean isMaster()
    {
        return master;
    }

    public void setMaster(Boolean master)
    {
        if (master == null)
            this.master = Boolean.FALSE;
        this.master = master;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (master ? 1231 : 1237);
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PlanningH other = (PlanningH) obj;
        if (!master.equals(other.master))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        // cascade disable
        Collection<ITaskGroup> taskGroups = CollectUtils.selectNotNull(getTaskGroups());
        Persistors.taskGroup.disableCollection(taskGroups);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("name", name);
    // b.append("projectId", project == null ? "null" : project.getId());
    // b.append("taskGroupIds", StringUtils.join(CollectUtils.getIds(getTaskGroups()), ','));
    // b.append("master", master).append("startDate", startDate).append("endDate", endDate);
    // b.append("taskChangeIds", StringUtils.join(CollectUtils.getIds(getTaskChanges()), ','));
    //
    // return b.toString();
    // }

    public void addTaskGroup(ITaskGroup taskGroup)
    {
        if (!getTaskGroups().contains(taskGroup))
        {
            getTaskGroups().add(taskGroup);
            // must set the index manually (seems hibernate does not work well)
            taskGroup.setIndex(getTaskGroups().indexOf(taskGroup));
        }
    }

    public void addTaskChanges(ITaskChanges taskChanges)
    {
        if (!getTaskChanges().contains(taskChanges))
        {
            getTaskChanges().add(taskChanges);
            // must set the index manually (seems hibernate does not work well)
            taskChanges.setIndex(getTaskChanges().indexOf(taskChanges));
        }
    }

    /**
     * To work properly planning object need that its stard and end date to be set. So if one of them this method set
     * defautl date : current date for start date and start date + 3 months for end date.
     */
    public void cleanDates() throws Hd3dException
    {
        if (this.getStartDate() == null || this.getEndDate() == null)
        {
            this.setStartDate(new Date());
            this.setEndDate(DateUtils.addMonths(this.getStartDate(), 3));
            Persistors.planning.persist(this);
        }

    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class PlanningComplexTypeProvider implements IComplexTypeProvider
    {
        public Object getValue(String field, String value)
        {
            if (START_DATE_FIELD.equals(field) || END_DATE_FIELD.equals(field))
            {
                return fr.hd3d.utils.StringUtils.parseDate(value);
            }
            else
                return null;
        }

        public List<String> getValidEnum(String field)
        {
            return Collections.emptyList();
        }

        public boolean isValidTypeField(String field)
        {
            if (START_DATE_FIELD.equals(field) || END_DATE_FIELD.equals(field))
                return true;
            return false;
        }
    }
}
