package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.common.client.enums.EScreenQualification;
import fr.hd3d.common.client.enums.EScreenType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILScreen;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.IScreenModel;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.EnumUtils;


@Entity
@Table(name = "hd3d_screen")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ScreenH extends InventoryItemH implements IScreen
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String TYPE_FIELD = "type";
    private static final String QUALIFICATION_FIELD = "qualification";

    private Float size;
    private EScreenType type;
    private EScreenQualification qualification;
    private IScreenModel screenModel;
    private Set<IComputer> computers;

    /*-------------
     * Constructors
     -------------*/
    public ScreenH()
    {}

    public ScreenH(Long id, Timestamp version, String name, String serial, String billingReference, Date purchaseDate,
            Date warrantyEnd, EInventoryStatus inventoryStatus, Float size, EScreenType type,
            EScreenQualification qualification, IScreenModel screenModel, Set<IComputer> computers,
            Set<IResourceGroup> resourceGroups)
    {
        super(id, version, name, serial, billingReference, purchaseDate, warrantyEnd, inventoryStatus, resourceGroups);

        this.size = size;
        this.type = type;
        this.qualification = qualification;
        this.screenModel = screenModel;
        this.computers = computers;
    }

    public ScreenH(String name, String serial, String billingReference, Date purchaseDate, Date warrantyEnd,
            EInventoryStatus inventoryStatus, Float size, EScreenType type, EScreenQualification qualification,
            IScreenModel screenModel, Set<IComputer> computers, Set<IResourceGroup> resourceGroups)
    {
        this(null, null, name, serial, billingReference, purchaseDate, warrantyEnd, inventoryStatus, size, type,
                qualification, screenModel, computers, resourceGroups);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "screen_size")
    public Float getSize()
    {
        return size;
    }

    public void setSize(Float size)
    {
        this.size = size;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "screen_type", nullable = false)
    public EScreenType getType()
    {
        return type;
    }

    public void setType(EScreenType type)
    {
        this.type = type;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "screen_qualification", nullable = false)
    public EScreenQualification getQualification()
    {
        return qualification;
    }

    public void setQualification(EScreenQualification qualification)
    {
        this.qualification = qualification;
    }

    @ManyToOne(targetEntity = ScreenModelH.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "screen_model_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IScreenModel getScreenModel()
    {
        return this.screenModel;
    }

    public void setScreenModel(IScreenModel screenModel)
    {
        this.screenModel = screenModel;
    }

    @ManyToMany(targetEntity = ComputerH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_computer__screen", joinColumns = @JoinColumn(name = "screen_id"), inverseJoinColumns = @JoinColumn(name = "computer_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IComputer> getComputers()
    {
        if (this.computers == null)
        {
            this.computers = new HashSet<IComputer>();
        }

        return this.computers;
    }

    public void setComputers(Set<IComputer> computers)
    {
        this.computers = computers;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((computers == null) ? 0 : computers.hashCode());
        result = prime * result + ((qualification == null) ? 0 : qualification.hashCode());
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ScreenH other = (ScreenH) obj;
        if (computers == null)
        {
            if (other.computers != null)
                return false;
        }
        else if (!computers.equals(other.computers))
            return false;
        if (qualification == null)
        {
            if (other.qualification != null)
                return false;
        }
        else if (!qualification.equals(other.qualification))
            return false;
        if (size == null)
        {
            if (other.size != null)
                return false;
        }
        else if (!size.equals(other.size))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        for (IComputer c : getComputers())
        {
            c.getDevices().remove(this);
        }
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILScreen, IScreen> defaultTranslator()
    {
        return Translators.screen;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("size", size).append("type", type).append("qualification", qualification);
    // b.append("screenModelId", screenModel == null ? "null" : screenModel.getId());
    // b.append("computerIds", StringUtils.join(CollectUtils.getIds(getComputers()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class ScreenComplexTypeProvider extends InventoryItemComplexTypeProvider
    {

        public Object getValue(String field, String value)
        {
            Object ret = super.getValue(field, value);
            if (ret != null)
                return ret;

            if (TYPE_FIELD.equals(field))
            {
                return EnumUtils.getEnumValueIgnoreCase(EScreenType.class, value);
            }
            if (QUALIFICATION_FIELD.equals(field))
            {
                return EnumUtils.getEnumValueIgnoreCase(EScreenQualification.class, value);
            }
            else
            {
                return null;
            }
        }

        public List<String> getValidEnum(String field)
        {
            List<String> ret = super.getValidEnum(field);
            if (CollectionUtils.isNotEmpty(ret))
                return ret;

            if (TYPE_FIELD.equals(field))
                return EnumUtils.getEnumStringValues(EScreenType.class);
            if (QUALIFICATION_FIELD.equals(field))
                return EnumUtils.getEnumStringValues(EScreenQualification.class);

            return super.getValidEnum(field);
        }

        public boolean isValidTypeField(String field)
        {
            return TYPE_FIELD.equals(field) || QUALIFICATION_FIELD.equals(field);
        }
    }
}
