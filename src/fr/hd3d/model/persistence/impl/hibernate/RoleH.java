package fr.hd3d.model.persistence.impl.hibernate;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.collections15.CollectionUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.AuthorizationUtil;
import fr.hd3d.services.security.utils.RESTPathUtil;


@Entity
@Table(name = "hd3d_role")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class RoleH extends BaseH implements IRole
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String name;
    private Set<IResourceGroup> resourceGroups;
    private Set<String> permissions;
    private Set<String> bans;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_role"), @Parameter(name = "initial_value", value = "1"),
            @Parameter(name = "increment_size", value = "1"), @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public RoleH()
    {}

    public RoleH(Long id, java.sql.Timestamp version, String name, Set<IResourceGroup> resourceGroups,
            Set<String> permissions, Set<String> bans)
    {
        super(id, version);
        this.setName(name);
        this.resourceGroups = resourceGroups;
        this.permissions = permissions;
        this.bans = bans;
    }

    public RoleH(String name, Set<IResourceGroup> groups, Set<String> permissions, Set<String> bans)
    {
        this(null, null, name, groups, permissions, bans);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "role_name", nullable = false)
    @NotEmpty
    public String getName()
    {
        return name;
    }

    @ManyToMany(targetEntity = ResourceGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_resourcegroup__role", joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "resourcegroup_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IResourceGroup> getResourceGroups()
    {
        if (resourceGroups == null)
            resourceGroups = new HashSet<IResourceGroup>();
        return resourceGroups;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setResourceGroups(Set<IResourceGroup> groups)
    {
        this.resourceGroups = groups;
    }

    /*
     * Note: do not switch to @ElementsCollections since it is JPA and mixing with @column (hibernate specific) is no
     * good.
     * 
     * @ElementsCollections creates its own columns and @column creates others
     */
    @CollectionOfElements
    @Column(name = "role_permissions")
    public Set<String> getPermissions()
    {
        if (permissions == null)
            permissions = new LinkedHashSet<String>();
        return permissions;
    }

    public void setPermissions(Set<String> permissions)
    {
        this.permissions = permissions;
    }

    /*
     * Note: do not switch to @ElementsCollections since it is JPA and mixing with @column (hibernate specific) is no
     * good.
     * 
     * @ElementsCollections creates its own columns and @column creates others
     */
    @CollectionOfElements
    @Column(name = "role_bans")
    public Set<String> getBans()
    {
        if (bans == null)
            bans = new LinkedHashSet<String>();
        return bans;
    }

    public void setBans(Set<String> bans)
    {
        this.bans = bans;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        RoleH other = (RoleH) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        for (IResourceGroup resourceGroup : getResourceGroups())
        {
            resourceGroup.getRoles().remove(this);
        }
    }

    public void add(IResourceGroup group)
    {
        getResourceGroups().add(group);
    }

    public void add(String permission)
    {
        getPermissions().add(permission);
    }

    public void addBan(String ban)
    {
        getBans().add(ban);
    }

    public IBaseTranslator<ILRole, IRole> defaultTranslator()
    {
        return Translators.role;
    }

    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public boolean canBeUpdatedFrom(ILRole lRole)
    {
        boolean result = true;
        List<String> lPerms = lRole.getPermissions();
        if (lPerms != null)
        {
            /* symmetric difference */
            List<String> perms = (List<String>) CollectionUtils.disjunction(lPerms, getPermissions());
            result = AuthorizationUtil.canAll(perms);
        }
        List<String> lBans = lRole.getBans();
        if (lBans != null)
        {
            /* symmetric difference */
            List<String> bans = (List<String>) CollectionUtils.disjunction(lBans, getBans());
            result &= AuthorizationUtil.canAll(bans);
        }
        return result;
    }

    public String toString()
    {
        return name;
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
