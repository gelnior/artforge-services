package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILMountPoint;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * Hibernate class to manage mount points according the operating system.
 * 
 * @author HD3D
 * 
 */

@Entity
@Table(name = "hd3d_mount_point")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class MountPointH extends BaseH implements IMountPoint
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String operatingSystem;
    private String mountPointPath;
    private String name;

    public MountPointH()
    {}

    public MountPointH(final String storageName, final String OperatingSystem, final String mountPointPath)
    {
        super();
        this.name = storageName;
        this.operatingSystem = OperatingSystem;
        this.mountPointPath = mountPointPath;
    }

    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_mount_point"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    @Column(name = "mount_point_storage_name")
    public String getName()
    {
        return name;
    }

    @Column(name = "mount_point_os")
    public String getOperatingSystem()
    {
        return operatingSystem;
    }

    @Column(name = "mount_point_path")
    public String getMountPointPath()
    {
        return mountPointPath;
    }

    public void setName(final String storageName)
    {
        this.name = storageName;
    }

    public void setOperatingSystem(final String os)
    {
        this.operatingSystem = os;
    }

    public void setMountPointPath(final String mountPointPath)
    {
        this.mountPointPath = mountPointPath;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((mountPointPath == null) ? 0 : mountPointPath.hashCode());
        result = prime * result + ((operatingSystem == null) ? 0 : operatingSystem.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;
        MountPointH other = (MountPointH) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (operatingSystem == null)
        {
            if (other.operatingSystem != null)
                return false;
        }
        else if (!operatingSystem.equals(other.operatingSystem))
            return false;
        if (mountPointPath == null)
        {
            if (other.mountPointPath != null)
                return false;
        }
        else if (!mountPointPath.equals(other.mountPointPath))
            return false;
        if (version == null)
        {
            if (other.version != null)
                return false;
        }
        else if (!version.equals(other.version))
            return false;
        return true;
    }

    public String toString()
    {
        return new ToStringBuilder(this).append("id", id).append("version", version).append("storageName", name)
                .append("operatingSystem", operatingSystem).append("mountPointPath", mountPointPath).toString();
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILMountPoint, IMountPoint> defaultTranslator()
    {
        return Translators.mountpoint;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public <T extends IBase> void merge(Collection<T> coll, Collection<Long> ids, Class<T> clazz) throws Hd3dException
    {
    // TODO Auto-generated method stub

    }

}
