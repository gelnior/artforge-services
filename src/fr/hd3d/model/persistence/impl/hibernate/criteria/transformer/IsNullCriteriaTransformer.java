package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.services.resources.ResourceContext;


public class IsNullCriteriaTransformer extends AbstractQueryTransformer<Criteria, Criteria>
{
    String field = null;

    public IsNullCriteriaTransformer(String field)
    {
        super();
        this.field = field;
    }

    @SuppressWarnings("unchecked")
    public IsNullCriteriaTransformer(ResourceContext context, String field)
    {
        super(context);
        this.field = field;
    }

    public Criteria transform(Criteria query)
    {
        if (field != null)
        {
            query.add(Restrictions.isNull(field));
        }
        return query;
    }
}
