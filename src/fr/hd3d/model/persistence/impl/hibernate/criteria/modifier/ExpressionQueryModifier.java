package fr.hd3d.model.persistence.impl.hibernate.criteria.modifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.list.SetUniqueList;
import org.apache.shiro.util.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.CompositionH;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryModifiersList;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.StringUtils;


/**
 * Handles constraints such as "greater than", "equal", etc...
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ExpressionQueryModifier implements IExpressionQueryModifier
{
    private final Criterion expression;
    private List<IQueryModifier> aliases;

    public ExpressionQueryModifier(final Criterion expression, final List<IQueryModifier> aliases)
    {
        this.expression = expression;
        this.aliases = aliases;
    }

    public Criterion getExpression()
    {
        return this.expression;
    }

    public List<IQueryModifier> getAliases()
    {
        if (aliases == null)
        {
            aliases = new ArrayList<IQueryModifier>();
        }
        return aliases;
    }

    /**
     * handles join attributes in queries. Join attributes are passed in dot notation: attribute1.attribute2.... Due to
     * the use of hibernate Criteria to query, aliases must be created. For values such as: column =
     * attribute1.attribute2.attribute3 the following aliases are created: "attribute1",alias
     * "attribute1_attribute2","attribute1_attribute2_attribute3"
     * 
     * @param persisted
     * @param column
     *            the attributes to query (ex: attribute1.attribute2...
     * @return list of CreateAliasQueryModifier to create
     */
    static public List<IQueryModifier> createAliases(QueryModifiersList allModifiers, final Class<?> persisted,
            final String column)
    {
        if (allModifiers == null)
            allModifiers = new QueryModifiersList();

        SetUniqueList<IQueryModifier> uniqueAliasesToCreate = null;

        /*--------------------------------
         * Check if alias creation needed
         ---------------------------------*/
        if (persisted != null && column != null)
        {
            /* split the attributes: attr1.attr2.attr3... */
            final List<String> attributes = Arrays.asList(org.apache.commons.lang.StringUtils.split(column, "."));

            /* do not create alias in these cases (may cause issues in query , such as "isnull" query): */
            /* - there is only one attribute */
            /* - attribute is like "attribute.id" && attribute is NOT a collection */
            final int nbOfAttributes = attributes.size();

            // final String lastAttribute = org.apache.commons.lang.StringUtils.substringAfterLast(column, ".");
            final String lastAttribute = CollectUtils.getLastOrNull(attributes);

            Class<?> firstAttributeType = EntitiesMaps.withClass(persisted).getTypeOf(
                    CollectUtils.getFirstOrNull(attributes));

            final boolean dontCreateAlias = nbOfAttributes <= 1
                    || (nbOfAttributes == 2 && Const.ID.equals(lastAttribute) && !Collection.class
                            .isAssignableFrom(firstAttributeType));

            if (dontCreateAlias)
            {
                return uniqueAliasesToCreate;
            }

            /*-----------------
             * Alias creation
             -----------------*/
            uniqueAliasesToCreate = SetUniqueList.decorate(new ArrayList<IQueryModifier>());
            /*
             * Pointer to the class currently being treated.the first attr1 belongs to this class, attr2 belongs to some
             * others.
             */
            Class<?> currentAttributeClass = persisted;
            String previousAttributeAlias = null;
            for (int i = 0; i < nbOfAttributes; i++)
            {
                final String currentAttribute = attributes.get(i);

                /* check if the attribute is an association in an entity class */
                Class<?> associationTargetEntity = EntitiesMaps.withClass(currentAttributeClass)
                        .getTypeOfAssociationTarget(currentAttribute);

                if (associationTargetEntity == null)
                {
                    continue;
                }

                CreateAliasQueryModifier newAlias = null;
                CreateAliasQueryModifier existingCreateAliasQueryModifier;

                /* associationPath= <previous alias>.<current attribute> */
                /* alias= <previous alias>_<current attribute> */
                final String currentAssociationPath;
                if (previousAttributeAlias == null)
                {
                    /* first attribute */
                    currentAssociationPath = currentAttribute;
                }
                else
                {
                    /* following attributes */
                    currentAssociationPath = new StringBuilder(previousAttributeAlias).append('.')
                            .append(currentAttribute).toString();
                }

                /* is there already an existing CreateAliasQueryModifier for the current attribute ? */
                existingCreateAliasQueryModifier = allModifiers.existingAssociationPathsToAliasMap
                        .get(currentAssociationPath);

                String currentAliasStr;
                /* No, create it */
                if (existingCreateAliasQueryModifier == null)
                {
                    currentAliasStr = currentAssociationPath.replace('.', '_');
                    newAlias = new CreateAliasQueryModifier(currentAssociationPath, currentAliasStr,
                            CriteriaSpecification.LEFT_JOIN);
                    allModifiers.existingAssociationPathsToAliasMap.put(currentAssociationPath, newAlias);
                    previousAttributeAlias = currentAliasStr;
                    CollectUtils.addIfNotNull(uniqueAliasesToCreate, newAlias);
                }
                /* Yes, use it */
                else
                {
                    if (i == 0)
                    {
                        currentAliasStr = existingCreateAliasQueryModifier.getAlias();
                        newAlias = existingCreateAliasQueryModifier;
                    }
                    else
                    {
                        currentAliasStr = existingCreateAliasQueryModifier.getAssociationPath().replace('.', '_');
                        newAlias = new CreateAliasQueryModifier(currentAssociationPath, currentAliasStr,
                                CriteriaSpecification.LEFT_JOIN);
                        allModifiers.existingAssociationPathsToAliasMap.put(currentAssociationPath, newAlias);
                        CollectUtils.addIfNotNull(uniqueAliasesToCreate, newAlias);
                    }

                    previousAttributeAlias = currentAliasStr;
                }

                currentAttributeClass = associationTargetEntity;// go to next attribute
            }
        }

        return uniqueAliasesToCreate;
    }

    /**
     * Apply a "<b>greater than</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return a Criteria modifier
     */
    static public IExpressionQueryModifier getGtInstance(final QueryModifiersList allModifiers,
            final Class<?> persisted, final String column, final Object value)
    {
        final List<IQueryModifier> aliases = createAliases(allModifiers, persisted, column);
        final String col = updateColumn(column, aliases);
        return new ExpressionQueryModifier(Restrictions.gt(col, StringUtils.narrow(persisted, value, column)), aliases);
    }

    /**
     * Apply a "<b>lesser than</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getLtInstance(final QueryModifiersList allModifiers,
            final Class<?> persisted, final String column, final Object value)
    {
        final List<IQueryModifier> aliases = createAliases(allModifiers, persisted, column);
        final String col = updateColumn(column, aliases);
        return new ExpressionQueryModifier(Restrictions.lt(col, StringUtils.narrow(persisted, value, column)), aliases);
    }

    /**
     * Apply a "<b>greater than or equal</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getGeInstance(final QueryModifiersList allModifiers,
            final Class<?> persisted, final String column, final Object value)
    {
        final List<IQueryModifier> aliases = createAliases(allModifiers, persisted, column);
        final String col = updateColumn(column, aliases);
        return new ExpressionQueryModifier(Restrictions.ge(col, StringUtils.narrow(persisted, value, column)), aliases);
    }

    /**
     * Apply a "<b>lesser than or equal</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getLeInstance(final QueryModifiersList allModifiers,
            final Class<?> persisted, final String column, final Object value)
    {
        final List<IQueryModifier> aliases = createAliases(allModifiers, persisted, column);
        final String col = updateColumn(column, aliases);
        return new ExpressionQueryModifier(Restrictions.le(col, StringUtils.narrow(persisted, value, column)), aliases);
    }

    /**
     * Apply a "<b>equal</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getEqInstance(final QueryModifiersList allModifiers,
            final Class<?> persisted, final String column, final Object value)
    {
        final List<IQueryModifier> aliases = createAliases(allModifiers, persisted, column);
        final String col = updateColumn(column, aliases);
        return new ExpressionQueryModifier(Restrictions.eq(col, StringUtils.narrow(persisted, value, column)), aliases);
    }

    /**
     * Apply a "<b>not equal</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getNeqInstance(final QueryModifiersList allModifiers,
            final Class<?> persisted, final String column, final Object value)
    {
        final List<IQueryModifier> aliases = createAliases(allModifiers, persisted, column);
        final String col = updateColumn(column, aliases);
        return new ExpressionQueryModifier(Restrictions.ne(col, StringUtils.narrow(persisted, value, column)), aliases);
    }

    /**
     * Apply a "<b>between</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getBetweenInstance(final QueryModifiersList allModifiers,
            final Class<?> persisted, final String column, final Object lo, Object hi)
    {
        final List<IQueryModifier> aliases = createAliases(allModifiers, persisted, column);
        final String col = updateColumn(column, aliases);
        return new ExpressionQueryModifier(Restrictions.between(col, StringUtils.narrow(persisted, lo, column),
                StringUtils.narrow(persisted, hi, column)), aliases);
    }

    /**
     * Apply a "<b>in</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getInInstance(final QueryModifiersList allModifiers,
            final Class<?> persisted, final String column, final Collection<?> set)
    {
        final List<IQueryModifier> aliases = createAliases(allModifiers, persisted, column);
        final String col = updateColumn(column, aliases);
        return new ExpressionQueryModifier(Restrictions.in(col, set), aliases);
    }

    /**
     * Apply a "<b>not in</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getNotInInstance(final QueryModifiersList allModifiers,
            final Class<?> persisted, final String column, final Collection<?> set)
    {
        final List<IQueryModifier> aliases = createAliases(allModifiers, persisted, column);
        final String col = updateColumn(column, aliases);
        return new ExpressionQueryModifier(Restrictions.not(Restrictions.in(col, set)), aliases);
    }

    /**
     * Apply a "<b>is null</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getIsNullInstance(final QueryModifiersList allModifiers,
            final Class<?> persisted, final String column)
    {
        final List<IQueryModifier> aliases = createAliases(allModifiers, persisted, column);
        final String col = updateColumn(column, aliases);
        return new ExpressionQueryModifier(Restrictions.isNull(col), aliases);
    }

    /**
     * Apply case-insensitive "<b>like</b>", similar to Postgres ilike operator.
     * 
     * @param column
     * @return
     * @param value
     */
    public static IExpressionQueryModifier getLikeInstance(final QueryModifiersList allModifiers,
            final Class<?> persisted, final String column, final Object value)
    {
        final List<IQueryModifier> aliases = createAliases(allModifiers, persisted, column);
        final String col = updateColumn(column, aliases);
        return new ExpressionQueryModifier(Restrictions.ilike(col, value), aliases);
    }

    /**
     * Apply a "<b>is not null</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getIsNotNullInstance(final QueryModifiersList allModifiers,
            final Class<?> persisted, final String column)
    {
        final List<IQueryModifier> aliases = createAliases(allModifiers, persisted, column);
        final String col = updateColumn(column, aliases);
        return new ExpressionQueryModifier(Restrictions.isNotNull(col), aliases);
    }

    static public IExpressionQueryModifier getInSequenceShotsInstance(final Object sequenceId) throws Hd3dException
    {
        List<IShot> shots = Persistors.shot.getByValue(null, "sequence.id", sequenceId);
        Collection<Object> constituentIds = new ArrayList<Object>();
        for (IShot shot : shots)
        {
            List<IComposition> compositions = Persistors.composition.getByValue(null, "shot.id", shot.getId());
            for (IComposition composition : compositions)
            {
                constituentIds.add(composition.getConstituent().getId());
            }
        }
        return new ExpressionQueryModifier(Restrictions.in("id", constituentIds), null);
    }

    static public IExpressionQueryModifier getInSequenceInstance(final Object sequenceId)
            throws Hd3dPersistenceException
    {
        IExpressionQueryModifier ret = null;

        if (sequenceId != null)
        {
            final DetachedCriteria subQuery = DetachedCriteria.forClass(CompositionH.class, "composition");
            subQuery.setProjection(Projections.distinct(Projections.id()));
            subQuery.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            subQuery.add(Restrictions.eq("sequence.id", sequenceId));
            subQuery.setProjection(Projections.property("constituent.id"));

            ret = new SubQueryPropertyInQueryModifier(Subqueries.propertyIn(Const.ID, subQuery));
        }

        return ret;
    }

    static public IExpressionQueryModifier getHasTagInstance(final Object tagId)
    {
        IExpressionQueryModifier ret = null;

        if (tagId != null)
        {
            final DetachedCriteria subQuery = getTagInstanceSubQuery(tagId);
            if (subQuery != null)
            {
                ret = new SubQueryPropertyInQueryModifier(Subqueries.propertyIn(Const.ID, subQuery));
            }
        }

        return ret;
    }

    static public IExpressionQueryModifier getHasNotTagInstance(final Object tagId)
    {
        IExpressionQueryModifier ret = null;

        if (tagId != null)
        {
            final DetachedCriteria subQuery = getTagInstanceSubQuery(tagId);
            if (subQuery != null)
            {
                ret = new SubQueryPropertyInQueryModifier(Subqueries.propertyNotIn(Const.ID, subQuery));
            }
        }

        return ret;
    }

    static private DetachedCriteria getTagInstanceSubQuery(final Object tagId)
    {
        DetachedCriteria subQuery = null;

        if (tagId != null)
        {
            subQuery = DetachedCriteria.forClass(TagParent.class, "tagParent");
            subQuery.setProjection(Projections.distinct(Projections.id()));
            subQuery.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            subQuery.add(Restrictions.eq("tag.id", tagId));
            subQuery.setProjection(Projections.property("parent"));
        }

        return subQuery;
    }

    public static String updateColumn(final String column, final List<IQueryModifier> aliases)
    {
        String ret;

        if (CollectionUtils.isEmpty(aliases))
        {
            ret = column;
        }
        else
        {
            ret = ((CreateAliasQueryModifier) CollectUtils.getLastOrNull(aliases)).getAlias() + '.'
                    + org.apache.commons.lang.StringUtils.substringAfterLast(column, ".");
        }

        return ret;
    }

    public void applyModifier(final Criteria query) throws Hd3dQueryFilterException
    {
        if (query != null)
        {
            query.add(expression);
        }
    }

    public void applyModifier(final DetachedCriteria query) throws Hd3dQueryFilterException
    {
        // TODO Auto-generated method stub
    }

    public void applyModifierForCount(Criteria query) throws Hd3dQueryFilterException
    {
        applyModifier(query);
    }

    public void applyModifierForCount(DetachedCriteria query) throws Hd3dQueryFilterException
    {}

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((aliases == null) ? 0 : aliases.hashCode());
        result = prime * result + ((expression == null) ? 0 : expression.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ExpressionQueryModifier other = (ExpressionQueryModifier) obj;
        if (aliases == null)
        {
            if (other.aliases != null)
                return false;
        }
        else if (!aliases.equals(other.aliases))
            return false;
        if (expression == null)
        {
            if (other.expression != null)
                return false;
        }
        else if (!expression.equals(other.expression))
            return false;
        return true;
    }

    public String toString()
    {
        String ret = expression.toString();
        if (CollectUtils.isNotEmpty(getAliases()))
        {
            ret += ", aliases=";
            for (IQueryModifier alias : getAliases())
            {
                ret += alias.toString() + ",";
            }
        }
        return ret;
    }
}
