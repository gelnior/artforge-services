package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import org.hibernate.Criteria;

import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.HibernateUtil;


public class AliasCriteriaTransformer extends AbstractQueryTransformer<Criteria, Criteria>
{
    String field = null;
    String alias = null;

    public AliasCriteriaTransformer(String field, String alias)
    {
        super();
        this.field = field;
        this.alias = alias;
    }

    public AliasCriteriaTransformer(ResourceContext<?, ?> context, String field, String alias)
    {
        super(context);
        this.field = field;
        this.alias = alias;
    }

    public Criteria transform(Criteria criteria)
    {
        if (field != null && alias != null)
        {
            if (!HibernateUtil.aliasExist(criteria, alias))
            {
                criteria.createAlias(field, alias);
            }
        }
        return criteria;
    }
}
