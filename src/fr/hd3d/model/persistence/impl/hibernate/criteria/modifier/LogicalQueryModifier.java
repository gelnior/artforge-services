package fr.hd3d.model.persistence.impl.hibernate.criteria.modifier;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;
import fr.hd3d.utils.CollectUtils;


/**
 * Handles binary logical expressions("and", "or", "not")
 * 
 * @author Try LAM
 */
public class LogicalQueryModifier implements IExpressionQueryModifier
{
    private final Criterion expression;

    public LogicalQueryModifier(final Criterion expression)
    {
        this.expression = expression;
    }

    public List<IQueryModifier> getAliases()
    {
        return null;
    }

    public Criterion getExpression()
    {
        return this.expression;
    }

    public void applyModifier(final Criteria query) throws Hd3dQueryFilterException
    {
        if (query != null)
        {
            query.add(expression);
        }
    }

    static public IExpressionQueryModifier getORinstance(final Criterion crit1, final Criterion crit2)
    {
        return new LogicalQueryModifier(Restrictions.or(crit1, crit2));
    }

    static public IExpressionQueryModifier getANDinstance(final Criterion crit1, final Criterion crit2)
    {
        return new LogicalQueryModifier(Restrictions.and(crit1, crit2));
    }

    static public IExpressionQueryModifier getNOTinstance(final Criterion crit)
    {
        return new LogicalQueryModifier(Restrictions.not(crit));
    }

    static public IExpressionQueryModifier getFileAttributeORinstance(final Criterion crit0, final Criterion crit1,
            final Criterion crit2, final Criterion crit3)
    {
        return new LogicalQueryModifier(Restrictions.or(Restrictions.and(crit0, crit1), Restrictions.and(crit2, crit3)));
    }

    static public IExpressionQueryModifier getFileAttributeANDinstance(final Criterion crit0, final Criterion crit1,
            final Criterion crit2, final Criterion crit3)
    {
        return new LogicalQueryModifier(
                Restrictions.and(Restrictions.and(crit0, crit1), Restrictions.and(crit2, crit3)));
    }

    static public IExpressionQueryModifier getFileAttributeNOTinstance(final Criterion crit0, final Criterion crit1)
    {
        return new LogicalQueryModifier(Restrictions.not(Restrictions.and(crit0, crit1)));
    }

    static public IExpressionQueryModifier getConjonctionInstance(
            final List<IExpressionQueryModifier> expressionQueryModifiers)
    {
        return getCriterion(expressionQueryModifiers, Restrictions.conjunction());
    }

    static public IExpressionQueryModifier getDisjonctionInstance(
            final List<IExpressionQueryModifier> expressionQueryModifiers)
    {
        return getCriterion(expressionQueryModifiers, Restrictions.disjunction());
    }

    private static IExpressionQueryModifier getCriterion(final List<IExpressionQueryModifier> expressionQueryModifiers,
            final Junction expression)
    {
        IExpressionQueryModifier ret = null;

        CollectUtils.removeNull(expressionQueryModifiers);
        if (CollectUtils.isNotEmpty(expressionQueryModifiers) && expression != null)
        {
            final List<Criterion> criterions = new ArrayList<Criterion>();

            for (IExpressionQueryModifier modifier : expressionQueryModifiers)
            {
                criterions.add(modifier.getExpression());
            }

            for (int i = 0; i < criterions.size(); i++)
            {
                expression.add(criterions.get(i));
            }
            ret = new LogicalQueryModifier(expression);
        }

        return ret;
    }

    public void applyModifier(final DetachedCriteria query) throws Hd3dQueryFilterException
    {
        // TODO Auto-generated method stub
    }

    public void applyModifierForCount(Criteria query) throws Hd3dQueryFilterException
    {
        applyModifier(query);
    }

    public void applyModifierForCount(DetachedCriteria query) throws Hd3dQueryFilterException
    {}

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((expression == null) ? 0 : expression.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LogicalQueryModifier other = (LogicalQueryModifier) obj;
        if (expression == null)
        {
            if (other.expression != null)
                return false;
        }
        else if (!expression.equals(other.expression))
            return false;
        return true;
    }

}
