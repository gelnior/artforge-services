package fr.hd3d.model.persistence.impl.hibernate.criteria.parser;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.client.enums.EDynMetaDataOperator;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.IComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryModifiersList;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.LogicalQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_Btw;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_Eq;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_Ge;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_Gt;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_HasNotTag;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_HasTag;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_In;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_InSequence;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_InSequenceShots;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_IsNotNull;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_IsNull;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_Le;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_Like;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_Lt;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_Neq;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.ExpressionExpressionQueryModifier_NotIn;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Btw;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Eq;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Ge;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Gt;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_In;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_IsNotNull;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_IsNull;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Le;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Like;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Lt;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Neq;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_NotIn;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.CollectUtils;


/**
 * Parse the query to build modifiers for the following constraints :<br />
 * "lesser than", "greater than", "equal", "greater than or equal", "lesser than or equal" , "not equal" , "between",
 * "in", "not in", "like", "is null", "is not null" <br />
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ConstraintQueryParser extends AbstractQueryParser
{
    static private Map<EDynMetaDataOperator, Class<? extends ExpressionQueryModifierProcessor>> dynAttributeProcessors = new EnumMap<EDynMetaDataOperator, Class<? extends ExpressionQueryModifierProcessor>>(
            EDynMetaDataOperator.class);
    static private Map<EConstraintOperator, Class<? extends ExpressionQueryModifierProcessor>> staticAttributeProcessors = new EnumMap<EConstraintOperator, Class<? extends ExpressionQueryModifierProcessor>>(
            EConstraintOperator.class);

    private Class<?> persisted;
    private String query;
    private IComplexTypeProvider complexTypeProvider;

    static
    {
        dynAttributeProcessors.put(EDynMetaDataOperator.lt, SubQueryPropertyInQueryModifier_Lt.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.gt, SubQueryPropertyInQueryModifier_Gt.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.eq, SubQueryPropertyInQueryModifier_Eq.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.geq, SubQueryPropertyInQueryModifier_Ge.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.leq, SubQueryPropertyInQueryModifier_Le.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.neq, SubQueryPropertyInQueryModifier_Neq.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.btw, SubQueryPropertyInQueryModifier_Btw.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.in, SubQueryPropertyInQueryModifier_In.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.notin, SubQueryPropertyInQueryModifier_NotIn.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.like, SubQueryPropertyInQueryModifier_Like.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.isnull, SubQueryPropertyInQueryModifier_IsNull.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.isnotnull, SubQueryPropertyInQueryModifier_IsNotNull.class);

        staticAttributeProcessors.put(EConstraintOperator.lt, ExpressionExpressionQueryModifier_Lt.class);
        staticAttributeProcessors.put(EConstraintOperator.gt, ExpressionExpressionQueryModifier_Gt.class);
        staticAttributeProcessors.put(EConstraintOperator.eq, ExpressionExpressionQueryModifier_Eq.class);
        staticAttributeProcessors.put(EConstraintOperator.geq, ExpressionExpressionQueryModifier_Ge.class);
        staticAttributeProcessors.put(EConstraintOperator.leq, ExpressionExpressionQueryModifier_Le.class);
        staticAttributeProcessors.put(EConstraintOperator.neq, ExpressionExpressionQueryModifier_Neq.class);
        staticAttributeProcessors.put(EConstraintOperator.btw, ExpressionExpressionQueryModifier_Btw.class);
        staticAttributeProcessors.put(EConstraintOperator.in, ExpressionExpressionQueryModifier_In.class);
        staticAttributeProcessors.put(EConstraintOperator.notin, ExpressionExpressionQueryModifier_NotIn.class);
        staticAttributeProcessors.put(EConstraintOperator.like, ExpressionExpressionQueryModifier_Like.class);
        staticAttributeProcessors.put(EConstraintOperator.isnull, ExpressionExpressionQueryModifier_IsNull.class);
        staticAttributeProcessors.put(EConstraintOperator.isnotnull, ExpressionExpressionQueryModifier_IsNotNull.class);
        staticAttributeProcessors.put(EConstraintOperator.inSequence,
                ExpressionExpressionQueryModifier_InSequence.class);
        staticAttributeProcessors.put(EConstraintOperator.inSequenceShots,
                ExpressionExpressionQueryModifier_InSequenceShots.class);
        staticAttributeProcessors.put(EConstraintOperator.hasTag, ExpressionExpressionQueryModifier_HasTag.class);
        staticAttributeProcessors.put(EConstraintOperator.hasNotTag, ExpressionExpressionQueryModifier_HasNotTag.class);
    }

    public List<IQueryModifier> handleSimpleConstraint(final Map<String, Object> constraintMap) throws Hd3dException
    {
        if (!isSimpleConstraint(constraintMap))
        {
            return null;
        }

        convertColumnValues(constraintMap, complexTypeProvider);

        final String column = getColumn(constraintMap, query);
        final String type = getType(constraintMap, query);
        final Object value = getValueOrNull(constraintMap, query);

        try
        {
            List<IQueryModifier> ret = new ArrayList<IQueryModifier>();

            IExpressionQueryModifier exp = getStaticOrDynamicAttributeQueryModifier(column, value, type);
            if (exp == null)
                return null;

            updateModififers(ret, exp.getAliases());

            ret.add(exp);

            return ret;
        }
        catch (SecurityException e)
        {
            throw new Hd3dException("Security Exception on constraint: " + e);
        }
    }

    private IExpressionQueryModifier getStaticOrDynamicAttributeQueryModifier(final String column, final Object value,
            final String type) throws Hd3dException
    {
        IExpressionQueryModifier exp;
        if (isStaticAttribute(column))
        {
            exp = getStaticAttributeQueryModifier(column, value, type);
        }
        else
        {
            exp = getDynAttributeQueryModifier(column, value, type);
        }
        return exp;
    }

    private boolean isStaticAttribute(final String attributeName)
    {
        boolean ret;
        /*
         * column may contain sub properties (ex: props1.props2.props3...) just test on the first prop. The remainder is
         * supposed to be static attribute as well
         */
        final String firstProp = StringUtils.substringBefore(attributeName, ".");

        if (getAttributeClass(firstProp) != null)
        {
            /* found in the entity class */
            ret = true;
        }
        else
        {
            /* property of a non Entity class (such as Audit) */
            try
            {
                ret = persisted.getDeclaredField(firstProp) != null;
            }
            catch (SecurityException e)
            {
                ret = false;
            }
            catch (NoSuchFieldException e)
            {
                ret = false;
            }
        }

        return ret;
    }

    public Class<?> getAttributeClass(final String attributeName)
    {
        return EntitiesMaps.withClass(persisted).getTypeOf(attributeName);
    }

    private IExpressionQueryModifier getDynAttributeQueryModifier(final String column, final Object value,
            final String type) throws Hd3dException
    {
        EDynMetaDataOperator constraint = EDynMetaDataOperator.valueOf(type);

        Class<? extends ExpressionQueryModifierProcessor> procClass = dynAttributeProcessors.get(constraint);

        if (procClass == null)
            return null;

        try
        {
            ExpressionQueryModifierProcessor proc = procClass.newInstance();
            proc.setAllModifiers(allModifiers);
            proc.setPersisted(persisted);
            proc.setColumn(column);
            proc.setValue(value);
            return proc.process();
        }
        catch (InstantiationException e)
        {
            throw new Hd3dException("Command not implemented " + type);
        }
        catch (IllegalAccessException e)
        {
            throw new Hd3dException("Command not implemented " + type);
        }
    }

    private IExpressionQueryModifier getStaticAttributeQueryModifier(final String column, final Object value,
            final String type) throws Hd3dException
    {
        EConstraintOperator constraint = EConstraintOperator.valueOf(type);

        Class<? extends ExpressionQueryModifierProcessor> procClass = staticAttributeProcessors.get(constraint);

        if (procClass == null)
            return null;

        try
        {
            ExpressionQueryModifierProcessor proc = procClass.newInstance();
            proc.setAllModifiers(allModifiers);
            proc.setPersisted(persisted);
            proc.setColumn(column);
            proc.setValue(value);
            return proc.process();
        }
        catch (InstantiationException e)
        {
            throw new Hd3dException("Command not implemented " + type);
        }
        catch (IllegalAccessException e)
        {
            throw new Hd3dException("Command not implemented " + type);
        }
    }

    @SuppressWarnings("unchecked")
    protected List<IQueryModifier> handleLogicalConstraint(final Map<String, Object> constraintMap)
            throws Hd3dException
    {
        List<IQueryModifier> ret = null;

        if (isLogicalMap(constraintMap))
        {
            convertColumnValues(constraintMap, complexTypeProvider);

            final String logicOperator = getLogicOperator(constraintMap, query);

            final List<IQueryModifier> subConstraintsQueryModifiers = handleSubConstraint((List<Map<String, Object>>) getFilter(constraintMap));

            switch (EConstraintLogicalOperator.valueOf(logicOperator))
            {
                case AND:
                {
                    if (subConstraintsQueryModifiers.isEmpty())
                    {
                        throw new Hd3dException("Missing operand for AND operator");
                    }
                    ret = new ArrayList<IQueryModifier>();
                    final List<IExpressionQueryModifier> expressions = populateAliases(ret,
                            subConstraintsQueryModifiers);
                    ret.add(LogicalQueryModifier.getConjonctionInstance(expressions));
                    break;
                }
                case OR:
                {
                    if (subConstraintsQueryModifiers.isEmpty())
                    {
                        throw new Hd3dException("Missing operand for OR operator");
                    }
                    ret = new ArrayList<IQueryModifier>();
                    final List<IExpressionQueryModifier> expressions = populateAliases(ret,
                            subConstraintsQueryModifiers);
                    ret.add(LogicalQueryModifier.getDisjonctionInstance(expressions));
                    break;
                }
                case NOT:
                {
                    if (subConstraintsQueryModifiers.size() < 1)
                    {
                        throw new Hd3dException("Missing operand for NOT operator");
                    }
                    ret = new ArrayList<IQueryModifier>();
                    final Criterion crit1 = ((IExpressionQueryModifier) subConstraintsQueryModifiers.get(0))
                            .getExpression();
                    ret.add(LogicalQueryModifier.getNOTinstance(crit1));
                    break;
                }
                    // Filter type unrecognized, throws exception.
                default:
                    throw new Hd3dException("Command not implemented " + logicOperator);
            }
        }

        return ret;
    }

    /**
     * convenience method
     */
    protected List<IExpressionQueryModifier> populateAliases(final List<IQueryModifier> aliases,
            final List<IQueryModifier> modifiers)
    {
        List<IExpressionQueryModifier> ret = null;

        CollectUtils.removeNull(aliases);
        CollectUtils.removeNull(modifiers);
        if (aliases != null && modifiers != null)
        {
            ret = new ArrayList<IExpressionQueryModifier>();

            for (IQueryModifier exp : modifiers)
            {
                if (IExpressionQueryModifier.class.isInstance(exp))
                {
                    /* collect the alias to create */
                    final IExpressionQueryModifier expression = ((IExpressionQueryModifier) exp);

                    updateModififers(aliases, expression.getAliases());
                    /* collect the expressions */
                    CollectionUtils.addIgnoreNull(ret, expression);
                }
                else
                {
                    aliases.add(exp);
                }
            }
        }

        return ret;
    }

    protected List<IQueryModifier> handleSubConstraint(final List<Map<String, Object>> subConstraints)
            throws Hd3dException
    {
        QueryModifiersList ret = new QueryModifiersList();

        if (CollectUtils.isNotEmpty(subConstraints))
        {
            processConstraints(ret, subConstraints);
        }

        return ret;
    }

    private void processConstraints(final QueryModifiersList modifiers, final List<Map<String, Object>> constraints)
            throws Hd3dException
    {
        for (Map<String, Object> constraint : constraints)
        {
            List<IQueryModifier> resultingModifiers;
            if (isLogicalMap(constraint))
            {
                resultingModifiers = handleLogicalConstraint(constraint);
            }
            else if (isSimpleConstraint(constraint))
            {
                resultingModifiers = handleSimpleConstraint(constraint);
            }
            else
            {
                throw new Hd3dQueryFilterException("Unknown Constraint type:" + constraint);
            }

            updateModififers(modifiers, resultingModifiers);
        }
    }

    /**
     * After processing, <i>modifiers</i> contains instances of ExpressionQueryModifier like equal, greater than...
     */
    public void handle(final Class<?> persisted, final String query, final IComplexTypeProvider complexTypeProvider)
            throws Hd3dException
    {
        this.persisted = persisted;
        this.query = query;
        this.complexTypeProvider = complexTypeProvider;

        processConstraints(allModifiers, getNotNullConstraintsFrom(query));
    }

    protected void convertColumnValues(final Map<String, Object> constraintMap) throws Hd3dQueryFilterException
    {
        if (complexTypeProvider != null)
        {
            tweakMap(constraintMap, complexTypeProvider);
        }
    }

}
