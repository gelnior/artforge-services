package fr.hd3d.model.persistence.impl.hibernate.criteria.parser;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.EDynMetaDataOperator;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.IComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.LogicalQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Btw;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Eq;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Ge;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Gt;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_In;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_IsNotNull;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_IsNull;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Le;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Like;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Lt;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_Neq;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ExpressionQueryModifierProcessor.SubQueryPropertyInQueryModifier_NotIn;
import fr.hd3d.utils.CollectUtils;


/**
 * Parse the query to build modifiers for the following constraints :<br />
 * "lesser than", "greater than", "equal", "greater than or equal", "lesser than or equal" , "not equal" , "between",
 * "in", "not in", "like", "is null", "is not null" <br />
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */

public class DynMetaDataQueryParser extends AbstractQueryParser
{
    // static private Map<EDynMetaDataOperator, ExpressionQueryModifierProcessor> dynAttributeProcessors = new
    // EnumMap<EDynMetaDataOperator, ExpressionQueryModifierProcessor>(
    // EDynMetaDataOperator.class);

    static private Map<EDynMetaDataOperator, Class<? extends ExpressionQueryModifierProcessor>> dynAttributeProcessors = new EnumMap<EDynMetaDataOperator, Class<? extends ExpressionQueryModifierProcessor>>(
            EDynMetaDataOperator.class);

    private Class<?> persisted;
    private String query;
    private IComplexTypeProvider complexTypeProvider;

    static
    {
        dynAttributeProcessors.put(EDynMetaDataOperator.lt, SubQueryPropertyInQueryModifier_Lt.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.gt, SubQueryPropertyInQueryModifier_Gt.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.eq, SubQueryPropertyInQueryModifier_Eq.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.geq, SubQueryPropertyInQueryModifier_Ge.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.leq, SubQueryPropertyInQueryModifier_Le.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.neq, SubQueryPropertyInQueryModifier_Neq.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.btw, SubQueryPropertyInQueryModifier_Btw.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.in, SubQueryPropertyInQueryModifier_In.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.notin, SubQueryPropertyInQueryModifier_NotIn.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.like, SubQueryPropertyInQueryModifier_Like.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.isnull, SubQueryPropertyInQueryModifier_IsNull.class);
        dynAttributeProcessors.put(EDynMetaDataOperator.isnotnull, SubQueryPropertyInQueryModifier_IsNotNull.class);

    }

    public IExpressionQueryModifier handleSimpleConstraint(final Map<String, Object> constraintMap)
            throws Hd3dException
    {
        if (!isSimpleConstraint(constraintMap))
            return null;

        convertColumnValues(constraintMap, complexTypeProvider);

        final String column = getColumn(constraintMap, query);
        final String type = getType(constraintMap, query);
        final Object value = getValueOrNull(constraintMap, query);

        EDynMetaDataOperator constraint = EDynMetaDataOperator.valueOf(type);

        Class<? extends ExpressionQueryModifierProcessor> procClass = dynAttributeProcessors.get(constraint);

        if (procClass == null)
            return null;

        try
        {
            ExpressionQueryModifierProcessor proc = procClass.newInstance();
            proc.setAllModifiers(allModifiers);
            proc.setPersisted(persisted);
            proc.setColumn(column);
            proc.setValue(value);
            return proc.process();
        }
        catch (InstantiationException e)
        {
            throw new Hd3dException("Command not implemented " + type);
        }
        catch (IllegalAccessException e)
        {
            throw new Hd3dException("Command not implemented " + type);
        }
    }

    @SuppressWarnings("unchecked")
    private IExpressionQueryModifier handleLogicalMap(final Map<String, Object> constraintMap) throws Hd3dException
    {
        IExpressionQueryModifier ret = null;

        if (isLogicalMap(constraintMap))
        {
            convertColumnValues(constraintMap, complexTypeProvider);

            final String logicOperator = getLogicOperator(constraintMap, query);

            final List<IExpressionQueryModifier> expressionQueryModifiers = handleSubConstraint((List<Map<String, Object>>) getFilter(constraintMap));

            Criterion crit1;
            Criterion crit2;
            switch (EConstraintLogicalOperator.valueOf(logicOperator))
            {
                case AND:
                {
                    if (expressionQueryModifiers.size() < 2)
                    {
                        throw new Hd3dException("Missing operand for AND operator");
                    }
                    crit1 = expressionQueryModifiers.get(0).getExpression();
                    crit2 = expressionQueryModifiers.get(1).getExpression();
                    ret = LogicalQueryModifier.getANDinstance(crit1, crit2);
                    break;
                }
                case OR:
                {
                    if (expressionQueryModifiers.size() < 2)
                    {
                        throw new Hd3dException("Missing operand for OR operator");
                    }
                    crit1 = expressionQueryModifiers.get(0).getExpression();
                    crit2 = expressionQueryModifiers.get(1).getExpression();
                    ret = LogicalQueryModifier.getORinstance(crit1, crit2);
                    break;
                }
                case NOT:
                {
                    if (expressionQueryModifiers.size() < 1)
                    {
                        throw new Hd3dException("Missing operand for NOT operator");
                    }
                    crit1 = expressionQueryModifiers.get(0).getExpression();
                    ret = LogicalQueryModifier.getNOTinstance(crit1);
                    break;
                }
                    // Filter type unrecognized, throws exception.
                default:
                    throw new Hd3dException("Command not implemented " + logicOperator);
            }
        }

        return ret;
    }

    protected List<IExpressionQueryModifier> handleSubConstraint(final List<Map<String, Object>> subConstraints)
            throws Hd3dException
    {
        List<IExpressionQueryModifier> ret = new ArrayList<IExpressionQueryModifier>();
        if (CollectUtils.isNotEmpty(subConstraints))
        {
            ret = new ArrayList<IExpressionQueryModifier>();
            processConstraints(ret, subConstraints);
        }
        return ret;
    }

    private <Q extends IQueryModifier> void processConstraints(final List<Q> modifiers,
            final List<Map<String, Object>> constraints) throws Hd3dException
    {
        for (Map<String, Object> constraint : constraints)
        {
            IExpressionQueryModifier queryModifier = null;
            if (isLogicalMap(constraint))
            {
                queryModifier = handleLogicalMap(constraint);
            }
            else if (isSimpleConstraint(constraint))
            {
                queryModifier = handleSimpleConstraint(constraint);
            }
            else
            {
                throw new Hd3dQueryFilterException("Unknown Constraint type:" + constraint);
            }
            CollectUtils.addIfNotNull(modifiers, (Q) queryModifier);
        }
    }

    /**
     * After processing, <i>modifiers</i> contains instances of ExpressionQueryModifier like equal, greater than...
     */
    public void handle(final Class<?> persisted, final String query, final IComplexTypeProvider complexTypeProvider)
            throws Hd3dException
    {
        this.persisted = persisted;
        this.query = query;
        this.complexTypeProvider = complexTypeProvider;

        processConstraints(allModifiers, getNotNullConstraintsFrom(query));
    }
}
