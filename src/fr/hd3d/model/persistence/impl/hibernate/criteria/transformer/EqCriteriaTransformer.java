package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;


public class EqCriteriaTransformer extends AbstractQueryTransformer<Criteria, Criteria>
{
    String field = null;
    Object value = null;
    Collection<Object> values = null;

    @SuppressWarnings("unchecked")
    public EqCriteriaTransformer(String field, Object value)
    {
        super();
        this.field = field;
        if (value instanceof Collection)
        {
            this.values = (Collection<Object>) value;
        }
        else
        {
            this.value = value;
        }
    }

    @SuppressWarnings("unchecked")
    public EqCriteriaTransformer(ResourceContext context, String field, Object value)
    {
        super(context);
        this.field = field;
        if (value instanceof Collection)
        {
            this.values = (Collection<Object>) value;
        }
        else
        {
            this.value = value;
        }
    }

    public Criteria transform(Criteria query)
    {
        if (value != null)
        {
            query.add(Restrictions.eq(field, value));
        }
        if (CollectUtils.isNotEmpty(values))
        {
            query.add(Restrictions.in(field, values));
        }

        return query;
    }
}
