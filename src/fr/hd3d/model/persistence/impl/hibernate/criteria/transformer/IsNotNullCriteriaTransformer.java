package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.services.resources.ResourceContext;


public class IsNotNullCriteriaTransformer extends AbstractQueryTransformer<Criteria, Criteria>
{
    String field = null;

    public IsNotNullCriteriaTransformer(String field)
    {
        super();
        this.field = field;
    }

    @SuppressWarnings("unchecked")
    public IsNotNullCriteriaTransformer(ResourceContext context, String field)
    {
        super(context);
        this.field = field;
    }

    public Criteria transform(Criteria query)
    {
        if (field != null)
        {
            query.add(Restrictions.isNotNull(field));
        }
        return query;
    }
}
