/**
 * 
 */
package fr.hd3d.model.persistence.impl.hibernate.criteria.modifier;

import static fr.hd3d.utils.Const.ID;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.criterion.Subqueries;

import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataValueH;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;


/**
 * @author Try LAM
 */
public class SubQueryPropertyInQueryModifier implements IExpressionQueryModifier
{
    private static final String VALUE = "value";
    private static final String DATEVALUE = "dateValue";
    private static final String BOOLEANVALUE = "booleanValue";
    private static final String LONGVALUE = "longValue";

    private final Criterion expression;

    public SubQueryPropertyInQueryModifier(final Criterion expression)
    {
        this.expression = expression;
    }

    public Criterion getExpression()
    {
        return this.expression;
    }

    public List<IQueryModifier> getAliases()
    {
        return null;
    }

    private static String getValueField(final Object value)
    {
        String valueField = null;

        if (value != null)
        {
            final Class<?> valueClass = value.getClass();

            if (Date.class.isAssignableFrom(valueClass))
            {
                valueField = DATEVALUE;
            }
            else if (Boolean.class.isAssignableFrom(valueClass))
            {
                valueField = BOOLEANVALUE;
            }
            else if (Long.class.isAssignableFrom(valueClass))
            {
                valueField = LONGVALUE;
            }
            else
            {
                valueField = VALUE;
            }
        }

        return valueField;
    }

    static private DetachedCriteria getDynMetaDataValueQuery(final String column)
    {
        final DetachedCriteria subQuery = DetachedCriteria.forClass(DynMetaDataValueH.class, "dyn");
        subQuery.setProjection(Projections.distinct(Projections.id()));
        subQuery.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        subQuery.createAlias("classDynMetaDataType", "classdyn");

        SimpleExpression lookupConstraint;
        if (StringUtils.isNumeric(column))
        {
            lookupConstraint = Restrictions.eq("classdyn.id", Long.valueOf(column));
        }
        else
        {
            lookupConstraint = Restrictions.eq("classdyn.name", column);
        }
        subQuery.add(lookupConstraint);

        subQuery.setProjection(Projections.property("parent"));

        return subQuery;
    }

    static private IExpressionQueryModifier getInstance(final String column, final Criterion restriction)
    {
        IExpressionQueryModifier ret = null;

        final DetachedCriteria subQuery = getDynMetaDataValueQuery(column);
        if (subQuery != null)
        {
            subQuery.add(restriction);

            ret = new SubQueryPropertyInQueryModifier(Subqueries.propertyIn(ID, subQuery));
        }

        return ret;
    }

    /**
     * Apply a "<b>greater than</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return a Criteria modifier
     */
    static public IExpressionQueryModifier getGtInstance(final Class<?> persisted, final String column,
            final Object value)
    {
        return getInstance(column, Restrictions.gt(getValueField(value), value));
    }

    /**
     * Apply a "<b>lesser than</b>" constraint to <i>column</i>.
     * 
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getLtInstance(final Class<?> persisted, final String column,
            final Object value)
    {
        return getInstance(column, Restrictions.lt(getValueField(value), value));
    }

    /**
     * Apply a "<b>greater than or equal</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getGeInstance(final Class<?> persisted, final String column,
            final Object value)
    {
        return getInstance(column, Restrictions.ge(getValueField(value), value));
    }

    /**
     * Apply a "<b>lesser than or equal</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getLeInstance(final Class<?> persisted, final String column,
            final Object value)
    {
        return getInstance(column, Restrictions.le(getValueField(value), value));
    }

    /**
     * Apply a "<b>equal</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getEqInstance(final Class<?> persisted, final String column,
            final Object value)
    {
        return getInstance(column, Restrictions.eq(getValueField(value), value));
    }

    /**
     * Apply a "<b>not equal</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getNeqInstance(final Class<?> persisted, final String column,
            final Object value)
    {
        return getInstance(column, Restrictions.ne(getValueField(value), value));
    }

    /**
     * Apply a "<b>between</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getBetweenInstance(final Class<?> persisted, final String column,
            final Object value, final Object start, final Object end)
    {
        return getInstance(column, Restrictions.between(getValueField(value), start, end));
    }

    /**
     * Apply a "<b>in</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getInInstance(final String column, final Object value,
            final Collection<?> set)
    {
        return getInstance(column, Restrictions.in(getValueField(value), set));
    }

    /**
     * Apply a "<b>not in</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getNotInInstance(final String column, final Object value,
            final Collection<?> set)
    {
        return getInstance(column, Restrictions.not(Restrictions.in(getValueField(value), set)));
    }

    /**
     * Apply a "<b>is null</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getIsNullInstance(final String column, final Object value)
    {
        return getInstance(column, Restrictions.isNull(getValueField(value)));
    }

    /**
     * Apply a "<b>is not null</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getIsNotNullInstance(final String column, final Object value)
    {
        return getInstance(column, Restrictions.isNotNull(getValueField(value)));
    }

    public void applyModifier(final Criteria query) throws Hd3dQueryFilterException
    {
        if (query != null)
        {
            query.add(expression);
        }
    }

    public void applyModifierForCount(Criteria query) throws Hd3dQueryFilterException
    {
        applyModifier(query);
    }

    public void applyModifierForCount(DetachedCriteria query) throws Hd3dQueryFilterException
    {}

    /**
     * Apply case-insensitive "<b>like</b>", similar to Postgres ilike operator.
     * 
     * @param column
     * @param value
     * @return
     */
    public static IExpressionQueryModifier getLikeInstance(final String column, final Object value)
    {
        return getInstance(column, Restrictions.ilike(getValueField(value), value));
    }

    public void applyModifier(final DetachedCriteria query) throws Hd3dQueryFilterException
    {
        // TODO Auto-generated method stub
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((expression == null) ? 0 : expression.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SubQueryPropertyInQueryModifier other = (SubQueryPropertyInQueryModifier) obj;
        if (expression == null)
        {
            if (other.expression != null)
                return false;
        }
        else if (!expression.equals(other.expression))
            return false;
        return true;
    }
}
