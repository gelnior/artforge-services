/**
 * 
 */
package fr.hd3d.model.persistence.impl.hibernate.criteria.modifier;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;

import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;
import fr.hd3d.utils.Conf;


/**
 * Set a limit upon the number of objects to be retrieved from an hibernate query.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public final class MaxResultQueryModifier implements IQueryModifier
{
    private final int maxResult;

    /**
     * @param maxResult
     *            The maximum number of results
     * @return the adapted Criteria (hibernate query) modifier.
     */
    static public IQueryModifier getInstance(final long maxResult)
    {
        final long max = (maxResult <= Conf.getMaxRecords()) ? maxResult : Conf.getMaxRecords();
        return new MaxResultQueryModifier((int) max);
    }

    /**
     * @param maxResult
     *            The maximum number of results.
     */
    private MaxResultQueryModifier(final int maxResult)
    {
        /* Max result cannot be negative. */
        this.maxResult = maxResult < 0 ? 0 : maxResult;
    }

    public void applyModifier(final Criteria query) throws Hd3dQueryFilterException
    {
        if (query != null)
        {
            query.setMaxResults(maxResult);
        }
    }

    public void applyModifier(final DetachedCriteria query) throws Hd3dQueryFilterException
    {
        /* nothing: setFirstResult does not apply to DetachedCriteria */
    }

    public void applyModifierForCount(Criteria query) throws Hd3dQueryFilterException
    {}

    public void applyModifierForCount(DetachedCriteria query) throws Hd3dQueryFilterException
    {}

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + maxResult;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MaxResultQueryModifier other = (MaxResultQueryModifier) obj;
        if (maxResult != other.maxResult)
            return false;
        return true;
    }
}
