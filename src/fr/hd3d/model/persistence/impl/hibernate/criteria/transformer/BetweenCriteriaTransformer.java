package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.services.resources.ResourceContext;


public class BetweenCriteriaTransformer extends AbstractQueryTransformer<Criteria, Criteria>
{
    String field = null;
    Object from = null;
    Object to = null;

    public BetweenCriteriaTransformer(String field, Object from, Object to)
    {
        super();
        this.field = field;
        this.from = from;
        this.to = to;
    }

    @SuppressWarnings("unchecked")
    public BetweenCriteriaTransformer(ResourceContext context, String field, Object from, Object to)
    {
        super(context);
        this.field = field;
        this.from = from;
        this.to = to;
    }

    public Criteria transform(Criteria query)
    {
        query.add(Restrictions.between(field, from, to));

        return query;
    }
}
