/**
 * 
 */
package fr.hd3d.model.persistence.impl.hibernate.criteria.modifier;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;

import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;


/**
 * Set a limit upon the number of objects to be retrieved from an hibernate query.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class CreateAliasQueryModifier implements IQueryModifier
{
    private String associationPath;
    private String alias;
    private int joinType;

    static public IQueryModifier getInstance(final String associationPath, final String alias)
    {
        return new CreateAliasQueryModifier(associationPath, alias);
    }

    static public IQueryModifier getInstance(final String associationPath, final String alias, int joinType)
    {
        return new CreateAliasQueryModifier(associationPath, alias, joinType);
    }

    /**
     * @param maxResult
     *            The maximum number of results.
     */
    public CreateAliasQueryModifier(final String associationPath, final String alias)
    {
        this.associationPath = associationPath;
        this.alias = alias;
    }

    public CreateAliasQueryModifier(final String associationPath, final String alias, final int joinType)
    {
        this(associationPath, alias);
        this.joinType = joinType;
    }

    public String getAssociationPath()
    {
        return associationPath;
    }

    public String getAlias()
    {
        return alias;
    }

    public void setAssociationPath(final String associationPath)
    {
        this.associationPath = associationPath;
    }

    public void setAlias(final String alias)
    {
        this.alias = alias;
    }

    public void applyModifier(final Criteria query) throws Hd3dQueryFilterException
    {
        query.createAlias(associationPath, alias, joinType);
    }

    public void applyModifier(final DetachedCriteria query) throws Hd3dQueryFilterException
    {
        query.createAlias(associationPath, alias, joinType);
    }

    public void applyModifierForCount(Criteria query) throws Hd3dQueryFilterException
    {
        applyModifier(query);
    }

    public void applyModifierForCount(DetachedCriteria query) throws Hd3dQueryFilterException
    {
        applyModifier(query);
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((alias == null) ? 0 : alias.hashCode());
        result = prime * result + ((associationPath == null) ? 0 : associationPath.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CreateAliasQueryModifier other = (CreateAliasQueryModifier) obj;
        if (alias == null)
        {
            if (other.alias != null)
                return false;
        }
        else if (!alias.equals(other.alias))
            return false;
        if (associationPath == null)
        {
            if (other.associationPath != null)
                return false;
        }
        else if (!associationPath.equals(other.associationPath))
            return false;
        return true;
    }

}
