package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.services.resources.ResourceContext;


public class GtCriteriaTransformer extends AbstractQueryTransformer<Criteria, Criteria>
{
    String field = null;
    Object value = null;

    public GtCriteriaTransformer(String field, Object value)
    {
        super();
        this.field = field;
        this.value = value;
    }

    @SuppressWarnings("unchecked")
    public GtCriteriaTransformer(ResourceContext context, String field, Object value)
    {
        super(context);
        this.field = field;
        this.value = value;
    }

    public Criteria transform(Criteria query)
    {
        if (value != null)
        {
            query.add(Restrictions.gt(field, value));
        }
        else
        {
            query.add(Restrictions.isNull(field));
        }

        return query;
    }
}
