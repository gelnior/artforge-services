package fr.hd3d.model.persistence.impl.hibernate.criteria.parser;

import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import fr.hd3d.common.client.Constraint;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.IComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.FirstResultQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.MaxResultQueryModifier;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.utils.CollectUtils;


/**
 * Parse a pagination query to build a "limit" query.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class PaginationQueryParser extends AbstractQueryParser
{
    /**
     * After processing, <i>modifiers</i> contains instances of FirstResultQueryModifier and/or MaxResultQueryModifier
     */
    public void handle(final Class<?> persisted, final String query, final IComplexTypeProvider enumProvider)
            throws Hd3dException
    {
        if (allModifiers == null)
            return;

        /* Parse query, get the attributes FIRST and QUANTITY. */
        final Map<String, Long> map = Hd3dJsonSerializer.<Map<String, Long>> unserialize(query);

        if (map == null)
            return;

        final Set<Entry<String, Long>> entrySet = map.entrySet();
        CollectUtils.removeNull(entrySet);
        String key;
        Long value;
        IQueryModifier queryModifier;
        for (Entry<String, Long> entry : entrySet)
        {
            key = entry.getKey();
            value = entry.getValue();
            /* For the first result add a FirstResultModifier */
            if (Constraint.FIRST_MAP_FIELD.equals(key))
            {
                queryModifier = FirstResultQueryModifier.getInstance(value);
            }
            /* For the quantity add a MaxResultModifier */
            else if (Constraint.QUANTITY_MAP_FIELD.equals(key))
            {
                queryModifier = MaxResultQueryModifier.getInstance(value);
            }
            else
            {
                throw new Hd3dQueryFilterException("Unknown pagination field:" + key);
            }

            allModifiers.add(queryModifier);
        }
    }
}
