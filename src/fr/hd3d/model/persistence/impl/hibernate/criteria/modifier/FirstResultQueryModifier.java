/**
 * 
 */
package fr.hd3d.model.persistence.impl.hibernate.criteria.modifier;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;

import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;


/**
 * Set the first result to retrieve (where retrieved list starts) for an hibernate query.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
final public class FirstResultQueryModifier implements IQueryModifier
{
    /** The first result to retrieve, numbered from 0. */
    private final int firstResult;

    /**
     * @param firstResult
     *            the first result to retrieve, numbered from 0.
     * @return the adapted Criteria (hibernate query) modifier.
     */
    static public IQueryModifier getInstance(final long firstResult)
    {
        return new FirstResultQueryModifier((int) firstResult);
    }

    /**
     * @param firstResult
     *            the first result to retrieve, numbered from 0.
     */
    private FirstResultQueryModifier(final int firstResult)
    {
        /* First result cannot be negative. */
        this.firstResult = firstResult < 0 ? 0 : firstResult;
    }

    public int getFirstResult()
    {
        return this.firstResult;
    }

    public void applyModifier(final Criteria query) throws Hd3dQueryFilterException
    {
        query.setFirstResult(firstResult);
    }

    public void applyModifier(final DetachedCriteria query) throws Hd3dQueryFilterException
    {
        /* nothing: setFirstResult does not apply to DetachedCriteria */
    }

    public void applyModifierForCount(Criteria query) throws Hd3dQueryFilterException
    {}

    public void applyModifierForCount(DetachedCriteria query) throws Hd3dQueryFilterException
    {}

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + firstResult;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FirstResultQueryModifier other = (FirstResultQueryModifier) obj;
        if (firstResult != other.firstResult)
            return false;
        return true;
    }

}
