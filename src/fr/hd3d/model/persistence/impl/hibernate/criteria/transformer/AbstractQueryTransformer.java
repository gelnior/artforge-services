package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import org.apache.commons.collections15.Transformer;

import fr.hd3d.services.resources.ResourceContext;


public abstract class AbstractQueryTransformer<I, O> implements Transformer<I, O>
{
    private ResourceContext<?, ?> context = new ResourceContext();
    /* the entity the criteria is currently applied on may not be the one in context: criteria may "createCriteria()" */
    private Class<?> currentClass;

    public ResourceContext<?, ?> getContext()
    {
        return context;
    }

    public Class<?> getCurrentClass()
    {
        return currentClass;
    }

    public void setCurrentClass(Class<?> clazz)
    {
        this.currentClass = clazz;
    }

    public AbstractQueryTransformer()
    {
        super();
    }

    public AbstractQueryTransformer(ResourceContext<?, ?> context)
    {
        super();
        if (context != null)
        {
            this.context = context;
        }
    }

    public AbstractQueryTransformer(ResourceContext<?, ?> context, Class<?> currentClass)
    {
        super();
        if (context != null)
        {
            this.context = context;
        }
        this.currentClass = currentClass;
    }
}
