package fr.hd3d.model.persistence.impl.hibernate.criteria;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.collections15.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.Constraint;
import fr.hd3d.common.client.ItemConstraint;
import fr.hd3d.common.client.LuceneConstraint;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.IComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.CreateAliasQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.ConstraintQueryParser;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.CustomQueryParser;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.DynMetaDataQueryParser;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.FileAttributeQueryParser;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.OrderByQueryParser;
import fr.hd3d.model.persistence.impl.hibernate.criteria.parser.PaginationQueryParser;
import fr.hd3d.model.persistence.impl.hibernate.query.ListResultQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.predicate.IsPaginationQueryModifierPredicate;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public final class QueryFilterFactory
{
    private QueryFilterFactory()
    {}

    /**
     * handles a collection of IQueryModifiers.
     */
    public final static class CompositeQueryModifier implements IQueryModifier
    {
        private boolean hasExpressionQueryModifier = false;
        private final List<IQueryModifier> modifiers = new ArrayList<IQueryModifier>();
        private final List<CreateAliasQueryModifier> createAliasModifiers = new ArrayList<CreateAliasQueryModifier>();

        public boolean hasExpressionQueryModifier()
        {
            return hasExpressionQueryModifier;
        }

        public CompositeQueryModifier(final List<IQueryModifier> modifiers)
        {
            addAll(modifiers);
        }

        public CompositeQueryModifier(final IQueryModifier... modifiers)
        {
            for (IQueryModifier queryModifier : modifiers)
            {
                add(queryModifier);
            }
        }

        public List<CreateAliasQueryModifier> getCreateAliasModifiers()
        {
            return createAliasModifiers;
        }

        public boolean containsCreateAliasModifier(final String associationPath)
        {
            boolean ok = false;
            for (CreateAliasQueryModifier modifier : getCreateAliasModifiers())
            {
                if (modifier != null && modifier.getAssociationPath() != null
                        && modifier.getAssociationPath().equals(associationPath))
                {
                    ok = true;
                    break;
                }
            }
            return ok;
        }

        public void add(final IQueryModifier modifier)
        {
            modifiers.add(modifier);

            /* keep trace of created alias */
            if (CreateAliasQueryModifier.class.isInstance(modifier))
            {
                getCreateAliasModifiers().add((CreateAliasQueryModifier) modifier);
            }
            /* keep trace of ExpressionQueryModifier */
            if (!hasExpressionQueryModifier && IExpressionQueryModifier.class.isInstance(modifier))
            {
                hasExpressionQueryModifier = true;
            }
        }

        public void addAll(final List<IQueryModifier> modifiers)
        {
            this.modifiers.addAll(modifiers);
            for (IQueryModifier modifier : modifiers)

            {
                /* keep trace of created alias */
                if (CreateAliasQueryModifier.class.isInstance(modifier))
                {
                    getCreateAliasModifiers().add((CreateAliasQueryModifier) modifier);
                }
                /* keep trace of ExpressionQueryModifier */
                if (!hasExpressionQueryModifier && IExpressionQueryModifier.class.isInstance(modifier))
                {
                    hasExpressionQueryModifier = true;
                }
            }
        }

        /**
         * applies the collection of IQueryModifiers to <i>query</i> Criteria
         */
        public void applyModifier(final Criteria query) throws Hd3dQueryFilterException
        {
            for (IQueryModifier pager : modifiers)
            {
                pager.applyModifier(query);
            }
        }

        public void applyModifier(final DetachedCriteria query) throws Hd3dQueryFilterException
        {
            for (IQueryModifier pager : modifiers)
            {
                pager.applyModifier(query);
            }
        }

        public void applyModifierForCount(final Criteria query) throws Hd3dQueryFilterException
        {
            for (IQueryModifier pager : modifiers)
            {
                if (!ListResultQuery.isToRemoveForCount(pager))
                    pager.applyModifier(query);
            }
        }

        public void applyModifierForCount(final DetachedCriteria query) throws Hd3dQueryFilterException
        {
            for (IQueryModifier pager : modifiers)
            {
                if (!ListResultQuery.isToRemoveForCount(pager))
                    pager.applyModifier(query);
            }
        }

        /**
         * Remove pagination filters from the criteria.
         */
        @SuppressWarnings("unchecked")
        public void removePagination()
        {
            CollectionUtils.filter(modifiers, new IsPaginationQueryModifierPredicate());
        }

        public void removeFilter(final IQueryModifier filter)
        {
            this.modifiers.remove(filter);
        }

    }

    @SuppressWarnings("serial")
    private static final Map<String, Class<? extends IQueryParser>> QUERY_KEYWORDS = Collections
            .unmodifiableMap(new HashMap<String, Class<? extends IQueryParser>>() {
                {
                    put(Constraint.PAGINATION, PaginationQueryParser.class);
                    put(Constraint.ORDERBY, OrderByQueryParser.class);
                    put(Constraint.CONSTRAINT, ConstraintQueryParser.class);
                    put(Const.CUSTOM, CustomQueryParser.class);
                    put(Const.DYNMETADATA, DynMetaDataQueryParser.class);
                    put(Const.FILEATTRIBUTE, FileAttributeQueryParser.class);
                    put(Const.TAGS, null);// not a constraint but an allowed parameter in url
                    put(Const.APPROVALS, null);// not a constraint but an allowed parameter in url
                    put(Const.ENTITYTASKLINKS, null);// not a constraint but an allowed parameter in url
                    put(Const.BOUNDTASKS, null);// not a constraint but an allowed parameter in url
                    put(Const.DYN, null);// not a constraint but an allowed parameter in url
                    put(Const.EXTRAFIELDS, null);// not a constraint but an allowed parameter in url
                }
            });

    /**
     * 
     * @param map
     * @param complexTypeProvider
     * @return
     * @throws Hd3dException
     * @throws Hd3dException
     */
    public static CompositeQueryModifier getFilter(final Class<?> persisted, final Map<String, String> map,
            final IComplexTypeProvider complexTypeProvider) throws Hd3dException
    {
        // final List<IQueryModifier> allModifiers = new ArrayList<IQueryModifier>();
        QueryModifiersList allModifiers = new QueryModifiersList();

        if (map != null)
        {
            final Set<Entry<String, String>> entries = map.entrySet();
            CollectUtils.removeNull(entries);

            for (Map.Entry<String, String> entry : entries)
            {
                /* find out the proper handler for the passed parameter */
                Class<? extends IQueryParser> parserClass = QUERY_KEYWORDS.get(entry.getKey());
                if (parserClass != null)
                {
                    try
                    {
                        IQueryParser queryParser = parserClass.newInstance();
                        queryParser.setAllModifiers(allModifiers);
                        /* if a lucene constraint is present, the result may be flawed by pagination, so handle it later */
                        if (queryParser instanceof PaginationQueryParser)
                        {
                            if (hibernatePagination(map))
                            {
                                queryParser.handle(persisted, map.get(entry.getKey()), complexTypeProvider);
                            }
                        }
                        else
                        {
                            queryParser.handle(persisted, map.get(entry.getKey()), complexTypeProvider);
                        }
                    }
                    catch (InstantiationException e)
                    {
                        throw new Hd3dException(e);
                    }
                    catch (IllegalAccessException e)
                    {
                        throw new Hd3dException(e);
                    }
                }
            }
        }

        return new CompositeQueryModifier(allModifiers);
    }

    public static boolean hibernatePagination(final Map<String, String> map)
    {
        return !map.containsKey(LuceneConstraint.LUCENECONSTRAINT) && !map.containsKey(ItemConstraint.ITEMSORT);
    }
}
