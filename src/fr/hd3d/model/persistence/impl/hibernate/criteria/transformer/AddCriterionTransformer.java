package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;

import fr.hd3d.services.resources.ResourceContext;


public class AddCriterionTransformer extends AbstractQueryTransformer<Criteria, Criteria>
{
    Criterion criterion = null;

    public AddCriterionTransformer(Criterion aCriterion)
    {
        super();
        this.criterion = aCriterion;
    }

    @SuppressWarnings("unchecked")
    public AddCriterionTransformer(ResourceContext context, Criterion aCriterion)
    {
        super(context);
        this.criterion = aCriterion;
    }

    public Criteria transform(Criteria query)
    {
        if (criterion != null)
        {
            query.add(criterion);
        }
        return query;
    }
}
