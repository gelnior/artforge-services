package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import static org.hibernate.criterion.Restrictions.in;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ServerResourceUtils;


public class IdEqCriteriaTransformer extends AbstractQueryTransformer<Criteria, Criteria>
{
    Long id = null;
    Collection<Long> ids = null;
    String field = null;

    public IdEqCriteriaTransformer(String field, Long id)
    {
        super();
        this.id = id;
        this.field = field;
    }

    public IdEqCriteriaTransformer(String field, Collection<Long> ids)
    {
        super();
        this.ids = ids;
        this.field = field;
    }

    public IdEqCriteriaTransformer(ResourceContext<?, ?> context, String field, Long id)
    {
        super(context);
        this.id = id;
        this.field = field;
    }

    public IdEqCriteriaTransformer(ResourceContext<?, ?> context, String field, Collection<Long> ids)
    {
        super(context);
        this.ids = ids;
        this.field = field;
    }

    public Criteria transform(Criteria query)
    {
        if (id != null)
        {
            if (getCurrentClass() == null)
                ServerResourceUtils.addIdEqRestriction(getContext(), query, field, id);
            else
                ServerResourceUtils.addIdEqRestriction(getContext(), query, field, id, getCurrentClass());
        }
        else if (CollectUtils.isNotEmpty(ids))
        {
            query.add(in(field, ids));
        }
        else
        {
            query.add(Restrictions.isNull(Const.ID));
        }

        return query;
    }
}
