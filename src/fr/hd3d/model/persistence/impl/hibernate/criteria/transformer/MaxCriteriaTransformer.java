package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;

import fr.hd3d.services.resources.ResourceContext;


public class MaxCriteriaTransformer extends AbstractQueryTransformer<Criteria, Criteria>
{
    String field = null;

    public MaxCriteriaTransformer(String field)
    {
        super();
        this.field = field;
    }

    @SuppressWarnings("unchecked")
    public MaxCriteriaTransformer(ResourceContext context, String field)
    {
        super(context);
        this.field = field;
    }

    public Criteria transform(Criteria query)
    {
        if (field != null)
        {
            query.setProjection(Projections.max(field));
        }
        return query;
    }
}
