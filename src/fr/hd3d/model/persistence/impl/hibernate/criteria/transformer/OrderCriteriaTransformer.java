package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;

import fr.hd3d.services.resources.ResourceContext;


public class OrderCriteriaTransformer extends AbstractQueryTransformer<Criteria, Criteria>
{
    String field = null;
    boolean ascending = true;

    public OrderCriteriaTransformer(String field, boolean ascending)
    {
        super();
        this.field = field;
        this.ascending = ascending;
    }

    @SuppressWarnings("unchecked")
    public OrderCriteriaTransformer(ResourceContext context, String field, boolean ascending)
    {
        super(context);
        this.field = field;
        this.ascending = ascending;
    }

    public Criteria transform(Criteria query)
    {
        if (field != null)
        {
            if (ascending)
            {
                query.addOrder(Order.asc(field));
            }
            else
            {
                query.addOrder(Order.desc(field));
            }
        }

        return query;
    }
}
