package fr.hd3d.model.persistence.impl.hibernate.criteria.modifier;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;


/**
 * Handles constraints such as "greater than", "equal", etc...
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class FileAttributeQueryModifier implements IExpressionQueryModifier
{
    /* NOTE: attr is an alias defined in FileAttributeQueryParser, handle method */
    private static final String ATTRIBUTE_NAME = "attr.name";
    private static final String ATTRIBUTE_VALUE = "attr.value";

    private final Criterion expression;

    public Criterion getExpression()
    {
        return this.expression;
    }

    public List<IQueryModifier> getAliases()
    {
        return null;
    }

    static public IExpressionQueryModifier getFileAttributeNameInstance(final String name)
    {
        return new FileAttributeQueryModifier(Restrictions.eq(ATTRIBUTE_NAME, name));
    }

    /**
     * Apply a "<b>equal</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getEqInstance(final String value)
    {
        return new FileAttributeQueryModifier(Restrictions.eq(ATTRIBUTE_VALUE, value));
    }

    /**
     * Apply a "<b>not equal</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getNeqInstance(final String value)
    {
        return new FileAttributeQueryModifier(Restrictions.ne(ATTRIBUTE_VALUE, value));
    }

    /**
     * Apply a "<b>is null</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getIsNullInstance()
    {
        return new FileAttributeQueryModifier(Restrictions.isNull(ATTRIBUTE_VALUE));
    }

    /**
     * Apply a "<b>is not null</b>" constraint to <i>column</i>.
     * 
     * @param column
     * @param value
     * @return
     */
    static public IExpressionQueryModifier getIsNotNullInstance()
    {
        return new FileAttributeQueryModifier(Restrictions.isNotNull(ATTRIBUTE_VALUE));
    }

    public FileAttributeQueryModifier(final Criterion expression)
    {
        this.expression = expression;
    }

    public void applyModifier(final Criteria query) throws Hd3dQueryFilterException
    {
        query.add(expression);
    }

    public void applyModifierForCount(Criteria query) throws Hd3dQueryFilterException
    {
        applyModifier(query);
    }

    public void applyModifierForCount(DetachedCriteria query) throws Hd3dQueryFilterException
    {
        applyModifier(query);
    }

    /**
     * Apply case-insensitive "<b>like</b>", similar to Postgres ilike operator.
     * 
     * @param column
     * @param value
     * @return
     */
    public static IExpressionQueryModifier getLikeInstance(final String value)
    {
        return new FileAttributeQueryModifier(Restrictions.ilike(ATTRIBUTE_VALUE, value));
    }

    public void applyModifier(final DetachedCriteria query) throws Hd3dQueryFilterException
    {
        query.add(expression);
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((expression == null) ? 0 : expression.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FileAttributeQueryModifier other = (FileAttributeQueryModifier) obj;
        if (expression == null)
        {
            if (other.expression != null)
                return false;
        }
        else if (!expression.equals(other.expression))
            return false;
        return true;
    }

}
