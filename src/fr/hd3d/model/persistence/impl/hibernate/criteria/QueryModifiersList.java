package fr.hd3d.model.persistence.impl.hibernate.criteria;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.CreateAliasQueryModifier;


public class QueryModifiersList extends ArrayList<IQueryModifier>
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /*
     * To avoid duplicate alias duplication. In Hibernate, it is not possible to create multiple aliases on a same
     * associationPath (=>"duplicate associationPath" error), nor to create multiple aliases (causes improper behaviour)
     */
    public Map<String, CreateAliasQueryModifier> existingAssociationPathsToAliasMap = new HashMap<String, CreateAliasQueryModifier>();

}
