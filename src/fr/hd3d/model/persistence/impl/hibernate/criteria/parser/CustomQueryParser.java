package fr.hd3d.model.persistence.impl.hibernate.criteria.parser;

import net.sf.sojo.interchange.json.JsonParser;
import net.sf.sojo.interchange.json.JsonParserException;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.IComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;


public class CustomQueryParser extends AbstractQueryParser
{
    public void handle(final Class<?> persisted, final String query, final IComplexTypeProvider enumProvider)
            throws Hd3dException
    {
        try
        {
            new JsonParser().parse(query);
        }
        catch (JsonParserException e)
        {
            throw new Hd3dQueryFilterException(e + ":" + query);
        }
    }
}
