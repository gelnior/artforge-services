package fr.hd3d.model.persistence.impl.hibernate.criteria.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.EFileAttributeOperator;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.IComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.CreateAliasQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.FileAttributeQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.LogicalQueryModifier;
import fr.hd3d.utils.CollectUtils;


/**
 * Parse the query to build modifiers for the following constraints :<br />
 * "lesser than", "greater than", "equal", "greater than or equal", "lesser than or equal" , "not equal" , "between",
 * "in", "not in", "like", "is null", "is not null" <br />
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class FileAttributeQueryParser extends AbstractQueryParser
{
    private String query;
    private IComplexTypeProvider complexTypeProvider;

    public List<IExpressionQueryModifier> handleSimpleConstraint(final Map<String, Object> constraintMap)
            throws Hd3dException
    {
        List<IExpressionQueryModifier> ret = null;

        if (isSimpleConstraint(constraintMap))
        {
            convertColumnValues(constraintMap, complexTypeProvider);

            final String name = getName(constraintMap, query);
            final String type = getType(constraintMap, query);
            final String value = (String) getValueOrNull(constraintMap, query);

            /* list size=2: one for "fileAttribute.name" and one for "fileAttribute.value" */
            ret = new ArrayList<IExpressionQueryModifier>(2);
            ret.add(FileAttributeQueryModifier.getFileAttributeNameInstance(name));

            // Build modifier function of the filter type.
            switch (EFileAttributeOperator.valueOf(type))
            {
                // Filter : ==
                case eq:
                    ret.add(FileAttributeQueryModifier.getEqInstance(value));
                    break;
                // Filter : !=
                case neq:
                    ret.add(FileAttributeQueryModifier.getNeqInstance(value));
                    break;
                // Filter : LIKE value_1%
                case like:
                    ret.add(FileAttributeQueryModifier.getLikeInstance(value));
                    break;
                // Filter : IS NULL
                case isnull:
                    ret.add(FileAttributeQueryModifier.getIsNullInstance());
                    break;
                // Filter : IS NOT NULL
                case isnotnull:
                    ret.add(FileAttributeQueryModifier.getIsNotNullInstance());
                    break;
                // Filter type unrecognized, throws exception.
                default:
                    throw new Hd3dException("Command not implemented " + type);
            }
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    private IExpressionQueryModifier handleLogicalMap(final Map<String, Object> constraint) throws Hd3dException
    {
        IExpressionQueryModifier ret = null;

        if (isLogicalMap(constraint))
        {
            convertColumnValues(constraint, complexTypeProvider);

            final String logicOperator = getLogicOperator(constraint, query);

            final List<IExpressionQueryModifier> expressionQueryModifiers = handleSubConstraint((List<Map<String, Object>>) getFilter(constraint));

            Criterion crit0;
            Criterion crit1;
            Criterion crit2;
            Criterion crit3;
            switch (EConstraintLogicalOperator.valueOf(logicOperator))
            {
                case AND:
                {
                    if (expressionQueryModifiers.size() < 4)
                    {
                        throw new Hd3dException("Missing operand for AND operator");
                    }
                    crit0 = expressionQueryModifiers.get(0).getExpression();// first name
                    crit1 = expressionQueryModifiers.get(1).getExpression();// first value
                    crit2 = expressionQueryModifiers.get(2).getExpression();// second name
                    crit3 = expressionQueryModifiers.get(3).getExpression();// second value
                    ret = LogicalQueryModifier.getFileAttributeANDinstance(crit0, crit1, crit2, crit3);
                    break;
                }
                case OR:
                {
                    if (expressionQueryModifiers.size() < 4)
                    {
                        throw new Hd3dException("Missing operand for OR operator");
                    }
                    crit0 = expressionQueryModifiers.get(0).getExpression();// first name
                    crit1 = expressionQueryModifiers.get(1).getExpression();// first value
                    crit2 = expressionQueryModifiers.get(2).getExpression();// second name
                    crit3 = expressionQueryModifiers.get(3).getExpression();// second value
                    ret = LogicalQueryModifier.getFileAttributeORinstance(crit0, crit1, crit2, crit3);
                    break;
                }
                case NOT:
                {
                    if (expressionQueryModifiers.size() < 2)
                    {
                        throw new Hd3dException("Missing operand for NOT operator");
                    }
                    crit0 = expressionQueryModifiers.get(0).getExpression();
                    crit1 = expressionQueryModifiers.get(1).getExpression();
                    ret = LogicalQueryModifier.getFileAttributeNOTinstance(crit0, crit1);
                    break;
                }
                    // Filter type unrecognized, throws exception.
                default:
                    throw new Hd3dException("Command not implemented " + logicOperator);
            }
        }

        return ret;
    }

    private List<IExpressionQueryModifier> handleSubConstraint(final List<Map<String, Object>> subConstraints)
            throws Hd3dException
    {
        List<IExpressionQueryModifier> ret = new ArrayList<IExpressionQueryModifier>();

        CollectUtils.removeNull(subConstraints);
        if (CollectUtils.isNotEmpty(subConstraints))
        {
            ret = new ArrayList<IExpressionQueryModifier>();
            processConstraints(ret, subConstraints);
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    private <Q extends IQueryModifier> void processConstraints(final List<Q> modifiers,
            final List<Map<String, Object>> constraints) throws Hd3dException
    {
        for (Map<String, Object> map : constraints)
        {
            if (isLogicalMap(map))
            {
                modifiers.add((Q) handleLogicalMap(map));
            }
            else if (isSimpleConstraint(map))
            {
                modifiers.addAll((Collection<? extends Q>) handleSimpleConstraint(map));
            }
            else
            {
                throw new Hd3dQueryFilterException("Unknown Constraint type:" + map);
            }
        }
    }

    /**
     * After processing, <i>modifiers</i> contains instances of ExpressionQueryModifier like equal, greater than...
     */
    public void handle(final Class<?> persisted, final String query, final IComplexTypeProvider complexTypeProvider)
            throws Hd3dException
    {
        this.query = query;
        this.complexTypeProvider = complexTypeProvider;

        /* this constraint only applies on FileHibernateImple */
        if (persisted != null && persisted.isAssignableFrom(FileRevisionH.class) && allModifiers != null)
        {
            /* need to create an alias */
            allModifiers.add(CreateAliasQueryModifier
                    .getInstance("attributes", "attr", CriteriaSpecification.LEFT_JOIN));

            processConstraints(allModifiers, getNotNullConstraintsFrom(query));
        }
    }
}
