package fr.hd3d.model.persistence.impl.hibernate.criteria.modifier;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;

import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;


/**
 * Add an ordering to the result set to an hibernate query.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class OrderByQueryModifier implements IQueryModifier
{
    private final boolean isAscendant;
    private final String value;

    /**
     * Ascendant ordering
     * 
     * @param column
     *            Column on which the ordering applies.
     * @return
     */
    static public IQueryModifier getAscendantInstance(final String column)
    {
        return new OrderByQueryModifier(true, column);
    }

    /**
     * Descendant ordering
     * 
     * @param column
     *            Column on which the ordering applies.
     * @return
     */
    static public IQueryModifier getDescendantInstance(final String column)
    {
        return new OrderByQueryModifier(false, column);
    }

    /**
     * @param isAscendant
     *            true=ascendant, false=descendant
     * @param column
     *            Column on which the ordering applies.
     */
    public OrderByQueryModifier(final boolean isAscendant, final String column)
    {
        this.isAscendant = isAscendant;
        this.value = column;
    }

    public void applyModifier(final Criteria query) throws Hd3dQueryFilterException
    {
        query.addOrder(isAscendant ? Order.asc(value) : Order.desc(value));
    }

    public void applyModifier(final DetachedCriteria query) throws Hd3dQueryFilterException
    {
        query.addOrder(isAscendant ? Order.asc(value) : Order.desc(value));
    }

    public void applyModifierForCount(Criteria query) throws Hd3dQueryFilterException
    {}

    public void applyModifierForCount(DetachedCriteria query) throws Hd3dQueryFilterException
    {}

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + (isAscendant ? 1231 : 1237);
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        OrderByQueryModifier other = (OrderByQueryModifier) obj;
        if (isAscendant != other.isAscendant)
            return false;
        if (value == null)
        {
            if (other.value != null)
                return false;
        }
        else if (!value.equals(other.value))
            return false;
        return true;
    }
}
