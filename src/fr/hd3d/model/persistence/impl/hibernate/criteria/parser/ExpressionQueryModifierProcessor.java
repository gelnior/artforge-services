package fr.hd3d.model.persistence.impl.hibernate.criteria.parser;

import java.util.Collection;
import java.util.Map;

import fr.hd3d.common.client.Constraint;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryModifiersList;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.ExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.SubQueryPropertyInQueryModifier;


public abstract class ExpressionQueryModifierProcessor
{
    protected QueryModifiersList allModifiers;
    protected Class<?> persisted;
    protected String column;
    protected Object value;

    public QueryModifiersList getAllModifiers()
    {
        return allModifiers;
    }

    public void setAllModifiers(QueryModifiersList allModifiers)
    {
        this.allModifiers = allModifiers;
    }

    public Class<?> getPersisted()
    {
        return persisted;
    }

    public String getColumn()
    {
        return column;
    }

    public Object getValue()
    {
        return value;
    }

    public void setPersisted(Class<?> persisted)
    {
        this.persisted = persisted;
    }

    public void setColumn(String column)
    {
        this.column = column;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }

    abstract IExpressionQueryModifier process() throws Hd3dException;

    /* ">" filter */
    public static class SubQueryPropertyInQueryModifier_Gt extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return SubQueryPropertyInQueryModifier.getGtInstance(persisted, column, value);
        }
    }

    /* "<" filter */
    public static class SubQueryPropertyInQueryModifier_Lt extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return SubQueryPropertyInQueryModifier.getLtInstance(persisted, column, value);
        }
    }

    /* ">=" filter */
    public static class SubQueryPropertyInQueryModifier_Ge extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return SubQueryPropertyInQueryModifier.getGeInstance(persisted, column, value);
        }
    }

    /* "<=" filter */
    public static class SubQueryPropertyInQueryModifier_Le extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return SubQueryPropertyInQueryModifier.getLeInstance(persisted, column, value);
        }
    }

    /* "==" filter */
    public static class SubQueryPropertyInQueryModifier_Eq extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return SubQueryPropertyInQueryModifier.getEqInstance(persisted, column, value);
        }
    }

    /* "!=" filter */
    public static class SubQueryPropertyInQueryModifier_Neq extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return SubQueryPropertyInQueryModifier.getNeqInstance(persisted, column, value);
        }
    }

    /* "BETWEEN" filter, value 1 (start) and value 2 (end) */
    public static class SubQueryPropertyInQueryModifier_Btw extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            /* in case of "between" operator, "value" contains a map with to entries: "start" and "end" */
            final Map<String, ?> betweenMap = (Map<String, ?>) value;
            return SubQueryPropertyInQueryModifier.getBetweenInstance(persisted, column, value, betweenMap
                    .get(Constraint.START_MAP_FIELD), betweenMap.get(Constraint.END_MAP_FIELD));
        }
    }

    /* "IN" filter, in (value 1, value 2, ..., value n) */
    public static class SubQueryPropertyInQueryModifier_In extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            final Collection<?> inSet = (Collection<?>) value;
            return SubQueryPropertyInQueryModifier.getInInstance(column, value, inSet);
        }
    }

    /* "NOTIN" filter, in (value 1, value 2, ..., value n) */
    public static class SubQueryPropertyInQueryModifier_NotIn extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            final Collection<?> notInSet = (Collection<?>) value;
            return SubQueryPropertyInQueryModifier.getNotInInstance(column, value, notInSet);
        }
    }

    /* "LIKE" filter */
    public static class SubQueryPropertyInQueryModifier_Like extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return SubQueryPropertyInQueryModifier.getLikeInstance(column, value);
        }
    }

    /* "ISNULL" filter */
    public static class SubQueryPropertyInQueryModifier_IsNull extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return SubQueryPropertyInQueryModifier.getIsNullInstance(column, value);
        }
    }

    /* "ISNOTNULL" filter */
    public static class SubQueryPropertyInQueryModifier_IsNotNull extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return SubQueryPropertyInQueryModifier.getIsNotNullInstance(column, value);
        }
    }

    public static class ExpressionExpressionQueryModifier_Gt extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return ExpressionQueryModifier.getGtInstance(allModifiers, persisted, column, value);
        }
    }

    public static class ExpressionExpressionQueryModifier_Lt extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return ExpressionQueryModifier.getLtInstance(allModifiers, persisted, column, value);
        }
    }

    public static class ExpressionExpressionQueryModifier_Ge extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return ExpressionQueryModifier.getGeInstance(allModifiers, persisted, column, value);
        }
    }

    public static class ExpressionExpressionQueryModifier_Le extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return ExpressionQueryModifier.getLeInstance(allModifiers, persisted, column, value);
        }
    }

    public static class ExpressionExpressionQueryModifier_Eq extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return ExpressionQueryModifier.getEqInstance(allModifiers, persisted, column, value);
        }
    }

    public static class ExpressionExpressionQueryModifier_Neq extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return ExpressionQueryModifier.getNeqInstance(allModifiers, persisted, column, value);
        }
    }

    public static class ExpressionExpressionQueryModifier_Btw extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            /* in case of "between" operator, "value" contains a map with to entries: "start" and "end" */
            final Map<String, ?> betweenMap = (Map<String, ?>) value;
            return ExpressionQueryModifier.getBetweenInstance(allModifiers, persisted, column, betweenMap
                    .get(Constraint.START_MAP_FIELD), betweenMap.get(Constraint.END_MAP_FIELD));
        }
    }

    public static class ExpressionExpressionQueryModifier_In extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            final Collection<?> inSet = (Collection<?>) value;
            return ExpressionQueryModifier.getInInstance(allModifiers, persisted, column, inSet);
        }
    }

    public static class ExpressionExpressionQueryModifier_NotIn extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            final Collection<?> notInSet = (Collection<?>) value;
            return ExpressionQueryModifier.getNotInInstance(allModifiers, persisted, column, notInSet);
        }
    }

    public static class ExpressionExpressionQueryModifier_Like extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return ExpressionQueryModifier.getLikeInstance(allModifiers, persisted, column, value);
        }
    }

    public static class ExpressionExpressionQueryModifier_IsNull extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return ExpressionQueryModifier.getIsNullInstance(allModifiers, persisted, column);
        }
    }

    public static class ExpressionExpressionQueryModifier_IsNotNull extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process()
        {
            return ExpressionQueryModifier.getIsNotNullInstance(allModifiers, persisted, column);
        }
    }

    public static class ExpressionExpressionQueryModifier_InSequence extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process() throws Hd3dException
        {
            return ExpressionQueryModifier.getInSequenceInstance(value);
        }
    }

    public static class ExpressionExpressionQueryModifier_InSequenceShots extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process() throws Hd3dException
        {
            return ExpressionQueryModifier.getInSequenceShotsInstance(value);
        }
    }

    public static class ExpressionExpressionQueryModifier_HasTag extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process() throws Hd3dException
        {
            return ExpressionQueryModifier.getHasTagInstance(value);
        }
    }

    public static class ExpressionExpressionQueryModifier_HasNotTag extends ExpressionQueryModifierProcessor
    {
        @Override
        IExpressionQueryModifier process() throws Hd3dException
        {
            return ExpressionQueryModifier.getHasNotTagInstance(value);
        }
    }

}
