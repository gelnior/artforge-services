package fr.hd3d.model.persistence.impl.hibernate.criteria;

import java.util.List;

import org.hibernate.criterion.Criterion;


/**
 * @author Try LAM
 */
public interface IExpressionQueryModifier extends IQueryModifier
{
    public Criterion getExpression();

    public List<IQueryModifier> getAliases();
}
