package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import org.hibernate.Criteria;

import fr.hd3d.services.resources.ResourceContext;


public class CreateCriteriaTransformer extends AbstractQueryTransformer<Criteria, Criteria>
{
    String critera = null;

    public CreateCriteriaTransformer(String aCriterion)
    {
        super();
        this.critera = aCriterion;
    }

    @SuppressWarnings("unchecked")
    public CreateCriteriaTransformer(ResourceContext context, String aCriterion)
    {
        super(context);
        this.critera = aCriterion;
    }

    public Criteria transform(Criteria query)
    {
        Criteria ret = null;

        if (critera != null)
        {
            ret = query.createCriteria(critera);
        }

        return ret;
    }
}
