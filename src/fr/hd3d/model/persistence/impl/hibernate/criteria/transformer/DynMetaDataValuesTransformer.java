package fr.hd3d.model.persistence.impl.hibernate.criteria.transformer;

import static fr.hd3d.utils.Const.CLASSDYN;
import static fr.hd3d.utils.Const.CLASSDYNMETATDATTYPE;
import static fr.hd3d.utils.Const.CLASSDYN_ID;
import static fr.hd3d.utils.Const.PARENT;
import static fr.hd3d.utils.Const.PARENTTYPE;
import static org.hibernate.criterion.Restrictions.eq;
import static org.hibernate.criterion.Restrictions.in;

import java.util.List;

import org.hibernate.Criteria;

import fr.hd3d.model.persistence.IBase;
import fr.hd3d.services.resources.ResourceContext;


public class DynMetaDataValuesTransformer extends AbstractQueryTransformer<Criteria, Criteria>
{
    IBase object = null;
    List<Long> ids = null;
    String entityName = null;
    Long classDynMetaDataId = null;

    public DynMetaDataValuesTransformer(IBase object, Long classDynMetaDataId)
    {
        super();
        this.object = object;
        this.classDynMetaDataId = classDynMetaDataId;
    }

    public DynMetaDataValuesTransformer(List<Long> ids, String entityName, Long classDynMetaDataId)
    {
        super();
        this.ids = ids;
        this.entityName = entityName;
        this.classDynMetaDataId = classDynMetaDataId;
    }

    public DynMetaDataValuesTransformer(ResourceContext<?, ?> context, IBase object, Long classDynMetaDataId)
    {
        super(context);
        this.object = object;
        this.classDynMetaDataId = classDynMetaDataId;
    }

    public DynMetaDataValuesTransformer(ResourceContext<?, ?> context, List<Long> ids, String entityName,
            Long classDynMetaDataId)
    {
        super(context);
        this.ids = ids;
        this.entityName = entityName;
        this.classDynMetaDataId = classDynMetaDataId;
    }

    public Criteria transform(Criteria query)
    {
        /* single object query */
        if (object != null)
        {
            query.add(eq(PARENT, object.getId()));
            if (classDynMetaDataId == null)
            {
                query.add(eq(PARENTTYPE, object.entityName()));
            }

            if (classDynMetaDataId != null)
            {
                query.createAlias(CLASSDYNMETATDATTYPE, CLASSDYN);
                query.add(eq(CLASSDYN_ID, classDynMetaDataId));
            }
        }
        /* multiple objects query */
        else if (ids != null)
        {
            query.add(in(PARENT, ids));

            if (classDynMetaDataId == null && entityName != null)
            {
                query.add(eq(PARENTTYPE, entityName));
            }

            if (classDynMetaDataId != null)
            {
                query.createAlias(CLASSDYNMETATDATTYPE, CLASSDYN);
                query.add(eq(CLASSDYN_ID, classDynMetaDataId));
            }
        }

        return query;
    }
}
