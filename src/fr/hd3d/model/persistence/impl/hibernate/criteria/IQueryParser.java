package fr.hd3d.model.persistence.impl.hibernate.criteria;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.IComplexTypeProvider;


/**
 * Base interface for all xxxQueryParsers&#46; A QueryParser parses a query string into IQueryModifiers which are then
 * applied to Hibernate Criteria
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public interface IQueryParser
{
    /**
     * parses <i>query</i> and populate <i>modifiers</i> with corresponding IQueryModifiers
     * 
     * @param modifiers
     *            contains collections of IQueryModifier resulting from the query parsing
     * @param query
     *            query to parse
     * @param enumProvider
     * @throws Hd3dException
     */
    void handle(Class<?> persisted, String query, IComplexTypeProvider enumProvider) throws Hd3dException;

    QueryModifiersList getAllModifiers();

    void setAllModifiers(QueryModifiersList allModifiers);
}
