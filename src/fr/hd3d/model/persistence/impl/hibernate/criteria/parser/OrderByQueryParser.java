package fr.hd3d.model.persistence.impl.hibernate.criteria.parser;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.IComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.ExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.OrderByQueryModifier;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.utils.CollectUtils;


/**
 * Parse ordering (ascendant, descendant) query to build an "order by" modifier.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class OrderByQueryParser extends AbstractQueryParser
{
    /**
     * after processing, <i>modifiers</i> contains instances of OrderByQueryModifier
     */
    public void handle(Class<?> persisted, String query, IComplexTypeProvider enumProvider) throws Hd3dException
    {
        final List<String> orderByColumns = getNotNullColumnsList(query);

        for (String aColumn : orderByColumns)
        {
            final String attribute = getAttributeFrom(aColumn);

            CollectUtils.addAllIfNotEmpty(allModifiers, ExpressionQueryModifier.createAliases(allModifiers, persisted,
                    attribute));

            CollectUtils.addIfNotNull(allModifiers, getModifier(isOrderedByAscending(aColumn), attribute));
        }
    }

    private List<String> getNotNullColumnsList(final String query)
    {
        List<String> ret = new ArrayList<String>();

        final List<String> columns = Hd3dJsonSerializer.<List<String>> unserialize(query);
        CollectUtils.removeNull(columns);
        CollectUtils.addAllIfNotEmpty(ret, columns);

        return ret;
    }

    private String getAttributeFrom(final String string)
    {
        final boolean ascending = isOrderedByAscending(string);
        return ascending ? string : string.substring(1);
    }

    private boolean isOrderedByAscending(final String string)
    {
        return string.charAt(0) != '-';
    }

    private boolean attributeExists(Class<?> persisted, String attribute)
    {
        return EntitiesMaps.withClass(persisted).getTypeOf(attribute) != null;
    }

    private IQueryModifier getModifier(final boolean ascending, final String attribute)
    {
        return ascending ? OrderByQueryModifier.getAscendantInstance(attribute) : OrderByQueryModifier
                .getDescendantInstance(attribute);
    }

}
