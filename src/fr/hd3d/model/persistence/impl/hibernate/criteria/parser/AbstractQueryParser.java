package fr.hd3d.model.persistence.impl.hibernate.criteria.parser;

import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import net.sf.sojo.interchange.json.JsonParserException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

import fr.hd3d.common.client.Constraint;
import fr.hd3d.common.client.LogicConstraint;
import fr.hd3d.model.persistence.impl.hibernate.IComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryParser;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryModifiersList;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


public abstract class AbstractQueryParser implements IQueryParser
{
    QueryModifiersList allModifiers = null;

    public QueryModifiersList getAllModifiers()
    {
        return allModifiers;
    }

    public void setAllModifiers(QueryModifiersList allModifiers)
    {
        this.allModifiers = allModifiers;
    }

    /**
     * Handle complex types, (example : for an enumeration retrieve the enumeration value, not the index value).
     * 
     * @param constraint
     *            The values to "translate"
     * @param complexTypeProvider
     *            The provider to handle specific complex type.
     * @throws Hd3dQueryFilterException
     *             If values are not valid.
     */
    @SuppressWarnings("unchecked")
    protected static void tweakMap(final Map<String, Object> constraint, final IComplexTypeProvider complexTypeProvider)
            throws Hd3dQueryFilterException
    {

        final String columnName = (String) constraint.get(Constraint.COLUMN_MAP_FIELD);
        if (!complexTypeProvider.isValidTypeField(columnName))
            return;

        final Object value = constraint.get(Constraint.VALUE_MAP_FIELD);

        if (value instanceof String)
        {
            /* For a string get directly, the right value. */
            constraint.put(Constraint.VALUE_MAP_FIELD, getComplexType(complexTypeProvider, columnName, (String) value));
        }
        else if (value instanceof List)
        {
            /* For a list deal with each element. */
            final List<Object> list = (List<Object>) value;
            final ListIterator<Object> iterator = list.listIterator();

            /*
             * Replacing the strings with the Enum version delegating the Enum retrieval to the IEnumProvider
             */
            while (iterator.hasNext())
                iterator.set(getComplexType(complexTypeProvider, columnName, (String) iterator.next()));
        }
        else if (value instanceof Map)
        {
            /* For a map deal with the tweakedValue. */
            final Map<String, Object> values = (Map<String, Object>) value;

            Set<Map.Entry<String, Object>> entries = values.entrySet();
            for (Map.Entry<String, Object> entry : entries)
            {
                String tweakedValue = entry.getValue().toString();
                values.put(entry.getKey(), getComplexType(complexTypeProvider, columnName, tweakedValue));
            }
        }
    }

    /**
     * Handles complex types like enumerations.
     * 
     * @param complexTypeProvider
     *            Object which handles complex Types for a specific resource
     * @param field
     *            The field to
     * @param value
     *            The value of the field.
     * @return The real expected field value. (Example : For an enumeration : { 1 = open, 2 = closed }, Further than
     *         retrieving 1 or 2, getCompexType will return "open" or "closed")
     * @throws Hd3dQueryFilterException
     *             Throws exception if the value is not valid.
     */
    protected static Object getComplexType(final IComplexTypeProvider complexTypeProvider, String field, String value)
            throws Hd3dQueryFilterException
    {
        try
        {
            /* Get value from the appropriate provider. */
            return complexTypeProvider.getValue(field, value);
        }
        catch (Exception e)
        {
            final StringBuilder buffer = new StringBuilder(e.getMessage());
            buffer.append(SystemUtils.LINE_SEPARATOR);

            /*
             * Append to the exception message the valid choices list for the field
             */
            final List<String> validEnum = complexTypeProvider.getValidEnum(field);
            buffer.append("Valid enums for field '" + field + "' are" + SystemUtils.LINE_SEPARATOR);
            for (String string : validEnum)
                buffer.append("* ").append(string).append(SystemUtils.LINE_SEPARATOR);

            throw new Hd3dQueryFilterException(buffer.toString());
        }
    }

    protected boolean isLogicalMap(Map<String, Object> constraintMap)
    {
        return constraintMap != null && constraintMap.get(LogicConstraint.LOGIC) != null;
    }

    protected boolean isSimpleConstraint(Map<String, Object> constraintMap)
    {
        return constraintMap != null && constraintMap.get(LogicConstraint.LOGIC) == null;
    }

    protected List<Map<String, Object>> getNotNullConstraintsFrom(final String query) throws Hd3dQueryFilterException
    {
        List<Map<String, Object>> ret;
        try
        {
            ret = Hd3dJsonSerializer.<List<Map<String, Object>>> unserialize(query);
            CollectUtils.removeNull(ret);
        }
        catch (JsonParserException e)
        {
            Log.LOGGER.error("a List<Map<String, Object>> is expected in JSON");
            Log.LOGGER.error(ExceptUtils.format(e));
            ret = Collections.emptyList();
        }
        catch (Exception e)
        {
            throw new Hd3dQueryFilterException(query + SystemUtils.LINE_SEPARATOR + e.getMessage());
        }

        return ret;
    }

    protected Object getFilter(final Map<String, Object> constraintMap) throws Hd3dQueryFilterException
    {
        return constraintMap.get(LogicConstraint.FILTER);
    }

    protected String getLogicOperator(final Map<String, Object> constraintMap, final String query)
            throws Hd3dQueryFilterException
    {
        final String logicOperator = (String) constraintMap.get(LogicConstraint.LOGIC);
        if (logicOperator == null)
        {
            throw new Hd3dQueryFilterException(query);
        }
        return logicOperator;
    }

    protected String getColumn(final Map<String, Object> constraintMap, final String query)
            throws Hd3dQueryFilterException
    {
        final String column = (String) constraintMap.get(Constraint.COLUMN_MAP_FIELD);
        if (StringUtils.isBlank(column))
        {
            throw new Hd3dQueryFilterException("Column field is invalid:" + query);
        }
        return column;
    }

    protected String getType(final Map<String, Object> constraintMap, final String query)
            throws Hd3dQueryFilterException
    {
        final String type = (String) constraintMap.get(Constraint.TYPE_MAP_FIELD);
        if (StringUtils.isBlank(type))
        {
            throw new Hd3dQueryFilterException("Type field is invalid:" + query);
        }
        return type;
    }

    protected Object getValueOrNull(final Map<String, Object> constraintMap, final String query)
            throws Hd3dQueryFilterException
    {
        /* Null may be acceptable */
        return constraintMap.get(Constraint.VALUE_MAP_FIELD);
    }

    protected Object getNotNullValue(final Map<String, Object> constraintMap, final String query)
            throws Hd3dQueryFilterException
    {
        final Object value = getValueOrNull(constraintMap, query);
        if (value == null)
        {
            throw new Hd3dQueryFilterException("Value field is invalid:" + query);
        }
        return value;
    }

    protected String getName(final Map<String, Object> constraintMap, final String query)
            throws Hd3dQueryFilterException
    {
        final String name = (String) constraintMap.get(Constraint.NAME_MAP_FIELD);
        if (StringUtils.isBlank(name))
        {
            throw new Hd3dQueryFilterException("Name field is invalid:" + query);
        }
        return name;
    }

    protected <Q extends IQueryModifier> void updateModififers(final List<Q> modifiers, final List<Q> newModifiers)
    {
        if (modifiers != null && newModifiers != null)
        {
            CollectUtils.removeNull(modifiers);
            CollectUtils.removeNull(newModifiers);
            for (Q q : newModifiers)
            {
                if (!modifiers.contains(q))/* avoid things such as "duplicate alias" */
                {
                    modifiers.add(q);
                }
            }
        }
    }

    protected static void convertColumnValues(final Map<String, Object> constraintMap,
            final IComplexTypeProvider complexTypeProvider) throws Hd3dQueryFilterException
    {
        if (complexTypeProvider != null)
        {
            tweakMap(constraintMap, complexTypeProvider);
        }
    }
}
