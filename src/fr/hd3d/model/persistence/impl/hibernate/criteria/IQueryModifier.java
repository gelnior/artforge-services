package fr.hd3d.model.persistence.impl.hibernate.criteria;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;


/**
 * Base interface for all QueryModifier classes altering Hibernate's Criteria.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public interface IQueryModifier
{
    /**
     * Alter <i>query</i> with the QueryModifier's modification.
     * 
     * @param query
     *            Hibernate Criteria
     * @throws Hd3dQueryFilterException
     *             When failed throws
     */
    void applyModifier(Criteria query) throws Hd3dQueryFilterException;

    void applyModifier(DetachedCriteria query) throws Hd3dQueryFilterException;

    void applyModifierForCount(Criteria query) throws Hd3dQueryFilterException;

    void applyModifierForCount(DetachedCriteria query) throws Hd3dQueryFilterException;
}
