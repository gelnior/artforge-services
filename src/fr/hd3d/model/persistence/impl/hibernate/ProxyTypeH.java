package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILProxyType;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


@Entity
@Table(name = "hd3d_proxy_type")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ProxyTypeH extends BaseH implements IProxyType
{

    private static final long serialVersionUID = 1L;

    private String name;
    private String openType;
    private String description;

    private Collection<IProxy> proxies;

    public ProxyTypeH()
    {}

    public ProxyTypeH(final String name, final String openType, final String description)
    {
        super();
        this.name = name;
        this.description = description;
        this.openType = openType;
    }

    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_proxytype"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    @Column(name = "proxy_type_name")
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Column(name = "proxy_type_opentype")
    public String getOpenType()
    {
        return openType;
    }

    public void setOpenType(String openType)
    {
        this.openType = openType;
    }

    @Column(name = "proxy_type_description")
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @OneToMany(targetEntity = ProxyH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "proxyType")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @Fetch(value = FetchMode.JOIN)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @BatchSize(size = 50)
    public Collection<IProxy> getProxies()
    {
        if (proxies == null)
        {
            return new HashSet<IProxy>();
        }
        return proxies;
    }

    public void setProxies(Collection<IProxy> proxies)
    {
        this.proxies = proxies;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();

        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((openType == null) ? 0 : openType.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!super.equals(obj))
            return false;

        if (getClass() != obj.getClass())
            return false;
        ProxyTypeH other = (ProxyTypeH) obj;

        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (openType == null)
        {
            if (other.openType != null)
                return false;
        }
        else if (!openType.equals(other.openType))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;

        return true;
    }

    public String toString()
    {
        return new ToStringBuilder(this).append("id", id).append("version", version).append("name", name).append(
                "open type", openType).append("description", description).toString();
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILProxyType, IProxyType> defaultTranslator()
    {
        return Translators.proxytype;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

}
