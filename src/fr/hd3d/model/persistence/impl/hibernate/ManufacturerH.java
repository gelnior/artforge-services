package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.model.lightweight.ILManufacturer;
import fr.hd3d.model.persistence.IComputerModel;
import fr.hd3d.model.persistence.IDeviceModel;
import fr.hd3d.model.persistence.IManufacturer;
import fr.hd3d.model.persistence.IScreenModel;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * Manufacturer hibernate object needed for database persistance.
 * 
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_manufacturer")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ManufacturerH extends SimpleH implements IManufacturer
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Set<IDeviceModel> deviceModels;
    private Set<IComputerModel> computerModels;
    private Set<IScreenModel> screenModels;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_manufacturer"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ManufacturerH()
    {}

    public ManufacturerH(Long id, Timestamp version, String name)
    {
        super(id, version, name);
    }

    public ManufacturerH(String name)
    {
        this(null, null, name);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @OneToMany(targetEntity = DeviceModelH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "manufacturer")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IDeviceModel> getDeviceModels()
    {
        if (this.deviceModels == null)
        {
            this.deviceModels = new HashSet<IDeviceModel>();
        }
        return this.deviceModels;
    }

    public void setDeviceModels(Set<IDeviceModel> deviceModels)
    {
        this.deviceModels = deviceModels;
    }

    @OneToMany(targetEntity = ComputerModelH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "manufacturer")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IComputerModel> getComputerModels()
    {
        if (this.computerModels == null)
        {
            this.computerModels = new HashSet<IComputerModel>();
        }
        return this.computerModels;
    }

    public void setComputerModels(Set<IComputerModel> computerModels)
    {
        this.computerModels = computerModels;
    }

    @OneToMany(targetEntity = ScreenModelH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "manufacturer")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IScreenModel> getModels()
    {
        if (this.screenModels == null)
        {
            this.screenModels = new HashSet<IScreenModel>();
        }
        return this.screenModels;
    }

    public void setModels(Set<IScreenModel> screenModels)
    {
        this.screenModels = screenModels;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ManufacturerH other = (ManufacturerH) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    public IBaseTranslator<ILManufacturer, IManufacturer> defaultTranslator()
    {
        return Translators.manufacturer;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }
    //
    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("deviceModelIds", StringUtils.join(CollectUtils.getIds(getDeviceModels()), ','));
    // b.append("computerModelIds", StringUtils.join(CollectUtils.getIds(getComputerModels()), ','));
    // b.append("screenModelIds", StringUtils.join(CollectUtils.getIds(getModels()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
