package fr.hd3d.model.persistence.impl.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Transient;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.impl.hibernate.query.GetCollectionByExampleCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.GetCollectionByIdsCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.GetCollectionByValueCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.GetCollectionByValueInCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.GetCollectionByValuesEqCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.GetCollectionByValuesInCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.GetCollectionCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.GetCollectionCount;
import fr.hd3d.model.persistence.impl.hibernate.query.GetObject;
import fr.hd3d.model.persistence.impl.hibernate.query.GetObjectByValueCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.model.persistence.impl.hibernate.query.LoadObject;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;


/**
 * Provides CRUD methods on HIBERNATE persisted objects.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 * 
 * @param <E>
 *            retrieved object interface.
 * @param <F>
 *            Hibernate annotated persisted class
 */
public class HibernatePersist<E extends IBase, F extends E> implements IPersist<E>
{
    final Class<F> persistedClass;// persisted class bound to this provider

    static List<Integer> allowedInternalStatus = null;

    private static final String MSG_0 = "ENTITY HAS BEEN DISABLED";

    /**
     * 
     * @param persistedClass
     *            Hibernate annotated persisted class
     */
    public HibernatePersist(final Class<F> persistedClass)
    {
        this.persistedClass = persistedClass;
    }

    public synchronized static List<Integer> getAllowedInternalStatus()
    {
        if (allowedInternalStatus == null)
        {
            allowedInternalStatus = new ArrayList<Integer>();
            allowedInternalStatus.add(fr.hd3d.common.client.Const.INTERNALSTATUS_ENABLED);
        }

        return allowedInternalStatus;
    }

    private static void setSessionParameters(Session session)
    {
        if (session != null)
        {
            /* list of all valid internalStatus to retrieve */
            session.enableFilter(Const.INTERNAL_STATUS).setParameterList(Const.INTERNAL_STATUS,
                    getAllowedInternalStatus());
            session.setFlushMode(FlushMode.MANUAL);// if read-only, better perf
        }
    }

    /* Sub Criteria */
    // public DetachedCriteria basicSubCriteria(final org.hibernate.Session session)
    // {
    // return basicSubCriteria(session, getPersistedClass());
    // }
    //
    // public static DetachedCriteria basicSubCriteria(final org.hibernate.Session session, final Class<?> clazz)
    // {
    // /* NOTE: ugly to make session do stuff here, though only a Detached Criteria is returned. */
    // setSessionParameters(session);
    //
    // /* detachedCriteria is a sub-query */
    // final DetachedCriteria subCriteria = DetachedCriteria.forClass(clazz, "main");// give it an alias
    // subCriteria.setProjection(Projections.distinct(Projections.id()));
    // subCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
    //
    // return subCriteria;
    // }
    //
    // /* Criteria */
    // public Criteria basicCriteria(final Session session, final DetachedCriteria dc)
    // {
    // return basicCriteria(session, dc, getPersistedClass());
    // }
    public Criteria basicCriteria(final Session session)
    {
        return basicCriteria(session, getPersistedClass());
    }

    // public static Criteria basicCriteria(final Session session, final DetachedCriteria subCriteria, final Class<?>
    // clazz)
    // {
    // setSessionParameters(session);
    //
    // final Criteria criteria = session.createCriteria(clazz);
    // // criteria.add(Subqueries.propertyIn(Const.ID, subCriteria));
    // criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
    // // criteria.setCacheable(true);
    // // criteria.setCacheRegion("queryCache");
    // // criteria.setCacheMode(CacheMode.GET);
    // return criteria;
    // }

    public static Criteria basicCriteria(final Session session, final Class<?> clazz)
    {
        setSessionParameters(session);

        final Criteria criteria = session.createCriteria(clazz);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria;
    }

    public Criteria basicIdCriteria(final Session session, final Long id)
    {
        return basicIdCriteria(session, id, getPersistedClass());
    }

    public static Criteria basicIdCriteria(final Session session, final Long id, final Class<?> clazz)
    {
        setSessionParameters(session);

        final Criteria criteria = session.createCriteria(clazz);
        criteria.add(Restrictions.idEq(id));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return criteria;
    }

    public static class ResultCollection<A>
    {
        private List<A> result;
        private List<Long> allIds;
        private int count;

        public ResultCollection(final List<A> result, final int count)
        {
            this.result = result;
            this.count = count;
        }

        public ResultCollection(final List<A> result, final List<Long> allIds, final int count)
        {
            this.result = result;
            this.allIds = allIds;
            this.count = count;
        }

        public List<A> getResult()
        {
            return result;
        }

        public int getCount()
        {
            return count;
        }

        public void setResult(final List<A> result)
        {
            this.result = result;
        }

        public void setCount(int count)
        {
            this.count = count;
        }

        public List<Long> getAllIds()
        {
            return allIds;
        }

        public void setAllIds(List<Long> allIds)
        {
            this.allIds = allIds;
        }

        public boolean isEmpty()
        {
            return CollectionUtils.isEmpty(result);
        }

        public boolean isNotEmpty()
        {
            return !isEmpty();
        }

        public int size()
        {
            return result == null ? 0 : result.size();
        }

        public void clear()
        {
            if (result != null)
            {
                result.clear();
                result = null;
            }
            if (allIds != null)
            {
                allIds.clear();
                allIds = null;
            }
            count = 0;
        }
    }

    @Transient
    public Criterion getCriterionEq(final String[] properties, final Object[] values, final String operator)
    {
        Criterion ret = null;

        if (properties.length == 1)
        {
            ret = getEqOrNullCriterion(properties[0], values[0]);
        }
        else if (operator.equalsIgnoreCase("AND"))
        {
            Junction conjunction = Restrictions.conjunction();
            for (int i = 0; i < properties.length; i++)
            {
                conjunction.add(getEqOrNullCriterion(properties[i], values[i]));
            }
            ret = conjunction;
        }
        else
        {
            Junction disjunction = Restrictions.disjunction();
            for (int i = 0; i < properties.length; i++)
            {
                disjunction.add(getEqOrNullCriterion(properties[i], values[i]));
            }
            ret = disjunction;
        }

        return ret;
    }

    private Criterion getEqOrNullCriterion(String property, Object value)
    {
        return value == null ? Restrictions.isNull(property) : Restrictions.eq(property, value);
    }

    @Transient
    public Criterion getCriterionIn(final String[] properties, final Object[] values, final String operator)
    {
        Criterion ret = null;

        if (properties.length == 1)
        {
            ret = Restrictions.eq(properties[0], values[0]);
        }
        else if (operator.equalsIgnoreCase("AND"))
        {
            Junction conjunction = Restrictions.conjunction();
            for (int i = 0; i < properties.length; i++)
            {
                conjunction.add(Restrictions.in(properties[i], (Object[]) values[i]));
            }
            ret = conjunction;
        }
        else
        {
            Junction disjunction = Restrictions.disjunction();
            for (int i = 0; i < properties.length; i++)
            {
                disjunction.add(Restrictions.in(properties[i], (Object[]) values[i]));
            }
            ret = disjunction;
        }

        return ret;
    }

    /**
     * returns all the objects of the bound persisted class
     * 
     * @return
     * @throws Hd3dException
     */
    public List<E> getAllCollection() throws Hd3dException
    {
        final GetCollectionCmd<E> cmd = new GetCollectionCmd<E>();
        cmd.getContext().setPersistor(this);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getAllCollection_NoPermCheck() throws Hd3dException
    {
        final GetCollectionCmd<E> cmd = new GetCollectionCmd<E>();
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public ResultCollection<E> getCollection(final ResourceContext<?, ?> context) throws Hd3dException
    {
        /* permissionOn = true & idOnly= false */
        return getCollection(context, false);
    }

    public ResultCollection<E> getCollection(final ResourceContext<?, ?> context, ProjectionList projectionList)
            throws Hd3dException
    {
        final GetCollectionCmd<E> cmd = new GetCollectionCmd<E>(context);
        cmd.getContext().setPersistor(this);
        cmd.setProjectionList(projectionList);
        HibernateUtil.safeQuery(cmd);
        return new ResultCollection<E>(cmd.result, cmd.allIds, cmd.totalCount);
    }

    public ResultCollection<E> getCollection(final ResourceContext<?, ?> context, boolean idOnly) throws Hd3dException
    {
        final GetCollectionCmd<E> cmd = new GetCollectionCmd<E>(context);
        cmd.getContext().setPersistor(this);
        cmd.setIdOnly(idOnly);
        HibernateUtil.safeQuery(cmd);
        return new ResultCollection<E>(cmd.result, cmd.allIds, cmd.totalCount);
    }

    public ResultCollection<E> getCollectionWithTotal(final ResourceContext<?, ?> context) throws Hd3dException
    {
        /* permissionOn = true & idOnly= false */
        return getCollectionWithTotal(context, false);
    }

    public ResultCollection<E> getCollectionWithTotal(final ResourceContext<?, ?> context, ProjectionList projectionList)
            throws Hd3dException
    {
        final GetCollectionCmd<E> cmd = new GetCollectionCmd<E>(context);
        cmd.getContext().setPersistor(this);
        cmd.setProjectionList(projectionList);
        cmd.wantTotalSize();
        HibernateUtil.safeQuery(cmd);
        return new ResultCollection<E>(cmd.result, cmd.allIds, cmd.totalCount);
    }

    public ResultCollection<E> getCollectionWithTotal(final ResourceContext<?, ?> context, boolean idOnly)
            throws Hd3dException
    {
        final GetCollectionCmd<E> cmd = new GetCollectionCmd<E>(context);
        cmd.getContext().setPersistor(this);
        cmd.setIdOnly(idOnly);
        cmd.wantTotalSize();
        HibernateUtil.safeQuery(cmd);
        return new ResultCollection<E>(cmd.result, cmd.allIds, cmd.totalCount);
    }

    public ResultCollection<E> getCollection_NoPermCheck(final ResourceContext<?, ?> context) throws Hd3dException
    {
        return getCollection_NoPermCheck(context, false);
    }

    public ResultCollection<E> getCollection_NoPermCheck(final ResourceContext<?, ?> context, boolean idOnly)
            throws Hd3dException
    {
        final GetCollectionCmd<E> cmd = new GetCollectionCmd<E>(context);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        cmd.setIdOnly(idOnly);
        HibernateUtil.safeQuery(cmd);
        return new ResultCollection<E>(cmd.result, cmd.allIds, cmd.totalCount);
    }

    public ResultCollection<E> getCollection_NoPermCheck(final ResourceContext<?, ?> context,
            ProjectionList projectionList) throws Hd3dException
    {
        final GetCollectionCmd<E> cmd = new GetCollectionCmd<E>(context);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        cmd.setProjectionList(projectionList);
        HibernateUtil.safeQuery(cmd);
        return new ResultCollection<E>(cmd.result, cmd.allIds, cmd.totalCount);
    }

    @Deprecated
    public int getCollectionCount(final ResourceContext<?, ?> context) throws Hd3dException
    {
        final GetCollectionCount<E> cmd = new GetCollectionCount<E>(new ResourceContext());
        cmd.getContext().setPersistor(this);
        HibernateUtil.safeQuery(cmd);
        return cmd.getResult();
    }

    /**
     * returns a list of objects with the given ids, WITH Permissions check
     */
    public List<E> getByIds(final ResourceContext<?, ?> context, final Collection<Long> ids, final LockMode lockMode)
            throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByIds_NoPermCheck(context, ids, lockMode));
    }

    public List<E> getByIds(final ResourceContext<?, ?> context, final Collection<Long> ids) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByIds_NoPermCheck(context, ids));
    }

    public List<E> getByIds(final Collection<Long> ids) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByIds_NoPermCheck(ids));
    }

    public List<E> getByIds(final Collection<Long> ids, final LockMode lockMode) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByIds_NoPermCheck(ids, lockMode));
    }

    /**
     * returns a list of objects with the given ids, WITHOUT Permissions check
     * 
     * @param pager
     * @param ids
     * @return
     * @throws Hd3dException
     */
    public List<E> getByIds_NoPermCheck(final ResourceContext<?, ?> context, final Collection<Long> ids)
            throws Hd3dException
    {
        return getByIds_NoPermCheck(context, ids, null);
    }

    public List<E> getByIds_NoPermCheck(final ResourceContext<?, ?> context, final Collection<Long> ids,
            final LockMode lockMode) throws Hd3dException
    {
        List<E> ret = new ArrayList<E>();// do not return an unmodifiable list, which cannot be "clear()ed"

        if (CollectUtils.isNotEmpty(ids))
        {
            final GetCollectionByIdsCmd<E> cmd;
            if (context != null)
            {
                cmd = new GetCollectionByIdsCmd<E>(context, ids);
            }
            else
            {
                cmd = new GetCollectionByIdsCmd<E>(ids);
            }
            cmd.getContext().setPersistor(this);
            cmd.setPermissionOn(false);
            if (lockMode != null)
            {
                cmd.setLockMode(lockMode);
            }
            HibernateUtil.safeQuery(cmd);
            ret.addAll(cmd.result);
        }

        return ret;
    }

    public List<E> getByIds_NoPermCheck(final Collection<Long> ids) throws Hd3dException
    {
        return getByIds_NoPermCheck(ids, null);
    }

    public List<E> getByIds_NoPermCheck(final Collection<Long> ids, final LockMode lockMode) throws Hd3dException
    {
        List<E> ret = new ArrayList<E>();// do not return an unmodifiable list, which cannot be "clear()ed"

        if (CollectUtils.isNotEmpty(ids))
        {
            final GetCollectionByIdsCmd<E> cmd = new GetCollectionByIdsCmd<E>(ids);
            cmd.getContext().setPersistor(this);
            cmd.setPermissionOn(false);
            if (lockMode != null)
            {
                cmd.setLockMode(lockMode);
            }
            HibernateUtil.safeQuery(cmd);
            ret.addAll(cmd.result);
        }

        return ret;
    }

    /**
     * returns a list of objects with property=value, WITH Permissions check
     */
    public List<E> getByValue(final ResourceContext<?, ?> context, final String property, final Object value)
            throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValue_NoPermCheck(context, property, value));
    }

    public List<E> getByValue(final String property, final Object value) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValue_NoPermCheck(property, value));
    }

    public List<E> getByValue_NoPermCheck(final ResourceContext<?, ?> context, final String property, final Object value)
            throws Hd3dException
    {
        final GetCollectionByValueCmd<E> cmd;
        if (context != null)
        {
            cmd = new GetCollectionByValueCmd<E>(context, property, value);
        }
        else
        {
            cmd = new GetCollectionByValueCmd<E>(property, value);
        }
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getByValue_NoPermCheck(final String property, final Object value) throws Hd3dException
    {
        final GetCollectionByValueCmd<E> cmd = new GetCollectionByValueCmd<E>(property, value);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getByValue(final ResourceContext<?, ?> context, final String property, final Object value,
            LockMode lockMode) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValue_NoPermCheck(context, property, value, lockMode));
    }

    public List<E> getByValue(final String property, final Object value, LockMode lockMode) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValue_NoPermCheck(property, value, lockMode));
    }

    public List<E> getByValue_NoPermCheck(final ResourceContext<?, ?> context, final String property,
            final Object value, LockMode lockMode) throws Hd3dException
    {
        final GetCollectionByValueCmd<E> cmd;
        if (context != null)
        {
            cmd = new GetCollectionByValueCmd<E>(context, property, value);
        }
        else
        {
            cmd = new GetCollectionByValueCmd<E>(property, value);
        }
        cmd.setLockMode(lockMode);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getByValue_NoPermCheck(final String property, final Object value, LockMode lockMode)
            throws Hd3dException
    {
        final GetCollectionByValueCmd<E> cmd = new GetCollectionByValueCmd<E>(property, value);
        cmd.setLockMode(lockMode);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getByValueIn(final ResourceContext<?, ?> context, final String property, final List<?> values)
            throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValueIn_NoPermCheck(context, property, values));
    }

    public List<E> getByValueIn(final String property, final List<?> values) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValueIn_NoPermCheck(property, values));
    }

    public List<E> getByValueIn_NoPermCheck(final ResourceContext<?, ?> context, final String property,
            final List<?> values) throws Hd3dException
    {
        final GetCollectionByValueInCmd<E> cmd;
        if (context != null)
        {
            cmd = new GetCollectionByValueInCmd<E>(context, property, values);
        }
        else
        {
            cmd = new GetCollectionByValueInCmd<E>(property, values);
        }
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getByValueIn_NoPermCheck(final String property, final List<?> values) throws Hd3dException
    {
        final GetCollectionByValueInCmd<E> cmd = new GetCollectionByValueInCmd<E>(property, values);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getByValues(final ResourceContext<?, ?> context, final String[] properties, final Object[] values,
            final String operator) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValues_NoPermCheck(context, properties, values, operator));
    }

    public List<E> getByValues(final String[] properties, final Object[] values, final String operator)
            throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValues_NoPermCheck(properties, values, operator));
    }

    public List<E> getByValues_NoPermCheck(final ResourceContext<?, ?> context, final String[] properties,
            final Object[] values, final String operator) throws Hd3dException
    {
        final GetCollectionByValuesEqCmd<E> cmd;
        if (context != null)
        {
            cmd = new GetCollectionByValuesEqCmd<E>(context, properties, values, operator);
        }
        else
        {
            cmd = new GetCollectionByValuesEqCmd<E>(properties, values, operator);
        }
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getByValues_NoPermCheck(final String[] properties, final Object[] values, final String operator)
            throws Hd3dException
    {
        final GetCollectionByValuesEqCmd<E> cmd = new GetCollectionByValuesEqCmd<E>(properties, values, operator);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getByValues_NoPermCheck(final String[] properties, final Object[] values, final String operator,
            LockMode lockMode) throws Hd3dException
    {
        final GetCollectionByValuesEqCmd<E> cmd = new GetCollectionByValuesEqCmd<E>(properties, values, operator);
        cmd.setLockMode(lockMode);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getByValues(final ResourceContext<?, ?> context, final String[] properties, final Object[] values,
            final String operator, LockMode lockMode) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValues_NoPermCheck(context, properties, values, operator, lockMode));
    }

    public List<E> getByValues(final String[] properties, final Object[] values, final String operator,
            LockMode lockMode) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValues_NoPermCheck(properties, values, operator, lockMode));
    }

    public List<E> getByValues_NoPermCheck(final ResourceContext<?, ?> context, final String[] properties,
            final Object[] values, final String operator, LockMode lockMode) throws Hd3dException
    {
        final GetCollectionByValuesEqCmd<E> cmd;
        if (context != null)
        {
            cmd = new GetCollectionByValuesEqCmd<E>(context, properties, values, operator);
        }
        else
        {
            cmd = new GetCollectionByValuesEqCmd<E>(properties, values, operator);
        }
        cmd.setLockMode(lockMode);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    //
    public List<E> getByValuesIn(final ResourceContext<?, ?> context, final String[] properties, final Object[] values,
            final String operator) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValuesIn_NoPermCheck(context, properties, values, operator));
    }

    public List<E> getByValuesIn(final String[] properties, final Object[] values, final String operator)
            throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValuesIn_NoPermCheck(properties, values, operator));
    }

    public List<E> getByValuesIn_NoPermCheck(final ResourceContext<?, ?> context, final String[] properties,
            final Object[] values, final String operator) throws Hd3dException
    {
        final GetCollectionByValuesInCmd<E> cmd;
        if (context != null)
        {
            cmd = new GetCollectionByValuesInCmd<E>(context, properties, values, operator);
        }
        else
        {
            cmd = new GetCollectionByValuesInCmd<E>(properties, values, operator);
        }
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getByValuesIn_NoPermCheck(final String[] properties, final Object[] values, final String operator)
            throws Hd3dException
    {
        final GetCollectionByValuesInCmd<E> cmd = new GetCollectionByValuesInCmd<E>(properties, values, operator);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getByValuesIn_NoPermCheck(final String[] properties, final Object[] values, final String operator,
            LockMode lockMode) throws Hd3dException
    {
        final GetCollectionByValuesInCmd<E> cmd = new GetCollectionByValuesInCmd<E>(properties, values, operator);
        cmd.setLockMode(lockMode);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public List<E> getByValuesIn(final ResourceContext<?, ?> context, final String[] properties, final Object[] values,
            final String operator, LockMode lockMode) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValuesIn_NoPermCheck(context, properties, values, operator, lockMode));
    }

    public List<E> getByValuesIn(final String[] properties, final Object[] values, final String operator,
            LockMode lockMode) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByValuesIn_NoPermCheck(properties, values, operator, lockMode));
    }

    public List<E> getByValuesIn_NoPermCheck(final ResourceContext<?, ?> context, final String[] properties,
            final Object[] values, final String operator, LockMode lockMode) throws Hd3dException
    {
        final GetCollectionByValuesInCmd<E> cmd;
        if (context != null)
        {
            cmd = new GetCollectionByValuesInCmd<E>(context, properties, values, operator);
        }
        else
        {
            cmd = new GetCollectionByValuesInCmd<E>(properties, values, operator);
        }
        cmd.setLockMode(lockMode);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    //

    public List<E> getByExample(final ResourceContext<?, E> context, final Object value) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getByExample_NoPermCheck(context, value));
    }

    public List<E> getByExample_NoPermCheck(final ResourceContext<?, E> context, final Object value)
            throws Hd3dException
    {
        final GetCollectionByExampleCmd<E> cmd = new GetCollectionByExampleCmd<E>(context, value);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    /**
     * loads the object with the given id
     */
    public E getById_NoPermCheck(final ResourceContext<?, ?> context, final Long id) throws Hd3dException
    {
        return getById(context, id, null);
    }

    public E getById_NoPermCheck(final ResourceContext<?, ?> context, final Long id, LockMode lockMode)
            throws Hd3dException
    {
        E ret = null;

        if (BaseH.isValidId(id))
        {
            final GetObject<E> cmd = new GetObject<E>(context, id);
            if (lockMode != null)
            {
                cmd.setLockMode(lockMode);
            }
            cmd.getContext().setPersistor(this);
            cmd.getContext().setPersistedClass(getPersistedClass());
            cmd.setPermissionOn(false);
            HibernateUtil.safeQuery(cmd);
            ret = cmd.result;
        }

        return ret;
    }

    public E getById_NoPermCheck(final Long id) throws Hd3dException
    {
        return getById(null, id);
    }

    public E getById_NoPermCheck(final Long id, LockMode lockMode) throws Hd3dException
    {
        return getById(null, id, lockMode);
    }

    public E getById(final ResourceContext<?, ?> context, final Long id) throws Hd3dException
    {
        return getById(context, id, null);
    }

    public E getById(final ResourceContext<?, ?> context, final Long id, LockMode lockMode) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getById_NoPermCheck(context, id, lockMode));
    }

    public E getById(final Long id) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getById_NoPermCheck(null, id));
    }

    public E getById(final Long id, LockMode lockMode) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getById_NoPermCheck(null, id, lockMode));
    }

    /**
     * loads the object with the given id
     */
    public E loadById(final ResourceContext<?, ?> context, Long id) throws Hd3dException
    {
        E ret = null;

        if (BaseH.isValidId(id))
        {
            final LoadObject<E> cmd = new LoadObject<E>(context, id);
            cmd.getContext().setPersistor(this);
            cmd.getContext().setPersistedClass(getPersistedClass());
            HibernateUtil.safeQuery(cmd);
            ret = cmd.result;
        }

        return ret;
    }

    public E loadById(Long id) throws Hd3dException
    {
        E ret = null;

        if (BaseH.isValidId(id))
        {
            final LoadObject<E> cmd = new LoadObject<E>(id);
            cmd.getContext().setPersistor(this);
            cmd.getContext().setPersistedClass(getPersistedClass());
            HibernateUtil.safeQuery(cmd);
            ret = cmd.result;
        }

        return ret;
    }

    /**
     * returns the object with property=value
     */
    public E getObjectByValue(final ResourceContext<?, ?> context, String property, Object value) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getObjectByValue_NoPermcheck(context, property, value));
    }

    public E getObjectByValue_NoPermcheck(final ResourceContext<?, ?> context, String property, Object value)
            throws Hd3dException
    {
        final GetObjectByValueCmd<E> cmd = new GetObjectByValueCmd<E>(context, property, value);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public E getObjectByValue(String property, Object value) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getObjectByValue_NoPermCheck(property, value));
    }

    public E getObjectByValue_NoPermCheck(String property, Object value) throws Hd3dException
    {
        final GetObjectByValueCmd<E> cmd = new GetObjectByValueCmd<E>(property, value);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public E getObjectByValue(final ResourceContext<?, ?> context, String property, Object value, LockMode lockMode)
            throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getObjectByValue_NoPermCheck(context, property, value, lockMode));
    }

    public E getObjectByValue_NoPermCheck(final ResourceContext<?, ?> context, String property, Object value,
            LockMode lockMode) throws Hd3dException
    {
        final GetObjectByValueCmd<E> cmd = new GetObjectByValueCmd<E>(context, property, value);
        cmd.setLockMode(lockMode);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    public E getObjectByValue(String property, Object value, LockMode lockMode) throws Hd3dException
    {
        return ObjectProviderCustoms.filter(getObjectByValue_NoPermCheck(property, value, lockMode));
    }

    public E getObjectByValue_NoPermCheck(String property, Object value, LockMode lockMode) throws Hd3dException
    {
        final GetObjectByValueCmd<E> cmd = new GetObjectByValueCmd<E>(property, value);
        cmd.setLockMode(lockMode);
        cmd.getContext().setPersistor(this);
        cmd.setPermissionOn(false);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    /**
     * returns the persisted class
     */
    public Class<F> getPersistedClass()
    {
        return persistedClass;
    }

    /**
     * deletes the current instance with the passed object
     */
    public void delete(final E object) throws Hd3dException
    {
        if (object != null)
        {
            HibernateUtil.safeTransaction(new IHibernateTransaction() {

                public void execute(final org.hibernate.Session session) throws Hd3dException
                {
                    session.delete(object);
                    // session.flush();
                }
            });
        }
    }

    /**
     * saves the current instance with the passed object
     */
    public void persist(final E object) throws Hd3dException
    {
        if (object != null)
        {
            if (!object.enabled())
            {
                throw new Hd3dException(MSG_0);
            }

            HibernateUtil.safeTransaction(new IHibernateTransaction() {

                public void execute(final org.hibernate.Session session) throws Hd3dException
                {
                    session.save(object);
                    // session.flush();
                }
            });
        }
    }

    public void persistCollection(final Iterable<E> objects) throws Hd3dException
    {
        CollectUtils.removeNull((Collection<?>) objects);

        if (CollectUtils.isNotEmpty(objects))
        {
            HibernateUtil.safeTransaction(new IHibernateTransaction() {

                public void execute(final org.hibernate.Session session) throws Hd3dException
                {
                    int i = 0;
                    for (E obj : objects)
                    {
                        if (!obj.enabled())
                        {
                            throw new Hd3dException(MSG_0);
                        }

                        i++;

                        session.save(obj);
                        if (i % HibernateUtil.JDBC_BATCHSIZE == 0)
                        {
                            session.flush();
                        }
                    }
                }
            });
        }
    }

    public static void persistRawCollection(final Iterable<?> objects) throws Hd3dException
    {
        CollectUtils.removeNull((Collection<?>) objects);

        if (CollectUtils.isEmpty(objects))
            return;

        HibernateUtil.safeTransaction(new IHibernateTransaction() {

            public void execute(org.hibernate.Session session) throws Hd3dException
            {
                int i = 0;
                for (Object obj : objects)
                {
                    i++;

                    session.save(obj);
                    if (i % HibernateUtil.JDBC_BATCHSIZE == 0)
                    {
                        session.flush();
                    }
                }
            }
        });
    }

    public static void deleteRawCollection(final Iterable<?> objects) throws Hd3dException
    {
        CollectUtils.removeNull((Collection<?>) objects);

        if (CollectUtils.isEmpty(objects))
            return;

        HibernateUtil.safeTransaction(new IHibernateTransaction() {

            public void execute(org.hibernate.Session session) throws Hd3dException
            {
                int i = 0;
                for (Object obj : objects)
                {
                    i++;

                    session.delete(obj);
                    if (i % HibernateUtil.JDBC_BATCHSIZE == 0)
                    {
                        session.flush();
                    }
                }
            }
        });
    }

    /**
     * updates the current instance with the passed object
     */
    // public void update(final E object) throws Hd3dException
    // {
    // if (object != null)
    // {
    // if (!object.enabled())
    // {
    // throw new Hd3dException(MSG_0);
    // }
    //
    // HibernateUtil.safeTransaction(new IHibernateTransaction() {
    //
    // public void execute(org.hibernate.Session session) throws Hd3dException
    // {
    // session.update(object);
    // session.flush();
    // }
    // });
    // }
    // }
    public void disable(final E object) throws Hd3dException
    {
        if (object != null && object.enabled())
        {
            HibernateUtil.safeTransaction(new IHibernateTransaction() {

                public void execute(org.hibernate.Session session) throws Hd3dException
                {
                    object.disable();
                    session.merge(object);
                }
            });
        }
    }

    public void disableCollection(final Iterable<E> objects) throws Hd3dException
    {
        CollectUtils.removeNull((Collection<?>) objects);

        if (CollectUtils.isEmpty(objects))
            return;

        HibernateUtil.safeTransaction(new IHibernateTransaction() {

            public void execute(org.hibernate.Session session) throws Hd3dException
            {
                int i = 0;
                for (E obj : objects)
                {
                    if (!obj.enabled())
                        continue;

                    i++;

                    obj.disable();
                    session.merge(obj);
                    if (i % HibernateUtil.JDBC_BATCHSIZE == 0)
                    {
                        session.flush();
                    }
                }
            }
        });
    }

    /**
     * merges the current instance with the passed object
     */
    public void merge(final E object) throws Hd3dException
    {
        if (object != null)
        {
            if (!object.enabled())
            {
                throw new Hd3dException(MSG_0);
            }
            HibernateUtil.safeTransaction(new IHibernateTransaction() {

                public void execute(org.hibernate.Session session) throws Hd3dException
                {
                    session.merge(object);
                }
            });
        }
    }

    public void mergeCollection(final Iterable<E> objects) throws Hd3dException
    {
        CollectUtils.removeNull((Collection<?>) objects);

        if (CollectUtils.isEmpty(objects))
            return;

        HibernateUtil.safeTransaction(new IHibernateTransaction() {

            public void execute(org.hibernate.Session session) throws Hd3dException
            {
                int i = 0;
                for (E obj : objects)
                {
                    if (!obj.enabled())
                        throw new Hd3dException(MSG_0);

                    i++;

                    session.merge(obj);
                    if (i % HibernateUtil.JDBC_BATCHSIZE == 0)
                    {
                        session.flush();
                    }
                }
            }
        });
    }

    /**
     * returns a new instance of the bound persisted class
     */
    public E newInstance()
    {
        try
        {
            return persistedClass.newInstance();
        }
        catch (Exception e)
        {
            return null;
        }
    }

}
