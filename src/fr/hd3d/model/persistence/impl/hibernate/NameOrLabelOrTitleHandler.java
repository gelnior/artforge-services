package fr.hd3d.model.persistence.impl.hibernate;

import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IEvent;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ISimple;
import fr.hd3d.model.persistence.Name;
import fr.hd3d.model.persistence.Persistent;


/**
 * Return the name or label or title or the entity. Note: this is a workaround due to the unconsistency of the methods.
 * 
 * @author try.lam
 * 
 */
public final class NameOrLabelOrTitleHandler
{
    private NameOrLabelOrTitleHandler()
    {}

    public static boolean hasNameOrLabelOrTitle(Persistent object)
    {
        return (Name.class.isInstance(object) || IScript.class.isInstance(object)
                || IConstituent.class.isInstance(object) || IShot.class.isInstance(object)
                || IEvent.class.isInstance(object) || IMileStone.class.isInstance(object) || ISimple.class
                .isInstance(object));
    }

    public static String getNameOrLabelOrTitle(Persistent object)
    {
        String ret = null;

        if (object != null)
        {
            if (Name.class.isInstance(object))
            {
                ret = ((Name) object).getName();
            }
            else if (ISimple.class.isInstance(object))
            {
                ret = ((ISimple) object).getName();
            }
            else if (IScript.class.isInstance(object))
            {
                ret = ((IScript) object).getName();
            }
            else if (IConstituent.class.isInstance(object))
            {
                ret = ((IConstituent) object).getLabel();
            }
            else if (IShot.class.isInstance(object))
            {
                ret = ((IShot) object).getLabel();
            }
            else if (IEvent.class.isInstance(object))
            {
                ret = ((IEvent) object).getTitle();
            }
            else if (IMileStone.class.isInstance(object))
            {
                ret = ((IMileStone) object).getTitle();
            }
        }

        return ret;
    }
}
