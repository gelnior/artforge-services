package fr.hd3d.model.persistence.impl.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlayListEdit;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * 
 * @author try LAM
 * 
 */
@Entity
@Table(name = "hd3d_playlistedit")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class PlayListEditH extends BaseH implements IPlayListEdit
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Integer srcIn;
    private Integer srcOut;
    private Integer recIn;
    private Integer recOut;
    // private IFileRevision fileRevision;
    private IProxy proxy;
    private IPlayListRevision playListRevision;
    private String name;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_playlistedit"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public PlayListEditH()
    {}

    public PlayListEditH(Long id, java.sql.Timestamp version, Integer srcIn, Integer srcOut, Integer recIn,
            Integer recOut, IProxy proxy, IPlayListRevision playListRevision)
    {
        super(id, version);
        this.srcIn = srcIn;
        this.srcOut = srcOut;
        this.recIn = recIn;
        this.recOut = recOut;
        this.proxy = proxy;
        this.playListRevision = playListRevision;
    }

    public PlayListEditH(Integer srcIn, Integer srcOut, Integer recIn, Integer recOut, IProxy proxy,
            IPlayListRevision playListRevision)
    {
        this(null, null, srcIn, srcOut, recIn, recOut, proxy, playListRevision);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "playlistedit_srcin")
    public Integer getSrcIn()
    {
        return srcIn;
    }

    @Column(name = "playlistedit_srcout")
    public Integer getSrcOut()
    {
        return srcOut;
    }

    @Column(name = "playlistedit_recin")
    public Integer getRecIn()
    {
        return recIn;
    }

    @Column(name = "playlistedit_recout")
    public Integer getRecOut()
    {
        return recOut;
    }

    @ManyToOne(targetEntity = ProxyH.class)
    @JoinColumn(name = "playlistedit_proxy")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProxy getProxy()
    {
        return proxy;
    }

    @ManyToOne(targetEntity = PlayListRevisionH.class)
    @JoinColumn(name = "playlistedit_playlistrevision")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IPlayListRevision getPlayListRevision()
    {
        return playListRevision;
    }

    public String getName()
    {
        return name;
    }

    public void setSrcIn(Integer srcIn)
    {
        this.srcIn = srcIn;
    }

    public void setSrcOut(Integer srcOut)
    {
        this.srcOut = srcOut;
    }

    public void setRecIn(Integer recIn)
    {
        this.recIn = recIn;
    }

    public void setRecOut(Integer recOut)
    {
        this.recOut = recOut;
    }

    public void setProxy(IProxy proxy)
    {
        this.proxy = proxy;
    }

    public void setPlayListRevision(IPlayListRevision playListRevision)
    {
        this.playListRevision = playListRevision;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((playListRevision == null) ? 0 : playListRevision.hashCode());
        result = prime * result + ((recIn == null) ? 0 : recIn.hashCode());
        result = prime * result + ((recOut == null) ? 0 : recOut.hashCode());
        result = prime * result + ((srcIn == null) ? 0 : srcIn.hashCode());
        result = prime * result + ((srcOut == null) ? 0 : srcOut.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PlayListEditH other = (PlayListEditH) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (playListRevision == null)
        {
            if (other.playListRevision != null)
                return false;
        }
        else if (!playListRevision.equals(other.playListRevision))
            return false;
        if (recIn == null)
        {
            if (other.recIn != null)
                return false;
        }
        else if (!recIn.equals(other.recIn))
            return false;
        if (recOut == null)
        {
            if (other.recOut != null)
                return false;
        }
        else if (!recOut.equals(other.recOut))
            return false;
        if (srcIn == null)
        {
            if (other.srcIn != null)
                return false;
        }
        else if (!srcIn.equals(other.srcIn))
            return false;
        if (srcOut == null)
        {
            if (other.srcOut != null)
                return false;
        }
        else if (!srcOut.equals(other.srcOut))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    @Override
    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public IBaseTranslator<ILPlayListEdit, IPlayListEdit> defaultTranslator()
    {
        return Translators.playlistedit;
    }

    public String toString()
    {
        return name;
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    // No ComplexTypeProvider
}
