package fr.hd3d.model.persistence.impl.hibernate.search;

import java.util.List;
import java.util.Map;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;


public interface ILuceneFilter
{
    public Map<String, String> getQueryMap();

    public void setQueryMap(Map<String, String> map);

    public ResultCollection filter(Class<?> persisted, List values, boolean onIdsOnly) throws Hd3dException;
}
