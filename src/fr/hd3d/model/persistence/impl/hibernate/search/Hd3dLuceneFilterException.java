package fr.hd3d.model.persistence.impl.hibernate.search;

import fr.hd3d.exception.Hd3dPersistenceException;


/**
 * Exception thrown when a web service filter parsing failed or when the modifier can't apply.
 * 
 * @author Try LAM
 */
public class Hd3dLuceneFilterException extends Hd3dPersistenceException
{
    private static final long serialVersionUID = 8121337755687521327L;

    /**
     * Constructor, add the message : <br />
     * "Invalid query filter : <i>query</i>".
     * 
     * @param query
     *            The query which failed.
     */
    public Hd3dLuceneFilterException(String query)
    {

        super("Invalid item filter : " + query);
    }
}
