package fr.hd3d.model.persistence.impl.hibernate.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import fr.hd3d.common.client.LuceneConstraint;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.utils.CollectUtils;


public final class LuceneFilterFactory
{
    private LuceneFilterFactory()
    {}

    public static class LuceneFilters
    {
        private final List<ILuceneFilter> filters = new ArrayList<ILuceneFilter>();

        public List<ILuceneFilter> getFilters()
        {
            return filters;
        }

        public void addFilter(final ILuceneFilter filter)
        {
            getFilters().add(filter);
        }

        public void removeFilter(final ILuceneFilter filter)
        {
            getFilters().remove(filter);
        }

        @SuppressWarnings("unchecked")
        public ResultCollection filter(final Class persisted, final ResultCollection values, boolean onIdsOnly)
                throws Hd3dException
        {
            ResultCollection ret = null;

            if (CollectUtils.isEmpty(getFilters()))
            {
                ret = values;
            }
            else
            {
                for (ILuceneFilter filter : getFilters())
                {
                    if (onIdsOnly)
                    {
                        ret = filter.filter(persisted, values.getAllIds(), onIdsOnly);
                    }
                    else
                    {
                        ret = filter.filter(persisted, values.getResult(), onIdsOnly);
                    }
                }
            }

            return ret;
        }
    }

    @SuppressWarnings("serial")
    private static final Map<String, Class<? extends ILuceneFilter>> QUERY_KEYWORDS = Collections
            .unmodifiableMap(new HashMap<String, Class<? extends ILuceneFilter>>() {
                {
                    put(LuceneConstraint.LUCENECONSTRAINT, LuceneFilterHandler.class);
                }
            });

    public static LuceneFilters getFilter(final Map<String, String> map) throws Hd3dException
    {
        final LuceneFilters filters = new LuceneFilters();

        if (map != null)
        {
            final Set<Entry<String, String>> entries = map.entrySet();
            CollectUtils.removeNull(entries);

            Class<? extends ILuceneFilter> procClass;
            for (Map.Entry<String, String> entry : entries)
            {
                procClass = QUERY_KEYWORDS.get(entry.getKey());

                if (procClass == null)
                    continue;

                try
                {
                    ILuceneFilter filter = procClass.newInstance();
                    filter.setQueryMap(map);
                    filters.addFilter(filter);
                }
                catch (InstantiationException e)
                {
                    throw new Hd3dException("Command not implemented: " + entry.getKey());
                }
                catch (IllegalAccessException e)
                {
                    throw new Hd3dException("Command not implemented: " + entry.getKey());
                }
            }
        }

        return filters;
    }
}
