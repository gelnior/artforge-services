package fr.hd3d.model.persistence.impl.hibernate.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import net.sf.sojo.interchange.json.JsonParser;
import net.sf.sojo.interchange.json.JsonParserException;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.hibernate.HibernateException;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;

import fr.hd3d.common.client.Constraint;
import fr.hd3d.common.client.LogicConstraint;
import fr.hd3d.common.client.LuceneConstraint;
import fr.hd3d.common.client.enums.ELuceneFilterLogicalOperator;
import fr.hd3d.common.client.enums.ELuceneFilterOperator;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


public class LuceneFilterHandler implements ILuceneFilter
{
    protected Map<String, String> queryMap;

    public Map<String, String> getQueryMap()
    {
        return this.queryMap;
    }

    public void setQueryMap(final Map<String, String> map)
    {
        this.queryMap = map;
    }

    public LuceneFilterHandler()
    {}

    @SuppressWarnings("unchecked")
    public ResultCollection filter(final Class<?> persisted, final List previousResult, boolean onIdsOnly)
            throws Hd3dException
    {
        ResultCollection ret = null;

        try
        {
            /* Parse query, get the expression type. */
            final List<Map<String, ?>> filterList = getLuceneFilters();

            /* prepare lucene query */
            final BooleanQuery bQuery = new BooleanQuery();
            boolean emptyQuery = buildBooleanQuery(bQuery, filterList, persisted);

            /* search */
            final List<Long> luceneResult = luceneSearch(persisted, bQuery, emptyQuery);

            List intersection;
            if (onIdsOnly)
            {
                intersection = intersection(previousResult, emptyQuery, luceneResult);
                ret = new ResultCollection(intersection, intersection, intersection.size());
            }
            else
            {
                intersection = intersection(CollectUtils.getIds(previousResult), emptyQuery, luceneResult);
                if (CollectUtils.isNotEmpty(intersection))
                {
                    List<Persistent> result = new ArrayList<Persistent>();
                    for (Object object : previousResult)
                    {
                        if (intersection.contains(((Persistent) object).getId()))
                        {
                            result.add((Persistent) object);
                        }
                    }
                    ret = new ResultCollection(result, CollectUtils.getIds(result), intersection.size());
                }
                else
                {
                    ret = new ResultCollection(Collections.emptyList(), Collections.emptyList(), 0);
                }
            }
        }
        catch (JsonParserException e)
        {
            Log.LOGGER.error("a List<Map<String, Object>> is expected in JSON");
            Log.LOGGER.error(ExceptUtils.format(e));
        }
        catch (Exception e)
        {
            throw new Hd3dLuceneFilterException(e.getMessage());
        }
        return ret;
    }

    private List<Long> getLuceneResultIds(List<Object[]> luceneResult)
    {
        List<Long> luceneResultIds = new ArrayList<Long>();
        for (Object[] id : luceneResult)
        {
            luceneResultIds.add((Long) id[0]);
        }
        return luceneResultIds;
    }

    @SuppressWarnings("unchecked")
    private List<Map<String, ?>> getLuceneFilters()
    {
        final List<Map<String, ?>> filterList = (List<Map<String, ?>>) new JsonParser().parse((String) getQueryMap()
                .get(LuceneConstraint.LUCENECONSTRAINT));

        CollectUtils.removeNull(filterList);
        return filterList;
    }

    /**
     * returns the ids of the intersection
     */
    private List<Long> luceneSearch(final Class<?> persisted, final BooleanQuery bQuery, boolean emptyQuery)
            throws HibernateException
    {
        List<Object[]> result;
        if (emptyQuery)
        {
            result = Collections.emptyList();
        }
        else
        {
            final FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
            final FullTextQuery hibQuery = fullTextSession.createFullTextQuery(bQuery, persisted);
            /* only retrieve id */
            hibQuery.setProjection(Const.ID);
            result = hibQuery.list();
        }
        return getLuceneResultIds(result);
    }

    @SuppressWarnings("unchecked")
    private <P> ResultCollection<P> handlePagination(final List<P> intersection) throws JsonParserException,
            Hd3dQueryFilterException
    {
        ResultCollection ret;

        /* Parse query, get the attributes FIRST and QUANTITY. */
        final String query = (String) getQueryMap().get(Constraint.PAGINATION);
        final Map<String, Long> map = query == null ? null : (Map<String, Long>) new JsonParser().parse(query);

        if (query == null || map == null)
        {
            ret = new ResultCollection(intersection, intersection.size());
        }
        else
        {
            final Set<Entry<String, Long>> entrySet = map.entrySet();
            CollectUtils.removeNull(entrySet);

            int first = 0;
            int qty = 50;
            String key;
            Long value;
            for (Entry<String, Long> entry : entrySet)
            {
                key = entry.getKey();
                value = entry.getValue();
                if (Constraint.FIRST_MAP_FIELD.equals(key))
                {
                    first = value.intValue();
                }
                else if (Constraint.QUANTITY_MAP_FIELD.equals(key))
                {
                    qty = value.intValue();
                }
                else
                {
                    throw new Hd3dQueryFilterException(query);
                }
            }

            final int startIndex = Math.min(first, intersection.size());
            ret = new ResultCollection(intersection
                    .subList(startIndex, Math.min(startIndex + qty, intersection.size())), intersection.size());
        }

        return ret;
    }

    private boolean buildBooleanQuery(final BooleanQuery query, final List<Map<String, ?>> filters,
            final Class<?> persisted) throws Hd3dException
    {
        query.add(getTermQuery(StringUtils.lowerCase(Const.INTERNAL_STATUS),
                fr.hd3d.common.client.Const.INTERNALSTATUS_ENABLED.toString()), BooleanClause.Occur.MUST);

        BooleanQuery aBQuery;
        boolean emptyQuery = true;
        for (Map<String, ?> map : filters)
        {
            if (isLogicalMap(map))
            {
                aBQuery = filterLogicalConstraint(persisted, map);
                if (aBQuery != null)
                {
                    query.add(aBQuery, BooleanClause.Occur.MUST);
                    emptyQuery = false;
                }
            }
            else if (isSimpleConstraint(map))
            {
                aBQuery = filterSimpleConstraint(persisted, map);
                if (aBQuery != null)
                {
                    query.add(aBQuery, BooleanClause.Occur.MUST);
                    emptyQuery = false;
                }
            }
        }

        return emptyQuery;
    }

    private List intersection(final List values, boolean emptyQuery, List<Long> luceneResultIds)
    {
        if (CollectUtils.isEmpty(values))
            return Collections.emptyList();

        final List intersection = new ArrayList();

        if (emptyQuery)
        {
            intersection.addAll(values);
        }
        else
        {
            intersection.addAll(getIntersectionIds(values, luceneResultIds));
            CollectUtils.removeNull(intersection);
        }

        return intersection;
    }

    @SuppressWarnings("unchecked")
    private Collection<Long> getIntersectionIds(final List values, List<Long> luceneResultIds)
    {
        final Collection<Long> intersectionIds;

        intersectionIds = org.apache.commons.collections15.CollectionUtils.intersection(values, luceneResultIds);

        return intersectionIds;
    }

    public BooleanQuery filter(final Class<?> persisted, final Map<String, ?> map) throws Hd3dException
    {
        BooleanQuery aBQuery = null;
        try
        {
            if (isLogicalMap(map))
            {
                aBQuery = filterLogicalConstraint(persisted, map);
            }
            else if (isSimpleConstraint(map))
            {
                aBQuery = filterSimpleConstraint(persisted, map);
            }
        }
        catch (JsonParserException e)
        {
            Log.LOGGER.error("a List<Map<String, Object>> is expected in JSON");
            Log.LOGGER.error(ExceptUtils.format(e));
        }
        catch (Exception e)
        {
            throw new Hd3dLuceneFilterException(e.getMessage());
        }

        return aBQuery;
    }

    protected boolean isLogicalMap(final Map<String, ?> map)
    {
        return map != null && map.get(LogicConstraint.LOGIC) != null;
    }

    protected boolean isSimpleConstraint(final Map<String, ?> map)
    {
        return map != null && map.get(LogicConstraint.LOGIC) == null;
    }

    @SuppressWarnings("unchecked")
    private BooleanQuery filterSimpleConstraint(final Class<?> persisted, final Map<String, ?> map)
            throws Hd3dException
    {
        BooleanQuery ret = null;

        if (persisted != null)
        {
            /*----------------------------
             * Collect parameters
            -----------------------------*/
            final String column = (String) map.get(LuceneConstraint.COLUMN);
            if (column != null)
            {
                /* type (=operator) */
                final String type = (String) map.get(LuceneConstraint.TYPE);

                /* check mandatory fields */
                if (type == null)
                {
                    throw new Hd3dLuceneFilterException("type cannot be null ");
                }

                /* value */
                final Object value = map.get(LuceneConstraint.VALUE);

                /*-----------------------------
                 * Filter
                ------------------------------*/
                final ELuceneFilterOperator filterType = ELuceneFilterOperator.valueOf(type);
                switch (filterType)
                {
                    case eq:
                    {
                        ret = termBooleanClause(column, (String) value, BooleanClause.Occur.MUST);
                        break;
                    }
                    case neq:
                    {
                        ret = termBooleanClause(column, (String) value, BooleanClause.Occur.MUST_NOT);
                        break;
                    }
                    case in:
                    {
                        ret = termBooleanClause(column, (Collection<String>) value, BooleanClause.Occur.SHOULD);
                        break;
                    }
                    case notin:
                    {
                        ret = termBooleanClause(column, (Collection<String>) value, BooleanClause.Occur.MUST_NOT);
                        break;
                    }
                    case prefix:
                    {
                        ret = prefixBooleanClause(column, (String) value, BooleanClause.Occur.MUST);
                        break;
                    }
                    default:
                    {
                        throw new Hd3dLuceneFilterException("Command not implemented " + filterType);
                    }
                }
            }
        }
        return ret;
    }

    private BooleanQuery termBooleanClause(final String column, final String value, final BooleanClause.Occur occur)
    {
        BooleanQuery ret = null;

        Query query = getTermQuery(column, value);
        if (query != null)
        {
            ret = new BooleanQuery();
            ret.add(new BooleanClause(query, occur));
        }

        return ret;
    }

    private BooleanQuery termBooleanClause(final String column, final Collection<String> value,
            final BooleanClause.Occur occur)
    {
        if (CollectUtils.isEmpty(value))
            return null;

        BooleanQuery ret = new BooleanQuery();
        for (String aValue : value)
        {
            Query query = getTermQuery(column, aValue);

            if (query != null)
            {
                ret.add(new BooleanClause(query, occur));
            }
        }

        return ret;
    }

    private BooleanQuery prefixBooleanClause(final String column, final String value, final BooleanClause.Occur occur)
    {
        BooleanQuery ret = null;
        Query query = getPrefixQuery(column, value);
        if (query != null)
        {
            ret = new BooleanQuery();
            ret.add(new BooleanClause(query, occur));
        }

        return ret;
    }

    private Query getTermQuery(final String fieldName, final String value)
    {
        Query ret = null;
        if (StringUtils.isNotEmpty(fieldName) && StringUtils.isNotEmpty(value))
        {
            ret = new TermQuery(new Term(fieldName, StringUtils.lowerCase(value)));
        }

        return ret;
    }

    private Query getPrefixQuery(final String fieldName, final String value)
    {
        Query ret = null;
        if (StringUtils.isNotEmpty(fieldName) && StringUtils.isNotEmpty(value))
        {
            ret = new PrefixQuery(new Term(fieldName, StringUtils.lowerCase(value)));
        }

        return ret;
    }

    private BooleanQuery filterLogicalConstraint(final Class<?> persisted, final Map<String, ?> map)
            throws Hd3dException
    {
        final BooleanQuery bQuery = new BooleanQuery();

        if (persisted != null)
        {
            /*----------------------------
             * Collect parameters
             -----------------------------*/
            /* itemId */
            final String logic = (String) map.get(LogicConstraint.LOGIC);
            if (logic == null)
            {
                throw new Hd3dLuceneFilterException("Not a logical expression");
            }

            final List<Map<String, ?>> maps = (List<Map<String, ?>>) map.get(LogicConstraint.FILTER);
            if (maps == null)
            {
                throw new Hd3dLuceneFilterException("No filter in logical expression");
            }

            switch (ELuceneFilterLogicalOperator.valueOf(logic))
            {
                case AND:
                {
                    BooleanQuery firstTermResult = filter(persisted, maps.get(0));
                    BooleanQuery secondTermResult = filter(persisted, maps.get(1));
                    bQuery.add(firstTermResult, BooleanClause.Occur.MUST);
                    bQuery.add(secondTermResult, BooleanClause.Occur.MUST);

                    break;
                }
                case OR:
                {
                    BooleanQuery firstTermResult = filter(persisted, maps.get(0));
                    BooleanQuery secondTermResult = filter(persisted, maps.get(1));
                    bQuery.add(firstTermResult, BooleanClause.Occur.SHOULD);
                    bQuery.add(secondTermResult, BooleanClause.Occur.SHOULD);

                    break;
                }
                case NOT:
                {
                    BooleanQuery firstTermResult = filter(persisted, maps.get(0));
                    bQuery.add(new MatchAllDocsQuery(), BooleanClause.Occur.SHOULD);
                    bQuery.add(firstTermResult, BooleanClause.Occur.MUST_NOT);

                    break;
                }
                    /* Filter type unrecognized, throws exception. */
                default:
                {
                    throw new Hd3dLuceneFilterException("Command not implemented " + logic);
                }
            }
        }

        return bQuery;
    }
}
