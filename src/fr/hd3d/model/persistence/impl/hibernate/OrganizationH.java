package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILOrganization;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.IOrganization;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


/**
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_organization")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class OrganizationH extends SimpleH implements IOrganization
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private IOrganization parent;
    private Collection<IOrganization> children;
    private Set<IResourceGroup> resourceGroups;
    private Set<IContract> contracts;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_organization"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public OrganizationH()
    {}

    public OrganizationH(Long id, Timestamp version, String name, IOrganization parent)
    {
        super(id, version, name);
        this.name = name;
        this.parent = parent;
    }

    public OrganizationH(String name, IOrganization parent)
    {
        this(null, null, name, parent);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToOne(targetEntity = OrganizationH.class, fetch = FetchType.LAZY)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IOrganization getParent()
    {
        return parent;
    }

    public void setParent(IOrganization parent)
    {
        if (parent != this)
            this.parent = parent;
    }

    @OneToMany(targetEntity = OrganizationH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parent")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Collection<IOrganization> getChildren()
    {
        if (children == null)
        {
            children = new ArrayList<IOrganization>();
        }
        return children;
    }

    @OneToMany(targetEntity = ContractH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "organization")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IContract> getContracts()
    {
        return contracts;
    }

    public void setContracts(Set<IContract> contracts)
    {
        this.contracts = contracts;
    }

    @ManyToMany(targetEntity = ResourceGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_organization__resource", joinColumns = @JoinColumn(name = "organization_id"), inverseJoinColumns = @JoinColumn(name = "resourcegroup_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IResourceGroup> getResourceGroups()
    {
        if (this.resourceGroups == null)
        {
            this.resourceGroups = new HashSet<IResourceGroup>();
        }
        return this.resourceGroups;
    }

    public void setResourceGroups(Set<IResourceGroup> resourceGroups)
    {
        this.resourceGroups = resourceGroups;
    }

    public void setChildren(Collection<IOrganization> children)
    {
        this.children = children;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((resourceGroups == null) ? 0 : resourceGroups.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        OrganizationH other = (OrganizationH) obj;
        if (resourceGroups == null)
        {
            if (other.resourceGroups != null)
                return false;
        }
        else if (!resourceGroups.equals(other.resourceGroups))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        for (IResourceGroup g : getResourceGroups())
        {
            g.getOrganizations().remove(this);
        }
    }

    @Transient
    public static boolean isRootOrganization(IOrganization group)
    {
        if (group == null)
        {
            return false;
        }
        return IOrganization.ROOT.equals(group.getName());
    }

    @Transient
    public boolean isRoot()
    {
        return isRootOrganization(this);
    }

    /**
     * return all the parents of this organization recursively
     * 
     * @return
     */
    public Set<IOrganization> parents()
    {
        Set<IOrganization> parents = new HashSet<IOrganization>();
        for (IOrganization group = getParent(); group != null; group = group.getParent())
        {
            parents.add(group);
        }
        return parents;
    }

    public Collection<IOrganization> allChildren(boolean includeSelf)
    {
        List<IOrganization> allChildren = new ArrayList<IOrganization>();
        if (includeSelf)
        {
            allChildren.add(this);
        }
        for (IOrganization group : getChildren())
        {
            allChildren.addAll(group.allChildren(includeSelf));
        }

        return allChildren;
    }

    @Transient
    public String getParentIds()
    {
        if (parents() == null)
        {
            return null;
        }

        return StringUtils.join(CollectUtils.getIds(parents()), ' ');
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILOrganization, IOrganization> defaultTranslator()
    {
        return Translators.organization;
    }

    @Override
    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("parentId", parent == null ? "null" : parent.getId());
    // b.append("childrenIds", StringUtils.join(CollectUtils.getIds(getChildren()), ','));
    // b.append("resourceGroupIds", StringUtils.join(CollectUtils.getIds(getResourceGroups()), ','));
    // b.append("contractIds", StringUtils.join(CollectUtils.getIds(getContracts()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
