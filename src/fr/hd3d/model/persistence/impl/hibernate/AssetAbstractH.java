package fr.hd3d.model.persistence.impl.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Transient;

import fr.hd3d.model.lightweight.ILAssetAbstract;
import fr.hd3d.model.persistence.IAssetAbstract;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


@Deprecated
public class AssetAbstractH extends BaseH implements IAssetAbstract
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String key;
    private String variation;
    private String name;
    private IProject project;
    private ITaskType taskType;
    private Date creationDate;
    private IPerson creator;
    private Set<IAssetRevision> revisions;

    /*-------------
     * id override
     -------------*/
    /* Note: AssetAbstractH is NOT persisted, it aggregates AssetRevisions */
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/

    /*------------------
     * Getters & Setters
     ------------------*/
    public Date getCreationDate()
    {
        return creationDate;
    }

    public IPerson getCreator()
    {
        return creator;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public void setVariation(String variation)
    {
        this.variation = variation;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setTaskType(ITaskType type)
    {
        this.taskType = type;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public void setCreator(IPerson creator)
    {
        this.creator = creator;
    }

    public String getKey()
    {
        return key;
    }

    public String getName()
    {
        return name;
    }

    public IProject getProject()
    {
        return this.project;
    }

    public void setProject(IProject project)
    {
        this.project = project;
    }

    public Set<IAssetRevision> getRevisions()
    {
        if (revisions == null)
            revisions = new HashSet<IAssetRevision>();
        return revisions;
    }

    public ITaskType getTaskType()
    {
        return taskType;
    }

    public String getVariation()
    {
        return variation;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 32;
        int result = super.hashCode();

        result = prime * result + (key == null ? 0 : key.hashCode());
        result = prime * result + (variation == null ? 0 : variation.hashCode());
        result = prime * result + (project == null ? 0 : project.hashCode());
        result = prime * result + (version.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == this)
            return true;

        if (this.getClass() != obj.getClass())
            return false;

        IAssetAbstract other = (IAssetAbstract) obj;

        if (!key.equals(other.getKey()))
            return false;

        if ((variation == null && other.getVariation() != null) || !other.getVariation().equals(variation))
            return false;

        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    public IBaseTranslator<ILAssetAbstract, IAssetAbstract> defaultTranslator()
    {
        return Translators.assetabstract;
    }

    public static List<IAssetAbstract> parseIAssets(List<IAssetRevision> list)
    {
        List<IAssetAbstract> result = new ArrayList<IAssetAbstract>();

        Long i = -1L;
        while (list.size() > 0)
        {
            IAssetRevision lastAsset = list.get(0);

            IAssetAbstract assetAbstract = new AssetAbstractH();
            assetAbstract.setProject(list.get(0).getProject());
            assetAbstract.setId(i);
            assetAbstract.setKey(list.get(0).getKey());
            assetAbstract.setVariation(list.get(0).getVariation());
            assetAbstract.setName(list.get(0).getName());
            assetAbstract.setTaskType(list.get(0).getTaskType());
            assetAbstract.setCreationDate(list.get(0).getCreationDate());
            assetAbstract.setCreator(list.get(0).getCreator());
            assetAbstract.setVersion(list.get(0).getVersion());
            result.add(assetAbstract);

            Set<IAssetRevision> rev = assetAbstract.getRevisions();
            // If there are elements left in list
            // AND we still on same project
            // AND key AND variation (null or not null)...
            while (list.size() > 0
                    && list.get(0).getProject() == assetAbstract.getProject()
                    && list.get(0).getKey().equals(assetAbstract.getKey())
                    && (list.get(0).getVariation() == assetAbstract.getVariation() || list.get(0).getVariation()
                            .equals(assetAbstract.getVariation())))
            {
                // Add the asset revision in the list
                rev.add(list.get(0));
                // remove the parsed asset revision
                lastAsset = list.remove(0);
            }
            if (rev.size() > 0 && lastAsset != null)
                assetAbstract.setVersion(lastAsset.getVersion());
            i--;
        }

        return result;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }
    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("key", key).append("variation", variation).append("name", name);
    // b.append("projectId", project == null ? "null" : project.getId());
    // b.append("taskTypeId", taskType == null ? "null" : taskType.getId());
    // b.append("creationDate", creationDate);
    // b.append("creatorId", creator == null ? "null" : creator.getId());
    // b.append("revisionIds", StringUtils.join(CollectUtils.getIds(getRevisions()), ','));
    //
    // return b.toString();
    // }
    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
