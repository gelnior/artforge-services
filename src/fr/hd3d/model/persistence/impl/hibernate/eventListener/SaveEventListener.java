package fr.hd3d.model.persistence.impl.hibernate.eventListener;

import java.io.Serializable;

import org.hibernate.event.EventSource;
import org.hibernate.event.def.DefaultSaveEventListener;
import org.hibernate.persister.entity.EntityPersister;


public class SaveEventListener extends DefaultSaveEventListener
{
    protected Serializable performSave(Object entity, Serializable id, EntityPersister persister,
            boolean useIdentityColumn, Object anything, EventSource source, boolean requiresImmediateIdAccess)
    {
        // if (entity instanceof IBase)
        // {
        // System.out.println(((IBase) entity).entityName() + " is being persisted");
        // }
        return super.performSave(entity, id, persister, useIdentityColumn, anything, source, requiresImmediateIdAccess);
    }
}
