package fr.hd3d.model.persistence.impl.hibernate.eventListener;

import org.hibernate.HibernateException;
import org.hibernate.event.PersistEvent;
import org.hibernate.event.def.DefaultPersistEventListener;


public class PersistEventListener extends DefaultPersistEventListener
{
    @Override
    public void onPersist(PersistEvent event) throws HibernateException
    {
        // Object o = event.getObject();
        // if (o instanceof IBase)
        // {
        // System.out.println(((IBase) o).entityName() + " is being persisted");
        // }

        super.onPersist(event);
    }
}
