package fr.hd3d.model.persistence.impl.hibernate.eventListener;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.event.SaveOrUpdateEvent;
import org.hibernate.event.def.DefaultSaveOrUpdateEventListener;
import org.hibernate.persister.entity.EntityPersister;


public class SaveOrUpdateEventListener extends DefaultSaveOrUpdateEventListener
{
    @Override
    public void onSaveOrUpdate(SaveOrUpdateEvent event) throws HibernateException
    {
        // Object o = event.getObject();
        // EntityEntry entry = event.getEntry();
        // if (o instanceof IBase)
        // {
        // IBase entity = (IBase) o;
        // if (entity.getId() == null)
        // {
        // System.out.println(entity.entityName() + "(id=" + entity.getId() + " is being persisted");
        // }
        //
        // }
        super.onSaveOrUpdate(event);
    }

    @Override
    protected Serializable performSaveOrUpdate(SaveOrUpdateEvent event)
    {
        // Object o = event.getObject();
        // if (o instanceof IBase)
        // {
        // IBase entity = (IBase) o;
        // if (entity.getId() == null)
        // {
        // System.out.println(entity.entityName() + "(id=" + entity.getId() + " is being persisted");
        // }
        //
        // }
        return super.performSaveOrUpdate(event);
    }

    @Override
    public void performUpdate(SaveOrUpdateEvent event, Object entity, EntityPersister persister)
            throws HibernateException
    {
        // Object o = event.getObject();
        // if (o instanceof IBase)
        // {
        // IBase en = (IBase) o;
        // System.out.println(en.entityName() + "(id=" + en.getId() + " is being updated");
        // }
        super.performUpdate(event, entity, persister);
    }
}
