package fr.hd3d.model.persistence.impl.hibernate.eventListener;

import org.hibernate.HibernateException;
import org.hibernate.event.FlushEntityEvent;
import org.hibernate.event.def.DefaultFlushEntityEventListener;


public class EntityCheckDirtyListener extends DefaultFlushEntityEventListener
{

    private static final long serialVersionUID = 1L;

    public void onFlushEntity(FlushEntityEvent event) throws HibernateException
    {
    // Call the super method to trigger the calculation of the dirty properties.
    // super.onFlushEntity(event);
    //
    // // Collection information from the Event
    // final Object entity = event.getEntity();
    // final Object[] values = event.getPropertyValues();
    // final EntityEntry entry = event.getEntityEntry();
    // int[] dirtyProperties = event.getDirtyProperties();
    //
    // // Collect data from the EntityEntry
    // final Serializable id = entry.getId();
    // final Object[] loadedState = entry.getLoadedState();
    // final EntityPersister persister = entry.getPersister();
    //
    // // Collect data from the EntityPersister
    // final String[] propertyNames = persister.getPropertyNames();
    //
    // if (entry != null)
    // {
    // if (!entry.isExistsInDatabase())
    // {
    // // System.out.println(entry.getEntityName() + " has been created with id=" + id);
    // }
    // else
    // {
    // // System.out.println(entry.getEntityName() + " has been loaded");
    // }
    // }
    //
    // // Logging of the Dirty Properties
    // if (dirtyProperties != null)
    // {
    // System.out.println(entry.getEntityName() + " has been updated");
    // for (int i = 0; i < dirtyProperties.length; i++)
    // {
    // int dirtyIndex = dirtyProperties[i];
    //
    // UpdatedObjectInfo objectInfo = new UpdatedObjectInfo(entity, id);
    // objectInfo.setPropertyName(propertyNames[dirtyIndex]);
    // System.out.println("propertyName=" + propertyNames[dirtyIndex]);
    // objectInfo.setPreviousValue(StringUtils.nullSafeString(loadedState[dirtyIndex]));
    // System.out.println("previousValue=" + StringUtils.nullSafeString(loadedState[dirtyIndex]));
    // objectInfo.setNewValue(StringUtils.nullSafeString(values[dirtyIndex]));
    // System.out.println("newValue=" + StringUtils.nullSafeString(values[dirtyIndex]));
    //
    // registerObjectInfo(event, objectInfo);
    // }
    // }
    }

    // private void registerObjectInfo(FlushEntityEvent event, UpdatedObjectInfo objectInfo)
    // {
    // Interceptor interceptor = event.getSession().getInterceptor();
    //
    // if (SessionStatisticsInterceptor.class.isAssignableFrom(interceptor.getClass()))
    // {
    // ((SessionStatisticsInterceptor) interceptor).addUpdatedObjectInfo(objectInfo);
    // }
    // }
}
