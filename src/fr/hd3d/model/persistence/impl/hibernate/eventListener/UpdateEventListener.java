package fr.hd3d.model.persistence.impl.hibernate.eventListener;

import org.hibernate.HibernateException;
import org.hibernate.event.SaveOrUpdateEvent;
import org.hibernate.event.def.DefaultUpdateEventListener;
import org.hibernate.persister.entity.EntityPersister;


public class UpdateEventListener extends DefaultUpdateEventListener
{
    @Override
    public void performUpdate(SaveOrUpdateEvent event, Object entity, EntityPersister persister)
            throws HibernateException
    {
        // Object o = event.getObject();
        // if (o instanceof IBase)
        // {
        // IBase en = (IBase) o;
        // System.out.println(en.entityName() + "(id=" + en.getId() + " is being persisted");
        // }
        super.onSaveOrUpdate(event);
    }
}
