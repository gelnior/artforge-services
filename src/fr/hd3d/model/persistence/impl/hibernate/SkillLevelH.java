package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.common.client.enums.ESkillLevel;
import fr.hd3d.common.client.enums.ESkillMotivation;
import fr.hd3d.model.lightweight.ILSkillLevel;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.ISkill;
import fr.hd3d.model.persistence.ISkillLevel;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.EnumUtils;


/**
 * Skill Level hibernate object needed for database persistence.
 * 
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_skill_level")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class SkillLevelH extends BaseH implements ISkillLevel
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private ESkillLevel level;
    private ESkillMotivation motivation;

    private IPerson person;
    private ISkill skill;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_skilllevel"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public SkillLevelH()
    {}

    public SkillLevelH(Long id, Timestamp version, ESkillLevel level, ESkillMotivation motivation, IPerson person,
            ISkill skill)
    {
        super(id, version);

        this.level = level;
        this.motivation = motivation;

        this.person = person;
        this.skill = skill;
    }

    public SkillLevelH(ESkillLevel level, ESkillMotivation motivation, IPerson person, ISkill skill)
    {
        this(null, null, level, motivation, person, skill);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Enumerated(EnumType.STRING)
    @Column(name = "level", nullable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    public ESkillLevel getLevel()
    {
        return level;
    }

    public void setLevel(ESkillLevel level)
    {
        this.level = level;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "motivation", nullable = false)
    public ESkillMotivation getMotivation()
    {
        return motivation;
    }

    public void setMotivation(ESkillMotivation motivation)
    {
        this.motivation = motivation;
    }

    @ManyToOne(targetEntity = PersonH.class)
    @JoinColumn(name = "person_id")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getPerson()
    {
        return this.person;
    }

    public void setPerson(IPerson person)
    {
        this.person = person;
    }

    @ManyToOne(targetEntity = SkillH.class)
    @JoinColumn(name = "skill_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public ISkill getSkill()
    {
        return this.skill;
    }

    public void setSkill(ISkill skill)
    {
        this.skill = skill;
    }

    /*------------------
     * equal & hashcode
     ------------------*/

    /*------------------
     * Transient methods
     ------------------*/
    public IBaseTranslator<ILSkillLevel, ISkillLevel> defaultTranslator()
    {
        return Translators.skillLevel;
    }

    @Override
    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("level", level).append("motivation", motivation);
        b.append("personId", person == null ? "null" : person.getId());
        b.append("skillId", skill == null ? "null" : skill.getId());

        return b.toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class SkillLevelComplexTypeProvider implements IComplexTypeProvider
    {

        private static final String LEVEL_STATUS_FIELD = "level";
        private static final String MOTIVATION_STATUS_FIELD = "motivation";

        public List<String> getValidEnum(String field)
        {
            if (LEVEL_STATUS_FIELD.equals(field))
                return EnumUtils.getEnumStringValues(ESkillLevel.class);
            if (MOTIVATION_STATUS_FIELD.equals(field))
                return EnumUtils.getEnumStringValues(ESkillMotivation.class);

            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            if (LEVEL_STATUS_FIELD.equals(field))
            {
                return EnumUtils.getEnumValueIgnoreCase(ESkillLevel.class, value);
            }
            else if (MOTIVATION_STATUS_FIELD.equals(field))
            {
                return EnumUtils.getEnumValueIgnoreCase(ESkillMotivation.class, value);
            }
            else
            {
                return null;
            }
        }

        public boolean isValidTypeField(String field)
        {
            return LEVEL_STATUS_FIELD.equals(field) || MOTIVATION_STATUS_FIELD.equals(field);
        }
    }
}
