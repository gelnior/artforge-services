package fr.hd3d.model.persistence.impl.hibernate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Table(name = "hd3d_person_day")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class PersonDayH extends BaseH implements IPersonDay
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String DATE_FIELD = "date";
    private static final String ACTIVITITES_APPROVED_DATE_FIELD = "activitiesApprovedDate";
    private Date date;
    private IPerson person;
    private Set<IActivity> activities;
    private IPerson activitiesApprovedBy;
    private Date activitiesApprovedDate;
    private List<IWorkingTime> workingTimes;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_personday"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public PersonDayH()
    {}

    /**
     * @param id
     * @param version
     */
    public PersonDayH(Long id, java.sql.Timestamp version, Date date, IPerson person)
    {
        super(id, version);
        this.setDate(date);
        this.person = person;
    }

    public PersonDayH(Date date, IPerson person)
    {
        this(null, null, date, person);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "person_day_date")
    @Index(name = "person_day_date_idx")
    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    @ManyToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "person_day_person_id")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getPerson()
    {
        return person;
    }

    public void setPerson(IPerson person)
    {
        this.person = person;
    }

    @OneToMany(targetEntity = ActivityH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "day")
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IActivity> getActivities()
    {
        if (activities == null)
            activities = new HashSet<IActivity>();
        return activities;
    }

    public void setActivities(Set<IActivity> activities)
    {
        this.activities = activities;
    }

    @ManyToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "person_day_approved_by_person_id")
    @BatchSize(size = 50)
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    public IPerson getActivitiesApprovedBy()
    {
        return activitiesApprovedBy;
    }

    @OneToMany(targetEntity = WorkingTimeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "personDay")
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public List<IWorkingTime> getWorkingTimes()
    {
        if (workingTimes == null)
            workingTimes = new ArrayList<IWorkingTime>();
        return workingTimes;
    }

    public void setWorkingTimes(List<IWorkingTime> workingTimes)
    {
        this.workingTimes = workingTimes;
    }

    public void setActivitiesApprovedBy(IPerson activitiesApprovedBy)
    {
        this.activitiesApprovedBy = activitiesApprovedBy;
    }

    @Column(name = "person_day_approved_date")
    public Date getActivitiesApprovedDate()
    {
        return activitiesApprovedDate;
    }

    public void setActivitiesApprovedDate(Date activitiesApprovedDate)
    {
        this.activitiesApprovedDate = activitiesApprovedDate;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((activitiesApprovedBy == null) ? 0 : activitiesApprovedBy.hashCode());
        result = prime * result + ((activitiesApprovedDate == null) ? 0 : activitiesApprovedDate.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((person == null) ? 0 : person.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PersonDayH other = (PersonDayH) obj;
        if (activitiesApprovedBy == null)
        {
            if (other.activitiesApprovedBy != null)
                return false;
        }
        else if (!activitiesApprovedBy.equals(other.activitiesApprovedBy))
            return false;
        if (activitiesApprovedDate == null)
        {
            if (other.activitiesApprovedDate != null)
                return false;
        }
        else if (!activitiesApprovedDate.equals(other.activitiesApprovedDate))
            return false;
        if (date == null)
        {
            if (other.date != null)
                return false;
        }
        else if (!date.equals(other.date))
            return false;
        if (person == null)
        {
            if (other.person != null)
                return false;
        }
        else if (!person.equals(other.person))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILPersonDay, IPersonDay> defaultTranslator()
    {
        return Translators.personDay;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("date", date);
        b.append("personId", person == null ? "null" : person.getId());
        // b.append("activitiesIds", StringUtils.join(CollectUtils.getIds(getActivities()), ','));
        // b.append("activitiesApprovedById", activitiesApprovedBy == null ? "null" : activitiesApprovedBy.getId());
        // b.append("activitiesApprovedDate", activitiesApprovedDate);
        // b.append("workingTimeIds", StringUtils.join(CollectUtils.getIds(getWorkingTimes()), ','));

        return b.toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class PersonDayComplexTypeProvider implements IComplexTypeProvider
    {
        public List<String> getValidEnum(String field)
        {
            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            if (!isValidTypeField(field))
                return null;

            return fr.hd3d.utils.StringUtils.parseDate(value);
        }

        public boolean isValidTypeField(String field)
        {
            return DATE_FIELD.equals(field) || ACTIVITITES_APPROVED_DATE_FIELD.equals(field);
        }
    }
}
