package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTag;
import fr.hd3d.model.lightweight.impl.LTag;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.predicate.MatchParentIdTypePredicate;
import fr.hd3d.utils.predicate.MatchParentsByTypePredicate;
import fr.hd3d.utils.transformer.GetParentsTransformer;
import fr.hd3d.utils.transformer.GetTagParentsIdTypeTransformer;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Table(name = "hd3d_tag")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class TagH extends BaseH implements ITag
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String name;
    protected List<TagParent> inverseTagParents;
    protected List<LTag.LTagParentIdType> tagParentsIdType;// not persisted
    private List<ITagCategory> categories;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_tag"), @Parameter(name = "initial_value", value = "1"),
            @Parameter(name = "increment_size", value = "1"), @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public TagH()
    {}

    public TagH(Long id, Timestamp version, String name)
    {
        super(id, version);
        this.name = name;
    }

    public TagH(String name)
    {
        this(null, null, name);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "tag_name", nullable = false)
    @NotEmpty
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @OneToMany(targetEntity = TagParent.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "tag", orphanRemoval = true)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @LazyToOne(LazyToOneOption.PROXY)
    public List<TagParent> getInverseTagParents()
    {
        if (inverseTagParents == null)
            inverseTagParents = new ArrayList<TagParent>();
        return inverseTagParents;
    }

    public void setInverseTagParents(List<TagParent> inverseTagParents)
    {
        this.inverseTagParents = inverseTagParents;
    }

    @ManyToMany(targetEntity = TagCategoryH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    // @IndexColumn is IMPORTANT: to avoid "cannot recreate collection while filter is enabled" error
    @IndexColumn(name = "tagcategory_index", base = 0)
    @JoinTable(name = "hd3d_tagcategory__tag", joinColumns = @JoinColumn(name = "tag_id"), inverseJoinColumns = @JoinColumn(name = "tagcategory_id"))
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public List<ITagCategory> getCategories()
    {
        if (categories == null)
            categories = new ArrayList<ITagCategory>();
        return categories;
    }

    public void setCategories(List<ITagCategory> categories)
    {
        this.categories = categories;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        TagH other = (TagH) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    public List<LTag.LTagParentIdType> parentsIdType()
    {
        if (getInverseTagParents().isEmpty())
            return new ArrayList<LTag.LTagParentIdType>(0);

        return new ArrayList<LTag.LTagParentIdType>(org.apache.commons.collections15.CollectionUtils.collect(
                getInverseTagParents(), new GetTagParentsIdTypeTransformer()));
    }

    public <T extends IBase> Collection<T> parentsByType(String type)
    {
        if (StringUtils.isBlank(type) || getInverseTagParents().isEmpty())
            return new ArrayList<T>(0);

        // get matching TagParents given type
        Collection<TagParent> matched = org.apache.commons.collections15.CollectionUtils.select(getInverseTagParents(),
                new MatchParentsByTypePredicate(type));

        return new ArrayList<T>(org.apache.commons.collections15.CollectionUtils.collect(matched,
                new GetParentsTransformer<T>()));
    }

    public boolean containsParent(Long id, String type)
    {
        if (isNotValidId(id) || StringUtils.isEmpty(type))// the tag's parent type may be null
            return false;

        return org.apache.commons.collections15.CollectionUtils.exists(getInverseTagParents(),
                new MatchParentIdTypePredicate(id, type));
    }

    public <T extends IBase> void addParent(T object)
    {
        if (object == null)
            return;

        if (containsParent(object.getId(), object.getClass().getCanonicalName()))
            return;

        getInverseTagParents().add(newTagParent(object, this));
    }

    public <T extends IBase> void removeParents(Collection<T> objects)
    {
        if (objects == null || objects.isEmpty())
            return;

        for (T object : objects)
            removeParent(object);
    }

    public <T extends IBase> void removeParent(T object)
    {
        if (object == null)
            return;

        removeInverseTagParent(object.getId(), object.entityName());
    }

    public void removeInverseTagParent(Long parentId, String parentType)
    {
        if (isNotValidId(parentId) || getInverseTagParents().isEmpty())
            return;

        TagParent tagparent = org.apache.commons.collections15.CollectionUtils.find(getInverseTagParents(),
                new MatchParentIdTypePredicate(parentId, parentType));

        if (tagparent == null)
            return;

        getInverseTagParents().remove(tagparent);
    }

    @SuppressWarnings("unchecked")
    public <T extends IBase> List<T> taggedEntities() throws Hd3dException
    {
        /* retrieve the entities */
        List<T> ret = new ArrayList<T>();
        IPersist persistor = null;
        for (Map.Entry<String, Collection<Long>> entry : taggedEntitiesIds().entrySet())
        {
            persistor = Persistors.getPersistor(entry.getKey());
            if (persistor == null)
                continue;
            ret.addAll(persistor.getByIds(entry.getValue()));
        }

        return ret;
    }

    public MultiHashMap<String, Long> taggedEntitiesIds() throws Hd3dException
    {
        /* Collect ids of the entities */
        MultiHashMap<String, Long> entity_N_Ids = new MultiHashMap<String, Long>();
        for (TagParent tp : getInverseTagParents())
        {
            entity_N_Ids.put(tp.getParentType(), tp.getParent());
        }

        return entity_N_Ids;

    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILTag, ITag> defaultTranslator()
    {
        return Translators.tag;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        getInverseTagParents().clear();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
