package fr.hd3d.model.persistence.impl.hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevisionLink;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * 
 * @author try LAM
 * 
 */
@Entity
@Table(name = "hd3d_filerevisionlink")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class FileRevisionLinkH extends BaseH implements IFileRevisionLink
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private IFileRevision inputFileRevision;
    private IFileRevision outputFileRevision;
    private String type;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_filerevisionlink"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public FileRevisionLinkH()
    {}

    public FileRevisionLinkH(Long id, java.sql.Timestamp version, IFileRevision inputFileRevision,
            IFileRevision outputFileRevision, String type)
    {
        super(id, version);
        this.inputFileRevision = inputFileRevision;
        this.outputFileRevision = outputFileRevision;
        this.type = type;
    }

    public FileRevisionLinkH(IFileRevision inputFileRevision, IFileRevision outputFileRevision, String type)
    {
        this(null, null, inputFileRevision, outputFileRevision, type);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @OneToOne(targetEntity = FileRevisionH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "filerevisionlink_inputfile")
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IFileRevision getInputFileRevision()
    {
        return inputFileRevision;
    }

    @OneToOne(targetEntity = FileRevisionH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "filerevisionlink_outputfile")
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IFileRevision getOutputFileRevision()
    {
        return outputFileRevision;
    }

    public String getType()
    {
        return type;
    }

    public void setInputFileRevision(IFileRevision inputFileRevision)
    {
        this.inputFileRevision = inputFileRevision;
    }

    public void setOutputFileRevision(IFileRevision outputFileRevision)
    {
        this.outputFileRevision = outputFileRevision;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((inputFileRevision == null) ? 0 : inputFileRevision.hashCode());
        result = prime * result + ((outputFileRevision == null) ? 0 : outputFileRevision.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        FileRevisionLinkH other = (FileRevisionLinkH) obj;
        if (inputFileRevision == null)
        {
            if (other.inputFileRevision != null)
                return false;
        }
        else if (!inputFileRevision.equals(other.inputFileRevision))
            return false;
        if (outputFileRevision == null)
        {
            if (other.outputFileRevision != null)
                return false;
        }
        else if (!outputFileRevision.equals(other.outputFileRevision))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILFileRevisionLink, IFileRevisionLink> defaultTranslator()
    {
        return Translators.filerevisionlink;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("inputFileRevisionId", inputFileRevision == null ? "null" : inputFileRevision.getId());
        b.append("outputFileRevisionId", outputFileRevision == null ? "null" : outputFileRevision.getId());
        b.append("type", type);

        return b.toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
