package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * A TaskChanges is related to an original Task and stands for a Task modification (startDate, endDate, duration,
 * etc...)
 * 
 * @author michael.guiral
 * 
 */
@Entity
@Table(name = "hd3d_task_changes")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class TaskChangesH extends TaskBaseH implements ITaskChanges
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Integer index;// display order
    private ITask task;
    private IPerson worker;
    private IPlanning planning;

    /*-------------
     * Constructors
     -------------*/
    public TaskChangesH()
    {}

    public TaskChangesH(Long id, java.sql.Timestamp version, IPerson worker, Date startDate, Date endDate,
            Long duration, ITaskGroup taskGroup, IPlanning planning, ITask task)
    {
        super(id, version, startDate, endDate, duration, taskGroup);
        this.planning = planning;
        if (planning != null)
            planning.addTaskChanges(this);
        this.task = task;
        this.worker = worker;
    }

    public TaskChangesH(IPerson worker, Date startDate, Date endDate, Long duration, ITaskGroup taskGroup,
            IPlanning planning, ITask task)
    {
        this(null, null, worker, startDate, endDate, duration, taskGroup, planning, task);
    }

    public TaskChangesH(ITaskChanges taskChanges)
    {
        super(taskChanges.getId(), taskChanges.getVersion(), taskChanges.getStartDate(), taskChanges.getEndDate(),
                taskChanges.getDuration(), taskChanges.getTaskGroup());
        this.worker = taskChanges.getWorker();
        this.planning = taskChanges.getPlanning();
        this.task = taskChanges.getTask();
    }

    public TaskChangesH(ITask task, IPlanning planning)
    {
        this(task.getWorker(), task.getStartDate(), task.getEndDate(), task.getDuration(), task.getTaskGroup(),
                planning, task);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "taskchanges_index")
    public Integer getIndex()
    {
        return index;
    }

    public void setIndex(Integer index)
    {
        if (index == null)
            this.index = NumberUtils.INTEGER_ZERO;
        else
            this.index = index;
    }

    @ManyToOne(targetEntity = TaskH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @JoinColumn(name = "task_changes_task_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public ITask getTask()
    {
        return task;
    }

    public void setTask(ITask task)
    {
        this.task = task;
    }

    @ManyToOne(targetEntity = PersonH.class, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "task_changes_worker_id")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getWorker()
    {
        return worker;
    }

    public void setWorker(IPerson worker)
    {
        this.worker = worker;
    }

    @ManyToOne(targetEntity = PlanningH.class, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @JoinColumn(name = "task_changes_planning_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public IPlanning getPlanning()
    {
        return planning;
    }

    public void setPlanning(IPlanning planning)
    {
        this.planning = planning;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((worker == null) ? 0 : worker.hashCode());
        result = prime * result + ((planning == null) ? 0 : planning.hashCode());
        result = prime * result + ((task == null) ? 0 : task.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        TaskChangesH other = (TaskChangesH) obj;
        if (worker == null)
        {
            if (other.worker != null)
                return false;
        }
        else if (!worker.equals(other.worker))
            return false;
        if (planning == null)
        {
            if (other.planning != null)
                return false;
        }
        else if (!planning.equals(other.planning))
            return false;
        if (task == null)
        {
            if (other.task != null)
                return false;
        }
        else if (!task.equals(other.task))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    public IBaseTranslator<?, ?> defaultTranslator()
    {
        return Translators.taskChanges;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("index", index);
        b.append("taskId", task == null ? "null" : task.getId());
        b.append("workerId", worker == null ? "null" : worker.getId());
        b.append("planningId", planning == null ? "null" : planning.getId());

        return b.toString();
    }

    /**
     * updates the bound task with this task changes' info
     */
    public void updateTask()
    {
        final ITask task = getTask();
        task.setWorker(getWorker());
        task.setStartDate(getStartDate());
        task.setEndDate(getEndDate());
        task.setDuration(getDuration());
        task.setExtraLine(getExtraLine());
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class TaskChangesComplexeTypeProvider extends TaskBaseComplexTypeProvider implements
            IComplexTypeProvider
    {

        public List<String> getValidEnum(String field)
        {
            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            return super.getValue(field, value);
        }

        public boolean isValidTypeField(String field)
        {
            return super.isValidTypeField(field);
        }
    }
}
