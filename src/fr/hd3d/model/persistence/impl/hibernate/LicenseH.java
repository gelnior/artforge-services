package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.common.client.enums.ELicenseType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILLicense;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.EnumUtils;


/**
 * License hibernate object needed for database persistance.
 * 
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_license")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class LicenseH extends InventoryItemH implements ILicense
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String TYPE_FIELD = "type";

    private Integer number;
    private ELicenseType type;
    private ISoftware software;
    private Set<IComputer> computers;

    /*-------------
     * Constructors
     -------------*/
    public LicenseH()
    {}

    public LicenseH(Long id, Timestamp version, String name, String serial, String billingReference, Date purchaseDate,
            Date warrantyEnd, EInventoryStatus inventoryStatus, Integer number, ELicenseType type, ISoftware software,
            Set<IComputer> computers, Set<IResourceGroup> resourceGroups)
    {
        super(id, version, name, serial, billingReference, purchaseDate, warrantyEnd, inventoryStatus, resourceGroups);

        this.name = name;
        this.number = number;
        this.type = type;
        this.software = software;
        this.computers = computers;
    }

    public LicenseH(String name, String serial, String billingReference, Date purchaseDate, Date warrantyEnd,
            EInventoryStatus inventoryStatus, Integer number, ELicenseType type, ISoftware software,
            Set<IComputer> computers, Set<IResourceGroup> resourceGroups)
    {
        this(null, null, name, serial, billingReference, purchaseDate, warrantyEnd, inventoryStatus, number, type,
                software, computers, resourceGroups);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "license_number")
    public Integer getNumber()
    {
        return this.number;
    }

    public void setNumber(Integer number)
    {
        if (number != null && number > 0)
        {
            this.number = number;
        }
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "license_type", nullable = false)
    public ELicenseType getType()
    {
        return type;
    }

    public void setType(ELicenseType type)
    {
        this.type = type;
    }

    @ManyToOne(targetEntity = SoftwareH.class)
    @JoinColumn(name = "software_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public ISoftware getSoftware()
    {
        return software;
    }

    public void setSoftware(ISoftware software)
    {
        this.software = software;
    }

    @ManyToMany(targetEntity = ComputerH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_computer__license", joinColumns = @JoinColumn(name = "license_id"), inverseJoinColumns = @JoinColumn(name = "computer_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IComputer> getComputers()
    {
        if (this.computers == null)
        {
            this.computers = new HashSet<IComputer>();
        }

        return this.computers;
    }

    public void setComputers(Set<IComputer> computers)
    {
        this.computers = computers;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((number == null) ? 0 : number.hashCode());
        result = prime * result + ((software == null) ? 0 : software.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LicenseH other = (LicenseH) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (number == null)
        {
            if (other.number != null)
                return false;
        }
        else if (!number.equals(other.number))
            return false;
        if (software == null)
        {
            if (other.software != null)
                return false;
        }
        else if (!software.equals(other.software))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    public IBaseTranslator<ILLicense, ILicense> defaultTranslator()
    {
        return Translators.license;
    }

    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        for (IComputer c : getComputers())
        {
            c.getDevices().remove(this);
        }
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    @Override
    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("number", number).append("type", type);
    // b.append("softwareId", software == null ? "null" : software.getId());
    // b.append("computerIds", StringUtils.join(CollectUtils.getIds(getComputers()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class LicenseComplexTypeProvider extends InventoryItemComplexTypeProvider
    {

        public Object getValue(String field, String value)
        {
            Object ret = super.getValue(field, value);
            if (ret != null)
                return ret;

            return TYPE_FIELD.equals(field) ? EnumUtils.getEnumValueIgnoreCase(ELicenseType.class, value) : null;
        }

        public List<String> getValidEnum(String field)
        {
            List<String> ret = super.getValidEnum(field);
            if (CollectionUtils.isNotEmpty(ret))
                return ret;

            if (TYPE_FIELD.equals(field))
            {
                return EnumUtils.getEnumStringValues(ELicenseType.class);
            }
            return super.getValidEnum(field);
        }

        public boolean isValidTypeField(String field)
        {
            return super.isDateField(field) || TYPE_FIELD.equals(field);
        }
    }

}
