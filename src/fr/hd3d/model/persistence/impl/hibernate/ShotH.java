package fr.hd3d.model.persistence.impl.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.HibernateUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Indexed
@Table(name = "hd3d_shot")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ShotH extends BaseH implements IShot
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private ISequence sequence;
    private Integer completion = 0;
    private String description;
    private Integer difficulty = 0;
    private String hook;
    private String label;
    private Integer trust = 0;
    private Boolean mattePainting = Boolean.FALSE;
    private Boolean layout = Boolean.FALSE;
    private Boolean animation = Boolean.FALSE;
    private Boolean export = Boolean.FALSE;
    private Boolean tracking = Boolean.FALSE;
    private Boolean dressing = Boolean.FALSE;
    private Boolean lighting = Boolean.FALSE;
    private Boolean rendering = Boolean.FALSE;
    private Boolean compositing = Boolean.FALSE;
    private Integer nbFrame = 1;
    private Integer startFrame = 0;
    private Integer endFrame = 0;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_shot"), @Parameter(name = "initial_value", value = "1"),
            @Parameter(name = "increment_size", value = "1"), @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ShotH()
    {
        super();
    }

    public ShotH(Long id, java.sql.Timestamp version, ISequence sequence, Integer completion, String description,
            Integer difficulty, String hook, String label, Integer trust, Boolean mattePainting, Boolean layout,
            Boolean animation, Boolean export, Boolean tracking, Boolean dressing, Boolean lighting, Boolean rendering,
            Boolean compositing, Integer nbFrame, Integer startFrame, Integer endFrame)
    {
        super(id, version);
        this.sequence = sequence;
        if (completion != null)
            this.completion = completion;
        this.description = description;
        if (difficulty != null)
            this.difficulty = difficulty;
        this.hook = hook;
        this.label = label;
        if (trust != null)
            this.trust = trust;
        if (mattePainting != null)
            this.mattePainting = mattePainting;
        if (layout != null)
            this.layout = layout;
        this.animation = animation;
        if (completion != null)
            this.export = export;
        if (tracking != null)
            this.tracking = tracking;
        if (dressing != null)
            this.dressing = dressing;
        if (lighting != null)
            this.lighting = lighting;
        if (rendering != null)
            this.rendering = rendering;
        if (compositing != null)
            this.compositing = compositing;
        if (nbFrame != null)
            this.nbFrame = nbFrame;
        if (startFrame != null)
            this.startFrame = startFrame;
        if (endFrame != null)
            this.endFrame = endFrame;
    }

    public ShotH(ISequence sequence, Integer completion, String description, Integer difficulty, String hook,
            String label, Integer trust, Boolean mattePainting, Boolean layout, Boolean animation, Boolean export,
            Boolean tracking, Boolean dressing, Boolean lighting, Boolean rendering, Boolean compositing,
            Integer nBFrame, Integer startFrame, Integer endFrame)
    {
        this(null, null, sequence, completion, description, difficulty, hook, label, trust, mattePainting, layout,
                animation, export, tracking, dressing, lighting, rendering, compositing, nBFrame, startFrame, endFrame);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToOne(targetEntity = SequenceH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "shot_sequence_id")
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @IndexedEmbedded(depth = 1, prefix = "sequence_", targetElement = SequenceH.class)
    @BatchSize(size = 50)
    @org.hibernate.annotations.LazyToOne(LazyToOneOption.PROXY)
    @NotFound(action = NotFoundAction.IGNORE)
    public ISequence getSequence()
    {
        return sequence;
    }

    @Column(name = "shot_completion")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    @Min(value = 0)
    public Integer getCompletion()
    {
        return completion;
    }

    @Column(name = "shot_description")
    @Field(index = Index.TOKENIZED, store = Store.YES)
    public String getDescription()
    {
        return description;
    }

    @Column(name = "shot_difficulty")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    @Min(value = 0)
    public Integer getDifficulty()
    {
        return difficulty;
    }

    @Column(name = "shot_hook", nullable = false)
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    public String getHook()
    {
        return hook;
    }

    @Column(name = "shot_label")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    @org.hibernate.annotations.Index(name = "shot_label_idx")
    @NotEmpty
    public String getLabel()
    {
        return label;
    }

    @Column(name = "shot_trust")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    @Min(value = 0)
    public Integer getTrust()
    {
        return trust;
    }

    @Column(name = "shot_mattePainting")
    public Boolean getMattePainting()
    {
        return mattePainting;
    }

    @Column(name = "shot_layout")
    public Boolean getLayout()
    {
        return layout;
    }

    @Column(name = "shot_animation")
    public Boolean getAnimation()
    {
        return animation;
    }

    @Column(name = "shot_export")
    public Boolean getExport()
    {
        return export;
    }

    @Column(name = "shot_tracking")
    public Boolean getTracking()
    {
        return tracking;
    }

    @Column(name = "shot_dressing")
    public Boolean getDressing()
    {
        return dressing;
    }

    @Column(name = "shot_lighting")
    public Boolean getLighting()
    {
        return lighting;
    }

    @Column(name = "shot_rendering")
    public Boolean getRendering()
    {
        return rendering;
    }

    @Column(name = "shot_compositing")
    public Boolean getCompositing()
    {
        return compositing;
    }

    @Column(name = "shot_startframe")
    public Integer getStartFrame()
    {
        return startFrame;
    }

    @Column(name = "shot_endframe")
    public Integer getEndFrame()
    {
        return endFrame;
    }

    @Column(name = "nb_frame")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    @Min(value = 0)
    public Integer getNbFrame()
    {
        return nbFrame;
    }

    public void setSequence(ISequence sequence)
    {
        this.sequence = sequence;
    }

    public void setCompletion(Integer completion)
    {
        this.completion = completion;
    }

    public void setDescription(String desc)
    {
        this.description = desc;
    }

    public void setDifficulty(Integer difficulty)
    {
        this.difficulty = difficulty;
    }

    public void setHook(String hook)
    {
        this.hook = hook;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public void setTrust(Integer trust)
    {
        this.trust = trust;
    }

    public void setMattePainting(Boolean mattePainting)
    {
        this.mattePainting = mattePainting;
    }

    public void setLayout(Boolean layout)
    {
        this.layout = layout;
    }

    public void setAnimation(Boolean animation)
    {
        this.animation = animation;
    }

    public void setExport(Boolean export)
    {
        this.export = export;
    }

    public void setTracking(Boolean tracking)
    {
        this.tracking = tracking;
    }

    public void setDressing(Boolean dressing)
    {
        this.dressing = dressing;
    }

    public void setLighting(Boolean lighting)
    {
        this.lighting = lighting;
    }

    public void setRendering(Boolean rendering)
    {
        this.rendering = rendering;
    }

    public void setCompositing(Boolean compositing)
    {
        this.compositing = compositing;
    }

    /**
     * @param frame
     *            the nBFrame to set
     */
    public void setNbFrame(Integer frame)
    {
        nbFrame = frame;
    }

    public void setStartFrame(Integer startFrame)
    {
        this.startFrame = startFrame;
    }

    public void setEndFrame(Integer endFrame)
    {
        this.endFrame = endFrame;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((animation == null) ? 0 : animation.hashCode());
        result = prime * result + ((completion == null) ? 0 : completion.hashCode());
        result = prime * result + ((compositing == null) ? 0 : compositing.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((difficulty == null) ? 0 : difficulty.hashCode());
        result = prime * result + ((dressing == null) ? 0 : dressing.hashCode());
        result = prime * result + ((export == null) ? 0 : export.hashCode());
        result = prime * result + ((hook == null) ? 0 : hook.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + ((layout == null) ? 0 : layout.hashCode());
        result = prime * result + ((lighting == null) ? 0 : lighting.hashCode());
        result = prime * result + ((mattePainting == null) ? 0 : mattePainting.hashCode());
        result = prime * result + ((nbFrame == null) ? 0 : nbFrame.hashCode());
        result = prime * result + ((rendering == null) ? 0 : rendering.hashCode());
        result = prime * result + ((sequence == null) ? 0 : sequence.hashCode());
        result = prime * result + ((tracking == null) ? 0 : tracking.hashCode());
        result = prime * result + ((trust == null) ? 0 : trust.hashCode());
        result = prime * result + ((startFrame == null) ? 0 : startFrame.hashCode());
        result = prime * result + ((endFrame == null) ? 0 : endFrame.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ShotH other = (ShotH) obj;
        if (animation == null)
        {
            if (other.animation != null)
                return false;
        }
        else if (!animation.equals(other.animation))
            return false;
        if (completion == null)
        {
            if (other.completion != null)
                return false;
        }
        else if (!completion.equals(other.completion))
            return false;
        if (compositing == null)
        {
            if (other.compositing != null)
                return false;
        }
        else if (!compositing.equals(other.compositing))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (difficulty == null)
        {
            if (other.difficulty != null)
                return false;
        }
        else if (!difficulty.equals(other.difficulty))
            return false;
        if (dressing == null)
        {
            if (other.dressing != null)
                return false;
        }
        else if (!dressing.equals(other.dressing))
            return false;
        if (export == null)
        {
            if (other.export != null)
                return false;
        }
        else if (!export.equals(other.export))
            return false;
        if (hook == null)
        {
            if (other.hook != null)
                return false;
        }
        else if (!hook.equals(other.hook))
            return false;
        if (label == null)
        {
            if (other.label != null)
                return false;
        }
        else if (!label.equals(other.label))
            return false;
        if (layout == null)
        {
            if (other.layout != null)
                return false;
        }
        else if (!layout.equals(other.layout))
            return false;
        if (lighting == null)
        {
            if (other.lighting != null)
                return false;
        }
        else if (!lighting.equals(other.lighting))
            return false;
        if (mattePainting == null)
        {
            if (other.mattePainting != null)
                return false;
        }
        else if (!mattePainting.equals(other.mattePainting))
            return false;
        if (nbFrame == null)
        {
            if (other.nbFrame != null)
                return false;
        }
        else if (!nbFrame.equals(other.nbFrame))
            return false;
        if (rendering == null)
        {
            if (other.rendering != null)
                return false;
        }
        else if (!rendering.equals(other.rendering))
            return false;
        if (sequence == null)
        {
            if (other.sequence != null)
                return false;
        }
        else if (!sequence.equals(other.sequence))
            return false;
        if (tracking == null)
        {
            if (other.tracking != null)
                return false;
        }
        else if (!tracking.equals(other.tracking))
            return false;
        if (trust == null)
        {
            if (other.trust != null)
                return false;
        }
        else if (!trust.equals(other.trust))
            return false;
        if (startFrame == null)
        {
            if (other.startFrame != null)
                return false;
        }
        else if (!startFrame.equals(other.startFrame))
            return false;
        if (endFrame == null)
        {
            if (other.endFrame != null)
                return false;
        }
        else if (!endFrame.equals(other.endFrame))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    public void completePrePersistOrUpdate()
    {
        if (StringUtils.isBlank(getHook()))
        {
            setHook(HookHandler.getHook(this));
        }
    }

    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    /*
     * Note: though this method is redundant with @IndexedEmbedded of getSequence(), it is useful because can be called
     * directly without requesting the lucene index.
     */
    @Transient
    @Field(name = "sequences_id", index = Index.TOKENIZED, store = Store.YES)
    public String getSequencesIdString()
    {
        StringBuilder buf = new StringBuilder(100);
        if (getSequence() != null)
        {
            buf.append(getSequence().getId()).append(' ').append(getSequence().getParentIds());
        }
        buf.trimToSize();
        return buf.toString();
    }

    @Transient
    @Field(name = "path", index = Index.UN_TOKENIZED, store = Store.YES)
    public String getPath()
    {
        return path(" > ", false);
    }

    public String path(String separator, boolean includeProject)
    {
        StringBuilder buf = new StringBuilder(100);
        if (getSequence() != null)
        {
            ISequence[] parentsArray = parents();
            if (includeProject && !ArrayUtils.isEmpty(parentsArray))
            {
                IProject project = parentsArray[0].getProject();
                if (project != null)
                {
                    buf.append(project.getName());
                }
            }
            /* build the string */
            for (int i = 0; i < parentsArray.length; i++)
            {
                final ISequence seq = parentsArray[i];
                if (!seq.isRoot())
                {
                    buf.append(seq.getName()).append(" > ");
                }
            }
        }
        buf.trimToSize();
        return buf.toString();
    }

    public ISequence[] parents()
    {
        List<ISequence> parentsList = new ArrayList<ISequence>();
        parentsList.add(getSequence());
        parentsList.addAll(getSequence().parents());
        ISequence[] parentsArray = (ISequence[]) parentsList.toArray(new ISequence[parentsList.size()]);
        CollectionUtils.reverseArray(parentsArray);
        return parentsArray;
    }

    public String thumbnail()
    {
        return this.getHook();
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILShot, IShot> defaultTranslator()
    {
        return Translators.shot;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    /**
     * DO NOT DELETE: called by Sheets
     */
    public List<IApprovalNote> approvalNotes(String approvalTypeId)
    {
        Long id = null;
        if (StringUtils.isNotEmpty(approvalTypeId))
        {
            id = Long.valueOf(approvalTypeId);
        }
        return this.getApprovalNotesByApprovalType(id);
    }

    /**
     * 
     * retourne le numero d'image de départ du plan dans l'episode
     */
    public Long shotStartFrameEpisode()
    {
        String query = "SELECT SUM(shot.nbFrame) FROM " + ShotH.class.getCanonicalName() + " shot "
                + " WHERE shot.sequence.id = " + this.sequence.getId() + " AND shot.label BETWEEN '0' AND '"
                + this.label + "' ORDER BY shot.label";

        Long duration = (Long) HibernateUtil.currentSession().createQuery(query).list().get(0);
        if (duration == null)
        {
            duration = NumberUtils.LONG_ZERO;
        }
        else
        {
            duration = duration - this.nbFrame;
        }
        return duration;
    }

    public String toString()
    {
        return label;
    }
    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("sequenceId", sequence == null ? "null" : sequence.getId());
    // b.append("completion", completion).append("difficulty", difficulty).append("hook", hook).append("label", label)
    // .append("trust", trust).append("mattePainting", mattePainting).append("layout", layout).append(
    // "animation", animation).append("export", export).append("tracking", tracking).append(
    // "dressing", dressing).append("lighting", lighting).append("rendering", rendering).append(
    // "nbFrame", nbFrame);
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
