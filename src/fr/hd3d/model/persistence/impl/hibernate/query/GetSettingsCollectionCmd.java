package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.model.preferences.Setting;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;


public class GetSettingsCollectionCmd extends ListResultQuery
{
    public GetSettingsCollectionCmd(ResourceContext<?, ?> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        final boolean self = "self".equals(context.getResource().getRequest().getOriginalRef().getLastSegment());
        IPerson person = AuthenticationUtil.getCurrentUser();

        final CompositeQueryModifier filter = context.getFilter();

        final Criteria query = HibernateUtil.currentSession().createCriteria(Setting.class);
        if (self)
            if (person != null)
                query.add(Restrictions.eq(Const.PERSON, person.getId()));
            else
                query.add(Restrictions.isNull(Const.PERSON));

        applyModifier(filter, query, forCount);

        if (!forCount)
            setMaxResult(query);

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
