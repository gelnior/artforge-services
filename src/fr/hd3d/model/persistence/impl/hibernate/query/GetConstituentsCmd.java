package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.CATEGORY_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;
import static fr.hd3d.utils.Const.CATEGORY;

import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.ServerResourceUtils;


public class GetConstituentsCmd extends ListResultQuery<ILConstituent, IConstituent>
{
    private Long projectId;
    private Long categoryId;

    public GetConstituentsCmd(ResourceContext<ILConstituent, IConstituent> context)
    {
        super(context);
    }

    public GetConstituentsCmd(ResourceContext<ILConstituent, IConstituent> context, Long projectId, Long categoryId)
    {
        super(context);
        this.projectId = projectId;
        this.categoryId = categoryId;
    }

    public GetConstituentsCmd(Long projectId, Long categoryId)
    {
        super();
        this.projectId = projectId;
        this.categoryId = categoryId;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        final Long projectId = idValue(this.projectId, PROJECT_ID_ATTRIBUTE);
        final Long categoryId = idValue(this.categoryId, CATEGORY_ID_ATTRIBUTE);

        final HibernatePersist<?, ?> persistor = (HibernatePersist<?, ?>) context.getPersistor();
        final CompositeQueryModifier filter = context.getFilter();

        // DetachedCriteria subQuery = persistor.basicSubCriteria(session);
        //
        // applyModifier(filter, subQuery, forCount);

        final Criteria query = persistor.basicCriteria(session);

        if (BaseH.isValidId(categoryId))
        {
            ServerResourceUtils.addIdEqRestriction(context, query, CATEGORY, categoryId);
        }
        else if (BaseH.isValidId(projectId))
        {
            query.createAlias(CATEGORY, "cat").createAlias("cat.project", "proj").add(
                    Restrictions.eq("proj.id", projectId));
        }

        applyModifier(filter, query, forCount);

        if (!forCount)
            setMaxResult(query);

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
