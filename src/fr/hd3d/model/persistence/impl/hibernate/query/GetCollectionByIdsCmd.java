package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetCollectionByIdsCmd<E extends IBase> extends ListResultQuery<ILBase, E>
{
    private final Collection<Long> ids;

    public GetCollectionByIdsCmd(final ResourceContext<?, ?> context, final Collection<Long> id)
    {
        super(context);
        ids = id;
    }

    public GetCollectionByIdsCmd(final Collection<Long> id)
    {
        this(null, id);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new EqCriteriaTransformer(Const.ID, ids));

        return transformers;
    }
}
