package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetPersonDaysCmd extends ListResultQuery<ILPersonDay, IPersonDay>
{
    private Long personId;

    public GetPersonDaysCmd(ResourceContext<ILPersonDay, IPersonDay> context)
    {
        super(context);
    }

    public GetPersonDaysCmd(ResourceContext<ILPersonDay, IPersonDay> context, Long personId)
    {
        super(context);
        this.personId = personId;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long personId = idValue(this.personId, PERSON_ID_ATTRIBUTE);
        checkId(personId, MessageFormat.format(ERR_11, personId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.PERSON, personId));

        return transformers;
    }

}
