package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.ABSENCE_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILAbsence;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetPersonAbsenceCmd extends UniqueResultQuery<ILAbsence, IAbsence>
{
    private Long personId;

    public GetPersonAbsenceCmd(ResourceContext<ILAbsence, IAbsence> context)
    {
        super(context);
    }

    public GetPersonAbsenceCmd(ResourceContext<ILAbsence, IAbsence> context, Long personId)
    {
        super(context);
        this.personId = personId;
    }

    public GetPersonAbsenceCmd(Long personId)
    {
        super();
        this.personId = personId;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        return weaveCriteria(session, context.getIdFromUrl(ABSENCE_ID_ATTRIBUTE));
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers() throws Hd3dPersistenceException
    {
        final Long personId = idValue(this.personId, PERSON_ID_ATTRIBUTE);

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.WORKER, personId));

        return transformers;
    }
}
