package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.ASSETREVISIONGROUP_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;
import static fr.hd3d.utils.Const.KEY;
import static fr.hd3d.utils.Const.PROJECT;
import static fr.hd3d.utils.Const.REVISION;
import static fr.hd3d.utils.Const.VARIATION;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILAssetAbstract;
import fr.hd3d.model.persistence.IAssetAbstract;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.AssetAbstractH;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;
import fr.hd3d.utils.ServerResourceUtils;


public class GetAssetAbstractsCmd extends ListResultQuery<ILAssetAbstract, IAssetAbstract>
{
    public GetAssetAbstractsCmd(ResourceContext<ILAssetAbstract, IAssetAbstract> context)
    {
        super(context);
    }

    @SuppressWarnings("unchecked")
    public void execute(Session session) throws Hd3dException
    {
        final Criteria query = getCriteria(session);

        if (getLockMode() != LockMode.NONE && getLockMode() != null)
        {
            query.setLockMode(getLockMode());
        }

        List<IAssetRevision> list = query.list();

        result = AssetAbstractH.parseIAssets(list);
        result = ObjectProviderCustoms.filter(result);

        setTotalSize(query, session);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {

        List<Criterion> criteria = new ArrayList<Criterion>();

        HibernatePersist<?, ?> provider = (HibernatePersist<?, ?>) Persistors.assetrevision;
        // DetachedCriteria subQuery = provider.basicSubCriteria(session);
        //
        // applyModifier(context.getFilter(), subQuery, forCount);

        final Criteria query = provider.basicCriteria(session);

        // Filter on project
        Long projectId = context.getIdFromUrl(PROJECT_ID_ATTRIBUTE);

        if (BaseH.isValidId(projectId))
        {
            IProject project = Persistors.project.getById(projectId);

            if (project == null)
            {
                result = new ArrayList<IAssetAbstract>();
                return query;
            }

            ServerResourceUtils.addIdEqRestriction(context, query, PROJECT, projectId);
        }

        // Filter on assetgroup
        Long groupId = context.getIdFromUrl(ASSETREVISIONGROUP_ID_ATTRIBUTE);
        if (BaseH.isValidId(groupId))
        {
            IAssetRevisionGroup group = Persistors.assetrevisiongroup.getById(groupId);
            if (group == null)
            {
                result = new ArrayList<IAssetAbstract>();
                return query;
            }
            else if (group.getAssetRevisions().size() < 1)
            {
                result = new ArrayList<IAssetAbstract>();
                return query;
            }
            else
            {
                Disjunction disjunction = Restrictions.disjunction();
                for (IAssetRevision a : group.getAssetRevisions())
                {
                    Criterion key = Restrictions.eq(KEY, a.getKey());
                    Criterion vari = a.getVariation() != null ? Restrictions.eq(VARIATION, a.getVariation())
                            : Restrictions.isNull(VARIATION);
                    disjunction.add(Restrictions.and(key, vari));
                }
                criteria.add(disjunction);
            }
        }

        // Add the criteria to the query
        for (Criterion c : criteria)
            query.add(c);

        query.addOrder(Order.asc(PROJECT));
        query.addOrder(Order.asc(KEY));
        query.addOrder(Order.asc(VARIATION));
        query.addOrder(Order.asc(REVISION));

        applyModifier(context.getFilter(), query, forCount);

        if (!forCount)
            setMaxResult(query);

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
