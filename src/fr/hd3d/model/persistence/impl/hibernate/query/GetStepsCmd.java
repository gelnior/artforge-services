package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILStep;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetStepsCmd extends ListResultQuery<ILStep, IStep>
{
    IBase object = null;

    public GetStepsCmd(ResourceContext<ILStep, IStep> context)
    {
        super(context);
    }

    public GetStepsCmd(IBase object) throws Hd3dPersistenceException
    {
        if (object == null)
        {
            throw new Hd3dPersistenceException("GetStepsCmd: object cannot be null");
        }
        this.object = object;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        if (forCount)
            return (Criteria) weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors.step);
        else
            return (Criteria) weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.step);
    }

    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new EqCriteriaTransformer(context, Const.BOUNDENTITY, object.getId()));
        transformers.add(new EqCriteriaTransformer(context, Const.BOUNDENTITYNAME, object.entityName()));

        return transformers;
    }
}
