package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILProxy;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteH;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;


public class GetLastProxiesCmd extends ListResultQuery<ILProxy, IProxy>
{

    public GetLastProxiesCmd(ResourceContext<ILProxy, IProxy> context)
    {
        super(context);
    }

    @Override
    public void execute(Session session) throws Hd3dException
    {
        Long workObjectId = context.getIdFromUrl(ServicesURI.WORKOBJECT_ID_ATTRIBUTE);
        String boundEntityName = context.getStringFromUrl(ServicesURI.BOUNDENTITY_ID_ATTRIBUTE);
        Long taskTypeId = context.getIdFromUrl(ServicesURI.TASK_TYPE_ID_ATTRIBUTE);

        List<IApprovalNote> approvals = getApprovalNoteWithFileRevision(session, workObjectId, boundEntityName,
                taskTypeId);
        if (CollectionUtils.isNotEmpty(approvals))
        {
            for (IApprovalNote note : approvals)
            {
                Set<IFileRevision> files = note.getFilerevisions();
                if (CollectUtils.isNotEmpty(files))
                {
                    Set<IProxy> proxies = files.iterator().next().getProxies();
                    if (CollectionUtils.isNotEmpty(proxies))
                    {
                        result = new ArrayList<IProxy>(proxies);
                        return;
                    }
                }
            }
        }
    }

    private List<IApprovalNote> getApprovalNoteWithFileRevision(Session session, Long workObjectId,
            String boundEntityName, Long taskTypeId) throws Hd3dException
    {
        final Criteria criteria = ((HibernatePersist) Persistors.approvalnote).basicCriteria(session,
                ApprovalNoteH.class);
        criteria.createAlias("approvalNoteType", "apnotetype").createAlias("apnotetype.taskType", "tasktypealias").add(
                Restrictions.eq("boundEntity", workObjectId)).add(Restrictions.eq("boundEntityName", boundEntityName))

        .add(Restrictions.eq("tasktypealias.id", taskTypeId)).add(Restrictions.isNotNull("filerevisions")).addOrder(
                Order.desc("approvalDate"));

        return criteria.list();
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
