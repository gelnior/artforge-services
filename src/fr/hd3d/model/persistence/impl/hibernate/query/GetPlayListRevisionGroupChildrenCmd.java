package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PLAYLISTREVISIONGROUP_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetPlayListRevisionGroupChildrenCmd extends
        ListResultQuery<ILPlayListRevisionGroup, IPlayListRevisionGroup>
{
    private List<Long> ids;

    public GetPlayListRevisionGroupChildrenCmd(ResourceContext<ILPlayListRevisionGroup, IPlayListRevisionGroup> context)
    {
        super(context);
    }

    public GetPlayListRevisionGroupChildrenCmd(
            ResourceContext<ILPlayListRevisionGroup, IPlayListRevisionGroup> context, List<Long> ids)
    {
        super(context);

        this.ids = ids;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long projectId = context.getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        checkId(projectId, MessageFormat.format(ERR_0, projectId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.PROJECT, projectId));

        if (ids == null)
        {
            final Long playListRevisionGroupId = context.getIdFromUrl(PLAYLISTREVISIONGROUP_ID_ATTRIBUTE);
            checkId(playListRevisionGroupId, MessageFormat.format(ERR_15, playListRevisionGroupId));
            transformers.add(new IdEqCriteriaTransformer(context, Const.PARENT, playListRevisionGroupId));
        }
        else
        {
            transformers.add(new IdEqCriteriaTransformer(context, Const.ID, ids));
        }

        return transformers;
    }
}
