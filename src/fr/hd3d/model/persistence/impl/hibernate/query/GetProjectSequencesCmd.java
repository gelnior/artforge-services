package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetProjectSequencesCmd extends ListResultQuery<ILSequence, ISequence>
{
    public GetProjectSequencesCmd(ResourceContext<ILSequence, ISequence> context)
    {
        super(context);
    }

    protected Criteria buildCriteriaOrQuery(Session session) throws Hd3dException
    {
        return (Criteria) weaveCriteria(session);
    }

    private IProject getProject() throws Hd3dException
    {
        final Long projectId = context.getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        checkId(projectId, MessageFormat.format(ERR_0, projectId));

        IProject project = Persistors.project.getById(projectId);
        if (project == null)
        {
            throw new Hd3dPersistenceException(MessageFormat.format(ERR_0, projectId));
        }
        return project;
    }

    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        transformers.add(new IdEqCriteriaTransformer(context, Const.PROJECT, getProject().getId()));
        transformers.add(new IdEqCriteriaTransformer(context, Const.PARENT, getProject().rootSequence().getId()));

        return transformers;
    }
}
