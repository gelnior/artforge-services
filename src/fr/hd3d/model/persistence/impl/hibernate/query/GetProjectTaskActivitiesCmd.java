package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.AliasCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetProjectTaskActivitiesCmd<L extends ILActivity, P extends ITaskActivity> extends ListResultQuery<L, P>
{
    public GetProjectTaskActivitiesCmd(ResourceContext<L, P> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        if (forCount)
            return (Criteria) weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors.taskActivity);
        else
            return (Criteria) weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.taskActivity);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long projectId = context.getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        checkId(projectId, MessageFormat.format(ERR_0, projectId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        // transformers.add(new FilterApplyer(context.getFilter()));
        // transformers.add(new CreateCriteriaTransformer(context, Const.TASK));
        transformers.add(new AliasCriteriaTransformer(context, "this." + Const.TASK, "activityTask"));
        transformers.add(new AliasCriteriaTransformer(context, "activityTask.project", "activityTaskProject"));
        transformers.add(new EqCriteriaTransformer(context, "activityTaskProject" + Const._ID, projectId));
        // IdEqCriteriaTransformer idEq = new IdEqCriteriaTransformer(context, Const.PROJECT, projectId);
        // idEq.setCurrentClass(TaskH.class);
        // transformers.add(idEq);

        return transformers;
    }

    // protected List<Transformer<Criteria, Criteria>> getTailingTransformers(Session session)
    // {
    // List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
    //
    // transformers.add(new DefaultMaxResultSetter());
    // return transformers;
    // }
}
