package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.SHEET_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILItemGroup;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.OrderCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetItemGroupsCmd extends ListResultQuery<ILItemGroup, IItemGroup>
{
    public GetItemGroupsCmd(ResourceContext<ILItemGroup, IItemGroup> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long sheetId = context.getIdFromUrl(SHEET_ID_ATTRIBUTE);
        checkId(sheetId, MessageFormat.format(ERR_9, sheetId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.SHEET, sheetId));
        transformers.add(new OrderCriteriaTransformer(context, Const.INDEX, true));

        return transformers;
    }

}
