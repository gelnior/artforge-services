package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.TAGCATEGORY_ID_ATTRIBUTE;
import static fr.hd3d.utils.Const.PARENT;

import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILTagCategory;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.ServerResourceUtils;


public class GetTagCategoriesCmd extends ListResultQuery<ILTagCategory, ITagCategory>
{

    public GetTagCategoriesCmd(ResourceContext<ILTagCategory, ITagCategory> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        final HibernatePersist<?, ?> persistor = (HibernatePersist<?, ?>) context.getPersistor();
        final CompositeQueryModifier filter = context.getFilter();

        final Long parentId = context.getIdFromUrl(TAGCATEGORY_ID_ATTRIBUTE);
        // Note: do not check for parentId nullness, it may be null or 0 but not provided

        // retrieves the sub-categories of the given category
        // DetachedCriteria subQuery = persistor.basicSubCriteria(session);
        //
        // applyModifier(filter, subQuery, forCount);

        final Criteria query = persistor.basicCriteria(session);

        if (BaseH.isNotValidId(parentId))
            query.add(Restrictions.isNull(PARENT));
        else
            ServerResourceUtils.addIdEqRestriction(context, query, PARENT, parentId);

        applyModifier(filter, query, forCount);

        setMaxResult(query);

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
