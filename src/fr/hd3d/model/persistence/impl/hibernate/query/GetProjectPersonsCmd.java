package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.AliasCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;


public class GetProjectPersonsCmd extends ListResultQuery<ILPerson, IPerson>
{
    private Long projectId;

    public GetProjectPersonsCmd(ResourceContext<ILPerson, IPerson> context)
    {
        super(context);
    }

    public GetProjectPersonsCmd(ResourceContext<ILPerson, IPerson> context, Long projectId)
    {
        super(context);
        this.projectId = projectId;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        final Long projectId = idValue(this.projectId, PROJECT_ID_ATTRIBUTE);

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new AliasCriteriaTransformer(context, "resourceGroups", "group"));
        transformers.add(new AliasCriteriaTransformer(context, "group.projects", "projs"));
        transformers.add(new EqCriteriaTransformer(context, "projs.id", projectId));

        return transformers;
    }

}
