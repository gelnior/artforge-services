package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.SHEET_ID_ATTRIBUTE;

import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.model.preferences.SheetFilter;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;


public class GetSheetFiltersBySheetCollectionCmd extends ListResultQuery
{
    public GetSheetFiltersBySheetCollectionCmd(ResourceContext<?, ?> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        final Long sheetId = context.getIdFromUrl(SHEET_ID_ATTRIBUTE);

        final CompositeQueryModifier filter = context.getFilter();

        final Criteria query = HibernateUtil.currentSession().createCriteria(SheetFilter.class);
        query.add(Restrictions.eq(Const.SHEET, sheetId));

        applyModifier(filter, query, forCount);

        if (!forCount)
            setMaxResult(query);

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
