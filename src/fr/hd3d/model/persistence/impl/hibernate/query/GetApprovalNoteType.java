package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNoteType;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetApprovalNoteType extends ListResultQuery<ILApprovalNoteType, IApprovalNoteType>
{
    private Long projectId;

    public GetApprovalNoteType(ResourceContext<ILApprovalNoteType, IApprovalNoteType> context)
    {
        super(context);
    }

    public GetApprovalNoteType(ResourceContext<ILApprovalNoteType, IApprovalNoteType> context, Long projectId)
    {
        super(context);
        this.projectId = projectId;
    }

    public GetApprovalNoteType(Long projectId)
    {
        super();
        this.projectId = projectId;
    }

    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long projectId = idValue(this.projectId, PROJECT_ID_ATTRIBUTE);

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        if (BaseH.isValidId(projectId))
        {// case of the approvalnoteType shortcut, when project id is not
            transformers.add(new IdEqCriteriaTransformer(context, Const.PROJECT, projectId));
        }

        return transformers;
    }

}
