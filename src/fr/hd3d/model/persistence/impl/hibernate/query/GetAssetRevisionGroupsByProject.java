package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetAssetRevisionGroupsByProject extends ListResultQuery<ILAssetRevisionGroup, IAssetRevisionGroup>
{
    private Long projectId;

    public GetAssetRevisionGroupsByProject(ResourceContext<ILAssetRevisionGroup, IAssetRevisionGroup> context)
    {
        super(context);
    }

    public GetAssetRevisionGroupsByProject(ResourceContext<ILAssetRevisionGroup, IAssetRevisionGroup> context,
            Long projectId)
    {
        super(context);
        this.projectId = projectId;
    }

    public GetAssetRevisionGroupsByProject(Long projectId)
    {
        super();
        this.projectId = projectId;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        if (forCount)
            return (Criteria) weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors.assetrevisiongroup);
        else
            return (Criteria) weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.assetrevisiongroup);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long projectId = idValue(this.projectId, PROJECT_ID_ATTRIBUTE);
        checkId(projectId, MessageFormat.format(ERR_0, projectId));

        IProject project = Persistors.project.getById(projectId);
        if (project == null)
        {
            throw new Hd3dPersistenceException(MessageFormat.format(ERR_0, projectId));
        }

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.PROJECT, projectId));

        return transformers;
    }
}
