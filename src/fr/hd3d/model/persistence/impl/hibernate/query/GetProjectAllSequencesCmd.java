package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;


public class GetProjectAllSequencesCmd extends ListResultQuery<ILSequence, ISequence>
{
    public GetProjectAllSequencesCmd(ResourceContext<ILSequence, ISequence> context)
    {
        super(context);
    }

    @SuppressWarnings("unchecked")
    public void execute(Session session) throws Hd3dException
    {
        final Long projectId = context.getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        checkId(projectId, MessageFormat.format(ERR_0, projectId));

        IProject project = Persistors.project.getById(projectId);

        if (project == null)
            throw new Hd3dPersistenceException(MessageFormat.format(ERR_0, projectId));

        Set<ISequence> projectSequences = project.getSequences();

        Set<ISequence> allSequences = new HashSet<ISequence>(projectSequences);

        for (ISequence seq : projectSequences)
            allSequences.addAll(seq.allChildren(true));

        result = new ArrayList(allSequences);
        result = ObjectProviderCustoms.filter(result);
        if (result != null)
            totalCount = result.size();
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
