package fr.hd3d.model.persistence.impl.hibernate.query;

import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;


/**
 * An Hibernate command represents a query to execute. This allow encapsulation of the Hibernate Framework and safe
 * execution of the query
 * 
 * @author Guillaume CHATELET
 * 
 */
public interface IHibernateQuery
{
    /**
     * Execute the command passing org.hibernate.Session as a parameter The session is not typed in order to make this
     * class independent of Hibernate.
     * 
     * @param session
     *            Hibernate session
     * @param modifier
     *            a Hibernate Criteria modifier.
     * @throws Hd3dPersistenceException
     * @throws Hd3dException
     */
    void execute(Session session) throws Hd3dException;
}
