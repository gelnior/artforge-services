package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.RESOURCEGROUP_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.AliasCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;


public class GetGroupPersonFullListCmd extends ListResultQuery<ILPerson, IPerson>
{
    public GetGroupPersonFullListCmd(ResourceContext<ILPerson, IPerson> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long groupId = context.getIdFromUrl(RESOURCEGROUP_ID_ATTRIBUTE);

        IResourceGroup parentGroup;
        parentGroup = Persistors.resourcegroup.getById(groupId);

        List<Long> groupIds = new ArrayList<Long>();
        if (parentGroup != null)
        {
            Collection<IResourceGroup> groups = parentGroup.allChildren(true);
            if (CollectionUtils.isNotEmpty(groups))
                for (IResourceGroup group : groups)
                    groupIds.add(group.getId());
        }

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new AliasCriteriaTransformer(context, "resourceGroups", "groups"));
        transformers.add(new EqCriteriaTransformer(context, "groups.id", groupIds));

        return transformers;
    }
}
