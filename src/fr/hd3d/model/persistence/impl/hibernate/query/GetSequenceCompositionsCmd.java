package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.SEQUENCE_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILComposition;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetSequenceCompositionsCmd extends ListResultQuery<ILComposition, IComposition>
{
    public GetSequenceCompositionsCmd(ResourceContext<ILComposition, IComposition> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long sequenceId = context.getIdFromUrl(SEQUENCE_ID_ATTRIBUTE);
        checkId(sequenceId, MessageFormat.format(ERR_5, sequenceId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.SEQUENCE, sequenceId));

        return transformers;
    }

}
