package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.TASK_CHANGES_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILTaskChanges;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.services.resources.ResourceContext;


public class GetPlanningTaskChangeCmd extends UniqueResultQuery<ILTaskChanges, ITaskChanges>
{
    public GetPlanningTaskChangeCmd(ResourceContext<ILTaskChanges, ITaskChanges> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        return weaveCriteria(session, context.getIdFromUrl(TASK_CHANGES_ID_ATTRIBUTE));
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers() throws Hd3dPersistenceException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        return transformers;
    }
}
