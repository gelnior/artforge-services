package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.AliasCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetProjectPersonTaskActivitiesCmd<L extends ILActivity, P extends ITaskActivity> extends
        GetProjectTaskActivitiesCmd<L, P>
{
    public GetProjectPersonTaskActivitiesCmd(ResourceContext<L, P> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        List<Transformer<Criteria, Criteria>> transformers = super.getCoreTransformers(session);

        final Long personId = context.getIdFromUrl(PERSON_ID_ATTRIBUTE);
        checkId(personId, MessageFormat.format(ERR_11, personId));
        // transformers.add(new FilterApplyer(context.getFilter()));
        transformers.add(new AliasCriteriaTransformer(context, "this." + Const.TASK, "activityTask"));
        transformers.add(new EqCriteriaTransformer(context, "activityTask" + '.' + Const.WORKER + Const._ID, personId));
        // transformers.add(new IdEqCriteriaTransformer(context, Const.WORKER, personId));

        return transformers;
    }
}
