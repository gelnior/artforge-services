package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.CATEGORY_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetCategoriesCmd extends ListResultQuery<ILCategory, ICategory>
{
    private Long projectId;
    private Long categoryId;

    public GetCategoriesCmd(ResourceContext<ILCategory, ICategory> context)
    {
        super(context);
    }

    public GetCategoriesCmd(ResourceContext<ILCategory, ICategory> context, Long projectId, Long categoryId)
    {
        super(context);
        this.projectId = projectId;
        this.categoryId = categoryId;
    }

    public GetCategoriesCmd(Long projectId, Long categoryId)
    {
        super();
        this.projectId = projectId;
        this.categoryId = categoryId;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long projectId = idValue(this.projectId, PROJECT_ID_ATTRIBUTE);
        checkId(projectId, MessageFormat.format(ERR_0, projectId));

        final Long categoryId = idValue(this.categoryId, CATEGORY_ID_ATTRIBUTE);
        checkId(categoryId, MessageFormat.format(ERR_3, categoryId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.PROJECT, projectId));
        transformers.add(new IdEqCriteriaTransformer(context, Const.PARENT, categoryId));

        return transformers;
    }
}
