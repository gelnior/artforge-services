package fr.hd3d.model.persistence.impl.hibernate.query;

import java.security.NoSuchAlgorithmException;

import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;


/**
 * @author Guillaume CHATELET An Hibernate command represents a query to execute. This allow encapsulation of the
 *         Hibernate Framework and safe execution of the query
 */
public interface IHibernateTransaction
{
    /**
     * Execute the command passing org.hibernate.Session as a parameter
     * 
     * @param session
     * @throws Hd3dException
     * @throws NoSuchAlgorithmException
     */
    void execute(Session session) throws Hd3dException, NoSuchAlgorithmException;
}
