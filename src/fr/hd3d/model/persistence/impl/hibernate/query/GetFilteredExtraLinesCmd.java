package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.TASK_GROUP_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILExtraLine;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetFilteredExtraLinesCmd extends ListResultQuery<ILExtraLine, IExtraLine>
{
    public GetFilteredExtraLinesCmd(ResourceContext<ILExtraLine, IExtraLine> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long taskGroupId = context.getIdFromUrl(TASK_GROUP_ID_ATTRIBUTE);
        checkId(taskGroupId, MessageFormat.format(ERR_6, taskGroupId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.TASKGROUP, taskGroupId));

        return transformers;
    }
}
