package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PLANNING_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTaskChanges;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetPlanningTaskChangesCmd extends ListResultQuery<ILTaskChanges, ITaskChanges>
{
    public GetPlanningTaskChangesCmd(ResourceContext<ILTaskChanges, ITaskChanges> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long planningId = context.getIdFromUrl(PLANNING_ID_ATTRIBUTE);
        checkId(planningId, MessageFormat.format(ERR_7, planningId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.PLANNING, planningId));

        return transformers;
    }
}
