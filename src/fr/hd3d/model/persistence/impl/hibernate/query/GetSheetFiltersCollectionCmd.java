package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.model.preferences.SheetFilter;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.HibernateUtil;


public class GetSheetFiltersCollectionCmd extends ListResultQuery
{
    public GetSheetFiltersCollectionCmd(ResourceContext<?, ?> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        final CompositeQueryModifier filter = context.getFilter();

        final Criteria query = HibernateUtil.currentSession().createCriteria(SheetFilter.class);

        applyModifier(filter, query, forCount);

        setMaxResult(query);

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
