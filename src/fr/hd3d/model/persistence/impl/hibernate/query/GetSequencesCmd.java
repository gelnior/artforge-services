package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.SEQUENCE_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetSequencesCmd extends ListResultQuery<ILSequence, ISequence>
{
    private List<Long> ids;

    public GetSequencesCmd(ResourceContext<ILSequence, ISequence> context)
    {
        super(context);
    }

    public GetSequencesCmd(ResourceContext<ILSequence, ISequence> context, List<Long> ids)
    {
        super(context);

        this.ids = ids;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long projectId = context.getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        checkId(projectId, MessageFormat.format(ERR_0, projectId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.PROJECT, projectId));
        if (ids == null)
        {
            final Long sequenceId = context.getIdFromUrl(SEQUENCE_ID_ATTRIBUTE);
            checkId(sequenceId, MessageFormat.format(ERR_5, sequenceId));
            transformers.add(new IdEqCriteriaTransformer(context, Const.PARENT, sequenceId));
        }
        else
        {
            transformers.add(new IdEqCriteriaTransformer(context, Const.ID, ids));
        }

        return transformers;
    }
}
