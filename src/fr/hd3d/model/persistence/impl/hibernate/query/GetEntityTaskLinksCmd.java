package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILEntityTaskLink;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;


public class GetEntityTaskLinksCmd extends ListResultQuery<ILEntityTaskLink, IEntityTaskLink>
{
    /* single object query */
    IBase object = null;
    /* multiple objects query */
    List<Long> ids = null;
    String entityName = null;

    public GetEntityTaskLinksCmd(ResourceContext<ILEntityTaskLink, IEntityTaskLink> context)
    {
        super(context);
    }

    public GetEntityTaskLinksCmd(IBase object) throws Hd3dPersistenceException
    {
        if (object == null)
        {
            throw new Hd3dPersistenceException("GetEntityTaskLinksCmd: object cannot be null");
        }
        this.object = object;
        ids = null;
        entityName = null;
    }

    public GetEntityTaskLinksCmd(List<Long> ids, String entityName) throws Hd3dPersistenceException
    {
        if (CollectUtils.isEmpty(ids) || StringUtils.isBlank(entityName))
        {
            throw new Hd3dPersistenceException("GetEntityTaskLinksCmd: ids or entityName cannot be null");
        }
        this.ids = ids;
        this.entityName = entityName;
        this.object = null;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        if (forCount)
            return (Criteria) weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors.entitytasklink);
        else
            return (Criteria) weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.entitytasklink);
    }

    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        if (object != null)
        {
            transformers.add(new EqCriteriaTransformer(context, Const.BOUNDENTITY, object.getId()));
            transformers.add(new EqCriteriaTransformer(context, Const.BOUNDENTITYNAME, object.entityName()));
        }
        else
        {
            transformers.add(new EqCriteriaTransformer(context, Const.BOUNDENTITY, ids));
            transformers.add(new EqCriteriaTransformer(context, Const.BOUNDENTITYNAME, entityName));
        }

        return transformers;
    }
}
