package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;


public class GetBoundEntitiesCmd extends ListResultQuery<ILBase, IBase>
{
    String entityName;
    Collection<Long> ids;

    public GetBoundEntitiesCmd(ResourceContext<ILBase, IBase> context)
    {
        super(context);
    }

    public GetBoundEntitiesCmd(String entityName, Collection<Long> ids) throws Hd3dPersistenceException
    {
        if (entityName == null)
        {
            throw new Hd3dPersistenceException("GetBoundEntitiesCmd: entityName cannot be null");
        }
        if (CollectUtils.isEmpty(ids))
        {
            throw new Hd3dPersistenceException("GetBoundEntitiesCmd: ids cannot be null or empty");
        }

        this.entityName = entityName;
        this.ids = ids;
    }

    public GetBoundEntitiesCmd(ResourceContext<ILBase, IBase> context, String entityName, Collection<Long> ids)
            throws Hd3dPersistenceException
    {
        super(context);
        if (entityName == null)
        {
            throw new Hd3dPersistenceException("GetBoundEntitiesCmd: entityName cannot be null");
        }
        if (CollectUtils.isEmpty(ids))
        {
            throw new Hd3dPersistenceException("GetBoundEntitiesCmd: ids cannot be null or empty");
        }

        this.entityName = entityName;
        this.ids = ids;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        if (forCount)
            return (Criteria) weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors
                    .getPersistor(entityName));
        else
            return (Criteria) weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.getPersistor(entityName));
    }

    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new EqCriteriaTransformer(Const.ID, ids));

        return transformers;
    }
}
