package fr.hd3d.model.persistence.impl.hibernate.query;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILItem;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.OrderCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


/**
 * Returns items linked to given sheet (via {sheetID} URL attribute).
 * 
 * @author HD3D
 */
public class GetSheetItemsCmd extends ListResultQuery<ILItem, IItem>
{
    public GetSheetItemsCmd(ResourceContext<ILItem, IItem> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long sheetId = context.getIdFromUrl(ServicesURI.SHEET_ID_ATTRIBUTE);
        checkId(sheetId, MessageFormat.format(ERR_10, sheetId));

        final ISheet sheet = Persistors.sheet.getById(sheetId);
        List<Long> groupIds = new ArrayList<Long>();
        for (IItemGroup group : sheet.getItemGroups())
            groupIds.add(group.getId());

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.ITEMGROUP + Const._ID, groupIds));
        transformers.add(new OrderCriteriaTransformer(context, Const.INDEX, true));

        return transformers;
    }
}
