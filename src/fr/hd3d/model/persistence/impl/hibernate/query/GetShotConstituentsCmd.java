package fr.hd3d.model.persistence.impl.hibernate.query;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


/**
 * GetConstituentShotsCmd returns constituents linked to given shot via compositions.
 * 
 * @author HD3D
 */
public class GetShotConstituentsCmd extends ListResultQuery<ILConstituent, IConstituent>
{
    public GetShotConstituentsCmd(ResourceContext<ILConstituent, IConstituent> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long shotId = context.getIdFromUrl(ServicesURI.SHOT_ID_ATTRIBUTE);
        checkId(shotId, MessageFormat.format(ERR_1, shotId));

        final List<IComposition> compositions = Persistors.composition.getByValue(Const.SHOT + Const._ID, shotId);
        List<Long> constituentIds = new ArrayList<Long>();
        for (IComposition composition : compositions)
            constituentIds.add(composition.getConstituent().getId());

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(Const.ID, constituentIds));

        return transformers;
    }
}
