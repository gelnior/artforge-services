package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.services.resources.ResourceContext;


public class GetPersonFilesCmd extends ListResultQuery<ILFileRevision, IFileRevision>
{
    private Long personId;

    public GetPersonFilesCmd(ResourceContext<ILFileRevision, IFileRevision> context)
    {
        super(context);
    }

    public GetPersonFilesCmd(ResourceContext<ILFileRevision, IFileRevision> context, Long personId)
    {
        super(context);
        this.personId = personId;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long personId = idValue(this.personId, PERSON_ID_ATTRIBUTE);
        checkId(personId, MessageFormat.format(ERR_11, personId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        // transformers.add(new IdEqCriteriaTransformer(context, Const.WORKER, personId));

        return transformers;
    }
}
