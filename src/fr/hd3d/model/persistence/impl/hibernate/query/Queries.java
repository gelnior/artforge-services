package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.utils.Const.BOUNDENTITY;
import static fr.hd3d.utils.Const.BOUNDENTITYNAME;
import static fr.hd3d.utils.Const.PARENT;
import static fr.hd3d.utils.Const.PARENTTYPE;
import static fr.hd3d.utils.Const.TYPE;
import static org.hibernate.criterion.Restrictions.eq;
import static org.hibernate.criterion.Restrictions.in;
import static org.hibernate.criterion.Restrictions.or;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Transient;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteH;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataValuesToCreate;
import fr.hd3d.model.persistence.impl.hibernate.ScriptH;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;
import fr.hd3d.model.preferences.SheetFilter;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


public final class Queries
{
    private Queries()
    {}

    // Note:ugly
    final static String CHUNK_0 = Const.CANONICALNAME_TASK
            + " task inner join task.boundEntityTaskLinks as links where links.boundEntityName = '";
    final static String CHUNK_1 = "select count(task) from " + CHUNK_0;
    final static String CHUNK_1_1 = "select count(task), links.boundEntity from " + CHUNK_0;
    final static String CHUNK_2 = "' AND links.boundEntity = ";
    final static String CHUNK_2_1 = "' AND links.boundEntity in ";
    final static String CHUNK_3 = " AND task.status = ";
    //
    final static String CHUNK_4 = "select status from " + CHUNK_0;
    final static String CHUNK_6 = " AND task.taskType.id = ";
    //
    final static String CHUNK_7 = "select worker from " + CHUNK_0;
    //
    final static String CHUNK_8 = " select sum(duration) from " + CHUNK_0;
    //
    final static String CHUNK_9 = " select sum(duration) from " + Const.CANONICALNAME_TASKACTIVITY
            + " activity inner join activity.task.boundEntityTaskLinks as links where links.boundEntityName = '";
    final static String CHUNK_10 = "' AND links.boundEntity = ";
    final static String CHUNK_11 = " AND activity.task.taskType.id = ";

    public static List<IEntityTaskLink> getEntityTaskLinks(IBase object)
    {
        List<IEntityTaskLink> ret = new ArrayList<IEntityTaskLink>(0);

        if (object != null && object.isTaskable())
        {
            try
            {
                final GetEntityTaskLinksCmd cmd = new GetEntityTaskLinksCmd(object);
                HibernateUtil.safeQuery(cmd.noMaxResult());
                ret.addAll(cmd.result);
            }
            catch (Hd3dException e)
            {
                Log.LOGGER
                        .error("Error occured when retrieving entitytasklinks for object id={}:{}", object.getId(), e);
            }
        }

        return ret;
    }

    public static List<IEntityTaskLink> getEntityTasksLinks(List<Long> ids, String entityName)
    {
        List<IEntityTaskLink> ret = new ArrayList<IEntityTaskLink>(0);

        if (CollectUtils.isNotEmpty(ids) && StringUtils.isNotEmpty(entityName))
        {
            try
            {
                final GetEntityTaskLinksCmd cmd = new GetEntityTaskLinksCmd(ids, entityName);
                HibernateUtil.safeQuery(cmd.noMaxResult());
                ret.addAll(cmd.result);
            }
            catch (Hd3dException e)
            {
                Object[] buf = new Object[] { StringUtils.join(ids, ','), entityName, e };
                Log.LOGGER.error("Error occured when retrieving entitytasklinks for object ids={}, entityName={}:{}",
                        buf);
            }
        }

        return ret;
    }

    public static List<TagParent> getTagParents(IBase object)
    {
        Criteria criteria = getTagParentsByEntityQuery(object);
        return executeTagParentQuery(criteria);
    }

    public static List<TagParent> getTagParents(List<Long> ids, String entityName)
    {
        Criteria criteria = getTagParentsByParentIdsQuery(ids, entityName);
        return executeTagParentQuery(criteria);
    }

    public static Criteria getTagParentsByEntityQuery(IBase object)
    {
        Criteria criteria = null;

        if (object != null)
        {
            criteria = HibernateUtil.currentSession().createCriteria(TagParent.class);
            criteria.add(eq(PARENT, object.getId()));
            criteria.add(eq(PARENTTYPE, object.entityName()));
        }

        return criteria;
    }

    public static Criteria getTagParentsByParentIdsQuery(List<Long> ids, String entityName)
    {
        Criteria criteria = null;

        if (CollectUtils.isNotEmpty(ids) && StringUtils.isNotEmpty(entityName))
        {
            criteria = HibernateUtil.currentSession().createCriteria(TagParent.class);
            criteria.add(in(PARENT, ids));
            criteria.add(eq(PARENTTYPE, entityName));
        }

        return criteria;
    }

    @SuppressWarnings("unchecked")
    public static List<TagParent> executeTagParentQuery(Criteria criteria)
    {
        List<TagParent> ret = new ArrayList<TagParent>(0);

        if (criteria != null)
        {
            ret.addAll(criteria.list());
        }

        return ret;
    }

    public static List<IDynMetaDataValue> getDynMetaDataValues(IBase object)
    {
        return getDynMetaDataValues(object, null);
    }

    public static List<IDynMetaDataValue> getDynMetaDataValues(List<Long> ids, String entityName)
    {
        return getDynMetaDataValues(ids, entityName, null);
    }

    public static List<IDynMetaDataValue> getDynMetaDataValues(IBase object, Long classDynMetaDataId)
    {
        List<IDynMetaDataValue> ret = new ArrayList<IDynMetaDataValue>(0);

        /* NOTE: do not check classDynMetaDataId nulliness (can be null) */
        if (object != null)
        {
            try
            {
                final GetDynMetaDataValuesCmd cmd = new GetDynMetaDataValuesCmd(object, classDynMetaDataId);
                HibernateUtil.safeQuery(cmd.noMaxResult());
                ret.addAll(cmd.result);
            }
            catch (Hd3dException e)
            {
                Object[] buf = new Object[] { object.getId(), object.entityName(), e };
                Log.LOGGER.error("Error occured when retrieving dynmetadatavalues for object id={}, entityName={}:{}",
                        buf);
            }
        }

        return ret;
    }

    public static List<IDynMetaDataValue> getDynMetaDataValues(List<Long> ids, Long classDynMetaDataId)
    {
        return getDynMetaDataValues(ids, null, classDynMetaDataId);
    }

    public static List<IDynMetaDataValue> getDynMetaDataValues(List<Long> ids, String entityName,
            Long classDynMetaDataId)
    {
        List<IDynMetaDataValue> ret = new ArrayList<IDynMetaDataValue>(0);

        if (CollectUtils.isNotEmpty(ids))
        {
            try
            {
                final GetDynMetaDataValuesCmd cmd = new GetDynMetaDataValuesCmd(ids, entityName, classDynMetaDataId);
                HibernateUtil.safeQuery(cmd.noMaxResult());
                ret.addAll(cmd.result);
            }
            catch (Hd3dException e)
            {
                Object[] buf = new Object[] { StringUtils.join(ids, ','), entityName, e };
                Log.LOGGER.error("Error occured when retrieving dynmetadatavalues for object ids={}, entityName={}:{}",
                        buf);
            }
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    public static Set<String> getDynMetaDataValuesToCreate_ParentTypes()
    {
        Criteria query = HibernateUtil.currentSession().createCriteria(DynMetaDataValuesToCreate.class);
        query.setProjection(Projections.property(PARENTTYPE));
        query.setProjection(Projections.distinct(Projections.property(PARENTTYPE)));
        query.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return new HashSet(query.list());
    }

    @SuppressWarnings("unchecked")
    public static List<DynMetaDataValuesToCreate> getDynMetaDataValuesToCreate(String entityName, List<Long> parentIds)
    {
        List<DynMetaDataValuesToCreate> ret;

        if (StringUtils.isBlank(entityName) && CollectionUtils.isEmpty(parentIds))
        {
            ret = new ArrayList<DynMetaDataValuesToCreate>(0);
        }
        else
        {
            Criteria query = HibernateUtil.currentSession().createCriteria(DynMetaDataValuesToCreate.class);
            if (StringUtils.isNotBlank(entityName))
            {
                query.add(eq(PARENTTYPE, entityName));
            }
            if (CollectUtils.isNotEmpty(parentIds))
            {
                query.add(in(PARENT, parentIds));
            }
            ret = query.list();
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    public static List<DynMetaDataValuesToCreate> getDynMetaDataValuesToCreate(String entityName, Long parentId,
            Long classDynMetaDataId)
    {
        List<DynMetaDataValuesToCreate> ret;
        // search may be done on one of the 3 parameters: entityName OR parentId OR classDynMetaDataId
        // Thus, some of the parameters may be left blank
        if (StringUtils.isBlank(entityName) && BaseH.isNotValidId(parentId) && BaseH.isNotValidId(classDynMetaDataId))
        {
            ret = new ArrayList<DynMetaDataValuesToCreate>(0);
        }
        else
        {
            Criteria query = HibernateUtil.currentSession().createCriteria(DynMetaDataValuesToCreate.class);
            if (StringUtils.isNotBlank(entityName))
            {
                query.add(eq(PARENTTYPE, entityName));
            }
            if (parentId != null)
            {
                query.add(eq(PARENT, parentId));
            }
            if (classDynMetaDataId != null)
            {
                query.add(eq("classDynMetaDataType.id", classDynMetaDataId));
            }
            ret = query.list();
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    public static List<DynMetaDataValuesToCreate> getDynMetaDataValuesToCreate(String entityName)
    {
        List<DynMetaDataValuesToCreate> ret;

        if (StringUtils.isNotBlank(entityName))
        {
            ret = getDynMetaDataValuesToCreateByParentTypeQuery(entityName).list();
        }
        else
        {
            ret = Collections.emptyList();
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    public static List<DynMetaDataValuesToCreate> getDynMetaDataValuesToCreate(String entityName, Long parentId)
    {
        List<DynMetaDataValuesToCreate> ret;

        if (StringUtils.isNotBlank(entityName) && BaseH.isValidId(parentId))
        {
            ret = getDynMetaDataValuesToCreateByParentTypeQuery(entityName).add(eq(PARENT, parentId)).list();
        }
        else
        {
            ret = Collections.emptyList();
        }

        return ret;
    }

    public static Criteria getDynMetaDataValuesToCreateByParentTypeQuery(String entityName)
    {
        Criteria query = HibernateUtil.currentSession().createCriteria(DynMetaDataValuesToCreate.class);
        query.add(eq(PARENTTYPE, entityName));
        return query;
    }

    public static List<IApprovalNote> getApprovalNotes(IBase object)
    {
        return getApprovalNotesByType(object, null);
    }

    public static List<IApprovalNote> getApprovalNotesByType(IBase object, Long approvalNoteTypeId)
    {
        List<IApprovalNote> ret = new ArrayList<IApprovalNote>(0);

        if (object != null && object.isApprovable())
        {
            try
            {
                final GetApprovalNotesCmd cmd = new GetApprovalNotesCmd(object, approvalNoteTypeId);
                HibernateUtil.safeQuery(cmd.noMaxResult());
                ret.addAll(cmd.result);
            }
            catch (Hd3dException e)
            {
                Object[] buf = new Object[] { object.getId(), object.entityName(), e };
                Log.LOGGER.error("Error occured when retrieving approvalnotes for object id={}, entityName={}:{}", buf);
            }
        }

        return ret;
    }

    public static List<IApprovalNote> getApprovalNotes(List<Long> ids, String entityName)
    {
        return getApprovalNotesByType(ids, null, entityName);
    }

    public static List<IApprovalNote> getApprovalNotesByType(List<Long> entityIds, Long approvalNoteTypeId,
            String entityName)
    {
        List<IApprovalNote> ret = new ArrayList<IApprovalNote>(0);

        if (CollectUtils.isNotEmpty(entityIds) && StringUtils.isNotEmpty(entityName))
        {
            try
            {
                final GetApprovalNotesCmd cmd = new GetApprovalNotesCmd(entityIds, entityName, approvalNoteTypeId);
                HibernateUtil.safeQuery(cmd.noMaxResult());
                ret.addAll(cmd.result);
            }
            catch (Hd3dException e)
            {
                Object[] buf = new Object[] { StringUtils.join(entityIds, ','), entityName, e };
                Log.LOGGER
                        .error("Error occured when retrieving approvalnotes for object ids={}, entityName={}:{}", buf);
            }
        }

        return ret;
    }

    /**
     * Return all the approval notes with date > given date
     * 
     * @param entityIds
     *            Work objects ids
     * @param approvalNoteTypeId
     *            ApprovalNote type id
     * @param entityName
     * @param approvalDate
     * @return
     */
    public static List<IApprovalNote> getMoreRecentApprovalNotesByType(List<Long> entityIds, Long approvalNoteTypeId,
            String entityName, Date approvalDate)
    {
        List<IApprovalNote> ret = new ArrayList<IApprovalNote>(0);

        if (CollectUtils.isNotEmpty(entityIds) && StringUtils.isNotEmpty(entityName))
        {
            try
            {
                final GetOrderedMoreRecentApprovalNotesCmd cmd = new GetOrderedMoreRecentApprovalNotesCmd(entityIds,
                        entityName, approvalNoteTypeId, approvalDate);
                HibernateUtil.safeQuery(cmd.noMaxResult());
                ret.addAll(cmd.result);
            }
            catch (Hd3dException e)
            {
                Object[] buf = new Object[] { StringUtils.join(entityIds, ','), entityName, e };
                Log.LOGGER
                        .error("Error occured when retrieving approvalnotes for object ids={}, entityName={}:{}", buf);
            }
        }

        return ret;
    }

    public static List<ITask> getEntitiesTasks(List<Long> entityIds, String entityName)
    {
        List<ITask> ret = new ArrayList<ITask>(0);

        if (CollectUtils.isNotEmpty(entityIds) && StringUtils.isNotEmpty(entityName))
        {
            try
            {
                final GetEntitiesTasksCmd cmd = new GetEntitiesTasksCmd(entityName, entityIds);
                HibernateUtil.safeQuery(cmd.noMaxResult());
                ret.addAll(cmd.result);
            }
            catch (Hd3dException e)
            {
                Object[] buf = new Object[] { StringUtils.join(entityIds, ','), entityName, e };
                Log.LOGGER.error("Error occured when retrieving tasks for object ids={}, entityName={}:{}", buf);
            }
        }

        return ret;
    }

    public static List<ITask> getEntitiesTasksByTaskTypes(List<Long> entityIds, String entityName,
            List<ITaskType> taskTypes)
    {
        List<ITask> ret = new ArrayList<ITask>(0);

        if (CollectUtils.isNotEmpty(entityIds) && StringUtils.isNotEmpty(entityName))
        {
            try
            {
                final GetEntitiesTasksByTaskTypesCmd cmd = new GetEntitiesTasksByTaskTypesCmd(entityName, entityIds,
                        CollectUtils.getIds(taskTypes));
                HibernateUtil.safeQuery(cmd.noMaxResult());
                ret.addAll(cmd.result);
            }
            catch (Hd3dException e)
            {
                Object[] buf = new Object[] { StringUtils.join(entityIds, ','), entityName,
                        StringUtils.join(CollectUtils.getIds(taskTypes), ','), e };
                Log.LOGGER.error(
                        "Error occured when retrieving tasks for object ids={}, entityName={} and taskTypes={}:{} ",
                        buf);
            }
        }

        return ret;
    }

    public static List<IApprovalNote> getApprovalNotesByTypes(List<Long> ids, List<Long> approvalNoteTypeIds,
            String entityName)
    {
        List<IApprovalNote> ret = new ArrayList<IApprovalNote>(0);

        if (CollectUtils.isNotEmpty(ids) && StringUtils.isNotEmpty(entityName))
        {
            try
            {
                final GetApprovalNotesCmd cmd = new GetApprovalNotesCmd(ids, entityName, approvalNoteTypeIds);
                HibernateUtil.safeQuery(cmd.noMaxResult());
                ret.addAll(cmd.result);
            }
            catch (Hd3dException e)
            {
                Object[] buf = new Object[] { StringUtils.join(ids, ','), entityName, e };
                Log.LOGGER
                        .error("Error occured when retrieving approvalnotes for object ids={}, entityName={}:{}", buf);
            }
        }

        return ret;
    }

    @Deprecated
    @SuppressWarnings("unchecked")
    public static List<IApprovalNote> getApprovalNotesByTypes(IBase object, String[] types)
    {
        Criteria criteria = HibernateUtil.currentSession().createCriteria(ApprovalNoteH.class);
        criteria.add(eq(BOUNDENTITY, object.getId()));
        criteria.add(eq(BOUNDENTITYNAME, object.entityName()));
        criteria.add(getOrCriterion(types));
        return criteria.list();
    }

    @Deprecated
    @SuppressWarnings("unchecked")
    public static Set<String> getApprovalNotesDistinctTypes()
    {
        String hql = "select distinct type from ApprovalNoteH";
        Session session = HibernateUtil.currentSession();
        session.enableFilter("internalStatus").setParameter("internalStatus",
                fr.hd3d.common.client.Const.INTERNALSTATUS_ENABLED);
        Set<String> result = (Set<String>) session.createQuery(hql).list();
        return result;
    }

    public static Long getTaskActivitiesDuration(IBase object, Long taskTypeId)
    {
        return executeDurationQuery(getTaskActivitiesDurationQuery(object, taskTypeId));
    }

    static Long executeDurationQuery(String query)
    {
        Long ret = null;

        if (query != null)
        {
            List<Long> result = HibernateUtil.currentSession().createQuery(query.toString()).list();

            if (result == null || result.isEmpty())
            {
                ret = NumberUtils.LONG_ZERO;
            }
            else
            {
                Long duration = (Long) result.get(0);
                ret = duration == null ? NumberUtils.LONG_ZERO : duration;
            }
        }

        return ret;
    }

    static String getTaskActivitiesDurationQuery(IBase object, Long taskTypeId)
    {
        /* build query */
        StringBuilder query = new StringBuilder(200);
        query.append(CHUNK_9).append(object.entityName()).append(CHUNK_10).append(object.getId());
        if (BaseH.isValidId(taskTypeId))
        {
            query.append(CHUNK_11).append(taskTypeId.toString());
        }
        query.trimToSize();

        return query.toString();
    }

    public static Long getTasksEstimation(IBase object, Long taskTypeId)
    {
        return executeDurationQuery(getTasksEstimationQuery(object, taskTypeId));
    }

    static String getTasksEstimationQuery(IBase object, Long taskTypeId)
    {
        StringBuilder query = new StringBuilder(200);
        query.append(CHUNK_8).append(object.entityName()).append(CHUNK_2).append(object.getId());
        if (BaseH.isValidId(taskTypeId))
        {
            query.append(CHUNK_6).append(taskTypeId.toString());
        }
        query.trimToSize();

        return query.toString();
    }

    @SuppressWarnings("unchecked")
    public static List<IPerson> getTaskWorkers(IBase object, Long taskTypeId)
    {
        List<IPerson> ret = new ArrayList<IPerson>(0);

        if (object != null)
        {
            /* build query */
            StringBuilder query = new StringBuilder(200);
            query.append(CHUNK_7).append(object.entityName()).append(CHUNK_2).append(object.getId());
            if (BaseH.isValidId(taskTypeId))
            {
                query.append(CHUNK_6).append(taskTypeId.toString());
            }
            query.trimToSize();

            ret = HibernateUtil.currentSession().createQuery(query.toString()).list();
        }

        return ret;
    }

    public static String getTaskStatus(IBase object, Long taskTypeId)
    {
        String ret = null;

        if (object != null)
        {
            /* build query */
            StringBuilder query = new StringBuilder(200);
            query.append(CHUNK_4).append(object.entityName()).append(CHUNK_2).append(object.getId());

            if (BaseH.isValidId(taskTypeId))
            {
                query.append(CHUNK_6).append(taskTypeId.toString());
            }
            query.trimToSize();

            List<?> values = HibernateUtil.currentSession().createQuery(query.toString()).list();
            if (values.isEmpty())
            {
                ret = "";
            }
            else
            {
                ETaskStatus status = (ETaskStatus) values.get(0);
                ret = status.name();
            }
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    static List<Long> getTaskCount(IBase object, int status)
    {
        List<Long> ret = null;

        if (object != null)
        {
            /* build query */
            StringBuilder query = new StringBuilder(200);
            query.append(CHUNK_1).append(object.entityName()).append(CHUNK_2).append(object.getId());
            if (status != -1)
            {
                query.append(CHUNK_3).append(status);
            }
            query.trimToSize();

            ret = HibernateUtil.currentSession().createQuery(query.toString()).list();
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    public static List<Object[]> getTaskCount(List<Long> ids, int status, String entityName)
    {
        List<Object[]> ret = null;

        if (CollectUtils.isNotEmpty(ids))
        {
            /* build query */
            StringBuilder query = new StringBuilder(200);
            query.append(CHUNK_1_1).append(entityName).append(CHUNK_2_1).append('(' + StringUtils.join(ids, ',') + ')');
            if (status != -1)
            {
                query.append(CHUNK_3).append(status);
            }
            query.trimToSize();

            ret = HibernateUtil.currentSession().createQuery(query.toString()).list();
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    public static List<ScriptH> scripts()
    {
        return HibernateUtil.currentSession().getNamedQuery("script.getAll").list();
    }

    static ScriptH scriptByName(String scriptname)
    {
        return (ScriptH) HibernateUtil.currentSession().getNamedQuery("script.getByName").setParameter("scriptname",
                scriptname).uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public static List<SheetFilter> getSheetFilterListBySheetId(Long sheetId)
    {
        List<SheetFilter> ret = new ArrayList<SheetFilter>(0);

        if (BaseH.isValidId(sheetId))
        {
            Criteria criteria = HibernateUtil.currentSession().createCriteria(SheetFilter.class);
            criteria.add(eq(Const.SHEET, sheetId));
            ret.addAll(criteria.list());
        }

        return ret;
    }

    public static List<IStep> getSteps(IBase object)
    {
        List<IStep> ret = new ArrayList<IStep>(0);

        if (object != null && BaseH.isValidId(object.getId()))
        {
            try
            {
                final GetStepsCmd cmd = new GetStepsCmd(object);
                HibernateUtil.safeQuery(cmd.noMaxResult());
                ret.addAll(cmd.result);
            }
            catch (Hd3dException e)
            {
                Object[] buf = new Object[] { object.getId(), object.entityName(), e };
                Log.LOGGER.error("Error occured when retrieving steps for object id={}, entityName={}:{}", buf);
            }
        }

        return ret;
    }

    public static List<IBase> getBoundEntities(Collection<Long> ids, String entityName)
    {
        List<IBase> ret = new ArrayList<IBase>(0);

        if (CollectUtils.isNotEmpty(ids) && StringUtils.isNotEmpty(entityName))
        {
            try
            {
                final GetBoundEntitiesCmd cmd = new GetBoundEntitiesCmd(entityName, ids);
                HibernateUtil.safeQuery(cmd.noMaxResult());
                ret = cmd.result;
            }
            catch (Hd3dException e)
            {
                Object[] buf = new Object[] { StringUtils.join(ids, ','), entityName, e };
                Log.LOGGER.error("Error occured when retrieving bound entities for object ids={}, entityName={}:{}",
                        buf);
            }
        }

        return ret;
    }

    @Transient
    static private Criterion getOrCriterion(String[] types)
    {
        Criterion ret = null;

        if (types != null)
        {
            final int length = types.length;

            if (length > 0)
            {
                if (length == 1)
                {
                    ret = eq(TYPE, types[0]);
                }
                else if (length == 2)
                {
                    ret = or(eq(TYPE, types[0]), eq(TYPE, types[1]));
                }
                else
                {
                    ret = or(eq(TYPE, types[0]), getOrCriterion((String[]) ArrayUtils.subarray(types, 1, types.length)));
                }
            }
        }

        return ret;
    }
}
