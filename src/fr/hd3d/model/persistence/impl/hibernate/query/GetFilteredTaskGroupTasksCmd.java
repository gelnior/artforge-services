package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PLANNING_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;
import static fr.hd3d.utils.Const.CATEGORY;
import static fr.hd3d.utils.Const.ID;
import static fr.hd3d.utils.Const.MASTER;
import static fr.hd3d.utils.Const.PARENTTYPE;
import static fr.hd3d.utils.Const.PLANNING_ID;
import static fr.hd3d.utils.Const.PROJECTID;
import static fr.hd3d.utils.Const.TASK;
import static fr.hd3d.utils.Const.TASKTYPEIDS;
import static fr.hd3d.utils.Const.TYPE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.sojo.interchange.json.JsonParser;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILTaskBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ITaskBase;
import fr.hd3d.model.persistence.impl.hibernate.CategoryH;
import fr.hd3d.model.persistence.impl.hibernate.EntityTaskLinkH;
import fr.hd3d.model.persistence.impl.hibernate.SequenceH;
import fr.hd3d.model.persistence.impl.hibernate.TaskChangesH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class GetFilteredTaskGroupTasksCmd extends ListResultQuery<ILTaskBase, ITaskBase>
{
    public GetFilteredTaskGroupTasksCmd(ResourceContext<ILTaskBase, ITaskBase> context)
    {
        super(context);
    }

    @SuppressWarnings("unchecked")
    public void execute(Session session) throws Hd3dPersistenceException
    {
        Long projectId = context.getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        Long planningId = context.getIdFromUrl(PLANNING_ID_ATTRIBUTE);
        Map<String, String> map = context.getQueryMap();
        String customValues = map.get(fr.hd3d.common.client.Const.CUSTOM);
        customValues = customValues.substring(1, customValues.length() - 1);
        JsonParser jsonParser = new JsonParser();
        Map<String, Object> object = (Map<String, Object>) jsonParser.parse(customValues);
        String idString = (String) object.get(ID);
        String type = (String) object.get(TYPE);
        String parentType = null;
        Long id = null;
        result = new ArrayList<ITaskBase>();
        if (idString != null)
        {
            id = Long.valueOf((String) object.get(ID));
        }
        Boolean isMaster = (Boolean) object.get(MASTER);
        List<Long> taskTypeIds = (List<Long>) object.get(TASKTYPEIDS);
        List<Long> boundEntityIds = new ArrayList<Long>();
        List<Long> parentBoudEntityIds = new ArrayList<Long>();
        String boundEntityQuery = null;
        String parentBoundEntityQuery = null;
        String taskTypeQuery = null;
        boundEntityIds.add(id);
        if (CATEGORY.equalsIgnoreCase(type))
        {
            parentType = CATEGORY;
            type = CONSTITUENT_TYPE;
            boundEntityIds.clear();
            Criteria criteria = HibernateUtil.currentSession().createCriteria(CategoryH.class);
            criteria.add(Restrictions.idEq(id));
            ICategory category = (ICategory) criteria.uniqueResult();
            returnConsituentIds(category, boundEntityIds, parentBoudEntityIds);
        }
        else if (CONSTITUENT_TYPE.equalsIgnoreCase(type))
        {
            type = CONSTITUENT_TYPE;
        }
        else if (SEQUENCE_TYPE.equalsIgnoreCase(type))
        {
            parentType = SEQUENCE_TYPE;
            type = SHOT_TYPE;
            boundEntityIds.clear();
            Criteria criteria = HibernateUtil.currentSession().createCriteria(SequenceH.class);
            criteria.add(Restrictions.idEq(id));
            ISequence sequence = (ISequence) criteria.uniqueResult();
            returnShotsIds(sequence, boundEntityIds, parentBoudEntityIds);
        }
        else if (SHOT_TYPE.equalsIgnoreCase(type))
        {
            type = SHOT_TYPE;
        }
        if (!parentBoudEntityIds.isEmpty() && parentType != null)
        {
            parentBoundEntityQuery = "(etl.boundEntityName = :parentType and etl.boundEntity in (:parentIds)) or ";
        }
        if (!boundEntityIds.isEmpty() && type != null)
        {
            boundEntityQuery = "(etl.boundEntityName = :type and etl.boundEntity in (:id))";
        }
        if (taskTypeIds != null)
        {
            taskTypeQuery = "etl.task.taskType.id in (:taskTypes)";
        }
        String queryString = "select etl.task from " + EntityTaskLinkH.class.getCanonicalName() + " etl where ";
        String closeEntityQuery = "";
        if (parentBoundEntityQuery != null)
        {
            queryString += "(" + parentBoundEntityQuery;
            closeEntityQuery = ")";
        }
        if (boundEntityQuery != null)
        {
            queryString += boundEntityQuery + closeEntityQuery + " and ";
        }
        if (taskTypeQuery != null)
        {
            queryString += taskTypeQuery + " and ";
        }
        queryString += "etl.task.project.id = :projectId" + " and etl.task.worker.id is not null"
                + " and etl.task.startDate is not null" + " and etl.task.endDate is not null";

        Query query = HibernateUtil.currentSession().createQuery(queryString);
        query.setLong(PROJECTID, projectId);
        if (taskTypeQuery != null)
        {
            query.setParameterList("taskTypes", taskTypeIds);
        }
        if (parentBoundEntityQuery != null)
        {
            query.setString(PARENTTYPE, type);
            query.setParameterList("parentIds", boundEntityIds);
        }
        if (boundEntityQuery != null)
        {
            query.setString(TYPE, type);
            query.setParameterList(ID, boundEntityIds);
        }
        List<ITaskBase> tasks = query.list();
        if (isMaster)
        {
            result.addAll((Collection<? extends ITaskBase>) tasks);
            result = ObjectProviderCustoms.filter(result);
        }
        else
        {
            if (!tasks.isEmpty())
            {
                Criteria taskChangesCriteria = HibernateUtil.currentSession().createCriteria(TaskChangesH.class);
                taskChangesCriteria.add(Restrictions.in(TASK, tasks));
                taskChangesCriteria.add(Restrictions.eq(PLANNING_ID, planningId));
                result = taskChangesCriteria.list();
                result = ObjectProviderCustoms.filter(result);
            }
        }
    }

    private void returnShotsIds(ISequence sequence, List<Long> boundEntityIds, List<Long> parentBoudEntityIds)
    {
        parentBoudEntityIds.add(sequence.getId());
        boundEntityIds.addAll(CollectUtils.getIds(sequence.getShots()));
        for (Iterator<ISequence> iterator = sequence.getChildren().iterator(); iterator.hasNext();)
        {
            ISequence childSequence = (ISequence) iterator.next();
            returnShotsIds(childSequence, boundEntityIds, parentBoudEntityIds);
        }
    }

    private void returnConsituentIds(ICategory category, List<Long> boundEntityIds, List<Long> parentBoudEntityIds)
    {
        parentBoudEntityIds.add(category.getId());
        boundEntityIds.addAll(CollectUtils.getIds(category.getConstituents()));
        for (Iterator<ICategory> iterator = category.getChildren().iterator(); iterator.hasNext();)
        {
            ICategory childCategory = iterator.next();
            returnConsituentIds(childCategory, boundEntityIds, parentBoudEntityIds);
        }
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
