package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILEntityTaskLink;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;


public class GetProjectEntityTaskLinksCmd extends ListResultQuery<ILEntityTaskLink, IEntityTaskLink>
{

    private Long projectId;

    public GetProjectEntityTaskLinksCmd(ResourceContext<ILEntityTaskLink, IEntityTaskLink> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        final Long projectId = idValue(this.projectId, PROJECT_ID_ATTRIBUTE);

        final HibernatePersist<?, ?> persistor = (HibernatePersist<?, ?>) context.getPersistor();
        final CompositeQueryModifier filter = context.getFilter();

        // DetachedCriteria subQuery = persistor.basicSubCriteria(session);
        // applyModifier(filter, subQuery, forCount);

        final Criteria query = persistor.basicCriteria(session);

        if (BaseH.isValidId(projectId))
        {
            query.createAlias("task", "tsk").createAlias("tsk.project", "proj").add(
                    Restrictions.eq("proj.id", projectId));
        }

        applyModifier(filter, query, forCount);

        if (!forCount)
            setMaxResult(query);

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        return null;
    }
}
