package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.utils.Const.ENDDATE;
import static fr.hd3d.utils.Const.ENDDATEVALUE;
import static fr.hd3d.utils.Const.ID;
import static fr.hd3d.utils.Const.IDVALUE;
import static fr.hd3d.utils.Const.PERSON;
import static fr.hd3d.utils.Const.STARTDATE;
import static fr.hd3d.utils.Const.STARTDATEVALUE;
import static fr.hd3d.utils.Const.WORKERIDVALUE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.sojo.interchange.json.JsonParser;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILTaskChanges;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;
import fr.hd3d.utils.HibernateUtil;


public class GetCustomPlanningTaskChangesCmd extends ListResultQuery<ILTaskChanges, ITaskChanges>
{
    private static final String getCustomPlanningTaskChangesQueryString = "from TaskChangesH t where "
            + "t.id not in (select task.id from TaskChangesH task where "
            + "task.startDate >= :endDateValue or task.endDate <= :startDateValue)" + " and t.task.id <> :idValue"
            + " and t.worker.id = :workerIdValue";

    public GetCustomPlanningTaskChangesCmd(ResourceContext<ILTaskChanges, ITaskChanges> context)
    {
        super(context);
    }

    public void execute(Session session) throws Hd3dException
    {
        super.execute(session);
        result = ObjectProviderCustoms.filter(result);

        setTotalSize(query, session);
    }

    @Override
    protected Query buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        Map<String, String> map = context.getQueryMap();
        String customValues = map.get(fr.hd3d.common.client.Const.CUSTOM);
        customValues = customValues.substring(1, customValues.length() - 1);
        JsonParser jsonParser = new JsonParser();
        Map<String, Object> object = (Map<String, Object>) jsonParser.parse(customValues);
        Long id = (Long) object.get(ID);
        String startDateValue = (String) object.get(STARTDATE);
        String endDateValue = (String) object.get(ENDDATE);
        Long person = (Long) object.get(PERSON);
        // String queryString = "from TaskChangesHibernateImpl t where "
        // + "t.id not in (select task.id from TaskChangesHibernateImpl task where "
        // + "task.startDate >= :endDateValue or task.endDate <= :startDateValue)"
        // + " and t.task.id <> :idValue" + " and t.worker.id = :workerIdValue";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        Date startDate = null;
        Date endDate = null;
        try
        {
            startDate = dateFormat.parse(startDateValue);
            endDate = dateFormat.parse(endDateValue);
        }
        catch (ParseException e)
        {
            throw new Hd3dPersistenceException(e);
        }
        Query query = HibernateUtil.currentSession().createQuery(getCustomPlanningTaskChangesQueryString);
        query.setDate(STARTDATEVALUE, startDate);
        query.setDate(ENDDATEVALUE, endDate);
        query.setLong(IDVALUE, id);
        query.setLong(WORKERIDVALUE, person);
        query.setCacheable(false);

        if (!forCount)
            setMaxResult(query);

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
