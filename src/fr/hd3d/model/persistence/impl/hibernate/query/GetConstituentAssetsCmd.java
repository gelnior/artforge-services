package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.CONSTITUENT_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.AliasCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetConstituentAssetsCmd extends ListResultQuery<ILAssetRevision, IAssetRevision>
{
    private Long constituentId;

    public GetConstituentAssetsCmd(ResourceContext<ILAssetRevision, IAssetRevision> context)
    {
        super(context);
    }

    public GetConstituentAssetsCmd(ResourceContext<ILAssetRevision, IAssetRevision> context, Long constituentId)
    {
        super(context);
        this.constituentId = constituentId;
    }

    public GetConstituentAssetsCmd(Long constituentId)
    {
        super();
        this.constituentId = constituentId;
    }

    // @SuppressWarnings("unchecked")
    // public void execute(Session session) throws Hd3dException
    // {
    // List<IAssetRevision> asset = getCriteria(session).list();
    // result
    //
    // // List<Long> ids = new ArrayList<Long>();
    // // for (IAssetRevisionGroup assetGroup : assetGroups)
    // // {
    // // ids.addAll(CollectUtils.getIds(assetGroup.getAssetRevisions()));
    // // }
    // //
    // // if (ids.isEmpty())
    // // {
    // // result = new ArrayList<IAssetRevision>();
    // // totalCount = 0;
    // // }
    // // else
    // // {
    // // Persistors.assetrevision.getByIds(ids);
    // // Criteria query = session.createCriteria(AssetRevisionH.class);
    // // query.add(Restrictions.in(ID, ids));
    // //
    // // applyModifier(context.getFilter(), query);
    // //
    // // setMaxResult(query);
    // //
    // // result = query.list();
    // // result = ObjectProviderCustoms.filter(result);
    // // setTotalSize(query, session);
    // // }
    // }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        if (forCount)
            return (Criteria) weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors.assetrevision);
        else
            return (Criteria) weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.assetrevision);

        // final Long constituentId = idValue(this.constituentId, CONSTITUENT_ID_ATTRIBUTE);
        // checkId(constituentId, MessageFormat.format(ERR_1, constituentId));
        //
        // Criteria criteria = session.createCriteria(AssetRevisionGroupH.class);
        // criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        // criteria.add(Restrictions.eq(CRITERIA, CONSTITUENT_TYPE));
        // criteria.add(Restrictions.eq(VALUE, constituentId.toString()));
        //
        // return criteria;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long constituentId = idValue(this.constituentId, CONSTITUENT_ID_ATTRIBUTE);
        checkId(constituentId, MessageFormat.format(ERR_1, constituentId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        transformers.add(new AliasCriteriaTransformer(context, Const.ASSETREVISIONGROUPS, "assetgroups"));

        transformers.add(new EqCriteriaTransformer(context, "assetgroups." + Const.CRITERIA, CONSTITUENT_TYPE));
        transformers.add(new EqCriteriaTransformer(context, "assetgroups." + Const.VALUE, constituentId.toString()));

        // criteria.add(Restrictions.eq(CRITERIA, CONSTITUENT_TYPE));
        // criteria.add(Restrictions.eq(VALUE, constituentId.toString()));

        return transformers;
    }
}
