package fr.hd3d.model.persistence.impl.hibernate.query;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;


/**
 * Return tasks linked to given entity. Entity ID is retrieved from context, so context ID key must be given.
 * 
 * @author HD3D
 */
public class GetWorkObjectTasksCmd extends ListResultQuery<ILTask, ITask>
{
    private String boundEntityName;
    private String idKey;

    public GetWorkObjectTasksCmd(ResourceContext<ILTask, ITask> context, String idKey, String boundEntityName)
    {
        super(context);
        this.boundEntityName = boundEntityName;
        this.idKey = idKey;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        final Long workObjectId = context.getIdFromUrl(this.idKey);
        checkId(workObjectId, MessageFormat.format(ERR_1, workObjectId));

        String[] properties = new String[] { Const.BOUNDENTITYNAME, Const.BOUNDENTITY };
        Object[] values = new Object[] { boundEntityName, workObjectId };
        final List<IEntityTaskLink> links = Persistors.entitytasklink.getByValues(properties, values, "AND");

        if (CollectUtils.isNotEmpty(links))
        {
            List<Long> taskIds = new ArrayList<Long>();
            for (IEntityTaskLink link : links)
                taskIds.add(link.getTask().getId());

            transformers.add(new EqCriteriaTransformer(Const.ID, taskIds));
        }
        else
        {
            setForceEmptyResult(true);
        }

        return transformers;
    }
}
