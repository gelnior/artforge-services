package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILMileStone;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ServerResourceUtils;


public class GetMilestonesCmd extends ListResultQuery<ILMileStone, IMileStone>
{

    public GetMilestonesCmd(ResourceContext<ILMileStone, IMileStone> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        final Long planningId = context.getIdFromUrl(ServicesURI.PLANNING_ID_ATTRIBUTE);

        final HibernatePersist<?, ?> persistor = (HibernatePersist<?, ?>) context.getPersistor();
        final CompositeQueryModifier filter = context.getFilter();

        // DetachedCriteria subQuery = persistor.basicSubCriteria(session);
        // applyModifier(filter, subQuery, forCount);

        final Criteria query = persistor.basicCriteria(session);
        ServerResourceUtils.addIdEqRestriction(context, query, Const.PLANNING, planningId);

        applyModifier(filter, query, forCount);

        if (forCount)
            setMaxResult(query);

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        return null;
    }

}
