package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PLANNING_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;
import static fr.hd3d.utils.Const.CANONICALNAME_TASKCHANGES;
import static fr.hd3d.utils.Const.ENDDATE;
import static fr.hd3d.utils.Const.EXTRALINE;
import static fr.hd3d.utils.Const.ID;
import static fr.hd3d.utils.Const.PLANNINGID;
import static fr.hd3d.utils.Const.PROJECT;
import static fr.hd3d.utils.Const.STARTDATE;
import static fr.hd3d.utils.Const.WORKER;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.BooleanUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.PlanningH;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.ServerResourceUtils;


public class GetPlanningPlannedTasks extends ListResultQuery<ILTask, ITask>
{
    private static final String getPlanningPlannedTasksQueryString = "select tc.task.id from "
            + CANONICALNAME_TASKCHANGES + " tc where tc.planning.id = :planningId";

    public GetPlanningPlannedTasks()
    {
        super();
    }

    public GetPlanningPlannedTasks(ResourceContext<ILTask, ITask> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        final Long projectId = context.getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        checkId(projectId, MessageFormat.format(ERR_0, projectId));
        final Long planningId = context.getIdFromUrl(PLANNING_ID_ATTRIBUTE);
        checkId(planningId, MessageFormat.format(ERR_7, planningId));

        /* get planning */
        Criteria criteria = HibernateUtil.currentSession().createCriteria(PlanningH.class);
        criteria.add(Restrictions.idEq(planningId));
        IPlanning planning = (IPlanning) criteria.uniqueResult();

        final HibernatePersist<?, ?> persistor = (HibernatePersist<?, ?>) context.getPersistor();
        final CompositeQueryModifier filter = context.getFilter();
        /* tasks query */
        // DetachedCriteria subQuery = persistor.basicSubCriteria(session);
        //
        // applyModifier(filter, subQuery, forCount);
        final Criteria query = persistor.basicCriteria(session);
        ServerResourceUtils.addIdEqRestriction(context, query, PROJECT, projectId);

        if (BooleanUtils.isTrue(planning.isMaster()))
        {
            // Conjunction and_1 = Restrictions.conjunction();
            // and_1.add(Restrictions.ge(STARTDATE, planning.getStartDate()));
            // and_1.add(Restrictions.ge(ENDDATE, planning.getStartDate()));

            Conjunction and_2 = Restrictions.conjunction();
            and_2.add(Restrictions.isNotNull(ENDDATE));
            // and_2.add(and_1);

            Conjunction and_3 = Restrictions.conjunction();
            and_3.add(Restrictions.isNotNull(STARTDATE));
            and_3.add(and_2);

            query.add(and_3);

            // query.add(
            // Restrictions.and(Restrictions.isNotNull(STARTDATE),
            // Restrictions.and(Restrictions.isNotNull(ENDDATE),
            // Restrictions.and(Restrictions.ge(STARTDATE, planning.getStartDate()),
            // Restrictions.ge(ENDDATE, planning.getStartDate())))));
            query.add(Restrictions.isNull(EXTRALINE));
            query.addOrder(Order.asc(WORKER));
        }
        else
        {
            // get task assigned in task changes of planning
            // String queryString = "select tc.task.id from " + TaskChangesHibernateImpl.class.getCanonicalName()
            // + " tc where tc.planning.id = :planningId";
            Query taskInTaskChangesQuery = HibernateUtil.currentSession().createQuery(
                    getPlanningPlannedTasksQueryString);
            taskInTaskChangesQuery.setLong(PLANNINGID, planning.getId());
            List<Long> taskIds = taskInTaskChangesQuery.list();
            if (!taskIds.isEmpty())
            {
                query.add(Restrictions.not(Restrictions.in(ID, taskIds)));
            }
        }

        applyModifier(filter, query, forCount);

        if (!forCount)
            setMaxResult(query);

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
