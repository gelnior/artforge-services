package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.FILEREVISION_ID_ATTRIBUTE;
import static fr.hd3d.utils.Const.OUTPUTFILEREVISION;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetFileRevisionUpstream extends ListResultQuery<ILFileRevision, IFileRevision>
{

    public GetFileRevisionUpstream(ResourceContext<ILFileRevision, IFileRevision> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        Long fileRevisionId = context.getIdFromUrl(FILEREVISION_ID_ATTRIBUTE);
        checkId(fileRevisionId, MessageFormat.format(ERR_14, fileRevisionId));

        final IFileRevision fileRevision = Persistors.filerevision.getById(fileRevisionId);

        if (fileRevision == null)
            throw new Hd3dPersistenceException(MessageFormat.format(ERR_14, fileRevisionId));

        final CompositeQueryModifier filter = context.getFilter();

        /* List the asset revision links where the watched filerevision is input */
        final Criteria query;
        {
            HibernatePersist<?, ?> fileRevisionLinkProvider = (HibernatePersist<?, ?>) Persistors.filerevisionlink;

            // DetachedCriteria subQuery = fileRevisionLinkProvider.basicSubCriteria(session);
            //
            // applyModifier(filter, subQuery, forCount);

            query = fileRevisionLinkProvider.basicCriteria(session).add(
                    Restrictions.eq(OUTPUTFILEREVISION, fileRevision)).setProjection(
                    Projections.groupProperty(Const.INPUTFILEREVISION));

            applyModifier(filter, query, forCount);
        }

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
