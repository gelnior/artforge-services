// package fr.hd3d.model.persistence.impl.hibernate.query;
//
// import static fr.hd3d.common.client.ServicesURI.ASSETREVISION_ID_ATTRIBUTE;
// import static fr.hd3d.utils.Const.ASSETKEY;
// import static fr.hd3d.utils.Const.ASSETREVISION;
// import static fr.hd3d.utils.Const.ASSETVARIATION;
// import static fr.hd3d.utils.Const.KEY;
// import static fr.hd3d.utils.Const.REVISION;
// import static fr.hd3d.utils.Const.VARIATION;
//
// import java.text.MessageFormat;
// import java.util.List;
//
// import org.apache.commons.collections15.Transformer;
// import org.hibernate.Criteria;
// import org.hibernate.Session;
// import org.hibernate.criterion.Conjunction;
// import org.hibernate.criterion.DetachedCriteria;
// import org.hibernate.criterion.Disjunction;
// import org.hibernate.criterion.Restrictions;
//
// import fr.hd3d.exception.Hd3dException;
// import fr.hd3d.exception.Hd3dPersistenceException;
// import fr.hd3d.model.lightweight.ILFileRevision;
// import fr.hd3d.model.persistence.IAssetRevision;
// import fr.hd3d.model.persistence.IAssetRevisionFileRevision;
// import fr.hd3d.model.persistence.IFileRevision;
// import fr.hd3d.model.persistence.Persistors;
// import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
// import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
// import fr.hd3d.services.resources.ResourceContext;
//
//
// public class GetFileRevisionsByAssetRevision extends ListResultQuery<ILFileRevision, IFileRevision>
// {
// public GetFileRevisionsByAssetRevision(ResourceContext<ILFileRevision, IFileRevision> context)
// {
// super(context);
// }
//
// @Override
// protected Criteria buildCriteriaOrQuery(Session session) throws Hd3dException
// {
// Long assetRevisionId = context.getIdFromUrl(ASSETREVISION_ID_ATTRIBUTE);
// checkId(assetRevisionId, MessageFormat.format(ERR_12, assetRevisionId));
//
// IAssetRevision assetRevision = Persistors.assetrevision.getById(assetRevisionId);
//
// if (assetRevision == null)
// throw new Hd3dPersistenceException(MessageFormat.format(ERR_12, assetRevisionId));
//
// // Find all assetrevision to filerevision links for this asset revision
// HibernatePersist assetRevisionFileRevisionProvider = (HibernatePersist) Persistors.assetrevisionfilerevision;
// DetachedCriteria linksSubQuery = assetRevisionFileRevisionProvider.basicSubCriteria(session);
// Criteria linksQuery = assetRevisionFileRevisionProvider.basicCriteria(session, linksSubQuery);
// linksQuery.add(Restrictions.eq(ASSETKEY, assetRevision.getKey()));
// linksQuery.add(Restrictions.eq(ASSETREVISION, assetRevision.getRevision()));
// if (assetRevision.getVariation() != null)
// linksQuery.add(Restrictions.eq(ASSETVARIATION, assetRevision.getVariation()));
// else
// linksQuery.add(Restrictions.isNull(ASSETVARIATION));
// List<IAssetRevisionFileRevision> links = linksQuery.list();
//
// // Query all file revisions corresponding to queried links
// HibernatePersist fileRevisionProvider = (HibernatePersist) Persistors.filerevision;
// DetachedCriteria subQuery = fileRevisionProvider.basicSubCriteria(session);
//
// final CompositeQueryModifier filter = context.getFilter();
//
// applyModifier(filter, subQuery);
//
// final Criteria query = fileRevisionProvider.basicCriteria(session, subQuery);
//
// if (!links.isEmpty()) // If there are links
// {
// Disjunction disjunction = Restrictions.disjunction();
// for (IAssetRevisionFileRevision rel : links)
// {
// Conjunction conjuction = Restrictions.conjunction();
// conjuction.add(Restrictions.eq(KEY, rel.getFileKey()));
// conjuction.add(Restrictions.eq(REVISION, rel.getFileRevision()));
// if (rel.getFileVariation() != null)
// conjuction.add(Restrictions.eq(VARIATION, rel.getFileVariation()));
// else
// conjuction.add(Restrictions.isNull(VARIATION));
// disjunction.add(conjuction);
// }
// query.add(disjunction);
// }
// else
// {
// query.add(Restrictions.idEq(fr.hd3d.common.client.Const.ID_RESET));
// }
//
// applyModifier(filter, query);
//
// return query;
// }
//
// @Override
// protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
// throws Hd3dPersistenceException
// {
// // TODO Auto-generated method stub
// return null;
// }
//
// }
