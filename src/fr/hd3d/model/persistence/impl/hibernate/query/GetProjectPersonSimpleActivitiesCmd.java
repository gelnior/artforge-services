package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetProjectPersonSimpleActivitiesCmd<L extends ILActivity, P extends ITaskActivity> extends
        GetProjectSimpleActivitiesCmd<L, P>
{
    private Long personId;

    public GetProjectPersonSimpleActivitiesCmd(ResourceContext<L, P> context)
    {
        super(context);
    }

    public GetProjectPersonSimpleActivitiesCmd(ResourceContext<L, P> context, Long personId)
    {
        super(context);
        this.personId = personId;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        List<Transformer<Criteria, Criteria>> transformers = super.getCoreTransformers(session);

        final Long personId = idValue(this.personId, PERSON_ID_ATTRIBUTE);
        checkId(personId, MessageFormat.format(ERR_11, personId));
        // transformers.add(new FilterApplyer(context.getFilter()));
        transformers.add(new IdEqCriteriaTransformer(context, Const.WORKER, personId));

        return transformers;
    }
}
