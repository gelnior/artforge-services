package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;


public class GetPersonProjectsCmd extends ListResultQuery<ILProject, IProject>
{
    public GetPersonProjectsCmd(ResourceContext<ILProject, IProject> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        final Long personId = context.getIdFromUrl(PERSON_ID_ATTRIBUTE);
        checkId(personId, MessageFormat.format(ERR_11, personId));

        final HibernatePersist<?, ?> persistor = (HibernatePersist<?, ?>) context.getPersistor();
        final CompositeQueryModifier filter = context.getFilter();

        // Query building
        // DetachedCriteria subQuery = persistor.basicSubCriteria(session);
        //
        // applyModifier(filter, subQuery, forCount);

        final Criteria query = persistor.basicCriteria(session);

        // Set the filter on the id
        query.createAlias("resourceGroups", "group").createAlias("group.resources", "rs").add(
                Restrictions.eq("rs.id", personId));// !!!GOOD

        // Other filters
        applyModifier(filter, query, forCount);

        if (!forCount)
            setMaxResult(query);

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
