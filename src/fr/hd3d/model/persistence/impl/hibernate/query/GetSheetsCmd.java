package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILSheet;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetSheetsCmd extends ListResultQuery<ILSheet, ISheet>
{
    public GetSheetsCmd(ResourceContext<ILSheet, ISheet> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        final Long projectId = context.getIdFromUrl(PROJECT_ID_ATTRIBUTE);

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        if (BaseH.isValidId(projectId))// case of the Sheets shortcut, when project id is not provided.
        {
            transformers.add(new IdEqCriteriaTransformer(context, Const.PROJECT, projectId));
        }
        return transformers;
    }
}
