package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.RESOURCEGROUP_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetGroupRolesCmd extends ListResultQuery<ILRole, IRole>
{
    public GetGroupRolesCmd(ResourceContext<ILRole, IRole> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long resourceGroupId = context.getIdFromUrl(RESOURCEGROUP_ID_ATTRIBUTE);
        checkId(resourceGroupId, MessageFormat.format(ERR_8, resourceGroupId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.RESOURCEGROUP, resourceGroupId));

        return transformers;
    }
}
