package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.ASSETABSTRACT_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILAssetRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IsNullCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.OrderCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetAssetAbstractRevisionsCmd extends ListResultQuery<ILAssetRevision, IAssetRevision>
{
    public GetAssetAbstractRevisionsCmd(ResourceContext<ILAssetRevision, IAssetRevision> context)
    {
        super(context);
    }

    protected Criteria buildCriteriaOrQuery(Session session) throws Hd3dException
    {
        return (Criteria) weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.assetrevision);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        String[] splitIdentifier = StringUtils.split(context.getStringFromUrl(ASSETABSTRACT_ID_ATTRIBUTE), '\\');

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new EqCriteriaTransformer(context, Const.KEY, splitIdentifier[0]));
        if (splitIdentifier.length > 1)
        {
            transformers.add(new EqCriteriaTransformer(context, Const.VARIATION, splitIdentifier[1]));
        }
        else
        {
            transformers.add(new IsNullCriteriaTransformer(context, Const.VARIATION));
        }
        transformers.add(new OrderCriteriaTransformer(context, Const.KEY, true));
        transformers.add(new OrderCriteriaTransformer(context, Const.VARIATION, true));
        transformers.add(new OrderCriteriaTransformer(context, Const.REVISION, true));

        return transformers;
    }

}
