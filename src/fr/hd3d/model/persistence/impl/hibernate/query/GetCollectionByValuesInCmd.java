package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.ExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.AddCriterionTransformer;
import fr.hd3d.services.resources.ResourceContext;


public class GetCollectionByValuesInCmd<E extends IBase> extends ListResultQuery<ILBase, E>
{
    private final String[] properties;
    private final Object[] values;
    private final String operator;

    public GetCollectionByValuesInCmd(final ResourceContext<?, ?> context, String[] properties, Object[] values,
            String operator) throws Hd3dException
    {
        super(context);
        if (!"and".equalsIgnoreCase(operator) && !"or".equalsIgnoreCase(operator))
        {
            throw new Hd3dException("Operator can only be AND or OR");
        }

        this.properties = properties;

        if (properties.length != values.length && properties.length < 2)
        {
            throw new Hd3dException("There must be at least 2 properties in passed arrays parameters");
        }

        this.values = values;
        this.operator = operator;
    }

    public GetCollectionByValuesInCmd(String[] properties, Object[] values, String operator) throws Hd3dException
    {
        this(null, properties, values, operator);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        for (int i = 0; i < properties.length; i++)
        {
            properties[i] = ExpressionQueryModifier.updateColumn(properties[i], createAliases(transformers,
                    properties[i]));
        }

        transformers.add(new AddCriterionTransformer(((HibernatePersist<?, ?>) getContext().getPersistor())
                .getCriterionIn(properties, values, operator)));

        return transformers;
    }
}
