package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.services.resources.ResourceContext;


public class GetCollectionCmd<E extends IBase> extends ListResultQuery<ILBase, E>
{
    public GetCollectionCmd()
    {
        super();
    }

    public GetCollectionCmd(final ResourceContext<?, ?> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        // TODO Auto-generated method stub
        return null;
    }
}
