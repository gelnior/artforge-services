package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.ASSETREVISION_ID_ATTRIBUTE;
import static fr.hd3d.utils.Const.OUTPUTASSETREVISION;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILAssetRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetAssetRevisionUpstream extends ListResultQuery<ILAssetRevision, IAssetRevision>
{
    public GetAssetRevisionUpstream(ResourceContext<ILAssetRevision, IAssetRevision> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        Long assetRevisionId = context.getIdFromUrl(ASSETREVISION_ID_ATTRIBUTE);
        checkId(assetRevisionId, MessageFormat.format(ERR_14, assetRevisionId));

        final IAssetRevision assetRevision = Persistors.assetrevision.getById(assetRevisionId);

        if (assetRevision == null)
            throw new Hd3dPersistenceException(MessageFormat.format(ERR_14, assetRevisionId));

        final CompositeQueryModifier filter = context.getFilter();

        /* List the asset revision links where the watched assetrevision is input */
        final Criteria query;
        {
            HibernatePersist<?, ?> assetRevisionLinkProvider = (HibernatePersist<?, ?>) Persistors.assetrevisionlink;

            // DetachedCriteria subQuery = assetRevisionLinkProvider.basicSubCriteria(session);
            //
            // applyModifier(filter, subQuery, forCount);

            query = assetRevisionLinkProvider.basicCriteria(session).add(
                    Restrictions.eq(OUTPUTASSETREVISION, assetRevision)).setProjection(
                    Projections.groupProperty(Const.INPUTASSETREVISION));

            applyModifier(filter, query, forCount);
        }

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
