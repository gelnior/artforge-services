package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IsNotNullCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetAllSequencesCmd extends ListResultQuery<ILSequence, ISequence>
{
    private Long projectId;

    public GetAllSequencesCmd(ResourceContext<ILSequence, ISequence> context)
    {
        super(context);
    }

    public GetAllSequencesCmd(ResourceContext<ILSequence, ISequence> context, Long projectId)
    {
        super(context);
        this.projectId = projectId;
    }

    public GetAllSequencesCmd(Long projectId)
    {
        super();
        this.projectId = projectId;
    }

    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long projectId = idValue(this.projectId, PROJECT_ID_ATTRIBUTE);
        checkId(projectId, MessageFormat.format(ERR_0, projectId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.PROJECT, projectId));
        transformers.add(new IsNotNullCriteriaTransformer(context, Const.PARENT));

        return transformers;
    }
}
