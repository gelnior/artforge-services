package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.BetweenCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetPersonDayFromToCollectionCmd extends ListResultQuery<ILPersonDay, IPersonDay>
{
    private Date start;
    private Date end;
    private Long personId;

    public GetPersonDayFromToCollectionCmd(ResourceContext<ILPersonDay, IPersonDay> context, Date start, Date end,
            Long personId)
    {
        super(context);
        this.start = start;
        this.end = end;
        this.personId = personId;
    }

    // @Override
    // protected Criteria buildCriteriaOrQuery(Session session) throws Hd3dPersistenceException
    // {
    // final CompositeQueryModifier filter = context.getFilter();
    //
    // final Criteria query = HibernateUtil.currentSession().createCriteria(PersonDayH.class);
    // query.add(Restrictions.between(Const.DATE, start, end));
    // query.add(Restrictions.eq(Const.PERSON + Const._ID, personId));
    //
    // applyModifier(filter, query);
    //
    // setMaxResult(query);
    //
    // return query;
    // }

    protected Object weaveCriteria(Session session) throws Hd3dException
    {
        return weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.personDay);
    }

    protected Object weaveCriteriaForCount(Session session) throws Hd3dException
    {
        return weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors.personDay);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new BetweenCriteriaTransformer(context, Const.DATE, start, end));
        transformers.add(new IdEqCriteriaTransformer(context, Const.PERSON, personId));

        return transformers;
    }

}
