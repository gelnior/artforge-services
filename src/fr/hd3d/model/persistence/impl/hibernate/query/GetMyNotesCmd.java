package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.resources.collectionquery.TaskBoundEntityApprovalNotesCollectionQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;


public class GetMyNotesCmd extends ListResultQuery<ILApprovalNote, IApprovalNote>
{

    protected Long userId;
    private List<Long> taskIds;

    public GetMyNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, Long userId, List<Long> taskIds)
    {
        super(context);

        this.userId = userId;
        this.taskIds = taskIds;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        List<Long> notesId = getApprovalNoteIds();

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        if (CollectUtils.isNotEmpty(notesId))
        {
            transformers.add(new IdEqCriteriaTransformer(context, Const.ID, notesId));
        }
        return transformers;
    }

    // private List<Long> getApprovalNoteIds() throws Hd3dException
    // {
    // List<ITask> tasks = Persistors.task.getByIds(this.taskIds);
    // List<Long> notesId = new ArrayList<Long>();
    // Map<Long, List<Long>> noteTypesId = new HashMap<Long, List<Long>>();
    //
    // // Build map that associate task type ids to work object ids.
    // Map<Long, Map<String, List<Long>>> taskTypeMap = new HashMap<Long, Map<String, List<Long>>>();
    // for (ITask task : tasks)
    // {
    // ITaskType taskType = task.getTaskType();
    //
    // if (taskType == null)
    // continue;
    //
    // if (task.getWorker() != null && task.getWorker().getId().longValue() == this.userId.longValue())
    // {
    // // For each task type, build a map of work objects for each available entities (constituent, shot...).
    // Map<String, List<Long>> entityMap = taskTypeMap.get(taskType.getId());
    // if (entityMap == null)
    // {
    // entityMap = new HashMap<String, List<Long>>();
    // taskTypeMap.put(taskType.getId(), entityMap);
    // }
    //
    // List<Long> entityTasks = entityMap.get(task.getBoundEntityName());
    // if (entityTasks == null)
    // {
    // entityTasks = new ArrayList<Long>();
    // entityMap.put(task.getBoundEntityName(), entityTasks);
    // }
    // entityTasks.add(task.getBoundEntityId());
    // }
    //
    // // Link current task type to every note type existing for current task type.
    // List<IApprovalNoteType> noteTypes = Persistors.approvalnotetype.getByValue("taskType.id", taskType.getId());
    // List<Long> noteTypeIds = new ArrayList<Long>();
    // for (IApprovalNoteType noteType : noteTypes)
    // noteTypeIds.add(noteType.getId());
    //
    // noteTypesId.put(taskType.getId(), noteTypeIds);
    // }
    //
    // String[] fields = { "approvalNoteType.id", "boundEntityName", "boundEntity" };
    // // For each task type add corresponding approvals to result list.
    // for (Entry<Long, Map<String, List<Long>>> entry : taskTypeMap.entrySet())
    // {
    // Long taskTypeId = entry.getKey();
    // Map<String, List<Long>> entityMap = entry.getValue();
    //
    // // For each entity corresponding approvals to result list.
    // for (Entry<String, List<Long>> entityEntry : entityMap.entrySet())
    // {
    // List<String> entityNameList = Arrays.asList(entityEntry.getKey());
    //
    // List<Long> noteTypesIdArray = noteTypesId.get(taskTypeId);
    // if (CollectionUtils.isNotEmpty(noteTypesIdArray) && CollectionUtils.isNotEmpty(entityNameList)
    // && CollectionUtils.isNotEmpty(entityEntry.getValue()))
    // {
    // Object[] values = { noteTypesIdArray.toArray(), entityNameList.toArray(),
    // entityEntry.getValue().toArray() };
    //
    // List<IApprovalNote> notes = Persistors.approvalnote
    // .getByValuesIn_NoPermCheck(fields, values, "and");
    // if (CollectionUtils.isNotEmpty(notes))
    // for (IApprovalNote note : notes)
    // notesId.add(note.getId());
    // }
    // }
    // }
    //
    // return notesId;
    // }

    private List<Long> getApprovalNoteIds() throws Hd3dException
    {
        if (CollectUtils.isNotEmpty(this.taskIds))
        {
            String[] fields = { "id", "worker.id" };
            Object[] values = { this.taskIds.toArray(), new Object[] { this.userId } };

            List<ITask> tasks = Persistors.task.getByValuesIn(fields, values, "AND");
            List<Long> notesId = new ArrayList<Long>();
            TaskBoundEntityApprovalNotesCollectionQuery cq = new TaskBoundEntityApprovalNotesCollectionQuery();
            cq.setEntities(tasks);
            List<List<IApprovalNote>> tasksApprovalNotes = cq.doQuery();
            for (List<IApprovalNote> notes : tasksApprovalNotes)
            {
                notesId.addAll(CollectUtils.getIds(notes));
            }
            return notesId;
        }
        else
        {
            return null;
        }
    }
}
