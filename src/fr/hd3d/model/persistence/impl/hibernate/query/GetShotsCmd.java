package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.SEQUENCE_ID_ATTRIBUTE;

import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ServerResourceUtils;


public class GetShotsCmd extends ListResultQuery<ILShot, IShot>
{
    private Long projectId;
    private Long sequenceId;

    public GetShotsCmd(ResourceContext<ILShot, IShot> context)
    {
        super(context);
    }

    public GetShotsCmd(ResourceContext<ILShot, IShot> context, Long projectId, Long sequenceId)
    {
        super(context);
        this.projectId = projectId;
        this.sequenceId = sequenceId;
    }

    public GetShotsCmd(Long projectId, Long sequenceId)
    {
        super();
        this.projectId = projectId;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        final Long projectId = idValue(this.projectId, PROJECT_ID_ATTRIBUTE);
        final Long sequenceId = idValue(this.sequenceId, SEQUENCE_ID_ATTRIBUTE);

        final HibernatePersist<?, ?> persistor = (HibernatePersist<?, ?>) context.getPersistor();
        final CompositeQueryModifier filter = context.getFilter();

        // DetachedCriteria subQuery = persistor.basicSubCriteria(session);
        // applyModifier(filter, subQuery, forCount);

        final Criteria query = persistor.basicCriteria(session);

        if (BaseH.isValidId(sequenceId))
        {
            ServerResourceUtils.addIdEqRestriction(context, query, Const.SEQUENCE, sequenceId);
        }
        else if (BaseH.isValidId(projectId))
        {
            query.createAlias(Const.SEQUENCE, "seq").createAlias("seq.project", "proj").add(
                    Restrictions.eq("proj.id", projectId));
        }

        applyModifier(filter, query, forCount);

        if (!forCount)
            setMaxResult(query);

        return query;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }
}
