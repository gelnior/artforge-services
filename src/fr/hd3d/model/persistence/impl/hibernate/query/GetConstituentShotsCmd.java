package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.CONSTITUENT_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


/**
 * GetConstituentShotsCmd returns shots linked to given constituent via compositions.
 * 
 * @author HD3D
 */
public class GetConstituentShotsCmd extends ListResultQuery<ILShot, IShot>
{
    public GetConstituentShotsCmd(ResourceContext<ILShot, IShot> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long constituentId = context.getIdFromUrl(CONSTITUENT_ID_ATTRIBUTE);
        checkId(constituentId, MessageFormat.format(ERR_1, constituentId));

        final List<IComposition> compositions = Persistors.composition.getByValue(Const.CONSTITUENT + Const._ID,
                constituentId);
        List<Long> shotIds = new ArrayList<Long>();
        for (IComposition composition : compositions)
            shotIds.add(composition.getShot().getId());

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(Const.ID, shotIds));

        return transformers;
    }
}
