package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.APPROVALNOTE_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetProjectApprovalNoteTasksCmd extends ListResultQuery<ILTask, ITask>
{
    public GetProjectApprovalNoteTasksCmd(ResourceContext<ILTask, ITask> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long noteId = context.getIdFromUrl(APPROVALNOTE_ID_ATTRIBUTE);
        checkId(noteId, MessageFormat.format(ERR_0, noteId));

        IApprovalNote note = Persistors.approvalnote.getById(noteId);
        IApprovalNoteType noteType = note.getApprovalNoteType();

        String[] fields = { "boundEntity", "boundEntityName" };
        Object[] values = { note.getBoundEntity(), note.getBoundEntityName() };

        List<IEntityTaskLink> links = Persistors.entitytasklink.getByValues(fields, values, "and");
        List<Long> taskIds = new ArrayList<Long>();
        for (IEntityTaskLink link : links)
        {
            if (link.getTask() != null)
                taskIds.add(link.getTask().getId());
        }

        List<ITask> tasks = Persistors.task.getByIds(taskIds);
        taskIds.clear();

        if (noteType != null && noteType.getTaskType() != null)
        {
            Long taskTypeId = noteType.getTaskType().getId();
            for (ITask task : tasks)
            {
                if (task != null && task.getTaskType() != null && task.getTaskType().getId().equals(taskTypeId))
                {
                    taskIds.add(task.getId());
                }
            }
        }

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.ID, taskIds));

        return transformers;
    }
}
