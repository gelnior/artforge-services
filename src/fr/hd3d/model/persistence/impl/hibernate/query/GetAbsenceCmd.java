package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.utils.Const.CANONICALNAME_ABSENCE;
import static fr.hd3d.utils.Const.ENDDATE;
import static fr.hd3d.utils.Const.ENDDATEVALUE;
import static fr.hd3d.utils.Const.ID;
import static fr.hd3d.utils.Const.PERSON;
import static fr.hd3d.utils.Const.STARTDATE;
import static fr.hd3d.utils.Const.STARTDATEVALUE;
import static fr.hd3d.utils.Const.WORKER;
import static fr.hd3d.utils.Const.WORKERIDVALUE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.sojo.interchange.json.JsonParser;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILAbsence;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;
import fr.hd3d.utils.HibernateUtil;


public class GetAbsenceCmd extends ListResultQuery<ILAbsence, IAbsence>
{
    private static final String getAbsenceQueryString = "from " + CANONICALNAME_ABSENCE + " a where "
            + "a.id not in (select abs.id from " + CANONICALNAME_ABSENCE + " abs where (abs." + STARTDATE
            + " is not null and abs." + STARTDATE + "> :" + ENDDATEVALUE + ")" + " or (abs." + ENDDATE
            + " is not null and abs." + ENDDATE + " < :" + STARTDATEVALUE + "))" + " and a." + STARTDATE
            + " is not null" + " and a." + ENDDATE + " is not null" + " and a." + WORKER + "." + ID + " = :"
            + WORKERIDVALUE;

    public GetAbsenceCmd(ResourceContext<ILAbsence, IAbsence> context)
    {
        super(context);
    }

    @Override
    public void execute(Session session) throws Hd3dException
    {
        super.execute(session);
        result = ObjectProviderCustoms.filter(result);

        setTotalSize(query, session);
    }

    protected Query buildCriteriaOrQuery(Session session) throws Hd3dPersistenceException
    {
        Map<String, String> map = context.getQueryMap();
        String customValues = map.get(fr.hd3d.common.client.Const.CUSTOM);
        customValues = customValues.substring(1, customValues.length() - 1);
        JsonParser jsonParser = new JsonParser();
        Map<String, Object> object = (Map<String, Object>) jsonParser.parse(customValues);
        String startDateValue = (String) object.get(STARTDATE);
        String endDateValue = (String) object.get(ENDDATE);
        Long person = (Long) object.get(PERSON);
        if (startDateValue != null && endDateValue != null && person != null)
        {
            // String queryString = "from " + AbsenceH.class.getCanonicalName() + " a where "
            // + "a.id not in (select abs.id from " + AbsenceH.class.getCanonicalName()
            // + " abs where (abs.startDate is not null and abs.startDate >= :endDateValue)"
            // + " or (abs.endDate is not null and abs.endDate <= :startDateValue))"
            // + " and a.startDate is not null" + " and a.endDate is not null"
            // + " and a.worker.id <> :userId";
            String queryString = getAbsenceQueryString;
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date startDate = null;
            Date endDate = null;
            try
            {
                startDate = dateFormat.parse(startDateValue);
                endDate = dateFormat.parse(endDateValue);
            }
            catch (ParseException e)
            {
                throw new Hd3dPersistenceException(e);
            }
            Query query = HibernateUtil.currentSession().createQuery(queryString);
            query.setTimestamp(STARTDATEVALUE, startDate);
            query.setTimestamp(ENDDATEVALUE, endDate);
            query.setLong(WORKERIDVALUE, person);
            query.setCacheable(false);

            setMaxResult(query);
            return query;
        }
        else
        {
            throw new Hd3dPersistenceException("Your request must match like this : {\"" + STARTDATE + "\":\""
                    + STARTDATEVALUE + "\",\"" + ENDDATE + "\":\"" + ENDDATEVALUE + "\",\"" + PERSON + "\":personID}");
        }
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
