package fr.hd3d.model.persistence.impl.hibernate.query;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.services.resources.ResourceContext;


public class LoadObject<E extends IBase> extends AbstractHibernateQuery<E>
{
    public E result;
    private final Long id;

    public LoadObject(final ResourceContext<?, ?> context, final Long id)
    {
        super(context);
        this.id = id;
    }

    public LoadObject(final ResourceContext<?, ?> context, final Long id, LockMode lockMode)
    {
        super(context, lockMode);
        this.id = id;
    }

    public LoadObject(final Long id)
    {
        this.id = id;
    }

    public LoadObject(final Long id, LockMode lockMode)
    {
        super(lockMode);
        this.id = id;
    }

    @SuppressWarnings("unchecked")
    public void execute(final org.hibernate.Session session) throws Hd3dException
    {
        /*
         * Note: it is not possible to use a Criteria or Query here because it may cause multiple loads of objects
         * already present in session (shared reference exception...).
         */
        /*
         * A obj = (A) session.load(getPersistedClass(), id); if (obj != null) { if
         * (IBaseObject.INTERNALSTATUS_ENABLED.equals(((IBaseObject) obj).getInternalStatus())) { result = obj; } }
         */
        // workaround: because filter does not apply on session.get(object) or session.load(object)
        // FilterImpl internalStatusFilter = (FilterImpl) session.getEnabledFilter("internalStatus");
        // if (IBaseObject.INTERNALSTATUS_ENABLED
        // .equals((Integer) internalStatusFilter.getParameter("internalStatus")))
        // result = obj;
        // Note: session.get does not apply filters, so a query is used instead.
        // But this may cause a "Found shared references to a collection" exception when the object
        // is still in session
        try
        {
            // final Criteria query = basicIdCriteria(session, id);
            // setLockMode(query);
            final Criteria query = getCriteria(session);

            if (getLockMode() != LockMode.NONE && getLockMode() != null)
            {
                query.setLockMode(getLockMode());
            }
            result = (E) query.uniqueResult();
        }
        catch (HibernateException e)
        {
            E obj = (E) session.load(getContext().getPersistedClass(), id);
            if (obj != null)
            {
                if (fr.hd3d.common.client.Const.INTERNALSTATUS_ENABLED.equals(((IBase) obj).getInternalStatus()))
                {
                    result = obj;
                }
            }
        }
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        final Criteria query = ((HibernatePersist<?, ?>) getContext().getPersistor()).basicIdCriteria(session, id);
        return query;
    }
}
