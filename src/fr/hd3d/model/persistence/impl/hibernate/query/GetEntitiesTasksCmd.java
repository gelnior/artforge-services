package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.AliasCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;


public class GetEntitiesTasksCmd extends ListResultQuery<ILTask, ITask>
{
    String entityName;
    Collection<Long> ids;

    public GetEntitiesTasksCmd()
    {
        super();
    }

    public GetEntitiesTasksCmd(ResourceContext<ILTask, ITask> context)
    {
        super(context);
    }

    public GetEntitiesTasksCmd(ResourceContext<ILTask, ITask> context, String entityName, Collection<Long> ids)
    {
        super(context);
        this.entityName = entityName;
        this.ids = ids;
    }

    public GetEntitiesTasksCmd(String entityName, Collection<Long> ids)
    {
        super();
        this.entityName = entityName;
        this.ids = ids;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new AliasCriteriaTransformer(context, "boundEntityTaskLinks", "etl"));
        transformers.add(new IdEqCriteriaTransformer(context, "etl.boundEntity", ids));
        transformers.add(new EqCriteriaTransformer(context, "etl.boundEntityName", entityName));

        return transformers;
    }

}
