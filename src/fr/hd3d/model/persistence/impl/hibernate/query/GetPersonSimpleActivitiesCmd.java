package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetPersonSimpleActivitiesCmd extends ListResultQuery<ILSimpleActivity, ISimpleActivity>
{
    private Long personId;

    public GetPersonSimpleActivitiesCmd(ResourceContext<ILSimpleActivity, ISimpleActivity> context)
    {
        super(context);
    }

    public GetPersonSimpleActivitiesCmd(ResourceContext<ILSimpleActivity, ISimpleActivity> context, Long personId)
    {
        super(context);
        this.personId = personId;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        if (forCount)
            return (Criteria) weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors.simpleActivity);
        else

            return (Criteria) weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.simpleActivity);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        final Long personId = idValue(this.personId, PERSON_ID_ATTRIBUTE);

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        // transformers.add(new FilterApplyer(context.getFilter()));
        transformers.add(new EqCriteriaTransformer(context, Const.WORKER + Const._ID, personId));

        return transformers;
    }
}
