package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;


public class GetWorkObjectCompositionTasksCmd extends ListResultQuery<ILTask, ITask>
{
    private String compositionBoundEntityName;
    private String taskBoundEntityName;
    private String idKey;

    public GetWorkObjectCompositionTasksCmd(ResourceContext<ILTask, ITask> context, String idKey,
            String compositionBoundEntityName, String taskBoundEntityName)
    {
        super(context);
        this.compositionBoundEntityName = compositionBoundEntityName;
        this.taskBoundEntityName = taskBoundEntityName;
        this.idKey = idKey;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        final Long workObjectId = context.getIdFromUrl(idKey);

        final List<IComposition> compositions = Persistors.composition.getByValue(compositionBoundEntityName
                + Const._ID, workObjectId);

        /* if no compositions, do not go further */
        if (CollectUtils.isNotEmpty(compositions))
        {
            List<Long> compositionIds = new ArrayList<Long>();
            for (IComposition composition : compositions)
            {
                if (Const.CONSTITUENT.equals(taskBoundEntityName))
                    compositionIds.add(composition.getConstituent().getId());
                else if (Const.SHOT.equals(taskBoundEntityName))
                    compositionIds.add(composition.getShot().getId());
            }

            if (CollectUtils.isNotEmpty(compositionIds))
            {
                Object[] entities = new Object[] { taskBoundEntityName };
                String[] properties = new String[] { Const.BOUNDENTITYNAME, Const.BOUNDENTITY };
                Object[] values = new Object[] { entities, compositionIds.toArray() };
                final List<IEntityTaskLink> links = Persistors.entitytasklink.getByValuesIn(properties, values, "AND");

                /* if no tasks, do not go further */
                if (CollectUtils.isNotEmpty(links))
                {
                    List<Long> taskIds = new ArrayList<Long>();
                    for (IEntityTaskLink link : links)
                        taskIds.add(link.getTask().getId());

                    transformers.add(new EqCriteriaTransformer(Const.ID, taskIds));
                }
                else
                {
                    setForceEmptyResult(true);
                }
            }
            else
            {
                setForceEmptyResult(true);
            }
        }
        else
        {
            setForceEmptyResult(true);
        }

        return transformers;
    }
}
