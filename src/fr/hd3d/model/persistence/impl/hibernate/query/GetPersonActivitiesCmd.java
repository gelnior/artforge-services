package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class GetPersonActivitiesCmd extends ListResultQuery<ILActivity, IActivity>
{
    public GetPersonActivitiesCmd(ResourceContext<ILActivity, IActivity> context)
    {
        super(context);
    }

    @SuppressWarnings("unchecked")
    public void execute(Session session) throws Hd3dException
    {
        ListResultQuery<ILTaskActivity, ITaskActivity> taskActivities = new GetPersonTaskActivitiesCmd(context)
                .wantTotalSize();
        HibernateUtil.safeQuery(taskActivities);

        ListResultQuery<ILSimpleActivity, ISimpleActivity> simpleActivities = new GetPersonSimpleActivitiesCmd(
                (ResourceContext<ILSimpleActivity, ISimpleActivity>) context).wantTotalSize();
        HibernateUtil.safeQuery(simpleActivities);

        List<IActivity> unfilteredActivities = new ArrayList<IActivity>();
        if (CollectUtils.isNotEmpty(taskActivities.result))
            unfilteredActivities.addAll(taskActivities.result);
        if (CollectUtils.isNotEmpty(simpleActivities.result))
            unfilteredActivities.addAll(simpleActivities.result);

        result = ObjectProviderCustoms.filter(unfilteredActivities);

        totalCount = taskActivities.totalCount + simpleActivities.totalCount;

        // final HibernatePersist persistor = (HibernatePersist) context.getPersistor();
        //
        // // Query building for task activities
        // DetachedCriteria subQuery = persistor.basicSubCriteria(session, Persistors.taskActivity.getPersistedClass());
        //
        // applyModifier(context.getFilter(), subQuery);
        //
        // final Criteria taskActivityQuery = persistor.basicCriteria(session, subQuery);
        // // Set activity type to task
        // final Criteria taskCriteria = taskActivityQuery.createCriteria(TASK);
        // // Set the filter on the id
        // ServerResourceUtils.addIdEqRestriction((CompositeQueryModifier) context.getFilter(), taskCriteria, WORKER,
        // personId);
        //
        // // Set other filters
        // applyModifier(context.getFilter(), taskActivityQuery);
        //
        // if (context.isPaginated())
        // {
        // taskCriteria.setFirstResult(0);
        // taskCriteria.setMaxResults(Integer.MAX_VALUE);
        // taskCriteria.setProjection(Projections.rowCount());
        // List objects = taskCriteria.list();
        // if (!objects.isEmpty())
        // totalCount = ((Long) objects.get(0)).intValue();
        // }
        //
        // // Temporary result
        // final ArrayList<IActivity> _result = new ArrayList<IActivity>(taskActivityQuery.list());
        //
        // // New Query for simple activities
        // DetachedCriteria subQuery_1 = persistor
        // .basicSubCriteria(session, Persistors.simpleActivity.getPersistedClass());
        //
        // applyModifier(context.getFilter(), subQuery);
        //
        // final Criteria simpleActivityQuery = persistor.basicCriteria(session, subQuery_1);
        //
        // // Set the filter on the id
        // ServerResourceUtils.addIdEqRestriction((CompositeQueryModifier) context.getFilter(), simpleActivityQuery,
        // WORKER, personId);
        //
        // // Set other filters
        // applyModifier(context.getFilter(), simpleActivityQuery);
        //
        // // merge simple activities result with task activities result
        // _result.addAll(simpleActivityQuery.list());
        //
        // _result.trimToSize();
        // result = _result;
        // result = ObjectProviderCustoms.filter(result);
        //
        // if (context.isPaginated())
        // {
        // simpleActivityQuery.setFirstResult(0);
        // simpleActivityQuery.setMaxResults(Integer.MAX_VALUE);
        // simpleActivityQuery.setProjection(Projections.rowCount());
        // List objects = taskCriteria.list();
        // if (!objects.isEmpty())
        // totalCount += ((Long) objects.get(0)).intValue();
        // }
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
