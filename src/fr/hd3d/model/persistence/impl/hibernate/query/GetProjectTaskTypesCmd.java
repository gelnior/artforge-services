package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTaskType;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


/**
 * Return list of task types linked to project list.
 * 
 * @author HD3D
 */
public class GetProjectTaskTypesCmd extends ListResultQuery<ILTaskType, ITaskType>
{
    public GetProjectTaskTypesCmd(ResourceContext<ILTaskType, ITaskType> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long projectId = context.getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        checkId(projectId, MessageFormat.format(ERR_0, projectId));
        IProject project = Persistors.project.getById(projectId);

        List<Long> ids = new ArrayList<Long>();
        for (ITaskType taskType : project.getTaskTypes())
            ids.add(taskType.getId());

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.ID, ids));

        return transformers;
    }

}
