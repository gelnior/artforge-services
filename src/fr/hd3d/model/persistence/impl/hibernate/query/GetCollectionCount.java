package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;


public class GetCollectionCount<E extends IBase> extends AbstractHibernateQuery<E>
{
    /** Query result */
    private int result = 0;

    public GetCollectionCount()
    {
        super();
    }

    public GetCollectionCount(final ResourceContext<?, E> context)
    {
        super(context);
    }

    public void execute(final org.hibernate.Session session) throws Hd3dException
    {
        /* Set result with count result. */
        List<?> objects = getCriteria(session).list();
        if (!objects.isEmpty())
        {
            result = ((Integer) objects.get(0)).intValue();
        }
    }

    public int getResult()
    {
        return result;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        // DetachedCriteria subQuery = ((HibernatePersist<?, ?>) getContext().getPersistor()).basicSubCriteria(session);

        final CompositeQueryModifier filter = context.getFilter();

        // if (filter != null)
        // {
        // filter.applyModifier(subQuery);
        // }

        final Criteria criteria = ((HibernatePersist<?, ?>) getContext().getPersistor()).basicCriteria(session);
        /* Set the query as a count query. */
        criteria.setProjection(Projections.rowCount());

        /* Apply filter. */
        if (filter != null)
        {
            filter.applyModifier(criteria);
        }

        return criteria;
    }
}
