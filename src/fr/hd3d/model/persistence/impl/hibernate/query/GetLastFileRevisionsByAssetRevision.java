package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.ASSETREVISION_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;


public class GetLastFileRevisionsByAssetRevision extends ListResultQuery<ILFileRevision, IFileRevision>
{
    public GetLastFileRevisionsByAssetRevision(ResourceContext<ILFileRevision, IFileRevision> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        if (forCount)
            return (Criteria) weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors.assetrevision);
        else
            return (Criteria) weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.assetrevision);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        final Long assetRevisionId = context.getIdFromUrl(ASSETREVISION_ID_ATTRIBUTE);

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, "id", assetRevisionId));

        return transformers;
    }

}
