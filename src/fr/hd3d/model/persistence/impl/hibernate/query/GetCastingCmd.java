package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.SHOT_ID_ATTRIBUTE;
import static fr.hd3d.utils.Const.CONSTITUENT;
import static fr.hd3d.utils.Const.SHOT;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;
import fr.hd3d.utils.ServerResourceUtils;


public class GetCastingCmd extends ListResultQuery<ILConstituent, IConstituent>
{
    public GetCastingCmd(ResourceContext<ILConstituent, IConstituent> context)
    {
        super(context);
    }

    public void execute(Session session) throws Hd3dException
    {
        final Criteria compositionQuery = getCriteria(session);
        List compos = compositionQuery.list();
        ArrayList<IConstituent> constituents = new ArrayList<IConstituent>(compos.size());
        Iterator iter = compos.iterator();
        while (iter.hasNext())
        {
            Map map = (Map) iter.next();

            IComposition compo = (IComposition) map.get(Criteria.ROOT_ALIAS);
            constituents.add((IConstituent) compo.getConstituent());
        }
        constituents.trimToSize();

        result = constituents;
        result = ObjectProviderCustoms.filter(result);

        setTotalSize(compositionQuery, session);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        final Long shotId = context.getIdFromUrl(SHOT_ID_ATTRIBUTE);
        checkId(shotId, MessageFormat.format(ERR_2, shotId));

        final IShot shot = Persistors.shot.getById(shotId);

        if (shot == null)
            throw new Hd3dPersistenceException(MessageFormat.format(ERR_2, shotId));

        final Class<?> clazz = Persistors.composition.getPersistedClass();

        final CompositeQueryModifier filter = context.getFilter();

        // DetachedCriteria subQuery = HibernatePersist.basicSubCriteria(session, clazz);
        //
        // applyModifier(filter, subQuery, forCount);

        final Criteria compositionQuery = HibernatePersist.basicCriteria(session, clazz);
        ServerResourceUtils.addIdEqRestriction(context, compositionQuery, SHOT, shot.getId());
        compositionQuery.createCriteria(CONSTITUENT).setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);

        applyModifier(filter, compositionQuery, forCount);

        if (!forCount)
            setMaxResult(compositionQuery);

        return compositionQuery;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
