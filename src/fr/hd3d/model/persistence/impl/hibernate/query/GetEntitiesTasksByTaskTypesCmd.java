package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;


public class GetEntitiesTasksByTaskTypesCmd extends GetEntitiesTasksCmd
{
    List<Long> taskTypesIds;

    public GetEntitiesTasksByTaskTypesCmd(ResourceContext<ILTask, ITask> context)
    {
        super(context);
    }

    public GetEntitiesTasksByTaskTypesCmd(ResourceContext<ILTask, ITask> context, String entityName,
            Collection<Long> ids, List<Long> taskTypes)
    {
        super(context, entityName, ids);
        this.taskTypesIds = taskTypes;
    }

    public GetEntitiesTasksByTaskTypesCmd(String entityName, Collection<Long> ids, List<Long> taskTypes)
    {
        super(entityName, ids);
        this.taskTypesIds = taskTypes;
    }

    protected Object weaveCriteria(Session session) throws Hd3dException
    {
        return weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.task);
    }

    protected Object weaveCriteriaForCount(Session session) throws Hd3dException
    {
        return weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors.task);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        List<Transformer<Criteria, Criteria>> transformers = super.getCoreTransformers(session);

        transformers.add(new IdEqCriteriaTransformer(context, "taskType.id", taskTypesIds));

        return transformers;
    }

}
