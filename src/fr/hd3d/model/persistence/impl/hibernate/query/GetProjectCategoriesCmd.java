package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetProjectCategoriesCmd extends ListResultQuery<ILCategory, ICategory>
{
    private Long projectId;

    public GetProjectCategoriesCmd() throws Hd3dException
    {
        super();
        context.setPersistor(Persistors.category);
    }

    public GetProjectCategoriesCmd(ResourceContext<ILCategory, ICategory> context)
    {
        super(context);
        projectId = idValue(this.projectId, PROJECT_ID_ATTRIBUTE);
    }

    public GetProjectCategoriesCmd(ResourceContext<ILCategory, ICategory> context, Long projectId)
    {
        super(context);
        this.projectId = projectId;
    }

    public GetProjectCategoriesCmd(Long projectId)
    {
        super();
        this.projectId = projectId;
    }

    public Long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        checkId(projectId, MessageFormat.format(ERR_0, projectId));

        IProject project = Persistors.project.getById(projectId);
        if (project == null)
        {
            throw new Hd3dPersistenceException(MessageFormat.format(ERR_0, projectId));
        }

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.PROJECT, projectId));
        transformers.add(new IdEqCriteriaTransformer(context, Const.PARENT, project.rootCategory().getId()));

        return transformers;

    }

}
