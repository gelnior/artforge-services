package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.utils.Const.CANONICALNAME_TASK;
import static fr.hd3d.utils.Const.ENDDATE;
import static fr.hd3d.utils.Const.ENDDATEVALUE;
import static fr.hd3d.utils.Const.ID;
import static fr.hd3d.utils.Const.IDVALUE;
import static fr.hd3d.utils.Const.PERSON;
import static fr.hd3d.utils.Const.PROJECT;
import static fr.hd3d.utils.Const.PROJECTID;
import static fr.hd3d.utils.Const.STARTDATE;
import static fr.hd3d.utils.Const.STARTDATEVALUE;
import static fr.hd3d.utils.Const.WORKERIDVALUE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.sojo.interchange.json.JsonParser;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;
import fr.hd3d.utils.HibernateUtil;


public class GetTasksCmd extends ListResultQuery<ILTask, ITask>
{
    private static final String getTasksQueryString = "from " + CANONICALNAME_TASK + " t where "
            + "t.id not in (select task.id from " + CANONICALNAME_TASK
            + " task where (task.startDate is not null and task.startDate >= :endDateValue)"
            + " or (task.endDate is not null and task.endDate <= :startDateValue))" + " and t.id <> :idValue"
            + " and t.startDate is not null" + " and t.endDate is not null" + " and t.project.id <> :projectId";

    public GetTasksCmd(ResourceContext<ILTask, ITask> context)
    {
        super(context);
    }

    public void execute(Session session) throws Hd3dException
    {
        super.execute(session);
        result = ObjectProviderCustoms.filter(result);

        setTotalSize(query, session);
    }

    @Override
    protected Query buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        Map<String, String> map = context.getQueryMap();
        String customValues = map.get(fr.hd3d.common.client.Const.CUSTOM);
        customValues = customValues.substring(1, customValues.length() - 1);
        JsonParser jsonParser = new JsonParser();
        Map<String, Object> object = (Map<String, Object>) jsonParser.parse(customValues);
        Long id = (Long) object.get(ID);
        String startDateValue = (String) object.get(STARTDATE);
        String endDateValue = (String) object.get(ENDDATE);
        Long person = (Long) object.get(PERSON);
        Long project = (Long) object.get(PROJECT);
        if (id != null && startDateValue != null && endDateValue != null && project != null)
        {
            // String queryString = "from " + TaskHibernateImpl.class.getCanonicalName() + " t where "
            // + "t.id not in (select task.id from " + TaskHibernateImpl.class.getCanonicalName()
            // + " task where (task.startDate is not null and task.startDate >= :endDateValue)"
            // + " or (task.endDate is not null and task.endDate <= :startDateValue))"
            // + " and t.id <> :idValue" + " and t.startDate is not null" + " and t.endDate is not null"
            // + " and t.project.id <> :projectId";
            String queryString = getTasksQueryString;
            if (person != null)
            {
                queryString += " and t.worker is not null and  t.worker.id = :workerIdValue";
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date startDate = null;
            Date endDate = null;
            try
            {
                startDate = dateFormat.parse(startDateValue);
                endDate = dateFormat.parse(endDateValue);
            }
            catch (ParseException e)
            {
                throw new Hd3dPersistenceException(e);
            }
            Query query = HibernateUtil.currentSession().createQuery(queryString);
            query.setTimestamp(STARTDATEVALUE, startDate);
            query.setTimestamp(ENDDATEVALUE, endDate);
            query.setLong(IDVALUE, id);
            query.setLong(PROJECTID, project);
            if (person != null)
            {
                query.setLong(WORKERIDVALUE, person);
            }
            query.setCacheable(false);

            if (!forCount)
                setMaxResult(query);

            return query;
        }
        else
        {
            throw new Hd3dPersistenceException(
                    "Your request must match like this : {\"startDate\":\"startDateValue\",\"endDate\":\"endDateValue\",\"id\":ID,\"person\":personID, \"project\":projectID}]}");
        }
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
