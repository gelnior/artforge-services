package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.ExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;


public class GetCollectionByValueCmd<E extends IBase> extends ListResultQuery<ILBase, E>
{
    private final String property;
    private final Object value;

    public GetCollectionByValueCmd(final ResourceContext<?, ?> context, String property, Object value)
    {
        super(context);
        this.property = property;
        this.value = value;
    }

    public GetCollectionByValueCmd(String property, Object value)
    {
        this(null, property, value);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        final String col = ExpressionQueryModifier.updateColumn(property, createAliases(transformers, property));
        transformers.add(new EqCriteriaTransformer(col, value));

        return transformers;
    }
}
