package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IsNullCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.OrderCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;


public class GetApprovalNotesCmd extends ListResultQuery<ILApprovalNote, IApprovalNote>
{
    /* single object query */
    IBase object = null;
    /* multiple objects query */
    List<Long> ids = null;
    String entityName = null;
    Long approvalNoteTypeId = null;
    List<Long> approvalNoteTypeIds = null;

    public GetApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context)
    {
        super(context);
    }

    /* Constructor for single object query */
    public GetApprovalNotesCmd(IBase object) throws Hd3dPersistenceException
    {
        if (object == null)
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: object cannot be null");
        }
        this.object = object;
    }

    public GetApprovalNotesCmd(IBase object, Long approvalNoteTypeId) throws Hd3dPersistenceException
    {
        if (object == null)
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: object cannot be null");
        }
        this.object = object;
        this.approvalNoteTypeId = approvalNoteTypeId;
    }

    public GetApprovalNotesCmd(IBase object, List<Long> approvalNoteTypeIds) throws Hd3dPersistenceException
    {
        if (object == null)
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: object cannot be null");
        }
        this.object = object;
        this.approvalNoteTypeIds = approvalNoteTypeIds;
    }

    /* Constructor for multiple objects query */
    public GetApprovalNotesCmd(List<Long> ids, String entityName) throws Hd3dPersistenceException
    {
        if (CollectUtils.isEmpty(ids) || StringUtils.isBlank(entityName))
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: ids or entityName cannot be null");
        }
        this.ids = ids;
        this.entityName = entityName;
    }

    public GetApprovalNotesCmd(List<Long> ids, String entityName, Long approvalNoteTypeId)
            throws Hd3dPersistenceException
    {
        if (CollectUtils.isEmpty(ids) || StringUtils.isBlank(entityName))
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: ids or entityName cannot be null");
        }
        this.ids = ids;
        this.entityName = entityName;
        this.approvalNoteTypeId = approvalNoteTypeId;
    }

    public GetApprovalNotesCmd(List<Long> ids, String entityName, List<Long> approvalNoteTypeIds)
            throws Hd3dPersistenceException
    {
        if (CollectUtils.isEmpty(ids) || StringUtils.isBlank(entityName))
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: ids or entityName cannot be null");
        }
        this.ids = ids;
        this.entityName = entityName;
        this.approvalNoteTypeIds = approvalNoteTypeIds;
    }

    /* Constructor for single object query */
    public GetApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, IBase object)
            throws Hd3dPersistenceException
    {
        super(context);
        if (object == null)
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: object cannot be null");
        }
        this.object = object;
    }

    public GetApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, IBase object,
            Long approvalNoteTypeId) throws Hd3dPersistenceException
    {
        super(context);
        if (object == null)
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: object cannot be null");
        }
        this.object = object;
        this.approvalNoteTypeId = approvalNoteTypeId;
    }

    public GetApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, IBase object,
            List<Long> approvalNoteTypeIds) throws Hd3dPersistenceException
    {
        super(context);
        if (object == null)
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: object cannot be null");
        }
        this.object = object;
        this.approvalNoteTypeIds = approvalNoteTypeIds;
    }

    /* Constructor for multiple objects query */
    public GetApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, List<Long> ids, String entityName)
            throws Hd3dPersistenceException
    {
        super(context);
        if (CollectUtils.isEmpty(ids) || StringUtils.isBlank(entityName))
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: ids or entityName cannot be null");
        }
        this.ids = ids;
        this.entityName = entityName;
    }

    public GetApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, List<Long> ids,
            String entityName, Long approvalNoteTypeId) throws Hd3dPersistenceException
    {
        super(context);
        if (CollectUtils.isEmpty(ids) || StringUtils.isBlank(entityName))
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: ids or entityName cannot be null");
        }
        this.ids = ids;
        this.entityName = entityName;
        this.approvalNoteTypeId = approvalNoteTypeId;
    }

    public GetApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, List<Long> ids,
            String entityName, List<Long> approvalNoteTypeIds) throws Hd3dPersistenceException
    {
        super(context);
        if (CollectUtils.isEmpty(ids) || StringUtils.isBlank(entityName))
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: ids or entityName cannot be null");
        }
        this.ids = ids;
        this.entityName = entityName;
        this.approvalNoteTypeIds = approvalNoteTypeIds;
    }

    public GetApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, List<Long> typeIds)
    {
        super(context);
        this.approvalNoteTypeIds = typeIds;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        if (forCount)
            return (Criteria) weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors.approvalnote);
        else
            return (Criteria) weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.approvalnote);
    }

    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        if (object != null)
        {
            transformers.add(new EqCriteriaTransformer(context, Const.BOUNDENTITY, object.getId()));
            transformers.add(new EqCriteriaTransformer(context, Const.BOUNDENTITYNAME, object.entityName()));
        }
        else if (ids != null)
        {
            transformers.add(new EqCriteriaTransformer(context, Const.BOUNDENTITY, ids));
            transformers.add(new EqCriteriaTransformer(context, Const.BOUNDENTITYNAME, entityName));
        }
        if (approvalNoteTypeId != null)
        {
            transformers.add(new EqCriteriaTransformer(context, Const.APPROVALNOTETYPE_ID, approvalNoteTypeId));
        }
        else if (approvalNoteTypeIds != null)
        {
            transformers.add(new EqCriteriaTransformer(context, Const.APPROVALNOTETYPE_ID, approvalNoteTypeIds));
        }
        else
        {
            transformers.add(new IsNullCriteriaTransformer(context, Const.APPROVALNOTETYPE_ID));
        }

        transformers.add(new OrderCriteriaTransformer(context, Const.APPROVALDATE, false));

        return transformers;
    }

}
