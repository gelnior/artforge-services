package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.services.resources.ResourceContext;


public class GetObject<E extends IBase> extends UniqueResultQuery<ILBase, E>
{
    private final Long id;

    public GetObject(final ResourceContext<?, ?> context, final Long id)
    {
        super(context);
        this.id = id;
    }

    public GetObject(final ResourceContext<?, ?> context, final Long id, LockMode lockMode)
    {
        super(context, lockMode);
        this.id = id;
    }

    public GetObject(final Long id)
    {
        super();
        this.id = id;
    }

    public GetObject(final Long id, LockMode lockMode)
    {
        super(lockMode);
        this.id = id;
    }

    @SuppressWarnings("unchecked")
    public void execute(final org.hibernate.Session session) throws Hd3dException
    {
        /*
         * Note: it is not possible to use a Criteria or Query here because it may cause multiple loads of objects
         * already present in session (shared reference exception...).
         */

        // A obj = (A) session.get(getPersistedClass(), id);
        // if (obj != null)
        // {
        // if (IBaseObject.INTERNALSTATUS_ENABLED.equals(((IBaseObject) obj).getInternalStatus()))
        // {
        // result = obj;
        // }
        // }
        // workaround: because filter does not apply on session.get(object) or session.load(object)
        // FilterImpl internalStatusFilter = (FilterImpl) session.getEnabledFilter("internalStatus");
        // if (IBaseObject.INTERNALSTATUS_ENABLED
        // .equals((Integer) internalStatusFilter.getParameter("internalStatus")))
        // result = obj;
        // Note: session.get does not apply filters, so a query is used instead.
        // But this may cause a "Found shared references to a collection" exception when the object
        // is still in session
        try
        {
            Criteria criteria = getCriteria(session);
            if (getLockMode() != LockMode.NONE && getLockMode() != null)
            {
                criteria.setLockMode(getLockMode());
            }
            result = (E) criteria.uniqueResult();
        }
        catch (HibernateException e)
        {
            E obj = (E) session.get(getContext().getPersistedClass(), id);
            if (obj != null)
            {
                if (fr.hd3d.common.client.Const.INTERNALSTATUS_ENABLED.equals(((IBase) obj).getInternalStatus()))
                {
                    result = obj;
                }
            }
        }
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        return weaveCriteria(session, id);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers() throws Hd3dException
    {
        // TODO Auto-generated method stub
        return null;
    }
}
