package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Example;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;


public class GetCollectionByExampleCmd<E extends IBase> extends AbstractHibernateQuery<E>
{
    public List<E> result;
    private final Object value;

    public GetCollectionByExampleCmd(final ResourceContext<?, E> context, Object value)
    {
        super(context);
        this.value = value;
    }

    public GetCollectionByExampleCmd(Object value)
    {
        this(null, value);
    }

    @SuppressWarnings("unchecked")
    public void execute(org.hibernate.Session session) throws Hd3dException
    {
        Criteria criteria = getCriteria(session);
        if (getLockMode() != LockMode.NONE && getLockMode() != null)
        {
            criteria.setLockMode(getLockMode());
        }
        result = criteria.list();
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        // DetachedCriteria subQuery = ((HibernatePersist<?, ?>) getContext().getPersistor()).basicSubCriteria(session);

        final CompositeQueryModifier filter = context.getFilter();

        // if (filter != null)
        // {
        // filter.applyModifier(subQuery);
        // }

        Criteria criteria = ((HibernatePersist<?, ?>) getContext().getPersistor()).basicCriteria(session);
        criteria.add(Example.create(value));

        if (filter != null)
        {
            filter.applyModifier(criteria);
        }

        return criteria;
    }
}
