package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.impl.hibernate.SimpleActivityH;
import fr.hd3d.model.persistence.impl.hibernate.TaskActivityH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class GetProjectPersonActivitiesCmd extends ListResultQuery<ILActivity, IActivity>
{
    public GetProjectPersonActivitiesCmd(ResourceContext<ILActivity, IActivity> context)
    {
        super(context);
    }

    @SuppressWarnings("unchecked")
    public void execute(Session session) throws Hd3dException
    {
        context.setPersistedClass(TaskActivityH.class);
        ListResultQuery<ILTaskActivity, ITaskActivity> taskActivities = new GetProjectPersonTaskActivitiesCmd(
                (ResourceContext<ILTaskActivity, ITaskActivity>) context).wantTotalSize();
        HibernateUtil.safeQuery(taskActivities);

        context.setPersistedClass(SimpleActivityH.class);
        ListResultQuery<ILSimpleActivity, ISimpleActivity> simpleActivities = new GetProjectPersonSimpleActivitiesCmd(
                (ResourceContext<ILSimpleActivity, ISimpleActivity>) context).wantTotalSize();
        HibernateUtil.safeQuery(simpleActivities);

        List<IActivity> unfilteredActivities = new ArrayList<IActivity>();
        if (CollectUtils.isNotEmpty(taskActivities.result))
            unfilteredActivities.addAll(taskActivities.result);
        if (CollectUtils.isNotEmpty(simpleActivities.result))
            unfilteredActivities.addAll(simpleActivities.result);

        result = ObjectProviderCustoms.filter(unfilteredActivities);

        totalCount = taskActivities.totalCount + simpleActivities.totalCount;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
