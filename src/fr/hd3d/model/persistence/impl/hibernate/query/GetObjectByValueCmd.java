package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.ExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;


public class GetObjectByValueCmd<E extends IBase> extends UniqueResultQuery<ILBase, E>
{
    private final String property;
    private final Object value;

    /**
     * @param property
     * @param value
     */
    public GetObjectByValueCmd(final ResourceContext<?, ?> context, final String property, final Object value)
    {
        super(context);
        this.property = property;
        this.value = value;
    }

    public GetObjectByValueCmd(final ResourceContext<?, ?> context, final String property, final Object value,
            LockMode lockMode)
    {
        super(context, lockMode);
        this.property = property;
        this.value = value;
    }

    public GetObjectByValueCmd(final String property, final Object value)
    {
        this.property = property;
        this.value = value;
    }

    public GetObjectByValueCmd(final String property, final Object value, LockMode lockMode)
    {
        super(lockMode);
        this.property = property;
        this.value = value;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        return weaveCriteria(session);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers() throws Hd3dException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        final String col = ExpressionQueryModifier.updateColumn(property, createAliases(transformers, property));
        transformers.add(new EqCriteriaTransformer(col, value));

        return transformers;
    }

}
