package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.TASK_ACTIVITY_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.utils.AuthenticationUtil;


public class GetPersonTaskActivityCmd extends UniqueResultQuery<ILTaskActivity, ITaskActivity>
{
    public GetPersonTaskActivityCmd(ResourceContext<ILTaskActivity, ITaskActivity> context)
    {
        super(context);
    }

    @Override
    public void execute(Session session) throws Hd3dException
    {
        ITaskActivity activity = (ITaskActivity) getCriteria(session).uniqueResult();

        Long id = context.getIdFromUrl(PERSON_ID_ATTRIBUTE);

        if (id == null || id == 0)
            id = AuthenticationUtil.getCurrentUser().getId();

        if (activity != null && activity.getFilledBy() != null && activity.getFilledBy().getId().equals(id))
        {
            result = (ITaskActivity) activity;
        }
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        return weaveCriteria(session, context.getIdFromUrl(TASK_ACTIVITY_ID_ATTRIBUTE));
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers() throws Hd3dPersistenceException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        return transformers;
    }
}
