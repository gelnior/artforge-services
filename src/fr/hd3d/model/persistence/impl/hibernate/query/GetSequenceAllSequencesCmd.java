package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.SEQUENCE_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;


public class GetSequenceAllSequencesCmd extends ListResultQuery<ILSequence, ISequence>
{
    public GetSequenceAllSequencesCmd(ResourceContext<ILSequence, ISequence> context)
    {
        super(context);
    }

    @SuppressWarnings("unchecked")
    public void execute(Session session) throws Hd3dException
    {
        final Long sequenceId = context.getIdFromUrl(SEQUENCE_ID_ATTRIBUTE);
        checkId(sequenceId, MessageFormat.format(ERR_5, sequenceId));

        ISequence sequence = Persistors.sequence.getById(sequenceId);

        if (sequence == null)
            throw new Hd3dPersistenceException(MessageFormat.format(ERR_5, sequenceId));

        Collection<ISequence> sequenceSequences = sequence.getChildren();

        Set<ISequence> allSequences = new HashSet<ISequence>(sequenceSequences);

        for (ISequence seq : sequenceSequences)
            allSequences.addAll(seq.allChildren(true));

        result = new ArrayList(allSequences);
        result = ObjectProviderCustoms.filter(result);

        if (result != null)
            totalCount = result.size();
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
