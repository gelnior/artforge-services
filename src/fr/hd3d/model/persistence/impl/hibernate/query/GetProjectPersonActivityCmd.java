package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.ACTIVITY_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetProjectPersonActivityCmd extends UniqueResultQuery<ILActivity, IActivity>
{
    private Long projectId;
    private Long personId;
    private Long activityId;

    public GetProjectPersonActivityCmd(ResourceContext<ILActivity, IActivity> context)
    {
        super(context);
    }

    public GetProjectPersonActivityCmd(ResourceContext<ILActivity, IActivity> context, LockMode lockMode)
    {
        super(context, lockMode);
    }

    public GetProjectPersonActivityCmd(ResourceContext<ILActivity, IActivity> context, Long projectId, Long personId,
            Long activityId)
    {
        super(context);
        this.projectId = projectId;
        this.personId = personId;
        this.activityId = activityId;
    }

    public GetProjectPersonActivityCmd(ResourceContext<ILActivity, IActivity> context, LockMode lockMode,
            Long projectId, Long personId, Long activityId)
    {
        super(context, lockMode);
        this.projectId = projectId;
        this.personId = personId;
        this.activityId = activityId;
    }

    public GetProjectPersonActivityCmd(LockMode lockMode, Long projectId, Long personId, Long activityId)
    {
        super(lockMode);
        this.projectId = projectId;
        this.personId = personId;
        this.activityId = activityId;
    }

    @Override
    public void execute(Session session) throws Hd3dException
    {
        super.execute(session);

        if (result == null)
        {
            final Long activityId = idValue(this.activityId, ACTIVITY_ID_ATTRIBUTE);
            final Criteria taskCriteria = ((HibernatePersist<?, ?>) context.getPersistor()).basicIdCriteria(session,
                    activityId).createCriteria("task");
            if (getLockMode() != LockMode.NONE && getLockMode() != null)
            {
                taskCriteria.setLockMode(getLockMode());
            }
            getTransformer(getCoreTransformers()).transform(taskCriteria);

            super.execute(taskCriteria);
        }
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        return weaveCriteria(session, idValue(this.activityId, ACTIVITY_ID_ATTRIBUTE));
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers() throws Hd3dPersistenceException
    {
        final Long projectId = idValue(this.projectId, PROJECT_ID_ATTRIBUTE);
        final Long personId = idValue(this.personId, PERSON_ID_ATTRIBUTE);

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.PROJECT, projectId));
        transformers.add(new IdEqCriteriaTransformer(context, Const.WORKER, personId));

        return transformers;
    }

    // protected List<Transformer<Criteria, Criteria>> getTaskActivityTransformers() throws Hd3dPersistenceException
    // {
    // return getCoreTransformers();
    // // final Long projectId = idValue(this.projectId, PROJECT_ID_ATTRIBUTE);
    // // final Long personId = idValue(this.personId, PERSON_ID_ATTRIBUTE);
    // //
    // // List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
    // // transformers.add(new IdEqCriteriaTransformer(context, Const.PROJECT, projectId));
    // // transformers.add(new IdEqCriteriaTransformer(context, Const.WORKER, personId));
    // //
    // // return transformers;
    // }
}
