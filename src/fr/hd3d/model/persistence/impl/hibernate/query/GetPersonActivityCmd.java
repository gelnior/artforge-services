package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.ACTIVITY_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.services.resources.ResourceContext;


public class GetPersonActivityCmd extends UniqueResultQuery<ILActivity, IActivity>
{
    public GetPersonActivityCmd(ResourceContext<ILActivity, IActivity> context)
    {
        super(context);
    }

    @Override
    public void execute(Session session) throws Hd3dException
    {
        IActivity activity = (IActivity) getCriteria(session).uniqueResult();

        final Long id = context.getIdFromUrl(PERSON_ID_ATTRIBUTE);
        if (activity != null && activity.getWorker() != null && activity.getWorker().getId().equals(id))
        {
            result = activity;
        }
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        return weaveCriteria(session, context.getIdFromUrl(ACTIVITY_ID_ATTRIBUTE));
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers() throws Hd3dPersistenceException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        return transformers;
    }
}
