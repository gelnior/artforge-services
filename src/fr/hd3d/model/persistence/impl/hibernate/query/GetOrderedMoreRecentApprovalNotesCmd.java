package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.GtCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.OrderCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


/**
 * Return the approval notes with date > given date. If given date is null, return the MOST RECENT approval note
 * 
 * @author tlam
 * 
 */
public class GetOrderedMoreRecentApprovalNotesCmd extends GetApprovalNotesCmd
{
    protected Date approvalDate;

    public GetOrderedMoreRecentApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context,
            Date approvalDate)
    {
        super(context);
        this.approvalDate = approvalDate;
    }

    /* Constructor for single object query */
    public GetOrderedMoreRecentApprovalNotesCmd(IBase object, Date approvalDate) throws Hd3dPersistenceException
    {
        super(object);
        this.approvalDate = approvalDate;
    }

    public GetOrderedMoreRecentApprovalNotesCmd(IBase object, Long approvalNoteTypeId, Date approvalDate)
            throws Hd3dPersistenceException
    {
        super(object, approvalNoteTypeId);
        this.approvalDate = approvalDate;
    }

    public GetOrderedMoreRecentApprovalNotesCmd(IBase object, List<Long> approvalNoteTypeIds, Date approvalDate)
            throws Hd3dPersistenceException
    {
        super(object, approvalNoteTypeIds);
        this.approvalDate = approvalDate;
    }

    /* Constructor for multiple objects query */
    public GetOrderedMoreRecentApprovalNotesCmd(List<Long> ids, String entityName, Date approvalDate)
            throws Hd3dPersistenceException
    {
        super(ids, entityName);
        this.approvalDate = approvalDate;
    }

    public GetOrderedMoreRecentApprovalNotesCmd(List<Long> ids, String entityName, Long approvalNoteTypeId,
            Date approvalDate) throws Hd3dPersistenceException
    {
        super(ids, entityName, approvalNoteTypeId);
        this.approvalDate = approvalDate;
    }

    public GetOrderedMoreRecentApprovalNotesCmd(List<Long> ids, String entityName, List<Long> approvalNoteTypeIds,
            Date approvalDate) throws Hd3dPersistenceException
    {
        super(ids, entityName, approvalNoteTypeIds);
        this.approvalDate = approvalDate;
    }

    /* Constructor for single object query */
    public GetOrderedMoreRecentApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, IBase object,
            Date approvalDate) throws Hd3dPersistenceException
    {
        super(context, object);
        this.approvalDate = approvalDate;
    }

    public GetOrderedMoreRecentApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, IBase object,
            Long approvalNoteTypeId, Date approvalDate) throws Hd3dPersistenceException
    {
        super(context, object, approvalNoteTypeId);
        this.approvalDate = approvalDate;
    }

    public GetOrderedMoreRecentApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, IBase object,
            List<Long> approvalNoteTypeIds, Date approvalDate) throws Hd3dPersistenceException
    {
        super(context, object, approvalNoteTypeIds);
        this.approvalDate = approvalDate;
    }

    /* Constructor for multiple objects query */
    public GetOrderedMoreRecentApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, List<Long> ids,
            String entityName, Date approvalDate) throws Hd3dPersistenceException
    {
        super(context, ids, entityName);
        this.approvalDate = approvalDate;
    }

    public GetOrderedMoreRecentApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, List<Long> ids,
            String entityName, Long approvalNoteTypeId, Date approvalDate) throws Hd3dPersistenceException
    {
        super(context, ids, entityName, approvalNoteTypeId);
        this.approvalDate = approvalDate;
    }

    public GetOrderedMoreRecentApprovalNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, List<Long> ids,
            String entityName, List<Long> approvalNoteTypeIds, Date approvalDate) throws Hd3dPersistenceException
    {
        super(context, ids, entityName, approvalNoteTypeIds);
        this.approvalDate = approvalDate;
    }

    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        transformers.addAll(super.getCoreTransformers(session));

        if (approvalDate != null)
        {
            transformers.add(new GtCriteriaTransformer(context, Const.APPROVALDATE, approvalDate));
        }
        // else
        // {
        // transformers.add(new IsNullCriteriaTransformer(context, Const.APPROVALDATE));
        // }
        transformers.add(new OrderCriteriaTransformer(context, Const.APPROVALDATE, true));

        return transformers;
    }

}
