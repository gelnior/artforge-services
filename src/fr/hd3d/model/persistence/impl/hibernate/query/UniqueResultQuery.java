package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ChainedTransformer;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;


public abstract class UniqueResultQuery<L, P extends IBase> extends AbstractHibernateQuery<P>
{
    public P result;

    public UniqueResultQuery()
    {
        super();
    }

    public UniqueResultQuery(ResourceContext<?, ?> context)
    {
        super(context);
    }

    public UniqueResultQuery(ResourceContext<?, ?> context, LockMode lockMode)
    {
        super(context, lockMode);
    }

    public UniqueResultQuery(LockMode lockMode)
    {
        super(lockMode);
    }

    public void execute(Session session) throws Hd3dException
    {
        execute(getCriteria(session));
    }

    protected void execute(Criteria criteria) throws Hd3dException
    {
        if (isForceEmptyResult())
        {
            result = null;
        }
        else
        {
            if (getLockMode() != LockMode.NONE && getLockMode() != null)
            {
                criteria.setLockMode(getLockMode());
            }
            result = (P) criteria.uniqueResult();
        }
    }

    protected Criteria weaveCriteria(Session session) throws Hd3dException
    {
        return weaveCriteria(session, (HibernatePersist<?, ?>) context.getPersistor());
    }

    protected Criteria weaveCriteria(Session session, Long id) throws Hd3dException
    {
        return weaveCriteria(session, (HibernatePersist<?, ?>) context.getPersistor(), id);
    }

    protected Criteria weaveCriteria(Session session, Long id, Transformer<Criteria, Criteria> transformer)
            throws Hd3dException
    {
        return weaveCriteria(session, (HibernatePersist<?, ?>) context.getPersistor(), id, transformer);
    }

    protected Criteria weaveCriteria(Session session, HibernatePersist<?, ?> persistor, Long id) throws Hd3dException
    {
        return weaveCriteria(session, persistor, id, getTransformer());
    }

    protected Criteria weaveCriteria(Session session, HibernatePersist<?, ?> persistor) throws Hd3dException
    {
        return weaveCriteria(session, persistor, getTransformer());
    }

    protected Criteria weaveCriteria(Session session, HibernatePersist<?, ?> persistor, Long id,
            Transformer<Criteria, Criteria> transformer) throws Hd3dException
    {
        final Criteria query = persistor.basicIdCriteria(session, id);
        transformer.transform(query);

        addACL(query, persistor.getPersistedClass());
        /* Note: does not make sense to apply constraints on an unique result */

        return query;
    }

    protected Criteria weaveCriteria(Session session, HibernatePersist<?, ?> persistor,
            Transformer<Criteria, Criteria> transformer) throws Hd3dException
    {
        // DetachedCriteria subQuery = ((HibernatePersist<?, ?>) getContext().getPersistor()).basicSubCriteria(session);

        final Criteria query = ((HibernatePersist<?, ?>) getContext().getPersistor()).basicCriteria(session);
        transformer.transform(query);

        addACL(query, persistor.getPersistedClass());

        return query;
    }

    abstract protected List<Transformer<Criteria, Criteria>> getCoreTransformers() throws Hd3dException;

    protected Transformer<Criteria, Criteria> getTransformer() throws Hd3dException
    {
        return getTransformer(getCoreTransformers());
    }

    protected Transformer<Criteria, Criteria> getTransformer(List<Transformer<Criteria, Criteria>> coreTransformer)
            throws Hd3dException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        CollectUtils.addAllIfNotEmpty(transformers, coreTransformer);

        return ChainedTransformer.getInstance(transformers.toArray(new Transformer[transformers.size()]));
    }

}
