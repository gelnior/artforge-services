package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PLANNING_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.TASK_GROUP_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILTaskGroup;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.IdEqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetPlanningTaskGroupCmd extends UniqueResultQuery<ILTaskGroup, ITaskGroup>
{
    public GetPlanningTaskGroupCmd(ResourceContext<ILTaskGroup, ITaskGroup> context)
    {
        super(context);
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        return weaveCriteria(session, context.getIdFromUrl(TASK_GROUP_ID_ATTRIBUTE));
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers() throws Hd3dPersistenceException
    {
        final Long planningId = context.getIdFromUrl(PLANNING_ID_ATTRIBUTE);

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new IdEqCriteriaTransformer(context, Const.PLANNING, planningId));

        return transformers;
    }
}
