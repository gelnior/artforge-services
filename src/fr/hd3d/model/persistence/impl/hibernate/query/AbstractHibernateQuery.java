package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryModifiersList;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.CreateAliasQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.ExpressionQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.AliasCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.dao.Hd3dACLH;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.HibernateUtil;


public abstract class AbstractHibernateQuery<P extends Persistent> implements IHibernateQuery
{
    Criteria criteria = null;
    Query query = null;
    protected ResourceContext<?, ?> context = new ResourceContext();
    protected LockMode lockMode = null;
    protected ProjectionList projectionList;
    /* by default, results are filtered by permission */
    protected boolean permissionOn = true && Conf.isAuthEnable();
    protected boolean forceEmptyResult;

    protected AbstractHibernateQuery()
    {}

    protected AbstractHibernateQuery(ResourceContext<?, ?> context)
    {
        if (context != null)
        {
            this.context = context;
        }
    }

    protected AbstractHibernateQuery(ResourceContext<?, ?> context, LockMode lockMode)
    {
        if (context != null)
        {
            this.context = context;
        }
        if (lockMode != null)
        {
            this.lockMode = lockMode;
        }
    }

    protected AbstractHibernateQuery(LockMode lockMode)
    {
        if (lockMode != null)
        {
            this.lockMode = lockMode;
        }
    }

    public LockMode getLockMode()
    {
        return lockMode;
    }

    public void setLockMode(LockMode lockMode)
    {
        this.lockMode = lockMode;
    }

    public Criteria getCriteria(Session session) throws Hd3dException
    {
        if (criteria == null)
        {
            Object criteriaOrQuery = buildCriteriaOrQuery(session, false);
            if (criteriaOrQuery instanceof Criteria)
            {
                criteria = (Criteria) criteriaOrQuery;
            }
        }
        return criteria;
    }

    public Query getQuery(Session session) throws Hd3dException
    {
        if (query == null)
        {
            Object criteriaOrQuery = buildCriteriaOrQuery(session, false);
            if (criteriaOrQuery instanceof Query)
            {
                query = (Query) criteriaOrQuery;
            }
        }
        return query;
    }

    public Criteria getCriteriaForCount(Session session) throws Hd3dException
    {
        if (criteria == null)
        {
            Object criteriaOrQuery = buildCriteriaOrQuery(session, true);
            if (criteriaOrQuery instanceof Criteria)
            {
                criteria = (Criteria) criteriaOrQuery;
            }
        }
        return criteria;
    }

    public Query getQueryForCount(Session session) throws Hd3dException
    {
        if (query == null)
        {
            Object criteriaOrQuery = buildCriteriaOrQuery(session, true);
            if (criteriaOrQuery instanceof Query)
            {
                query = (Query) criteriaOrQuery;
            }
        }
        return query;
    }

    public ProjectionList getProjectionList()
    {
        return projectionList;
    }

    public void setProjectionList(ProjectionList projectionList)
    {
        this.projectionList = projectionList;
    }

    public void clearCriteria()
    {
        this.criteria = null;
    }

    public void clearQuery()
    {
        this.query = null;
    }

    public ResourceContext<?, ?> getContext()
    {
        return context;
    }

    public boolean isPermissionOn()
    {
        return permissionOn;
    }

    public void setPermissionOn(boolean permissionOn)
    {
        this.permissionOn = permissionOn && Conf.isAuthEnable();
    }

    protected Long idValue(Long idField, String idUrlAttribute)
    {
        return idField == null ? context.getIdFromUrl(idUrlAttribute) : idField;
    }

    protected void addACL(Criteria criteria, final Class<?> clazz)
    {
        return;

        // if (!isPermissionOn())
        // return;
        //
        // boolean a = AuthenticationUtil.getCurrentUserPrincipal() != null;
        // boolean b = !AuthenticationUtil.isCurrentSuperUser();
        // if (a && b)
        // {
        // final List<Long> roleIds = CollectUtils.getIds(AuthenticationUtil.getCurrentUserRoles());
        // final String entityName = EntitiesMaps.withClass(clazz).getEntityName();
        //
        // final List<IHd3dACL> acls = getACLs(roleIds, entityName);
        //
        // final Set<Long> readIds = getReadIds(acls);
        //
        // boolean allPermitted = readIds.size() == 1 && readIds.contains(0L);
        // if (!allPermitted)
        // {
        // Collection<Long> permittedIds = getPermitted(readIds);
        // Collection<Long> notPermittedIds = getNotPermitted(readIds);
        //
        // /* "all permitted except some" */
        // if (acls.contains(0L))
        // {
        // if (CollectUtils.isNotEmpty(notPermittedIds))
        // {
        // criteria.add(Restrictions.not(Restrictions.in(Const.ID, notPermittedIds)));
        // }
        // }
        // else
        // {
        // /* "some are permitted except some" */
        // if (CollectUtils.isNotEmpty(permittedIds))
        // {
        // criteria.add(Restrictions.in(Const.ID, permittedIds));
        // }
        // else
        // {
        // setForceEmptyResult(true);
        // }
        // /* Note: no need to handle negative id, when not all permitted, only the positive ids are allowed */
        // }
        // }
        // }
    }

    private List<IHd3dACL> getACLs(List<Long> roleIds, String entityName)
    {
        // Session session = HibernateUtil.getSessionFactory().openStatelessSession();
        Session session = HibernateUtil.currentSession();
        final Criteria crit = session.createCriteria(Hd3dACLH.class, "acl");
        crit.createAlias("acl.roles", "rls");
        crit.add(Restrictions.in("rls.id", roleIds));
        crit.add(Restrictions.eq("entityName", entityName));
        /* No projection: used later to check update and delete */
        // statelessSessionCrit.setProjection(Projections.property("allowedToRead"));
        crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return crit.list();
    }

    private Collection<Long> getPermitted(Set<Long> acls)
    {
        return org.apache.commons.collections15.CollectionUtils.select(acls,
                new org.apache.commons.collections15.Predicate<Long>() {

                    public boolean evaluate(Long id)
                    {
                        return id > 0;
                    }
                });
    }

    private Collection<Long> getNotPermitted(Set<Long> acls)
    {
        return org.apache.commons.collections15.CollectionUtils.collect(
                org.apache.commons.collections15.CollectionUtils.select(acls,
                        new org.apache.commons.collections15.Predicate<Long>() {

                            public boolean evaluate(Long id)
                            {
                                return id < 0;
                            }
                        }), new Transformer<Long, Long>() {

                    /* reverse the sign */
                    public Long transform(Long id)
                    {
                        return -id;
                    }
                });
    }

    protected abstract Object buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException;

    public boolean isForceEmptyResult()
    {
        return forceEmptyResult;
    }

    public void setForceEmptyResult(boolean forceEmptyResult)
    {
        this.forceEmptyResult = forceEmptyResult;
    }

    protected List<IQueryModifier> createAliases(List<Transformer<Criteria, Criteria>> transformers, String property)
    {
        List<IQueryModifier> aliases = ExpressionQueryModifier.createAliases(new QueryModifiersList(), context
                .getPersistedClass(), property);
        if (CollectUtils.isNotEmpty(aliases))
        {
            CreateAliasQueryModifier aliasModifier;
            for (IQueryModifier alias : aliases)
            {
                aliasModifier = (CreateAliasQueryModifier) alias;
                transformers.add(new AliasCriteriaTransformer(aliasModifier.getAssociationPath(), aliasModifier
                        .getAlias()));
            }
        }
        return aliases;
    }
}
