package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.AliasCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


public class GetPersonTaskActivitiesCmd<L extends ILActivity, P extends ITaskActivity> extends ListResultQuery<L, P>
{
    private Long personId;

    public GetPersonTaskActivitiesCmd(ResourceContext<L, P> context)
    {
        super(context);
    }

    public GetPersonTaskActivitiesCmd(ResourceContext<L, P> context, Long personId)
    {
        super(context);
        this.personId = personId;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        if (forCount)
            return (Criteria) weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors.taskActivity);
        else
            return (Criteria) weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.taskActivity);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        final Long personId = idValue(this.personId, PERSON_ID_ATTRIBUTE);

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        // transformers.add(new FilterApplyer(context.getFilter()));
        transformers.add(new AliasCriteriaTransformer(context, "this." + Const.DAY, "activityPersonDay"));
        transformers.add(new EqCriteriaTransformer(context, "activityPersonDay" + '.' + Const.PERSON + Const._ID,
                personId));

        // transformers.add(new CreateCriteriaTransformer(context, Const.DAY));
        // IdEqCriteriaTransformer idEq = new IdEqCriteriaTransformer(context, Const.PERSON, personId);
        // idEq.setCurrentClass(PersonDayH.class);
        // transformers.add(idEq);

        return transformers;
    }

    // protected List<Transformer<Criteria, Criteria>> getTailingTransformers(Session session)
    // {
    // List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
    //
    // transformers.add(new DefaultMaxResultSetter());
    // return transformers;
    // }
}
