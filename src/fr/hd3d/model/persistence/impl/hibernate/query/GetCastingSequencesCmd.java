package fr.hd3d.model.persistence.impl.hibernate.query;

import static fr.hd3d.common.client.ServicesURI.SEQUENCE_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.ServerResourceUtils;


public class GetCastingSequencesCmd extends ListResultQuery<ILConstituent, IConstituent>
{
    public GetCastingSequencesCmd(ResourceContext<ILConstituent, IConstituent> context)
    {
        super(context);
    }

    @SuppressWarnings("unchecked")
    public void execute(Session session) throws Hd3dException
    {
        final Criteria compositionQuery = getCriteria(session);

        List compos = compositionQuery.list();

        ArrayList<IConstituent> constituents = new ArrayList<IConstituent>(compos.size());

        Iterator iter = compos.iterator();
        while (iter.hasNext())
        {
            Map map = (Map) iter.next();

            IComposition compo = (IComposition) map.get(Criteria.ROOT_ALIAS);
            constituents.add((IConstituent) compo.getConstituent());
        }
        constituents.trimToSize();

        result = ObjectProviderCustoms.filter(result);

        this.result = constituents;
        if (result != null)
            totalCount = result.size();

        if (context.isPaginated() || totalCount <= Conf.getMaxRecords())
        {
            compositionQuery.setFirstResult(0);
            compositionQuery.setMaxResults(Integer.MAX_VALUE);
            compositionQuery.setProjection(Projections.rowCount());
            List objects = compositionQuery.list();
            if (!objects.isEmpty())
                totalCount = ((Long) objects.get(0)).intValue();
        }
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        final Long id = context.getIdFromUrl(SEQUENCE_ID_ATTRIBUTE);
        final ISequence sequence = Persistors.sequence.getById(id);

        final CompositeQueryModifier filter = context.getFilter();

        // DetachedCriteria subQuery = HibernatePersist.basicSubCriteria(session, Persistors.composition
        // .getPersistedClass());
        //
        // if (filter != null)
        // {
        // if (forCount)
        // filter.applyModifierForCount(subQuery);
        // else
        // filter.applyModifier(subQuery);
        // }

        final Criteria compositionQuery = HibernatePersist.basicCriteria(session, Persistors.composition
                .getPersistedClass());
        ServerResourceUtils.addIdEqRestriction(context, compositionQuery, "sequence", sequence.getId());
        compositionQuery.createCriteria("constituent").setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);

        if (filter != null)
        {
            if (forCount)
                filter.applyModifierForCount(compositionQuery);
            else
                filter.applyModifier(compositionQuery);
        }

        // limit when retrieving records
        if (!context.isPaginated() && !noMaxResult)
            compositionQuery.setMaxResults(Conf.getMaxRecords());

        return compositionQuery;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
