package fr.hd3d.model.persistence.impl.hibernate.query;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.EqCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;


/**
 * Return approval notes linked to given entity. Entity ID is retrieved from context, so context ID key must be given.
 * 
 * @author HD3D
 */
public class GetWorkObjectNotesCmd extends ListResultQuery<ILApprovalNote, IApprovalNote>
{
    private String boundEntityName;
    private String idKey;

    public GetWorkObjectNotesCmd(ResourceContext<ILApprovalNote, IApprovalNote> context, String idKey,
            String boundEntityName)
    {
        super(context);

        this.boundEntityName = boundEntityName;
        this.idKey = idKey;
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException
    {
        final Long workObjectId = context.getIdFromUrl(this.idKey);
        checkId(workObjectId, MessageFormat.format(ERR_1, workObjectId));

        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new EqCriteriaTransformer(Const.BOUNDENTITYNAME, boundEntityName));
        transformers.add(new EqCriteriaTransformer(Const.BOUNDENTITY, workObjectId));

        return transformers;
    }
}
