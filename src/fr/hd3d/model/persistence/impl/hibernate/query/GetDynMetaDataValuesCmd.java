package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILDynMetaDataValue;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.DynMetaDataValuesTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;


public class GetDynMetaDataValuesCmd extends ListResultQuery<ILDynMetaDataValue, IDynMetaDataValue>
{
    /* single object query */
    IBase object = null;
    /* multiple objects query */
    List<Long> ids = null;
    String entityName = null;
    Long classDynMetaDataId = null;

    public GetDynMetaDataValuesCmd(ResourceContext<ILDynMetaDataValue, IDynMetaDataValue> context)
    {
        super(context);
    }

    /* Constructor for single object query */
    public GetDynMetaDataValuesCmd(IBase object) throws Hd3dPersistenceException
    {
        this(object, null);
    }

    public GetDynMetaDataValuesCmd(IBase object, Long classDynMetaDataId) throws Hd3dPersistenceException
    {
        if (object == null)
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: object cannot be null");
        }
        this.object = object;
        this.classDynMetaDataId = classDynMetaDataId;
        ids = null;
        entityName = null;
    }

    /* Constructor for multiple objects query */
    public GetDynMetaDataValuesCmd(List<Long> ids, String entityName) throws Hd3dPersistenceException
    {
        this(ids, entityName, null);
    }

    public GetDynMetaDataValuesCmd(List<Long> ids, String entityName, Long classDynMetaDataId)
            throws Hd3dPersistenceException
    {
        if (CollectUtils.isEmpty(ids))
        {
            throw new Hd3dPersistenceException("GetDynMetaDataValuesCmd: ids or entityName cannot be null");
        }
        this.ids = ids;
        this.entityName = entityName;
        this.object = null;
        this.classDynMetaDataId = classDynMetaDataId;
    }

    @Override
    protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        if (forCount)
            return (Criteria) weaveCriteriaForCount(session, (HibernatePersist<?, ?>) Persistors.dynmetadatavalue);
        else
            return (Criteria) weaveCriteria(session, (HibernatePersist<?, ?>) Persistors.dynmetadatavalue);
    }

    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        if (object != null)
        {
            transformers.add(new DynMetaDataValuesTransformer(context, object, classDynMetaDataId));
        }
        else
        {
            transformers.add(new DynMetaDataValuesTransformer(context, ids, entityName, classDynMetaDataId));
        }

        return transformers;
    }
}
