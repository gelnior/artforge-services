package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.list.SetUniqueList;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ChainedTransformer;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.IQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.FirstResultQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.MaxResultQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.modifier.OrderByQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.MaxCriteriaTransformer;
import fr.hd3d.model.persistence.impl.hibernate.criteria.transformer.OrderCriteriaTransformer;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.customs.ObjectProviderCustoms;
import fr.hd3d.utils.Bench;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


public abstract class ListResultQuery<L, P extends IBase> extends AbstractHibernateQuery<P>
{
    protected static final String LCONSTITUENT_ID = "fr.hd3d.model.lightweight.impl.LConstituent.id";
    protected static final String LSHOT_ID = "fr.hd3d.model.lightweight.impl.LShot.id";
    protected static final String CONSTITUENT_TYPE = "Constituent";
    protected static final String SEQUENCE_TYPE = "Sequence";
    protected static final String SHOT_TYPE = "Shot";

    protected static final CharSequence DOESNOTEXIST = " does not exist or is not accessible";
    protected static final String ERR_0 = "Project id= {0}" + DOESNOTEXIST;
    protected static final String ERR_1 = "Constituent id= {0}" + DOESNOTEXIST;
    protected static final String ERR_2 = "Shot id= {0}" + DOESNOTEXIST;
    protected static final String ERR_3 = "Category id= {0}" + DOESNOTEXIST;
    protected static final String ERR_4 = "Group id= {0}" + DOESNOTEXIST;
    protected static final String ERR_5 = "Sequence id= {0}" + DOESNOTEXIST;
    protected static final String ERR_6 = "TaskGroup id= {0}" + DOESNOTEXIST;
    protected static final String ERR_7 = "Planning id= {0}" + DOESNOTEXIST;
    protected static final String ERR_8 = "ResourceGroup id= {0}" + DOESNOTEXIST;
    protected static final String ERR_9 = "Sheet id= {0}" + DOESNOTEXIST;
    protected static final String ERR_10 = "ItemGroup id= {0}" + DOESNOTEXIST;
    protected static final String ERR_11 = "Person id= {0}" + DOESNOTEXIST;
    protected static final String ERR_12 = "AssetRevision id= {0}" + DOESNOTEXIST;
    protected static final String ERR_13 = "TagCategory id= {0}" + DOESNOTEXIST;
    protected static final String ERR_14 = "FileRevision id= {0}" + DOESNOTEXIST;
    protected static final String ERR_15 = "PlayListRevision id= {0}" + DOESNOTEXIST;

    public List<P> result;
    public List<Long> allIds;
    public int totalCount = 0;
    /* retrieve only id, the result list is cleared */
    public boolean idOnly = false;

    /* if requested, make a second query to obtain the total size, ohterwise save a query */
    public boolean wantTotalSize = false;

    /*
     * if true, criteria's setMaxResult is NOT SET to the value defined in services.properties file hd3d.maxrecords.
     * This can be harmful if a lot of records
     */
    public boolean noMaxResult = false;

    public ListResultQuery()
    {
        super();
    }

    public ListResultQuery(ResourceContext<?, ?> context)
    {
        super(context);
    }

    public boolean isIdOnly()
    {
        return idOnly;
    }

    public void setIdOnly(boolean idOnly)
    {
        this.idOnly = idOnly;
    }

    public ListResultQuery<L, P> wantTotalSize()
    {
        this.wantTotalSize = true;
        return this;
    }

    public ListResultQuery<L, P> noMaxResult()
    {
        this.noMaxResult = true;
        return this;
    }

    @SuppressWarnings("unchecked")
    public void execute(Session session) throws Hd3dException
    {
        Bench b = new Bench();

        final Criteria criteria;
        Session newSession = null;
        try
        {
            if (idOnly)
            {
                newSession = HibernateUtil.getSessionFactory().openSession();// without interceptor
                criteria = getCriteria(newSession);
            }
            else
            {
                criteria = getCriteria(session);
            }

            /*
             * Note: forceEmptyResult flag is set in getCriteria method and sub methods, so getCriteria must be called
             * before
             */
            if (isForceEmptyResult())
            {
                result = Collections.emptyList();
                totalCount = 0;
            }
            else
            {
                if (criteria != null)
                {
                    if (getLockMode() != LockMode.NONE && getLockMode() != null)
                    {
                        criteria.setLockMode(getLockMode());
                    }

                    int nbOfFilteredObjects = 0;
                    P object;

                    /*
                     * Note: permission must be handled here instead of anywhere else because some projection (eg: on id
                     * only) may be applied and permission filtering may need more than id to work
                     */
                    if (isPermissionOn())
                    {
                        Class<?> clazz = context.getPersistedClass();
                        if (clazz != null && IBase.class.isAssignableFrom(clazz))
                        {
                            result = SetUniqueList.decorate(new ArrayList<P>());
                            allIds = SetUniqueList.decorate(new ArrayList<Long>());

                            /* WARNING: the order is not done-> error */
                            //
                            // criteria.setReadOnly(true);
                            // criteria.setProjection(Projections.id());
                            // criteria.addOrder(Order.asc("id"));
                            // List<Long> ids = criteria.list();
                            // final IPersist persistor = Persistors.getPersistor(clazz.getCanonicalName());
                            // for (List<Long> partition : CollectUtils.partition(ids, Conf.getMaxRecords()))
                            // {
                            // int i = 0;
                            // List<P> objects = persistor.getByIds(partition);
                            // for (P obj : objects)
                            // {
                            // object = ObjectProviderCustoms.filter(obj);
                            // if (object != null)
                            // {
                            // if (idOnly)
                            // {
                            // allIds.add(object.getId());
                            // newSession.evict(object);
                            // object = null;
                            // i++;
                            // if (i % HibernateUtil.JDBC_BATCHSIZE == 0)
                            // {
                            // newSession.flush();
                            // }
                            // }
                            // else
                            // {
                            // result.add(object);
                            // }
                            // }
                            // else
                            // {
                            // nbOfFilteredObjects++;
                            // }
                            // }
                            // objects.clear();
                            // }
                            // }
                            criteria.setReadOnly(true);
                            ScrollableResults scroll = criteria.scroll();

                            // int i = 0;
                            while (scroll.next())
                            {
                                object = ObjectProviderCustoms.filter((P) scroll.get(0));
                                if (object != null)
                                {
                                    allIds.add(object.getId());
                                    // if (idOnly)
                                    // {
                                    // newSession.evict(object);
                                    // object = null;
                                    // i++;
                                    // // if (i % HibernateUtil.JDBC_BATCHSIZE == 0)
                                    // // if (i % 10 == 0)
                                    // // {
                                    // // newSession.flush();
                                    // // }
                                    // }
                                    // else
                                    // {
                                    result.add(object);
                                    // }
                                }
                                else
                                {
                                    nbOfFilteredObjects++;
                                }
                            }
                        }
                        else
                        {
                            result = criteria.list();
                        }
                    }
                    else
                    {
                        /*
                         * TODO Note: this bug may happen sometimes
                         * http://opensource.atlassian.com/projects/hibernate/browse/HHH-5188
                         */
                        result = criteria.list();
                    }

                    if (wantTotalSize)
                    {
                        setTotalSize(criteria, session, nbOfFilteredObjects);
                    }
                }
                else
                {
                    final Query query;

                    if (idOnly)
                    {
                        if (newSession == null)
                        {
                            /* new session without interceptor, in order not to mess up with existing session */
                            newSession = HibernateUtil.getSessionFactory().openSession();
                        }
                        query = getQuery(newSession);
                    }
                    else
                    {
                        query = getQuery(session);
                    }

                    if (query != null)
                    {
                        if (getLockMode() != LockMode.NONE && getLockMode() != null)
                        {
                            ((Criteria) query).setLockMode(getLockMode());
                        }

                        int nbOfFilteredObjects = 0;
                        P object;

                        /*
                         * Note: permission must be handled here instead of anywhere else because some projection (eg:
                         * on id only) may be applied and permission filtering may need more than id to work
                         */
                        if (isPermissionOn())
                        {
                            if (IBase.class.isAssignableFrom(context.getPersistedClass()))
                            {
                                result = SetUniqueList.decorate(new ArrayList<P>());
                                allIds = SetUniqueList.decorate(new ArrayList<Long>());
                                ScrollableResults scroll = query.scroll();
                                // int i = 0;
                                while (scroll.next())
                                {
                                    object = ObjectProviderCustoms.filter((P) scroll.get(0));
                                    if (object != null)
                                    {
                                        allIds.add(object.getId());
                                        // if (idOnly)
                                        // {
                                        // newSession.evict(object);
                                        // object = null;
                                        // i++;
                                        // if (i % HibernateUtil.JDBC_BATCHSIZE == 0)
                                        // {
                                        // newSession.flush();
                                        // }
                                        // }
                                        // else
                                        // {
                                        result.add(object);
                                        // }
                                    }
                                    else
                                    {
                                        nbOfFilteredObjects++;
                                    }
                                }
                            }
                            else
                            {
                                result = query.list();
                            }
                        }
                        else
                        {
                            /*
                             * TODO Note: this bug may happen sometimes
                             * http://opensource.atlassian.com/projects/hibernate/browse/HHH-5188
                             */
                            result = query.list();
                        }

                        if (wantTotalSize)
                        {
                            setTotalSize(criteria, session, nbOfFilteredObjects);
                        }
                    }
                }
            }
        }
        finally
        {
            if (idOnly)
            {
                newSession.flush();
                newSession.close();
            }
        }

        // Log.LOGGER.info("Query " + this.getClass().getName() + " executed in " + b.e() + " ms");
    }

    protected void setTotalSize(Criteria query, Session session, int nbFilteredObject) throws Hd3dException
    {
        if (result != null)
        {
            totalCount = result.size() - nbFilteredObject;
        }

        if ((context.isPaginated() || totalCount <= Conf.getMaxRecords()))
        {
            clearCriteria();
            criteria = getCriteriaForCount(session);
            criteria.setFirstResult(0);
            criteria.setProjection(Projections.rowCount());
            totalCount = ((Long) criteria.uniqueResult()).intValue() - nbFilteredObject;
        }
    }

    protected void setTotalSize(Criteria query, Session session) throws Hd3dException
    {
        setTotalSize(query, session, 0);
    }

    protected void setTotalSize(Query query, Session session) throws Hd3dException
    {
        if (result != null)
        {
            totalCount = result.size();
        }

        if (context.isPaginated() || totalCount <= Conf.getMaxRecords())
        {
            clearQuery();
            query = getQueryForCount(session);
            query.setFirstResult(0);
            ((Criteria) query).setProjection(Projections.rowCount());
            totalCount = ((Long) criteria.uniqueResult()).intValue();
        }
    }

    protected void setMaxResult(Criteria query)
    {
        // limit when retrieving records
        if (!context.isPaginated() && !noMaxResult)
        {
            query.setMaxResults(Conf.getMaxRecords());
        }
    }

    protected void setMaxResult(Query query)
    {
        // limit when retrieving records
        if (!context.isPaginated() && !noMaxResult)
        {
            query.setMaxResults(Conf.getMaxRecords());
        }
    }

    protected static void checkId(Long id, String message) throws Hd3dException
    {
        if (BaseH.isNotValidId(id))
        {
            throw new Hd3dPersistenceException(message);
        }
    }

    // protected void applyModifier(IQueryModifier modifier, DetachedCriteria subQuery, boolean forCount)
    // throws Hd3dQueryFilterException
    // {
    // if (modifier != null)
    // {
    // if (forCount)
    // modifier.applyModifierForCount(subQuery);
    // else
    // modifier.applyModifier(subQuery);
    // }
    // }

    protected void applyModifier(IQueryModifier modifier, Criteria subQuery, boolean forCount)
            throws Hd3dQueryFilterException
    {
        if (modifier != null)
        {
            if (forCount)
                modifier.applyModifierForCount(subQuery);
            else
                modifier.applyModifier(subQuery);
        }
    }

    public static boolean isToRemoveForCount(IQueryModifier modifier)
    {
        return modifier != null
                && (modifier instanceof OrderByQueryModifier || modifier instanceof MaxResultQueryModifier || modifier instanceof FirstResultQueryModifier);
    }

    protected void applyModifierForCount(IQueryModifier modifier, DetachedCriteria subQuery)
            throws Hd3dQueryFilterException
    {
        /* when counting total number of records, not point to apply commands such as order by */
        // if (!isToRemoveForCount(modifier))
        // {
        if (modifier != null)
            modifier.applyModifierForCount(subQuery);
        // }
    }

    protected void applyModifierForCount(IQueryModifier modifier, Criteria subQuery) throws Hd3dQueryFilterException
    {
        /* when counting total number of records, not point to apply commands such as order by */
        // if (!isToRemoveForCount(modifier))
        // {
        if (modifier != null)
            modifier.applyModifierForCount(subQuery);
        // }
    }

    protected class FilterApplyer implements Transformer<Criteria, Criteria>
    {
        CompositeQueryModifier modifier;

        public FilterApplyer(CompositeQueryModifier modifier)
        {
            this.modifier = modifier;
        }

        public Criteria transform(Criteria query)
        {
            try
            {
                applyModifier(modifier, query, false);
            }
            catch (Hd3dQueryFilterException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
            return query;
        }

        // public DetachedCriteria transform(DetachedCriteria subQuery)
        // {
        // try
        // {
        // applyModifier(modifier, subQuery, false);
        // }
        // catch (Hd3dQueryFilterException e)
        // {
        // Log.LOGGER.error(ExceptUtils.format(e));
        // }
        // return subQuery;
        // }
    }

    protected class FilterApplyerForCount implements Transformer<Criteria, Criteria>
    {
        CompositeQueryModifier modifier;

        public FilterApplyerForCount(CompositeQueryModifier modifier)
        {
            this.modifier = modifier;
        }

        public Criteria transform(Criteria query)
        {
            try
            {
                applyModifierForCount(modifier, query);
            }
            catch (Hd3dQueryFilterException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
            return query;
        }

        public DetachedCriteria transform(DetachedCriteria subQuery)
        {
            try
            {
                applyModifierForCount(modifier, subQuery);
            }
            catch (Hd3dQueryFilterException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
            return subQuery;
        }
    }

    protected class DefaultMaxResultSetter implements Transformer<Criteria, Criteria>
    {
        public Criteria transform(Criteria query)
        {
            // limit when retrieving records
            if (!context.isPaginated() && !noMaxResult)
            {
                query.setMaxResults(Conf.getMaxRecords());
            }

            return query;
        }
    }

    protected Transformer<Criteria, Criteria> getTransformer(Session session) throws Hd3dException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        CollectUtils.addAllIfNotEmpty(transformers, getCoreTransformers(session));
        CollectUtils.addAllIfNotEmpty(transformers, getTailingTransformers(session));

        return ChainedTransformer.getInstance(transformers.toArray(new Transformer[transformers.size()]));
    }

    protected Transformer<Criteria, Criteria> getTransformerForCount(Session session) throws Hd3dException
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();

        /* get rid of the order by, max result, first result */
        for (Transformer<Criteria, Criteria> aTransformer : CollectUtils.nullSafe(getCoreTransformers(session)))
        {
            if (aTransformer != null && !(aTransformer instanceof OrderCriteriaTransformer)
                    && !(aTransformer instanceof MaxCriteriaTransformer))
            {
                transformers.add(aTransformer);
            }
        }
        CollectUtils.addAllIfNotEmpty(transformers, getTailingTransformersForCount(session));

        return ChainedTransformer.getInstance(transformers.toArray(new Transformer[transformers.size()]));
    }

    protected Object buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
    {
        if (forCount)
            return (Criteria) weaveCriteriaForCount(session);
        else
            return (Criteria) weaveCriteria(session);
    }

    protected Object weaveCriteria(Session session, HibernatePersist<?, ?> persistor) throws Hd3dException
    {
        /*
         * A sub query is used to retrieve distinct ids first. Otherwise, pagination is applied before DISTINCT,
         * returning less elements than expected
         */
        // DetachedCriteria subCriteria = persistor.basicSubCriteria(session);
        // final FilterApplyer filterApplyer = new FilterApplyer(context.getFilter());
        // filterApplyer.transform(subCriteria);
        // final Criteria criteria = persistor.basicCriteria(session, subCriteria);
        final Criteria criteria = persistor.basicCriteria(session);
        getTransformer(session).transform(criteria);
        if (getProjectionList() != null)
        {
            criteria.setProjection(getProjectionList());
        }

        addACL(criteria, persistor.getPersistedClass());

        return criteria;
    }

    protected Object weaveCriteriaForCount(Session session) throws Hd3dException
    {
        return weaveCriteriaForCount(session, (HibernatePersist<?, ?>) context.getPersistor());
    }

    protected Object weaveCriteriaForCount(Session session, HibernatePersist<?, ?> persistor) throws Hd3dException
    {
        /*
         * A sub query is used to retrieve distinct ids first. Otherwise, pagination is applied before DISTINCT,
         * returning less elements than expected
         */
        // DetachedCriteria subCriteria = persistor.basicSubCriteria(session);
        // final FilterApplyerForCount filterApplyer = new FilterApplyerForCount(context.getFilter());
        // filterApplyer.transform(subCriteria);
        // final Criteria criteria = persistor.basicCriteria(session, subCriteria);
        final Criteria criteria = persistor.basicCriteria(session);
        getTransformerForCount(session).transform(criteria);
        if (getProjectionList() != null)
        {
            criteria.setProjection(getProjectionList());
        }

        addACL(criteria, persistor.getPersistedClass());

        return criteria;
    }

    protected Object weaveCriteria(Session session) throws Hd3dException
    {
        return weaveCriteria(session, (HibernatePersist<?, ?>) context.getPersistor());
    }

    abstract protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session) throws Hd3dException;

    protected List<Transformer<Criteria, Criteria>> getTailingTransformers(Session session)
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new FilterApplyer(context.getFilter()));
        transformers.add(new DefaultMaxResultSetter());
        return transformers;
    }

    protected List<Transformer<Criteria, Criteria>> getTailingTransformersForCount(Session session)
    {
        List<Transformer<Criteria, Criteria>> transformers = new ArrayList<Transformer<Criteria, Criteria>>();
        transformers.add(new FilterApplyerForCount(context.getFilter()));
        return transformers;
    }

}
