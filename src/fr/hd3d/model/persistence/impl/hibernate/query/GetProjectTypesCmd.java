package fr.hd3d.model.persistence.impl.hibernate.query;

import java.util.List;

import org.apache.commons.collections15.Transformer;
import org.hibernate.Criteria;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILProjectType;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.services.resources.ResourceContext;


public class GetProjectTypesCmd extends ListResultQuery<ILProjectType, IProjectType>
{
    public GetProjectTypesCmd(ResourceContext<ILProjectType, IProjectType> context)
    {
        super(context);
    }

    @Override
    protected List<Transformer<Criteria, Criteria>> getCoreTransformers(Session session)
            throws Hd3dPersistenceException
    {
        // TODO Auto-generated method stub
        return null;
    }
}
