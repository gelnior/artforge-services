package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.ISimple;


/**
 * Pool hibernate object needed for database persistance.
 * 
 * @author HD3D
 */
// @Entity
// @Table(name = "hd3d_simple")
@MappedSuperclass
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public abstract class SimpleH extends BaseH implements ISimple
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    protected String name;

    /*-------------
     * Constructors
     -------------*/
    public SimpleH()
    {}

    public SimpleH(Long id, Timestamp version, String name)
    {
        super(id, version);
        this.name = name;
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "name")
    @NotEmpty
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SimpleH other = (SimpleH) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
    }

    public String toString()
    {
        return name;
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
