package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILGraphicalAnnotation;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


@Entity
@Table(name = "hd3d_graphicalannotation")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class GraphicalAnnotationH extends BaseH implements IGraphicalAnnotation
{

    private static final long serialVersionUID = 1L;

    private static final String DATE_FIELD = "date";

    private String annotationSvg;
    private Long markIn;
    private Long markOut;
    private String comment;
    private IPerson author;
    private IProxy proxy;
    private IApprovalNote approvalNote;
    private Date date;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_graphicalannotation"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public GraphicalAnnotationH()
    {}

    public GraphicalAnnotationH(Long id, Timestamp version, String annotationSvg, Long markIn, Long markOut,
            String comment, IPerson author, IProxy proxy, Date date)
    {
        super(id, version);
        this.annotationSvg = annotationSvg;
        this.markIn = markIn;
        this.markOut = markOut;
        this.comment = comment;
        this.author = author;
        this.proxy = proxy;
        this.date = date;
    }

    public GraphicalAnnotationH(String annotationSvg, Long markIn, Long markOut, String comment, IPerson author,
            IProxy proxy, Date date)
    {
        this(null, null, annotationSvg, markIn, markOut, comment, author, proxy, date);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "graphicalannotation_svg")
    public String getAnnotationSvg()
    {
        return annotationSvg;
    }

    @Column(name = "graphicalannotation_markin")
    public Long getMarkIn()
    {
        return markIn;
    }

    @Column(name = "graphicalannotation_markout")
    public Long getMarkOut()
    {
        return markOut;
    }

    @Column(columnDefinition = "TEXT", name = "approvalnote_comment")
    public String getComment()
    {
        return comment;
    }

    @ManyToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "graphicalannotation_author")
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getAuthor()
    {
        return author;
    }

    @ManyToOne(targetEntity = ProxyH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "graphicalannotation_proxy")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProxy getProxy()
    {
        return proxy;
    }

    @ManyToOne(targetEntity = ApprovalNoteH.class)
    @JoinColumn(name = "graphicalannotation_approvalnote")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IApprovalNote getApprovalNote()
    {
        return approvalNote;
    }

    @Column(name = "graphicalannotation_date")
    public Date getDate()
    {
        return date;
    }

    public void setAnnotationSvg(String annotationSvg)
    {
        this.annotationSvg = annotationSvg;
    }

    public void setMarkIn(Long markIn)
    {
        this.markIn = markIn;
    }

    public void setMarkOut(Long markOut)
    {
        this.markOut = markOut;
    }

    public void setAuthor(IPerson author)
    {
        this.author = author;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setProxy(IProxy proxy)
    {
        this.proxy = proxy;
    }

    public void setApprovalNote(IApprovalNote approvalNote)
    {
        this.approvalNote = approvalNote;
    }

    /*------------------
     * equal & hashcode
     ------------------*/

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((annotationSvg == null) ? 0 : annotationSvg.hashCode());
        result = prime * result + ((approvalNote == null) ? 0 : approvalNote.hashCode());
        result = prime * result + ((author == null) ? 0 : author.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((markIn == null) ? 0 : markIn.hashCode());
        result = prime * result + ((markOut == null) ? 0 : markOut.hashCode());
        result = prime * result + ((proxy == null) ? 0 : proxy.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        GraphicalAnnotationH other = (GraphicalAnnotationH) obj;
        if (annotationSvg == null)
        {
            if (other.annotationSvg != null)
                return false;
        }
        else if (!annotationSvg.equals(other.annotationSvg))
            return false;
        if (approvalNote == null)
        {
            if (other.approvalNote != null)
                return false;
        }
        else if (!approvalNote.equals(other.approvalNote))
            return false;
        if (author == null)
        {
            if (other.author != null)
                return false;
        }
        else if (!author.equals(other.author))
            return false;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (date == null)
        {
            if (other.date != null)
                return false;
        }
        else if (!date.equals(other.date))
            return false;
        if (markIn == null)
        {
            if (other.markIn != null)
                return false;
        }
        else if (!markIn.equals(other.markIn))
            return false;
        if (markOut == null)
        {
            if (other.markOut != null)
                return false;
        }
        else if (!markOut.equals(other.markOut))
            return false;
        if (proxy == null)
        {
            if (other.proxy != null)
                return false;
        }
        else if (!proxy.equals(other.proxy))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILGraphicalAnnotation, IGraphicalAnnotation> defaultTranslator()
    {
        return Translators.graphicalannotation;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return comment;
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class GraphicalAnnotationComplexTypeProvider implements IComplexTypeProvider
    {
        public List<String> getValidEnum(String field)
        {
            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            return isValidTypeField(field) ? fr.hd3d.utils.StringUtils.parseDate(value) : null;
        }

        public boolean isValidTypeField(String field)
        {
            return DATE_FIELD.equals(field);
        }
    }
}
