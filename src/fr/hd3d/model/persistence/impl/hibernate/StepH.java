package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILStep;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


@Entity
@Indexed
@Table(name = "hd3d_step")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class StepH extends BaseH implements IStep
{
    private static final long serialVersionUID = 1L;

    private Long boundEntity;
    private String boundEntityName;
    private ITaskType taskType;
    private Boolean createTasknAsset = Boolean.FALSE;
    private Integer estimatedDuration = 0;
    private IBase entity = null;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_step"), @Parameter(name = "initial_value", value = "1"),
            @Parameter(name = "increment_size", value = "1"), @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public StepH()
    {}

    public StepH(Long id, Timestamp version, Long boundEntity, String entityName, ITaskType taskType,
            Boolean createTasknAsset, Integer estimatedDuration)
    {
        super(id, version);
        this.boundEntity = boundEntity;
        this.boundEntityName = entityName;
        this.taskType = taskType;
        setCreateTasknAsset(createTasknAsset);
        setEstimatedDuration(estimatedDuration);
    }

    public StepH(Long boundEntity, String entityName, ITaskType taskType, Boolean createTasknAsset,
            Integer estimatedDuration)
    {
        this(null, null, boundEntity, entityName, taskType, createTasknAsset, estimatedDuration);
    }

    public StepH(IBase object, ITaskType taskType, Boolean createTasknAsset, Integer estimatedDuration)
    {
        this(null, null, object.getId(), object.entityName(), taskType, createTasknAsset, estimatedDuration);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "step_boundentity")
    @Index(name = "step_boundentity_idx")
    public Long getBoundEntity()
    {
        return boundEntity;
    }

    @Column(name = "step_boundentityname")
    @Index(name = "step_boundentityname_idx")
    public String getBoundEntityName()
    {
        return boundEntityName;
    }

    @ManyToOne(targetEntity = TaskTypeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "step__tasktype")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public ITaskType getTaskType()
    {
        return taskType;
    }

    @Column(name = "step_create_task_n_asset")
    public Boolean getCreateTasknAsset()
    {
        return createTasknAsset;
    }

    @Column(name = "step_estimated_duration")
    public Integer getEstimatedDuration()
    {
        return estimatedDuration;
    }

    public void setCreateTasknAsset(Boolean createTasknAsset)
    {
        if (createTasknAsset != null)
        {
            this.createTasknAsset = createTasknAsset;
        }
    }

    public void setEstimatedDuration(Integer estimatedDuration)
    {
        if (estimatedDuration != null)
        {
            this.estimatedDuration = estimatedDuration;
        }
    }

    public void setBoundEntity(Long boundEntity)
    {
        this.boundEntity = boundEntity;
    }

    public void setBoundEntityName(String boundEntityName)
    {
        this.boundEntityName = boundEntityName;
    }

    public void setTaskType(ITaskType task)
    {
        this.taskType = task;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((boundEntity == null) ? 0 : boundEntity.hashCode());
        result = prime * result + ((boundEntityName == null) ? 0 : boundEntityName.hashCode());
        result = prime * result + ((taskType == null) ? 0 : taskType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        StepH other = (StepH) obj;
        if (boundEntity == null)
        {
            if (other.boundEntity != null)
                return false;
        }
        else if (!boundEntity.equals(other.boundEntity))
            return false;
        if (boundEntityName == null)
        {
            if (other.boundEntityName != null)
                return false;
        }
        else if (!boundEntityName.equals(other.boundEntityName))
            return false;
        if (taskType == null)
        {
            if (other.taskType != null)
                return false;
        }
        else if (!taskType.equals(other.taskType))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILStep, IStep> defaultTranslator()
    {
        return Translators.step;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        String duration = "0";
        if (this.estimatedDuration != null)
        {
            duration = this.estimatedDuration.toString();
        }
        return duration;
    }

    public IBase boundEntity() throws Hd3dException
    {
        IBase ret = entity;

        if (entity == null)
        {
            final IPersist<?> persistor = Persistors.getPersistor(getBoundEntityName());
            if (persistor != null)
            {
                entity = persistor.getById(getBoundEntity());
                ret = entity;
            }
        }
        return ret;
    }

    @Transient
    @Field(name = "wo_project_id", index = org.hibernate.search.annotations.Index.UN_TOKENIZED, store = Store.YES)
    public String getBoundEntityProjectIdString() throws Hd3dException
    {
        String ret = null;

        IProject project = RelatedProjectHandler.getProject(boundEntity());
        if (project != null)
        {
            ret = String.valueOf(project.getId());
        }

        return ret;
    }
    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
