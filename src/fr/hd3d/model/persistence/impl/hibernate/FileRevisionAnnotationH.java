package fr.hd3d.model.persistence.impl.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevisionAnnotation;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IFileRevisionAnnotation;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * 
 * @author try LAM
 * 
 */
@Entity
@Table(name = "hd3d_filerevision_annotation")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class FileRevisionAnnotationH extends BaseH implements IFileRevisionAnnotation
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Integer markIn;
    private Integer markOut;
    private String name;
    private String comment;
    private IFileRevision graphicData;
    private IFileRevision annotatedFile;
    private IPerson annotator;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_filerevisionannotation"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public FileRevisionAnnotationH()
    {}

    public FileRevisionAnnotationH(Long id, java.sql.Timestamp version, Integer markIn, Integer markOut, String name,
            String comment, IFileRevision graphicData, IFileRevision annotatedFile, IPerson annotator)
    {
        super(id, version);
        this.markIn = markIn;
        this.markOut = markOut;
        this.name = name;
        this.comment = comment;
        this.graphicData = graphicData;
        this.annotatedFile = annotatedFile;
        this.annotator = annotator;
    }

    public FileRevisionAnnotationH(Integer markIn, Integer markOut, String name, String comment,
            IFileRevision graphicData, IFileRevision annotatedFile, IPerson annotator)
    {
        this(null, null, markIn, markOut, name, comment, graphicData, annotatedFile, annotator);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "filerevision_annotation_markin")
    public Integer getMarkIn()
    {
        return markIn;
    }

    @Column(name = "filerevision_annotation_markout")
    public Integer getMarkOut()
    {
        return markOut;
    }

    @Column(name = "filerevision_annotation_name")
    public String getName()
    {
        return name;
    }

    @Column(name = "filerevision_annotation_comment")
    public String getComment()
    {
        return comment;
    }

    @ManyToOne(targetEntity = FileRevisionH.class)
    @JoinColumn(name = "playlistedit_graphicdata")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IFileRevision getGraphicData()
    {
        return graphicData;
    }

    @ManyToOne(targetEntity = FileRevisionH.class)
    @JoinColumn(name = "playlistedit_annotatedfile")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IFileRevision getAnnotatedFile()
    {
        return annotatedFile;
    }

    @ManyToOne(targetEntity = PersonH.class)
    @JoinColumn(name = "playlistedit_annotator")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getAnnotator()
    {
        return annotator;
    }

    public void setMarkIn(Integer markIn)
    {
        this.markIn = markIn;
    }

    public void setMarkOut(Integer markOut)
    {
        this.markOut = markOut;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setGraphicData(IFileRevision graphicData)
    {
        this.graphicData = graphicData;
    }

    public void setAnnotatedFile(IFileRevision annotatedFile)
    {
        this.annotatedFile = annotatedFile;
    }

    public void setAnnotator(IPerson annotator)
    {
        this.annotator = annotator;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((annotator == null) ? 0 : annotator.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((markIn == null) ? 0 : markIn.hashCode());
        result = prime * result + ((markOut == null) ? 0 : markOut.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        FileRevisionAnnotationH other = (FileRevisionAnnotationH) obj;
        if (annotator == null)
        {
            if (other.annotator != null)
                return false;
        }
        else if (!annotator.equals(other.annotator))
            return false;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (markIn == null)
        {
            if (other.markIn != null)
                return false;
        }
        else if (!markIn.equals(other.markIn))
            return false;
        if (markOut == null)
        {
            if (other.markOut != null)
                return false;
        }
        else if (!markOut.equals(other.markOut))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    @Override
    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public IBaseTranslator<ILFileRevisionAnnotation, IFileRevisionAnnotation> defaultTranslator()
    {
        return Translators.filerevisionannotation;
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("markIn", markIn).append("markOut", markOut).append("name", name).append("comment", comment);
    // b.append("graphicDataId", graphicData == null ? "null" : graphicData.getId());
    // b.append("annotatedFileId", annotatedFile == null ? "null" : annotatedFile.getId());
    // b.append("annotatorId", annotator == null ? "null" : annotator.getId());
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    // No ComplexTypeProvider
}
