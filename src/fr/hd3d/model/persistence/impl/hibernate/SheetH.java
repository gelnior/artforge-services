package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILSheet;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Table(name = "hd3d_sheet")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class SheetH extends BaseH implements ISheet
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String TYPE = "type";

    private String name;
    private String description;
    private IProject project;
    private String boundClassName;
    private List<IItemGroup> itemGroups;
    private ESheetType type;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_sheet"), @Parameter(name = "initial_value", value = "1"),
            @Parameter(name = "increment_size", value = "1"), @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public SheetH()
    {}

    public SheetH(Long id, Timestamp version, String name, String description, IProject project, String boundclassname,
            List<IItemGroup> itemgroups)
    {
        super(id, version);
        this.name = name;
        this.description = description;
        this.project = project;
        this.boundClassName = boundclassname;
        setItemGroups(itemgroups);
    }

    public SheetH(String name, String description, IProject project, String boundclassname, List<IItemGroup> itemgroups)
    {
        this(null, null, name, description, project, boundclassname, itemgroups);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "sheet_name", nullable = false)
    @NotEmpty
    public String getName()
    {
        return name;
    }

    @Column(name = "sheet_description")
    public String getDescription()
    {
        return description;
    }

    @ManyToOne(targetEntity = ProjectH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "sheet_project_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    @Column(name = "sheet_boundclassname", nullable = false)
    @Index(name = "sheet_boundclassname_idx")
    @NotEmpty
    public String getBoundClassName()
    {
        return boundClassName;
    }

    @OneToMany(targetEntity = ItemGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "sheet")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @IndexColumn(name = "itemgroup_index", base = 0)
    @org.hibernate.annotations.OrderBy(clause = "itemgroup_index")
    @Fetch(value = FetchMode.SELECT)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public List<IItemGroup> getItemGroups()
    {
        if (itemGroups == null)
            itemGroups = new ArrayList<IItemGroup>();
        return itemGroups;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "sheet_type")
    public ESheetType getType()
    {
        return type;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setProject(IProject project)
    {
        this.project = project;
    }

    public void setBoundClassName(String boundclassname)
    {
        this.boundClassName = boundclassname;
    }

    public void setItemGroups(List<IItemGroup> itemgroups)
    {
        if (itemgroups == null)
            this.itemGroups = new ArrayList<IItemGroup>();

        // must set the index manually (seems hibernate does not work well)
        // note: do this first...
        else
        {
            for (IItemGroup g : itemgroups)
            {
                if (g == null)
                    continue;
                if (g.getIndex() == null || g.getIndex().equals(0))
                {
                    g.setIndex(itemgroups.indexOf(g));
                }
                g.setSheet(this);
            }
            // ...and let hibernate synchronize
            this.itemGroups = itemgroups;
        }
    }

    public void setType(ESheetType type)
    {
        this.type = type;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((boundClassName == null) ? 0 : boundClassName.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SheetH other = (SheetH) obj;
        if (boundClassName == null)
        {
            if (other.boundClassName != null)
                return false;
        }
        else if (!boundClassName.equals(other.boundClassName))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILSheet, ISheet> defaultTranslator()
    {
        return Translators.sheet;
    }

    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        // cascade disable
        Collection<IItemGroup> itemGroups = CollectUtils.selectNotNull(getItemGroups());
        Persistors.itemgroup.disableCollection(itemGroups);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public void addItemGroup(IItemGroup itemGroup)
    {
        if (!getItemGroups().contains(itemGroup))
        {
            getItemGroups().add(itemGroup);
            // must set the index manually (seems hibernate does not work well)
            itemGroup.setIndex(getItemGroups().indexOf(itemGroup));
        }
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("name", name);
    // b.append("projectId", project == null ? "null" : project.getId());
    // b.append("boundClassName", boundClassName);
    // b.append("itemGroupIds", StringUtils.join(CollectUtils.getIds(getItemGroups()), ','));
    // b.append("type", type);
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class SheetComplexTypeProvider implements IComplexTypeProvider
    {
        public List<String> getValidEnum(String field)
        {
            if (TYPE.equals(field))
                return EnumUtils.getEnumStringValues(ESheetType.class);
            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            if (!isValidTypeField(field))
                return null;

            if (TYPE.equals(field))
                return EnumUtils.getEnumValueIgnoreCase(ESheetType.class, value);
            else
                return null;
        }

        public boolean isValidTypeField(String field)
        {
            return TYPE.equals(field);
        }
    }

}
