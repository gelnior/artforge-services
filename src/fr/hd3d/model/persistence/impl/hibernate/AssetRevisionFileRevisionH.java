// package fr.hd3d.model.persistence.impl.hibernate;
//
// import java.util.List;
//
// import javax.persistence.Column;
// import javax.persistence.Entity;
// import javax.persistence.GeneratedValue;
// import javax.persistence.Id;
// import javax.persistence.Table;
// import javax.persistence.Transient;
//
// import org.apache.commons.lang.builder.ToStringBuilder;
// import org.hibernate.annotations.Filter;
// import org.hibernate.annotations.FilterDef;
// import org.hibernate.annotations.GenericGenerator;
// import org.hibernate.annotations.OptimisticLockType;
// import org.hibernate.annotations.ParamDef;
// import org.hibernate.annotations.Parameter;
//
// import fr.hd3d.exception.Hd3dException;
// import fr.hd3d.model.lightweight.ILAssetRevisionFileRevision;
// import fr.hd3d.model.persistence.IAssetRevision;
// import fr.hd3d.model.persistence.IAssetRevisionFileRevision;
// import fr.hd3d.model.persistence.IFileRevision;
// import fr.hd3d.model.persistence.Persistors;
// import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
// import fr.hd3d.model.translator.IBaseTranslator;
// import fr.hd3d.model.translator.Translators;
// import fr.hd3d.services.security.customs.Customs;
// import fr.hd3d.services.security.utils.RESTPathUtil;
//
//
// /**
// *
// * @author try LAM
// *
// */
// @Entity
// @Table(name = "hd3d_assetrevisionfilerevision")
// @FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters =
// @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
// @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
// @org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
// public class AssetRevisionFileRevisionH extends BaseH implements IAssetRevisionFileRevision
// {
// /**
// *
// */
// private static final long serialVersionUID = 1L;
//
// private String assetKey;
// private String fileKey;
//
// private String assetVariation;
// private String fileVariation;
//
// private Integer assetRevision;
// private Integer fileRevision;
//
// /*-------------
// * id override
// -------------*/
// @Id
// @GeneratedValue(generator = "seq_gen")
// @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
// @Parameter(name = "sequence_name", value = "seq_assetrevisionfilerevision"),
// @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
// @Parameter(name = "optimizer", value = "pooled") })
// public Long getId()
// {
// return id;
// }
//
// /*-------------
// * Constructors
// -------------*/
// public AssetRevisionFileRevisionH()
// {
// super();
// }
//
// public AssetRevisionFileRevisionH(Long id, java.sql.Timestamp version, String assetKey, String fileKey,
// String assetVariation, String fileVariation, Integer assetRevision, Integer fileRevision)
// {
// super(id, version);
// this.assetKey = assetKey;
// this.fileKey = fileKey;
// this.assetVariation = assetVariation;
// this.fileVariation = fileVariation;
// this.assetRevision = assetRevision;
// this.fileRevision = fileRevision;
// }
//
// public AssetRevisionFileRevisionH(String assetKey, String fileKey, String assetVariation, String fileVariation,
// Integer assetRevision, Integer fileRevision)
// {
// this(null, null, assetKey, fileKey, assetVariation, fileVariation, assetRevision, fileRevision);
// }
//
// public AssetRevisionFileRevisionH(IAssetRevision asset, IFileRevision file)
// {
// super(null, null);
// this.setAsset(asset);
// this.setFile(file);
// }
//
// /*------------------
// * Getters & Setters
// ------------------*/
// @Column(name = "assetrevisionfilerevision_assetkey")
// public String getAssetKey()
// {
// return assetKey;
// }
//
// @Column(name = "assetrevisionfilerevision_filekey")
// public String getFileKey()
// {
// return fileKey;
// }
//
// @Column(name = "assetrevisionfilerevision_assetvariation")
// public String getAssetVariation()
// {
// return assetVariation;
// }
//
// @Column(name = "assetrevisionfilerevision_filevariation")
// public String getFileVariation()
// {
// return fileVariation;
// }
//
// @Column(name = "assetrevisionfilerevision_assetrevision")
// public Integer getAssetRevision()
// {
// return assetRevision;
// }
//
// @Column(name = "assetrevisionfilerevision_filerevision")
// public Integer getFileRevision()
// {
// return fileRevision;
// }
//
// public void setAssetKey(final String assetKey)
// {
// this.assetKey = assetKey;
// }
//
// public void setFileKey(final String fileKey)
// {
// this.fileKey = fileKey;
// }
//
// public void setAssetVariation(final String assetVariation)
// {
// this.assetVariation = assetVariation;
// }
//
// public void setFileVariation(final String fileVariation)
// {
// this.fileVariation = fileVariation;
// }
//
// public void setAssetRevision(final Integer assetRevision)
// {
// this.assetRevision = assetRevision;
// }
//
// public void setFileRevision(final Integer fileRevision)
// {
// this.fileRevision = fileRevision;
// }
//
// /*------------------
// * equal & hashcode
// ------------------*/
// @Override
// public int hashCode()
// {
// final int prime = 31;
// int result = super.hashCode();
// result = prime * result + ((assetKey == null) ? 0 : assetKey.hashCode());
// result = prime * result + ((assetRevision == null) ? 0 : assetRevision.hashCode());
// result = prime * result + ((assetVariation == null) ? 0 : assetVariation.hashCode());
// result = prime * result + ((fileKey == null) ? 0 : fileKey.hashCode());
// result = prime * result + ((fileRevision == null) ? 0 : fileRevision.hashCode());
// result = prime * result + ((fileVariation == null) ? 0 : fileVariation.hashCode());
// return result;
// }
//
// @Override
// public boolean equals(Object obj)
// {
// if (this == obj)
// return true;
// if (!super.equals(obj))
// return false;
// if (getClass() != obj.getClass())
// return false;
// AssetRevisionFileRevisionH other = (AssetRevisionFileRevisionH) obj;
// if (assetKey == null)
// {
// if (other.assetKey != null)
// return false;
// }
// else if (!assetKey.equals(other.assetKey))
// return false;
// if (assetRevision == null)
// {
// if (other.assetRevision != null)
// return false;
// }
// else if (!assetRevision.equals(other.assetRevision))
// return false;
// if (assetVariation == null)
// {
// if (other.assetVariation != null)
// return false;
// }
// else if (!assetVariation.equals(other.assetVariation))
// return false;
// if (fileKey == null)
// {
// if (other.fileKey != null)
// return false;
// }
// else if (!fileKey.equals(other.fileKey))
// return false;
// if (fileRevision == null)
// {
// if (other.fileRevision != null)
// return false;
// }
// else if (!fileRevision.equals(other.fileRevision))
// return false;
// if (fileVariation == null)
// {
// if (other.fileVariation != null)
// return false;
// }
// else if (!fileVariation.equals(other.fileVariation))
// return false;
// return true;
// }
//
// /*------------------
// * Transient methods
// ------------------*/
// @Transient
// public boolean isTaskable()
// {
// return true;
// }
//
// @Transient
// public boolean isApprovable()
// {
// return true;
// }
//
// @Transient
// public IAssetRevision getAssetRevisionObject() throws Hd3dException
// {
// String[] properties = new String[] { "key", "variation", "revision" };
// Object[] values = new Object[] { assetKey, assetVariation, assetRevision };
// return (IAssetRevision) Persistors.assetrevision.getByValues(null, properties, values, "AND");
// }
//
// @Transient
// public IFileRevision getFileRevisionObject() throws Hd3dException
// {
// return getFileRevisionObject(true);
// }
//
// public IFileRevision getFileRevisionObject(boolean permCheck) throws Hd3dException
// {
// String[] properties = new String[] { "key", "variation", "revision" };
// Object[] values = new Object[] { fileKey, fileVariation, fileRevision };
//
// List<IFileRevision> filerevs;
// if (permCheck)
// {
// filerevs = Persistors.filerevision.getByValues(null, properties, values, "AND");
// }
// else
// {
// filerevs = Persistors.filerevision.getByValues_NoPermCheck(null, properties, values, "AND");
// }
// return filerevs.get(0);
// }
//
// @Override
// public void accept(BasePersistenceVisitor visitor) throws Hd3dException
// {
// visitor.visit(this);
// }
//
// public IBaseTranslator<ILAssetRevisionFileRevision, IAssetRevisionFileRevision> defaultTranslator()
// {
// return Translators.assetrevisionfilerevision;
// }
//
// protected boolean accept(Customs customs)
// {
// return customs.visit(this);
// }
//
// public String defaultPath()
// {
// return RESTPathUtil.getDefaultPath(this);
// }
//
// /**
// * Initialize asset fields (key, revision, variation) with information from asset given in parameter.
// *
// * @param asset
// * The asset needed to update asset fields.
// */
// public final void setAsset(IAssetRevision asset)
// {
// this.setAssetKey(asset.getKey());
// this.setAssetRevision(asset.getRevision());
// this.setAssetVariation(asset.getVariation());
// }
//
// /**
// * Initialize file fields (key, revision, variation) with information from file given in parameter.
// *
// * @param file
// * The file needed to update file fields.
// */
// public final void setFile(IFileRevision file)
// {
// this.setFileKey(file.getKey());
// this.setFileRevision(file.getRevision());
// this.setFileVariation(file.getVariation());
// }
//
// public String toString()
// {
// return new ToStringBuilder(this).appendSuper(super.toString()).append("assetKey", assetKey).append("fileKey",
// fileKey).append("assetVariation", assetVariation).append("fileVariation", fileVariation).append(
// "assetRevision", assetRevision).append("fileRevision", fileRevision).toString();
// }
//
// /*-------------------------------------------
// * ComplexTypeProvider (only for Enum & Date)
// -------------------------------------------*/
// }
