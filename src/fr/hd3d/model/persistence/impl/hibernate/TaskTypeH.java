/**
 * 
 */
package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTaskType;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


/**
 * @author thomas-eskenazi
 * 
 */
@Entity
@Table(name = "hd3d_task_type")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class TaskTypeH extends BaseH implements ITaskType
{
    private static final long serialVersionUID = 1L;

    private IProject project;// may be null
    private String color;
    private String name;
    private String description;
    private String entityName;
    private List<IApprovalNote> approvalNotes;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_tasktype"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public TaskTypeH()
    {}

    public TaskTypeH(Long id, Timestamp version, IProject project, String color, String name, String description)
    {
        super(id, version);
        this.project = project;
        this.color = color;
        this.name = name;
        this.description = description;
    }

    public TaskTypeH(IProject project, String color, String name, String description)
    {
        this(null, null, project, color, name, description);
    }

    public TaskTypeH(IProject project, String color, String name, String description, String entityName)
    {
        this(null, null, project, color, name, description);
        this.entityName = entityName;
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "task_type_color")
    public String getColor()
    {
        return color;
    }

    @Column(name = "task_type_description")
    public String getDescription()
    {
        return description;
    }

    @Column(name = "task_type_name", nullable = false)
    @org.hibernate.annotations.Index(name = "type_name_idx")
    @NotEmpty
    public String getName()
    {
        return name;
    }

    @ManyToOne(targetEntity = ProjectH.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "task_type__project_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    @Column(name = "task_type_entityname")
    @Index(name = "task_type_entityname_idx")
    public String getEntityName()
    {
        return entityName;
    }

    @OneToMany(targetEntity = ApprovalNoteTypeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "taskType")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @Fetch(value = FetchMode.SELECT)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public List<IApprovalNote> getApprovalNotes()
    {
        if (approvalNotes == null)
            approvalNotes = new ArrayList<IApprovalNote>();
        return approvalNotes;
    }

    public void setApprovalNotes(List<IApprovalNote> approvalNotes)
    {
        this.approvalNotes = approvalNotes;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setProject(IProject project)
    {
        this.project = project;
    }

    public void setEntityName(String entityName)
    {
        this.entityName = entityName;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        TaskTypeH other = (TaskTypeH) obj;
        if (color == null)
        {
            if (other.color != null)
                return false;
        }
        else if (!color.equals(other.color))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public void disable() throws Hd3dException
    {
        super.disable();

        /* cannot disable this tasktype if referenced */
        checkReference(Persistors.task.getByValue_NoPermCheck("taskType", this));
        checkReference(Persistors.step.getByValue_NoPermCheck("taskType", this));
        checkReference(Persistors.approvalnotetype.getByValue_NoPermCheck("taskType", this));
        checkReference(Persistors.assetrevision.getByValue_NoPermCheck("taskType", this));
        checkReference(Persistors.taskGroup.getByValue_NoPermCheck("taskType", this));
    }

    private void checkReference(List<? extends IBase> list) throws Hd3dException
    {
        if (CollectUtils.isNotEmpty(list))
        {
            throw new Hd3dException("Cannot disable Tasktype because it is referenced by " + list.get(0).entityName(),
                    false);
        }
    }

    public IBaseTranslator<ILTaskType, ITaskType> defaultTranslator()
    {
        return Translators.taskType;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("projectId", project == null ? "null" : project.getId());
    // b.append("color", color).append("name", name);
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
