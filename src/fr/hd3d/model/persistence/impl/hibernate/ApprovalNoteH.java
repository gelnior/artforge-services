package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


@Entity
@Table(name = "hd3d_approvalnote")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ApprovalNoteH extends BaseH implements IApprovalNote
{
    private static final long serialVersionUID = 1L;

    private static final String APPROVAL_DATE_FIELD = "approvalDate";

    private Long boundEntity;
    private String boundEntityName;
    private Date approvalDate;
    private String status;
    private String comment;
    private IPerson approver;
    private Set<IFileRevision> filerevisions;
    private String type;
    private IApprovalNoteType approvalNoteType;
    private String media;
    private Set<IGraphicalAnnotation> graphicalAnnotations;
    private boolean isUpdatedByTask = false;

    public static long getSerialVersionUID()
    {
        return serialVersionUID;
    }

    @Transient
    public boolean isUpdatedByTask()
    {
        return isUpdatedByTask;
    }

    public void setUpdatedByTask(boolean isUpdatedByTask)
    {
        this.isUpdatedByTask = isUpdatedByTask;
    }

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_approvalnote"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ApprovalNoteH()
    {}

    public ApprovalNoteH(Long id, Timestamp version, Date date, String status, String comment, IPerson approver,
            Long boundEntity, String entityName, String type, IApprovalNoteType approvalNoteType, String media)
    {
        super(id, version);
        this.approvalDate = date;
        this.status = status;
        this.comment = comment;
        this.approver = approver;
        this.boundEntity = boundEntity;
        this.boundEntityName = entityName;
        this.type = type;
        this.approvalNoteType = approvalNoteType;
        this.media = media;
    }

    public ApprovalNoteH(Date date, String status, String comment, IPerson approver, Long boundEntity,
            String entityName, String type, IApprovalNoteType approvalNoteType, String media)
    {
        this(null, null, date, status, comment, approver, boundEntity, entityName, type, approvalNoteType, media);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "approvalnote_boundentity")
    @Index(name = "boundentity_idx")
    public Long getBoundEntity()
    {
        return boundEntity;
    }

    @Column(name = "approvalnote_boundentityname")
    @Index(name = "boundentityname_idx")
    public String getBoundEntityName()
    {
        return boundEntityName;
    }

    @Column(name = "approvalnote_approvaldate")
    @Index(name = "approvaldate_idx")
    public Date getApprovalDate()
    {
        return approvalDate;
    }

    @Column(name = "approvalnote_status")
    @Index(name = "status_idx")
    public String getStatus()
    {
        return status;
    }

    @Column(columnDefinition = "TEXT", name = "approvalnote_comment")
    public String getComment()
    {
        return comment;
    }

    @ManyToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "approvalnote_approver")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getApprover()
    {
        return approver;
    }

    @ManyToMany(targetEntity = FileRevisionH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    // @ManyToOne(targetEntity = FileRevisionH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch =
    // FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_approvalnote_filerevision", joinColumns = @JoinColumn(name = "approvalnote_filerevision"), inverseJoinColumns = @JoinColumn(name = "filerevision_approvalnote"))
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public Set<IFileRevision> getFilerevisions()
    {
        if (filerevisions == null)
            filerevisions = new HashSet<IFileRevision>();
        return filerevisions;
    }

    @Column(name = "approvalnote_type")
    public String getType()
    {
        return type;
    }

    @ManyToOne(targetEntity = ApprovalNoteTypeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "approvalnote_approvalnotetype")
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IApprovalNoteType getApprovalNoteType()
    {
        return approvalNoteType;
    }

    @Column(name = "approvalnote_media")
    public String getMedia()
    {
        return media;
    }

    public void setBoundEntity(Long boundEntity)
    {
        this.boundEntity = boundEntity;
    }

    public void setBoundEntityName(String boundEntityName)
    {
        this.boundEntityName = boundEntityName;
    }

    public void setApprovalDate(Date approvalDate)
    {
        this.approvalDate = approvalDate;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setApprover(IPerson approver)
    {
        this.approver = approver;
    }

    public void setFilerevisions(Set<IFileRevision> filerevisions)
    {
        this.filerevisions = filerevisions;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setApprovalNoteType(IApprovalNoteType approvalNoteType)
    {
        this.approvalNoteType = approvalNoteType;
    }

    public void setMedia(String media)
    {
        this.media = media;
    }

    @OneToMany(targetEntity = GraphicalAnnotationH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "approvalNote")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Fetch(value = FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IGraphicalAnnotation> getGraphicalAnnotations()
    {
        if (graphicalAnnotations == null)
            graphicalAnnotations = new HashSet<IGraphicalAnnotation>();
        return graphicalAnnotations;
    }

    public void setGraphicalAnnotations(Set<IGraphicalAnnotation> graphicalAnnotations)
    {
        this.graphicalAnnotations = graphicalAnnotations;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((approvalDate == null) ? 0 : approvalDate.hashCode());
        result = prime * result + ((approver == null) ? 0 : approver.hashCode());
        result = prime * result + ((boundEntity == null) ? 0 : boundEntity.hashCode());
        result = prime * result + ((boundEntityName == null) ? 0 : boundEntityName.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((filerevisions == null) ? 0 : filerevisions.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ApprovalNoteH other = (ApprovalNoteH) obj;
        if (approvalDate == null)
        {
            if (other.approvalDate != null)
                return false;
        }
        else if (!approvalDate.equals(other.approvalDate))
            return false;
        if (approver == null)
        {
            if (other.approver != null)
                return false;
        }
        else if (!approver.equals(other.approver))
            return false;
        if (boundEntity == null)
        {
            if (other.boundEntity != null)
                return false;
        }
        else if (!boundEntity.equals(other.boundEntity))
            return false;
        if (boundEntityName == null)
        {
            if (other.boundEntityName != null)
                return false;
        }
        else if (!boundEntityName.equals(other.boundEntityName))
            return false;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (filerevisions == null)
        {
            if (other.filerevisions != null)
                return false;
        }
        else if (!filerevisions.equals(other.filerevisions))
            return false;
        if (status == null)
        {
            if (other.status != null)
                return false;
        }
        else if (!status.equals(other.status))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILApprovalNote, IApprovalNote> defaultTranslator()
    {
        return Translators.approvalnote;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return status + '-' + boundEntity;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("boundEntity", boundEntity).append("boundEntityName", boundEntityName).append("approvalDate",
    // approvalDate).append("status", status);
    // b.append("approverId", approver == null ? "null" : approver.getId());
    // b.append("filerevisionId", filerevision == null ? "null" : filerevision.getId());
    // b.append("type", type);
    // b.append("approvalNoteTypeId", approvalNoteType == null ? "null" : approvalNoteType.getId());
    // b.append("media", media);
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class ApprovalNoteComplexTypeProvider implements IComplexTypeProvider
    {
        public List<String> getValidEnum(String field)
        {
            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            return isValidTypeField(field) ? fr.hd3d.utils.StringUtils.parseDate(value) : null;
        }

        public boolean isValidTypeField(String field)
        {
            return APPROVAL_DATE_FIELD.equals(field);
        }
    }

    public List<IAssetRevision> assetRevisions() throws Hd3dException
    {
        List<IAssetRevision> assetRevisions;

        if (this.approvalNoteType != null && this.approvalNoteType.getTaskType() != null)
        {
            String[] properties = { "assetRevisionGroups.criteria", "assetRevisionGroups.value", "taskType.id" };
            Object[] values = { this.boundEntityName, String.valueOf(this.boundEntity),
                    this.approvalNoteType.getTaskType().getId() };
            assetRevisions = Persistors.assetrevision.getByValues(null, properties, values, "AND");
        }
        else
        {
            assetRevisions = Collections.emptyList();
        }

        return assetRevisions;
    }
}
