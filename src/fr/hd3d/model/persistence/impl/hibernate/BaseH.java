package fr.hd3d.model.persistence.impl.hibernate;

import static fr.hd3d.utils.CollectUtils.getIds;
import static org.apache.commons.collections15.CollectionUtils.collect;
import static org.apache.commons.collections15.CollectionUtils.find;
import static org.apache.commons.collections15.CollectionUtils.select;
import static org.apache.commons.collections15.CollectionUtils.selectRejected;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.persistence.Column;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.collections15.set.SynchronizedSet;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Resolution;
import org.hibernate.search.annotations.Store;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.lightweight.impl.LTag;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.Queries;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.DynMetaDataValueHandler;
import fr.hd3d.model.persistence.impl.hibernate.visitor.delete.PostDeleteVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.delete.PreDeleteVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.persist.PostPersistVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.persist.PrePersistVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.read.PostReadVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.read.PreReadVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.update.PostUpdateVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.update.PreUpdateVisitor;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.customs.CustomsVisitor;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.predicate.EqIdPredicate;
import fr.hd3d.utils.predicate.MatchClassDynMetaDataTypePredicate;
import fr.hd3d.utils.predicate.MatchTagParentIdsPredicate;
import fr.hd3d.utils.transformer.GetEntityTaskLinkTaskTransformer;
import fr.hd3d.utils.transformer.GetTagTransformer;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@MappedSuperclass
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
// Note about inheritance strategy: JOIN is too slow and SINGLE is too rigid
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public abstract class BaseH implements IBase
{
    private static final long serialVersionUID = 1L;

    protected Long id;
    protected java.sql.Timestamp version;
    protected String internalUUID;
    protected Integer internalStatus = Const.INTERNALSTATUS_ENABLED;
    /* tags */
    protected Set<TagParent> tagParents;
    protected Set<LTag.LTagParentIdType> tagParentsIdType;

    /* key=class name, value=List of related ClassDynMetaDataTypes */
    public final static org.apache.commons.collections15.multimap.MultiHashMap<String, IClassDynMetaDataType> CLASSDYNMETADATATYPES_MAP = new MultiHashMap<String, IClassDynMetaDataType>();
    public final static Set<String> DYNVALUES_TO_CREATE_SET = new CopyOnWriteArraySet<String>();

    /*-------------
     * Constructors
     -------------*/
    protected BaseH()
    {
        internalUUID = getUUID();
        this.tagParents = SynchronizedSet.decorate(new HashSet<TagParent>());
    }

    protected BaseH(final Long id, final Timestamp version)
    {
        this.id = id;
        this.version = version;
        this.internalUUID = getUUID();
        this.tagParents = SynchronizedSet.decorate(new HashSet<TagParent>());
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    /*-
     * IMPORTANT NOTE: getId() method must be @Transient, though it is abstract. Otherwise, Hibernate will create one
     * column named "id", and in sub-classes, implementation of getId() must be annotated @Column with a different name.
     * This results in 2 id columns.
     * 
     * Id generation: EACH ENTITY MUST HAVE ITS OWN GENERATOR
     * 
     * Note:
     * -MySQL does not support GenerationType.SEQUENCE.
     * -"@GeneratedValue(strategy = GenerationType.TABLE)" only
     * without generator does not generate table, thus every time the server restarts, there is a gap in ids (30 000).
     * -GenerationType.AUTO in MySQL uses IDENTITY, but IDENTITY is not supported for
     * subclasses
     */
    @Transient
    public abstract Long getId();

    @Version
    @Field(index = Index.TOKENIZED, store = Store.YES)
    /* Note: @DateBridge stores in UTC format (with an offset compared to local time) */
    @DateBridge(resolution = Resolution.SECOND)
    public java.sql.Timestamp getVersion()
    {
        return version;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public void setVersion(final java.sql.Timestamp version)
    {
        this.version = version;
    }

    @Column(name = "internalUUID", updatable = false)
    @Field(index = Index.UN_TOKENIZED, store = Store.YES, name = "internaluuid")
    public String getInternalUUID()
    {
        return internalUUID;
    }

    public void setInternalUUID(final String uuid)
    {
        this.internalUUID = uuid;
    }

    @Column(name = "internalStatus")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES, name = "internalstatus")
    @org.hibernate.annotations.Index(name = "internal_status_idx")
    public Integer getInternalStatus()
    {
        return internalStatus;
    }

    public void setInternalStatus(final Integer internalStatus)
    {
        this.internalStatus = internalStatus;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((internalUUID == null) ? 0 : internalUUID.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BaseH other = (BaseH) obj;
        if (internalUUID == null)
        {
            if (other.internalUUID != null)
                return false;
        }
        else if (!internalUUID.equals(other.internalUUID))
            return false;
        if (version == null)
        {
            if (other.version != null)
                return false;
        }
        else if (!version.equals(other.version))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    /*-
     *  Implementation note: the subclass implementation of this method may:
     * -return a hardcoded true or false. The main benefit is DB consistency 
     * -return true/false from configuration file. The main drawback may be DB inconsistency: one day an entity is
     * taskable with tasks in DB, and later no more taskable, with still tasks in DB 
     */
    @Transient
    public abstract boolean isTaskable();

    @Transient
    public abstract boolean isApprovable();

    @Transient
    public static boolean isValidId(final Long id)
    {
        return id != null && Const.ID_NULL.compareTo(id) < 0;
    }

    @Transient
    public static boolean isNotValidId(final Long id)
    {
        return !isValidId(id);
    }

    public static boolean areValidIds(final Iterable<Long> ids)
    {
        boolean ok = false;
        if (CollectUtils.isNotEmpty(ids))
        {
            ok = true;
            for (Long id : ids)
            {
                ok &= isValidId(id);
            }
        }
        return ok;
    }

    public String entityName()
    {
        return EntitiesMaps.withClass(this.getClass()).getEntityName();
    }

    /**
     * Note: since there is no easy and safe way to find out the Type given the parameter, better pass it as an argument
     * 
     * @throws Hd3dPersistenceException
     */
    public <T extends IBase> void merge(final Collection<T> coll, final Collection<Long> ids, final Class<T> clazz)
            throws Hd3dException
    {
        CollectUtils.merge(coll, ids, clazz);
    }

    public static void init() throws Hd3dException
    {
        DynMetaDataValueHandler.loadClassDynMetaDataTypes();

        DYNVALUES_TO_CREATE_SET.addAll(Queries.getDynMetaDataValuesToCreate_ParentTypes());
    }

    public void completePrePersistOrUpdate()
    {}

    public static void shutdown()
    {
        CLASSDYNMETADATATYPES_MAP.clear();
        DYNVALUES_TO_CREATE_SET.clear();
    }

    public void doPreRead() throws Hd3dException
    {
        accept(new PreReadVisitor());
    }

    public void doPostRead() throws Hd3dException
    {
        accept(new PostReadVisitor());
    }

    public void doPrePersist() throws Hd3dException
    {
        completePrePersistOrUpdate();
        accept(new PrePersistVisitor());
    }

    public void doPostPersist() throws Hd3dException
    {
        accept(new PostPersistVisitor());
    }

    public <L extends ILBase> void doPreUpdate(final L light) throws Hd3dException
    {
        completePrePersistOrUpdate();
        accept(new PreUpdateVisitor(light));
    }

    public void doPreUpdate() throws Hd3dException
    {
        completePrePersistOrUpdate();
        accept(new PreUpdateVisitor());
    }

    public <L extends ILBase> void doPostUpdate(final L light) throws Hd3dException
    {
        accept(new PostUpdateVisitor());
    }

    public void doPostUpdate() throws Hd3dException
    {
        accept(new PostUpdateVisitor());
    }

    public void doPreDelete() throws Hd3dException
    {
        accept(new PreDeleteVisitor());
    }

    public void doPostDelete() throws Hd3dException
    {
        accept(new PostDeleteVisitor());
    }

    public void accept(final BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    // -------------------------------------------------
    //
    // permission stuffs
    //
    // -------------------------------------------------
    public boolean canRead()
    {
        return accept(new CustomsVisitor(Customs.READ));
    }

    public boolean canUpdate()
    {
        return accept(new CustomsVisitor(Customs.UPDATE));
    }

    public boolean canDelete()
    {
        return accept(new CustomsVisitor(Customs.DELETE));
    }

    public boolean canPersist()
    {
        return accept(new CustomsVisitor(Customs.CREATE));
    }

    /**
     * Applies the given customs to the object.
     * 
     * @param customs
     *            the permission checker
     * @return the result of the <code>customs.visit()</code> method on the object
     */
    protected abstract boolean accept(final Customs customs);

    // -------------------------------------------------
    //
    // "WORKOBJECT" stuffs
    //
    // -------------------------------------------------
    @Transient
    public List<IEntityTaskLink> getEntityTaskLinks()
    {
        return Queries.getEntityTaskLinks(this);
    }

    @Transient
    public List<ITask> boundTasks()
    {
        final List<ITask> ret = new ArrayList<ITask>();

        CollectUtils.addAllIfNotEmpty(ret, collect(getEntityTaskLinks(), new GetEntityTaskLinkTaskTransformer()));

        CollectUtils.removeNull(ret);

        return ret;
    }

    public void setEntityTaskLinks(final List<IEntityTaskLink> entityTaskLinks) throws Hd3dException
    {
        if (!isTaskable())
        {
            throw new Hd3dRuntimeException("this entity is not taskable.");
        }

        if (CollectUtils.isNotEmpty(entityTaskLinks))
        {
            for (IEntityTaskLink entityTaskLink : entityTaskLinks)
            {
                entityTaskLink.setBoundEntity(getId());
                entityTaskLink.setBoundEntityName(entityName());
            }

            for (IEntityTaskLink entityTaskLink : entityTaskLinks)
            {
                entityTaskLink.doPrePersist();
            }
            Persistors.entitytasklink.persistCollection(entityTaskLinks);
            for (IEntityTaskLink entityTaskLink : entityTaskLinks)
            {
                entityTaskLink.doPostPersist();
            }

        }
    }

    // -------------------------------------------------
    //
    // tags stuffs
    //
    // -------------------------------------------------
    @Transient
    public Set<TagParent> getTagParents()
    {
        final List<TagParent> result = Queries.getTagParents(this);
        final Set<TagParent> tagParents = CollectUtils.isNotEmpty(result) ? new HashSet<TagParent>(result)
                : new HashSet<TagParent>(0);
        this.tagParents.clear();
        this.tagParents.addAll(tagParents);
        return tagParents;
    }

    public void setTagParents(final Set<TagParent> parents) throws Hd3dException
    {
        CollectUtils.removeNull(parents);
        if (CollectUtils.isNotEmpty(parents))
        {
            this.tagParents.clear();
            this.tagParents.addAll(parents);
            for (TagParent tgp : parents)
            {
                addTag(tgp.getTag().getId());
            }
        }
    }

    public List<ITag> tags()
    {
        final List<ITag> ret = new ArrayList<ITag>();

        CollectUtils.addAllIfNotEmpty(ret, collect(getTagParents(), new GetTagTransformer()));

        return ret;
    }

    /**
     * given a list of tag ids, updates the current TagParent list. What is in the current TagParent list and NOT in tag
     * ids are REMOVED. What is in the tag ids and NOT in the current TagParent list are ADDED.
     * 
     * @param tagIds
     * @throws Hd3dPersistenceException
     */
    public synchronized void mergeTags(final List<Long> tagIds) throws Hd3dException
    {
        if (CollectUtils.isNotEmpty(tagIds))
        {
            /* Remove from current TagParent list the tags not in tagIds */
            for (TagParent tgp : selectRejected(getTagParents(), new MatchTagParentIdsPredicate(tagIds)))
            {
                tgp.getTag().removeParent(this);
            }

            /* Add tags that are in tagIds list but not in current TagParent list */
            for (Long tagId : tagIds)
            {
                addTag(tagId);
            }
        }
        else
        {
            for (TagParent tgp : getTagParents())
            {
                tgp.getTag().removeParent(this);
            }
        }
    }

    protected <T extends IBase> TagParent newTagParent(final T object, final ITag tag)
    {
        TagParent ret = null;

        if (object != null && tag != null)
        {
            ret = new TagParent();
            ret.setTag(tag);
            ret.setParent(object.getId());
            ret.setParentType(object.entityName());
        }

        return ret;
    }

    public synchronized void addTag(final Long tagId) throws Hd3dException
    {
        if (isValidId(tagId) && !getIds(this.tags()).contains(tagId))
        {
            final ITag tag = Persistors.tag.getById(tagId);
            if (tag != null)
            {
                tag.addParent(this);
            }
        }
    }

    public synchronized void removeTag(final Long tagId)
    {
        if (isValidId(tagId) && CollectUtils.isNotEmpty(getTagParents()))
        {
            try
            {
                final ITag tag = Persistors.tag.getById(tagId);
                if (tag != null)
                {
                    tag.removeParent(this);
                }
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
        }
    }

    // -------------------------------------------------
    //
    // dynmetadata stuffs
    //
    // -------------------------------------------------
    public Set<IClassDynMetaDataType> classDynMetaDataTypes()
    {
        Set<IClassDynMetaDataType> thisAndSuperClassesTypes = new HashSet<IClassDynMetaDataType>();

        /* read this class MetaDataValues... */
        final String className = this.getClass().getCanonicalName();

        CollectUtils.addAllIfNotEmpty(thisAndSuperClassesTypes, CLASSDYNMETADATATYPES_MAP.getCollection(className));

        /* ...and its super classes' MetaDataValues */
        Collection<String> superClassesNames = EntitiesMaps.classUpHierarchyNames(className);
        if (CollectUtils.isNotEmpty(superClassesNames))
        {
            for (String superClassName : superClassesNames)
            {
                CollectUtils.addAllIfNotEmpty(thisAndSuperClassesTypes, CLASSDYNMETADATATYPES_MAP
                        .getCollection(superClassName));
            }
        }

        return thisAndSuperClassesTypes;
    }

    public static void setClassDynMetaDataTypes(final Collection<IClassDynMetaDataType> classDynMetaDataTypes)
    {
        for (IClassDynMetaDataType c : classDynMetaDataTypes)
        {
            addClassDynMetaDataType(c);
        }
    }

    /**
     * note: always return a non-null reference
     */
    @Transient
    public List<IDynMetaDataValue> getDynMetaDataValues()
    {
        return Queries.getDynMetaDataValues(this);
    }

    public Collection<IDynMetaDataValue> getDynMetaDataValues(final IClassDynMetaDataType type)
    {
        return select(getDynMetaDataValues(), new MatchClassDynMetaDataTypePredicate(type));
    }

    public IDynMetaDataValue getDynMetaDataValue(final Long classDynMetaDataId)
    {
        final List<IDynMetaDataValue> result = Queries.getDynMetaDataValues(this, classDynMetaDataId);
        return CollectUtils.isNotEmpty(result) ? result.get(0) : null;
    }

    public void setDynMetaDataValue(final Long metaDataId, final String value) throws Hd3dException
    {
        /* retrieve all metadatavalues bound to this object and select the first one */
        final String[] properties = new String[] { "classDynMetaDataType.id", "parent" };
        final Object[] values = new Object[] { metaDataId, this.getId() };
        final List<IDynMetaDataValue> metaDataValues = Persistors.dynmetadatavalue.getByValues(properties, values,
                "AND");

        if (CollectUtils.isNotEmpty(metaDataValues))
        {
            IDynMetaDataValue found = metaDataValues.get(0);
            if (found != null)
            {
                found.setValue(value);
                found.doPreUpdate();
                Persistors.dynmetadatavalue.merge(found);
                found.doPostUpdate();
            }
        }
    }

    public void removeDynMetaDataValues(final IClassDynMetaDataType type) throws Hd3dException
    {
        removeDynMetaDataValues(getDynMetaDataValues(type));
    }

    public static void addClassDynMetaDataType(final IClassDynMetaDataType type)
    {
        if (type != null)
        {
            synchronized (type)
            {
                /* this is a MultiHashMap: a same key matches SEVERAL (a list) DynMetaDataTypes */
                CLASSDYNMETADATATYPES_MAP.put(type.getClassName(), type);
            }
        }
    }

    public static void removeClassDynMetaDataType(final IClassDynMetaDataType type)
    {
        if (type != null)
        {
            synchronized (type)
            {
                /* this is a MultiHashMap: a same key matches SEVERAL (a list) DynMetaDataTypes */
                CLASSDYNMETADATATYPES_MAP.remove(type.getClassName(), type);
            }
        }
    }

    public static void updateClassDynMetaDataType(final IClassDynMetaDataType type)
    {
        if (type != null)
        {
            /* is all that mandatory ? */
            final String className = type.getClassName();

            final Collection<IClassDynMetaDataType> coll = CLASSDYNMETADATATYPES_MAP.getCollection(className);
            if (CollectUtils.isNotEmpty(coll))
            {
                IClassDynMetaDataType match = find(coll, new EqIdPredicate<IClassDynMetaDataType>(type.getId()));

                if (match != null)
                {
                    // ?
                    removeClassDynMetaDataType(match);
                    addClassDynMetaDataType(match);
                }
            }
        }
    }

    public void removeDynMetaDataValues(final Collection<IDynMetaDataValue> values) throws Hd3dException
    {
        fr.hd3d.model.persistence.impl.hibernate.visitor.DynMetaDataValueHandler.remove(values);
    }

    public void removeDynMetaDataValue(final IDynMetaDataValue value) throws Hd3dException
    {
        fr.hd3d.model.persistence.impl.hibernate.visitor.DynMetaDataValueHandler.remove(value);
    }

    // -------------------------------------------------
    //
    // approvalnotes stuffs
    //
    // -------------------------------------------------
    @Transient
    public List<IApprovalNote> getApprovalNotes()
    {
        return Queries.getApprovalNotes(this);
    }

    @Transient
    public List<IApprovalNote> getApprovalNotesByApprovalType(final Long approvalTypeId)
    {
        return Queries.getApprovalNotesByType(this, approvalTypeId);
    }

    @Transient
    @Deprecated
    public List<IApprovalNote> getApprovalNotesByTypes(String[] types)
    {
        if (!isApprovable())
            return null;

        if (types == null)
            return getApprovalNotes();

        List<IApprovalNote> result = Queries.getApprovalNotesByTypes(this, types);
        if (result == null)
            result = new ArrayList<IApprovalNote>();
        return result;
    }

    public void setApprovalNotes(final List<IApprovalNote> approvalNotes) throws Hd3dException
    {
        if (!isApprovable())
        {
            throw new Hd3dRuntimeException("this entity is not approvable.");
        }

        for (IApprovalNote note : approvalNotes)
        {
            note.setBoundEntity(getId());
            note.setBoundEntityName(entityName());
        }

        for (IApprovalNote note : approvalNotes)
        {
            note.doPrePersist();
        }
        Persistors.approvalnote.persistCollection(approvalNotes);
        for (IApprovalNote note : approvalNotes)
        {
            note.doPostPersist();
        }
    }

    public static String getUUID()
    {
        return java.util.UUID.randomUUID().toString();
    }

    public void enable()
    {
        setInternalStatus(Const.INTERNALSTATUS_ENABLED);
    }

    public void disable() throws Hd3dException
    {
        disableTasks();
        setInternalStatus(Const.INTERNALSTATUS_DISABLED);
        postDisable();
    }

    protected void disableTasks() throws Hd3dException
    {
        /* an entity cannot be disabled if some activities have been reported upon it */
        final List<ITask> tasks = boundTasks();
        CollectUtils.removeNull(tasks);

        for (ITask t : tasks)
        {
            if (t.hasNotEmptyActivities())
            {
                throw new Hd3dException("Entity cannot be disabled: some activities have been reported upon it.");
            }
        }
    }

    public boolean enabled()
    {
        return Const.INTERNALSTATUS_ENABLED.equals(getInternalStatus());
    }

    public void postDisable() throws Hd3dException
    {
        disableDynMetaDataValues();
        disableApprovalNotes();
        disableEntityTaskLinks();
        /* really delete, not disable */
        deleteTagParents();
    }

    private void disableDynMetaDataValues() throws Hd3dException
    {
        final Collection<IDynMetaDataValue> dynValues = CollectUtils.selectNotNull(getDynMetaDataValues());
        Persistors.dynmetadatavalue.disableCollection(dynValues);
    }

    private void disableApprovalNotes() throws Hd3dException
    {
        final Collection<IApprovalNote> approvalNotes = CollectUtils.selectNotNull(getApprovalNotes());
        Persistors.approvalnote.disableCollection(approvalNotes);
    }

    private void disableEntityTaskLinks() throws Hd3dException
    {
        final Collection<IEntityTaskLink> entityTaskLinks = CollectUtils.selectNotNull(getEntityTaskLinks());
        Persistors.entitytasklink.disableCollection(entityTaskLinks);
    }

    private void deleteTagParents() throws Hd3dException
    {
        final Collection<TagParent> tagParents = CollectUtils.selectNotNull(getTagParents());
        HibernatePersist.deleteRawCollection(tagParents);
    }

    // -------------------------------------------------
    //
    // other stuffs
    //
    // -------------------------------------------------
    /*
     * DO NOT DELETE: called by Sheets
     */
    public List<IApprovalNote> approvalNotes(final String approvalTypeId)
    {
        return getApprovalNotesByApprovalType(NumberUtils.toLong(approvalTypeId));
    }

    /*
     * DO NOT DELETE: called by Sheets
     */
    public Long activitiesDuration()
    {
        final Long result = Queries.getTaskActivitiesDuration(this, null);

        return result == null ? NumberUtils.LONG_ZERO : result;
    }

    public Long tasksActivitiesDuration(final String taskTypeId)
    {
        return Queries.getTaskActivitiesDuration(this, NumberUtils.toLong(taskTypeId));
    }

    public Long tasksEstimation(final String taskTypeId)
    {
        return Queries.getTasksEstimation(this, NumberUtils.toLong(taskTypeId));
    }

    public Long tasksDurationGap(final String taskTypeId)
    {
        final Long id = NumberUtils.toLong(taskTypeId);
        final Long estimation = Queries.getTasksEstimation(this, id) / 1000L;
        final Long activities = Queries.getTaskActivitiesDuration(this, id);
        return estimation - activities;
    }

    public List<IPerson> tasksWorkers(final String taskTypeId)
    {
        return Queries.getTaskWorkers(this, NumberUtils.toLong(taskTypeId));
    }

    public String tasksStatus(final String taskTypeId)
    {
        return Queries.getTaskStatus(this, NumberUtils.toLong(taskTypeId));
    }

    public String toString()
    {
        return new ToStringBuilder(this).append("id", id).append("version", version).append("internalStatus",
                internalStatus).toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
