package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.common.client.enums.EItemMetaType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILItem;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.EnumUtils;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Table(name = "hd3d_item")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ItemH extends BaseH implements IItem
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String METATYPE = "metatype";

    private Integer index;// display order
    private String renderer;
    private String editor;
    private String name;
    private String type;// "attribute","hql"...
    private String query;
    private String controlContent;
    private IItemGroup itemGroup;
    private String metaType;
    private String transformer;// fully qualified java class name, applied on result of "query" field
    private String transformerParameters;// parameters used by transformer

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_item"), @Parameter(name = "initial_value", value = "1"),
            @Parameter(name = "increment_size", value = "1"), @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ItemH()
    {}

    public ItemH(Long id, Timestamp version, String name, String type, String query, String controlcontent,
            IItemGroup itemgroup, String metatype, String transformer, String transformerParameters)
    {
        super(id, version);
        this.name = name;
        this.type = type;
        this.query = query;
        this.controlContent = controlcontent;
        this.itemGroup = itemgroup;
        if (itemgroup != null)
            itemgroup.addItem(this);
        this.metaType = metatype;
        this.renderer = "";
        this.editor = "";
        this.transformer = transformer;
        this.transformerParameters = transformerParameters;
    }

    public ItemH(String name, String type, String query, String controlcontent, IItemGroup itemgroup, String metatype,
            String transformer, String transformerParameters)
    {
        this(null, null, name, type, query, controlcontent, itemgroup, metatype, transformer, transformerParameters);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "item_index")
    public Integer getIndex()
    {
        return index;
    }

    public void setIndex(Integer index)
    {
        if (index == null)
            this.index = NumberUtils.INTEGER_ZERO;
        else
            this.index = index;
    }

    @Column(name = "item_name")
    public String getName()
    {
        return name;
    }

    @Column(name = "item_controlcontent")
    public String getControlContent()
    {
        return this.controlContent;
    }

    @ManyToOne(targetEntity = ItemGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.MERGE,
            org.hibernate.annotations.CascadeType.LOCK })
    @JoinColumn(name = "item_itemgroup_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IItemGroup getItemGroup()
    {
        return this.itemGroup;
    }

    @Column(name = "item_query")
    public String getQuery()
    {
        return this.query;
    }

    @Column(name = "item_type")
    public String getType()
    {
        return this.type;
    }

    @Column(name = "item_metatype")
    public String getMetaType()
    {
        return metaType;
    }

    @Column(name = "item_editor")
    public String getEditor()
    {
        return this.editor;
    }

    @Column(name = "item_renderer")
    public String getRenderer()
    {
        return this.renderer;
    }

    @Column(name = "item_transformer")
    public String getTransformer()
    {
        return transformer;
    }

    @Column(name = "item_transformerparameters")
    public String getTransformerParameters()
    {
        return transformerParameters;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setControlContent(String controlcontent)
    {
        this.controlContent = controlcontent;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setItemGroup(IItemGroup itemgroup)
    {
        this.itemGroup = itemgroup;
    }

    public void setMetaType(String metatype)
    {
        this.metaType = metatype;
    }

    public void setEditor(String editor)
    {
        if (editor != null)
        {
            this.editor = editor;
        }
        else
        {
            this.editor = "";
        }
    }

    public void setRenderer(String renderer)
    {
        if (renderer != null)
        {
            this.renderer = renderer;
        }
        else
        {
            this.renderer = "";
        }
    }

    public void setTransformer(String transformer)
    {
        this.transformer = transformer;
    }

    public void setTransformerParameters(String transformerParameters)
    {
        this.transformerParameters = transformerParameters;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((controlContent == null) ? 0 : controlContent.hashCode());
        result = prime * result + ((index == null) ? 0 : index.hashCode());
        result = prime * result + ((itemGroup == null) ? 0 : itemGroup.hashCode());
        result = prime * result + ((metaType == null) ? 0 : metaType.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((query == null) ? 0 : query.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ItemH other = (ItemH) obj;
        if (controlContent == null)
        {
            if (other.controlContent != null)
                return false;
        }
        else if (!controlContent.equals(other.controlContent))
            return false;
        if (index == null)
        {
            if (other.index != null)
                return false;
        }
        else if (!index.equals(other.index))
            return false;
        if (itemGroup == null)
        {
            if (other.itemGroup != null)
                return false;
        }
        else if (!itemGroup.equals(other.itemGroup))
            return false;
        if (metaType == null)
        {
            if (other.metaType != null)
                return false;
        }
        else if (!metaType.equals(other.metaType))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (query == null)
        {
            if (other.query != null)
                return false;
        }
        else if (!query.equals(other.query))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILItem, IItem> defaultTranslator()
    {
        return Translators.item;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("index", index).append("renderer", renderer).append("editor", editor).append("name", name).append(
    // "type", type).append("query", query).append("controlContent", controlContent);
    // b.append("itemGroupId", itemGroup == null ? "null" : itemGroup.getId());
    // b.append("metaType", metaType).append("transformer", transformer).append("transformerParameters",
    // transformerParameters);
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class ItemComplexTypeProvider implements IComplexTypeProvider
    {
        public Object getValue(String field, String value)
        {
            return METATYPE.equals(field) ? EnumUtils.getEnumValueIgnoreCase(EItemMetaType.class, value) : null;
        }

        public List<String> getValidEnum(String field)
        {
            if (METATYPE.equals(field))
                return EnumUtils.getEnumStringValues(EItemMetaType.class);
            return Collections.emptyList();
        }

        public boolean isValidTypeField(String field)
        {
            return METATYPE.equals(field);
        }
    }
}
