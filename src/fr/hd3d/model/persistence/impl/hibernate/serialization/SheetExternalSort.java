package fr.hd3d.model.persistence.impl.hibernate.serialization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.collections15.list.SetUniqueList;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.serialize.ArraySerializer;
import com.esotericsoftware.kryo.serialize.CollectionSerializer;
import com.esotericsoftware.kryo.serialize.DateSerializer;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.Pair;
import fr.hd3d.utils.ServerResourceUtils;


public class SheetExternalSort
{
    final String UUID = java.util.UUID.randomUUID().toString();
    AtomicInteger counter = new AtomicInteger();

    public File serialize(Object[][][] values, final int nbColumns) throws IOException
    {
        if (values == null)
        {
            return null;
        }

        /* serialize it into a temp file */
        File file = new File(Conf.getTmpDir(), UUID + '_' + counter.incrementAndGet() + ".ser");
        file.createNewFile();

        serialize(values, nbColumns, values, file);

        return file;
    }

    public void serialize(Object[][][] part, final int nbColumns, Object[][][] filteredRes, File file)
            throws IOException
    {
        List<SheetDataHolder> holders = convertForSerialisation(part, nbColumns, filteredRes);

        serializeWithKryo(holders, file);
    }

    public List<SheetDataHolder> convertForSerialisation(Object[][][] part, final int nbColumns,
            Object[][][] filteredRes)
    {
        List<SheetDataHolder> holders = new ArrayList<SheetDataHolder>();

        for (Object[][] line : part)
        {
            holders.add(new SheetDataHolder(line, true));
        }

        return holders;
    }

    private void serializeWithKryo(List<SheetDataHolder> holders, File file) throws IOException
    {
        final Kryo kryo = getKryo();

        /* global buffer containing serialized objects */
        ByteBuffer objectsBuffer = ByteBuffer.allocateDirect(1024 * 1000);
        /* objectBuffer must be large enough to contain one serialized object */
        ByteBuffer oneObjectBuffer = ByteBuffer.allocateDirect(1024 * 10);

        FileOutputStream output = new FileOutputStream(file, true);
        FileChannel channel = output.getChannel();

        try
        {
            /* serialize number of objects in this file */
            objectsBuffer.putInt(holders.size());

            /* serialize objects */
            for (SheetDataHolder sdh : holders)
            {
                /* populate the global buffer */
                if (objectsBuffer.remaining() > 1024 * 10 + 4)
                {
                    kryo.writeClassAndObject(oneObjectBuffer, sdh);
                    /* object size in bytes */
                    objectsBuffer.putInt(oneObjectBuffer.position());
                    /* serialized object */
                    oneObjectBuffer.flip();
                    objectsBuffer.put(oneObjectBuffer);

                    oneObjectBuffer.clear();
                }
                /* flush the global buffer */
                else
                {
                    objectsBuffer.flip();
                    channel.write(objectsBuffer);
                    objectsBuffer.clear();
                }
            }
            objectsBuffer.flip();
            channel.write(objectsBuffer);
            objectsBuffer.clear();
        }
        finally
        {
            channel.close();
            output.close();
        }
    }

    private static Kryo getKryo()
    {
        Kryo kryo = new Kryo();
        kryo.setRegistrationOptional(true);
        kryo.register(SheetDataHolder.class);
        kryo.register(Object[].class, new ArraySerializer(kryo));
        // kryo.register(EntityDescriptor.class, new EntityDescriptorSerializer());
        kryo.register(EntityDescriptor.class);
        kryo.register(ArrayList.class, new CollectionSerializer(kryo));
        kryo.register(Timestamp.class, new DateSerializer());
        kryo.register(SetUniqueList.class, new SetUniqueListSerializer(kryo));
        return kryo;
    }

    public Object[][][] mergeSort(List<File> sortedFiles, final Map<String, String> map, final int length,
            org.apache.commons.collections15.comparators.ComparatorChain<Object[][]> comparators) throws IOException,
            Hd3dException
    {
        /* merge sort the temporary small sorted files */
        Object[][][] result = null;
        Object[][][] paginatedRes = null;
        File mergedSortedFile = null;
        try
        {
            mergedSortedFile = new File(Conf.getTmpDir(), UUID + "_sorted.ser");
            mergedSortedFile.createNewFile();

            mergeSortedFiles(sortedFiles, mergedSortedFile, comparators, length);

            List<File> files = new ArrayList<File>();
            files.add(mergedSortedFile);
            /* read back */
            paginatedRes = deserializeAndPaginate(files, map, length);

            result = paginatedRes;
        }
        finally
        {
            mergedSortedFile.delete();
        }
        return result;
    }

    private void mergeSortedFiles(List<File> files, File outputfile,
            org.apache.commons.collections15.comparators.ComparatorChain<Object[][]> comparators, final int length)
            throws IOException, Hd3dException
    {
        /**/
        final Kryo kryo = getKryo();

        /* global buffer containing serialized objects */
        ByteBuffer objectsBuffer = ByteBuffer.allocateDirect(1024 * 1000);
        /* objectBuffer must be large enough to contain one serialized object */
        ByteBuffer oneObjectBuffer = ByteBuffer.allocateDirect(1024 * 10);

        FileOutputStream output = new FileOutputStream(outputfile, true);
        FileChannel channel = output.getChannel();

        /*
         * Note: do not pass comparator to PriorityQueue's constructor, as it is passed to SheetDataBinaryFileBuffer's
         * constructor. Doing both will give unpredictable behavior.
         */
        PriorityBlockingQueue<SheetDataBinaryFileBuffer> pq = new PriorityBlockingQueue<SheetDataBinaryFileBuffer>(
                files.size());

        for (File f : files)
        {
            if (f.length() > 0)
            {
                SheetDataBinaryFileBuffer bfb = new SheetDataBinaryFileBuffer(getKryo(), f, comparators, files.size());
                pq.add(bfb);
            }
        }

        try
        {
            /* number of objects */
            objectsBuffer.putInt(length);

            while (pq.size() > 0)
            {
                /* since this a priority (sorted) queue, the first object is always the right one */
                SheetDataBinaryFileBuffer bfb = pq.poll();

                if (bfb == null || bfb.peek() == null)
                {
                    continue;
                }

                SheetDataHolder holder = new SheetDataHolder(bfb.pop(), true);

                /* write */
                kryo.writeClassAndObject(oneObjectBuffer, holder);
                /* object size in bytes */
                objectsBuffer.putInt(oneObjectBuffer.position());
                /* serialized object */
                oneObjectBuffer.flip();
                objectsBuffer.put(oneObjectBuffer);
                objectsBuffer.flip();
                channel.write(objectsBuffer);

                oneObjectBuffer.clear();
                objectsBuffer.clear();

                if (bfb.empty())
                {
                    bfb.clear();
                    pq.remove(bfb);
                }
                else
                {
                    pq.add(bfb); // add it back
                }
            }
        }
        finally
        {
            channel.close();
            output.close();
            pq.clear();
        }
    }

    public static Object[][][] deserializeAndPaginate(List<File> createdFiles, Map<String, String> map, int length)
            throws IOException, Hd3dException
    {
        List<Object[][]> readBack = new ArrayList<Object[][]>();

        for (SheetDataHolder holder : deSerializeWithKryoAndPaginate(createdFiles, map, length))
        {
            readBack.add(holder.getSheetData());
        }

        return hydrate(readBack.toArray(new Object[readBack.size()][][]));
    }

    private static List<SheetDataHolder> deSerializeWithKryoAndPaginate(List<File> files, Map<String, String> map,
            int length) throws IOException, Hd3dQueryFilterException
    {
        /* pagination info */
        Pair<Integer, Integer> paginationIndexes = ServerResourceUtils.getStartAndEndIndex(map, length);
        final int start = paginationIndexes.getField_1();
        final int end = paginationIndexes.getField_2();
        int pos = 0;

        /* deserialize */
        final Kryo kryo = getKryo();

        List<SheetDataHolder> savedholders = new ArrayList<SheetDataHolder>();

        ByteBuffer oneObjectBuffer;
        ByteBuffer lengthBuffer = ByteBuffer.allocateDirect(4);

        for (File file : files)
        {
            FileInputStream input = new FileInputStream(file);
            FileChannel channel = input.getChannel();

            try
            {
                /* number of objects in this file */
                int numberOfObjects = 0;
                if (channel.read(lengthBuffer) != -1)
                {
                    lengthBuffer.flip();
                    numberOfObjects = lengthBuffer.getInt();
                    lengthBuffer.clear();
                }

                if (numberOfObjects > 0)
                {
                    while (channel.read(lengthBuffer) != -1)
                    {
                        /* pagination end reached */
                        if (pos >= end)
                        {
                            break;
                        }

                        /* read object size */
                        lengthBuffer.flip();
                        int objectSize = lengthBuffer.getInt();

                        /* only deserialize objects starting after first element of pagination */
                        if (pos >= start)
                        {
                            /* read object */
                            oneObjectBuffer = ByteBuffer.allocateDirect(objectSize);
                            channel.read(oneObjectBuffer);
                            oneObjectBuffer.flip();
                            SheetDataHolder sdh = (SheetDataHolder) kryo.readClassAndObject(oneObjectBuffer);
                            savedholders.add(sdh);

                            oneObjectBuffer.clear();
                        }
                        else
                        {
                            /* skip unnecessary objects (not in pagination) */
                            channel.position(channel.position() + objectSize);
                        }
                        lengthBuffer.clear();
                        pos++;
                    }
                }
            }
            finally
            {
                channel.close();
                input.close();
            }
        }

        return savedholders;
    }

    public static Object[][][] hydrate(Object[][][] values) throws Hd3dException
    {
        if (values == null || values.length < 1)
        {
            return values;
        }

        /* store hydrated itemGroups and items */
        Map<EntityDescriptor, IItemGroup> itemGroups = extractItemGroups(values);
        Map<EntityDescriptor, IItem> items = extractItems(values);

        /* find out columns containing EntityDescriptors */
        List<Integer> entityDescriptorCols = SetUniqueList.decorate(new ArrayList<Integer>());
        for (int i = 0; i < values[0].length; i++)
        {
            Object[][] col = CollectUtils.getColFromArray(values, i);
            for (Object[] cell : col)
            {
                if (cell == null)
                    continue;

                if (cell[0] instanceof Collection)
                {
                    Iterator<?> it = ((Collection<?>) cell[0]).iterator();
                    if (it == null || !it.hasNext())
                        continue;

                    Object firstObject = it.next();
                    if (firstObject instanceof EntityDescriptor)
                    {
                        entityDescriptorCols.add(i);
                    }
                }
                else if (cell[0] instanceof EntityDescriptor)
                {
                    entityDescriptorCols.add(i);
                    break;
                }
            }
        }

        /* hydrate itemGroups & items from entity descriptor */
        hydrateItemGroupAndItems(itemGroups, items, values);

        /* for each selected column, collect the ids, query the objects and reorder */
        for (Integer index : entityDescriptorCols)
        {
            /* collect entityDescriptors info */
            Map<String, SetUniqueList<Long>> entityNamesAndIds = entityNamesAndIds(values, index);

            /* retrieve the objects matching the entity descriptors */
            Map<String, Map<Long, Persistent>> entityNamesAndObjects = entityNamesAndObjects(entityNamesAndIds);

            /* reorder */
            EntityDescriptor descriptor;
            for (Object[][] row : values)
            {
                Object valueObject = row[index] == null ? null : row[index][0];

                if (valueObject instanceof Collection)
                {
                    List<Persistent> hydratedValues = new ArrayList<Persistent>();
                    for (Object valueObj : (Collection) valueObject)
                    {
                        if (valueObj instanceof EntityDescriptor)
                        {
                            descriptor = (EntityDescriptor) valueObj;
                            hydratedValues.add(getHydratedValue(descriptor, entityNamesAndObjects));
                        }
                    }
                    row[index][0] = hydratedValues;
                }
                else
                {
                    descriptor = (EntityDescriptor) valueObject;
                    row[index][0] = getHydratedValue(descriptor, entityNamesAndObjects);
                }

            }
        }

        return values;
    }

    private static Persistent getHydratedValue(EntityDescriptor descriptor,
            Map<String, Map<Long, Persistent>> entityNamesAndObjects)
    {
        Long id;
        String name;
        Map<Long, ? extends Persistent> objectsMap;

        if (descriptor != null)
        {
            id = descriptor.getId();
            name = descriptor.getName();
            if (id != null)
            {
                objectsMap = entityNamesAndObjects.get(name);
                if (objectsMap != null)
                {
                    Persistent obj = objectsMap.get(id);
                    if (obj != null)
                    {
                        return obj;
                    }
                }
            }
        }

        return null;
    }

    private static Map<EntityDescriptor, IItemGroup> extractItemGroups(Object[][][] values) throws Hd3dException
    {
        Map<EntityDescriptor, IItemGroup> ret = new HashMap<EntityDescriptor, IItemGroup>();
        Object[][] firstRow = values[0];
        for (Object[] cell : firstRow)
        {
            EntityDescriptor itemGroupDescriptor = (EntityDescriptor) cell[1];
            ret.put(itemGroupDescriptor, Persistors.itemgroup.getById(itemGroupDescriptor.getId()));
        }

        return ret;
    }

    private static Map<EntityDescriptor, IItem> extractItems(Object[][][] values) throws Hd3dException
    {
        Map<EntityDescriptor, IItem> ret = new HashMap<EntityDescriptor, IItem>();
        Object[][] firstRow = values[0];
        for (Object[] cell : firstRow)
        {
            EntityDescriptor itemDescriptor = (EntityDescriptor) cell[2];
            ret.put(itemDescriptor, Persistors.item.getById(itemDescriptor.getId()));
        }

        return ret;
    }

    private static void hydrateItemGroupAndItems(Map<EntityDescriptor, IItemGroup> itemGroups,
            Map<EntityDescriptor, IItem> items, Object[][][] values)
    {
        if (values == null)
            return;

        for (int i = 0; i < values.length; i++)
        {
            Object[][] row = values[i];
            if (row == null)
                continue;

            for (int j = 0; j < row.length; j++)
            {
                row[j][1] = itemGroups.get(row[j][1]);
                row[j][2] = items.get(row[j][2]);
            }
        }
    }

    private static Map<String, SetUniqueList<Long>> entityNamesAndIds(Object[][][] values, int index)
    {
        Map<String, SetUniqueList<Long>> ret = new HashMap<String, SetUniqueList<Long>>();

        if (values == null)
            return ret;

        for (Object[][] row : values)
        {
            if (row[index] == null || row[index][0] == null)
            {
                continue;
            }
            else if (row[index][0] instanceof EntityDescriptor)
            {
                addDescriptorInMap(ret, (EntityDescriptor) row[index][0]);
            }
            else if (row[index][0] instanceof Collection)
            {
                if (CollectUtils.isNotEmpty((Collection<?>) row[index][0]))
                {
                    List<? extends EntityDescriptor> descriptors = CollectUtils
                            .toEntityDescriptors((Collection) row[index][0]);
                    for (EntityDescriptor desc : descriptors)
                    {
                        addDescriptorInMap(ret, desc);
                    }
                }
            }
        }

        return ret;
    }

    private static void addDescriptorInMap(Map<String, SetUniqueList<Long>> map, EntityDescriptor descriptor)
    {
        Long id;
        String name;
        if (descriptor != null)
        {
            id = descriptor.getId();
            name = descriptor.getName();
            if (id == null)
                return;

            if (map.get(name) == null)
            {
                SetUniqueList<Long> ids = SetUniqueList.decorate(new ArrayList<Long>());
                ids.add(id);
                map.put(name, ids);
            }
            else
            {
                map.get(name).add(id);
            }
        }
    }

    private static <T extends Persistent> Map<String, Map<Long, T>> entityNamesAndObjects(
            Map<String, SetUniqueList<Long>> entityNamesAndIds) throws Hd3dException
    {
        Map<String, Map<Long, T>> entityNamesAndObjects = new HashMap<String, Map<Long, T>>();
        for (Entry<String, SetUniqueList<Long>> entry : entityNamesAndIds.entrySet())
        {
            if (entityNamesAndObjects.get(entry.getKey()) == null)
            {
                Map<Long, T> map = new HashMap<Long, T>();
                entityNamesAndObjects.put(entry.getKey(), map);
            }

            for (List<Long> ids : CollectUtils.partition(entry.getValue(), Conf.getMaxRecords()))
            {
                List<? extends Persistent> objs = Persistors.getPersistor(entry.getKey()).getByIds(ids);

                entityNamesAndObjects.get(entry.getKey()).putAll(
                        (Map<? extends Long, ? extends T>) CollectUtils.to_Id_EntityMap(objs));
            }

        }
        return entityNamesAndObjects;
    }
}
