package fr.hd3d.model.persistence.impl.hibernate.serialization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;


public class SheetDataHolder
{
    /*
     * Collect Values. Array containing: dim 0 = lines of result; dim 1 = columns (items); dim 2 = [0] item value,[1]
     * item group, [2] item, [3] entity id
     */
    private Object[][] sheetData;

    public SheetDataHolder()
    {}

    public SheetDataHolder(Object[][] sheetData, boolean dehydrate)
    {
        if (dehydrate)
        {
            setSheetDataDehydrate(sheetData);
        }
        else
        {
            setSheetData(sheetData);
        }
    }

    public Object[][] getSheetData()
    {
        return sheetData;
    }

    public void setSheetDataDehydrate(Object[][] sheetData)
    {
        this.sheetData = new Object[sheetData.length][4];

        for (int i = 0; i < sheetData.length; i++)
        {
            this.sheetData[i][0] = getValue(sheetData[i][0]);
            this.sheetData[i][1] = new EntityDescriptor((IItemGroup) sheetData[i][1]);
            this.sheetData[i][2] = new EntityDescriptor((IItem) sheetData[i][2]);
            this.sheetData[i][3] = sheetData[i][3];
        }
    }

    private Object getValue(Object object)
    {
        if (object instanceof Collection)
        {
            return getValue((Collection) object);
        }
        else if (object instanceof IBase)
        {
            return new EntityDescriptor((IBase) object);
        }
        else
        {
            return object;
        }
    }

    @SuppressWarnings("unchecked")
    private List<Object> getValue(Collection<Object> objects)
    {
        List<Object> list = new ArrayList<Object>();
        for (Object obj : objects)
        {
            if (obj instanceof Collection)
            {
                return getValue((Collection) obj);
            }
            else if (obj instanceof IBase)
            {
                IBase entity = (IBase) obj;
                list.add(new EntityDescriptor(entity));
            }
            else
            {
                list.add(obj);
            }
        }
        return list;
    }

    public void setSheetData(Object[][] sheetData)
    {
        this.sheetData = sheetData;
    }
}
