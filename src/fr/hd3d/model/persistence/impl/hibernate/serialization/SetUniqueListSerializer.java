package fr.hd3d.model.persistence.impl.hibernate.serialization;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import org.apache.commons.collections15.list.SetUniqueList;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.serialize.CollectionSerializer;


public class SetUniqueListSerializer extends CollectionSerializer
{
    public SetUniqueListSerializer(Kryo kryo)
    {
        super(kryo);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T readObjectData(ByteBuffer buffer, Class<T> clazz)
    {

        if (clazz == SetUniqueList.class)
        {
            return (T) super.readObjectData(buffer, ArrayList.class);
        }
        else
        {
            return super.readObjectData(buffer, clazz);
        }
    }

}
