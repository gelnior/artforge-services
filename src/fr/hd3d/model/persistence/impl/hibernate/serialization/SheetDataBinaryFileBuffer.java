package fr.hd3d.model.persistence.impl.hibernate.serialization;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.esotericsoftware.kryo.Kryo;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


public class SheetDataBinaryFileBuffer implements Comparable<SheetDataBinaryFileBuffer>
{

    public static int BUFFERSIZE = 512;
    public FileChannel channel;
    Comparator<Object[][]> comparator;
    public File originalfile;
    private FileInputStream input;
    private Object[][] cache;
    private LinkedList<Object[][]> cacheList = new LinkedList<Object[][]>();
    private int numberOfObjects;
    private List<Integer> nbObjectsPerSlice;// FIFO
    private int sliceIndex = 0;
    private boolean empty;
    private ByteBuffer oneObjectBuffer;
    private ByteBuffer lengthBuffer = ByteBuffer.allocateDirect(4);
    private Kryo kryo;

    public SheetDataBinaryFileBuffer(Kryo kryo, File f, Comparator<Object[][]> cmp, int nbFiles) throws IOException,
            Hd3dException
    {
        originalfile = f;
        comparator = cmp;
        this.kryo = kryo;
        input = new FileInputStream(originalfile);
        channel = input.getChannel();
        if (channel.read(lengthBuffer) != -1)
        {
            /* read total number of objects */
            lengthBuffer.flip();
            numberOfObjects = lengthBuffer.getInt();
            lengthBuffer.clear();
            /* prepare number of objects to read for each slice */
            nbObjectsPerSlice = CollectUtils.partition(Conf.getMaxRecords(), nbFiles);
        }
        reload();
    }

    public boolean empty()
    {
        return empty;
    }

    private void reload() throws IOException, Hd3dException
    {
        try
        {
            if (cacheList.isEmpty())
            {
                populateCacheList();
                empty = false;
            }
            if (cacheList.peek() != null)
            {
                cache = cacheList.pop();
                empty = false;
            }
            else
            {
                empty = true;
                cache = null;
            }
        }
        catch (EOFException oef)
        {
            empty = true;
            cache = null;
        }
    }

    private void populateCacheList() throws IOException, Hd3dException
    {
        try
        {
            /* No need to populate */
            if (!cacheList.isEmpty() || sliceIndex >= nbObjectsPerSlice.size())
                return;

            for (int i = 0; i < nbObjectsPerSlice.get(sliceIndex); i++)
            {
                if (channel.read(lengthBuffer) == -1)
                    break;

                /* read object size */
                lengthBuffer.flip();
                int objectSize = lengthBuffer.getInt();

                /* read object */
                oneObjectBuffer = ByteBuffer.allocateDirect(objectSize);
                channel.read(oneObjectBuffer);
                oneObjectBuffer.flip();
                SheetDataHolder sdh = (SheetDataHolder) kryo.readClassAndObject(oneObjectBuffer);

                if (sdh != null)
                {
                    cacheList.add(sdh.getSheetData());
                }

                oneObjectBuffer.clear();
                lengthBuffer.clear();
            }

            /* slice processed */
            sliceIndex++;

            SheetExternalSort.hydrate(cacheList.toArray(new Object[cacheList.size()][][]));

        }
        catch (EOFException oef)
        {
            empty = true;
            cache = null;
        }
    }

    public Object[][] peek()
    {
        if (empty())
            return null;
        return cache;
    }

    public Object[][] pop() throws IOException, Hd3dException
    {
        Object[][] answer = peek();
        reload();
        return answer;
    }

    public int compareTo(SheetDataBinaryFileBuffer b)
    {
        return comparator.compare(peek(), b.peek());
    }

    public void clear()
    {
        try
        {
            comparator = null;
            channel.close();
            channel = null;
            input.close();
            input = null;
            originalfile = null;
            cache = null;
            empty = true;
            oneObjectBuffer.clear();
            oneObjectBuffer = null;
            lengthBuffer.clear();
            lengthBuffer = null;
            kryo = null;
        }
        catch (IOException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
    }
}
