package fr.hd3d.model.persistence.impl.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILDynamicPath;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


@Entity
@Table(name = "hd3d_dynamic_path")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class DynamicPathH extends BaseH implements IDynamicPath
{

    private static final long serialVersionUID = 1L;

    private String name;
    private String type;
    private ITaskType taskType;
    private Long boundEntityId;
    private String boundEntityName;
    private String wipDynamicPath;
    private String publishDynamicPath;
    private String oldDynamicPath;
    private IProject project;
    private IDynamicPath parent;
    private Collection<IDynamicPath> children;

    public DynamicPathH()
    {}

    public DynamicPathH(final String name, final String type, final Long boundEntityId, final String boundEntityName,
            final ITaskType taskType, final String wipDynamicPath, final String publishDynamicPath,
            final String oldDynamicPath, final IProject project, final IDynamicPath parent)
    {
        super();
        this.name = name;
        this.type = type;
        this.taskType = taskType;
        this.boundEntityId = boundEntityId;
        this.boundEntityName = boundEntityName;
        this.wipDynamicPath = wipDynamicPath;
        this.publishDynamicPath = publishDynamicPath;
        this.oldDynamicPath = oldDynamicPath;
        this.project = project;
        this.parent = parent;
        if (type.equals("file"))
        {
            createDynamicPathHProxy();
        }
    }

    private void createDynamicPathHProxy()
    {
        IDynamicPath dynamicPath = new DynamicPathH(this.name + "Proxy", "proxy", this.boundEntityId,
                this.boundEntityName, this.taskType, this.wipDynamicPath, this.publishDynamicPath, this.oldDynamicPath,
                this.project, this);
        this.getChildren().add(dynamicPath);
    }

    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_dynamicpath"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    @Column(name = "dynamic_path_name")
    public String getName()
    {
        return name;
    }

    @Column(name = "dynamic_path_type")
    public String getType()
    {
        return type;
    }

    @ManyToOne(targetEntity = ProjectH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @JoinColumn(name = "dynamic_path_project")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    @Column(name = "dynamic_path_boundentity")
    public Long getBoundEntityId()
    {
        return boundEntityId;
    }

    @Column(name = "dynamicpath_boundentityname")
    public String getBoundEntityName()
    {
        return boundEntityName;
    }

    @ManyToOne(targetEntity = TaskTypeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @JoinColumn(name = "dynamic_path_tasktype")
    @NotFound(action = NotFoundAction.IGNORE)
    public ITaskType getTaskType()
    {
        return taskType;
    }

    @Column(name = "dynamic_path_wipdynamicpath")
    public String getWipDynamicPath()
    {
        return wipDynamicPath;
    }

    @Column(name = "dynamic_path_publishdynamicpath")
    public String getPublishDynamicPath()
    {
        return publishDynamicPath;
    }

    @Column(name = "dynamic_path_olddynamicpath")
    public String getOldDynamicPath()
    {
        return oldDynamicPath;
    }

    @ManyToOne(targetEntity = DynamicPathH.class, fetch = FetchType.LAZY)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IDynamicPath getParent()
    {
        return parent;
    }

    @OneToMany(targetEntity = DynamicPathH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parent")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Collection<IDynamicPath> getChildren()
    {
        if (children == null)
            children = new ArrayList<IDynamicPath>();
        return children;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public void setType(final String type)
    {
        this.type = type;
    }

    public void setProject(final IProject project)
    {
        this.project = project;
    }

    public void setBoundEntityId(final Long boundEntityId)
    {
        this.boundEntityId = boundEntityId;
    }

    public void setBoundEntityName(final String boundEntityName)
    {
        this.boundEntityName = boundEntityName;
    }

    public void setTaskType(final ITaskType taskType)
    {
        this.taskType = taskType;
    }

    public void setWipDynamicPath(final String wipDynamicPath)
    {
        this.wipDynamicPath = wipDynamicPath;
    }

    public void setPublishDynamicPath(final String publishDynamicPath)
    {
        this.publishDynamicPath = publishDynamicPath;
    }

    public void setOldDynamicPath(final String oldDynamicPath)
    {
        this.oldDynamicPath = oldDynamicPath;
    }

    public void setParent(final IDynamicPath parent)
    {
        this.parent = parent;
    }

    public void setChildren(Collection<IDynamicPath> children)
    {
        this.children = children;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();

        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((boundEntityId == null) ? 0 : boundEntityId.hashCode());
        result = prime * result + ((boundEntityName == null) ? 0 : boundEntityName.hashCode());
        result = prime * result + ((taskType == null) ? 0 : taskType.hashCode());
        result = prime * result + ((wipDynamicPath == null) ? 0 : wipDynamicPath.hashCode());
        result = prime * result + ((publishDynamicPath == null) ? 0 : publishDynamicPath.hashCode());
        result = prime * result + ((oldDynamicPath == null) ? 0 : oldDynamicPath.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!super.equals(obj))
            return false;

        if (getClass() != obj.getClass())
            return false;
        DynamicPathH other = (DynamicPathH) obj;

        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (boundEntityId == null)
        {
            if (other.boundEntityId != null)
                return false;
        }
        else if (!boundEntityId.equals(other.boundEntityId))
            return false;
        if (boundEntityName == null)
        {
            if (other.boundEntityName != null)
                return false;
        }
        else if (!boundEntityName.equals(other.boundEntityName))
            return false;
        if (taskType == null)
        {
            if (other.taskType != null)
                return false;
        }
        else if (!taskType.equals(other.taskType))
            return false;
        if (wipDynamicPath == null)
        {
            if (other.wipDynamicPath != null)
                return false;
        }
        else if (!wipDynamicPath.equals(other.wipDynamicPath))
            return false;
        if (oldDynamicPath == null)
        {
            if (other.oldDynamicPath != null)
                return false;
        }
        else if (!oldDynamicPath.equals(other.oldDynamicPath))
            return false;
        if (publishDynamicPath == null)
        {
            if (other.publishDynamicPath != null)
                return false;
        }
        else if (!publishDynamicPath.equals(other.publishDynamicPath))
            return false;

        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;

        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;

        return true;
    }

    public String toString()
    {
        return new ToStringBuilder(this).append("id", id).append("version", version).append("name", name).append(
                "boundEntityId", boundEntityId).append("boundEntityName", boundEntityName).append("taskType", taskType)
                .append("wipDynamicPath", wipDynamicPath).append("publishDynamicPath", publishDynamicPath).append(
                        "oldDynamicPath", oldDynamicPath).append("type", type).toString();
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILDynamicPath, IDynamicPath> defaultTranslator()
    {
        return Translators.dynamicpath;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public Collection<IDynamicPath> allChildren(boolean includeSelf)
    {
        List<IDynamicPath> allChildren = new ArrayList<IDynamicPath>();
        if (includeSelf)
            allChildren.add(this);
        for (IDynamicPath dynmaicPath : getChildren())
            allChildren.addAll(dynmaicPath.allChildren(true));

        return allChildren;
    }

    public IBase boundEntity() throws Hd3dException
    {
        IBase ret = null;
        IPersist<? extends IBase> persistor = Persistors.getPersistor(getBoundEntityName());
        if (persistor != null)
        {
            ret = persistor.getById(getBoundEntityId());
        }

        return ret;
    }

    @Override
    public void postDisable() throws Hd3dException
    {
        Collection<IDynamicPath> children = CollectUtils.selectNotNull(getChildren());
        Persistors.dynamicpath.disableCollection(children);
    }

}
