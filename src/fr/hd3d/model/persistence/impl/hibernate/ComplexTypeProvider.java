package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.audit.Audit;
import fr.hd3d.model.persistence.impl.hibernate.AbsenceH.AbsenceComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteH.ApprovalNoteComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionH.AssetComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.ComputerH.ComputerComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.ContractH.ContractComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.DeviceH.DeviceComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.EventH.EventComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionH.FileComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.GraphicalAnnotationH.GraphicalAnnotationComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.LicenseH.LicenseComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.MileStoneH.MileStoneComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.PlanningH.PlanningComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.PlayListRevisionH.PlayListRevisionComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.ScreenH.ScreenComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.ScriptH.ScriptComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.SheetH.SheetComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.SkillLevelH.SkillLevelComplexTypeProvider;
import fr.hd3d.services.security.templates.ProjectSecurityTemplateH;
import fr.hd3d.services.security.templates.RoleTemplateH;


/**
 * Hub containing all the ComplexTypeProviders.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ComplexTypeProvider
{
    public static final ComplexTypeProvider INSTANCE = new ComplexTypeProvider();

    private static HashMap<Class<?>, Class<? extends IComplexTypeProvider>> _MAP = new HashMap<Class<?>, Class<? extends IComplexTypeProvider>>();

    static
    {
        _MAP.put(ActivityH.class, ActivityH.ActivityComplexTypeProvider.class);
        _MAP.put(TaskActivityH.class, TaskActivityH.ActivityComplexTypeProvider.class);
        _MAP.put(SimpleActivityH.class, SimpleActivityH.SimpleActivityComplexTypeProvider.class);
        _MAP.put(ProjectH.class, ProjectH.ProjectComplexTypeProvider.class);
        _MAP.put(TaskH.class, TaskH.TaskComplexTypeProvider.class);
        _MAP.put(PersonH.class, DefaultComplexTypeProvider.class);
        _MAP.put(PersonDayH.class, PersonDayH.PersonDayComplexTypeProvider.class);
        _MAP.put(WorkingTimeH.class, WorkingTimeH.WorkingTimeComplexTypeProvider.class);
        _MAP.put(PlanningH.class, PlanningComplexTypeProvider.class);
        _MAP.put(TaskGroupH.class, DefaultComplexTypeProvider.class);
        _MAP.put(TaskChangesH.class, TaskChangesH.TaskChangesComplexeTypeProvider.class);
        _MAP.put(TaskBaseH.class, TaskBaseH.TaskBaseComplexTypeProvider.class);
        _MAP.put(ConstituentH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ConstituentLinkH.class, DefaultComplexTypeProvider.class);
        _MAP.put(CategoryH.class, DefaultComplexTypeProvider.class);
        _MAP.put(SequenceH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ShotH.class, DefaultComplexTypeProvider.class);
        _MAP.put(HintH.class, DefaultComplexTypeProvider.class);
        _MAP.put(FactH.class, DefaultComplexTypeProvider.class);
        _MAP.put(FactTypeH.class, DefaultComplexTypeProvider.class);
        _MAP.put(CompositionH.class, DefaultComplexTypeProvider.class);
        _MAP.put(SheetH.class, SheetComplexTypeProvider.class);
        _MAP.put(ItemGroupH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ItemH.class, ItemH.ItemComplexTypeProvider.class);
        _MAP.put(TagH.class, DefaultComplexTypeProvider.class);
        _MAP.put(TagCategoryH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ClassDynMetaDataTypeH.class, DefaultComplexTypeProvider.class);
        _MAP.put(DynMetaDataTypeH.class, DefaultComplexTypeProvider.class);
        _MAP.put(DynMetaDataValueH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ScriptH.class, ScriptComplexTypeProvider.class);
        _MAP.put(AssetRevisionH.class, AssetComplexTypeProvider.class);
        _MAP.put(AssetRevisionLinkH.class, DefaultComplexTypeProvider.class);
        _MAP.put(AssetRevisionGroupH.class, DefaultComplexTypeProvider.class);
        _MAP.put(FileRevisionH.class, FileComplexTypeProvider.class);
        // _MAP.put(AssetRevisionFileRevisionH.class, DefaultComplexTypeProvider.class);
        _MAP.put(FileRevisionLinkH.class, DefaultComplexTypeProvider.class);
        _MAP.put(AssetAbstractH.class, DefaultComplexTypeProvider.class);
        _MAP.put(PlayListRevisionH.class, PlayListRevisionComplexTypeProvider.class);
        _MAP.put(PlayListEditH.class, DefaultComplexTypeProvider.class);
        _MAP.put(FileRevisionAnnotationH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ResourceGroupH.class, DefaultComplexTypeProvider.class);
        _MAP.put(SkillH.class, DefaultComplexTypeProvider.class);
        _MAP.put(SkillLevelH.class, SkillLevelComplexTypeProvider.class);
        _MAP.put(QualificationH.class, DefaultComplexTypeProvider.class);
        _MAP.put(OrganizationH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ContractH.class, ContractComplexTypeProvider.class);
        _MAP.put(SoftwareH.class, DefaultComplexTypeProvider.class);
        _MAP.put(LicenseH.class, LicenseComplexTypeProvider.class);
        _MAP.put(ComputerH.class, ComputerComplexTypeProvider.class);
        _MAP.put(DeviceH.class, DeviceComplexTypeProvider.class);
        _MAP.put(ScreenH.class, ScreenComplexTypeProvider.class);
        _MAP.put(StudioMapH.class, DefaultComplexTypeProvider.class);
        _MAP.put(RoomH.class, DefaultComplexTypeProvider.class);
        _MAP.put(PoolH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ManufacturerH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ProcessorH.class, DefaultComplexTypeProvider.class);
        _MAP.put(DeviceModelH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ScreenModelH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ComputerModelH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ComputerTypeH.class, DefaultComplexTypeProvider.class);
        _MAP.put(DeviceTypeH.class, DefaultComplexTypeProvider.class);
        _MAP.put(FileAttributeH.class, DefaultComplexTypeProvider.class);
        _MAP.put(RoleH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ApprovalNoteH.class, ApprovalNoteComplexTypeProvider.class);
        _MAP.put(ApprovalNoteTypeH.class, DefaultComplexTypeProvider.class);
        _MAP.put(Audit.class, Audit.AuditComplexTypeProvider.class);
        _MAP.put(EventH.class, EventComplexTypeProvider.class);
        _MAP.put(MileStoneH.class, MileStoneComplexTypeProvider.class);
        _MAP.put(ExtraLineH.class, DefaultComplexTypeProvider.class);
        _MAP.put(TaskTypeH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ProjectTypeH.class, DefaultComplexTypeProvider.class);
        _MAP.put(AbsenceH.class, AbsenceComplexTypeProvider.class);
        _MAP.put(EntityTaskLinkH.class, DefaultComplexTypeProvider.class);
        _MAP.put(StepH.class, DefaultComplexTypeProvider.class);
        _MAP.put(GraphicalAnnotationH.class, GraphicalAnnotationComplexTypeProvider.class);
        _MAP.put(ListValuesH.class, DefaultComplexTypeProvider.class);
        _MAP.put(ProjectSecurityTemplateH.class, DefaultComplexTypeProvider.class);
        _MAP.put(RoleTemplateH.class, DefaultComplexTypeProvider.class);
        _MAP.put(DynamicPathH.class, DefaultComplexTypeProvider.class);
    }

    private final static Map<Class<?>, Class<? extends IComplexTypeProvider>> MAP = Collections.unmodifiableMap(_MAP);

    /**
     * 
     */
    private ComplexTypeProvider()
    {}

    public static void shutdown()
    {
        if (_MAP != null)
        {
            _MAP.clear();
        }
        _MAP = null;
    }

    /**
     * @param clazz
     * @return the ComplexTypeProvider used in the persisted class.
     */
    public IComplexTypeProvider getProvider(Class<?> clazz)
    {
        try
        {
            IComplexTypeProvider ret = null;
            Class<? extends IComplexTypeProvider> prov = MAP.get(clazz);
            if (prov != null)
            {
                ret = prov.newInstance();
            }

            return ret;
        }
        catch (InstantiationException e)
        {
            throw new Hd3dRuntimeException("unable to instanciate ComplexTypeProvider for class:"
                    + getClass().getCanonicalName() + e.getMessage());
        }
        catch (IllegalAccessException e)
        {
            throw new Hd3dRuntimeException("unable to instanciate ComplexTypeProvider for class:"
                    + getClass().getCanonicalName() + e.getMessage());
        }
        catch (RuntimeException e)
        {
            throw new Hd3dRuntimeException("unable to instanciate ComplexTypeProvider for class:"
                    + getClass().getCanonicalName() + e.getMessage());
        }
    }

    public static IComplexTypeProvider defaultComplexTypeProvider()
    {
        return new DefaultComplexTypeProvider();
    }

    public static class DefaultComplexTypeProvider implements IComplexTypeProvider
    {
        public Object getValue(String field, String value)
        {
            return null;
        }

        public List<String> getValidEnum(String field)
        {
            return Collections.emptyList();
        }

        public boolean isValidTypeField(String field)
        {
            return false;
        }
    }

}
