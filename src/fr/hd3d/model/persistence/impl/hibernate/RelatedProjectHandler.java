package fr.hd3d.model.persistence.impl.hibernate;

import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.persistence.Project;


/**
 * For given entity, returns its parent. Note: reason why this is not implemented as an interface is that some entities
 * implements getParent and some not, since the concept of Parent differs, and Hibernate enforces the java bean
 * convention (an attribute must match getters and setters properly). One can use @Transier method, but it will fool
 * reflection API.
 * 
 * @author try.lam
 * 
 */
public final class RelatedProjectHandler
{
    private RelatedProjectHandler()
    {}

    public static IProject getProject(Persistent object)
    {
        IProject ret = null;

        if (Project.class.isInstance(object))
        {
            ret = retrieveProject(object);
        }
        else
        {
            ret = retrieveProject(ParentHandler.getParent(object));
        }

        return ret;
    }

    private static IProject retrieveProject(Persistent object)
    {
        IProject ret = null;

        if (object != null && Project.class.isInstance(object))
        {
            ret = ((Project) object).getProject();
        }

        return ret;
    }
}
