package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.common.client.enums.EAssetStatus;
import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;


/**
 * 
 * @author try LAM
 * 
 */
@Entity
@Table(name = "hd3d_assetrevision")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class AssetRevisionH extends BaseH implements IAssetRevision
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String TIMESTAMP_FIELD = "creationDate";
    private static final String LASTOPERATIONDATE_FIELD = "lastOperationDate";
    private static final String STATUS_FIELD = "status";

    private Set<IAssetRevisionGroup> assetRevisionGroups;
    private IProject project;
    private Date creationDate;
    private String key;
    private String variation;
    private Integer revision;
    private IPerson creator;
    private ITaskType taskType;
    private String name;
    private EAssetStatus status;
    private Date lastOperationDate;
    private IPerson lastUser;
    private String comment;
    private String validity;// Enum ?

    private String mountPointWipPath;
    private String wipPath;
    private String mountPointPublishPath;
    private String publishPath;
    private String mountPointOldPath;
    private String oldPath;

    private List<IFileRevision> fileRevisions;

    @OneToMany(targetEntity = FileRevisionH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "assetRevision")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @Fetch(value = FetchMode.JOIN)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @BatchSize(size = 50)
    public List<IFileRevision> getFileRevisions()
    {
        return fileRevisions;
    }

    public void setFileRevisions(List<IFileRevision> fileRevisions)
    {
        this.fileRevisions = fileRevisions;
    }

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_assetrevision"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public AssetRevisionH()
    {}

    public AssetRevisionH(Long id, java.sql.Timestamp version, IProject project, Date creationDate, String key,
            String variation, Integer revision, IPerson creator, ITaskType type, String name, EAssetStatus status,
            Date lastOperationDate, IPerson lastUser, String comment, String validity, String mountPointWipPath,
            String wipPath, String mountPointPublishPath, String publishPath, String mountPointOldPath, String oldPath)
    {
        super(id, version);
        this.project = project;
        this.creationDate = creationDate;
        this.key = key;
        this.variation = variation;
        if (revision != null)
        {
            this.revision = revision;
        }
        this.creator = creator;
        this.taskType = type;
        this.name = name;
        this.status = status;
        this.lastOperationDate = lastOperationDate;
        this.lastUser = lastUser;
        this.comment = comment;
        this.validity = validity;

        this.wipPath = wipPath;
        this.mountPointWipPath = mountPointWipPath;
        this.publishPath = publishPath;
        this.mountPointPublishPath = mountPointPublishPath;
        this.oldPath = oldPath;
        this.mountPointOldPath = mountPointOldPath;
    }

    public AssetRevisionH(IProject project, Date creationDate, String key, String variation, Integer revision,
            IPerson creator, ITaskType type, String name, EAssetStatus status, Date lastOperationDate,
            IPerson lastUser, String comment, String validity)
    {
        this(null, null, project, creationDate, key, variation, revision, creator, type, name, status,
                lastOperationDate, lastUser, comment, validity, "", "", "", "", "", "");
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToOne(targetEntity = ProjectH.class)
    @JoinColumn(name = "assetrevision_project_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    @ManyToMany(targetEntity = AssetRevisionGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_assetrevisiongroup__assetrevision", joinColumns = @JoinColumn(name = "assetrevision_id"), inverseJoinColumns = @JoinColumn(name = "assetrevisiongroup_id"))
    // @IndexColumn is IMPORTANT: to avoid "cannot recreate collection while filter is enabled" error
    // @IndexColumn(name = "assetrevision_index", base = 0)
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IAssetRevisionGroup> getAssetRevisionGroups()
    {
        if (assetRevisionGroups == null)
            assetRevisionGroups = new HashSet<IAssetRevisionGroup>();
        return assetRevisionGroups;
    }

    @Column(name = "assetrevision_creationdate")
    public Date getCreationDate()
    {
        return creationDate;
    }

    @Column(name = "assetrevision_key")
    @NotEmpty
    public String getKey()
    {
        return key;
    }

    @Column(name = "assetrevision_variation")
    public String getVariation()
    {
        return variation;
    }

    @Column(name = "assetrevision_revision")
    public Integer getRevision()
    {
        if (this.revision == null)
        {
            this.revision = 1;
        }
        return revision;
    }

    @OneToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "assetrevision_creator")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getCreator()
    {
        return creator;
    }

    @OneToOne(targetEntity = TaskTypeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "assetrevision_tasktype")
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public ITaskType getTaskType()
    {
        return taskType;
    }

    @Column(name = "assetrevision_name")
    public String getName()
    {
        return name;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "assetrevision_status", nullable = false)
    public EAssetStatus getStatus()
    {
        return status;
    }

    @Column(name = "assetrevision_lastoperationdate")
    public Date getLastOperationDate()
    {
        return lastOperationDate;
    }

    @OneToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "assetrevision_lastuser")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getLastUser()
    {
        return lastUser;
    }

    @Column(name = "assetrevision_comment")
    public String getComment()
    {
        return comment;
    }

    @Column(name = "assetrevision_validity")
    public String getValidity()
    {
        return validity;
    }

    @Column(name = "assetrevision_wippath")
    public String getWipPath()
    {
        return wipPath;
    }

    @Column(name = "assetrevision_publishpath")
    public String getPublishPath()
    {
        return publishPath;
    }

    @Column(name = "assetrevision_oldpath")
    public String getOldPath()
    {
        return oldPath;
    }

    @Column(name = "assetrevision_mountpoint_wippath")
    public String getMountPointWipPath()
    {
        return mountPointWipPath;
    }

    @Column(name = "assetrevision_mountpoint_publishpath")
    public String getMountPointPublishPath()
    {
        return mountPointPublishPath;
    }

    @Column(name = "assetrevision_mountpoint_oldpath")
    public String getMountPointOldPath()
    {
        return mountPointOldPath;
    }

    public void setMountPointWipPath(String mountPointWipPath)
    {
        this.mountPointWipPath = mountPointWipPath;
    }

    public void setMountPointPublishPath(String mountPointPublishPath)
    {
        this.mountPointPublishPath = mountPointPublishPath;
    }

    public void setMountPointOldPath(String mountPointOldPath)
    {
        this.mountPointOldPath = mountPointOldPath;
    }

    public void setProject(IProject project)
    {
        this.project = project;
    }

    public void setAssetRevisionGroups(Set<IAssetRevisionGroup> assetRevisionGroups)
    {
        this.assetRevisionGroups = assetRevisionGroups;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public void setVariation(String variation)
    {
        this.variation = variation;
    }

    public void setRevision(Integer revision)
    {
        this.revision = revision;
    }

    public void setCreator(IPerson creator)
    {
        this.creator = creator;
    }

    public void setTaskType(ITaskType type)
    {
        this.taskType = type;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setStatus(EAssetStatus status)
    {
        this.status = status;
    }

    public void setLastOperationDate(Date lastOperationDate)
    {
        this.lastOperationDate = lastOperationDate;
    }

    public void setLastUser(IPerson lastUser)
    {
        this.lastUser = lastUser;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setValidity(String validity)
    {
        this.validity = validity;
    }

    public void setWipPath(String wipPath)
    {
        this.wipPath = wipPath;
    }

    public void setPublishPath(String publishPath)
    {
        this.publishPath = publishPath;
    }

    public void setOldPath(String oldPath)
    {
        this.oldPath = oldPath;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + ((creator == null) ? 0 : creator.hashCode());
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((lastOperationDate == null) ? 0 : lastOperationDate.hashCode());
        result = prime * result + ((lastUser == null) ? 0 : lastUser.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((revision == null) ? 0 : revision.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((taskType == null) ? 0 : taskType.hashCode());
        result = prime * result + ((validity == null) ? 0 : validity.hashCode());
        result = prime * result + ((variation == null) ? 0 : variation.hashCode());
        result = prime * result + ((wipPath == null) ? 0 : wipPath.hashCode());
        result = prime * result + ((publishPath == null) ? 0 : publishPath.hashCode());
        result = prime * result + ((oldPath == null) ? 0 : oldPath.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AssetRevisionH other = (AssetRevisionH) obj;
        if (creationDate == null)
        {
            if (other.creationDate != null)
                return false;
        }
        else if (!creationDate.equals(other.creationDate))
            return false;
        if (creator == null)
        {
            if (other.creator != null)
                return false;
        }
        else if (!creator.equals(other.creator))
            return false;
        if (key == null)
        {
            if (other.key != null)
                return false;
        }
        else if (!key.equals(other.key))
            return false;
        if (lastOperationDate == null)
        {
            if (other.lastOperationDate != null)
                return false;
        }
        else if (!lastOperationDate.equals(other.lastOperationDate))
            return false;
        if (lastUser == null)
        {
            if (other.lastUser != null)
                return false;
        }
        else if (!lastUser.equals(other.lastUser))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (revision == null)
        {
            if (other.revision != null)
                return false;
        }
        else if (!revision.equals(other.revision))
            return false;
        if (status == null)
        {
            if (other.status != null)
                return false;
        }
        else if (!status.equals(other.status))
            return false;
        if (taskType == null)
        {
            if (other.taskType != null)
                return false;
        }
        else if (!taskType.equals(other.taskType))
            return false;
        if (validity == null)
        {
            if (other.validity != null)
                return false;
        }
        else if (!validity.equals(other.validity))
            return false;
        if (variation == null)
        {
            if (other.variation != null)
                return false;
        }
        else if (!variation.equals(other.variation))
            return false;
        if (wipPath == null)
        {
            if (other.wipPath != null)
                return false;
        }
        else if (!wipPath.equals(other.wipPath))
            return false;
        if (publishPath == null)
        {
            if (other.publishPath != null)
                return false;
        }
        else if (!publishPath.equals(other.publishPath))
            return false;
        if (oldPath == null)
        {
            if (other.oldPath != null)
                return false;
        }
        else if (!oldPath.equals(other.oldPath))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    // @Transient
    // public Set<IAssetRevisionFileRevision> getAssetRevisionFileRevisions() throws Hd3dException
    // {
    // String[] properties = { "assetKey", "assetVariation", "assetRevision" };
    // Object[] values = { this.getKey(), this.getVariation(), this.getRevision() };
    //
    // List<IAssetRevisionFileRevision> assetRevisionFileRevisions = Persistors.assetrevisionfilerevision.getByValues(
    // null, properties, values, "AND");
    //
    // if (assetRevisionFileRevisions == null)
    // return new HashSet<IAssetRevisionFileRevision>();
    // else
    // return new HashSet<IAssetRevisionFileRevision>(assetRevisionFileRevisions);
    // }

    @Transient
    public Set<IAssetRevisionLink> getAssetRevisionLinks_ThisAsInput() throws Hd3dException
    {

        List<IAssetRevisionLink> assetRevisionLinks = Persistors.assetrevisionlink.getByValue("inputAsset.id", this
                .getId());

        if (assetRevisionLinks == null)
            return new HashSet<IAssetRevisionLink>();
        else
            return new HashSet<IAssetRevisionLink>(assetRevisionLinks);
    }

    @Transient
    public Set<IAssetRevisionLink> getAssetRevisionLinks_ThisAsOutput() throws Hd3dException
    {

        List<IAssetRevisionLink> assetRevisionLinks = Persistors.assetrevisionlink.getByValue("outputAsset.id", this
                .getId());

        if (assetRevisionLinks == null)
            return new HashSet<IAssetRevisionLink>();
        else
            return new HashSet<IAssetRevisionLink>(assetRevisionLinks);
    }

    @Transient
    public Set<IAssetRevisionLink> getAssetRevisionLinks() throws Hd3dException
    {
        Set<IAssetRevisionLink> assetRevisionLinks = new HashSet<IAssetRevisionLink>();
        assetRevisionLinks.addAll(getAssetRevisionLinks_ThisAsInput());
        assetRevisionLinks.addAll(getAssetRevisionLinks_ThisAsOutput());
        return assetRevisionLinks;
    }

    public void addAssetRevisionGroup(IAssetRevisionGroup group)
    {
        getAssetRevisionGroups().add(group);
    }

    public void removeAssetRevisionGroup(IAssetRevisionGroup group)
    {
        getAssetRevisionGroups().remove(group);
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        // cascade disable
        Collection<IAssetRevisionLink> assetRevisionLinks = CollectUtils.selectNotNull(getAssetRevisionLinks());
        Persistors.assetrevisionlink.disableCollection(assetRevisionLinks);

        Collection<IFileRevision> fileRevisions = CollectUtils.selectNotNull(getFileRevisions());
        for (IFileRevision fileRevision : fileRevisions)
        {
            fileRevision.setAssetRevision(null);
        }
        // Collection<IAssetRevisionFileRevision> assetRevisionFileRevisions = CollectUtils
        // .selectNotNull(getAssetRevisionFileRevisions());
        // Persistors.assetrevisionfilerevision.disableCollection(assetRevisionFileRevisions);
    }

    public IBaseTranslator<ILAssetRevision, IAssetRevision> defaultTranslator()
    {
        return Translators.assetrevision;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public boolean belongsTo(IAssetRevisionGroup group)
    {
        return getAssetRevisionGroups().contains(this);
    }

    public String toString()
    {
        return name;
    }

    @Transient
    public String getMountPointByState(EFileState state)
    {
        String mountPoint = "";
        switch (state)
        {
            case WIP:
                mountPoint = getMountPointWipPath();
                break;
            case PUBLISH:
                mountPoint = getMountPointPublishPath();
                break;
            case OLD:
                mountPoint = getMountPointOldPath();
                break;
        }
        return mountPoint;
    }

    // public String toString()
    // {
    // return new ToStringBuilder(this).appendSuper(super.toString()).append("project", project).append(
    // "creationDate", creationDate).append("key", key).append("variation", variation).append("revision",
    // revision).append("creator", creator).append("taskType", taskType).append("name", name).append("status",
    // status).append("lastOperationDate", lastOperationDate).append("lastUser", lastUser).append("validity",
    // validity).toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class AssetComplexTypeProvider implements IComplexTypeProvider
    {
        public List<String> getValidEnum(String field)
        {
            List<String> ret;

            if (STATUS_FIELD.equals(field))
            {
                ret = EnumUtils.getEnumStringValues(EAssetStatus.class);
            }
            else
            {
                ret = Collections.emptyList();
            }

            return ret;
        }

        public Object getValue(String field, String value)
        {
            Object ret = null;

            if (isValidTypeField(field))
            {
                if (STATUS_FIELD.equals(field))
                {
                    ret = EnumUtils.getEnumValueIgnoreCase(EAssetStatus.class, value);
                }
                else if (TIMESTAMP_FIELD.equals(field) || LASTOPERATIONDATE_FIELD.equals(field))
                {
                    ret = fr.hd3d.utils.StringUtils.parseDate(value);
                }
            }

            return ret;
        }

        public boolean isValidTypeField(String field)
        {
            return STATUS_FIELD.equals(field) || TIMESTAMP_FIELD.equals(field) || LASTOPERATIONDATE_FIELD.equals(field);
        }
    }

    @Transient
    public Collection<IFileRevision> getLastFileRevisions()
    {
        HashMap<String, IFileRevision> map = new HashMap<String, IFileRevision>();
        for (IFileRevision fileRevision : fileRevisions)
        {
            IFileRevision f = map.get(fileRevision.getKey());
            if (f == null)
            {
                map.put(fileRevision.getKey(), fileRevision);
            }
            else
            {
                if (fileRevision.getRevision().longValue() > f.getRevision().longValue())
                {
                    map.put(fileRevision.getKey(), fileRevision);
                }
            }
        }
        return map.values();
    }

    @Override
    public <T extends IBase> void merge(Collection<T> coll, Collection<Long> ids, Class<T> clazz) throws Hd3dException
    {
    // TODO Auto-generated method stub

    }

}
