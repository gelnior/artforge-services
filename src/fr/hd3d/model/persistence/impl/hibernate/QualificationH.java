package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILQualification;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.IQualification;
import fr.hd3d.model.persistence.ISkill;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * Qualification hibernate object needed for database persistence.
 * 
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_qualification")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class QualificationH extends SimpleH implements IQualification
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Set<ISkill> skills;
    private Set<IContract> contracts;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_qualification"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public QualificationH()
    {}

    public QualificationH(Long id, Timestamp version, String name, Set<ISkill> skills)
    {
        super(id, version, name);

        this.skills = skills;
    }

    public QualificationH(String name, Set<ISkill> skills)
    {
        this(null, null, name, skills);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToMany(targetEntity = SkillH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_qualification__skills", joinColumns = @JoinColumn(name = "qualification_id"), inverseJoinColumns = @JoinColumn(name = "skill_id"))
    @BatchSize(size = 50)
    @Fetch(value = FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<ISkill> getSkills()
    {
        if (this.skills == null)
        {
            this.skills = new HashSet<ISkill>();
        }
        return this.skills;
    }

    public void setSkills(Set<ISkill> skills)
    {
        this.skills = skills;
    }

    @OneToMany(targetEntity = ContractH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "qualification")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IContract> getContracts()
    {
        return contracts;
    }

    public void setContracts(Set<IContract> contracts)
    {
        this.contracts = contracts;
    }

    /*------------------
     * equal & hashcode
     ------------------*/

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        for (ISkill s : getSkills())
        {
            s.getQualifications().remove(this);
        }
    }

    public IBaseTranslator<ILQualification, IQualification> defaultTranslator()
    {
        return Translators.qualification;
    }

    @Override
    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
