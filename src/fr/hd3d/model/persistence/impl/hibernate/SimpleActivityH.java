package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;

import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.EnumUtils;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Table(name = "hd3d_simple_activity")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class SimpleActivityH extends ActivityH implements ISimpleActivity
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String ACTIVITY_FIELD = "type";
    private IPerson worker;
    private ESimpleActivityType type;
    private IProject project;

    /*-------------
     * Constructors
     -------------*/
    public SimpleActivityH()
    {}

    public SimpleActivityH(Long id, java.sql.Timestamp version, IPersonDay day, Long duration, String comment,
            ESimpleActivityType type, IProject project, IPerson worker, IPerson filledBy, Date filledDate)
    {
        super(id, version, day, duration, comment, filledBy, filledDate);
        this.type = type;
        this.worker = worker;
        this.project = project;
    }

    public SimpleActivityH(IPersonDay day, Long duration, String comment, ESimpleActivityType type, IProject project,
            IPerson worker, IPerson filledBy, Date filledDate)
    {
        this(null, null, day, duration, comment, type, project, worker, filledBy, filledDate);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Enumerated(value = EnumType.STRING)
    @Column(name = "simple_activity_type_id", nullable = false)
    public ESimpleActivityType getType()
    {
        return type;
    }

    @ManyToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "simple_activity_person_id")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getWorker()
    {
        return worker;
    }

    public void setType(ESimpleActivityType type)
    {
        this.type = type;
    }

    public void setWorker(IPerson worker)
    {
        this.worker = (PersonH) worker;
    }

    @ManyToOne(targetEntity = ProjectH.class)
    @JoinColumn(name = "simple_activity_project_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    public void setProject(IProject project)
    {
        this.project = (ProjectH) project;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    /**
     * Equality definition What's make an activity unique is its project, type and worker
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((getProject() == null) ? 0 : getProject().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getWorker() == null) ? 0 : getWorker().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof SimpleActivityH))
            return false;
        SimpleActivityH other = (SimpleActivityH) obj;
        if (getProject() == null)
        {
            if (other.getProject() != null)
                return false;
        }
        else if (!getProject().equals(other.getProject()))
            return false;
        if (getType() == null)
        {
            if (other.getType() != null)
                return false;
        }
        else if (!getType().equals(other.getType()))
            return false;
        if (getWorker() == null)
        {
            if (other.getWorker() != null)
                return false;
        }
        else if (!getWorker().equals(other.getWorker()))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    public String taskName()
    {
        return getType().toString();
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILSimpleActivity, ISimpleActivity> defaultTranslator()
    {
        return Translators.simpleActivity;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("workerId", worker == null ? "null" : worker.getId());
        b.append("type", type);
        b.append("projectId", project == null ? "null" : project.getId());

        return b.toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class SimpleActivityComplexTypeProvider extends ActivityComplexTypeProvider
    {
        public Object getValue(String field, String value)
        {
            if (ACTIVITY_FIELD.equals(field))
                return EnumUtils.getEnumValueIgnoreCase(ESimpleActivityType.class, value);
            else
                return super.getValue(field, value);
        }

        public List<String> getValidEnum(String field)
        {
            if (ACTIVITY_FIELD.equals(field))
                return EnumUtils.getEnumStringValues(ESimpleActivityType.class);
            else
                return super.getValidEnum(field);
        }

        public boolean isValidTypeField(String field)
        {
            if (ACTIVITY_FIELD.equals(field))
                return true;
            else
                return super.isValidTypeField(field);
        }
    }

}
