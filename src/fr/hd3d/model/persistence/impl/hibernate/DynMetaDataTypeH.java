package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Table(name = "hd3d_dynmetadatatype")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class DynMetaDataTypeH extends BaseH implements IDynMetaDataType
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String name;
    private String nature;// scalar, list
    private String type;// String, List, hql query, etc...
    private String defaultValue;
    private String structName;// name of the structure holding values
    private Long structId;// id of the structure holding values

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_dynmetadatatype"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public DynMetaDataTypeH()
    {}

    public DynMetaDataTypeH(Long id, Timestamp version, String name, String nature, String type, String defaultValue)
    {
        super(id, version);
        this.name = name;
        this.nature = nature;
        this.type = type;
        this.defaultValue = defaultValue;
    }

    public DynMetaDataTypeH(String name, String nature, String type, String defaultValue)
    {
        this(null, null, name, nature, type, defaultValue);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    /*
     * Note: these fields are not updatable and not nullable because of the difficulty to handle their impact on the
     * rest. Better create new DynMetaDataType
     */
    @Column(name = "dynmetadatatype_name", updatable = false, nullable = false)
    @NotEmpty
    public String getName()
    {
        return name;
    }

    @Column(name = "dynmetadatatype_nature", updatable = false, nullable = false)
    @NotEmpty
    public String getNature()
    {
        return nature;
    }

    @Column(name = "dynmetadatatype_type", updatable = false, nullable = false)
    @NotEmpty
    public String getType()
    {
        return type;
    }

    @Column(name = "dynmetadatatype_defaultvalue", updatable = true)
    public String getDefaultValue()
    {
        return defaultValue;
    }

    @Column(name = "dynmetadatatype_structname", updatable = false)
    public String getStructName()
    {
        return structName;
    }

    @Column(name = "dynmetadatatype_structid", updatable = false)
    public Long getStructId()
    {
        return structId;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setNature(String nature)
    {
        this.nature = nature;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public void setStructName(String structName)
    {
        this.structName = structName;
    }

    public void setStructId(Long structId)
    {
        this.structId = structId;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((defaultValue == null) ? 0 : defaultValue.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((nature == null) ? 0 : nature.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        DynMetaDataTypeH other = (DynMetaDataTypeH) obj;
        if (defaultValue == null)
        {
            if (other.defaultValue != null)
                return false;
        }
        else if (!defaultValue.equals(other.defaultValue))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (nature == null)
        {
            if (other.nature != null)
                return false;
        }
        else if (!nature.equals(other.nature))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    public IBaseTranslator<ILDynMetaDataType, IDynMetaDataType> defaultTranslator()
    {
        return Translators.dynmetadatatype;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // return new ToStringBuilder(this).appendSuper(super.toString()).append("name", name).append("nature", nature)
    // .append("type", type).append("defaultValue", defaultValue).append("structName", structName).append(
    // "structId", structId).toString();
    // }

    @Transient
    public IListValues getListValues()
    {
        /* is it a List ? */
        if (!Const.JAVA_UTIL_LIST.equalsIgnoreCase(getNature()))
            return null;

        if (fr.hd3d.common.client.Entity.LISTVALUES.equalsIgnoreCase(getStructName()))
        {
            /* find out the IListValues */
            IListValues listValues;
            try
            {
                listValues = Persistors.listvalues.getById(getStructId());
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
                listValues = null;
            }
            return listValues;
        }

        return null;
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
