package fr.hd3d.model.persistence.impl.hibernate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resolver.AssetGroupToEntityResolver;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Log;


/**
 * 
 * @author try LAM
 * 
 */
@Entity
@Indexed
@Table(name = "hd3d_assetrevisiongroup")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class AssetRevisionGroupH extends BaseH implements IAssetRevisionGroup
{
    private static final long serialVersionUID = 1L;

    private String name;
    private Set<IAssetRevision> assetRevisions;
    /*
     * "criteria" and "value" act together as a "soft link" to reference the business entity bound to this
     * AssetRevsiongroup. It's up to the caller to interpret these values (company internal convention) Ex: "criteria" =
     * "Constituent" and "value"="<id>" means the
     */
    private String criteria;
    private String value;

    private IProject project;

    private IBase associatedEntity;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_assetrevisiongroup"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public AssetRevisionGroupH()
    {}

    public AssetRevisionGroupH(Long id, java.sql.Timestamp version, String name, Set<IAssetRevision> assetRevisions,
            String criteria, String value, IProject project)
    {
        super(id, version);
        this.name = name;
        this.assetRevisions = assetRevisions;
        this.criteria = criteria;
        this.value = value;
        this.project = project;
    }

    public AssetRevisionGroupH(String name, Set<IAssetRevision> assetRevisions, String criteria, String value,
            IProject project)
    {
        this(null, null, name, assetRevisions, criteria, value, project);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "assetrevisiongroup_name")
    public String getName()
    {
        return name;
    }

    @ManyToMany(targetEntity = AssetRevisionH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_assetrevisiongroup__assetrevision", joinColumns = @JoinColumn(name = "assetrevisiongroup_id"), inverseJoinColumns = @JoinColumn(name = "assetrevision_id"))
    // @IndexColumn is IMPORTANT: to avoid "cannot recreate collection while filter is enabled" error
    // @IndexColumn(name = "assetrevisiongroup_index", base = 0)
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IAssetRevision> getAssetRevisions()
    {
        if (assetRevisions == null)
            assetRevisions = new HashSet<IAssetRevision>();
        return assetRevisions;
    }

    @Column(name = "assetrevisiongroup_criteria")
    public String getCriteria()
    {
        return criteria;
    }

    @Column(name = "assetrevisiongroup_value")
    public String getValue()
    {
        return value;
    }

    @ManyToOne(targetEntity = ProjectH.class)
    @JoinColumn(name = "assetrevisiongroup_project_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setAssetRevisions(Set<IAssetRevision> assetRevisions)
    {
        this.assetRevisions = assetRevisions;
    }

    public void setCriteria(String criteria)
    {
        this.criteria = criteria;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public void setProject(IProject project)
    {
        this.project = project;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((criteria == null) ? 0 : criteria.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AssetRevisionGroupH other = (AssetRevisionGroupH) obj;
        if (criteria == null)
        {
            if (other.criteria != null)
                return false;
        }
        else if (!criteria.equals(other.criteria))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (value == null)
        {
            if (other.value != null)
                return false;
        }
        else if (!value.equals(other.value))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        for (IAssetRevision assetrevision : getAssetRevisions())
        {
            assetrevision.getAssetRevisionGroups().remove(this);
        }
    }

    @Transient
    @Field(index = Index.UN_TOKENIZED, store = Store.YES, name = "project_id")
    public Long getProjectId()
    {
        return CollectUtils.nullSafeId(getProject());
    }

    /**
     * AssetRevisionGroup is linked to another entity by a couple of string (criteria and value). The "standard" way is
     * criteria=entity name and value=entity value. With respect to this rule, returns the linked entity
     */
    @Transient
    public IBase getAssociatedEntity()
    {
        IBase ret;

        if (associatedEntity != null)
        {
            ret = associatedEntity;
        }
        else
        {
            try
            {
                associatedEntity = new AssetGroupToEntityResolver(this).getEntity();
                ret = associatedEntity;
            }
            catch (Exception e)
            {
                ret = null;
            }
        }

        return ret;
    }

    @Transient
    @Field(index = Index.TOKENIZED, store = Store.YES, name = "associatedobj_id")
    public String getAssociatedObjectId()
    {
        try
        {
            return String.valueOf(getAssociatedEntity().getId());
        }
        catch (Exception e)
        {
            return null;
        }
    }

    @Transient
    @Field(name = "associatedobj_name", index = Index.UN_TOKENIZED, store = Store.YES)
    public String getAssociatedEntityName()
    {
        String name = null;
        try
        {
            IBase obj = getAssociatedEntity();
            if (obj != null)
            {
                name = StringUtils.lowerCase(obj.entityName());
            }
        }
        catch (Exception e)
        {
            Log.LOGGER.warn("No associated entity can be found for AssetRevisionGroup id={}", getId());
        }
        return name;
    }

    /**
     * Retrieves FileRevisions ids related to this AssetRevisionGroup, separated by ' '
     */
    @Transient
    @Field(index = Index.TOKENIZED, store = Store.YES, name = "filerevisionsids")
    public String getFileRevisionsIds() throws Hd3dException
    {
        return StringUtils.join(CollectUtils.getIds(getFileRevisions()), ' ');
    }

    /**
     * Retrieve FileRevisions related to this AssetRevisionGroup
     */
    @Transient
    public List<IFileRevision> getFileRevisions() throws Hd3dException
    {
        // Set<IFileRevision> fileRevisions = new HashSet<IFileRevision>();
        //
        // /* lookup fields */
        // final String[] properties = { "assetKey", "assetVariation", "assetRevision" };
        // for (IAssetRevision rev : getAssetRevisions())
        // {
        // /* lookup values */
        // Object[] values = { rev.getKey(), rev.getVariation(), rev.getRevision() };
        //
        // List<IAssetRevisionFileRevision> result = Persistors.assetrevisionfilerevision.getByValues(null,
        // properties, values, "AND");
        //
        // /* if anything found, keep the ids */
        // if (CollectUtils.isNotEmpty(result))
        // for (IAssetRevisionFileRevision a : result)
        // try
        // {
        // fileRevisions.add(a.getFileRevisionObject());
        // }
        // catch (Hd3dPersistenceException e)
        // {
        // Log.LOGGER.warn("Cannot retrieve FileRevision for AssetRevisionFileRevision, id={}", a.getId());
        // }
        // }
        // return new ArrayList<IFileRevision>(fileRevisions);

        ArrayList<IFileRevision> fileRevisions = new ArrayList<IFileRevision>();
        for (IAssetRevision assetRevision : getAssetRevisions())
        {
            fileRevisions.addAll(assetRevision.getFileRevisions());
        }
        return fileRevisions;
    }

    public void addAssetRevision(IAssetRevision assetRevision)
    {
        // getAssetRevisions().add(assetRevision);
        assetRevision.addAssetRevisionGroup(this);
    }

    public void removeAssetRevision(IAssetRevision assetRevision)
    {
        // getAssetRevisions().remove(assetRevision);
        assetRevision.removeAssetRevisionGroup(this);
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILAssetRevisionGroup, IAssetRevisionGroup> defaultTranslator()
    {
        return Translators.assetrevisiongroup;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }
}
