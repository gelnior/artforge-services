package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILSoftware;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.ISkill;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * Software implementation for Hibernate.
 * 
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_software")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class SoftwareH extends SimpleH implements ISoftware
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Set<ILicense> licenses;
    private Set<IComputer> computers;
    private Set<ISkill> skills;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_software"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public SoftwareH()
    {}

    public SoftwareH(Long id, Timestamp version, String name, Set<ILicense> licenses, Set<IComputer> computers,
            Set<ISkill> skills)
    {
        super(id, version, name);
        this.computers = computers;
        this.licenses = licenses;
    }

    public SoftwareH(String name, Set<ILicense> licenses, Set<IComputer> computers, Set<ISkill> skills)
    {
        this(null, null, name, licenses, computers, skills);
    }

    public SoftwareH(String name, String serial, Set<ILicense> licenses, Set<IComputer> computers, Set<ISkill> skills)
    {
        this(null, null, name, licenses, computers, skills);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @OneToMany(targetEntity = LicenseH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "software")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<ILicense> getLicenses()
    {
        return this.licenses;
    }

    public void setLicenses(Set<ILicense> licenses)
    {
        this.licenses = licenses;
    }

    @OneToMany(targetEntity = SkillH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "software")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<ISkill> getSkills()
    {
        return this.skills;
    }

    public void setSkills(Set<ISkill> skills)
    {
        this.skills = skills;
    }

    @ManyToMany(targetEntity = ComputerH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_computer__software", joinColumns = @JoinColumn(name = "software_id"), inverseJoinColumns = @JoinColumn(name = "computer_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IComputer> getComputers()
    {
        if (this.computers == null)
        {
            this.computers = new HashSet<IComputer>();
        }

        return this.computers;
    }

    public void setComputers(Set<IComputer> computers)
    {
        this.computers = computers;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((computers == null) ? 0 : computers.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SoftwareH other = (SoftwareH) obj;
        if (computers == null)
        {
            if (other.computers != null)
                return false;
        }
        else if (!computers.equals(other.computers))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        for (IComputer c : getComputers())
        {
            c.getDevices().remove(this);
        }
    }

    public IBaseTranslator<ILSoftware, ISoftware> defaultTranslator()
    {
        return Translators.software;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("licenseIds", StringUtils.join(CollectUtils.getIds(getLicenses()), ','));
    // b.append("computerIds", StringUtils.join(CollectUtils.getIds(getComputers()), ','));
    // b.append("skillIds", StringUtils.join(CollectUtils.getIds(getSkills()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
