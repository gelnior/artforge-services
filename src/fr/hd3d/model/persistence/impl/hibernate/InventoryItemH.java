package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.common.client.enums.EScreenType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IInventoryItem;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.utils.EnumUtils;


/**
 * Base class for computer inventory items : computers, devices, softwares and screens.
 * 
 * @author HD3D
 */
@Entity
// @Table(name = "hd3d_inventoryitem")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public abstract class InventoryItemH extends ResourceH implements IInventoryItem
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String PURCHASE_DATE_FIELD = "purchaseDate";
    private static final String WARRANTY_END_FIELD = "warrantyEnd";
    private static final String STATUS_FIELD = "status";

    protected String name;
    protected String serial;
    protected String billingReference;
    protected Date purchaseDate;
    protected Date warrantyEnd;
    protected EInventoryStatus inventoryStatus;

    /*-------------
     * Constructors
     -------------*/
    public InventoryItemH()
    {}

    public InventoryItemH(Long id, Timestamp version, String name, String serial, String billingReference,
            Date purchaseDate, Date warrantyEnd, EInventoryStatus inventoryStatus, Set<IResourceGroup> resourceGroups)
    {
        super(id, version, resourceGroups);

        this.name = name;
        this.serial = serial;
        this.billingReference = billingReference;
        this.purchaseDate = purchaseDate;
        this.warrantyEnd = warrantyEnd;
        this.inventoryStatus = inventoryStatus;
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "name")
    @NotEmpty
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Column(name = "serial")
    public String getSerial()
    {
        return serial;
    }

    public void setSerial(String serial)
    {
        this.serial = serial;
    }

    @Column(name = "reference")
    public String getBillingReference()
    {
        return billingReference;
    }

    public void setBillingReference(String billingReference)
    {
        this.billingReference = billingReference;
    }

    @Column(name = "purchase_date")
    public Date getPurchaseDate()
    {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate)
    {
        this.purchaseDate = purchaseDate;
    }

    @Column(name = "warranty_end")
    public Date getWarrantyEnd()
    {
        return warrantyEnd;
    }

    public void setWarrantyEnd(Date warrantyEnd)
    {
        this.warrantyEnd = warrantyEnd;
    }

    @Column(name = "inventory_status")
    public EInventoryStatus getInventoryStatus()
    {
        return this.inventoryStatus;
    }

    public void setInventoryStatus(EInventoryStatus status)
    {
        this.inventoryStatus = status;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((billingReference == null) ? 0 : billingReference.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((purchaseDate == null) ? 0 : purchaseDate.hashCode());
        result = prime * result + ((serial == null) ? 0 : serial.hashCode());
        result = prime * result + ((warrantyEnd == null) ? 0 : warrantyEnd.hashCode());
        result = prime * result + ((inventoryStatus == null) ? 0 : inventoryStatus.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        InventoryItemH other = (InventoryItemH) obj;
        if (billingReference == null)
        {
            if (other.billingReference != null)
                return false;
        }
        else if (!billingReference.equals(other.billingReference))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (purchaseDate == null)
        {
            if (other.purchaseDate != null)
                return false;
        }
        else if (!purchaseDate.equals(other.purchaseDate))
            return false;
        if (serial == null)
        {
            if (other.serial != null)
                return false;
        }
        else if (!serial.equals(other.serial))
            return false;
        if (warrantyEnd == null)
        {
            if (other.warrantyEnd != null)
                return false;
        }
        else if (!warrantyEnd.equals(other.warrantyEnd))
            return false;
        if (inventoryStatus == null)
        {
            if (other.inventoryStatus != null)
                return false;
        }
        else if (!inventoryStatus.equals(other.inventoryStatus))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
    }

    @Transient
    public String getResourceName()
    {
        return this.name;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public String toString()
    {
        return name;
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class InventoryItemComplexTypeProvider implements IComplexTypeProvider
    {
        public List<String> getValidEnum(String field)
        {
            if (STATUS_FIELD.equals(field))
                return EnumUtils.getEnumStringValues(EInventoryStatus.class);

            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            if (STATUS_FIELD.equals(field))
                return EnumUtils.getEnumValueIgnoreCase(EScreenType.class, value);

            return isDateField(field) ? fr.hd3d.utils.StringUtils.parseDate(value) : null;
        }

        public boolean isDateField(String field)
        {
            return PURCHASE_DATE_FIELD.equals(field) || WARRANTY_END_FIELD.equals(field);
        }

        public boolean isValidTypeField(String field)
        {
            return PURCHASE_DATE_FIELD.equals(field) || WARRANTY_END_FIELD.equals(field) || STATUS_FIELD.equals(field);
        }
    }
}
