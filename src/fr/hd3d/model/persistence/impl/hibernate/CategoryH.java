package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Indexed
@Table(name = "hd3d_category")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class CategoryH extends BaseH implements ICategory
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String name;
    private ICategory parent;
    private Collection<ICategory> children;
    private IProject project;
    private Set<IConstituent> constituents;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_category"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public CategoryH()
    {}

    public CategoryH(Long id, Timestamp version, String name, ICategory parent, IProject project)
    {
        super(id, version);
        if (project == null)
            throw new Hd3dRuntimeException("A Category must have a project");
        this.name = name;
        this.project = project;
        setParent(parent);
    }

    public CategoryH(String name, ICategory parent, IProject project)
    {
        this(null, null, name, parent, project);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "category_name", nullable = false)
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    @org.hibernate.annotations.Index(name = "category_name_idx")
    @NotEmpty
    public String getName()
    {
        return name;
    }

    @ManyToOne(targetEntity = CategoryH.class, fetch = FetchType.LAZY)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public ICategory getParent()
    {
        return parent;
    }

    @OneToMany(targetEntity = CategoryH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parent")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Collection<ICategory> getChildren()
    {
        if (children == null)
            children = new ArrayList<ICategory>();
        return children;
    }

    public void setChildren(Collection<ICategory> children)
    {
        this.children = children;
    }

    @ManyToOne(targetEntity = ProjectH.class, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "category_project_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    @OneToMany(targetEntity = ConstituentH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "category")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IConstituent> getConstituents()
    {
        if (constituents == null)
            constituents = new HashSet<IConstituent>();
        return constituents;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setParent(ICategory parent)
    {
        if (Const.CATEGORY_ROOT.equals(getName()))
        {
            this.parent = null;
        }
        else
        {
            if (parent != null && isValidId(parent.getId()) && parent != this)
            {
                this.parent = parent;
            }
            else
            {
                if (getProject() == null)
                {
                    throw new Hd3dRuntimeException("A Category must have a project");
                }
                this.parent = project.rootCategory();
            }
        }
    }

    public void setProject(IProject project)
    {
        if (project != null)
        {
            this.project = project;
        }
    }

    public void setConstituents(Set<IConstituent> constituents)
    {
        this.constituents = constituents;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        CategoryH other = (CategoryH) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/

    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Transient
    public static boolean isRootCategory(ICategory cat)
    {
        return cat != null && Const.CATEGORY_ROOT.equals(cat.getName());
    }

    @Transient
    public boolean isRoot()
    {
        return isRootCategory(this);
    }

    /**
     * return all the parents of this category recursively
     * 
     * @return
     */
    public Set<ICategory> parents()
    {
        Set<ICategory> parents = new LinkedHashSet<ICategory>();// keep the order
        for (ICategory cat = getParent(); cat != null; cat = cat.getParent())
            parents.add(cat);
        return parents;
    }

    @Transient
    public String getPath()
    {
        return path(" > ", false);
    }

    public String path(String separator, boolean includeProject)
    {
        StringBuilder buf = new StringBuilder(100);

        Set<ICategory> parents = parents();
        ICategory[] parentsArray = (ICategory[]) parents.toArray(new ICategory[parents.size()]);
        CollectionUtils.reverseArray(parentsArray);

        if (includeProject && !ArrayUtils.isEmpty(parentsArray))
        {
            IProject project = parentsArray[0].getProject();
            if (project != null)
            {
                buf.append(project.getName());
            }
        }
        /* build the string */
        for (int i = 0; i < parentsArray.length; i++)
        {
            final ICategory category = parentsArray[i];
            if (!category.isRoot())
            {
                buf.append(category.getName()).append(separator);
            }
        }
        buf.trimToSize();
        return buf.toString();
    }

    public Collection<ICategory> allChildren(boolean includeSelf)
    {
        List<ICategory> allChildren = new ArrayList<ICategory>();
        if (includeSelf)
            allChildren.add(this);
        for (ICategory category : getChildren())
            allChildren.addAll(category.allChildren(true));

        return allChildren;
    }

    // IMPORTANT NOTE: in order for Hibernate Search to index the result of this method
    // which is a TRANSIENT method and not an attribute, the method name must start by getXXX.
    // The other way (actually the proper one) is to implement a @ClassBridge
    @Transient
    @Field(name = "parents", index = Index.TOKENIZED, store = Store.YES)
    public String getParentIds()
    {
        return parents() == null ? null : StringUtils.join(CollectUtils.getIds(parents()), ' ');
    }

    @Transient
    @Field(name = "project", index = Index.UN_TOKENIZED, store = Store.YES)
    public Long getProjectId()
    {
        return getProject().getId();
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILCategory, ICategory> defaultTranslator()
    {
        return Translators.category;
    }

    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        // cascade disable
        Collection<IConstituent> constituents = CollectUtils.selectNotNull(getConstituents());
        Persistors.constituent.disableCollection(constituents);

        Collection<ICategory> categories = CollectUtils.selectNotNull(allChildren(false));
        Persistors.category.disableCollection(categories);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }
    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("name", name);
    // b.append("parentId", parent == null ? "null" : parent.getId());
    // b.append("childrenIds", StringUtils.join(CollectUtils.getIds(getChildren()), ','));
    // b.append("projectId", project == null ? "null" : project.getId());
    // b.append("constituentIds", StringUtils.join(CollectUtils.getIds(getConstituents()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
