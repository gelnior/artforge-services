package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.persistence.IOrganization;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IResource;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


/**
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_resourcegroup")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ResourceGroupH extends SimpleH implements IResourceGroup
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private IResourceGroup parent;
    private IPerson leader;
    private Collection<IResourceGroup> children;
    private Set<IResource> resources;
    private Set<IOrganization> organizations;
    private Set<IProject> projects;
    private Set<IRole> roles;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_resourcegroup"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ResourceGroupH()
    {}

    public ResourceGroupH(Long id, Timestamp version, String name, IResourceGroup parent, IPerson leader,
            Set<IResource> resources, Set<IProject> projects, Set<IOrganization> organizations, Set<IRole> roles)
    {
        super(id, version, name);
        this.name = name;
        this.parent = parent;
        this.resources = resources;
        this.leader = leader;
        this.projects = projects;
        this.organizations = organizations;
        this.roles = roles;
    }

    public ResourceGroupH(String name, IResourceGroup parent, IPerson leader, Set<IResource> resources,
            Set<IProject> projects, Set<IOrganization> organizations, Set<IRole> roles)
    {
        this(null, null, name, parent, leader, resources, projects, organizations, roles);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToOne(targetEntity = ResourceGroupH.class, fetch = FetchType.LAZY)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IResourceGroup getParent()
    {
        return parent;
    }

    public void setParent(IResourceGroup parent)
    {
        if (parent != this)
        {
            this.parent = parent;
            if (parent != null)
            {
                if (CollectUtils.isNotEmpty(parent.getProjects()))
                {
                    getProjects().addAll(new HashSet<IProject>(parent.getProjects()));
                }
                if (CollectUtils.isNotEmpty(parent.getRoles()))
                {
                    getRoles().addAll(new HashSet<IRole>(parent.getRoles()));
                }
                if (CollectUtils.isNotEmpty(parent.getOrganizations()))
                {
                    getOrganizations().addAll(new HashSet<IOrganization>(parent.getOrganizations()));
                }
            }
        }
    }

    @ManyToOne(targetEntity = PersonH.class)
    @JoinColumn(name = "leader_id")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getLeader()
    {
        return this.leader;
    }

    public void setLeader(IPerson leader)
    {
        this.leader = leader;
    }

    @OneToMany(targetEntity = ResourceGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parent")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Collection<IResourceGroup> getChildren()
    {
        if (children == null)
        {
            children = new ArrayList<IResourceGroup>();
        }
        return children;
    }

    public void setChildren(Collection<IResourceGroup> children)
    {
        this.children = children;
    }

    @ManyToMany(targetEntity = ResourceH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_resourcegroup__resource", joinColumns = @JoinColumn(name = "resourcegroup_id"), inverseJoinColumns = @JoinColumn(name = "resource_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IResource> getResources()
    {
        if (this.resources == null)
        {
            this.resources = new HashSet<IResource>();
        }
        return this.resources;
    }

    public void setResources(Set<IResource> resources)
    {
        this.resources = resources;
    }

    @ManyToMany(targetEntity = OrganizationH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_resourcegroup__organization", joinColumns = @JoinColumn(name = "resourcegroup_id"), inverseJoinColumns = @JoinColumn(name = "organization_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IOrganization> getOrganizations()
    {
        if (this.organizations == null)
        {
            this.organizations = new HashSet<IOrganization>();
        }
        return this.organizations;
    }

    public void setOrganizations(Set<IOrganization> organizations)
    {
        this.organizations = organizations;
    }

    @ManyToMany(targetEntity = ProjectH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_resourcegroup__project", joinColumns = @JoinColumn(name = "resourcegroup_id"), inverseJoinColumns = @JoinColumn(name = "project_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IProject> getProjects()
    {
        if (projects == null)
            projects = new HashSet<IProject>();
        return projects;
    }

    public void setProjects(Set<IProject> projects)
    {
        this.projects = projects;
    }

    @ManyToMany(targetEntity = RoleH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_resourcegroup__role", joinColumns = @JoinColumn(name = "resourcegroup_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IRole> getRoles()
    {
        if (roles == null)
        {
            roles = new HashSet<IRole>();
        }

        return roles;
    }

    public void setRoles(Set<IRole> roles)
    {
        this.roles = roles;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        int result = super.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        for (IResourceGroup g : getChildren())
        {
            g.disable();
        }

        for (IOrganization o : getOrganizations())
        {
            o.getResourceGroups().remove(this);
        }
        for (IProject p : getProjects())
        {
            p.getResourceGroups().remove(this);
        }
        for (IResource r : getResources())
        {
            r.getResourceGroups().remove(this);
        }
        for (IRole r : getRoles())
        {
            r.getResourceGroups().remove(this);
        }
    }

    @Transient
    public Set<IPerson> getStaff()
    {
        Set<IPerson> staff = new HashSet<IPerson>();
        for (IResource r : getResources())
        {
            if (r instanceof IPerson)
            {
                staff.add((IPerson) r);
            }
        }
        return staff;
    }

    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Transient
    public static boolean isRootResourceGroup(IResourceGroup group)
    {
        if (group == null)
        {
            return false;
        }
        return Const.RESOURCE_GROUP_ROOT.equals(group.getName());
    }

    @Transient
    public boolean isRoot()
    {
        return isRootResourceGroup(this);
    }

    /**
     * return all the parents of this category recursively
     * 
     * @return
     */
    public Set<IResourceGroup> parents()
    {
        Set<IResourceGroup> parents = new HashSet<IResourceGroup>();
        for (IResourceGroup group = getParent(); group != null; group = group.getParent())
        {
            parents.add(group);
        }
        return parents;
    }

    public Set<IRole> parentsRoles(boolean includeSelf)
    {
        Set<IRole> roles = new HashSet<IRole>();
        if (includeSelf)
        {
            roles.addAll(getRoles());
        }
        for (IResourceGroup group : parents())
        {
            roles.addAll(group.getRoles());
        }
        return roles;
    }

    public Collection<IResourceGroup> allChildren(boolean includeSelf)
    {
        List<IResourceGroup> allChildren = new ArrayList<IResourceGroup>();
        if (includeSelf)
        {
            allChildren.add(this);
        }
        for (IResourceGroup group : getChildren())
        {
            allChildren.addAll(group.allChildren(includeSelf));
        }

        return allChildren;
    }

    @Transient
    public String getParentIds()
    {
        if (parents() == null)
        {
            return null;
        }

        return StringUtils.join(CollectUtils.getIds(parents()), ' ');
    }

    @Transient
    public IResource getResourceById(Long id)
    {
        IResource ret = null;

        Set<IResource> resources = getResources();
        for (IResource r : resources)
            if (r.getId().equals(id))
            {
                ret = r;
                break;
            }
        return ret;
    }

    public void add(IRole role)
    {
        getRoles().add(role);
    }

    public void addPerson(IPerson person)
    {
        getResources().add(person);
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILResourceGroup, IResourceGroup> defaultTranslator()
    {
        return Translators.resourcegroup;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("parentId", parent == null ? "null" : parent.getId());
    // b.append("leaderId", leader == null ? "null" : leader.getId());
    // b.append("childrenIds", StringUtils.join(CollectUtils.getIds(getChildren()), ','));
    // b.append("resourceIds", StringUtils.join(CollectUtils.getIds(getResources()), ','));
    // b.append("organizationIds", StringUtils.join(CollectUtils.getIds(getOrganizations()), ','));
    // b.append("projectIds", StringUtils.join(CollectUtils.getIds(getProjects()), ','));
    // b.append("roleIds", StringUtils.join(CollectUtils.getIds(getRoles()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
