package fr.hd3d.model.persistence.impl.hibernate;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.beanutils.WrapDynaBean;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import fr.hd3d.common.client.DateFormat;
import fr.hd3d.common.client.Sheet;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.Queries;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.resources.collectionquery.AbstractCollectionQuery;
import fr.hd3d.services.resources.collectionquery.CollectionQueriesMap;
import fr.hd3d.services.resources.itemstransformer.AbstractItemsTransformerQuery;
import fr.hd3d.services.resources.itemstransformer.ItemsTransformersMap;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.transformer.GetDynValueParentTransformer;


public class ItemTypeHandler
{
    Object entityInstance;
    List<Object> entityInstances;
    IItem item;
    Object[][][] itemResult;

    public ItemTypeHandler(Object entityInstance, IItem item, Object[][][] itemResult)
    {
        this.entityInstance = entityInstance;
        this.item = item;
        this.itemResult = itemResult;
    }

    public ItemTypeHandler(List<Object> entityInstances, IItem item, Object[][][] itemResult)
    {
        this.entityInstances = entityInstances;
        this.item = item;
        this.itemResult = itemResult;
    }

    /**
     * given an entity and an IItem, return the value of the IItem
     * 
     * @return
     */
    public Object getValue()
    {
        final Object value;

        final String type = item.getType();
        final String query = item.getQuery();

        /* if type="attribute", query contains the attribute name */
        if (Sheet.ITEMTYPE_ATTRIBUTE.equalsIgnoreCase(type))
        {
            value = getAttributeValue(entityInstance, query);
        }
        /* if type="method", query contains the name of the method to invoke */
        else if (Sheet.ITEMTYPE_METHOD.equalsIgnoreCase(type))
        {
            value = getMethodValue(entityInstance, query);
        }
        else if (Sheet.ITEMTYPE_METADATA.equalsIgnoreCase(type))
        {
            value = getDynMetaDataValue(entityInstance, query);
        }
        else
        {
            value = Const.UNKNOWN;
        }

        return value;
    }

    /* Note: returned values respect the entities order */
    public List<?> getValues() throws Hd3dException
    {
        List<?> values;

        final String type = item.getType();
        final String query = item.getQuery();

        /* if type="attribute", query contains the attribute name */
        if (Sheet.ITEMTYPE_ATTRIBUTE.equalsIgnoreCase(type))
        {
            values = getAttributeValues(entityInstances, query);
        }
        /* if type="method", query contains the name of the method to invoke */
        else if (Sheet.ITEMTYPE_METHOD.equalsIgnoreCase(type))
        {
            values = getMethodValues(entityInstances, query);
        }
        else if (Sheet.ITEMTYPE_METADATA.equalsIgnoreCase(type))
        {
            values = getDynMetaDataValues(entityInstances, query);
        }
        else if (Sheet.ITEMTYPE_COLLECTIONQUERY.equalsIgnoreCase(type))
        {
            values = getCollectionQueryValues(entityInstances, query);
        }
        else if (Sheet.ITEMTYPE_ITEMSTRANSFORMER.equalsIgnoreCase(type))
        {
            values = getItemsTransformerValues(itemResult, query);
        }
        else
        {
            values = new ArrayList<Object>();
        }

        return values;
    }

    /**
     * given an entity and an IItem, sets the value of the IItem
     * 
     * @return
     */
    public void setValue(Object value) throws Exception
    {
        final String type = item.getType();

        if (Sheet.ITEMTYPE_ATTRIBUTE.equals(type))
        {
            setAttributeValue(entityInstance, value);
        }
        else if (Sheet.ITEMTYPE_METADATA.equals(type))
        {
            setDynMetaDataValue(entityInstance, value);
        }
    }

    /**
     * this method is used to complete entities with extra info from client side
     * 
     * @param extra
     */
    public void complete(Object extra)
    {
        if (entityInstance instanceof ITask)
        {
            ((ITask) entityInstance).setCommentForApprovalNote((String) extra);
        }
    }

    private Object getAttributeValue(Object entityInstance, String query)
    {
        return new WrapDynaBean(entityInstance).get(query);
    }

    private List<Object> getAttributeValues(List<Object> entityInstances, String query)
    {
        List<Object> values = new ArrayList<Object>(entityInstances.size());
        for (Object object : entityInstances)
        {
            values.add(getAttributeValue(object, query));
        }
        return values;
    }

    @SuppressWarnings("unchecked")
    private void setAttributeValue(Object entityInstance, Object value) throws Exception
    {
        /* wrap it in a Dynabean */
        final DynaBean dynabean = new WrapDynaBean(entityInstance);

        /*
         * discover the attribute's setter parameter type, if type is "attribute", query is supposed to contain the
         * attribute name
         */
        final String attributeName = item.getQuery();
        final DynaProperty dynaProp = dynabean.getDynaClass().getDynaProperty(attributeName);
        if (dynaProp == null)
            return;

        /* convert the itemValue to the attribute type */
        final Class destClass = dynaProp.getType();
        // test if the waiting value is an instance of IBaseObject

        /* value to assign is null, whatever the type, set to null */
        if (value == null)
        {
            dynabean.set(attributeName, null);
        }
        /* value is Enum */
        else if (Enum.class.isAssignableFrom(destClass))
        {
            Object enumValue = Enum.valueOf(destClass, (String) value);
            dynabean.set(attributeName, enumValue);
        }
        /* value is an Entity */
        else if (IBase.class.isAssignableFrom(destClass))
        {
            Long valueID;
            if (value instanceof String)
                valueID = NumberUtils.toLong((String) value);
            else
                valueID = (Long) value;

            if (BaseH.isValidId(valueID))
            {
                IPersist valueObjectPersistor = Persistors.getPersistor(destClass.getCanonicalName());
                Object valuePersistedObject = valueObjectPersistor.getById(valueID);
                if (valuePersistedObject == null)
                    return;
                dynabean.set(attributeName, valuePersistedObject);
            }
            else
            {
                dynabean.set(attributeName, null);
            }
        }
        /* value is Date */
        else if (Date.class.isAssignableFrom(destClass))
        {
            // it's a date we need to parse it from string
            SimpleDateFormat dateFormater = new SimpleDateFormat(DateFormat.DATE_TIME_STRING);
            String sValue = (String) value;
            Date date = null;
            if (StringUtils.isNotBlank(sValue))
            {
                date = dateFormater.parse((String) value);
            }
            dynabean.set(attributeName, date);
        }
        /* Default */
        else
        {
            // simple value
            Object converted = ConvertUtils.convert(value, destClass);
            dynabean.set(attributeName, converted);
        }
    }

    private Object getMethodValue(Object entityInstance, String query)
    {
        Object value = Const.UNKNOWN;

        String name = entityInstance.getClass().getCanonicalName();

        if (EntitiesMaps.isNotExposed(name, query))
        {
            throw new Hd3dRuntimeException(query + " is not a valid or exposed method");
        }

        try
        {
            Method method = null;
            try
            {
                /* invoke the method WITHOUT parameter */
                method = entityInstance.getClass().getMethod(query);
                if (method != null)
                {
                    value = method.invoke(entityInstance, (Object[]) null);
                }
            }
            catch (NoSuchMethodException e)
            {
                /* invoke the method WITH parameter, stored in ItemTransformer parameter field */
                method = entityInstance.getClass().getMethod(query, String.class);
                String param = item.getTransformerParameters();
                if (param == null)
                {
                    param = "";
                }
                if (method != null)
                {
                    value = method.invoke(entityInstance, param);
                }
            }
        }
        catch (IllegalArgumentException e)
        {
            Log.LOGGER.error("unable to invoke method {}", query);
            value = e;
        }
        catch (SecurityException e)
        {
            Log.LOGGER.error("unable to invoke method {}", query);
            value = e;
        }
        catch (IllegalAccessException e)
        {
            Log.LOGGER.error("unable to invoke method {}", query);
            value = e;
        }
        catch (InvocationTargetException e)
        {
            Log.LOGGER.error("unable to invoke method {}", query);
            value = e;
        }
        catch (NoSuchMethodException e)
        {
            Log.LOGGER.error("unable to invoke method {}", query);
            value = e;
        }
        return value;
    }

    private List<Object> getMethodValues(List<Object> entityInstances, String query)
    {
        List<Object> values = new ArrayList<Object>(entityInstances.size());
        for (Object object : entityInstances)
            values.add(getMethodValue(object, query));
        return values;
    }

    private Object getDynMetaDataValue(Object entityInstance, String query)
    {
        Object value = Const.UNKNOWN;
        if (IBase.class.isAssignableFrom(entityInstance.getClass()))
        {
            /* query is supposed to contain ClassDynMetaDataType id */
            Long metaDataId = Long.parseLong(query);
            IBase entity = (IBase) entityInstance;

            IDynMetaDataValue dynValue = entity.getDynMetaDataValue(metaDataId);

            value = new DynMetaDataTypeConverter(dynValue).getValue();
        }
        return value;
    }

    private List<Object> getDynMetaDataValues(final List<Object> entityInstances, final String query)
            throws Hd3dException
    {
        List<Object> ret;

        if (CollectUtils.isEmpty(entityInstances))
        {
            ret = new ArrayList<Object>(0);
        }
        else if (!IBase.class.isAssignableFrom(entityInstances.get(0).getClass()))
        {
            ret = listOfNull(entityInstances.size());
        }
        else
        {
            ret = listOfNull(entityInstances.size());

            try
            {
                List<? extends IBase> entities = CollectUtils.toEntities(entityInstances);
                List<Long> ids = CollectUtils.getIds(entities);

                /* query is supposed to contain ClassDynMetaDataType id */
                IClassDynMetaDataType classDynMetaDataType = Persistors.classdynmetadatatype.getById(NumberUtils
                        .toLong(item.getQuery()));

                if (classDynMetaDataType != null)
                {
                    List<IDynMetaDataValue> dynValues = Queries.getDynMetaDataValues(ids, classDynMetaDataType.getId());

                    if (CollectUtils.isNotEmpty(dynValues))
                    {
                        /* reorder DynMetaDataValues according to the ids of entities */
                        List<IDynMetaDataValue> orderedDynValues = CollectUtils.reorder(ids, dynValues,
                                new GetDynValueParentTransformer());

                        /* return the values of DynMetaDataValues in the proper order */
                        ret = new ArrayList<Object>(new DynMetaDataTypeConverter(orderedDynValues).getSameTypeValues());

                        orderedDynValues.clear();
                        orderedDynValues = null;
                        dynValues.clear();
                        dynValues = null;
                    }
                }

                ids.clear();
            }
            catch (Hd3dPersistenceException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
        }

        return ret;
    }

    private void setDynMetaDataValue(Object entityInstance, Object value) throws Hd3dException
    {
        Long metaDataId = Long.parseLong(item.getQuery());
        if (IBase.class.isAssignableFrom(entityInstance.getClass()))
        {
            ((IBase) entityInstance).setDynMetaDataValue(metaDataId, (String) value);
        }
    }

    private List<?> getCollectionQueryValues(List<Object> entityInstances, String query) throws Hd3dException
    {
        List<?> ret;

        if (CollectUtils.isEmpty(entityInstances))
        {
            ret = new ArrayList<Object>();
        }
        else
        {
            Class<? extends AbstractCollectionQuery> cqClass = CollectionQueriesMap.map.get(query);

            /* item query field must contain the CollectionQuery full name name */
            AbstractCollectionQuery collectionQuery = null;
            try
            {
                collectionQuery = cqClass.newInstance();
            }
            catch (InstantiationException e1)
            {
                throw new Hd3dException("Cannot instanciate collection query of class:" + cqClass);
            }
            catch (IllegalAccessException e1)
            {
                throw new Hd3dException("Cannot instanciate collection query of class:" + cqClass);
            }

            if (collectionQuery != null)
            {
                collectionQuery.setEntities(entityInstances);
                collectionQuery.setItem(item);
            }
            else
            {
                Class<?> clazz = null;
                try
                {
                    clazz = Class.forName(query);
                }
                catch (ClassNotFoundException e)
                {
                    Log.LOGGER.error(ExceptUtils.format(e));
                    ret = listOfNull(entityInstances.size());
                }

                if (clazz != null)
                {
                    try
                    {
                        collectionQuery = (AbstractCollectionQuery) clazz.newInstance();
                        collectionQuery.setEntities(entityInstances);
                        collectionQuery.setItem(item);
                    }
                    catch (InstantiationException e)
                    {
                        Log.LOGGER.error(ExceptUtils.format(e));
                        Log.LOGGER.error("Cannot instantiate CollectionQuery:" + query);
                        ret = listOfNull(entityInstances.size());
                    }
                    catch (IllegalAccessException e)
                    {
                        Log.LOGGER.error(ExceptUtils.format(e));
                        ret = listOfNull(entityInstances.size());
                    }
                }
                else
                {
                    ret = listOfNull(entityInstances.size());
                }
            }

            ret = collectionQuery.doQuery();
        }

        return ret;
    }

    private List<Object> listOfNull(int size)
    {
        return CollectUtils.asList(null, size);
    }

    private List<?> getItemsTransformerValues(Object[][][] itemResult, String query) throws Hd3dException
    {
        if (ArrayUtils.isEmpty(itemResult))
            return new ArrayList<Object>();

        /* item query field must contain the ItemTransformer full name name */
        Class<? extends AbstractItemsTransformerQuery> transformerClass = ItemsTransformersMap.map.get(query);
        AbstractItemsTransformerQuery itemsTransformer = null;
        if (transformerClass != null)
        {
            try
            {
                itemsTransformer = transformerClass.newInstance();
            }
            catch (InstantiationException e)
            {
                throw new Hd3dException(e);
            }
            catch (IllegalAccessException e)
            {
                throw new Hd3dException(e);
            }

            if (itemsTransformer != null)
            {
                itemsTransformer.setEntities(entityInstances);
                itemsTransformer.setItem(item);
                itemsTransformer.setItemResult(itemResult);
            }
        }
        else
        {
            Class<?> clazz = null;
            try
            {
                clazz = Class.forName(query);
            }
            catch (ClassNotFoundException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
                return listOfNull(entityInstances.size());

            }
            if (clazz != null)
            {
                try
                {
                    itemsTransformer = (AbstractItemsTransformerQuery) clazz.newInstance();
                    itemsTransformer.setEntities(entityInstances);
                    itemsTransformer.setItem(item);
                    itemsTransformer.setItemResult(itemResult);
                }
                catch (InstantiationException e)
                {
                    Log.LOGGER.error(ExceptUtils.format(e));
                    return listOfNull(entityInstances.size());
                }
                catch (IllegalAccessException e)
                {
                    Log.LOGGER.error(ExceptUtils.format(e));
                    return listOfNull(entityInstances.size());
                }
            }
            else
            {
                return listOfNull(entityInstances.size());
            }
        }

        return itemsTransformer.doQuery();
    }
}
