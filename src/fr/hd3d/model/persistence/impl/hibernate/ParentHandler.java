package fr.hd3d.model.persistence.impl.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.utils.CollectUtils;


/**
 * For given entity, returns its parent. Note: reason why this is not implemented as an interface is that some entities
 * implements getParent and some not, since the concept of Parent differs, and Hibernate enforces the java bean
 * convention (an attribute must match getters and setters properly). One can use @Transier method, but it will fool
 * reflection API.
 * 
 * @author try.lam
 * 
 */
public final class ParentHandler
{
    private ParentHandler()
    {}

    public static Persistent getParent(Persistent object)
    {
        Persistent ret = null;

        if (object != null)
        {
            if (IConstituent.class.isInstance(object))
            {
                ret = ((IConstituent) object).getCategory();
            }
            else if (IShot.class.isInstance(object))
            {
                ret = ((IShot) object).getSequence();
            }
            else if (ICategory.class.isInstance(object))
            {
                ret = ((ICategory) object).getParent();
            }
            else if (ISequence.class.isInstance(object))
            {
                ret = ((ISequence) object).getParent();
            }
        }

        return ret;
    }

    public static List<Persistent> getParents(Collection<Persistent> objects)
    {
        List<Persistent> ret = new ArrayList<Persistent>();

        if (CollectUtils.isNotEmpty(objects))
        {
            for (Persistent object : objects)
            {
                ret.add(getParent(object));
            }
        }

        return ret;
    }
}
