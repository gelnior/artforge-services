package fr.hd3d.model.persistence.impl.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Indexed
@Table(name = "hd3d_sequence")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class SequenceH extends BaseH implements ISequence
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String name;
    private String title;
    private ISequence parent;
    private Collection<ISequence> children;
    private IProject project;
    private Set<IShot> shots;
    private String hook;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_sequence"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public SequenceH()
    {}

    public SequenceH(Long id, java.sql.Timestamp version, String name, String title, ISequence parent,
            IProject project, String hook)
    {
        super(id, version);
        if (project == null)
            throw new Hd3dRuntimeException("A Sequence must have a project");
        this.name = name;
        this.title = title;
        this.project = project;
        setParent(parent);
        this.hook = hook;
    }

    public SequenceH(String name, String title, ISequence parent, IProject project, String hook)
    {
        this(null, null, name, title, parent, project, hook);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "sequence_name", nullable = false)
    @Field(index = Index.TOKENIZED, store = Store.YES)
    @org.hibernate.annotations.Index(name = "sequence_name_idx")
    @NotEmpty
    public String getName()
    {
        return name;
    }

    @Column(name = "sequence_title")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    public String getTitle()
    {
        return title;
    }

    @ManyToOne(targetEntity = SequenceH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @IndexedEmbedded(depth = 1, targetElement = SequenceH.class)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    @ContainedIn
    public ISequence getParent()
    {
        return parent;
    }

    @OneToMany(targetEntity = SequenceH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parent")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Collection<ISequence> getChildren()
    {
        if (children == null)
            children = new ArrayList<ISequence>();
        return children;
    }

    public void setChildren(Collection<ISequence> children)
    {
        this.children = children;
    }

    @ManyToOne(targetEntity = ProjectH.class, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "sequence_project_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    @OneToMany(targetEntity = ShotH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "sequence")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @ContainedIn
    public Set<IShot> getShots()
    {
        if (shots == null)
            shots = new HashSet<IShot>();
        return shots;
    }

    @Column(name = "sequence_hook")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    public String getHook()
    {
        return hook;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setParent(ISequence parent)
    {
        if (Const.SEQUENCE_ROOT.equals(getName()))
            this.parent = null;
        else
        {
            if (parent != null && isValidId(parent.getId()) && parent != this)
                this.parent = parent;
            else
            {
                if (getProject() == null)
                    throw new Hd3dRuntimeException("A Sequence must have a project");
                this.parent = project.rootSequence();
            }
        }
    }

    public void setProject(IProject project)
    {
        if (project != null)
            this.project = project;
    }

    public void setShots(Set<IShot> shots)
    {
        this.shots = shots;
    }

    public void setHook(String hook)
    {
        this.hook = hook;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((hook == null) ? 0 : hook.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SequenceH other = (SequenceH) obj;
        if (hook == null)
        {
            if (other.hook != null)
                return false;
        }
        else if (!hook.equals(other.hook))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    public void completePrePersistOrUpdate()
    {
        if (StringUtils.isBlank(getHook()))
        {
            setHook(HookHandler.getHook(this));
        }
    }

    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Transient
    public static boolean isRootSequence(ISequence seq)
    {
        if (seq == null)
            return false;
        return Const.SEQUENCE_ROOT.equals(seq.getName());
    }

    @Transient
    public boolean isRoot()
    {
        return isRootSequence(this);
    }

    /**
     * return all the parents of this sequence recursively
     * 
     * @return
     */
    public Set<ISequence> parents()
    {
        Set<ISequence> parents = new LinkedHashSet<ISequence>();
        for (ISequence seq = getParent(); seq != null; seq = seq.getParent())
            parents.add(seq);
        return parents;
    }

    @Transient
    public String getPath()
    {
        return path(" > ", false);
    }

    public String path(String separator, boolean includeProject)
    {
        StringBuilder buf = new StringBuilder(100);

        Set<ISequence> parents = parents();
        ISequence[] parentsArray = (ISequence[]) parents.toArray(new ISequence[parents.size()]);
        CollectionUtils.reverseArray(parentsArray);

        if (includeProject && !ArrayUtils.isEmpty(parentsArray))
        {
            IProject project = parentsArray[0].getProject();
            if (project != null)
            {
                buf.append(project.getName());
            }
        }
        /* build the string */
        for (int i = 0; i < parentsArray.length; i++)
        {
            final ISequence sequence = parentsArray[i];
            if (!sequence.isRoot())
            {
                buf.append(sequence.getName()).append(separator);
            }
        }
        buf.trimToSize();
        return buf.toString();
    }

    public Collection<ISequence> allChildren(boolean includeSelf)
    {
        List<ISequence> allChildren = new ArrayList<ISequence>();
        if (includeSelf)
            allChildren.add(this);
        for (ISequence sequence : getChildren())
            allChildren.addAll(sequence.allChildren(true));

        return allChildren;
    }

    @Transient
    @Field(name = "parents", index = Index.TOKENIZED, store = Store.YES)
    public String getParentIds()
    {
        if (parents() == null)
            return null;

        return StringUtils.join(CollectUtils.getIds(parents()), ' ');
    }

    @Field(name = "project", index = Index.UN_TOKENIZED, store = Store.YES)
    public Long projectId()
    {
        return getProject().getId();
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILSequence, ISequence> defaultTranslator()
    {
        return Translators.sequence;
    }

    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        // cascade disable
        Collection<IShot> shots = CollectUtils.selectNotNull(getShots());
        Persistors.shot.disableCollection(shots);

        Collection<ISequence> sequences = CollectUtils.selectNotNull(allChildren(false));
        Persistors.sequence.disableCollection(sequences);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public Double shotsApprovalCompletion(String approvalTypeId)
    {
        Double completion = 0D;
        Long nbShotApproved = NumberUtils.LONG_ZERO;
        // il faut récupérer la liste des plans
        Collection<IShot> shots = this.getShots();
        if (!shots.isEmpty())
        {
            // pour chaque plan récupérer le status de la derniere approval
            for (IShot shot : shots)
            {
                List<IApprovalNote> approvalNotes = shot.approvalNotes(approvalTypeId);
                if (!approvalNotes.isEmpty())
                {
                    if (approvalNotes.get(0).getStatus().equals("OK"))
                    {
                        nbShotApproved++;
                    }
                }
            }
            completion = (nbShotApproved * 100 / (double) shots.size()) / 100D;
        }
        return completion;
    }

    public Long sequenceLength()
    {
        String query = " select sum(nbFrame) from " + ShotH.class.getCanonicalName() + " shot where sequence.id="
                + this.id;
        Long length = (Long) HibernateUtil.currentSession().createQuery(query).uniqueResult();

        if (length == null)
            length = NumberUtils.LONG_ZERO;

        for (ISequence child : this.children)
        {
            length += child.sequenceLength();
        }
        return length;
    }

    public String toString()
    {
        return name;
    }
    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("name", name).append("title", title);
    // b.append("parentId", parent == null ? "null" : parent.getId());
    // b.append("childrenIds", StringUtils.join(CollectUtils.getIds(getChildren()), ','));
    // b.append("projectId", project == null ? "null" : project.getId());
    // b.append("shotIds", StringUtils.join(CollectUtils.getIds(getShots()), ','));
    // b.append("hook", hook);
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
