package fr.hd3d.model.persistence.impl.hibernate;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;

import fr.hd3d.model.persistence.IClassDynMetaDataType;


/**
 * 
 * @author try.lam
 * 
 */

@Entity
@Table(name = "hd3d_dynmetadatavaluestocreate")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class DynMetaDataValuesToCreate implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;
    private IClassDynMetaDataType classDynMetaDataType;
    private Long parent;
    private String parentType;

    public DynMetaDataValuesToCreate()
    {}

    public DynMetaDataValuesToCreate(IClassDynMetaDataType classDynMetaDataType, Long parent, String parentType)
    {
        this.classDynMetaDataType = classDynMetaDataType;
        this.parent = parent;
        this.parentType = parentType;
    }

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getId()
    {
        return id;
    }

    @ManyToOne(targetEntity = ClassDynMetaDataTypeH.class, fetch = FetchType.LAZY)
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IClassDynMetaDataType getClassDynMetaDataType()
    {
        return classDynMetaDataType;
    }

    public Long getParent()
    {
        return parent;
    }

    public String getParentType()
    {
        return parentType;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setClassDynMetaDataType(IClassDynMetaDataType classDynMetaDataType)
    {
        this.classDynMetaDataType = classDynMetaDataType;
    }

    public void setParent(Long parent)
    {
        this.parent = parent;
    }

    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((classDynMetaDataType == null) ? 0 : classDynMetaDataType.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((parentType == null) ? 0 : parentType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DynMetaDataValuesToCreate other = (DynMetaDataValuesToCreate) obj;
        if (classDynMetaDataType == null)
        {
            if (other.classDynMetaDataType != null)
                return false;
        }
        else if (!classDynMetaDataType.equals(other.classDynMetaDataType))
            return false;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        if (parentType == null)
        {
            if (other.parentType != null)
                return false;
        }
        else if (!parentType.equals(other.parentType))
            return false;
        return true;
    }

}
