package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.functors.NotNullPredicate;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.MapUtils;
import fr.hd3d.utils.Pair;


/**
 * Depending on the DynMetaDataValue's DynMetaDataType, handles get and set properly.
 * 
 * @author try.lam
 * 
 */
public class DynMetaDataTypeConverter
{
    private IDynMetaDataType dynType;
    private IDynMetaDataValue dynValue;
    private List<IDynMetaDataValue> dynValues;

    public DynMetaDataTypeConverter(IDynMetaDataValue dynValue)
    {
        if (dynValue == null)
        {
            throw new Hd3dRuntimeException("Error in DynMetaDataTypeConverter constructor: IDynMetaDataValue is null");
        }
        this.dynValue = dynValue;

        if (dynValue.getClassDynMetaDataType() == null)
        {
            throw new Hd3dRuntimeException(
                    "Error in DynMetaDataTypeConverter constructor: IDynMetaDataValue's ClassDynMetaDataType is null");
        }
        this.dynType = dynValue.getClassDynMetaDataType().getDynMetaDataType();
    }

    public DynMetaDataTypeConverter(List<IDynMetaDataValue> dynValues)
    {
        if (CollectUtils.isEmpty(dynValues))
        {
            throw new Hd3dRuntimeException(
                    "Error in DynMetaDataTypeConverter constructor: List<IDynMetaDataValue> is null or empty");
        }
        this.dynValues = dynValues;
        /* extract type */
        for (IDynMetaDataValue dyn : dynValues)
        {
            if (dyn == null || dyn.getClassDynMetaDataType() == null)
            {
                continue;
            }
            dynType = dyn.getClassDynMetaDataType().getDynMetaDataType();
            break;
        }
        if (dynType == null)
        {
            throw new Hd3dRuntimeException(
                    "Error in DynMetaDataTypeConverter constructor: List<IDynMetaDataValue>'s ClassDynMetaDataType is null");
        }
    }

    public DynMetaDataTypeConverter(IDynMetaDataType dynType)
    {
        if (dynType == null)
        {
            throw new Hd3dRuntimeException("Error in DynMetaDataTypeConverter constructor: IDynMetaDataType is null");
        }

        this.dynType = dynType;
    }

    public void setValue(String value)
    {
        /* Do not test "value" nullness, null may be a valid value */
        if (dynValue != null)
        {
            /*
             * if hd3d entity, contains its id. The entity type can be found out by
             * classDynMetaDataType.dynMetaDataType.type. In this case, use the entity Translator and return the
             * serialized string. Given the entity type and the id (hold by value here), it is possible to read the
             * entity
             */
            final String type = dynValue.getDynMetaDataType_Type();

            if (type != null)
            {
                /* if type is an hd3d entity... */
                if (dynValue.isEntity())
                {
                    /* ... longValue contains its id */
                    Long id = NumberUtils.toLong(value);
                    dynValue.setLongValue(id);

                    /* ... retrieve the entity and set it to the DynMetaDataValue */
                    IPersist<?> persistor = Persistors.getPersistor(type);
                    try
                    {
                        dynValue.setObjectValue((IBase) persistor.getById(id));
                    }
                    catch (Hd3dException e)
                    {
                        Log.LOGGER.error(ExceptUtils.format(e));
                    }
                }
                else
                {
                    /* Number: all numeric types are stored as long */
                    if (NumberUtils.isNumber(value))
                    {
                        dynValue.setLongValue(NumberUtils.toLong(value));
                    }
                    /* TODO make it a map */
                    else if (Const.JAVA_UTIL_DATE.equals(type) && StringUtils.isNotBlank(value))
                    {
                        dynValue.setDateValue(fr.hd3d.utils.StringUtils.parseDate(value));
                    }
                    else if (Const.JAVA_LANG_BOOLEAN.equals(type))
                    {
                        dynValue.setBooleanValue(BooleanUtils.toBoolean(value));
                    }
                }
            }
        }
    }

    /**
     * Sets a list of values to the list of DynMetaDataValues passed in Constructor. All the DynMetaDataValues MUST HAVE
     * THE SAME DYNMETADATATYPE. The purpose of this method is to do a single query instead of multiple single queries.
     * 
     * @param values
     */
    @SuppressWarnings("unchecked")
    public void setSameTypeValues(List<String> values) throws Hd3dException
    {
        if (CollectUtils.isEmpty(dynValues) || CollectUtils.isEmpty(values))
        {
            return;
        }

        if (dynValues.size() != values.size())
        {
            throw new Hd3dException(
                    "Cannot set values to DynMetaDataValues: values size is different from DynMetaDataValues size.");
        }

        /* extract the DynMetaDataType of the DynMetaDataValues */
        IDynMetaDataValue aDyn = CollectionUtils.<IDynMetaDataValue> find(dynValues, NotNullPredicate.INSTANCE);
        if (aDyn == null)
        {
            return;
        }

        final String type = aDyn.getDynMetaDataType_Type();

        if (type == null)
        {
            return;
        }

        /*
         * if hd3d entity, contains its id. The entity type can be found out by
         * classDynMetaDataType.dynMetaDataType.type. In this case, use the entity Translator and return the serialized
         * string. Given the entity type and the id (hold by value here), it is possible to read the entity
         */
        /* if type is an hd3d entity... */
        if (aDyn.isEntity())
        {
            /* ... retrieve the entities and set them to the DynMetaDataValues */
            final IPersist<?> persistor = Persistors.getPersistor(type);

            /* list of values must contain entity ids in String, convert them */
            final List<Long> ids = CollectUtils.toLong(values);

            /*
             * build a map with key=dynMetaDataValue id, value=entity id to recover order later. The Map must be an
             * ORDERED map to keep the order(LinkedHashMap here)
             */
            final Map<Long, Long> map = (Map<Long, Long>) MapUtils.toMap(CollectUtils.getIds(dynValues), ids,
                    LinkedHashMap.class);

            /* retrieve the entities */
            List<IBase> entities = (List<IBase>) persistor.getByIds(null, ids);

            /* Reorder result according to DynMetaDataValues order */
            List<IBase> orderedEntities = new ArrayList<IBase>(entities.size());
            for (Map.Entry<Long, Long> entry : map.entrySet())
            {
                for (IBase entity : entities)
                {
                    if (entity.getId().equals(entry.getValue()))
                    {
                        orderedEntities.add(entity);
                        break;
                    }
                }
            }

            /* set the values in the proper order */
            IDynMetaDataValue aDynValue;
            IBase aEntity;
            for (int i = 0; i < dynValues.size(); i++)
            {
                aDynValue = dynValues.get(i);
                aEntity = orderedEntities.get(i);
                aDynValue.setLongValue(aEntity.getId());
                aDynValue.setObjectValue(aEntity);
            }
        }
        else
        {
            IDynMetaDataValue aDynValue;
            String aValue;
            for (int i = 0; i < dynValues.size(); i++)
            {
                aDynValue = dynValues.get(i);
                aValue = values.get(i);
                /* Number: all numeric types are stored as long */
                if (NumberUtils.isNumber(aValue))
                {
                    aDynValue.setLongValue(NumberUtils.toLong(aValue));
                }

                /* TODO make it a map */
                else if (Const.JAVA_UTIL_DATE.equals(type))
                {
                    if (StringUtils.isNotBlank(aValue))
                    {
                        aDynValue.setDateValue(fr.hd3d.utils.StringUtils.parseDate(aValue));
                    }
                }
                else if (Const.JAVA_LANG_BOOLEAN.equals(type))
                {
                    aDynValue.setBooleanValue(BooleanUtils.toBoolean(aValue));
                }
            }
        }
    }

    public Object getValue()
    {
        return getValue(dynValue);
    }

    private Object getValue(final IDynMetaDataValue value)
    {
        Object ret = null;

        if (value != null)
        {
            final String dynNature = dynType.getNature();

            if (Const.DYNMETADATATYPE_SIMPLE.equals(dynNature) || dynNature == null)
            {
                ret = getSimpleValue(value);
            }
            else if (Const.JAVA_UTIL_LIST.equals(dynNature))
            {
                ret = value.getValue();
                if ("".equals(ret) && !"".equals(dynType.getDefaultValue()))
                {
                    ret = dynType.getDefaultValue();
                }
            }
        }

        return ret;
    }

    /**
     * Given a String with a separator defined in IListValues, returns a List of the proper type
     * 
     * @param values
     *            a String of values with separator
     * @return
     */
    private List<Object> split(final String values)
    {
        List<Object> result = new ArrayList<Object>();

        final IListValues listValues = dynType.getListValues();
        if (listValues == null)
            return result;

        /* List values as String */
        String[] stringValues = StringUtils.split(values, listValues.getSeparator());

        final String type = dynType.getType();

        /* convert them in the proper type */
        for (String aStringValue : stringValues)
        {
            if (EntitiesMaps.isEntity(type))
            {
                IPersist<? extends IBase> persistor = Persistors.getPersistor(type);
                final Long id = NumberUtils.toLong(aStringValue);
                try
                {
                    result.add(persistor.getById(id));
                }
                catch (Hd3dException e)
                {
                    Log.LOGGER.warn("Cannot retrieve entity of type " + type + " with id=" + id);
                    result.add(null);
                }
            }
            else if (Const.JAVA_LANG_BOOLEAN.equals(type))
            {
                result.add(BooleanUtils.toBoolean(aStringValue));
            }
            else if (Const.JAVA_UTIL_DATE.equals(type))
            {
                result.add(fr.hd3d.utils.StringUtils.parseDate(aStringValue));
            }
            else if (Const.JAVA_LANG_LONG.equals(type))
            {
                result.add(new Long(NumberUtils.toLong(aStringValue)));
            }
            else if (Const.JAVA_LANG_INTEGER.equals(type))
            {
                result.add(new Integer(NumberUtils.toInt(aStringValue)));
            }
            else if (Const.JAVA_LANG_DOUBLE.equals(type))
            {
                result.add(new Double(NumberUtils.toDouble(aStringValue)));
            }
            else if (Const.JAVA_LANG_FLOAT.equals(type))
            {
                result.add(new Float(NumberUtils.toFloat(aStringValue)));
            }
            else if (Const.JAVA_SQL_TIMESTAMP.equals(type))
            {
                result.add(new Timestamp(fr.hd3d.utils.StringUtils.parseDate(aStringValue).getTime()));
            }
            else if (Const.JAVA_LANG_BYTE.equals(type))
            {
                result.add(Byte.valueOf(aStringValue));
            }
            else
            {
                /* Default type is String */
                result.add(aStringValue);
            }
        }
        return result;
    }

    private Object getSimpleValue(final IDynMetaDataValue value)
    {
        Object ret = null;

        if (value != null)
        {
            final String dynType = value.getClassDynMetaDataType().getDynMetaDataType().getType();

            if (value.isEntity())
            {
                ret = value.getObjectValue();
            }
            else if (Const.JAVA_LANG_BOOLEAN.equals(dynType))
            {
                ret = value.getBooleanValue();
            }
            else if (Const.JAVA_UTIL_DATE.equals(dynType))
            {
                ret = value.getDateValue();
            }
            else if (Const.JAVA_LANG_LONG.equals(dynType))
            {
                ret = value.getLongValue();
            }
            else if (Const.JAVA_LANG_INTEGER.equals(dynType))
            {
                ret = new Integer(value.getLongValue().intValue());
            }
            else if (Const.JAVA_LANG_DOUBLE.equals(dynType))
            {
                ret = new Double(value.getLongValue().doubleValue());
            }
            else if (Const.JAVA_LANG_FLOAT.equals(dynType))
            {
                ret = new Float(value.getLongValue().floatValue());
            }
            else if (Const.JAVA_SQL_TIMESTAMP.equals(dynType))
            {
                ret = new Timestamp(value.getDateValue().getTime());
            }
            else if (Const.JAVA_LANG_BYTE.equals(dynType))
            {
                ret = Byte.valueOf(value.getValue());
            }
            else
            {
                /* Default type is String */
                ret = value.getValue();
            }
        }

        return ret;
    }

    private Pair<String, Boolean> extractInfo()
    {
        String type = null;
        Boolean isEntity = false;

        final Collection<IDynMetaDataValue> values = CollectUtils.selectNotNull(dynValues);
        if (CollectUtils.isNotEmpty(values))
        {
            final IDynMetaDataValue first = values.iterator().next();
            type = first.getDynMetaDataType_Type();
            isEntity = first.isEntity();
        }

        return new Pair<String, Boolean>(type, isEntity);

    }

    private List<Long> getEntityIds()
    {
        List<Long> entityIds = new ArrayList<Long>(dynValues.size());
        for (IDynMetaDataValue v : dynValues)
        {
            entityIds.add(v == null ? null : v.getLongValue());
        }
        return entityIds;
    }

    private List<Object> getSameTypeEntityValues(final String type) throws Hd3dException
    {
        List<Object> ret;

        /* collect the ids of the bound entities */
        List<Long> entityIds = getEntityIds();

        /*
         * build a map with key=dynMetaDataValue id, value=entity id to recover order later. The Map must be an ORDERED
         * map to keep the order(LinkedHashMap here)
         */
        Map<Long, Long> map = (Map<Long, Long>) MapUtils.toMap(CollectUtils.getIds(dynValues), entityIds,
                LinkedHashMap.class);

        List<? extends IBase> entities = new ArrayList<IBase>(Persistors.getPersistor(type).getByIds_NoPermCheck(null,
                entityIds));

        /* Reorder result according to DynMetaDataValues id order */
        if (CollectUtils.isEmpty(entities))
        {
            ret = CollectUtils.asList(null, dynValues.size());
        }
        else
        {
            ret = new ArrayList<Object>(dynValues.size());
            for (Map.Entry<Long, Long> entry : map.entrySet())
            {
                boolean found = false;
                for (IBase entity : entities)
                {
                    if (entity.getId().equals(entry.getValue()))
                    {
                        ret.add(entity);
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    ret.add(null);// for null entity, must add too
                }
            }
        }

        /* cleaning */
        entityIds.clear();
        entityIds = null;
        if (map != null)
        {
            map.clear();
        }
        map = null;
        return ret;
    }

    public List<Object> getSameTypeValues() throws Hd3dException
    {
        List<Object> ret;

        if (CollectUtils.isEmpty(dynValues))
        {
            ret = new ArrayList<Object>(0);
        }
        else
        {
            /* extract the DynMetaDataType of the DynMetaDataValues */
            final Pair<String, Boolean> tuple = extractInfo();
            final String type = tuple.getField_1();
            final boolean isEntity = tuple.getField_2();

            /* process */
            if (isEntity)
            {
                ret = getSameTypeEntityValues(type);
            }
            else
            {
                ret = new ArrayList<Object>(dynValues.size());
                for (IDynMetaDataValue v : dynValues)
                {
                    ret.add(getValue(v));
                }
            }
        }

        return ret;
    }

    public List<?> getDynMetaDataTypeList()
    {
        List<?> ret = null;

        if (dynType != null && Const.JAVA_UTIL_LIST.equals(dynType.getNature()))
        {
            ret = split(dynType.getListValues().getValues());
        }

        return ret;
    }
}
