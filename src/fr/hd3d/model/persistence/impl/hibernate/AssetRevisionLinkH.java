package fr.hd3d.model.persistence.impl.hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevisionLink;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * 
 * @author try LAM
 * 
 */
@Entity
@Table(name = "hd3d_assetrevisionlink")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class AssetRevisionLinkH extends BaseH implements IAssetRevisionLink
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private IAssetRevision inputAsset;
    private IAssetRevision outputAsset;
    private String type;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_assetrevisionlink"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public AssetRevisionLinkH()
    {}

    public AssetRevisionLinkH(Long id, java.sql.Timestamp version, IAssetRevision inputAsset,
            IAssetRevision outputAsset, String type)
    {
        super(id, version);
        this.inputAsset = inputAsset;
        this.outputAsset = outputAsset;
        this.type = type;
    }

    public AssetRevisionLinkH(IAssetRevision inputAsset, IAssetRevision outputAsset, String type)
    {
        this(null, null, inputAsset, outputAsset, type);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @OneToOne(targetEntity = AssetRevisionH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "assetrevisionlink_inputasset")
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IAssetRevision getInputAsset()
    {
        return inputAsset;
    }

    @OneToOne(targetEntity = AssetRevisionH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "assetrevisionlink_outputasset")
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IAssetRevision getOutputAsset()
    {
        return outputAsset;
    }

    public String getType()
    {
        return type;
    }

    public void setInputAsset(IAssetRevision inputAsset)
    {
        this.inputAsset = inputAsset;
    }

    public void setOutputAsset(IAssetRevision outputAsset)
    {
        this.outputAsset = outputAsset;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((inputAsset == null) ? 0 : inputAsset.hashCode());
        result = prime * result + ((outputAsset == null) ? 0 : outputAsset.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AssetRevisionLinkH other = (AssetRevisionLinkH) obj;
        if (inputAsset == null)
        {
            if (other.inputAsset != null)
                return false;
        }
        else if (!inputAsset.equals(other.inputAsset))
            return false;
        if (outputAsset == null)
        {
            if (other.outputAsset != null)
                return false;
        }
        else if (!outputAsset.equals(other.outputAsset))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILAssetRevisionLink, IAssetRevisionLink> defaultTranslator()
    {
        return Translators.assetrevisionlink;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("inputAssetRevisionId", inputAsset == null ? "null" : inputAsset.getId());
        b.append("outputAssetRevisionId", outputAsset == null ? "null" : outputAsset.getId());
        b.append("type", type);

        return b.toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
