/**
 * 
 */
package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILWorkingTime;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * @author thomas-eskenazi
 * 
 */
@Entity
@Table(name = "hd3d_workingtime")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class WorkingTimeH extends BaseH implements IWorkingTime
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String START_DATE_FIELD = "startdate";
    private static final String END_DATE_FIELD = "enddate";

    private IPersonDay personDay;
    private Date startDate;
    private Date endDate;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_workingtime"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public WorkingTimeH()
    {}

    public WorkingTimeH(Long id, java.sql.Timestamp version, IPersonDay personDay, Date startDate, Date endDate)
    {
        super(id, version);
        this.personDay = personDay;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public WorkingTimeH(IPersonDay personDay, Date startDate, Date endDate)
    {
        this(null, null, personDay, startDate, endDate);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToOne(targetEntity = PersonDayH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "personday_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IPersonDay getPersonDay()
    {
        return personDay;
    }

    @Column(name = "workingtime_end_date")
    @Index(name = "workingtime_end_date_idx")
    public Date getEndDate()
    {
        return endDate;
    }

    @Column(name = "workingtime_start_date")
    @Index(name = "workingtime_start_date_idx")
    public Date getStartDate()
    {
        return startDate;
    }

    public void setPersonDay(IPersonDay personDay)
    {
        this.personDay = personDay;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((personDay == null) ? 0 : personDay.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        WorkingTimeH other = (WorkingTimeH) obj;
        if (endDate == null)
        {
            if (other.endDate != null)
                return false;
        }
        else if (!endDate.equals(other.endDate))
            return false;
        if (personDay == null)
        {
            if (other.personDay != null)
                return false;
        }
        else if (!personDay.equals(other.personDay))
            return false;
        if (startDate == null)
        {
            if (other.startDate != null)
                return false;
        }
        else if (!startDate.equals(other.startDate))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILWorkingTime, IWorkingTime> defaultTranslator()
    {
        return Translators.workingTime;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("personDayId", personDay == null ? "null" : personDay.getId());
        b.append("startDate", startDate).append("endDate", endDate);

        return b.toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class WorkingTimeComplexTypeProvider implements IComplexTypeProvider
    {
        public List<String> getValidEnum(String field)
        {
            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            if (!isValidTypeField(field))
                return null;

            return fr.hd3d.utils.StringUtils.parseDate(value);
        }

        public boolean isValidTypeField(String field)
        {
            return START_DATE_FIELD.equals(field) || END_DATE_FIELD.equals(field);
        }
    }
}
