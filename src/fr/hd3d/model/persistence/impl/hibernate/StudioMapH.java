package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILStudioMap;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * Studio map implementation for Hibernate.
 * 
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_studiomap")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class StudioMapH extends BaseH implements IStudioMap
{
    private static final long serialVersionUID = 1L;

    private String name;
    private String imagePath;
    private Integer sizeX;
    private Integer sizeY;
    private Set<IComputer> computers;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_studiomap"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public StudioMapH()
    {}

    public StudioMapH(Long id, Timestamp version, String name, String imagePath, Integer sizeX, Integer sizeY,
            Set<IComputer> computers)
    {
        super(id, version);
        this.name = name;
        this.imagePath = imagePath;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.computers = computers;
    }

    public StudioMapH(String name, String imagePath, Integer sizeX, Integer sizeY, Set<IComputer> computers)
    {
        this(null, null, name, imagePath, sizeX, sizeY, computers);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "studiomap_name")
    @NotEmpty
    public String getName()
    {
        return name;
    }

    @Column(name = "studiomap_image_path")
    public String getImagePath()
    {
        return imagePath;
    }

    @Column(name = "studiomap_size_x")
    public Integer getSizeX()
    {
        return sizeX;
    }

    @Column(name = "studiomap_size_y")
    public Integer getSizeY()
    {
        return sizeY;
    }

    @OneToMany(targetEntity = ComputerH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "studioMap")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IComputer> getComputers()
    {
        if (this.computers == null)
        {
            this.computers = new HashSet<IComputer>();
        }
        return this.computers;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setImagePath(String imagePath)
    {
        this.imagePath = imagePath;
    }

    public void setSizeX(Integer sizeX)
    {
        this.sizeX = sizeX;
    }

    public void setSizeY(Integer sizeY)
    {
        this.sizeY = sizeY;
    }

    public void setComputers(Set<IComputer> computers)
    {
        this.computers = computers;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((imagePath == null) ? 0 : imagePath.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((sizeX == null) ? 0 : sizeX.hashCode());
        result = prime * result + ((sizeY == null) ? 0 : sizeY.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        StudioMapH other = (StudioMapH) obj;
        if (imagePath == null)
        {
            if (other.imagePath != null)
                return false;
        }
        else if (!imagePath.equals(other.imagePath))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (sizeX == null)
        {
            if (other.sizeX != null)
                return false;
        }
        else if (!sizeX.equals(other.sizeX))
            return false;
        if (sizeY == null)
        {
            if (other.sizeY != null)
                return false;
        }
        else if (!sizeY.equals(other.sizeY))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    public void addComputer(IComputer computer)
    {
        getComputers().add(computer);
        computer.setStudioMap(this);
    }

    public IBaseTranslator<ILStudioMap, IStudioMap> defaultTranslator()
    {
        return Translators.studiomap;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("name", name).append("imagePath", imagePath).append("sizeX", sizeX).append("sizeY", sizeY);
    // b.append("computerIds", StringUtils.join(CollectUtils.getIds(getComputers()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
