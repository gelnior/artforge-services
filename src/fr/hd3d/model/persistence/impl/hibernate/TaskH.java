package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dCollectionQuery;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.TaskHandler;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.collectionquery.TaskActivitiesDurationCollectionQuery;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Indexed
@Table(name = "hd3d_task")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class TaskH extends TaskBaseH implements ITask
{
    private static final long serialVersionUID = 1L;

    private static final String STATUS_FIELD = "status";
    private static final String COMPLETION_DATE_FIELD = "completionDate";
    private static final String DEADLINE_DATE_FIELD = "deadLineDate";
    private static final String ACTUALSTART_DATE_FIELD = "actualStartDate";
    private static final String ACTUALEND_DATE_FIELD = "actualEndDate";

    private String name;
    private ETaskStatus status;
    private Byte completion;
    private Date completionDate;
    private Set<ITaskActivity> taskActivities;
    private Set<ITaskChanges> taskChanges;
    private IPerson creator;
    private IPerson worker;
    private IProject project;
    private Date deadLine;
    private Date actualStartDate;
    private Date actualEndDate;
    private Boolean startable;
    private Boolean confirmed;
    private Boolean startup;
    private ITaskType taskType;
    private IEntityTaskLink boundEntityTaskLink;
    private List<IEntityTaskLink> boundEntityTaskLinks;
    /* Transient fields */
    private String boundEntityName;
    private Long boundEntityId;
    private String commentForApprovalNote;
    private IBase boundEntity = null;
    private boolean isUpdatedByApprovalNote = false;

    private Set<IFileRevision> fileRevisionsForApprovalNote;

    @Hd3dCollectionQuery(TaskActivitiesDurationCollectionQuery.class)
    private Long totalActivitiesDuration;

    @Transient
    public boolean isUpdatedByApprovalNote()
    {
        return isUpdatedByApprovalNote;
    }

    public void setUpdatedByApprovalNote(boolean isUpdatedByApprovalNote)
    {
        this.isUpdatedByApprovalNote = isUpdatedByApprovalNote;
    }

    @Transient
    public Long getTotalActivitiesDuration()
    {
        return totalActivitiesDuration;
    }

    public void setTotalActivitiesDuration(Long totalActivitiesDuration)
    {
        this.totalActivitiesDuration = totalActivitiesDuration;
    }

    /*-------------
     * Constructors
     -------------*/
    public TaskH()
    {
        super();
    }

    /**
     * @param id
     * @param version
     * @param name
     * @param completion
     * @param completionDate
     * @param status
     * @param project
     * @param creator
     * @param worker
     * @param startDate
     * @param endDate
     * @param duration
     * @param deadLine
     * @param actualStartDate
     * @param actualEndDate
     * @param startable
     * @param confirmed
     */
    public TaskH(Long id, java.sql.Timestamp version, String name, Byte completion, Date completionDate,
            ETaskStatus status, IProject project, IPerson creator, IPerson worker, Date startDate, Date endDate,
            Long duration, Date deadLine, Date actualStartDate, Date actualEndDate, Boolean startable,
            Boolean confirmed, Boolean startup, ITaskGroup taskGroup, ITaskType taskType)
    {
        super(id, version, startDate, endDate, duration, taskGroup);
        this.name = name;
        setCompletion(completion);
        this.completionDate = completionDate;
        this.status = status;
        this.project = project;
        this.creator = creator;
        this.worker = worker;
        this.deadLine = deadLine;
        this.actualEndDate = actualEndDate;
        this.actualStartDate = actualStartDate;
        this.startable = startable;
        this.confirmed = confirmed;
        this.startup = startup;
        this.taskType = taskType;
    }

    public TaskH(String name, Byte completion, Date completionDate, ETaskStatus status, IProject project,
            IPerson creator, IPerson worker, Date startDate, Date endDate, Long duration, Date deadLine,
            Date actualStartDate, Date actualEndDate, Boolean startable, Boolean confirmed, Boolean startup,
            ITaskGroup taskGroup, ITaskType taskType)
    {
        this(null, null, name, completion, completionDate, status, project, creator, worker, startDate, endDate,
                duration, deadLine, actualStartDate, actualEndDate, startable, confirmed, startup, taskGroup, taskType);
    }

    public TaskH(ITask task)
    {
        super(task.getId(), task.getVersion(), task.getStartDate(), task.getEndDate(), task.getDuration(), task
                .getTaskGroup());
        this.name = task.getName();
        setCompletion(task.getCompletion());
        this.completionDate = task.getCompletionDate();
        this.status = task.getStatus();
        this.project = task.getProject();
        this.creator = task.getCreator();
        this.worker = task.getWorker();
        this.actualEndDate = task.getActualEndDate();
        this.actualStartDate = task.getActualStartDate();
        this.confirmed = task.getConfirmed();
        this.deadLine = task.getDeadLine();
        this.startable = task.getStartable();
        this.startup = task.getStartup();
        this.taskType = task.getTaskType();
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    // @OneToOne(targetEntity = EntityTaskLinkH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch =
    // FetchType.LAZY, mappedBy = "task")
    // @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // @NotFound(action = NotFoundAction.IGNORE)
    @Transient
    public IEntityTaskLink getBoundEntityTaskLink()
    {
        if (boundEntityTaskLink != null)
            return boundEntityTaskLink;

        List<IEntityTaskLink> links = getBoundEntityTaskLinks();

        if (CollectUtils.isNotEmpty(links))
        {
            this.boundEntityTaskLink = links.get(0);
        }

        return boundEntityTaskLink;
    }

    public void setBoundEntityTaskLink(IEntityTaskLink entityTaskLink)
    {
        this.boundEntityTaskLink = entityTaskLink;
        if (entityTaskLink != null)
            getBoundEntityTaskLinks().add(0, entityTaskLink);
        else
            getBoundEntityTaskLinks().remove(0);
    }

    @OneToMany(targetEntity = EntityTaskLinkH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "task")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public List<IEntityTaskLink> getBoundEntityTaskLinks()
    {
        if (boundEntityTaskLinks == null)
            boundEntityTaskLinks = new ArrayList<IEntityTaskLink>();

        return boundEntityTaskLinks;
    }

    public void setBoundEntityTaskLinks(List<IEntityTaskLink> entityTaskLinks)
    {
        this.boundEntityTaskLinks = entityTaskLinks;
        // if (CollectUtils.isNotEmpty(this.boundEntityTaskLinks))
        // {
        // setBoundEntityTaskLink(this.boundEntityTaskLinks.get(0));
        // }
    }

    @Column(name = "task_name", nullable = false)
    @NotEmpty
    public String getName()
    {
        return name;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "task_status", nullable = false)
    @org.hibernate.annotations.Index(name = "status_idx")
    public ETaskStatus getStatus()
    {
        return status;
    }

    @Column(name = "task_completion")
    @Min(value = 0)
    public Byte getCompletion()
    {
        return completion;
    }

    @Column(name = "task_completion_date")
    public Date getCompletionDate()
    {
        return completionDate;
    }

    @OneToMany(targetEntity = TaskActivityH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "task")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<ITaskActivity> getTaskActivities()
    {
        if (taskActivities == null)
        {
            taskActivities = new HashSet<ITaskActivity>();
        }
        return taskActivities;
    }

    @OneToMany(targetEntity = TaskChangesH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "task")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<ITaskChanges> getTaskChanges()
    {
        if (taskChanges == null)
        {
            taskChanges = new HashSet<ITaskChanges>();
        }
        return taskChanges;
    }

    @ManyToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "task_creator_person_id")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getCreator()
    {
        return creator;
    }

    @ManyToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "task_worker_person_id")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getWorker()
    {
        return worker;
    }

    @ManyToOne(targetEntity = ProjectH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    // must be earger here
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "task_project_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    @Column(name = "task_confirmed")
    public Boolean getConfirmed()
    {
        return confirmed;
    }

    @Column(name = "task_deadline")
    public Date getDeadLine()
    {
        return deadLine;
    }

    @Column(name = "task_actual_start_date")
    @org.hibernate.annotations.Index(name = "actual_start_date_idx")
    public Date getActualStartDate()
    {
        return actualStartDate;
    }

    @Column(name = "task_actual_end_date")
    @org.hibernate.annotations.Index(name = "actual_end_date_idx")
    public Date getActualEndDate()
    {
        return actualEndDate;
    }

    @Column(name = "task_startable")
    public Boolean getStartable()
    {
        return startable;
    }

    @Column(name = "task_startup")
    public Boolean getStartup()
    {
        return startup;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setStatus(ETaskStatus status)
    {
        this.status = status;

        if (status == ETaskStatus.OK || status == ETaskStatus.CANCELLED || status == ETaskStatus.CLOSE)
        {
            if (getCompletionDate() != null)
            {
                /* Do not use Date, because Hibernate stores dates as TimeStamp */
                setCompletionDate(new Timestamp(new Date().getTime()));
            }
        }

        if (status == ETaskStatus.STAND_BY || status == ETaskStatus.WORK_IN_PROGRESS || status == ETaskStatus.WAIT_APP)
        {
            setCompletionDate(null);
        }
    }

    public void setCompletion(Byte completion)
    {
        this.completion = completion == null ? 0 : completion;
    }

    public void setCompletionDate(Date completionDate)
    {
        this.completionDate = completionDate;
    }

    public void setTaskActivities(Set<ITaskActivity> taskActivities)
    {
        this.taskActivities = taskActivities;
    }

    public void setTaskChanges(Set<ITaskChanges> taskChanges)
    {
        this.taskChanges = taskChanges;
    }

    public void setCreator(IPerson creator)
    {
        this.creator = creator;
    }

    public void setWorker(IPerson worker)
    {
        this.worker = worker;
    }

    public void setProject(IProject project)
    {
        this.project = project;
    }

    public void setDeadLine(Date deadLine)
    {
        this.deadLine = deadLine;
    }

    public void setActualStartDate(Date actualStartDate)
    {
        this.actualStartDate = actualStartDate;
    }

    public void setActualEndDate(Date actualEndDate)
    {
        this.actualEndDate = actualEndDate;
    }

    public void setStartable(Boolean startable)
    {
        this.startable = startable;
    }

    public void setConfirmed(Boolean confirmed)
    {
        this.confirmed = confirmed;
    }

    public void setStartup(Boolean startup)
    {
        this.startup = startup;
    }

    @ManyToOne(targetEntity = TaskTypeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "task__tasktype_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public ITaskType getTaskType()
    {
        return taskType;
    }

    public void setTaskType(ITaskType taskType)
    {
        this.taskType = taskType;
    }

    @Transient
    public String getCommentForApprovalNote()
    {
        return commentForApprovalNote;
    }

    public void setCommentForApprovalNote(String commentForApprovalNote)
    {
        this.commentForApprovalNote = commentForApprovalNote;
    }

    @Transient
    public Set<IFileRevision> getFileRevisionsForApprovalNote()
    {
        if (fileRevisionsForApprovalNote == null)
        {
            fileRevisionsForApprovalNote = new HashSet<IFileRevision>();
        }
        return fileRevisionsForApprovalNote;
    }

    @Transient
    public void setFileRevisionsForApprovalNote(Set<IFileRevision> fileRevision)
    {
        this.fileRevisionsForApprovalNote = fileRevision;
    }

    @Transient
    public Long getBoundEntityId()
    {
        return boundEntityId;
    }

    public void setBoundEntityId(Long boundEntityId)
    {
        this.boundEntityId = boundEntityId;
    }

    public void setBoundEntityName(String boundEntityName)
    {
        this.boundEntityName = boundEntityName;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((actualEndDate == null) ? 0 : actualEndDate.hashCode());
        result = prime * result + ((actualStartDate == null) ? 0 : actualStartDate.hashCode());
        result = prime * result + ((completionDate == null) ? 0 : completionDate.hashCode());
        result = prime * result + ((creator == null) ? 0 : creator.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((taskType == null) ? 0 : taskType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        TaskH other = (TaskH) obj;
        if (actualEndDate == null)
        {
            if (other.actualEndDate != null)
                return false;
        }
        else if (!actualEndDate.equals(other.actualEndDate))
            return false;
        if (actualStartDate == null)
        {
            if (other.actualStartDate != null)
                return false;
        }
        else if (!actualStartDate.equals(other.actualStartDate))
            return false;
        if (completionDate == null)
        {
            if (other.completionDate != null)
                return false;
        }
        else if (!completionDate.equals(other.completionDate))
            return false;
        if (creator == null)
        {
            if (other.creator != null)
                return false;
        }
        else if (!creator.equals(other.creator))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (status == null)
        {
            if (other.status != null)
                return false;
        }
        else if (!status.equals(other.status))
            return false;
        if (taskType == null)
        {
            if (other.taskType != null)
                return false;
        }
        else if (!taskType.equals(other.taskType))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    // Note: DO NOT DELETE
    @Transient
    public IBase boundEntity() throws Hd3dException
    {
        IBase ret = null;

        if (boundEntity != null)
        {
            ret = boundEntity;
        }
        else
        {
            IEntityTaskLink entityTaskLink = getBoundEntityTaskLink();
            if (entityTaskLink != null)
            {
                IPersist<?> persistor = Persistors.getPersistor(entityTaskLink.getBoundEntityName());
                if (persistor != null)
                {
                    boundEntity = (IBase) persistor.getById(entityTaskLink.getBoundEntity());
                    if (boundEntity != null)
                    {
                        setBoundEntityId(boundEntity.getId());
                        setBoundEntityName(boundEntity.entityName());
                    }
                    ret = boundEntity;
                }
            }
        }

        return ret;
    }

    @Transient
    public String getBoundEntityName() throws Hd3dException
    {
        if (boundEntityName != null)
            return boundEntityName;
        else
        {
            final IBase boundEntity = boundEntity();
            boundEntityName = boundEntity == null ? null : boundEntity.entityName();
            return boundEntityName;
        }
    }

    @Transient
    @Field(name = "bound_name", index = Index.UN_TOKENIZED, store = Store.YES)
    public String getBoundEntityNameLowerCase() throws Hd3dException
    {
        if (getBoundEntityName() != null)
            return StringUtils.lowerCase(getBoundEntityName());
        else
            return null;
    }

    @Transient
    @Field(name = "wo_name", index = Index.UN_TOKENIZED, store = Store.YES)
    /* Note: must be UNTOKENIZED because may contain "_" */
    public String getBoundEntityWOName() throws Hd3dException
    {
        String ret = null;

        final IBase boundEntity = boundEntity();
        if (boundEntity != null)
        {
            if (boundEntity instanceof IConstituent)
            {
                ret = StringUtils.lowerCase(((IConstituent) boundEntity).getLabel());
            }
            else if (boundEntity instanceof IShot)
            {
                ret = StringUtils.lowerCase(((IShot) boundEntity).getLabel());
            }
        }

        return ret;
    }

    /**
     * returns the parent ids of this entity as a String. Note that when the bound entity is updated, this task must be
     * re-indexed.
     * 
     * @return
     * @throws Hd3dException
     */
    @Transient
    @Field(name = "bound_par_ids", index = Index.TOKENIZED, store = Store.YES)
    public String getBoundEntityParentsIdString() throws Hd3dException
    {
        String ret = null;

        final IBase boundEntity = boundEntity();
        if (boundEntity != null)
        {
            if (boundEntity instanceof ConstituentH)
            {
                ret = ((ConstituentH) boundEntity).getCategoriesIdString();
            }
            else if (boundEntity instanceof ShotH)
            {
                ret = ((ShotH) boundEntity).getSequencesIdString();
            }
        }

        return ret;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILTask, ITask> defaultTranslator()
    {
        return Translators.task;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    @Override
    public void disable() throws Hd3dException
    {
        super.disable();
        /* a task cannot be disabled if some activities have been reported */
        if (hasNotEmptyActivities())
        {
            throw new Hd3dException("task (id=" + getId()
                    + ") cannot be disabled: some activities have been reported (id="
                    + StringUtils.join(CollectUtils.getIds(getTaskActivities()), ',') + ")", false);
        }

        setInternalStatus(Const.INTERNALSTATUS_DISABLED);
        postDisable();
    }

    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        /* disable all related EntityTaskLinks */
        for (IEntityTaskLink etl : getBoundEntityTaskLinks())
        {
            etl.disable();
        }
        /* disable related approvalnotes */
        TaskHandler h = new TaskHandler(this);
        Persistors.approvalnote.disableCollection(h.getRelatedApprovalNotes());
    }

    @Transient
    public boolean hasActivities()
    {
        return !getTaskActivities().isEmpty();
    }

    @Transient
    public boolean hasNotEmptyActivities()
    {
        boolean ret = false;

        for (ITaskActivity aTaskActivity : getTaskActivities())
        {
            if (aTaskActivity.getDuration() > 0L)
            {
                ret = true;
                break;
            }
        }

        return ret;
    }

    public String toString()
    {
        return name;
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class TaskComplexTypeProvider extends TaskBaseComplexTypeProvider implements IComplexTypeProvider
    {
        public Object getValue(String field, String value)
        {
            Object ret = super.getValue(field, value);
            if (ret != null)
                return ret;

            if (STATUS_FIELD.equals(field))
            {
                return EnumUtils.getEnumValueIgnoreCase(ETaskStatus.class, value);
            }
            else if (COMPLETION_DATE_FIELD.equals(field) || DEADLINE_DATE_FIELD.equals(field)
                    || ACTUALSTART_DATE_FIELD.equals(field) || ACTUALEND_DATE_FIELD.equals(field))
            {
                return fr.hd3d.utils.StringUtils.parseDate(value);
            }
            else
                return null;
        }

        public List<String> getValidEnum(String field)
        {
            if (STATUS_FIELD.equals(field))
                return EnumUtils.getEnumStringValues(ETaskStatus.class);
            return Collections.emptyList();
        }

        public boolean isValidTypeField(String field)
        {
            boolean validTypeField = super.isValidTypeField(field);
            if (!validTypeField)
            {
                if (STATUS_FIELD.equals(field) || COMPLETION_DATE_FIELD.equals(field)
                        || DEADLINE_DATE_FIELD.equals(field) || ACTUALSTART_DATE_FIELD.equals(field)
                        || ACTUALEND_DATE_FIELD.equals(field))
                    return true;
            }
            else
            {
                return false;
            }
            return false;
        }
    }
}
