package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTagCategory;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


/**
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_tagcategory")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class TagCategoryH extends BaseH implements ITagCategory
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String name;
    private ITagCategory parent;
    private List<ITagCategory> children;
    private List<ITag> boundTags;// IMPORTANT: do not use "tags" to avoid confusion with global "tags"

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_tagcategory"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public TagCategoryH()
    {}

    public TagCategoryH(Long id, Timestamp version, String name, ITagCategory parent, List<ITag> boundTags)
    {
        super(id, version);
        this.name = name;
        this.parent = parent;
        this.boundTags = boundTags;
    }

    public TagCategoryH(String name, ITagCategory parent, List<ITag> boundTags)
    {
        this(null, null, name, parent, boundTags);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "tagcategory_name", nullable = false)
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @ManyToOne(targetEntity = TagCategoryH.class, fetch = FetchType.LAZY)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public ITagCategory getParent()
    {
        return parent;
    }

    public void setParent(ITagCategory parent)
    {
        this.parent = parent;
    }

    @OneToMany(targetEntity = TagCategoryH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parent")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public List<ITagCategory> getChildren()
    {
        if (children == null)
            children = new ArrayList<ITagCategory>();
        return children;
    }

    public void setChildren(List<ITagCategory> children)
    {
        this.children = children;
    }

    @ManyToMany(targetEntity = TagH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    // @IndexColumn is IMPORTANT: to avoid "cannot recreate collection while filter is enabled" error
    @IndexColumn(name = "tagcategory_index", base = 0)
    @JoinTable(name = "hd3d_tagcategory__tag", joinColumns = @JoinColumn(name = "tagcategory_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public List<ITag> getBoundTags()
    {
        if (boundTags == null)
            boundTags = new ArrayList<ITag>();
        return boundTags;
    }

    public void setBoundTags(List<ITag> tags)
    {
        this.boundTags = tags;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        TagCategoryH other = (TagCategoryH) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Transient
    public Set<ITagCategory> getParents()
    {
        Set<ITagCategory> parents = new HashSet<ITagCategory>();
        for (ITagCategory parent = getParent(); parent != null; parent = parent.getParent())
            parents.add(parent);

        return parents;
    }

    public List<ITagCategory> allChildren(boolean includeSelf)
    {
        List<ITagCategory> allChildren = new ArrayList<ITagCategory>();
        if (includeSelf)
            allChildren.add(this);
        for (ITagCategory tagCategory : getChildren())
            allChildren.addAll(tagCategory.allChildren(includeSelf));

        return allChildren;
    }

    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        // cascade disable
        Collection<ITagCategory> tagCategories = CollectUtils.selectNotNull(allChildren(false));
        Persistors.tagcategory.disableCollection(tagCategories);
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    @Override
    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public IBaseTranslator<ILTagCategory, ITagCategory> defaultTranslator()
    {
        return Translators.tagcategory;
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("name", name);
    // b.append("parentId", parent == null ? "null" : parent.getId());
    // b.append("childrenIds", StringUtils.join(CollectUtils.getIds(getChildren()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
