package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILDevice;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.IDeviceModel;
import fr.hd3d.model.persistence.IDeviceType;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


@Entity
@Table(name = "hd3d_device")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class DeviceH extends InventoryItemH implements IDevice
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Float size;
    private IDeviceType type;
    private IDeviceModel deviceModel;
    private Set<IComputer> computers;

    /*-------------
     * Constructors
     -------------*/
    public DeviceH()
    {}

    public DeviceH(Long id, Timestamp version, String name, String serial, String billingReference, Date purchaseDate,
            Date warrantyEnd, EInventoryStatus inventoryStatus, Float size, IDeviceType type, IDeviceModel deviceModel,
            Set<IComputer> computers, Set<IResourceGroup> resourceGroups)
    {
        super(id, version, name, serial, billingReference, purchaseDate, warrantyEnd, inventoryStatus, resourceGroups);

        this.size = size;
        this.type = type;
        this.deviceModel = deviceModel;
        this.computers = computers;
    }

    public DeviceH(String name, String serial, String billingReference, Date purchaseDate, Date warrantyEnd,
            EInventoryStatus inventoryStatus, Float size, IDeviceType type, IDeviceModel deviceModel,
            Set<IComputer> computers, Set<IResourceGroup> resourceGroups)
    {
        this(null, null, name, serial, billingReference, purchaseDate, warrantyEnd, inventoryStatus, size, type,
                deviceModel, computers, resourceGroups);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "device_size")
    public Float getSize()
    {
        return size;
    }

    public void setSize(Float size)
    {
        this.size = size;
    }

    @ManyToOne(targetEntity = DeviceTypeH.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "device_type_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IDeviceType getDeviceType()
    {
        return type;
    }

    public void setDeviceType(IDeviceType type)
    {
        this.type = type;
    }

    @ManyToOne(targetEntity = DeviceModelH.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "device_model_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IDeviceModel getDeviceModel()
    {
        return this.deviceModel;
    }

    public void setDeviceModel(IDeviceModel deviceModel)
    {
        this.deviceModel = deviceModel;
    }

    @ManyToMany(targetEntity = ComputerH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_computer__device", joinColumns = @JoinColumn(name = "device_id"), inverseJoinColumns = @JoinColumn(name = "computer_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IComputer> getComputers()
    {
        if (this.computers == null)
        {
            this.computers = new HashSet<IComputer>();
        }

        return this.computers;
    }

    public void setComputers(Set<IComputer> computers)
    {
        this.computers = computers;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((computers == null) ? 0 : computers.hashCode());
        // result = prime * result + Float.floatToIntBits(size);
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        DeviceH other = (DeviceH) obj;
        if (computers == null)
        {
            if (other.computers != null)
                return false;
        }
        else if (!computers.equals(other.computers))
            return false;
        if (Float.floatToIntBits(size) != Float.floatToIntBits(other.size))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        for (IComputer c : getComputers())
        {
            c.getDevices().remove(this);
        }
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILDevice, IDevice> defaultTranslator()
    {
        return Translators.device;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("size", size);
    //
    // b.append("typeId", type == null ? "null" : type.getId());
    // b.append("deviceModelId", deviceModel == null ? "null" : deviceModel.getId());
    // b.append("computerIds", StringUtils.join(CollectUtils.getIds(getComputers()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class DeviceComplexTypeProvider extends InventoryItemComplexTypeProvider
    {
        public Object getValue(String field, String value)
        {
            return super.getValue(field, value);
        }

        public List<String> getValidEnum(String field)
        {
            return super.getValidEnum(field);
        }

        public boolean isValidTypeField(String field)
        {
            return super.isValidTypeField(field);
        }
    }
}
