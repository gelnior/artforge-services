package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


/**
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_playlistrevisiongroup")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class PlayListRevisionGroupH extends BaseH implements IPlayListRevisionGroup
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String name;
    private IProject project;
    private IPlayListRevisionGroup parent;
    private Collection<IPlayListRevisionGroup> children;

    private List<IPlayListRevision> playListRevisions;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_playlistrevisiongroup"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public PlayListRevisionGroupH()
    {}

    public PlayListRevisionGroupH(Long id, Timestamp version, String name, IPlayListRevisionGroup parent)
    {
        super(id, version);
        this.name = name;
        this.parent = parent;
    }

    public PlayListRevisionGroupH(String name, IPlayListRevisionGroup parent)
    {
        this(null, null, name, parent);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @ManyToOne(targetEntity = ProjectH.class)
    @JoinColumn(name = "playlistrevisiongroup__project")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    public void setProject(IProject project)
    {
        this.project = project;
    }

    @ManyToOne(targetEntity = PlayListRevisionGroupH.class, fetch = FetchType.LAZY)
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IPlayListRevisionGroup getParent()
    {
        return parent;
    }

    public void setParent(IPlayListRevisionGroup parent)
    {
        if (parent != this)
        {
            this.parent = parent;
        }
    }

    @OneToMany(targetEntity = PlayListRevisionGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parent")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Collection<IPlayListRevisionGroup> getChildren()
    {
        if (children == null)
        {
            children = new ArrayList<IPlayListRevisionGroup>();
        }
        return children;
    }

    public void setChildren(Collection<IPlayListRevisionGroup> children)
    {
        this.children = children;
    }

    @OneToMany(targetEntity = PlayListRevisionH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "playListRevisionGroup")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public List<IPlayListRevision> getPlayListRevisions()
    {
        if (this.playListRevisions == null)
        {
            this.playListRevisions = new ArrayList<IPlayListRevision>();
        }
        return this.playListRevisions;
    }

    public void setPlayListRevisions(List<IPlayListRevision> playListRevisions)
    {
        this.playListRevisions = playListRevisions;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((children == null) ? 0 : children.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PlayListRevisionGroupH other = (PlayListRevisionGroupH) obj;
        if (children == null)
        {
            if (other.children != null)
                return false;
        }
        else if (!children.equals(other.children))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();

    }

    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Transient
    public static boolean isRootPlayListRevisionGroup(IPlayListRevisionGroup group)
    {
        return group != null && Const.PLAYLIST_REVISION_GROUP_ROOT.equals(group.getName());
    }

    @Transient
    public boolean isRoot()
    {
        return isRootPlayListRevisionGroup(this);
    }

    /**
     * return all the parents of this category recursively
     * 
     * @return
     */
    public Set<IPlayListRevisionGroup> parents()
    {
        Set<IPlayListRevisionGroup> parents = new HashSet<IPlayListRevisionGroup>();
        for (IPlayListRevisionGroup group = getParent(); group != null; group = group.getParent())
        {
            parents.add(group);
        }
        return parents;
    }

    public Collection<IPlayListRevisionGroup> allChildren(boolean includeSelf)
    {
        List<IPlayListRevisionGroup> allChildren = new ArrayList<IPlayListRevisionGroup>();
        if (includeSelf)
        {
            allChildren.add(this);
        }
        for (IPlayListRevisionGroup group : getChildren())
        {
            allChildren.addAll(group.allChildren(includeSelf));
        }

        return allChildren;
    }

    @Transient
    public String getParentIds()
    {
        if (parents() == null)
        {
            return null;
        }

        return StringUtils.join(CollectUtils.getIds(parents()), ' ');
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILPlayListRevisionGroup, IPlayListRevisionGroup> defaultTranslator()
    {
        return Translators.playlistrevisiongroup;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
