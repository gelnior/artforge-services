package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILScript;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.impl.hibernate.query.Queries;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * Stores scripts to run triggered on Entity's operation (CUD, Creation, Update, Delete)
 * 
 * @author Try LAM
 * 
 */
@Entity
@Table(name = "hd3d_script")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@NamedQueries( {
        @javax.persistence.NamedQuery(name = "script.getAll", query = "from ScriptH"),
        @javax.persistence.NamedQuery(name = "script.getByName", query = "from ScriptH s where s.name = :scriptname and internalStatus=0") })
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ScriptH extends BaseH implements IScript
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String LASTRUN_FIELD = "lastRun";

    private String trigger;
    private String name;
    private String description;
    private String location;
    private Integer exitCode;// expected exit code
    private java.util.Date lastRun;
    private Integer lastExitCode;
    private String lastRunMessage;

    private static MultiHashMap<String, IScript> scripts = new MultiHashMap<String, IScript>();
    public volatile static boolean refreshScriptsMap = true;
    static ReadWriteLock rw = new ReentrantReadWriteLock();

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_script"), @Parameter(name = "initial_value", value = "1"),
            @Parameter(name = "increment_size", value = "1"), @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ScriptH()
    {}

    public ScriptH(Long id, Timestamp version, String trigger, String name, String description, String location,
            Integer exitCode)
    {
        super(id, version);
        this.trigger = trigger;
        this.name = name;
        this.description = description;
        this.location = location;
        this.exitCode = exitCode;
    }

    public ScriptH(String trigger, String name, String description, String location, Integer exitCode)
    {
        this(null, null, trigger, name, description, location, exitCode);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "script_trigger")
    public String getTrigger()
    {
        return trigger;
    }

    @Column(name = "script_name")
    @NotEmpty
    public String getName()
    {
        return name;
    }

    @Column(name = "script_description")
    public String getDescription()
    {
        return description;
    }

    @Column(name = "script_location", nullable = false)
    public String getLocation()
    {
        return location;
    }

    @Column(name = "script_exitcode", nullable = false)
    public Integer getExitCode()
    {
        return exitCode;
    }

    @Column(name = "script_lastrun")
    public java.util.Date getLastRun()
    {
        return lastRun;
    }

    @Column(name = "script_lastexitcode")
    public Integer getLastExitCode()
    {
        return lastExitCode;
    }

    @Column(name = "script_lastrunmessage")
    public String getLastRunMessage()
    {
        return lastRunMessage;
    }

    public void setLastRunMessage(String lastRunMessage)
    {
        this.lastRunMessage = lastRunMessage;
    }

    public void setLastRun(java.util.Date lastRun)
    {
        this.lastRun = lastRun;
    }

    public void setLastExitCode(Integer lastExitCode)
    {
        this.lastExitCode = lastExitCode;
    }

    public void setTrigger(String trigger)
    {
        this.trigger = trigger;
    }

    public void setName(String name) throws Hd3dException
    {
        if (StringUtils.contains(name, ' '))
            throw new Hd3dException("Script name must not contain space");
        this.name = name;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public void setExitCode(Integer exitCode)
    {
        this.exitCode = exitCode;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((trigger == null) ? 0 : trigger.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ScriptH other = (ScriptH) obj;
        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (trigger == null)
        {
            if (other.trigger != null)
                return false;
        }
        else if (!trigger.equals(other.trigger))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    public static MultiHashMap<String, IScript> scripts()
    {
        if (refreshScriptsMap)
        {
            rw.writeLock().lock();
            try
            {
                ScriptH.scripts.clear();

                List<ScriptH> result = Queries.scripts();
                for (ScriptH sc : result)
                    ScriptH.scripts.put(sc.getTrigger(), sc);
                refreshScriptsMap = false;
            }
            finally
            {
                rw.writeLock().unlock();
            }
        }
        return scripts;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILScript, IScript> defaultTranslator()
    {
        return Translators.script;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // return new ToStringBuilder(this).appendSuper(super.toString()).append("trigger", trigger).append("name", name)
    // .append("location", location).append("exitCode", exitCode).append("lastRun", lastRun).append(
    // "lastExitCode", lastExitCode).append("lastRunMessage", lastRunMessage).toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class ScriptComplexTypeProvider implements IComplexTypeProvider
    {
        public List<String> getValidEnum(String field)
        {
            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            if (!isValidTypeField(field))
                return null;

            return fr.hd3d.utils.StringUtils.parseDate(value);
        }

        public boolean isValidTypeField(String field)
        {
            return LASTRUN_FIELD.equals(field);
        }
    }
}
