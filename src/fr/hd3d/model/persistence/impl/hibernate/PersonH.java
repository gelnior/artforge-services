package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.common.client.PersonNameUtils;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.ISkillLevel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


/**
 * A34 time sheet implementation https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Table(name = "hd3d_person")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class PersonH extends ResourceH implements IPerson
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String login;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String photoPath;

    private Set<IPersonDay> days;
    private Set<ISkillLevel> skillLevels;
    private Set<IResourceGroup> ledResourceGroups;// resource Groups where this person is the leader
    private Set<IContract> contracts;

    /*-------------
     * Constructors
     -------------*/
    public PersonH()
    {}

    public PersonH(Long id, java.sql.Timestamp version, String login, String firstName, String lastName, String email,
            String phone, String photoPath, Set<IResourceGroup> resourceGroups, Set<IContract> contracts)
    {
        super(id, version, resourceGroups);

        this.setLogin(login);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.photoPath = photoPath;
        this.contracts = contracts;
    }

    public PersonH(String login, String firstName, String lastName, String email, String phone, String photoPath,
            Set<IResourceGroup> resourceGroups, Set<IContract> contracts)
    {
        this(null, null, login, firstName, lastName, email, phone, photoPath, resourceGroups, contracts);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "person_login", nullable = false)
    /*
     * Note: DO NOT USE NaturalId, as an Id it must be unique and will prevent from having several same login (one
     * disabled and one active)
     */
    // @NaturalId(mutable = true)
    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    @Column(name = "person_first_name", nullable = false)
    @NotEmpty
    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    @Column(name = "person_last_name")
    @NotEmpty
    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    @Column(name = "person_email")
    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    @Column(name = "person_phone")
    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    @Column(name = "person_photo_path")
    public String getPhotoPath()
    {
        return photoPath;
    }

    public void setPhotoPath(String photoPath)
    {
        this.photoPath = photoPath;
    }

    @OneToMany(targetEntity = PersonDayH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "person")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IPersonDay> getDays()
    {
        if (days == null)
            days = new HashSet<IPersonDay>();
        return days;
    }

    public void setDays(Set<IPersonDay> days)
    {
        this.days = days;
    }

    @ManyToMany(targetEntity = ResourceGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "leader_id")
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IResourceGroup> getLedResourceGroups()
    {
        return this.ledResourceGroups;
    }

    public void setLedResourceGroups(Set<IResourceGroup> resourceGroups)
    {
        this.ledResourceGroups = resourceGroups;
    }

    @OneToMany(targetEntity = SkillLevelH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "person")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<ISkillLevel> getSkillLevels()
    {
        if (skillLevels == null)
            skillLevels = new HashSet<ISkillLevel>();
        return skillLevels;
    }

    public void setSkillLevels(Set<ISkillLevel> skillLevels)
    {
        this.skillLevels = skillLevels;
    }

    @OneToMany(targetEntity = ContractH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "person")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IContract> getContracts()
    {
        return contracts;
    }

    public void setContracts(Set<IContract> contracts)
    {
        this.contracts = contracts;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((login == null) ? 0 : login.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PersonH other = (PersonH) obj;
        if (firstName == null)
        {
            if (other.firstName != null)
                return false;
        }
        else if (!firstName.equals(other.firstName))
            return false;
        if (lastName == null)
        {
            if (other.lastName != null)
                return false;
        }
        else if (!lastName.equals(other.lastName))
            return false;
        if (login == null)
        {
            if (other.login != null)
                return false;
        }
        else if (!login.equals(other.login))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Transient
    public String getResourceName()
    {
        return fullName();

    }

    public String fullName()
    {
        // final String first = getFirstName();
        // final String last = getLastName();
        // StringBuilder buffer = new StringBuilder();
        // buffer.append(WordUtils.capitalizeFully(first)).append(' ').append(WordUtils.capitalizeFully(last));
        //
        // buffer.trimToSize();
        // return buffer.toString();
        return PersonNameUtils.getFullName(getLastName(), getFirstName(), getLogin());
    }

    public IBaseTranslator<ILPerson, IPerson> defaultTranslator()
    {
        return Translators.person;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        // cascade disable
        Collection<IPersonDay> personDays = CollectUtils.selectNotNull(getDays());
        Persistors.personDay.disableCollection(personDays);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return login;
    }
    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("login", login).append("firstName", firstName).append("lastName", lastName).append("email", email)
    // .append("phone", phone).append("photoPath", photoPath);
    // b.append("dayIds", StringUtils.join(CollectUtils.getIds(getDays()), ','));
    // b.append("skillLevelIds", StringUtils.join(CollectUtils.getIds(getSkillLevels()), ','));
    // b.append("ledResourceGroups", StringUtils.join(CollectUtils.getIds(getLedResourceGroups()), ','));
    // b.append("contractIds", StringUtils.join(CollectUtils.getIds(getContracts()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
