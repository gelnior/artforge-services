package fr.hd3d.model.persistence.impl.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.common.client.enums.EFrameRate;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlayListRevision;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;


/**
 * 
 * @author try LAM
 * 
 */
@Entity
@Table(name = "hd3d_playlist")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class PlayListRevisionH extends BaseH implements IPlayListRevision
{
    private static final long serialVersionUID = 1L;

    private static final String FRAMERATE = "framerate";

    private IProject project;
    private EFrameRate frameRate = EFrameRate.FILM_24;// default
    private String name;
    private Integer revision;
    private String hook;
    private String comment;
    private List<IPlayListEdit> playListEdits;
    private IPlayListRevisionGroup playListRevisionGroup;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_playlistrevision"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public PlayListRevisionH()
    {}

    public PlayListRevisionH(Long id, java.sql.Timestamp version, IProject project, EFrameRate frameRate, String name,
            Integer revision, String hook, String comment, List<IPlayListEdit> playListEdits,
            IPlayListRevisionGroup playListRevisionGroup)
    {
        super(id, version);
        this.project = project;
        if (frameRate != null)
            this.frameRate = frameRate;
        this.name = name;
        this.revision = revision;
        this.hook = hook;
        this.comment = comment;
        this.playListEdits = playListEdits;
        this.playListRevisionGroup = playListRevisionGroup;
    }

    public PlayListRevisionH(IProject project, EFrameRate frameRate, String name, Integer revision, String hook,
            String comment, List<IPlayListEdit> playListEdits, IPlayListRevisionGroup playListRevisionGroup)
    {
        this(null, null, project, frameRate, name, revision, hook, comment, playListEdits, playListRevisionGroup);
    }

    @SuppressWarnings("unchecked")
    public synchronized PlayListRevisionH copy()
    {

        final PlayListRevisionH newRevision = new PlayListRevisionH(getProject(), getFrameRate(), getName(), -1, null,
                null, new ArrayList<IPlayListEdit>(playListEdits.size()), getPlayListRevisionGroup());

        for (IPlayListEdit edit : playListEdits)
        {
            copyEditTo(edit, newRevision);
        }
        /*
         * For collection association, a new reference must be created otherwise:
         * "Found shared references to a collection" error will be raised
         */
        return newRevision;

    }

    private void copyEditTo(IPlayListEdit edit, IPlayListRevision playlist)
    {
        if (edit.getPlayListRevision() == playlist)
        {
            return;
        }
        final IPlayListEdit newEdit = new PlayListEditH();
        newEdit.setRecIn(edit.getRecIn());
        newEdit.setRecOut(edit.getRecOut());
        newEdit.setSrcIn(edit.getSrcIn());
        newEdit.setSrcOut(edit.getSrcOut());
        newEdit.setName(edit.getName());
        // newEdit.setFileRevision(edit.getFileRevision());
        newEdit.setProxy(edit.getProxy());
        newEdit.setPlayListRevision(playlist);
        playlist.getPlayListEdits().add(newEdit);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToOne(targetEntity = ProjectH.class)
    @JoinColumn(name = "playlist_project_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "playlist_framerate", nullable = false)
    public EFrameRate getFrameRate()
    {
        return frameRate;
    }

    @Column(name = "playlist_name")
    public String getName()
    {
        return name;
    }

    @Column(name = "playlist_revision")
    public Integer getRevision()
    {
        return revision;
    }

    @Column(name = "playlist_hook")
    public String getHook()
    {
        return hook;
    }

    @Column(name = "playlist_comment")
    public String getComment()
    {
        return comment;
    }

    @OneToMany(targetEntity = PlayListEditH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "playListRevision")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public List<IPlayListEdit> getPlayListEdits()
    {
        if (playListEdits == null)
            playListEdits = new ArrayList<IPlayListEdit>();
        return playListEdits;
    }

    public void setProject(IProject project)
    {
        this.project = project;
    }

    public void setFrameRate(EFrameRate frameRate)
    {
        if (frameRate != null)
            this.frameRate = frameRate;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setRevision(Integer revision)
    {
        this.revision = revision;
    }

    public void setHook(String hook)
    {
        this.hook = hook;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setPlayListEdits(List<IPlayListEdit> playListEdits)
    {
        this.playListEdits = playListEdits;
    }

    @ManyToOne(targetEntity = PlayListRevisionGroupH.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "playlist_playlistrevisiongroup")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IPlayListRevisionGroup getPlayListRevisionGroup()
    {
        return playListRevisionGroup;
    }

    public void setPlayListRevisionGroup(IPlayListRevisionGroup playListRevisionGroup)
    {
        this.playListRevisionGroup = playListRevisionGroup;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((frameRate == null) ? 0 : frameRate.hashCode());
        result = prime * result + ((hook == null) ? 0 : hook.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((project == null) ? 0 : project.hashCode());
        result = prime * result + ((revision == null) ? 0 : revision.hashCode());
        result = prime * result + ((playListRevisionGroup == null) ? 0 : playListRevisionGroup.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PlayListRevisionH other = (PlayListRevisionH) obj;
        if (comment == null)
        {
            if (other.comment != null)
                return false;
        }
        else if (!comment.equals(other.comment))
            return false;
        if (frameRate == null)
        {
            if (other.frameRate != null)
                return false;
        }
        else if (!frameRate.equals(other.frameRate))
            return false;
        if (hook == null)
        {
            if (other.hook != null)
                return false;
        }
        else if (!hook.equals(other.hook))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (project == null)
        {
            if (other.project != null)
                return false;
        }
        else if (!project.equals(other.project))
            return false;
        if (revision == null)
        {
            if (other.revision != null)
                return false;
        }
        else if (!revision.equals(other.revision))
            return false;
        if (playListRevisionGroup == null)
        {
            if (other.playListRevisionGroup != null)
                return false;
        }
        else if (!playListRevisionGroup.equals(other.playListRevisionGroup))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    public void completePrePersistOrUpdate()
    {
        if (StringUtils.isBlank(getHook()))
        {
            setHook(HookHandler.getHook(this));
        }
    }

    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        // cascade disable
        Collection<IPlayListEdit> playListEdits = CollectUtils.selectNotNull(getPlayListEdits());
        Persistors.playlistedit.disableCollection(playListEdits);
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILPlayListRevision, IPlayListRevision> defaultTranslator()
    {
        return Translators.playlistrevision;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("projectId", project == null ? "null" : project.getId());
    // b.append("frameRate", frameRate).append("name", name).append("revision", revision).append("hook", hook).append(
    // "comment", comment);
    // b.append("playListEditIds", StringUtils.join(CollectUtils.getIds(getPlayListEdits()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class PlayListRevisionComplexTypeProvider implements IComplexTypeProvider
    {
        public List<String> getValidEnum(String field)
        {
            if (FRAMERATE.equals(field))
                return EnumUtils.getEnumStringValues(EFrameRate.class);
            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            if (!isValidTypeField(field))
                return null;

            if (FRAMERATE.equals(field))
                return EnumUtils.getEnumValueIgnoreCase(EFrameRate.class, value);
            else
                return null;
        }

        public boolean isValidTypeField(String field)
        {
            return FRAMERATE.equals(field);
        }
    }

}
