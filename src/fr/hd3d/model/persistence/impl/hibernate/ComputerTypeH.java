package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.Parameter;

import fr.hd3d.model.lightweight.ILComputerType;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IComputerType;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * Pool hibernate object needed for database persistance.
 * 
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_computer_type")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ComputerTypeH extends SimpleH implements IComputerType
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Set<IComputer> computers;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_computertype"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ComputerTypeH()
    {}

    public ComputerTypeH(Long id, Timestamp version, String name)
    {
        super(id, version, name);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @OneToMany(targetEntity = ComputerH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "computerType")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IComputer> getComputers()
    {
        if (this.computers == null)
        {
            this.computers = new HashSet<IComputer>();
        }
        return this.computers;
    }

    public void setComputers(Set<IComputer> computers)
    {
        this.computers = computers;
    }

    /*------------------
     * equal & hashcode
     ------------------*/

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    public IBaseTranslator<ILComputerType, IComputerType> defaultTranslator()
    {
        return Translators.computerType;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("computerIds", StringUtils.join(CollectUtils.getIds(getComputers()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
