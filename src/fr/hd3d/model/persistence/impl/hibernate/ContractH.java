package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.common.client.enums.EContractType;
import fr.hd3d.model.lightweight.ILContract;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.IOrganization;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IQualification;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.EnumUtils;


/**
 * Contract hibernate object needed for database persistence.
 * 
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_contract")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ContractH extends SimpleH implements IContract
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Date startDate;
    private Date endDate;
    private Date realEndDate;
    private String docPath;
    private EContractType type;

    private IPerson person;
    private IQualification qualification;
    private IOrganization organization;

    private Float salary;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_contract"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ContractH()
    {}

    public ContractH(Long id, Timestamp version, String name, Date startDate, Date endDate, Date realEndDate,
            String docPath, EContractType type, IPerson person, IQualification qualification,
            IOrganization organization, Float salary)
    {
        super(id, version, name);

        this.startDate = startDate;
        this.endDate = endDate;
        this.realEndDate = realEndDate;
        this.docPath = docPath;
        this.type = type;

        this.person = person;
        this.qualification = qualification;
        this.organization = organization;

        this.salary = salary;
    }

    public ContractH(String name, Date startDate, Date endDate, Date realEndDate, String docPath, EContractType type,
            IPerson person, IQualification qualification, IOrganization organization, Float salary)
    {
        this(null, null, name, startDate, endDate, realEndDate, docPath, type, person, qualification, organization,
                salary);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "contract_start_date")
    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    @Column(name = "contract_end_date")
    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    @Column(name = "contract_real_end_date")
    public Date getRealEndDate()
    {
        return realEndDate;
    }

    public void setRealEndDate(Date realEndDate)
    {
        this.realEndDate = realEndDate;
    }

    @Column(name = "contract_doc_path")
    public String getDocPath()
    {
        return docPath;
    }

    public void setDocPath(String docPath)
    {
        this.docPath = docPath;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "contract_type", nullable = false)
    public EContractType getType()
    {
        return type;
    }

    public void setType(EContractType type)
    {
        this.type = type;
    }

    @ManyToOne(targetEntity = PersonH.class)
    @BatchSize(size = 50)
    @JoinColumn(name = "person_id")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getPerson()
    {
        return person;
    }

    public void setPerson(IPerson person)
    {
        this.person = person;
    }

    @ManyToOne(targetEntity = QualificationH.class)
    @JoinColumn(name = "qualification_id")
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IQualification getQualification()
    {
        return qualification;
    }

    public void setQualification(IQualification qualification)
    {
        this.qualification = qualification;
    }

    @ManyToOne(targetEntity = OrganizationH.class)
    @JoinColumn(name = "organization_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public IOrganization getOrganization()
    {
        return organization;
    }

    public void setOrganization(IOrganization organization)
    {
        this.organization = organization;
    }

    @Column(name = "contract_salary")
    public Float getSalary()
    {
        return salary;
    }

    public void setSalary(Float salary)
    {
        this.salary = salary;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ContractH other = (ContractH) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    public IBaseTranslator<ILContract, IContract> defaultTranslator()
    {
        return Translators.contract;
    }

    @Override
    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("startDate", startDate).append("endDate", endDate).append("realEndDate", realEndDate).append(
    // "docPath", docPath).append("type", type);
    //
    // b.append("personId", person == null ? "null" : person.getId());
    // b.append("qualificationId", qualification == null ? "null" : qualification.getId());
    // b.append("organizationId", organization == null ? "null" : organization.getId());
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class ContractComplexTypeProvider implements IComplexTypeProvider
    {
        private static final String START_DATE_FIELD = "startDate";
        private static final String END_DATE_FIELD = "endDate";
        private static final String REAL_END_DATE_FIELD = "realEndDate";
        private static final String TYPE_FIELD = "type";

        public List<String> getValidEnum(String field)
        {
            return Collections.emptyList();
        }

        public Object getValue(String field, String value)
        {
            Object ret = null;

            if (TYPE_FIELD.equals(field))
            {
                ret = EnumUtils.getEnumValueIgnoreCase(EContractType.class, value);
            }
            else if (isDateField(field))
            {
                ret = fr.hd3d.utils.StringUtils.parseDate(value);
            }

            return ret;
        }

        public boolean isDateField(String field)
        {
            return START_DATE_FIELD.equals(field) || END_DATE_FIELD.equals(field) || REAL_END_DATE_FIELD.equals(field);
        }

        public boolean isValidTypeField(String field)
        {
            return START_DATE_FIELD.equals(field) || END_DATE_FIELD.equals(field) || REAL_END_DATE_FIELD.equals(field)
                    || TYPE_FIELD.equals(field);
        }
    }
}
