package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.common.client.enums.EWorkerStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILComputer;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IComputerModel;
import fr.hd3d.model.persistence.IComputerType;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.IProcessor;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRoom;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.EnumUtils;


/**
 * Computer implementation for Hibernate.
 * 
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_computer")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ComputerH extends InventoryItemH implements IComputer
{
    private static final long serialVersionUID = -3672969727547388216L;
    private static final String WORKER_STATUS_FIELD = "workerStatus";

    private Long inventoryId;
    private String dnsName;
    private String ipAdress;
    private String macAdress;
    private Integer procFrequency;
    private Integer numberOfProcs;
    private Integer numberOfCores;
    private Integer ramQuantity;
    private Integer xPosition;
    private Integer yPosition;
    private EWorkerStatus workerStatus;
    private IProcessor processor;
    private IComputerType computerType;
    private IComputerModel computerModel;
    private IStudioMap studioMap;
    private IRoom room;
    private IPerson person;
    private Set<IPool> pools;
    private Set<ISoftware> softwares;
    private Set<ILicense> licenses;
    private Set<IDevice> devices;
    private Set<IScreen> screens;

    /*-------------
     * Constructors
     -------------*/
    public ComputerH()
    {}

    public ComputerH(Long id, Timestamp version, String name, String serial, String billingReference,
            Date purchaseDate, Date warrantyEnd, EInventoryStatus inventoryStatus, Long inventoryId, String dnsName,
            String ipAdress, String macAdress, Integer procFrequency, Integer numberOfProcs, Integer numberOfCores,
            Integer ramQuantity, EWorkerStatus workerStatus, Integer xPosition, Integer yPosition,
            IProcessor processor, IComputerType computerType, IComputerModel computerModel, IStudioMap studioMap,
            IRoom room, Set<IPool> pools, Set<ISoftware> softwares, Set<ILicense> licenses, Set<IDevice> devices,
            Set<IScreen> screens, Set<IResourceGroup> resourceGroups)
    {
        super(id, version, name, serial, billingReference, purchaseDate, warrantyEnd, inventoryStatus, resourceGroups);
        this.inventoryId = inventoryId;
        this.dnsName = dnsName;
        this.ipAdress = ipAdress;
        this.macAdress = macAdress;
        this.procFrequency = procFrequency;
        this.numberOfProcs = numberOfProcs;
        this.numberOfCores = numberOfCores;
        this.ramQuantity = ramQuantity;
        this.workerStatus = workerStatus;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.processor = processor;
        this.computerType = computerType;
        this.computerModel = computerModel;
        this.studioMap = studioMap;
        this.room = room;
        this.pools = pools;
        this.softwares = softwares;
        this.licenses = licenses;
        this.devices = devices;
        this.screens = screens;
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "computer_inventory_id")
    public Long getInventoryId()
    {
        return this.inventoryId;
    }

    public void setInventoryId(Long inventoryId)
    {
        this.inventoryId = inventoryId;
    }

    @Column(name = "computer_dns_name")
    public String getDnsName()
    {
        return this.dnsName;
    }

    public void setDnsName(String dnsName)
    {
        this.dnsName = dnsName;
    }

    @Column(name = "computer_ip_adress")
    public String getIpAdress()
    {
        return this.ipAdress;
    }

    public void setIpAdress(String ipAdress)
    {
        this.ipAdress = ipAdress;
    }

    @Column(name = "computer_mac_adress")
    public String getMacAdress()
    {
        return this.macAdress;
    }

    public void setMacAdress(String macAdress)
    {
        this.macAdress = macAdress;
    }

    @Column(name = "computer_proc_frequency")
    public Integer getProcFrequency()
    {
        return this.procFrequency;
    }

    public void setProcFrequency(Integer procFrequency)
    {
        this.procFrequency = procFrequency;
    }

    @Column(name = "computer_number_procs")
    public Integer getNumberOfProcs()
    {
        return this.numberOfProcs;
    }

    public void setNumberOfCores(Integer numberOfCores)
    {
        this.numberOfCores = numberOfCores;
    }

    public void setNumberOfProcs(Integer numberOfProcs)
    {
        this.numberOfProcs = numberOfProcs;
    }

    @Column(name = "computer_number_cores")
    public Integer getNumberOfCores()
    {
        return this.numberOfCores;
    }

    @Column(name = "computer_ram_quantity")
    public Integer getRamQuantity()
    {
        return this.ramQuantity;
    }

    public void setRamQuantity(Integer ramQuantity)
    {
        this.ramQuantity = ramQuantity;
    }

    @Column(name = "computer_worker_status")
    public EWorkerStatus getWorkerStatus()
    {
        return this.workerStatus;
    }

    public void setWorkerStatus(EWorkerStatus workerStatus)
    {
        this.workerStatus = workerStatus;
    }

    @Column(name = "computer_x_position")
    public Integer getxPosition()
    {
        return this.xPosition;
    }

    public void setxPosition(Integer xPosition)
    {
        this.xPosition = xPosition;
    }

    @Column(name = "computer_y_position")
    public Integer getyPosition()
    {
        return this.yPosition;
    }

    public void setyPosition(Integer yPosition)
    {
        this.yPosition = yPosition;
    }

    @ManyToOne(targetEntity = ProcessorH.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "processor_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProcessor getProcessor()
    {
        return this.processor;
    }

    public void setProcessor(IProcessor processor)
    {
        this.processor = processor;
    }

    @ManyToOne(targetEntity = ComputerTypeH.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "computer_type_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IComputerType getComputerType()
    {
        return this.computerType;
    }

    public void setComputerType(IComputerType computerType)
    {
        this.computerType = computerType;
    }

    @ManyToOne(targetEntity = ComputerModelH.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "computer_model_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IComputerModel getComputerModel()
    {
        return this.computerModel;
    }

    public void setComputerModel(IComputerModel computerModel)
    {
        this.computerModel = computerModel;
    }

    @ManyToOne(targetEntity = RoomH.class)
    @JoinColumn(name = "room_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IRoom getRoom()
    {
        return this.room;
    }

    public void setRoom(IRoom room)
    {
        this.room = room;
    }

    @ManyToOne(targetEntity = PersonH.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getPerson()
    {
        return this.person;
    }

    public void setPerson(IPerson person)
    {
        this.person = person;
    }

    @ManyToOne(targetEntity = StudioMapH.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "studiomap_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IStudioMap getStudioMap()
    {
        return this.studioMap;
    }

    public void setStudioMap(IStudioMap studioMap)
    {
        this.studioMap = studioMap;
    }

    @ManyToMany(targetEntity = PoolH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_computer__pool", joinColumns = @JoinColumn(name = "computer_id"), inverseJoinColumns = @JoinColumn(name = "pool_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IPool> getPools()
    {
        return this.pools;
    }

    public void setPools(Set<IPool> pools)
    {
        this.pools = pools;
    }

    @ManyToMany(targetEntity = SoftwareH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_computer__software", joinColumns = @JoinColumn(name = "computer_id"), inverseJoinColumns = @JoinColumn(name = "software_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<ISoftware> getSoftwares()
    {
        if (this.softwares == null)
        {
            this.softwares = new HashSet<ISoftware>();
        }
        return this.softwares;
    }

    public void setSoftwares(Set<ISoftware> softwares)
    {
        this.softwares = softwares;
    }

    @ManyToMany(targetEntity = LicenseH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_computer__license", joinColumns = @JoinColumn(name = "computer_id"), inverseJoinColumns = @JoinColumn(name = "license_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<ILicense> getLicenses()
    {
        if (this.licenses == null)
        {
            this.licenses = new HashSet<ILicense>();
        }
        return this.licenses;
    }

    public void setLicenses(Set<ILicense> licenses)
    {
        this.licenses = licenses;
    }

    @ManyToMany(targetEntity = DeviceH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_computer__device", joinColumns = @JoinColumn(name = "computer_id"), inverseJoinColumns = @JoinColumn(name = "device_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IDevice> getDevices()
    {
        if (this.devices == null)
        {
            this.devices = new HashSet<IDevice>();
        }
        return this.devices;
    }

    public void setDevices(Set<IDevice> devices)
    {
        this.devices = devices;
    }

    @ManyToMany(targetEntity = ScreenH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_computer__screen", joinColumns = @JoinColumn(name = "computer_id"), inverseJoinColumns = @JoinColumn(name = "screen_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IScreen> getScreens()
    {
        if (this.screens == null)
        {
            this.screens = new HashSet<IScreen>();
        }
        return this.screens;
    }

    public void setScreens(Set<IScreen> screens)
    {
        this.screens = screens;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((computerModel == null) ? 0 : computerModel.hashCode());
        result = prime * result + ((computerType == null) ? 0 : computerType.hashCode());
        result = prime * result + ((dnsName == null) ? 0 : dnsName.hashCode());
        result = prime * result + ((inventoryId == null) ? 0 : inventoryId.hashCode());
        result = prime * result + ((ipAdress == null) ? 0 : ipAdress.hashCode());
        result = prime * result + ((macAdress == null) ? 0 : macAdress.hashCode());
        result = prime * result + ((numberOfCores == null) ? 0 : numberOfCores.hashCode());
        result = prime * result + ((numberOfProcs == null) ? 0 : numberOfProcs.hashCode());
        result = prime * result + ((procFrequency == null) ? 0 : procFrequency.hashCode());
        result = prime * result + ((processor == null) ? 0 : processor.hashCode());
        result = prime * result + ((ramQuantity == null) ? 0 : ramQuantity.hashCode());
        result = prime * result + ((room == null) ? 0 : room.hashCode());
        result = prime * result + ((studioMap == null) ? 0 : studioMap.hashCode());
        result = prime * result + ((workerStatus == null) ? 0 : workerStatus.hashCode());
        result = prime * result + ((xPosition == null) ? 0 : xPosition.hashCode());
        result = prime * result + ((yPosition == null) ? 0 : yPosition.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ComputerH other = (ComputerH) obj;
        if (computerModel == null)
        {
            if (other.computerModel != null)
                return false;
        }
        else if (!computerModel.equals(other.computerModel))
            return false;
        if (computerType == null)
        {
            if (other.computerType != null)
                return false;
        }
        else if (!computerType.equals(other.computerType))
            return false;
        if (dnsName == null)
        {
            if (other.dnsName != null)
                return false;
        }
        else if (!dnsName.equals(other.dnsName))
            return false;
        if (inventoryId == null)
        {
            if (other.inventoryId != null)
                return false;
        }
        else if (!inventoryId.equals(other.inventoryId))
            return false;
        if (ipAdress == null)
        {
            if (other.ipAdress != null)
                return false;
        }
        else if (!ipAdress.equals(other.ipAdress))
            return false;
        if (macAdress == null)
        {
            if (other.macAdress != null)
                return false;
        }
        else if (!macAdress.equals(other.macAdress))
            return false;
        if (numberOfCores == null)
        {
            if (other.numberOfCores != null)
                return false;
        }
        else if (!numberOfCores.equals(other.numberOfCores))
            return false;
        if (numberOfProcs == null)
        {
            if (other.numberOfProcs != null)
                return false;
        }
        else if (!numberOfProcs.equals(other.numberOfProcs))
            return false;
        if (procFrequency == null)
        {
            if (other.procFrequency != null)
                return false;
        }
        else if (!procFrequency.equals(other.procFrequency))
            return false;
        if (processor == null)
        {
            if (other.processor != null)
                return false;
        }
        else if (!processor.equals(other.processor))
            return false;
        if (ramQuantity == null)
        {
            if (other.ramQuantity != null)
                return false;
        }
        else if (!ramQuantity.equals(other.ramQuantity))
            return false;
        if (room == null)
        {
            if (other.room != null)
                return false;
        }
        else if (!room.equals(other.room))
            return false;
        if (studioMap == null)
        {
            if (other.studioMap != null)
                return false;
        }
        else if (!studioMap.equals(other.studioMap))
            return false;
        if (workerStatus == null)
        {
            if (other.workerStatus != null)
                return false;
        }
        else if (!workerStatus.equals(other.workerStatus))
            return false;
        if (xPosition == null)
        {
            if (other.xPosition != null)
                return false;
        }
        else if (!xPosition.equals(other.xPosition))
            return false;
        if (yPosition == null)
        {
            if (other.yPosition != null)
                return false;
        }
        else if (!yPosition.equals(other.yPosition))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        for (IDevice d : getDevices())
        {
            d.getComputers().remove(this);
        }
        for (ILicense l : getLicenses())
        {
            l.getComputers().remove(this);
        }
        for (IPool p : getPools())
        {
            p.getComputers().remove(this);
        }
        for (IScreen s : getScreens())
        {
            s.getComputers().remove(this);
        }
        for (ISoftware s : getSoftwares())
        {
            s.getComputers().remove(this);
        }
    }

    public IBaseTranslator<ILComputer, IComputer> defaultTranslator()
    {
        return Translators.computer;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    public static class ComputerComplexTypeProvider extends InventoryItemComplexTypeProvider
    {
        public List<String> getValidEnum(String field)
        {
            return super.getValidEnum(field);
        }

        public Object getValue(String field, String value)
        {
            Object ret = null;

            if (WORKER_STATUS_FIELD.equals(field))
            {
                ret = EnumUtils.getEnumValueIgnoreCase(EWorkerStatus.class, value);
            }
            else if (super.isDateField(field))
            {
                ret = fr.hd3d.utils.StringUtils.parseDate(value);
            }

            return ret;
        }

        public boolean isValidTypeField(String field)
        {
            return super.isValidTypeField(field) || WORKER_STATUS_FIELD.equals(field);
        }
    }
}
