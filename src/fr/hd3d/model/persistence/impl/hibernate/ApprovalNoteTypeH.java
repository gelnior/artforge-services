package fr.hd3d.model.persistence.impl.hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNoteType;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


@Entity
@Table(name = "hd3d_approvalnotetype")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ApprovalNoteTypeH extends BaseH implements IApprovalNoteType
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String name;
    private ITaskType taskType;
    private IProject project;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_approvalnotetype"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ApprovalNoteTypeH()
    {}

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "approvalnotetype_name")
    public String getName()
    {
        return name;
    }

    // @OneToOne(targetEntity = TaskTypeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch =
    // FetchType.LAZY)
    // @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @ManyToOne(targetEntity = TaskTypeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @JoinColumn(name = "approvalnotetype_tasktype")
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public ITaskType getTaskType()
    {
        return taskType;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setTaskType(ITaskType taskType)
    {
        this.taskType = taskType;
    }

    @ManyToOne(targetEntity = ProjectH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "approvalnotetype_project_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IProject getProject()
    {
        return project;
    }

    public void setProject(IProject project)
    {
        this.project = project;

    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((taskType == null) ? 0 : taskType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ApprovalNoteTypeH other = (ApprovalNoteTypeH) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (taskType == null)
        {
            if (other.taskType != null)
                return false;
        }
        else if (!taskType.equals(other.taskType))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    // @Override
    // public void postDisable() throws Hd3dException
    // {
    // super.postDisable();
    //
    // List<IApprovalNote> boundApprovalNotes = Persistors.approvalnote.getByValue("approvalNoteType", this);
    //
    // for (IApprovalNote approvalNote : boundApprovalNotes)
    // {
    // approvalNote.setApprovalNoteType(null);
    // }
    // }
    @Transient
    @Override
    public boolean isApprovable()
    {
        return false;
    }

    @Transient
    @Override
    public boolean isTaskable()
    {
        return false;
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILApprovalNoteType, IApprovalNoteType> defaultTranslator()
    {
        return Translators.approvalnotetype;
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("name", name);
    // b.append("taskTypeId", taskType == null ? "null" : taskType.getId());
    // b.append("projectId", project == null ? "null" : project.getId());
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
    // no complextype provider
}
