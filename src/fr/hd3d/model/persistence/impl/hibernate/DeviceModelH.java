package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.model.lightweight.ILDeviceModel;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.IDeviceModel;
import fr.hd3d.model.persistence.IManufacturer;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * Device model hibernate object needed for database persistance.
 * 
 * @author HD3D
 */
@Entity
@Table(name = "hd3d_device_model")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class DeviceModelH extends SimpleH implements IDeviceModel
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Set<IDevice> devices;
    private IManufacturer manufacturer;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_devicemodel"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public DeviceModelH()
    {}

    public DeviceModelH(Long id, Timestamp version, String name, IManufacturer manufacturer)
    {
        super(id, version, name);

        this.manufacturer = manufacturer;
    }

    public DeviceModelH(String name, IManufacturer manufacturer)
    {
        this(null, null, name, manufacturer);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @OneToMany(targetEntity = DeviceH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "deviceModel")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IDevice> getDevices()
    {
        if (this.devices == null)
        {
            this.devices = new HashSet<IDevice>();
        }
        return this.devices;
    }

    public void setDevices(Set<IDevice> devices)
    {
        this.devices = devices;
    }

    @ManyToOne(targetEntity = ManufacturerH.class)
    @JoinColumn(name = "manufacturer_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IManufacturer getManufacturer()
    {
        return manufacturer;
    }

    public void setManufacturer(IManufacturer manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    /*------------------
     * equal & hashcode
     ------------------*/

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    public IBaseTranslator<ILDeviceModel, IDeviceModel> defaultTranslator()
    {
        return Translators.deviceModel;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("deviceIds", StringUtils.join(CollectUtils.getIds(getDevices()), ','));
    // b.append("manufacturerId", manufacturer == null ? "null" : manufacturer.getId());
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
