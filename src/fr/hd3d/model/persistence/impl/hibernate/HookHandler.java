package fr.hd3d.model.persistence.impl.hibernate;

import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.utils.StringUtils;


public final class HookHandler
{
    public static final String SEPARATOR = ".";

    private HookHandler()
    {}

    public static String getHook(IConstituent constituent)
    {
        String ret = null;
        if (constituent != null)
        {
            ret = constituent.getPath(SEPARATOR, true) + SEPARATOR + constituent.getLabel();
        }
        return truncate(ret);
    }

    public static String getHook(IItemGroup itemGroup)
    {
        String ret = null;
        if (itemGroup != null)
        {
            String projectName = null;
            String sheetName = null;
            ISheet sheet = itemGroup.getSheet();
            if (sheet != null)
            {
                sheetName = StringUtils.nullSafeString(sheet.getName());
                IProject project = sheet.getProject();
                if (project != null)
                {
                    projectName = StringUtils.nullSafeString(project.getName());
                }
            }
            ret = projectName + SEPARATOR + sheetName + SEPARATOR + itemGroup.getName();
        }
        return truncate(ret);
    }

    public static String getHook(IPlayListRevision playListRevision)
    {
        String ret = null;
        if (playListRevision != null)
        {
            String projectName = null;
            String name = StringUtils.nullSafeString(playListRevision.getName());
            String revision = StringUtils.nullSafeString(String.valueOf(playListRevision.getRevision()));
            if (playListRevision.getProject() != null)
            {
                projectName = StringUtils.nullSafeString(playListRevision.getProject().getName());
            }

            ret = projectName + SEPARATOR + name + SEPARATOR + revision;
        }
        return truncate(ret);
    }

    public static String getHook(IProject project)
    {
        String ret = null;
        if (project != null)
        {
            ret = project.getName();
        }
        return truncate(ret);
    }

    public static String getHook(ISequence sequence)
    {
        String ret = null;
        if (sequence != null)
        {
            ret = sequence.path(SEPARATOR, true) + SEPARATOR + sequence.getName();
        }
        return truncate(ret);
    }

    public static String getHook(IShot shot)
    {
        String ret = null;
        if (shot != null)
        {
            ret = shot.path(SEPARATOR, true) + SEPARATOR + shot.getLabel();
        }
        return truncate(ret);
    }

    /* hook length is 255 characters, if over return a UUID */
    public static String truncate(String hook)
    {
        return hook == null ? null : (hook.length() >= 255 ? java.util.UUID.randomUUID().toString() : hook);
    }

}
