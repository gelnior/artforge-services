package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILDynMetaDataValue;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Table(name = "hd3d_dynmetadatavalue")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class DynMetaDataValueH extends BaseH implements IDynMetaDataValue
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private IClassDynMetaDataType classDynMetaDataType;

    private Long parent;

    private String parentType;

    private String value;// always stored as String, in case of List, values are stored as String with separator, even
    // if not String

    private Object objectValue;

    private Date dateValue;// stored when appropriate

    private Long longValue;// stored when appropriate

    private Boolean booleanValue;// stored when appropriate

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_dynmetadatavalue"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public DynMetaDataValueH()
    {}

    public DynMetaDataValueH(Long id, Timestamp version, IClassDynMetaDataType classDynMetaDataType, Long parent,
            String parentType, String value) throws Hd3dPersistenceException
    {
        super(id, version);
        if (classDynMetaDataType == null || classDynMetaDataType.getDynMetaDataType() == null)
            throw new Hd3dPersistenceException("DynMetaDataValue's type must not be null");
        this.classDynMetaDataType = classDynMetaDataType;
        this.parent = parent;
        this.parentType = parentType;
        setValue(value);
    }

    public DynMetaDataValueH(IClassDynMetaDataType classDynMetaDataType, Long parent, String parentType, String value)
            throws Hd3dPersistenceException
    {
        this(null, null, classDynMetaDataType, parent, parentType, value);
    }

    public DynMetaDataValueH(IClassDynMetaDataType classDynMetaDataType, Long parent, String parentType)
            throws Hd3dPersistenceException
    {
        this(null, null, classDynMetaDataType, parent, parentType, classDynMetaDataType.getDynMetaDataType()
                .getDefaultValue());
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToOne(targetEntity = ClassDynMetaDataTypeH.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "dynmetadatavalue_classdynmetadatatype")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public IClassDynMetaDataType getClassDynMetaDataType()
    {
        return classDynMetaDataType;
    }

    @Column(name = "dynmetadatavalue_parent")
    // @NotEmpty //WARNING: do not use @NotEmpty for Long ! Will be considered as a String
    public Long getParent()
    {
        return parent;
    }

    @Column(name = "dynmetadatavalue_parenttype")
    @NotEmpty
    public String getParentType()
    {
        return parentType;
    }

    @Column(name = "dynmetadatavalue_value")
    public String getValue()
    {
        return value;
    }

    @Transient
    public Object getObjectValue()
    {
        return objectValue;
    }

    @Column(name = "dynmetadatavalue_datevalue")
    public Date getDateValue()
    {
        return dateValue;
    }

    @Column(name = "dynmetadatavalue_longvalue")
    public Long getLongValue()
    {
        return longValue;
    }

    @Column(name = "dynmetadatavalue_booleanvalue")
    public Boolean getBooleanValue()
    {
        return booleanValue;
    }

    public void setObjectValue(IBase objectValue)
    {
        this.objectValue = objectValue;
    }

    public void setDateValue(Date dateValue)
    {
        this.dateValue = dateValue;
    }

    public void setLongValue(Long longValue)
    {
        this.longValue = longValue;
    }

    public void setBooleanValue(Boolean booleanValue)
    {
        this.booleanValue = booleanValue;
    }

    public void setClassDynMetaDataType(IClassDynMetaDataType classDynMetaDataType)
    {
        this.classDynMetaDataType = classDynMetaDataType;
    }

    public void setParent(Long parent)
    {
        this.parent = parent;
    }

    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    public void setValue(String value)
    {
        this.value = value;

        // TODO ugly, to refactor
        new DynMetaDataTypeConverter(this).setValue(value);
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((classDynMetaDataType == null) ? 0 : classDynMetaDataType.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        DynMetaDataValueH other = (DynMetaDataValueH) obj;
        if (classDynMetaDataType == null)
        {
            if (other.classDynMetaDataType != null)
                return false;
        }
        else if (!classDynMetaDataType.equals(other.classDynMetaDataType))
            return false;
        if (parent == null)
        {
            if (other.parent != null)
                return false;
        }
        else if (!parent.equals(other.parent))
            return false;
        if (value == null)
        {
            if (other.value != null)
                return false;
        }
        else if (!value.equals(other.value))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Transient
    public String getDynMetaDataType_Type()
    {
        if (getClassDynMetaDataType() != null && getClassDynMetaDataType().getDynMetaDataType() != null)
            return getClassDynMetaDataType().getDynMetaDataType().getType();
        else
            return null;
    }

    @Transient
    public boolean isEntity()
    {
        return EntitiesMaps.isEntity(getDynMetaDataType_Type());
    }

    public IBaseTranslator<ILDynMetaDataValue, IDynMetaDataValue> defaultTranslator()
    {
        return Translators.dynmetadatavalue;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return getValue();
    }
    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("classDynMetaDataTypeId", classDynMetaDataType == null ? "null" : classDynMetaDataType.getId());
    // b.append("parent", parent).append("parentType", parentType).append("value", value).append("objectValue",
    // objectValue).append("dateValue", dateValue).append("longValue", longValue).append("booleanValue",
    // booleanValue);
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
