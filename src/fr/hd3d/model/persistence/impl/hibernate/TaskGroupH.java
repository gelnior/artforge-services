package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTaskGroup;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


/**
 * @author michael.guiral
 * 
 */
@Entity
@Table(name = "hd3d_task_group")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class TaskGroupH extends TaskBaseH implements ITaskGroup
{
    private static final long serialVersionUID = 1L;

    private Integer index;// display order
    private String color;
    private String name;
    private Collection<ITaskGroup> children;
    private Set<IExtraLine> extraLines;
    private IPlanning planning;
    private String filter;
    private ITaskType taskType;

    /*-------------
     * Constructors
     -------------*/
    public TaskGroupH()
    {}

    /**
     * @param id
     * @param version
     * @param color
     * @param endDate
     * @param startDate
     * @param name
     * @param parentId
     * @param planning
     */
    public TaskGroupH(Long id, Timestamp version, String color, Date endDate, Date startDate, String name,
            ITaskGroup parent, IPlanning planning, Long duration, String filter)
    {
        super(id, version, startDate, endDate, duration, parent);
        this.color = color;
        this.name = name;
        this.planning = planning;
        if (planning != null)
            planning.addTaskGroup(this);
        this.filter = filter;
    }

    /**
     * @param taskGroup
     */
    public TaskGroupH(ITaskGroup taskGroup)
    {
        super(taskGroup.getId(), taskGroup.getVersion(), taskGroup.getStartDate(), taskGroup.getEndDate(), taskGroup
                .getDuration(), taskGroup.getTaskGroup());
        this.name = taskGroup.getName();
        this.color = taskGroup.getColor();
        this.planning = taskGroup.getPlanning();
        this.filter = taskGroup.getFilter();
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "taskgroup_index")
    public Integer getIndex()
    {
        return index;
    }

    public void setIndex(Integer index)
    {
        if (index == null)
            this.index = NumberUtils.INTEGER_ZERO;
        else
            this.index = index;
    }

    @Column(name = "task_group_color")
    public String getColor()
    {
        return this.color;
    }

    @Column(name = "task_group_name")
    public String getName()
    {
        return this.name;
    }

    /*
     * Note: though the children are TaskGroups, the targetEntity must reference TaskBaseH because mappedBy references a
     * property in TaskBaseH
     */
    @OneToMany(targetEntity = TaskBaseH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "taskGroup")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Collection<ITaskGroup> getChildren()
    {
        if (children == null)
            children = new ArrayList<ITaskGroup>();
        return children;
    }

    public void setChildren(Collection<ITaskGroup> children)
    {
        this.children = children;
    }

    @OneToMany(targetEntity = ExtraLineH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "taskGroup")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IExtraLine> getExtraLines()
    {
        if (extraLines == null)
        {
            extraLines = new HashSet<IExtraLine>();
        }
        return extraLines;
    }

    public void setExtraLines(Set<IExtraLine> extraLines)
    {
        this.extraLines = extraLines;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @ManyToOne(targetEntity = PlanningH.class)
    @JoinColumn(name = "task_group_planning_id")
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IPlanning getPlanning()
    {
        return planning;
    }

    public void setPlanning(IPlanning planning)
    {
        this.planning = planning;
    }

    @ManyToOne(targetEntity = TaskTypeH.class)
    @JoinColumn(name = "task_group_task_type_id")
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public ITaskType getTaskType()
    {
        return this.taskType;
    }

    public void setTaskType(ITaskType taskType)
    {
        this.taskType = taskType;
    }

    @Column(name = "task_group_filter")
    public String getFilter()
    {
        return this.filter;
    }

    public void setFilter(String filter)
    {
        this.filter = filter;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((children == null) ? 0 : children.hashCode());
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((planning == null) ? 0 : planning.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        TaskGroupH other = (TaskGroupH) obj;
        if (children == null)
        {
            if (other.children != null)
                return false;
        }
        else if (!children.equals(other.children))
            return false;
        if (color == null)
        {
            if (other.color != null)
                return false;
        }
        else if (!color.equals(other.color))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (planning == null)
        {
            if (other.planning != null)
                return false;
        }
        else if (!planning.equals(other.planning))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    public Collection<ITaskGroup> allChildren(boolean includeSelf)
    {
        List<ITaskGroup> allChildren = new ArrayList<ITaskGroup>();
        if (includeSelf)
            allChildren.add(this);
        for (ITaskGroup taskGroup : getChildren())
            allChildren.addAll(taskGroup.allChildren(true));

        return allChildren;
    }

    public IBaseTranslator<ILTaskGroup, ITaskGroup> defaultTranslator()
    {
        return Translators.taskGroup;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        // cascade disable children
        Collection<ITaskGroup> children = CollectUtils.selectNotNull(getChildren());
        Persistors.taskGroup.disableCollection(children);
    }

    @Transient
    public IPerson getWorker()
    {
        throw new UnsupportedOperationException("A Workgroup dosen't have a worker!");
    }

    @Transient
    public void setWorker(IPerson worker)
    {
        throw new UnsupportedOperationException("A Workgroup dosen't have a worker!");

    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }
    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("index", index).append("color", color).append("name", name);
    // b.append("childrenIds", StringUtils.join(CollectUtils.getIds(getChildren()), ','));
    // b.append("extraLineIds", StringUtils.join(CollectUtils.getIds(getExtraLines()), ','));
    // b.append("planningId", planning == null ? "null" : planning.getId());
    // b.append("filter", filter);
    // b.append("taskTypeId", taskType == null ? "null" : taskType.getId());
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
