/**
 * 
 */
package fr.hd3d.model.persistence.impl.hibernate;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.Session;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILExtraLine;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.ITaskBase;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


/**
 * In the planning application, one person is bound to 1 "bar/line" standing for the total of his tasks. But it is
 * possible to add some more "extra line/bars" below to insert some more tasks. Though this entity is in the model, it
 * is actually a graphic representation helper.
 * 
 * @author thomas-eskenazi
 */
@Entity
@Table(name = "hd3d_extra_line")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ExtraLineH extends BaseH implements IExtraLine
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String name;
    private Set<ITaskBase> taskBases;
    private ITaskGroup taskGroup;
    private IPerson hiddenPerson;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_extraline"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToOne(targetEntity = PersonH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "extra_line__hidden_person_id")
    // NOTE: do not filter on internalStatus here, because should a person be disabled, related files
    // must be visible anyway
    // @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    // END NOTE
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IPerson getHiddenPerson()
    {
        return hiddenPerson;
    }

    public void setHiddenPerson(IPerson hiddenPerson)
    {
        this.hiddenPerson = hiddenPerson;
    }

    /**
     * @return the name
     */
    @Column(name = "extra_line_name", nullable = false)
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the taskBase
     */
    @OneToMany(targetEntity = TaskBaseH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "extraLine")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<ITaskBase> getTaskBases()
    {
        if (taskBases == null)
        {
            taskBases = new HashSet<ITaskBase>();
        }
        return taskBases;
    }

    public void setTaskBases(Set<ITaskBase> taskBases)
    {
        this.taskBases = taskBases;
    }

    /**
     * @return the taskGroup
     */
    @ManyToOne(targetEntity = TaskGroupH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "extra_line__task_group_id")
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public ITaskGroup getTaskGroup()
    {
        return taskGroup;
    }

    /**
     * @param taskGroup
     *            the taskGroup to set
     */
    public void setTaskGroup(ITaskGroup taskGroup)
    {
        this.taskGroup = taskGroup;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((hiddenPerson == null) ? 0 : hiddenPerson.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((taskBases == null) ? 0 : taskBases.hashCode());
        result = prime * result + ((taskGroup == null) ? 0 : taskGroup.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ExtraLineH other = (ExtraLineH) obj;
        if (hiddenPerson == null)
        {
            if (other.hiddenPerson != null)
                return false;
        }
        else if (!hiddenPerson.equals(other.hiddenPerson))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (taskBases == null)
        {
            if (other.taskBases != null)
                return false;
        }
        else if (!taskBases.equals(other.taskBases))
            return false;
        if (taskGroup == null)
        {
            if (other.taskGroup != null)
                return false;
        }
        else if (!taskGroup.equals(other.taskGroup))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILExtraLine, IExtraLine> defaultTranslator()
    {
        return Translators.extraLine;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return name;
    }

    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("name", name);
    // b.append("taskBaseIds", StringUtils.join(CollectUtils.getIds(getTaskBases()), ','));
    // b.append("taskGroupId", taskGroup == null ? "null" : taskGroup.getId());
    // b.append("hiddenPersonId", hiddenPerson == null ? "null" : hiddenPerson.getId());
    //
    // return b.toString();
    // }

    @Override
    public void postDisable() throws Hd3dException
    {
        super.postDisable();

        Collection<ITaskBase> taskBases = CollectUtils.selectNotNull(getTaskBases());
        if (CollectUtils.isNotEmpty(taskBases))
        {
            Session session = HibernateUtil.currentSession();

            for (ITaskBase taskBase : taskBases)
            {
                taskBase.setExtraLine(null);
                taskBase.setWorker(null);
                session.update(taskBase);
                session.flush();
            }
            getTaskBases().clear();
            session.update(this);
            session.flush();
        }
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
