package fr.hd3d.model.persistence.impl.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Entity
@Indexed
@Table(name = "hd3d_constituent")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ConstituentH extends BaseH implements IConstituent
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private ICategory category;
    private Integer completion;
    private String description;
    private Integer difficulty;
    private String hook;
    private String label;
    private Integer trust;
    private Boolean design;
    private Boolean modeling;
    private Boolean setup;
    private Boolean texturing;
    private Boolean shading;
    private Boolean hairs;
    private Collection<IComposition> compositions;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_constituent"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ConstituentH()
    {}

    public ConstituentH(Long id, java.sql.Timestamp version, ICategory category, Integer completion,
            String description, Integer difficulty, String hook, String label, Integer trust, Boolean design,
            Boolean modeling, Boolean setup, Boolean texturing, Boolean shading, Boolean hairs)
    {
        super(id, version);
        this.category = category;
        this.completion = (completion == null) ? 0 : completion;
        this.description = description;
        this.difficulty = (difficulty == null) ? 0 : difficulty;
        this.hook = hook;
        this.label = label;
        this.trust = (trust == null) ? 0 : trust;
        this.design = (design == null) ? Boolean.FALSE : design;
        this.modeling = (modeling == null) ? Boolean.FALSE : modeling;
        this.setup = (setup == null) ? Boolean.FALSE : setup;
        this.texturing = (texturing == null) ? Boolean.FALSE : texturing;
        this.shading = (shading == null) ? Boolean.FALSE : shading;
        this.hairs = (hairs == null) ? Boolean.FALSE : hairs;
    }

    public ConstituentH(ICategory category, Integer completion, String description, Integer difficulty, String hook,
            String label, Integer trust, Boolean design, Boolean modeling, Boolean setup, Boolean texturing,
            Boolean shading, Boolean hairs)
    {
        this(null, null, category, completion, description, difficulty, hook, label, trust, design, modeling, setup,
                texturing, shading, hairs);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToOne(targetEntity = CategoryH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "constituent_category_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public ICategory getCategory()
    {
        return category;
    }

    @Column(name = "constituent_completion")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    @Min(value = 0)
    public Integer getCompletion()
    {
        return completion;
    }

    @Column(name = "constituent_description")
    @Field(index = Index.TOKENIZED, store = Store.YES)
    public String getDescription()
    {
        return description;
    }

    @Column(name = "constituent_difficulty")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    @Min(value = 0)
    public Integer getDifficulty()
    {
        return difficulty;
    }

    @Column(name = "constituent_hook")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    public String getHook()
    {
        return hook;
    }

    @Column(name = "constituent_label")
    @org.hibernate.annotations.Index(name = "constituent_label_idx")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    @NotEmpty
    public String getLabel()
    {
        return label;
    }

    @Column(name = "constituent_trust")
    @Field(index = Index.UN_TOKENIZED, store = Store.YES)
    @Min(value = 0)
    public Integer getTrust()
    {
        return trust;
    }

    @Column(name = "constituent_design")
    public Boolean getDesign()
    {
        return design;
    }

    @Column(name = "constituent_modeling")
    public Boolean getModeling()
    {
        return modeling;
    }

    @Column(name = "constituent_setup")
    public Boolean getSetup()
    {
        return setup;
    }

    @Column(name = "constituent_texturing")
    public Boolean getTexturing()
    {
        return texturing;
    }

    @Column(name = "constituent_shading")
    public Boolean getShading()
    {
        return shading;
    }

    @Column(name = "constituent_hairs")
    public Boolean getHairs()
    {
        return hairs;
    }

    @OneToMany(targetEntity = CompositionH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "constituent")
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Collection<IComposition> getCompositions()
    {
        if (compositions == null)
        {
            compositions = new ArrayList<IComposition>();
        }
        return compositions;
    }

    public void setCategory(ICategory category)
    {
        this.category = category;
    }

    public void setCompletion(Integer completion)
    {
        this.completion = completion;
    }

    public void setDescription(String desc)
    {
        if (desc != null)
        {
            this.description = desc;
        }
    }

    public void setDifficulty(Integer difficulty)
    {
        this.difficulty = difficulty;
    }

    public void setHook(String hook)
    {
        if (hook != null)
        {
            this.hook = hook;
        }
    }

    public void setLabel(String label)
    {
        if (label != null)
        {
            this.label = label;
        }
    }

    public void setTrust(Integer trust)
    {
        this.trust = trust;
    }

    public void setDesign(Boolean design)
    {
        this.design = design;
    }

    public void setModeling(Boolean modeling)
    {
        this.modeling = modeling;
    }

    public void setSetup(Boolean setup)
    {
        this.setup = setup;
    }

    public void setTexturing(Boolean texturing)
    {
        this.texturing = texturing;
    }

    public void setShading(Boolean shading)
    {
        this.shading = shading;
    }

    public void setHairs(Boolean hairs)
    {
        this.hairs = hairs;
    }

    public void setCompositions(Collection<IComposition> compositions)
    {
        this.compositions = compositions;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((category == null) ? 0 : category.hashCode());
        result = prime * result + ((completion == null) ? 0 : completion.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((design == null) ? 0 : design.hashCode());
        result = prime * result + ((difficulty == null) ? 0 : difficulty.hashCode());
        result = prime * result + ((hairs == null) ? 0 : hairs.hashCode());
        result = prime * result + ((hook == null) ? 0 : hook.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + ((modeling == null) ? 0 : modeling.hashCode());
        result = prime * result + ((setup == null) ? 0 : setup.hashCode());
        result = prime * result + ((shading == null) ? 0 : shading.hashCode());
        result = prime * result + ((texturing == null) ? 0 : texturing.hashCode());
        result = prime * result + ((trust == null) ? 0 : trust.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ConstituentH other = (ConstituentH) obj;
        if (category == null)
        {
            if (other.category != null)
                return false;
        }
        else if (!category.equals(other.category))
            return false;
        if (completion == null)
        {
            if (other.completion != null)
                return false;
        }
        else if (!completion.equals(other.completion))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (design == null)
        {
            if (other.design != null)
                return false;
        }
        else if (!design.equals(other.design))
            return false;
        if (difficulty == null)
        {
            if (other.difficulty != null)
                return false;
        }
        else if (!difficulty.equals(other.difficulty))
            return false;
        if (hairs == null)
        {
            if (other.hairs != null)
                return false;
        }
        else if (!hairs.equals(other.hairs))
            return false;
        if (hook == null)
        {
            if (other.hook != null)
                return false;
        }
        else if (!hook.equals(other.hook))
            return false;
        if (label == null)
        {
            if (other.label != null)
                return false;
        }
        else if (!label.equals(other.label))
            return false;
        if (modeling == null)
        {
            if (other.modeling != null)
                return false;
        }
        else if (!modeling.equals(other.modeling))
            return false;
        if (setup == null)
        {
            if (other.setup != null)
                return false;
        }
        else if (!setup.equals(other.setup))
            return false;
        if (shading == null)
        {
            if (other.shading != null)
                return false;
        }
        else if (!shading.equals(other.shading))
            return false;
        if (texturing == null)
        {
            if (other.texturing != null)
                return false;
        }
        else if (!texturing.equals(other.texturing))
            return false;
        if (trust == null)
        {
            if (other.trust != null)
                return false;
        }
        else if (!trust.equals(other.trust))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    public void completePrePersistOrUpdate()
    {
        if (StringUtils.isBlank(getHook()))
        {
            setHook(HookHandler.getHook(this));
        }
    }

    @Transient
    public boolean isTaskable()
    {
        return true;
    }

    @Transient
    public boolean isApprovable()
    {
        return true;
    }

    public void addComposition(IComposition compo)
    {
        if (compo != null)
        {
            getCompositions().add(compo);
            compo.setConstituent(this);
        }
    }

    @Transient
    @Field(name = "categories_id", index = Index.TOKENIZED, store = Store.YES)
    public String getCategoriesIdString()
    {
        StringBuilder buf = new StringBuilder(100);
        if (getCategory() != null)
        {
            buf.append(getCategory().getId()).append(' ').append(getCategory().getParentIds());
        }
        buf.trimToSize();
        return buf.toString();
    }

    @Transient
    @Field(name = "path", index = Index.UN_TOKENIZED, store = Store.YES)
    public String getPath()
    {
        return getPath(" > ", false);
    }

    public String getPath(String separator, boolean includeProject)
    {
        StringBuilder buf = new StringBuilder(100);
        if (getCategory() != null)
        {
            ICategory[] parentsArray = parents();
            if (includeProject && !ArrayUtils.isEmpty(parentsArray))
            {
                IProject project = parentsArray[0].getProject();
                if (project != null)
                {
                    buf.append(project.getName());
                }
            }
            /* build the string */
            for (int i = 0; i < parentsArray.length; i++)
            {
                final ICategory cat = parentsArray[i];
                if (!cat.isRoot())
                {
                    buf.append(cat.getName()).append(separator);
                }
            }
        }
        buf.trimToSize();
        return buf.toString();
    }

    public ICategory[] parents()
    {
        /* retrieve the parents in proper order */
        List<ICategory> parentsList = new ArrayList<ICategory>();
        parentsList.add(getCategory());
        parentsList.addAll(getCategory().parents());
        ICategory[] parentsArray = (ICategory[]) parentsList.toArray(new ICategory[parentsList.size()]);
        CollectionUtils.reverseArray(parentsArray);
        return parentsArray;
    }

    public IBaseTranslator<ILConstituent, IConstituent> defaultTranslator()
    {
        return Translators.constituent;
    }

    public String thumbnail()
    {
        return this.getHook();
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public void postDisable() throws Hd3dException
    {
        super.postDisable();
        /* cascade disable */
        Collection<IComposition> compositions = CollectUtils.selectNotNull(getCompositions());
        Persistors.composition.disableCollection(compositions);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return label;
    }
    // public String toString()
    // {
    // ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
    // b.append("categoryId", category == null ? "null" : category.getId());
    // b.append("completion", completion).append("description", description).append("difficulty", difficulty).append(
    // "hook", hook).append("label", label).append("trust", trust).append("design", design).append("modeling",
    // modeling).append("setup", setup).append("texturing", texturing).append("shading", shading).append(
    // "hairs", hairs);
    //
    // b.append("compositionIds", StringUtils.join(CollectUtils.getIds(getCompositions()), ','));
    //
    // return b.toString();
    // }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
