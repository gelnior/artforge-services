package fr.hd3d.model.persistence.impl.hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILProxy;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


@Entity
@Table(name = "hd3d_proxy")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ProxyH extends BaseH implements IProxy
{

    private static final long serialVersionUID = 1L;

    private IProxyType proxyType;
    private String location;
    private String mountPoint;
    private String description;
    private IFileRevision fileRevision;
    private String filename;
    private Integer framerate = 25;

    public ProxyH()
    {}

    public ProxyH(IFileRevision fileRevision, IProxyType proxyType, String mountPoint, final String location,
            final String filename, final String description, Integer framerate)
    {
        super();
        this.description = description;
        this.proxyType = proxyType;
        this.fileRevision = fileRevision;
        this.location = location;
        this.mountPoint = mountPoint;
        this.filename = filename;
        if (framerate != null)
            this.framerate = framerate;
    }

    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_proxy"), @Parameter(name = "initial_value", value = "1"),
            @Parameter(name = "increment_size", value = "1"), @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    @ManyToOne(targetEntity = FileRevisionH.class)
    @JoinColumn(name = "proxy_filerevision_id")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public IFileRevision getFileRevision()
    {
        return fileRevision;
    }

    public void setFileRevision(IFileRevision fileRevision)
    {
        this.fileRevision = fileRevision;
    }

    @ManyToOne(targetEntity = ProxyTypeH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "proxy_proxytype")
    @BatchSize(size = 50)
    @NotFound(action = NotFoundAction.IGNORE)
    public IProxyType getProxyType()
    {
        return proxyType;
    }

    public void setProxyType(IProxyType proxyType)
    {
        this.proxyType = proxyType;
    }

    @Column(name = "proxy_location")
    public String getLocation()
    {
        return location;
    }

    @Column(name = "proxy_filename")
    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    @Column(name = "proxy_description")
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Column(name = "proxy_mountpoint")
    public String getMountPoint()
    {
        return mountPoint;
    }

    public void setMountPoint(String mountPoint)
    {
        this.mountPoint = mountPoint;
    }

    @Column(name = "proxy_framerate")
    public Integer getFramerate()
    {
        return framerate;
    }

    public void setFramerate(Integer framerate)
    {
        this.framerate = framerate;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();

        result = prime * result + ((fileRevision == null) ? 0 : fileRevision.hashCode());
        result = prime * result + ((proxyType == null) ? 0 : proxyType.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((mountPoint == null) ? 0 : mountPoint.hashCode());
        result = prime * result + ((filename == null) ? 0 : filename.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!super.equals(obj))
            return false;

        if (getClass() != obj.getClass())
            return false;
        ProxyH other = (ProxyH) obj;

        if (fileRevision == null)
        {
            if (other.fileRevision != null)
                return false;
        }
        else if (!fileRevision.equals(other.fileRevision))
            return false;
        if (proxyType == null)
        {
            if (other.proxyType != null)
                return false;
        }
        else if (!proxyType.equals(other.proxyType))
            return false;
        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;

        if (mountPoint == null)
        {
            if (other.mountPoint != null)
                return false;
        }
        else if (!mountPoint.equals(other.mountPoint))
            return false;

        if (filename == null)
        {
            if (other.filename != null)
                return false;
        }
        else if (!filename.equals(other.filename))
            return false;
        return true;
    }

    public String toString()
    {
        return new ToStringBuilder(this).append("id", id).append("version", version).append("fileRevision",
                fileRevision).append("proxyType", proxyType).append("location", location).append("filename", filename)
                .append("description", description).append("mount point", mountPoint).append("framerate", framerate)
                .toString();
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILProxy, IProxy> defaultTranslator()
    {
        return Translators.proxy;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

}
