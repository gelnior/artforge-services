package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.CollectionUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILClassDynMetaDataType;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.impl.hibernate.visitor.persist.ClassDynMetaDataTypePrePersistChecker;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.predicate.EqIdPredicate;


public class ClassDynMetaDataTypePreUpdateChecker extends
        AbstractPreUpdateChecker<ILClassDynMetaDataType, IClassDynMetaDataType>
{
    public ClassDynMetaDataTypePreUpdateChecker(IClassDynMetaDataType object, ILClassDynMetaDataType light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueForSameEntity())
        {
            ret = MessageFormat.format(ERR_2, light.getName(), light.getClassName());
        }

        return ret;
    }

    private boolean isUniqueForSameEntity() throws Hd3dException
    {
        boolean ret = true;

        if (light != null
                && (!object.getClassName().equals(light.getClassName()) || !object.getName().equals(light.getName())))
        {
            // name must be unique for a same entity
            List<IClassDynMetaDataType> classDynMetaDataTypes = ClassDynMetaDataTypePrePersistChecker
                    .getSimilarClassDynMetaDataTypes(light.getClassName(), light.getName());

            Collection<IClassDynMetaDataType> differentIdClassDynMetaDataTypes = CollectionUtils.selectRejected(
                    classDynMetaDataTypes, new EqIdPredicate<IClassDynMetaDataType>(light.getId()));

            ret = CollectUtils.isEmpty(differentIdClassDynMetaDataTypes);
        }

        return ret;
    }
}
