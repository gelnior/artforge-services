package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import java.text.MessageFormat;
import java.util.Set;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILFileAttribute;
import fr.hd3d.model.persistence.IFileAttribute;


public class FileAttributePreUpdateChecker extends AbstractPreUpdateChecker<ILFileAttribute, IFileAttribute>
{
    public FileAttributePreUpdateChecker(IFileAttribute object, ILFileAttribute light)
    {
        super(object, light);
    }

    public String check() throws Hd3dPersistenceException
    {
        String ret = null;

        if (!isUniqueForSameFileRevision())
        {
            ret = MessageFormat.format(ERR_15, light.getName());
        }

        return ret;
    }

    private boolean isUniqueForSameFileRevision() throws Hd3dPersistenceException
    {
        boolean ret = true;

        if (light != null && object.getFileRevision() != null)
        {
            // For a same FileRevision, a FileAttribute's name must be unique
            Set<IFileAttribute> fileAttributes = object.getFileRevision().getAttributes();
            for (IFileAttribute fa : fileAttributes)
            {
                if (fa.getName() != null && fa.getName().equals(light.getName()) && !fa.getId().equals(light.getId()))
                {
                    ret = false;
                    break;
                }
            }
        }

        return ret;
    }

}
