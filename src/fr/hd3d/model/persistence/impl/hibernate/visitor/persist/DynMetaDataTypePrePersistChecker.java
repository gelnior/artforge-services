package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import static fr.hd3d.utils.Const.NAME;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.CollectUtils;


public class DynMetaDataTypePrePersistChecker extends AbstractPrePersistChecker<IDynMetaDataType>
{
    public DynMetaDataTypePrePersistChecker(IDynMetaDataType object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueForSameEntity())
        {
            ret = MessageFormat.format(ERR_20, object.getName());
        }

        return ret;
    }

    private boolean isUniqueForSameEntity() throws Hd3dException
    {
        boolean ret = false;

        if (object != null)
        {
            ret = CollectUtils.isEmpty(getSimilarDynMetaDataTypes(object.getName()));
        }

        return ret;
    }

    public static List<IDynMetaDataType> getSimilarDynMetaDataTypes(String name) throws Hd3dException
    {
        return Persistors.dynmetadatatype.getByValue_NoPermCheck(NAME, name, LockMode.OPTIMISTIC);
    }
}
