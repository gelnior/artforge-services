package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import static fr.hd3d.utils.Const.LOGIN;

import java.text.MessageFormat;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;


public class PersonPrePersistChecker extends AbstractPrePersistChecker<IPerson>
{
    public PersonPrePersistChecker(IPerson object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (alreadyExist())
        {
            ret = MessageFormat.format(ERR_1, object.getLogin());
        }

        return ret;
    }

    private boolean alreadyExist() throws Hd3dException
    {
        return Persistors.person.getObjectByValue_NoPermCheck(LOGIN, object.getLogin()) != null;
    }
}
