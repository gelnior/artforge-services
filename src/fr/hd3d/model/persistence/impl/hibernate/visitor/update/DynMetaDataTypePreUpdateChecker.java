package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.CollectionUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.impl.hibernate.visitor.persist.AbstractPrePersistChecker;
import fr.hd3d.model.persistence.impl.hibernate.visitor.persist.DynMetaDataTypePrePersistChecker;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.predicate.EqIdPredicate;


public class DynMetaDataTypePreUpdateChecker extends AbstractPreUpdateChecker<ILDynMetaDataType, IDynMetaDataType>
{
    public DynMetaDataTypePreUpdateChecker(IDynMetaDataType object, ILDynMetaDataType light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueForSameEntity())
        {
            ret = MessageFormat.format(AbstractPrePersistChecker.ERR_20, light.getName());
        }

        return ret;
    }

    private boolean isUniqueForSameEntity() throws Hd3dException
    {
        boolean ret = true;

        if (light != null && !object.getName().equals(light.getName()))
        {
            List<IDynMetaDataType> dynMetaDataTypes = DynMetaDataTypePrePersistChecker.getSimilarDynMetaDataTypes(light
                    .getName());

            Collection<IDynMetaDataType> differentIdDynMetaDataTypes = CollectionUtils.selectRejected(dynMetaDataTypes,
                    new EqIdPredicate<IDynMetaDataType>(light.getId()));

            ret = CollectUtils.isEmpty(differentIdDynMetaDataTypes);
        }

        return ret;
    }
}
