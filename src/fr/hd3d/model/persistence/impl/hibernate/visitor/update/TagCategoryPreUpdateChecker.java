package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.CollectionUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTagCategory;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.impl.hibernate.visitor.persist.TagCategoryPrePersistChecker;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.predicate.EqIdPredicate;


public class TagCategoryPreUpdateChecker extends AbstractPreUpdateChecker<ILTagCategory, ITagCategory>
{
    public TagCategoryPreUpdateChecker(ITagCategory object, ILTagCategory light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUnique())
        {
            ret = MessageFormat.format(ERR_17, light.getName());
        }

        return ret;
    }

    private boolean isUnique() throws Hd3dException
    {
        boolean ret = true;

        if (light != null && !object.getName().equals(light.getName()))
        {
            /* name or hook must be unique */
            List<ITagCategory> tagCategories = TagCategoryPrePersistChecker.getSimilarTagCatefories(light.getName());

            Collection<ITagCategory> differentIdTagCategories = CollectionUtils.selectRejected(tagCategories,
                    new EqIdPredicate<ITagCategory>(light.getId()));

            ret = CollectUtils.isEmpty(differentIdTagCategories);
        }

        return ret;
    }
}
