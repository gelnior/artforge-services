package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import static fr.hd3d.utils.Const.NAME;
import static fr.hd3d.utils.Const.PROJECT;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class AssetRevisionGroupPrePersistChecker extends AbstractPrePersistChecker<IAssetRevisionGroup>
{
    public AssetRevisionGroupPrePersistChecker(IAssetRevisionGroup object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueWithinProject())
        {
            ret = MessageFormat.format(ERR_13, object.getName());
        }

        return ret;
    }

    private boolean isUniqueWithinProject() throws Hd3dException
    {
        boolean ret = true;

        if (object != null && object.getProject() != null)
        {

            // within a project, an AssetRevision name must be unique
            List<IAssetRevisionGroup> assetRevisionGroup;

            class GetCollectionCmd implements IHibernateQuery
            {
                List<IAssetRevisionGroup> result;

                @SuppressWarnings("unchecked")
                public void execute(Session session) throws Hd3dException
                {
                    HibernatePersist persistor = (HibernatePersist) Persistors.assetrevisiongroup;
                    // DetachedCriteria subQuery = persistor.basicSubCriteria(session);

                    // Note: DO NOT APPLY ANY FILTER

                    final Criteria query = persistor.basicCriteria(session);
                    query.setLockMode(LockMode.OPTIMISTIC);// VERY IMPORTANT
                    Conjunction and = Restrictions.conjunction();
                    and.add(object.getProject() != null ? Restrictions.eq(PROJECT, object.getProject()) : Restrictions
                            .isNull(PROJECT));
                    and.add(Restrictions.eq(NAME, object.getName()));
                    and.add(Restrictions.not(Restrictions.idEq(object.getId())));
                    query.add(and);

                    result = query.list();
                }
            }

            GetCollectionCmd cmd = new GetCollectionCmd();
            HibernateUtil.safeQuery(cmd);
            assetRevisionGroup = cmd.result;

            ret = CollectUtils.isEmpty(assetRevisionGroup);
        }

        return ret;
    }
}
