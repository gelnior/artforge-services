package fr.hd3d.model.persistence.impl.hibernate.visitor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.client.Sheet;
import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ItemGroupH;
import fr.hd3d.model.persistence.impl.hibernate.ItemH;
import fr.hd3d.model.persistence.impl.hibernate.PlanningH;
import fr.hd3d.model.persistence.impl.hibernate.ResourceGroupH;
import fr.hd3d.model.persistence.impl.hibernate.RoleH;
import fr.hd3d.model.persistence.impl.hibernate.SheetH;
import fr.hd3d.model.persistence.impl.hibernate.TaskGroupH;
import fr.hd3d.services.resources.collectionquery.StepsByTaskTypeCollectionQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;


public class ProjectHandler
{
    private static final String DEFAULT_PROJECT_CONSTITUENT_STEP_SHEET_NAME = "Default constituent sheet (steps)";
    private static final String DEFAULT_PROJECT_SHOT_STEP_SHEET_NAME = "Default shot sheet (steps)";
    private static final String DEFAULT_STEP_GROUP_NAME = "Steps";

    private static final String DEFAULT_PROJECT_CONSTITUENT_VALIDATION_SHEET_NAME = "Default constituent sheet (validations)";
    private static final String DEFAULT_PROJECT_SHOT_VALIDATION_SHEET_NAME = "Default shot sheet (validations)";

    /**
     * Creates two simple roles linked to project group :
     * <ul>
     * <li>First role give read-only access to given project.</li>
     * <li>Second role give full access to given project.</li>
     * </ul>
     * 
     * @param group
     * 
     * @param project
     *            The project for which roles will be created.
     * 
     * @throws Hd3dException
     *             Exception raised in case of error while doing requests to database.
     */
    public static void createRolesForProject(IProject project) throws Hd3dException
    {
        String readRight = "*:projects:" + project.getId() + ":read";
        String fullRight = "*:projects:" + project.getId() + ":*";

        IRole role = new RoleH();
        role.setName(project.getName() + " read-only");
        role.getPermissions().add(readRight);
        Persistors.role.persist(role);

        role = new RoleH();
        role.setName(project.getName() + " full");
        role.getPermissions().add(fullRight);
        Persistors.role.persist(role);
    }

    /**
     * Create a group directly linked to the given project.
     * 
     * @param project
     *            The project that needs a group.
     * @return The created group.
     * @throws Hd3dException
     *             Exception raised in case of error while doing requests to database.
     */
    public static IResourceGroup createGroupForProject(IProject project) throws Hd3dException
    {
        IResourceGroup group = new ResourceGroupH();
        group.setName(project.getName());
        group.getProjects().add(project);
        Persistors.resourcegroup.persist(group);

        return group;
    }

    /**
     * Creates four sheets linked to the project :
     * <ul>
     * <li>A breakdown sheet for constituents.</li>
     * <li>A breakdown sheet for shots.</li>
     * <li>A production sheet for constituents.</li>
     * <li>A production sheet for shots.</li>
     * </ul>
     * 
     * @param project
     *            Project for which sheets are needed.
     * @throws Hd3dException
     *             Exception raised in case of error while doing requests to database.
     */
    public static void createSheetsForProject(IProject project) throws Hd3dException
    {
        createSheetForProject(project, ESheetType.BREAKDOWN, "Constituent",
                DEFAULT_PROJECT_CONSTITUENT_STEP_SHEET_NAME, "Steps");

        createSheetForProject(project, ESheetType.BREAKDOWN, "Shot", DEFAULT_PROJECT_SHOT_STEP_SHEET_NAME, "Steps");

        createSheetForProject(project, ESheetType.PRODUCTION, "Constituent",
                DEFAULT_PROJECT_CONSTITUENT_VALIDATION_SHEET_NAME, "Validations");

        createSheetForProject(project, ESheetType.PRODUCTION, "Shot", DEFAULT_PROJECT_SHOT_VALIDATION_SHEET_NAME,
                "Validations");
    }

    /**
     * Creates a sheet linked to given project.
     * 
     * @param project
     *            The project for which sheet is created.
     * @param sheetType
     *            The type (breakdown, production...) of the sheet to create.
     * @param entityName
     *            Entity of the sheet (Constituent, Shot...).
     * @param sheetName
     *            The name of the sheet.
     * @param groupName
     *            The name of the additional group. An empty additional group is created, because default sheets are
     *            used to receive special columns like step or not columns.
     * 
     * @throws Hd3dException
     *             Exception raised in case of error while doing requests to database.
     */
    private static void createSheetForProject(IProject project, ESheetType sheetType, String entityName,
            String sheetName, String groupName) throws Hd3dException
    {
        IItem itemThumbnail = createItem("Thumbnail", "method", "thumbnail", "java.lang.String", Renderer.THUMBNAIL,
                null);
        IItem itemPath = createItem("Path", "collectionQuery",
                "fr.hd3d.services.resources.collectionquery.PathCollectionQuery", "java.lang.String", null, null);
        IItem itemLabel = createItem("Label", "attribute", "label", "java.lang.String", Renderer.SHORT_TEXT,
                Editor.SHORT_TEXT);
        IItem itemDescription = createItem("Description", "attribute", "description", "java.lang.String",
                Renderer.LONG_TEXT, Editor.LONG_TEXT);

        List<IItem> groupItems = new ArrayList<IItem>();
        groupItems.add(itemThumbnail);
        groupItems.add(itemPath);
        groupItems.add(itemLabel);
        groupItems.add(itemDescription);

        if ("Shot".equals(entityName))
        {
            IItem itemNbFrame = createItem("Frames", "attribute", "nbFrame", "java.lang.Integer", null, Editor.INT);

            groupItems.add(itemNbFrame);
        }

        IItemGroup groupInfos = new ItemGroupH("Infos", groupItems, null, "informations");
        Persistors.itemgroup.persist(groupInfos);

        IItemGroup groupSteps = new ItemGroupH(groupName, new ArrayList<IItem>(), null, groupName.toLowerCase());
        Persistors.itemgroup.persist(groupSteps);

        ISheet sheet = new SheetH(sheetName, "Autogenerated sheet", project, entityName, Arrays.asList(groupInfos,
                groupSteps));
        sheet.setType(sheetType);
        Persistors.sheet.persist(sheet);
    }

    /**
     * Create a sheet item and persist it to DB.
     * 
     * @param name
     *            Name of the item.
     * @param type
     *            Type of the item (method, collectionQuery...).
     * @param query
     *            The name of the field or the name of the collection query.
     * @param javaType
     *            Java type of data (java.lang.String, java.lang.Long...)
     * @param renderer
     *            The render of the item.
     * @param editor
     *            The editor of the item.
     * @return The created item.
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    private static IItem createItem(String name, String type, String query, String javaType, String renderer,
            String editor) throws Hd3dException
    {
        IItem item = new ItemH(name, type, query, "", null, javaType, null, null);
        item.setRenderer(renderer);
        item.setEditor(editor);
        Persistors.item.persist(item);

        return item;
    }

    /**
     * @param project
     *            The project for which plannings are requested.
     * @return The plannings linked to the project. If no planning exist, a new one is created as the master planning of
     *         the project.
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    private static List<IPlanning> getOrCreateProjectPlannings(IProject project) throws Hd3dException
    {
        List<IPlanning> plannings = Persistors.planning.getByValue(Const.PROJECT + Const._ID, project.getId());

        if (CollectionUtils.isNotEmpty(plannings))
            return plannings;

        /* no existing planning, create one */
        IPlanning planning = new PlanningH();
        planning.setName(project.getName());
        planning.setProject(project);
        planning.setMaster(Boolean.TRUE);
        planning.setStartDate(new Date());
        planning.setEndDate(DateUtils.addMonths(planning.getStartDate(), 3));

        Persistors.planning.persist(planning);

        List<IPlanning> ret = new ArrayList<IPlanning>();
        ret.add(planning);

        return ret;
    }

    /**
     * Synchronize all data needed for the project to work correctly : update planning task groups, update available
     * note types, update default sheets to set step and note columns directly.
     * 
     * @param project
     *            The project for which data must be updated.
     * 
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    public static void updateTaskTypeRelatedData(IProject project) throws Hd3dException
    {
        if (project == null)
            return;

        final Set<ITaskType> taskTypes = project.getTaskTypes();

        Map<Long, ITaskType> taskTypeMap = CollectUtils.to_Id_EntityMap(taskTypes);

        updateTaskGroupForProject(project, taskTypes, taskTypeMap);

        ApprovalNoteHandler.updateApprovalNoteTypeForProject(project, taskTypes, taskTypeMap);

        updateStepSheetsItems(project, taskTypeMap);
    }

    /**
     * For a given project, synchronize task type linked to the project with the task groups linked to the master
     * planning of the project.
     * 
     * @param project
     *            The project for which synchronization is needed.
     * @param taskTypeMap
     *            Map that gives task type for a task type ID.
     * @param taskTypes
     *            List of task types to synchronize.
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    public static void updateTaskGroupForProject(IProject project, Set<ITaskType> taskTypes,
            Map<Long, ITaskType> taskTypeMap) throws Hd3dException
    {
        if (project == null)
            return;

        List<IPlanning> plannings = getOrCreateProjectPlannings(project);

        for (IPlanning planning : plannings)
        {
            planning.cleanDates();

            final List<ITaskGroup> taskGroups = planning.getTaskGroups();
            Map<Long, ITaskGroup> taskGroupMap = getTaskGroupMap(taskGroups);

            deleteUnusedTaskGroups(project, planning, taskGroups, taskTypeMap);
            createMissingTaskGroups(planning, taskTypes, taskGroupMap);
        }
    }

    /**
     * It deletes all task group that have no corresponding task types in task type list.
     * 
     * @param project
     *            The project for which synchronization is needed.
     * @param planning
     *            The master planning of the project.
     * @param taskGroups
     *            The task groups to synchronize with task task types.
     * @param taskTypeMap
     *            The map to easily find not types corresponding to a given task type ID.
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    private static void deleteUnusedTaskGroups(IProject project, IPlanning planning, List<ITaskGroup> taskGroups,
            Map<Long, ITaskType> taskTypeMap) throws Hd3dException
    {
        final List<ITaskGroup> groupsToDelete = new ArrayList<ITaskGroup>();

        CollectUtils.removeNull(taskGroups);
        for (ITaskGroup taskGroup : taskGroups)
        {
            ITaskType taskGroupTaskType = taskGroup.getTaskType();
            if (taskGroup != null && taskTypeMap.get(taskGroupTaskType.getId()) == null)
            {
                taskGroup.setTaskType(null);
                groupsToDelete.add(taskGroup);
            }
        }

        if (planning != null)
        {
            planning.getTaskGroups().removeAll(groupsToDelete);
            Persistors.planning.merge(planning);
        }
        Persistors.taskGroup.disableCollection(groupsToDelete);
    }

    /**
     * For each task type link to the project, if there is no not type corresponding the note type is created.
     * 
     * 
     * @param project
     *            The project for which synchronization is needed.
     * @param planning
     *            The master planning of the project.
     * @param taskTypes
     *            The task types to synchronize with task groups.
     * @param taskGroupMap
     *            Map to easily find task group from the task type they are linked with.
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    private static void createMissingTaskGroups(IPlanning planning, Set<ITaskType> taskTypes,
            Map<Long, ITaskGroup> taskGroupMap) throws Hd3dException
    {
        if (CollectUtils.isEmpty(taskTypes))
            return;

        List<ITaskGroup> toCreate = new ArrayList<ITaskGroup>();
        ITaskGroup group, newTaskGroup;
        for (ITaskType taskType : taskTypes)
        {
            group = taskGroupMap.get(taskType.getId());
            if (group == null)
            {
                newTaskGroup = new TaskGroupH();
                newTaskGroup.setName(taskType.getName());
                newTaskGroup.setPlanning(planning);
                newTaskGroup.setTaskType(taskType);
                newTaskGroup.setColor(taskType.getColor());
                newTaskGroup.setStartDate(planning.getStartDate());
                newTaskGroup.setEndDate(planning.getEndDate());

                toCreate.add(newTaskGroup);
            }
        }

        Persistors.taskGroup.persistCollection(toCreate);
    }

    /**
     * Update default step sheet items for given project. If a default sheet does not exist, nothing is done and there
     * is no exception raising.
     * 
     * @param project
     *            The project of which breakdown sheets must be updated.
     * @param taskTypeMap
     *            The map to easily find task types corresponding to a given task type ID.
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    public static void updateStepSheetsItems(IProject project, Map<Long, ITaskType> taskTypeMap) throws Hd3dException
    {
        updateStepSheetItems(project, taskTypeMap, DEFAULT_PROJECT_CONSTITUENT_STEP_SHEET_NAME);
        updateStepSheetItems(project, taskTypeMap, DEFAULT_PROJECT_SHOT_STEP_SHEET_NAME);
    }

    /**
     * Update default step sheet items for given project and a given sheet. For each sheet, a step column is created for
     * each task type set on the project. <br>
     * If sheet does not exist, nothing is done and there is no exception raising.
     * 
     * @param project
     *            The project of which breakdown sheets must be updated.
     * @param taskTypeMap
     *            The map to easily find task types corresponding to a given task type ID.
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    public static void updateStepSheetItems(IProject project, Map<Long, ITaskType> taskTypeMap, String sheetName)
            throws Hd3dException
    {
        if (project == null || CollectUtils.isEmpty(taskTypeMap))
            return;

        ISheet sheet = getSheet(project, sheetName);

        if (sheet == null)
            return;

        IItemGroup stepGroup = getItemGroupByName(sheet, DEFAULT_STEP_GROUP_NAME);

        if (stepGroup == null)
            return;

        List<IItem> items = stepGroup.getItems();

        /* Note: Do not check for items nullness here, they will be created */

        Map<Long, IItem> itemMap = new HashMap<Long, IItem>();
        for (IItem item : items)
        {
            if (item == null || StringUtils.isBlank(item.getTransformerParameters()))
                continue;

            itemMap.put(Long.valueOf(item.getTransformerParameters()), item);
        }

        // Add new items
        for (ITaskType taskType : taskTypeMap.values())
        {
            if (itemMap.get(taskType.getId()) == null && sheet.getBoundClassName() != null
                    && taskType.getEntityName() != null
                    && sheet.getBoundClassName().equalsIgnoreCase(taskType.getEntityName()))
            {
                IItem itemStep = new ItemH(taskType.getName(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                        StepsByTaskTypeCollectionQuery.class.getCanonicalName(), null, stepGroup, "java.lang.Long",
                        null, taskType.getId().toString());
                itemStep.setRenderer(Renderer.STEP);
                itemStep.setEditor(Renderer.STEP);

                stepGroup.addItem(itemStep);
            }
        }
        Persistors.itemgroup.merge(stepGroup);

        // Delete unused items
        List<IItem> itemsToDelete = new ArrayList<IItem>();
        for (IItem item : items)
        {
            if (item != null)
            {
                Long taskTypeId = Long.valueOf(item.getTransformerParameters());
                if (taskTypeMap.get(taskTypeId) == null)
                {
                    item.setItemGroup(null);
                    itemsToDelete.add(item);
                }
            }
        }

        stepGroup.getItems().removeAll(itemsToDelete);
        Persistors.item.disableCollection(itemsToDelete);
        Persistors.itemgroup.merge(stepGroup);
    }

    /**
     * @param taskGroups
     *            The list to convert to map.
     * @return Build a map from a task group list with this entry : key = ID of the task type link to the note type and
     *         value = note type.
     */
    private static Map<Long, ITaskGroup> getTaskGroupMap(List<ITaskGroup> taskGroups) throws Hd3dException
    {
        Map<Long, ITaskGroup> taskGroupMap = new HashMap<Long, ITaskGroup>();
        for (ITaskGroup taskGroup : taskGroups)
        {
            if (taskGroup != null && taskGroup.getTaskType() != null)
                taskGroupMap.put(taskGroup.getTaskType().getId(), taskGroup);
        }

        return taskGroupMap;
    }

    /**
     * @param project
     *            The project linked to the sheet to retrieved.
     * @param sheetName
     *            Name of the sheet to retrieve from DB
     * @return Sheet of which name is equal to sheet name and that is linked to project.
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    private static ISheet getSheet(IProject project, String sheetName) throws Hd3dException
    {
        String[] fields = { "name", "project.id" };
        Object[] values = { sheetName, project.getId() };

        return CollectUtils.getFirstOrNull(Persistors.sheet.getByValues(fields, values, "and"));
    }

    /**
     * @param sheet
     *            The sheet linked to the sheet to retrieved.
     * @param groupName
     *            Name of the item group to retrieve from DB
     * @return Item group of which name is equal to groupName and that is linked to the given sheet.
     */
    private static IItemGroup getItemGroupByName(ISheet sheet, String groupName)
    {
        if (sheet == null || groupName == null)
            return null;

        List<IItemGroup> groups = sheet.getItemGroups();

        IItemGroup stepGroup = null;
        for (IItemGroup group : groups)
        {
            if (groupName.equals(group.getName()))
            {
                stepGroup = group;
                break;
            }
        }

        return stepGroup;
    }
}
