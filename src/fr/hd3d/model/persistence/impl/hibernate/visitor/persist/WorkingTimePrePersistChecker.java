package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import java.text.MessageFormat;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IWorkingTime;


public class WorkingTimePrePersistChecker extends AbstractPrePersistChecker<IWorkingTime>
{
    public WorkingTimePrePersistChecker(IWorkingTime object)
    {
        super(object);
    }

    public String check() throws Hd3dPersistenceException
    {
        String ret = null;

        /* startDate must be before endDate */
        if (object.getStartDate().after(object.getEndDate()))
        {
            ret = MessageFormat.format(ERR_18, object.getStartDate(), object.getEndDate());
        }

        return ret;
    }
}
