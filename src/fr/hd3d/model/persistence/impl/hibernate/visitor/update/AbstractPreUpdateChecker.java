package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import fr.hd3d.exception.Hd3dException;


/**
 * 
 * 
 * IMPORTANT NOTE: <br>
 * It is not possible to do things such as:<br>
 * Ex: for IConstituent to be created<br>
 * Set<IConstituent> constituents = object.getCategory().getConstituents();<br>
 * for (IConstituent c : constituents) <br>
 * { <br>
 * tests... <br>
 * } since object (to be created) is not yet persisted, any method call on it would be inconsistent.<br>
 * One must use HibernateUtil.SafeQuery instead, or whatever other way to do a "fresh query"
 * 
 * @author try.lam
 * 
 */
public abstract class AbstractPreUpdateChecker<L, P>
{
    public static String ERR_1 = "A person with login=={0} already exists";
    public static String ERR_2 = "A ClassDynMetaDataType with name={0} or with className={1} already exists";
    public static String ERR_3 = "A project with name={0} already exists";
    public static String ERR_4 = "A Category with name={0} already exists in this project";
    public static String ERR_5 = "A Constituent with label={0} already exists in this category";
    public static String ERR_6 = "A Sequence with name={0} already exists in this project";
    public static String ERR_7 = "A Shot with label={0} already exists in this sequence";
    public static String ERR_8 = "A Composition with name={0} already exists";
    public static String ERR_9 = "A sheet with name={0} already exists in this project";
    public static String ERR_10 = "A AssetRevision with name={0} or triplet(key,variation,revision)={1},{2},{3} already exists in this project";
    public static String ERR_11 = "A FileRevision with triplet(key,variation,revision)={0},{1},{2} already exists in this project";
    public static String ERR_12 = "A FileRevision with location + relativePath={0} already exists in this project";
    public static String ERR_13 = "A AssetRevisionGroup with name={0} already exists in this project";
    public static String ERR_14 = "An ItemGroup with name={0} already exists in this project";
    public static String ERR_15 = "A FileAttribute with name={0} already exists for this FileRevision";
    public static String ERR_16 = "A Group with name={0} already exists";
    public static String ERR_17 = "A Tag Category with name={0} already exists";
    public static String ERR_19 = "A Composition's Constituent must not be null";
    public static String ERR_20 = "A Task with project=={0} and name={1} already exists";

    protected P object;
    protected L light;

    public AbstractPreUpdateChecker(P object, L light)
    {
        this.object = object;
        this.light = light;
    }

    public abstract String check() throws Hd3dException;
}
