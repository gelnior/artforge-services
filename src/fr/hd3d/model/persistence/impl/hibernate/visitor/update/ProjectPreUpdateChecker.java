package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.CollectionUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.impl.hibernate.visitor.persist.ProjectPrePersistChecker;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.predicate.EqIdPredicate;


public class ProjectPreUpdateChecker extends AbstractPreUpdateChecker<ILProject, IProject>
{
    public ProjectPreUpdateChecker(IProject object, ILProject light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUnique())
        {
            ret = MessageFormat.format(ERR_3, light.getName());
        }

        return ret;
    }

    private boolean isUnique() throws Hd3dException
    {
        boolean ret = true;

        if (light != null)
        {
            if (light.getName() == null)
            {
                return true;
            }
            /* name or hook must be unique */
            if (!object.getName().equals(light.getName()))
            {
                List<IProject> projects = ProjectPrePersistChecker.getSimilarProjects(light.getName(), light.getHook());

                Collection<IProject> differentIdProjects = CollectionUtils.selectRejected(projects,
                        new EqIdPredicate<IProject>(light.getId()));

                ret = CollectUtils.isEmpty(differentIdProjects);
            }
        }

        return ret;
    }
}
