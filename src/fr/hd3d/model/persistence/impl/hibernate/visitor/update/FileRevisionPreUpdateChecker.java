package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import static fr.hd3d.utils.Const.ID;
import static fr.hd3d.utils.Const.KEY;
import static fr.hd3d.utils.Const.REVISION;
import static fr.hd3d.utils.Const.VARIATION;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class FileRevisionPreUpdateChecker extends AbstractPreUpdateChecker<ILFileRevision, IFileRevision>
{
    public FileRevisionPreUpdateChecker(IFileRevision object, ILFileRevision light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isTripletUnique())
        {
            ret = MessageFormat.format(ERR_11, light.getKey(), light.getVariation(), light.getRevision());
        }

        if (!isFullPathUnique())
        {
            ret += MessageFormat.format(ERR_12, light.getFullPath());
        }

        return ret;
    }

    private boolean isTripletUnique() throws Hd3dException
    {
        boolean ret = true;

        if (object != null
                && (!object.getKey().equals(light.getKey()) || !object.getVariation().equals(light.getVariation()) || !object
                        .getRevision().equals(light.getRevision())))
        {
            // Independently from projects,
            // the triplet (key,variation,revision) must be unique
            List<IFileRevision> assetrevisions;

            class GetCollectionCmd implements IHibernateQuery
            {
                List<IFileRevision> result;

                @SuppressWarnings("unchecked")
                public void execute(Session session) throws Hd3dException
                {
                    HibernatePersist persistor = (HibernatePersist) Persistors.filerevision;
                    // DetachedCriteria subQuery = persistor.basicSubCriteria(session);

                    // Note: DO NOT APPLY ANY FILTER

                    final Criteria query = persistor.basicCriteria(session);
                    query.setLockMode(LockMode.OPTIMISTIC);// VERY IMPORTANT
                    query.add(Restrictions.ne(ID, light.getId()));
                    query.add(Restrictions.eq(KEY, light.getKey()));
                    query.add(Restrictions.eq(VARIATION, light.getVariation()));
                    query.add(Restrictions.eq(REVISION, light.getRevision()));

                    result = query.list();
                }
            }

            GetCollectionCmd cmd = new GetCollectionCmd();
            HibernateUtil.safeQuery(cmd);
            assetrevisions = cmd.result;

            ret = CollectUtils.isEmpty(assetrevisions);
        }

        return ret;
    }

    private boolean isFullPathUnique() throws Hd3dPersistenceException
    {
        boolean ret = true;

        // ===============================================================
        // 2nd check
        // location + relativePath must be unique
        // ===============================================================
        if (!object.getFullPath().equals(light.getFullPath()))
        {
            BooleanQuery bq = new BooleanQuery();

            TermQuery internalStatusTq = new TermQuery(new Term("internalstatus", "0"));
            bq.add(new BooleanClause(internalStatusTq, BooleanClause.Occur.MUST));

            TermQuery idTq = new TermQuery(new Term(ID, String.valueOf(light.getId())));
            bq.add(new BooleanClause(idTq, BooleanClause.Occur.MUST_NOT));

            TermQuery fullPathTq = new TermQuery(new Term("fullpath", light.getFullPath()));
            bq.add(new BooleanClause(fullPathTq, BooleanClause.Occur.MUST));

            FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());

            org.hibernate.Query hibQuery = fullTextSession.createFullTextQuery(bq, FileRevisionH.class);

            List<IFileRevision> result = hibQuery.list();

            ret = CollectionUtils.isEmpty(result);
        }

        return ret;
    }
}
