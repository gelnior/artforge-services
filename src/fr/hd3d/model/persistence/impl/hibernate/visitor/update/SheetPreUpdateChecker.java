package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.CollectionUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILSheet;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.impl.hibernate.visitor.persist.SheetPrePersistChecker;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.predicate.EqIdPredicate;


public class SheetPreUpdateChecker extends AbstractPreUpdateChecker<ILSheet, ISheet>
{
    public SheetPreUpdateChecker(ISheet object, ILSheet light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueWithinProject())
        {
            ret = MessageFormat.format(ERR_9, light.getName());
        }

        return ret;
    }

    private boolean isUniqueWithinProject() throws Hd3dException
    {
        boolean ret = true;

        // if the sheet does not (yet) belong to a project, no problem
        if (light != null && light.getProject() != null && !object.getProject().getId().equals(light.getProject())
                || !object.getName().equals(light.getName()))
        {
            List<ISheet> sheets = SheetPrePersistChecker.getSimilarSheets(light.getProject(), light.getName());

            Collection<ISheet> differentIdSheets = CollectionUtils.selectRejected(sheets, new EqIdPredicate<ISheet>(
                    light.getId()));

            ret = CollectUtils.isEmpty(differentIdSheets);
        }

        return ret;
    }
}
