package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;


public class TaskTypePrePersistChecker extends AbstractPrePersistChecker<ITaskType>
{
    public TaskTypePrePersistChecker(ITaskType object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUnique())
        {
            ret = MessageFormat.format(ERR_21, object.getProject(), object.getName());
        }

        return ret;
    }

    private boolean isUnique() throws Hd3dException
    {
        boolean ret = true;

        // tasktype's project + name must be unique (for enabled entities)
        String[] properties = new String[] { "project", "name" };
        Object[] values = new Object[] { object.getProject(), object.getName() };

        List<ITaskType> taskTypes = Persistors.tasktype.getByValues(null, properties, values, "AND");
        ret = CollectionUtils.isEmpty(taskTypes);

        return ret;
    }
}
