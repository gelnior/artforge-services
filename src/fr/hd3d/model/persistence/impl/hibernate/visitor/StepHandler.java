package fr.hd3d.model.persistence.impl.hibernate.visitor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.Closure;
import org.apache.commons.collections15.FunctorException;
import org.apache.commons.collections15.MultiMap;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.enums.EAssetStatus;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.Hook;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.model.persistence.impl.hibernate.query.Queries;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Pair;


public class StepHandler
{
    public static void createTasksForStep(final IStep step) throws Hd3dPersistenceException
    {
        if (Conf.createTaskForStep() && isOkForTaskCreation(step))
        {
            List<IStep> steps = new ArrayList<IStep>();
            steps.add(step);
            createForSteps(steps);
            CollectUtils.safeClear(steps);
            steps = null;
        }
    }

    public static void createTasksForSteps(final List<IStep> steps) throws Hd3dPersistenceException
    {
        if (Conf.createTaskForStep())
        {
            for (IStep step : steps)
            {
                if (isOkForTaskCreation(step))
                {
                    List<IStep> newSteps = new ArrayList<IStep>();
                    newSteps.add(step);
                    createForSteps(newSteps);
                    CollectUtils.safeClear(newSteps);
                    newSteps = null;
                }
            }
        }
    }

    private static List<IStep> filterStepsForTasksCreation(final List<IStep> steps)
    {
        return (List<IStep>) org.apache.commons.collections15.CollectionUtils.select(steps, new Predicate<IStep>() {
            public boolean evaluate(IStep step)
            {
                return isOkForTaskCreation(step);
            }
        });
    }

    private static List<IStep> filterStepsForTasksUpdate(final List<IStep> steps)
    {
        return (List<IStep>) org.apache.commons.collections15.CollectionUtils.select(steps, new Predicate<IStep>() {
            public boolean evaluate(IStep step)
            {
                return isOkForTaskUpdate(step);
            }
        });
    }

    private static List<IStep> filterStepsForTasksDeletion(final List<IStep> steps)
    {
        return (List<IStep>) org.apache.commons.collections15.CollectionUtils.select(steps, new Predicate<IStep>() {
            public boolean evaluate(IStep step)
            {
                return isOkForTaskDeletion(step);
            }
        });
    }

    private static MultiMap<String, Long> collectWorkObjectIds(final List<IStep> steps)
    {
        /* woIdsMap: key=entityName, value=ids of the entities */
        final MultiMap<String, Long> woIdsMap = new MultiHashMap<String, Long>();

        Collection<IStep> notNullSteps = CollectUtils.selectNotNull(steps);
        if (CollectUtils.isNotEmpty(notNullSteps))
        {
            for (final IStep step : steps)
            {
                woIdsMap.put(step.getBoundEntityName(), step.getBoundEntity());
            }

            CollectUtils.safeClear(notNullSteps);
        }

        notNullSteps = null;

        return woIdsMap;
    }

    private static MultiMap<String, IBase> collectWorkObjects(final MultiMap<String, Long> stepsWoIdsMap)
    {
        final MultiMap<String, IBase> woMap = new MultiHashMap<String, IBase>();

        for (Entry<String, Collection<Long>> entry : stepsWoIdsMap.entrySet())
        {
            woMap.putAll(entry.getKey(), Queries.getBoundEntities(entry.getValue(), entry.getKey()));
        }

        return woMap;
    }

    private static Map<IStep, IBase> reorderByStep(final List<IStep> filteredSteps, final MultiMap<String, IBase> woMap)
    {
        Map<IStep, IBase> stepToWoMap = null;

        Collection<IStep> notNullSteps = CollectUtils.selectNotNull(filteredSteps);
        if (CollectUtils.isNotEmpty(notNullSteps))
        {
            stepToWoMap = new HashMap<IStep, IBase>(notNullSteps.size());

            for (final IStep step : notNullSteps)
            {
                for (IBase wo : woMap.get(step.getBoundEntityName()))
                {
                    if (wo.getId().equals(step.getBoundEntity()))
                    {
                        stepToWoMap.put(step, wo);
                    }
                }
            }

            CollectUtils.safeClear(notNullSteps);
        }

        notNullSteps = null;

        return stepToWoMap;
    }

    private static MultiMap<IStep, ITask> buildStepTaskMap(final List<IStep> filteredSteps,
            final MultiMap<String, Long> woIdsMap)
    {
        Collection<IStep> notNullSteps = CollectUtils.selectNotNull(filteredSteps);

        /*---------------------------------------------------------------------*/
        /* for each steps, found out corresponding task with the same TaskType */
        /*---------------------------------------------------------------------*/
        final Map<Long, ITaskType> stepTaskTypesMap = getStepTaskTypesMap(notNullSteps);

        /* Process by slices */
        Map<String, List<List<Long>>> partitionedMap = CollectUtils.partitionValue(woIdsMap,
                HibernateUtil.JDBC_BATCHSIZE);

        String boundEntities;
        String taskTypeIds;
        String hql;
        List<ITask> result = new ArrayList<ITask>();
        for (Entry<String, List<List<Long>>> entry : partitionedMap.entrySet())
        {
            for (List<Long> idsPartition : entry.getValue())
            {
                boundEntities = StringUtils.join(idsPartition, ',');
                taskTypeIds = StringUtils.join(CollectUtils.getIds(stepTaskTypesMap.values()), ',');

                hql = "select t from TaskH t inner join t.boundEntityTaskLinks as links where ";
                if (StringUtils.isNotEmpty(boundEntities))
                {
                    hql += " links.boundEntity in (" + boundEntities + ") and ";
                }

                hql += " links.boundEntityName='"
                /* must be done one entity type at once(different type may have same id) */
                + entry.getKey() + "' ";

                if (StringUtils.isNotEmpty(taskTypeIds))
                {
                    hql += " and t.taskType.id in (" + taskTypeIds + ") and ";
                }
                hql += " t.internalStatus=0 order by links.boundEntityName,links.boundEntity";

                result.addAll((List<ITask>) HibernateUtil.currentSession().createQuery(hql).list());

                CollectUtils.removeNull(result);
            }
        }
        /*
         * associate the tasks with the steps, in theory, there may be many tasks for a same step but normally, there is
         * just one task for one step. stepTasksMap: key=step, value=list of corresponding tasks
         */
        // TODO simple algo, may be done better.
        final MultiMap<IStep, ITask> stepTasksMap = new MultiHashMap<IStep, ITask>();
        for (final IStep step : notNullSteps)
        {
            boolean found = false;
            for (ITask task : result)
            {
                if (task.getBoundEntityTaskLink() != null && step.getBoundEntity() != null
                        && step.getBoundEntity().equals(task.getBoundEntityTaskLink().getBoundEntity())
                        && step.getBoundEntityName().equals(task.getBoundEntityTaskLink().getBoundEntityName())
                        && step.getTaskType() != null && task.getTaskType() != null
                        && step.getTaskType().getId().equals(task.getTaskType().getId()))
                {
                    stepTasksMap.put(step, task);
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                stepTasksMap.put(step, null);
            }
        }

        /* clean */
        CollectUtils.safeClear(notNullSteps);
        notNullSteps = null;
        CollectUtils.safeClear(partitionedMap);
        partitionedMap = null;

        return stepTasksMap;
    }

    private static String getAssetGroupName(final IBase wo)
    {
        String ret = null;

        if (wo != null)
        {
            String hook = null;
            if (Hook.class.isInstance(wo))
            {
                hook = ((Hook) wo).getHook();
            }

            if (hook != null)
            {
                ret = new StringBuilder(String.valueOf(wo.getId())).append('_').append(hook).append("_GRP").toString();
            }
        }

        return ret;
    }

    private static String getAssetName(final IStep step)
    {
        String ret = null;

        if (step != null && step.getTaskType() != null)
        {
            ret = step.getTaskType().getName() + " (default)";
        }

        return ret;
    }

    private static Map<Long, ITaskType> getStepTaskTypesMap(Collection<IStep> steps)
    {
        final Map<Long, ITaskType> stepTaskTypesMap = new HashMap<Long, ITaskType>();
        for (IStep step : steps)
        {
            stepTaskTypesMap.put(step.getId(), step.getTaskType());
        }
        return stepTaskTypesMap;
    }

    private static Pair<MultiMap<IStep, IAssetRevisionGroup>, MultiMap<IStep, IAssetRevision>> buildStepAssetGroupMap(
            final MultiMap<String, Long> woIdsMap, final Map<IStep, IBase> stepToWoMap)
    {
        /* Process by slices */
        List<List<Entry<IStep, IBase>>> partitionedMap = CollectUtils.partition(new ArrayList<Entry<IStep, IBase>>(
                stepToWoMap.entrySet()), HibernateUtil.JDBC_BATCHSIZE);

        List<IAssetRevisionGroup> groupResult = new ArrayList<IAssetRevisionGroup>();
        List<IAssetRevision> assetResult = new ArrayList<IAssetRevision>();
        for (List<Entry<IStep, IBase>> entries : partitionedMap)
        {
            /* Collect assetGroup info */
            final Set<String> assetGroupNamesSet = new HashSet<String>();
            final Set<String> woEntityNamesSet = new HashSet<String>();
            final Set<Long> woIds = new HashSet<Long>();

            IBase workObj;
            String assetGroupName;
            for (Entry<IStep, IBase> entry : entries)
            {
                workObj = entry.getValue();
                assetGroupName = getAssetGroupName(workObj);
                if (assetGroupName != null)
                {
                    assetGroupNamesSet.add(assetGroupName);
                    woEntityNamesSet.add(workObj.entityName());
                    woIds.add(workObj.getId());
                }
            }

            String hql = "from AssetRevisionGroupH a where a.criteria in ('"
                    + StringUtils.join(woEntityNamesSet, "','") + "') and a.value in ('"
                    + StringUtils.join(woIds, "','") + "') and a.internalStatus=0 order by a.criteria,a.value";

            groupResult.addAll(HibernateUtil.currentSession().createQuery(hql).list());

            CollectUtils.removeNull(groupResult);

            if (CollectUtils.isNotEmpty(groupResult))
            {
                /* Collect assets info */
                final Set<String> assetNamesSet = new HashSet<String>();
                final Set<Long> taskTypeIdsSet = new HashSet<Long>();
                final Set<Long> projectIdsSet = new HashSet<Long>();

                IStep step;
                String assetName;
                for (Entry<IStep, IBase> entry : entries)
                {
                    step = entry.getKey();
                    workObj = entry.getValue();
                    assetName = getAssetName(step);
                    if (assetName != null)
                    {
                        assetNamesSet.add(assetName);
                    }

                    final ITaskType taskType = step.getTaskType();
                    if (taskType != null)
                    {
                        taskTypeIdsSet.add(taskType.getId());
                    }

                    final IProject project = getProjectFromWo(workObj);
                    if (project != null)
                    {
                        projectIdsSet.add(project.getId());
                    }
                }

                // if (CollectUtils.isNotEmpty(assetNamesSet) && CollectUtils.isNotEmpty(taskTypeIdsSet))
                // {
                hql = "from AssetRevisionH as a join a.assetRevisionGroups as groups where a.taskType.id in ("
                        + StringUtils.join(taskTypeIdsSet, ',') + ") and a.project.id in ("
                        + StringUtils.join(projectIdsSet, ',') + ") and groups.id in ("
                        + StringUtils.join(CollectUtils.getIds(groupResult), ',')
                        + ") and a.internalStatus=0 order by a.taskType.id, a.project.id";

                assetResult.addAll(HibernateUtil.currentSession().createQuery(hql).list());
                // }
            }
        }

        CollectUtils.removeNull(assetResult);

        /*
         * associate the tasks with the asset revision groups, in theory, there may be many groups for a same step but
         * normally, there is just one groups for one step. stepAssetGroupsMap: key=step, value=list of corresponding
         * groups
         */
        // TODO simple algo, may be done better.
        final MultiMap<IStep, IAssetRevisionGroup> stepAssetGroupsMap = new MultiHashMap<IStep, IAssetRevisionGroup>();
        for (final Entry<IStep, IBase> entry : stepToWoMap.entrySet())
        {
            final IStep step = entry.getKey();
            final IBase workObj = entry.getValue();

            IAssetRevisionGroup group = null;

            if (workObj != null)
            {
                for (IAssetRevisionGroup assetRevGrp : groupResult)
                {
                    if (assetRevGrp != null && assetRevGrp.getCriteria() != null && assetRevGrp.getValue() != null
                            && workObj.entityName().equals(assetRevGrp.getCriteria())
                            && String.valueOf(workObj.getId()).equals(assetRevGrp.getValue()))
                    {
                        group = assetRevGrp;
                        break;
                    }
                }
            }

            stepAssetGroupsMap.put(step, group);
        }

        /* associate steps to corresponding asset revisions */
        final MultiMap<IStep, IAssetRevision> stepAssetsMap = new MultiHashMap<IStep, IAssetRevision>();
        for (final Entry<IStep, IBase> entry : stepToWoMap.entrySet())
        {
            final IStep step = entry.getKey();
            final IBase wo = entry.getValue();

            IAssetRevision assetRevision = null;

            if (wo != null)
            {
                Long woProjectId;
                Long taskTypId;
                String stepName;

                for (IAssetRevision assetRev : assetResult)
                {
                    woProjectId = CollectUtils.nullSafeId(getProjectFromWo(wo));
                    taskTypId = CollectUtils.nullSafeId(step.getTaskType());
                    stepName = getDefaultAssetRevisionName(step);

                    if (assetRev.getProject() != null && assetRev.getTaskType() != null && assetRev.getName() != null
                            && assetRev.getProject().getId().equals(woProjectId)
                            && assetRev.getTaskType().getId().equals(taskTypId) && assetRev.getName().equals(stepName))

                    {
                        assetRevision = assetRev;
                        break;
                    }
                }
            }

            stepAssetsMap.put(step, assetRevision);
        }

        Pair<MultiMap<IStep, IAssetRevisionGroup>, MultiMap<IStep, IAssetRevision>> ret = new Pair<MultiMap<IStep, IAssetRevisionGroup>, MultiMap<IStep, IAssetRevision>>(
                stepAssetGroupsMap, stepAssetsMap);

        return ret;
    }

    private static IProject getProjectFromWo(final IBase wo)
    {
        IProject ret = null;

        if (wo != null)
        {
            if (Const.ENTITYNAME_CONSTITUENT.equals(wo.entityName()))
            {
                ret = ((IConstituent) wo).getCategory().getProject();
            }
            else if (Const.ENTITYNAME_SHOT.equals(wo.entityName()))
            {
                ret = ((IShot) wo).getSequence().getProject();
            }
        }

        return ret;
    }

    private static String getHookFromWo(final IBase wo)
    {
        String ret = null;

        if (wo != null && Hook.class.isInstance(wo))
        {
            ret = ((Hook) wo).getHook();
        }

        return ret;
    }

    private static ITask createNewTask(final IStep step, final IBase wo) throws Hd3dException
    {
        ITask ret = null;

        if (step != null && wo != null)
        {
            ret = new TaskH();
            ret.setName(step.getTaskType().getName() + ' ' + getHookFromWo(wo));
            ret.setStatus(ETaskStatus.defaultStatus());
            ret.setProject(getProjectFromWo(wo));
            ret.setDuration(Long.valueOf(step.getEstimatedDuration()));// in days
            ret.setTaskType(step.getTaskType());
            ret.setBoundEntityId(wo.getId());
            ret.setBoundEntityName(wo.entityName());
        }

        return ret;
    }

    private static String getDefaultAssetRevisionName(final IStep step)
    {
        String ret = null;

        if (step != null && step.getTaskType() != null && step.getTaskType().getName() != null)
        {
            ret = step.getTaskType().getName() + " (default)";
        }

        return ret;
    }

    private static void createTasks(final List<IStep> steps, final List<IStep> filteredSteps,
            final MultiMap<String, Long> woIdsMap, final Map<IStep, IBase> stepToWoMap) throws Hd3dPersistenceException
    {
        if (CollectUtils.isNotEmpty(steps) && CollectUtils.isNotEmpty(filteredSteps)
                && CollectUtils.isNotEmpty(woIdsMap) && CollectUtils.isNotEmpty(stepToWoMap))
        {
            /*---------------------------------------*/
            /* bulk create tasks and entityTaskLinks */
            /* If task exists, nothing to do */
            /* If task does not exist, create it */
            /*---------------------------------------*/
            final MultiMap<IStep, ITask> stepTasksMap = buildStepTaskMap(filteredSteps, woIdsMap);

            if (CollectUtils.isNotEmpty(stepTasksMap))
            {
                CollectUtils.doByPartition(new ArrayList<Entry<IStep, Collection<ITask>>>(stepTasksMap.entrySet()),
                        HibernateUtil.JDBC_BATCHSIZE, new CreateTaskClosure(stepToWoMap));
            }
        }
    }

    public static class CreateTaskClosure implements Closure<List<Entry<IStep, Collection<ITask>>>>
    {
        final Map<IStep, IBase> stepToWoMap;

        public CreateTaskClosure(Map<IStep, IBase> stepToWoMap)
        {
            this.stepToWoMap = stepToWoMap;
        }

        public void execute(List<Entry<IStep, Collection<ITask>>> partition) throws FunctorException
        {
            /* new tasks to create */
            List<ITask> newTasks = new ArrayList<ITask>(partition.size());
            IStep step;
            Collection<ITask> tasks;
            for (final Entry<IStep, Collection<ITask>> entry : partition)
            {
                step = entry.getKey();
                tasks = entry.getValue();

                /* in POST method, if a step has no task, create it */
                if (CollectUtils.isNotEmpty(tasks))
                {
                    IBase workObj;
                    String name;
                    for (final ITask task : tasks)
                    {
                        if (task == null)
                        {
                            workObj = stepToWoMap.get(step);
                            name = getHookFromWo(workObj);

                            if (name != null)
                            {
                                ITask newTask;
                                try
                                {
                                    newTask = createNewTask(step, workObj);
                                    if (newTask != null)
                                    {
                                        newTasks.add(newTask);
                                    }
                                }
                                catch (Hd3dException e)
                                {
                                    throw new FunctorException(e);
                                }

                            }
                        }
                    }
                }
            }

            /* Persist */
            if (CollectUtils.isNotEmpty(newTasks))
            {
                try
                {
                    for (ITask task : newTasks)
                    {
                        task.doPrePersist();
                    }
                    Persistors.task.persistCollection(newTasks);
                    for (ITask task : newTasks)
                    {
                        task.doPostPersist();
                    }
                    CollectUtils.safeClear(newTasks);
                    newTasks = null;
                }
                catch (Hd3dException e)
                {
                    throw new FunctorException(e);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private static void createAssetGroups(final MultiMap<String, Long> woIdsMap, final Map<IStep, IBase> stepToWoMap)
            throws Hd3dPersistenceException
    {
        final Pair<MultiMap<IStep, IAssetRevisionGroup>, MultiMap<IStep, IAssetRevision>> buildStepAssetGroupMap = buildStepAssetGroupMap(
                woIdsMap, stepToWoMap);

        MultiMap<IStep, IAssetRevisionGroup> stepAssetGroupsMap = buildStepAssetGroupMap.getField_1();
        MultiMap<IStep, IAssetRevision> stepAssetsMap = buildStepAssetGroupMap.getField_2();

        if (CollectUtils.isNotEmpty(stepAssetGroupsMap))
        {
            CollectUtils.doByPartition(new ArrayList(stepAssetGroupsMap.entrySet()), HibernateUtil.JDBC_BATCHSIZE,
                    new CreateAssetGroupClosure(stepToWoMap, stepAssetsMap));
        }
    }

    public static class CreateAssetGroupClosure implements Closure<List<Entry<IStep, Collection<IAssetRevisionGroup>>>>
    {
        final Map<IStep, IBase> stepToWoMap;
        final MultiMap<IStep, IAssetRevision> stepAssetsMap;

        public CreateAssetGroupClosure(Map<IStep, IBase> stepToWoMap, MultiMap<IStep, IAssetRevision> stepAssetsMap)
        {
            this.stepToWoMap = stepToWoMap;
            this.stepAssetsMap = stepAssetsMap;
        }

        public void execute(List<Entry<IStep, Collection<IAssetRevisionGroup>>> partition) throws FunctorException
        {
            /* new assetrevisiongroups to create */
            List<IAssetRevisionGroup> newGroups = new ArrayList<IAssetRevisionGroup>();

            /* IAssetRevisionGroup to update */
            List<IAssetRevisionGroup> updatedGroups = new ArrayList<IAssetRevisionGroup>();

            /* new assetrevisions to create */
            List<IAssetRevision> newAssets = new ArrayList<IAssetRevision>();

            /* assetrevisions to update */
            List<IAssetRevision> updatedAssets = new ArrayList<IAssetRevision>();

            IStep step;
            Collection<IAssetRevisionGroup> groups;
            String assetName;
            Set<IAssetRevision> assetRevisions;
            IBase workObj;
            for (Entry<IStep, Collection<IAssetRevisionGroup>> entry : partition)
            {
                step = entry.getKey();
                groups = entry.getValue();

                CollectUtils.removeNull(groups);

                /* for each step, if there's no AssetRevisionGroup, */
                if (CollectUtils.isNotEmpty(groups))
                {
                    for (IAssetRevisionGroup assetRevisionGroup : groups)
                    {
                        /* for each step, if AssetRevisionGroup exists, check for corresponding AssetRevisions */
                        assetName = getDefaultAssetRevisionName(step);
                        if (assetName != null)
                        {
                            boolean found = false;
                            assetRevisions = assetRevisionGroup.getAssetRevisions();
                            CollectUtils.removeNull(assetRevisions);
                            if (CollectUtils.isNotEmpty(assetRevisions))
                            {
                                for (IAssetRevision asset : assetRevisions)
                                {
                                    if (asset.getTaskType() != null && asset.getTaskType().getName() != null
                                            && asset.getTaskType().getName().equals(assetName))
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    workObj = stepToWoMap.get(step);
                                    if (workObj != null)
                                    {
                                        IAssetRevision asset = createAssetRevision(step, workObj, assetRevisionGroup);
                                        if (asset != null)
                                        {
                                            asset.addAssetRevisionGroup(assetRevisionGroup);
                                            newAssets.add(asset);
                                            updatedGroups.add(assetRevisionGroup);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    workObj = stepToWoMap.get(step);
                    if (workObj != null)
                    {
                        /* for each step, if there's no assetrevisiongroup, create it */
                        IAssetRevisionGroup group = createAssetRevisionGroup(step, workObj);
                        if (group != null)
                        {
                            newGroups.add(group);
                        }

                        /* if asset revisions already exist for this step, bind them to the newly created group */
                        assetName = getDefaultAssetRevisionName(step);
                        if (assetName != null)
                        {
                            Collection<IAssetRevision> assets = stepAssetsMap.get(step);
                            CollectUtils.removeNull(assets);
                            if (CollectionUtils.isNotEmpty(assets))
                            {
                                for (IAssetRevision asset : assets)
                                {
                                    if (asset.getTaskType() != null && asset.getTaskType().getName() != null
                                            && asset.getTaskType().getName().equals(assetName)
                                            && !asset.belongsTo(group))
                                    {
                                        asset.addAssetRevisionGroup(group);
                                        updatedAssets.add(asset);
                                    }
                                }
                            }
                            else
                            {
                                IAssetRevision asset = createAssetRevision(step, workObj, group);
                                if (asset != null)
                                {
                                    newAssets.add(asset);
                                }
                            }
                        }
                        /* otherwise, create new assetrevision and bind it to the newly created group */
                        else
                        {
                            IAssetRevision asset = createAssetRevision(step, workObj, group);
                            if (asset != null)
                            {
                                newAssets.add(asset);
                            }
                        }
                    }
                }
            }

            /* Persist */
            if (CollectUtils.isNotEmpty(updatedGroups))
            {
                try
                {
                    for (IAssetRevisionGroup group : updatedGroups)
                    {
                        group.doPreUpdate();
                    }
                    Persistors.assetrevisiongroup.mergeCollection(updatedGroups);
                    for (IAssetRevisionGroup group : updatedGroups)
                    {
                        group.doPostUpdate();
                    }
                    CollectUtils.safeClear(updatedGroups);
                    updatedGroups = null;
                }
                catch (Hd3dException e)
                {
                    throw new FunctorException(e);
                }
            }
            if (CollectUtils.isNotEmpty(updatedAssets))
            {
                try
                {
                    for (IAssetRevision assetRevision : updatedAssets)
                    {
                        assetRevision.doPreUpdate();
                    }
                    Persistors.assetrevision.mergeCollection(updatedAssets);
                    for (IAssetRevision assetRevision : updatedAssets)
                    {
                        assetRevision.doPostUpdate();
                    }
                    CollectUtils.safeClear(updatedAssets);
                    updatedAssets = null;
                }
                catch (Hd3dException e)
                {
                    throw new FunctorException(e);
                }
            }
            if (CollectUtils.isNotEmpty(newAssets))
            {
                try
                {
                    for (IAssetRevision assetRevision : newAssets)
                    {
                        assetRevision.doPrePersist();
                    }
                    Persistors.assetrevision.persistCollection(newAssets);
                    for (IAssetRevision assetRevision : newAssets)
                    {
                        assetRevision.doPostPersist();
                    }
                    CollectUtils.safeClear(newAssets);
                    newAssets = null;
                }
                catch (Hd3dException e)
                {
                    throw new FunctorException(e);
                }
            }
            if (CollectUtils.isNotEmpty(newGroups))
            {
                try
                {
                    for (IAssetRevisionGroup assetRevisionGroup : newGroups)
                    {
                        assetRevisionGroup.doPrePersist();
                    }
                    Persistors.assetrevisiongroup.persistCollection(newGroups);
                    for (IAssetRevisionGroup assetRevisionGroup : newGroups)
                    {
                        assetRevisionGroup.doPostPersist();
                    }
                    CollectUtils.safeClear(newGroups);
                    newGroups = null;
                }
                catch (Hd3dException e)
                {
                    throw new FunctorException(e);
                }
            }
        }
    }

    public static void createForSteps(final List<IStep> steps) throws Hd3dPersistenceException
    {
        List<IStep> filteredSteps = null;
        MultiMap<String, Long> woIdsMap = null;
        Map<IStep, IBase> stepToWoMap = null;

        if (Conf.createTaskForStep())
        {
            filteredSteps = filterStepsForTasksCreation(steps);
            woIdsMap = collectWorkObjectIds(filteredSteps);
            stepToWoMap = reorderByStep(filteredSteps, collectWorkObjects(woIdsMap));

            createTasks(steps, filteredSteps, woIdsMap, stepToWoMap);
        }

        /*-----------------------------------------------------*/
        /* bulk create assetrevision groups and assetrevisions */
        /*-----------------------------------------------------*/
        if (Conf.createAssetForStep())
        {
            if (filteredSteps == null)
            {
                filteredSteps = filterStepsForTasksCreation(steps);
            }
            if (woIdsMap == null)
            {
                woIdsMap = collectWorkObjectIds(filteredSteps);
            }
            if (stepToWoMap == null)
            {
                stepToWoMap = reorderByStep(filteredSteps, collectWorkObjects(woIdsMap));
            }

            createAssetGroups(woIdsMap, stepToWoMap);
        }

        /* cleaning */
        CollectUtils.safeClear(filteredSteps);
        filteredSteps = null;

        CollectUtils.safeClear(woIdsMap);
        woIdsMap = null;

        CollectUtils.safeClear(stepToWoMap);
        stepToWoMap = null;
    }

    private static IAssetRevisionGroup createAssetRevisionGroup(final IStep step, final IBase wo)
    {
        IAssetRevisionGroup ret = null;

        final String name = getHookFromWo(wo);
        if (name != null)
        {
            ret = new AssetRevisionGroupH();
            ret.setName(wo.getId() + '_' + name + "_GRP" + BaseH.getUUID());
            ret.setProject(getProjectFromWo(wo));
            ret.setCriteria(wo.entityName());
            ret.setValue(String.valueOf(wo.getId()));
        }

        return ret;
    }

    private static IAssetRevision createAssetRevision(final IStep step, final IBase wo, final IAssetRevisionGroup group)
    {
        IAssetRevision ret = null;

        final String name = getDefaultAssetRevisionName(step);
        if (name != null)
        {
            ret = new AssetRevisionH();
            ret.setName(name);
            ret.setKey(getHookFromWo(wo) + '_' + name + BaseH.getUUID());
            ret.setProject(getProjectFromWo(wo));
            ret.setStatus(EAssetStatus.UNVALIDATED);
            ret.setValidity("PROD");
            ret.setTaskType(step.getTaskType());
            ret.setVariation("default");
            ret.addAssetRevisionGroup(group);
        }

        return ret;
    }

    private static boolean isOkForTaskCreation(final IStep step)
    {
        return step != null && step.getTaskType() != null && (step.getEstimatedDuration().intValue() > 0);
    }

    private static boolean isOkForTaskUpdate(final IStep step)
    {
        return step != null && step.getTaskType() != null && (step.getEstimatedDuration().intValue() > 0);
    }

    private static boolean isOkForTaskDeletion(final IStep step)
    {
        return isOkForTaskUpdate(step);
    }

    public static void updateTasksForStep(final IStep step) throws Hd3dPersistenceException
    {
        if (Conf.createTaskForStep() && isOkForTaskCreation(step))
        {
            List<IStep> steps = new ArrayList<IStep>(1);
            steps.add(step);
            updateTasksForSteps(steps);
            CollectUtils.safeClear(steps);
            steps = null;
        }
    }

    public static void updateTasksForSteps(final List<IStep> steps) throws Hd3dPersistenceException
    {
        List<IStep> notNullSteps = (List<IStep>) CollectUtils.selectNotNull(steps);

        if (Conf.createTaskForStep() && CollectUtils.isNotEmpty(notNullSteps))
        {
            /*--------------------------------------
             *  find out the work objects to handle 
             --------------------------------------*/
            List<IStep> filteredSteps = filterStepsForTasksUpdate(steps);
            MultiMap<String, Long> woIdsMap = collectWorkObjectIds(filteredSteps);
            Map<IStep, IBase> stepToWoMap = reorderByStep(filteredSteps, collectWorkObjects(woIdsMap));

            /*-
             *----------------------------------------------------------------------------
             * bulk update tasks 
             * If task exists and step's estimated duration > 0, update the duration field 
             * If task exists and step's estimated duration = 0, delete the task
             * If task does not exist and step's estimated duration > 0, create it 
             *----------------------------------------------------------------------------*/
            final MultiMap<IStep, ITask> stepTasksMap = buildStepTaskMap(filteredSteps, woIdsMap);

            if (CollectUtils.isNotEmpty(stepTasksMap))
            {
                CollectUtils.doByPartition(new ArrayList<Entry<IStep, Collection<ITask>>>(stepTasksMap.entrySet()),
                        HibernateUtil.JDBC_BATCHSIZE, new UpdateTaskClosure(stepToWoMap));
            }

            /* cleaning */
            CollectUtils.safeClear(filteredSteps);
            filteredSteps = null;

            CollectUtils.safeClear(woIdsMap);
            woIdsMap = null;

            CollectUtils.safeClear(stepToWoMap);
            stepToWoMap = null;
        }

        /* clean */
        CollectUtils.safeClear(notNullSteps);
        notNullSteps = null;
    }

    public static class UpdateTaskClosure implements Closure<List<Entry<IStep, Collection<ITask>>>>
    {
        final Map<IStep, IBase> stepToWoMap;

        public UpdateTaskClosure(Map<IStep, IBase> stepToWoMap)
        {
            this.stepToWoMap = stepToWoMap;
        }

        public void execute(List<Entry<IStep, Collection<ITask>>> partition) throws FunctorException
        {
            /* new tasks to create */
            List<ITask> newTasks = new ArrayList<ITask>(partition.size());
            /* tasks to update */
            List<ITask> tasksToUpdate = new ArrayList<ITask>(partition.size());
            /* tasks to delete */
            List<ITask> tasksToDelete = new ArrayList<ITask>(partition.size());
            IStep step;
            Collection<ITask> tasks;
            for (final Entry<IStep, Collection<ITask>> entry : partition)
            {
                step = entry.getKey();
                tasks = entry.getValue();

                /* in POST method, if a step has no task, create it */
                if (CollectUtils.isNotEmpty(tasks))
                {
                    IBase workObj;
                    String name;
                    for (final ITask task : tasks)
                    {
                        /* task exists */
                        if (task != null)
                        {
                            /* step estimated duration > 0, update the task */
                            if (step.getEstimatedDuration() > 0)
                            {
                                task.setDuration(Long.valueOf(step.getEstimatedDuration()));
                                tasksToUpdate.add(task);
                            }
                            /* step estimated duration <=0 , delete the task */
                            else
                            {
                                tasksToDelete.add(task);
                            }
                        }
                        /* task does not exist, create it */
                        else
                        {
                            workObj = stepToWoMap.get(step);
                            name = getHookFromWo(workObj);

                            if (name != null)
                            {
                                ITask newTask;
                                try
                                {
                                    newTask = createNewTask(step, workObj);
                                    if (newTask != null)
                                    {
                                        newTasks.add(newTask);
                                    }
                                }
                                catch (Hd3dException e)
                                {
                                    throw new FunctorException(e);
                                }

                            }
                        }
                    }
                }
            }

            /* Persist */
            if (CollectUtils.isNotEmpty(tasksToUpdate))
            {
                try
                {
                    for (ITask task : tasksToUpdate)
                    {
                        task.doPreUpdate();
                    }
                    Persistors.task.mergeCollection(tasksToUpdate);
                    for (ITask task : tasksToUpdate)
                    {
                        task.doPostUpdate();
                    }
                    CollectUtils.safeClear(tasksToUpdate);
                    tasksToUpdate = null;
                }
                catch (Hd3dException e)
                {
                    throw new FunctorException(e);
                }
            }
            if (CollectUtils.isNotEmpty(newTasks))
            {
                try
                {
                    for (ITask task : newTasks)
                    {
                        task.doPrePersist();
                    }
                    Persistors.task.persistCollection(newTasks);
                    for (ITask task : newTasks)
                    {
                        task.doPostPersist();
                    }
                    CollectUtils.safeClear(newTasks);
                    newTasks = null;
                }
                catch (Hd3dException e)
                {
                    throw new FunctorException(e);
                }
            }
            if (CollectUtils.isNotEmpty(tasksToDelete))
            {
                try
                {
                    Persistors.task.disableCollection(tasksToDelete);
                    CollectUtils.safeClear(tasksToDelete);
                    tasksToDelete = null;
                }
                catch (Hd3dException e)
                {
                    throw new FunctorException(e);
                }
            }
        }
    }

    @SuppressWarnings("serial")
    public static void deleteTasksForStep(final IStep step) throws Hd3dPersistenceException
    {
        if (Conf.createTaskForStep() && isOkForTaskDeletion(step))
        {
            deleteTasksForSteps(new ArrayList<IStep>() {
                {
                    add(step);
                }
            });
        }
    }

    public static void deleteTasksForSteps(final List<IStep> steps) throws Hd3dPersistenceException
    {
        List<IStep> notNullSteps = (List<IStep>) CollectUtils.selectNotNull(steps);

        if (Conf.createTaskForStep() && CollectUtils.isNotEmpty(notNullSteps))
        {
            /*--------------------------------------
             *  find out the work objects to handle 
             --------------------------------------*/
            final List<IStep> filteredSteps = filterStepsForTasksDeletion(notNullSteps);

            final MultiMap<String, Long> woIdsMap = collectWorkObjectIds(filteredSteps);

            /*---------------------------------------*/
            /* bulk create tasks and entityTaskLinks */
            /* If task exists, nothing to do */
            /* If task does not exist, create it */
            /*---------------------------------------*/
            final MultiMap<IStep, ITask> stepTasksMap = buildStepTaskMap(filteredSteps, woIdsMap);

            if (CollectUtils.isNotEmpty(stepTasksMap))
            {
                CollectUtils.doByPartition(new ArrayList<Entry<IStep, Collection<ITask>>>(stepTasksMap.entrySet()),
                        HibernateUtil.JDBC_BATCHSIZE, new DeleteTaskClosure());
            }
        }
    }

    public static class DeleteTaskClosure implements Closure<List<Entry<IStep, Collection<ITask>>>>
    {
        public void execute(List<Entry<IStep, Collection<ITask>>> partition) throws FunctorException
        {
            for (final Entry<IStep, Collection<ITask>> entry : partition)
            {
                Collection<ITask> tasksToDelete = CollectUtils.selectNotNull(entry.getValue());

                if (CollectUtils.isNotEmpty(tasksToDelete))
                {
                    try
                    {
                        Persistors.task.disableCollection(tasksToDelete);
                        CollectUtils.safeClear(tasksToDelete);
                        tasksToDelete = null;
                    }
                    catch (Hd3dException e)
                    {
                        throw new FunctorException(e);
                    }
                }
            }
        }
    }
}
