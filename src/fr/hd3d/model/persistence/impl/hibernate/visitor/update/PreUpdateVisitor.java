package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import java.io.Serializable;

import org.hibernate.type.Type;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevisionGroup;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.lightweight.ILClassDynMetaDataType;
import fr.hd3d.model.lightweight.ILComposition;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.lightweight.ILDynMetaDataType;
import fr.hd3d.model.lightweight.ILFileAttribute;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.lightweight.ILItemGroup;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.lightweight.ILSheet;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.lightweight.ILTagCategory;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionFileRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IConstituentLink;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IEvent;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IFact;
import fr.hd3d.model.persistence.IFactType;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IFileRevisionAnnotation;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.IHint;
import fr.hd3d.model.persistence.IInventoryItem;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskBase;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.impl.hibernate.visitor.ApprovalNoteHandler;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.TaskHandler;
import fr.hd3d.model.persistence.impl.hibernate.visitor.persist.WorkingTimePrePersistChecker;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.services.security.dao.IUserAccount;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class PreUpdateVisitor extends BasePersistenceVisitor
{
    private ILBase lObject;

    public PreUpdateVisitor(ILBase lObject)
    {
        super();
        this.lObject = lObject;
    }

    public PreUpdateVisitor()
    {
        super();
    }

    /* TODO to refine */
    public boolean visit(Object object, Serializable id, Object[] currentState, Object[] previousState,
            String[] propertyNames, Type[] types) throws Hd3dException
    {
        if (object instanceof ITask)
        {
            return visit((ITask) object, id, currentState, previousState, propertyNames, types);
        }
        else if (object instanceof IApprovalNote)
        {
            return visit((IApprovalNote) object, id, currentState, previousState, propertyNames, types);
        }

        return false;
    }

    public void visit(IBase object) throws Hd3dException
    {}

    public void visit(IActivity object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevision object) throws Hd3dException
    {
        visit((IBase) object);

        // postCheck(new AssetRevisionPreUpdateChecker(object, (ILAssetRevision) lObject).check());
    }

    public void visit(IAssetRevisionFileRevision object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevisionGroup object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new AssetRevisionGroupPreUpdateChecker(object, (ILAssetRevisionGroup) lObject).check());
    }

    public void visit(IAssetRevisionLink object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPerson object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new PersonPreUpdateChecker(object, (ILPerson) lObject).check());
    }

    public void visit(IPersonDay object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new PersonDayPreUpdateChecker(object, (ILPersonDay) lObject).check());
    }

    public void visit(IUserAccount object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProject object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new ProjectPreUpdateChecker(object, (ILProject) lObject).check());
    }

    public void visit(ICategory object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new CategoryPreUpdateChecker(object, (ILCategory) lObject).check());
    }

    public void visit(IConstituent object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new ConstituentPreUpdateChecker(object, (ILConstituent) lObject).check());
    }

    public void visit(ISequence object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new SequencePreUpdateChecker(object, (ILSequence) lObject).check());
    }

    public void visit(IShot object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new ShotPreUpdateChecker(object, (ILShot) lObject).check());
    }

    public void visit(IComposition object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new CompositionPreUpdateChecker(object, (ILComposition) lObject).check());
    }

    public void visit(ISheet object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new SheetPreUpdateChecker(object, (ILSheet) lObject).check());
    }

    public void visit(final IClassDynMetaDataType object) throws Hd3dException
    {
        visit((IBase) object);
        postCheck(new ClassDynMetaDataTypePreUpdateChecker(object, (ILClassDynMetaDataType) lObject).check());
    }

    public void visit(IDynMetaDataValue object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(final IDynMetaDataType object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new DynMetaDataTypePreUpdateChecker(object, (ILDynMetaDataType) lObject).check());
    }

    public void visit(IComputer object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IConstituentLink object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IDevice object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFact object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFactType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFileAttribute object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new FileAttributePreUpdateChecker(object, (ILFileAttribute) lObject).check());
    }

    public void visit(IFileRevision object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new FileRevisionPreUpdateChecker(object, (ILFileRevision) lObject).check());
    }

    public void visit(IFileRevisionLink object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IHint object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IInventoryItem object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IItem object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IItemGroup object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new ItemGroupPreUpdateChecker(object, (ILItemGroup) lObject).check());
    }

    public void visit(IPlanning object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPool object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IRole object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IResourceGroup object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IScreen object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IScript object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ISimpleActivity object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ISoftware object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IStudioMap object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITag object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITagCategory object) throws Hd3dException
    {
        postCheck(new TagCategoryPreUpdateChecker(object, (ILTagCategory) lObject).check());
    }

    public void visit(ITask object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public boolean visit(ITask object, Serializable id, Object[] currentState, Object[] previousState,
            String[] propertyNames, Type[] types) throws Hd3dException
    {
        boolean ret = false;

        if (object == null)
        {
            return ret;
        }

        visit((IBase) object);
        /* set properly actualEndDate */
        // if (object.getStatus() == ETaskStatus.WAIT_APP)
        // {
        // int index = ArrayUtils.indexOf(propertyNames, fr.hd3d.utils.Const.ACTUALENDDATE);
        // currentState[index] = new Date();
        // ret = true;
        // }
        // else
        // {
        // int statusIndex = ArrayUtils.indexOf(propertyNames, fr.hd3d.utils.Const.STATUS);
        //
        // final ETaskStatus previousStatus = previousState == null ? null : (ETaskStatus) previousState[statusIndex];
        // final ETaskStatus currentStatus = (ETaskStatus) currentState[statusIndex];
        // if (previousStatus == ETaskStatus.WAIT_APP && currentStatus != ETaskStatus.WAIT_APP
        // && currentStatus != ETaskStatus.OK && currentStatus != ETaskStatus.CLOSE)
        // {
        // int index = ArrayUtils.indexOf(propertyNames, fr.hd3d.utils.Const.ACTUALENDDATE);
        // currentState[index] = null;
        // ret = true;
        // }
        // }

        if (!object.isUpdatedByApprovalNote())
        {
            TaskHandler handler = new TaskHandler(object, id, currentState, previousState, propertyNames, types);
            handler.createApprovalNotesForTaskOnUpdate();
            handler = null;
        }

        return ret;
    }

    public void visit(ITaskActivity object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskBase object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskChanges object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskGroup object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ILicense object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IApprovalNote object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public boolean visit(IApprovalNote object, Serializable id, Object[] currentState, Object[] previousState,
            String[] propertyNames, Type[] types) throws Hd3dException
    {
        boolean ret = false;

        if (object == null)
        {
            return ret;
        }

        visit((IBase) object);

        /* update related task status */
        new ApprovalNoteHandler(object).updateApprovalNoteRelatedTasks();

        return true;
    }

    public void visit(IAbsence object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IEvent object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IMileStone object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IExtraLine object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IEntityTaskLink object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IApprovalNoteType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProjectType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IWorkingTime object) throws Hd3dException
    {
        visit((IBase) object);

        postCheck(new WorkingTimePrePersistChecker(object).check());
    }

    public void visit(IStep object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPlayListEdit object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPlayListRevision object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFileRevisionAnnotation object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IListValues object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IDynamicPath object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IGraphicalAnnotation object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IMountPoint object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProxy object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProxyType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPlayListRevisionGroup object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IHd3dACL object) throws Hd3dException
    {
        visit((IBase) object);
    }
}
