package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import static fr.hd3d.utils.Const.CLASSNAME;
import static fr.hd3d.utils.Const.NAME;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.CollectUtils;


public class ClassDynMetaDataTypePrePersistChecker extends AbstractPrePersistChecker<IClassDynMetaDataType>
{
    public ClassDynMetaDataTypePrePersistChecker(IClassDynMetaDataType object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueForSameEntity())
        {
            ret = MessageFormat.format(ERR_2, object.getName(), object.getClassName());
        }

        return ret;
    }

    private boolean isUniqueForSameEntity() throws Hd3dException
    {
        boolean ret = false;

        if (object != null)
        {
            ret = CollectUtils.isEmpty(getSimilarClassDynMetaDataTypes(object.getClassName(), object.getName()));
        }

        return ret;
    }

    public static List<IClassDynMetaDataType> getSimilarClassDynMetaDataTypes(String className, String name)
            throws Hd3dException
    {
        String[] properties = new String[] { CLASSNAME, NAME };
        Object[] values = new Object[] { className, name };
        return Persistors.classdynmetadatatype.getByValues_NoPermCheck(properties, values, "AND", LockMode.OPTIMISTIC);
    }
}
