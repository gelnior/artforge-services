package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionFileRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IConstituentLink;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IEvent;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IFact;
import fr.hd3d.model.persistence.IFactType;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IFileRevisionAnnotation;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.IHint;
import fr.hd3d.model.persistence.IInventoryItem;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskBase;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataValuesToCreate;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.visitor.ApprovalNoteHandler;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.DynMetaDataValueHandler;
import fr.hd3d.model.persistence.impl.hibernate.visitor.ProjectHandler;
import fr.hd3d.model.persistence.impl.hibernate.visitor.StepHandler;
import fr.hd3d.model.persistence.impl.hibernate.visitor.TaskHandler;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.services.security.dao.IUserAccount;
import fr.hd3d.services.security.utils.AuthorizationUtil;
import fr.hd3d.utils.HibernateUtil;


/**
 * Post Persistence business logic, applied to Business Entities
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class PostPersistVisitor extends BasePersistenceVisitor
{
    public PostPersistVisitor()
    {}

    public void visit(IBase object) throws Hd3dException
    {
        DynMetaDataValueHandler.createDynMetaDataValuesForMatchingObjects(object);
    }

    public void visit(IPerson object) throws Hd3dException
    {
        AuthorizationUtil.clearCache(); // permissions might have changed
    }

    public void visit(IUserAccount object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITag object) throws Hd3dException
    {}

    public void visit(ITagCategory object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IActivity object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevision object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevisionFileRevision object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevisionGroup object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevisionLink object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProject object) throws Hd3dException
    {
        visit((IBase) object);

        ProjectHandler.createGroupForProject(object);
        ProjectHandler.createRolesForProject(object);
        ProjectHandler.createSheetsForProject(object);
    }

    public void visit(ICategory object) throws Hd3dException
    {
        visit((IBase) object);
    }

    /**
     * IClassDynMetaDataType post persist treatment
     */
    public void visit(IClassDynMetaDataType object) throws Hd3dException
    {
        if (object == null || StringUtils.isBlank(object.getClassName()))
            return;

        BaseH.addClassDynMetaDataType(object);

        final String scope = object.getClassName();
        /* apply to a class */
        if (!"*".equals(scope))
        {
            Collection<IBase> matchedObjects = DynMetaDataValueHandler.getMatchingEntityInstances(object);

            if (!matchedObjects.isEmpty())
            {
                /* build the list of DynMetaDataValuesToCreate, they will be used later to create dynMetaDataValues */
                List<DynMetaDataValuesToCreate> valuesToCreate = new ArrayList<DynMetaDataValuesToCreate>(
                        matchedObjects.size());

                for (IBase matchedObject : matchedObjects)
                {
                    valuesToCreate.add(new DynMetaDataValuesToCreate(object, matchedObject.getId(), scope));
                }

                HibernatePersist.persistRawCollection(valuesToCreate);

                /* keep trace in memory as well */
                BaseH.DYNVALUES_TO_CREATE_SET.add(scope);
            }
        }
    }

    public void visit(IConstituent object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ISequence object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IShot object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IComposition object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ISheet object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IComputer object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IDevice object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ISoftware object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IDynMetaDataValue object) throws Hd3dException
    {}

    public void visit(IDynMetaDataType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IConstituentLink object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFact object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFactType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFileAttribute object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFileRevision object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFileRevisionLink object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IHint object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IInventoryItem object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IItem object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IItemGroup object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPlanning object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPool object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IRole object) throws Hd3dException
    {
        AuthorizationUtil.clearCache(); // permissions might have changed
    }

    public void visit(IResourceGroup object) throws Hd3dException
    {
        AuthorizationUtil.clearCache(); // permissions might have changed
    }

    public void visit(IScreen object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IScript object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ISimpleActivity object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IStudioMap object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITask object) throws Hd3dException
    {
        visit((IBase) object);

        TaskHandler taskHandler = new TaskHandler(object);
        taskHandler.createEntityTaskLinkForTask();
        taskHandler.createApprovalNotesForTaskOnCreate();
    }

    public void visit(ITaskActivity object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskBase object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskChanges object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskGroup object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ILicense object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IApprovalNote object) throws Hd3dException
    {
        visit((IBase) object);
        /*
         * when creating a new Approval Note with an ApprovalNoteType(related to a TaskType), update the related
         * tasks'status with the status of the Approval Note
         */
        new ApprovalNoteHandler(object).updateApprovalNoteRelatedTasks();
    }

    public void visit(IAbsence object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IEvent object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IMileStone object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IExtraLine object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IEntityTaskLink object) throws Hd3dException
    {
        visit((IBase) object);

        /* re-index bound Tasks */
        /* Note: DO NOT open and close transaction here, it is done in AbstractHd3dCollectionResource */
        FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
        fullTextSession.index(object.getTask());
        fullTextSession.flushToIndexes();
    }

    public void visit(IApprovalNoteType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProjectType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IWorkingTime object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IStep object) throws Hd3dException
    {
        visit((IBase) object);
        /* create task list */
        StepHandler.createTasksForStep(object);
    }

    public void visit(List<IStep> objects) throws Hd3dException
    {
        for (IStep object : objects)
        {
            visit((IBase) object);
        }

        /* create task list */
        StepHandler.createTasksForSteps(objects);
    }

    public void visit(IPlayListEdit object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPlayListRevision object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFileRevisionAnnotation object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IListValues object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IDynamicPath object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IGraphicalAnnotation object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IMountPoint object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProxyType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProxy object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPlayListRevisionGroup object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IHd3dACL object) throws Hd3dException
    {
        visit((IBase) object);
    }
}
