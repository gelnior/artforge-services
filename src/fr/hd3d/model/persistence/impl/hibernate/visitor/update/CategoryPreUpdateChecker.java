package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import static fr.hd3d.utils.Const.ID;
import static fr.hd3d.utils.Const.NAME;
import static fr.hd3d.utils.Const.PARENT;
import static fr.hd3d.utils.Const.PROJECT;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class CategoryPreUpdateChecker extends AbstractPreUpdateChecker<ILCategory, ICategory>
{
    public CategoryPreUpdateChecker(ICategory object, ILCategory light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueWithinProject())
        {
            ret = MessageFormat.format(ERR_4, light.getName());
        }

        return ret;
    }

    private boolean isUniqueWithinProject() throws Hd3dException
    {
        boolean ret = true;

        // if the category does not (yet) belong to a project, no problem
        if (light != null
                && light.getProject() != null
                && (!object.getProject().getId().equals(light.getProject())
                        || !object.getParent().getId().equals(light.getParent()) || !object.getName().equals(
                        light.getName())))
        {
            // Within a project, a category's name must be unique
            List<ICategory> categories;

            class GetCollectionCmd implements IHibernateQuery
            {
                List<ICategory> result;

                @SuppressWarnings("unchecked")
                public void execute(Session session) throws Hd3dException
                {
                    HibernatePersist catpersistor = (HibernatePersist) Persistors.category;
                    IPersist<IProject> projectpersistor = Persistors.project;

                    // DetachedCriteria subQuery = catpersistor.basicSubCriteria(session);

                    // Note: DO NOT APPLY ANY FILTER

                    final Criteria query = catpersistor.basicCriteria(session);
                    query.setLockMode(LockMode.OPTIMISTIC);// VERY IMPORTANT
                    query.add(Restrictions.ne(ID, light.getId()));
                    query.add(Restrictions.eq(PROJECT, projectpersistor.getById(light.getProject())));
                    query.add(Restrictions.eq(PARENT, catpersistor.getById(light.getParent())));
                    query.add(Restrictions.eq(NAME, light.getName()));

                    result = query.list();
                }
            }

            GetCollectionCmd cmd = new GetCollectionCmd();
            HibernateUtil.safeQuery(cmd);
            categories = cmd.result;

            ret = CollectUtils.isEmpty(categories);
        }

        return ret;
    }
}
