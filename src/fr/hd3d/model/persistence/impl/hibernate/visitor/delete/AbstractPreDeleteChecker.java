package fr.hd3d.model.persistence.impl.hibernate.visitor.delete;

import fr.hd3d.exception.Hd3dException;


/**
 * 
 * 
 * IMPORTANT NOTE: <br>
 * It is not possible to do things such as:<br>
 * Ex: for IConstituent to be created<br>
 * Set<IConstituent> constituents = object.getCategory().getConstituents();<br>
 * for (IConstituent c : constituents) <br>
 * { <br>
 * tests... <br>
 * } since object (to be created) is not yet persisted, any method call on it would be inconsistent.<br>
 * One must use HibernateUtil.SafeQuery instead, or whatever other way to do a "fresh query"
 * 
 * @author try.lam
 * 
 */
public abstract class AbstractPreDeleteChecker<P>
{
    public static String ERR_1 = "There are still {0} tasks activities with duration > 0 referencing the task type (id={1})";
    public static String ERR_2 = "There are still {0} approval notes referencing the approval note type (id={1})";

    protected P object;

    public AbstractPreDeleteChecker(P object)
    {
        this.object = object;
    }

    public abstract String check() throws Hd3dException;
}
