package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import static fr.hd3d.utils.Const.ID;
import static fr.hd3d.utils.Const.LABEL;
import static fr.hd3d.utils.Const.SEQUENCE;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class ShotPreUpdateChecker extends AbstractPreUpdateChecker<ILShot, IShot>
{
    public ShotPreUpdateChecker(IShot object, ILShot light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueWithinSequence())
        {
            ret = MessageFormat.format(ERR_7, light.getLabel());
        }

        return ret;
    }

    private boolean isUniqueWithinSequence() throws Hd3dException
    {
        boolean ret = true;

        // if the shot does not (yet) belong to a sequence, no problem
        if (light != null
                && light.getSequence() != null
                && (!object.getSequence().getId().equals(light.getSequence()) || !object.getLabel().equals(
                        light.getLabel())))
        {
            // Within a sequence, a shot label or hook must be unique
            List<IShot> shots;

            class GetCollectionCmd implements IHibernateQuery
            {
                List<IShot> result;

                @SuppressWarnings("unchecked")
                public void execute(Session session) throws Hd3dException
                {
                    HibernatePersist persistor = (HibernatePersist) Persistors.shot;
                    IPersist<ISequence> seqpersistor = Persistors.sequence;
                    // DetachedCriteria subQuery = persistor.basicSubCriteria(session);

                    // Note: DO NOT APPLY ANY FILTER

                    final Criteria query = persistor.basicCriteria(session);
                    query.setLockMode(LockMode.OPTIMISTIC);// VERY IMPORTANT
                    query.add(Restrictions.ne(ID, light.getId()));
                    query.add(Restrictions.eq(SEQUENCE, seqpersistor.getById(light.getSequence())));
                    query.add(Restrictions.eq(LABEL, light.getLabel()));

                    result = query.list();
                }
            }

            GetCollectionCmd cmd = new GetCollectionCmd();
            HibernateUtil.safeQuery(cmd);
            shots = cmd.result;

            ret = CollectUtils.isEmpty(shots);
        }

        return ret;
    }
}
