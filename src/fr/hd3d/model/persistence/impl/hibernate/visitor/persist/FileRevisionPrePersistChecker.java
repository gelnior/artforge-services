package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import static fr.hd3d.utils.Const.KEY;
import static fr.hd3d.utils.Const.REVISION;
import static fr.hd3d.utils.Const.VARIATION;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class FileRevisionPrePersistChecker extends AbstractPrePersistChecker<IFileRevision>
{
    public FileRevisionPrePersistChecker(IFileRevision object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isTripletUnique())
        {
            ret = MessageFormat.format(ERR_11, object.getKey(), object.getVariation(), object.getRevision());
        }

        if (!isFullPathUnique())
        {
            ret += MessageFormat.format(ERR_12, object.getFullPath());
        }

        return ret;
    }

    private boolean isTripletUnique() throws Hd3dException
    {
        boolean ret = true;

        if (object != null)
        {
            // ===============================================================
            // 1st check
            // Independently from projects,
            // the triplet (key,variation,revision) must be unique
            // ===============================================================
            List<IFileRevision> filerevisions;

            class GetCollectionCmd implements IHibernateQuery
            {
                List<IFileRevision> result;

                @SuppressWarnings("unchecked")
                public void execute(Session session) throws Hd3dException
                {
                    HibernatePersist persistor = (HibernatePersist) Persistors.filerevision;
                    // DetachedCriteria subQuery = persistor.basicSubCriteria(session);

                    // Note: DO NOT APPLY ANY FILTER

                    final Criteria query = persistor.basicCriteria(session);
                    query.setLockMode(LockMode.OPTIMISTIC);// VERY IMPORTANT
                    query.add(Restrictions.eq(KEY, object.getKey()));
                    if (object.getVariation() != null)
                        query.add(Restrictions.eq(VARIATION, object.getVariation()));
                    else
                        query.add(Restrictions.isNull(VARIATION));
                    query.add(Restrictions.eq(REVISION, object.getRevision()));
                    query.add(Restrictions.eq("assetRevision", object.getAssetRevision()));

                    result = query.list();
                }
            }

            GetCollectionCmd cmd = new GetCollectionCmd();
            HibernateUtil.safeQuery(cmd);
            filerevisions = cmd.result;

            ret = CollectUtils.isEmpty(filerevisions);
        }

        return ret;
    }

    private boolean isFullPathUnique() throws Hd3dPersistenceException
    {
        boolean ret = true;

        // ===============================================================
        // 2nd check
        // location + relativePath must be unique
        // ===============================================================
        BooleanQuery bq = new BooleanQuery();

        TermQuery internalStatusTq = new TermQuery(new Term("internalstatus", "0"));
        bq.add(new BooleanClause(internalStatusTq, BooleanClause.Occur.MUST));

        TermQuery fullPathTq = new TermQuery(new Term("fullpath", object.getFullPath()));
        bq.add(new BooleanClause(fullPathTq, BooleanClause.Occur.MUST));

        FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());

        org.hibernate.Query hibQuery = fullTextSession.createFullTextQuery(bq, FileRevisionH.class);

        List<IFileRevision> result = hibQuery.list();

        ret = CollectionUtils.isEmpty(result);

        return ret;
    }
}
