package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import static fr.hd3d.utils.Const.ID;
import static fr.hd3d.utils.Const.KEY;
import static fr.hd3d.utils.Const.NAME;
import static fr.hd3d.utils.Const.PROJECT;
import static fr.hd3d.utils.Const.REVISION;
import static fr.hd3d.utils.Const.VARIATION;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class AssetRevisionPreUpdateChecker extends AbstractPreUpdateChecker<ILAssetRevision, IAssetRevision>
{
    public AssetRevisionPreUpdateChecker(IAssetRevision object, ILAssetRevision light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueWithinProject())
        {
            ret = MessageFormat.format(ERR_10, object.getName(), object.getKey(), object.getVariation(), object
                    .getRevision());
        }

        return ret;
    }

    private boolean isUniqueWithinProject() throws Hd3dException
    {
        boolean ret = true;

        if (object != null && object.getProject() != null && light != null)
        {
            // within a project, an AssetRevision name must be unique
            // the triplet (key,variation,revision) must be unique as well
            List<IAssetRevision> assetrevisions;

            class GetCollectionCmd implements IHibernateQuery
            {
                List<IAssetRevision> result;

                @SuppressWarnings("unchecked")
                public void execute(Session session) throws Hd3dException
                {
                    HibernatePersist persistor = (HibernatePersist) Persistors.assetrevision;
                    // DetachedCriteria subQuery = persistor.basicSubCriteria(session);

                    // Note: DO NOT APPLY ANY FILTER

                    final Criteria query = persistor.basicCriteria(session);
                    query.setLockMode(LockMode.OPTIMISTIC);// VERY IMPORTANT
                    query.add(Restrictions.ne(ID, object.getId()));

                    Conjunction sameNameInProject = Restrictions.conjunction();
                    IProject project = Persistors.project.getById(light.getProject());
                    sameNameInProject.add(light.getProject() != null ? Restrictions.eq(PROJECT, project) : Restrictions
                            .isNull(PROJECT));
                    sameNameInProject.add(Restrictions.eq(NAME, light.getName()));
                    // Same name BUT different KEY/VARIATION, or else other revisions of the same assets will be
                    // returned
                    Disjunction or = Restrictions.disjunction();
                    or.add(Restrictions.ne(KEY, light.getKey()));
                    or.add(Restrictions.ne(VARIATION, light.getVariation()));
                    sameNameInProject.add(or);

                    Conjunction sameIdentifier = Restrictions.conjunction();
                    sameIdentifier.add(Restrictions.eq(KEY, light.getKey()));
                    sameIdentifier.add(Restrictions.eq(VARIATION, light.getVariation()));
                    sameIdentifier.add(Restrictions.eq(REVISION, light.getRevision()));

                    query.add(Restrictions.or(sameNameInProject, sameIdentifier));

                    result = query.list();
                }
            }

            GetCollectionCmd cmd = new GetCollectionCmd();
            HibernateUtil.safeQuery(cmd);
            assetrevisions = cmd.result;

            ret = CollectUtils.isEmpty(assetrevisions);
        }

        return ret;
    }
}
