package fr.hd3d.model.persistence.impl.hibernate.visitor.delete;

import java.text.MessageFormat;
import java.util.List;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;


public class TaskTypePreDeleteChecker extends AbstractPreDeleteChecker<ITaskType>
{
    public TaskTypePreDeleteChecker(ITaskType object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        final int numberOfReferencesOnThis = nbReferences();
        if (numberOfReferencesOnThis > 0)
        {
            ret = MessageFormat.format(ERR_1, numberOfReferencesOnThis, object.getId());
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    private int nbReferences() throws Hd3dException
    {
        String hql = "from " + Const.CANONICALNAME_TASKACTIVITY + " ta where ta.task.taskType.id=" + object.getId()
                + " and ta.duration is not null and ta.duration > 0 " + " and ta.internalStatus=0";
        List<ITaskActivity> taskActivities = HibernateUtil.currentSession().createQuery(hql).list();
        return taskActivities.size();
    }
}
