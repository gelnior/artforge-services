package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import java.text.MessageFormat;
import java.util.List;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.persist.AbstractPrePersistChecker;
import fr.hd3d.utils.CollectUtils;


public class PersonDayPreUpdateChecker extends AbstractPreUpdateChecker<ILPersonDay, IPersonDay>
{
    public PersonDayPreUpdateChecker(IPersonDay object, ILPersonDay light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (alreadyExist())
        {
            ret = MessageFormat.format(AbstractPrePersistChecker.ERR_23, light.getPerson().getLogin(), light.getDate());
        }

        return ret;
    }

    private boolean alreadyExist() throws Hd3dException
    {
        if (!object.getPerson().getId().equals(light.getPerson().getId()) || !object.getDate().equals(light.getDate()))
        {
            final String[] properties = new String[] { "person.id", "date" };
            final Object[] values = new Object[] { light.getPerson().getId(), light.getDate() };
            List<IPersonDay> personDays = Persistors.personDay.getByValues(properties, values, "AND");
            if (CollectUtils.isNotEmpty(personDays))
            {
                return true;
            }
            else
            {
                for (IPersonDay personDay : personDays)
                {
                    if (personDay.getId().equals(object.getId()))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
