package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import java.text.MessageFormat;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.Persistors;


public class PersonDayPrePersistChecker extends AbstractPrePersistChecker<IPersonDay>
{
    public PersonDayPrePersistChecker(IPersonDay object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (alreadyExist())
        {
            ret = MessageFormat.format(ERR_23, object.getPerson().getLogin(), object.getDate());
        }

        return ret;
    }

    private boolean alreadyExist() throws Hd3dException
    {
        final String[] properties = new String[] { "person", "date" };
        final Object[] values = new Object[] { object.getPerson(), object.getDate() };
        return Persistors.personDay.getByValues(properties, values, "AND") != null;
    }
}
