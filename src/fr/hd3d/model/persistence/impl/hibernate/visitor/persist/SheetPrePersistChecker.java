package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import static fr.hd3d.utils.Const.NAME;
import static fr.hd3d.utils.Const.PROJECT;
import static fr.hd3d.utils.Const._ID;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.CollectUtils;


public class SheetPrePersistChecker extends AbstractPrePersistChecker<ISheet>
{
    public SheetPrePersistChecker(ISheet object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueWithinProject())
        {
            ret = MessageFormat.format(ERR_9, object.getName());
        }

        return ret;
    }

    private boolean isUniqueWithinProject() throws Hd3dException
    {
        boolean ret = false;

        if (object != null)
        {
            ret = CollectUtils
                    .isEmpty(getSimilarSheets(CollectUtils.nullSafeId(object.getProject()), object.getName()));
        }

        return ret;
    }

    public static List<ISheet> getSimilarSheets(Long projectId, String name) throws Hd3dException
    {
        String[] properties = new String[] { PROJECT + _ID, NAME };
        Object[] values = new Object[] { projectId, name };
        return Persistors.sheet.getByValues_NoPermCheck(properties, values, "AND", LockMode.OPTIMISTIC);
    }
}
