package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.CollectionUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.impl.hibernate.visitor.persist.ResourceGroupPrePersistChecker;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.predicate.EqIdPredicate;


public class ResourceGroupPreUpdateChecker extends AbstractPreUpdateChecker<ILResourceGroup, IResourceGroup>
{
    public ResourceGroupPreUpdateChecker(IResourceGroup object, ILResourceGroup light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUnique())
        {
            ret = MessageFormat.format(ERR_16, light.getName());
        }

        return ret;
    }

    private boolean isUnique() throws Hd3dException
    {
        boolean ret = true;

        if (light != null && light.getName() != null && !object.getName().equals(light.getName()))
        {
            List<IResourceGroup> assetRevisionGroups = ResourceGroupPrePersistChecker.getSimilarResourceGroups(light
                    .getName());

            Collection<IResourceGroup> differentIdResourceGroups = CollectionUtils.selectRejected(assetRevisionGroups,
                    new EqIdPredicate<IResourceGroup>(light.getId()));

            ret = CollectUtils.isEmpty(differentIdResourceGroups);
        }

        return ret;
    }
}
