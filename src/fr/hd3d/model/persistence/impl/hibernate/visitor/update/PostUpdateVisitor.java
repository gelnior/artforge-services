package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionFileRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IConstituentLink;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IEvent;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IFact;
import fr.hd3d.model.persistence.IFactType;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IFileRevisionAnnotation;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.IHint;
import fr.hd3d.model.persistence.IInventoryItem;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskBase;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.DynMetaDataValueHandler;
import fr.hd3d.model.persistence.impl.hibernate.visitor.ProjectHandler;
import fr.hd3d.model.persistence.impl.hibernate.visitor.StepHandler;
import fr.hd3d.model.persistence.impl.hibernate.visitor.TaskHandler;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.services.security.dao.IUserAccount;
import fr.hd3d.services.security.utils.AuthorizationUtil;
import fr.hd3d.services.security.utils.UserAccountsUtil;
import fr.hd3d.utils.HibernateUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class PostUpdateVisitor extends BasePersistenceVisitor
{
    public PostUpdateVisitor()
    {}

    public void visit(IBase object) throws Hd3dException
    {
        // update the dyn values
        for (IDynMetaDataValue dyn : object.getDynMetaDataValues())
        {
            visit(dyn);
        }
        // disable then the non-matching dyn values
        // NOTE: better disable after update to avoid not allowed dyn values creation
        DynMetaDataValueHandler.disableDynMetaDataValuesForNonMatchingObjects(object);
        TaskHandler.reIndexBoundTask(object);
    }

    public void visit(IActivity object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevision object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevisionFileRevision object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevisionGroup object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevisionLink object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPerson object) throws Hd3dException
    {
        visit((IBase) object);

        String login = object.getLogin();
        if (UserAccountsUtil.getUserAccountStatus(login).longValue() == -1) // user now has no account
        {
            UserAccountsUtil.removeOrphanAccounts(); // user account might have become orphan
        }
        AuthorizationUtil.clearCache(); // user's permissions might have changed
    }

    public void visit(IUserAccount object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProject object) throws Hd3dException
    {
        visit((IBase) object);

        ProjectHandler.updateTaskTypeRelatedData(object);
    }

    public void visit(ICategory object) throws Hd3dException
    {
        visit((IBase) object);
        /* re-index children constituents */
        Collection<ICategory> children = object.allChildren(true);
        List<IConstituent> childrenConstituents = new ArrayList<IConstituent>();
        for (ICategory child : children)
        {
            childrenConstituents.addAll(child.getConstituents());
        }
        HibernateUtil.indexCollection(childrenConstituents);
    }

    public void visit(IConstituent object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ISequence object) throws Hd3dException
    {
        visit((IBase) object);
        /* re-index children shots */
        Collection<ISequence> children = object.allChildren(true);
        List<IShot> childrenShots = new ArrayList<IShot>();
        for (ISequence child : children)
        {
            childrenShots.addAll(child.getShots());
        }
        HibernateUtil.indexCollection(childrenShots);
    }

    public void visit(IShot object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IComposition object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ISheet object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IClassDynMetaDataType object) throws Hd3dException
    {
        BaseH.updateClassDynMetaDataType(object);
        DynMetaDataValueHandler.createDynMetaDataValuesForMatchingObjects(object);
        DynMetaDataValueHandler.disableDynMetaDataValuesForNonMatchingObjects(object);
    }

    public void visit(IDynMetaDataValue object) throws Hd3dException
    {
    // if nature, type or default value changed, find out related DynMetaDataValues and change their value to
    // new default value
    // String value = object.getValue();
    // if (value)
    }

    public void visit(IDynMetaDataType object) throws Hd3dException
    {}

    public void visit(IComputer object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IConstituentLink object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IDevice object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFact object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFactType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFileAttribute object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFileRevision object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFileRevisionLink object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IHint object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IInventoryItem object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IItem object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IItemGroup object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPlanning object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPool object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IRole object) throws Hd3dException
    {
        visit((IBase) object);

        AuthorizationUtil.clearCache(); // role's permissions might have changed
    }

    public void visit(IResourceGroup object) throws Hd3dException
    {
        visit((IBase) object);

        AuthorizationUtil.clearCache(); // role's permissions might have changed
    }

    public void visit(IScreen object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IScript object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ISimpleActivity object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ISoftware object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IStudioMap object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITag object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITagCategory object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITask object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskActivity object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskBase object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskChanges object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskGroup object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ILicense object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IApprovalNote object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IAbsence object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IEvent object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IMileStone object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IExtraLine object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(ITaskType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IEntityTaskLink object) throws Hd3dException
    {
        visit((IBase) object);

        /* re-index bound Tasks */
        /* Note: DO NOT open and close transaction here, it is done in AbstractHd3dCollectionResource */
        FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
        fullTextSession.index(object.getTask());
        fullTextSession.flushToIndexes();
    }

    public void visit(IApprovalNoteType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProjectType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IWorkingTime object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IStep object) throws Hd3dException
    {
        visit((IBase) object);
        /* update task list */
        StepHandler.updateTasksForStep(object);
    }

    public void visit(List<IStep> objects) throws Hd3dException
    {
        for (IStep object : objects)
        {
            visit((IBase) object);
        }

        /* update task list */
        StepHandler.updateTasksForSteps(objects);
    }

    public void visit(IPlayListEdit object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPlayListRevision object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IFileRevisionAnnotation object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IListValues object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IDynamicPath object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IGraphicalAnnotation object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IMountPoint object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProxyType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProxy object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IPlayListRevisionGroup object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IHd3dACL object) throws Hd3dException
    {
        visit((IBase) object);
    }
}
