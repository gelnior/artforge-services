package fr.hd3d.model.persistence.impl.hibernate.visitor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ClassDynMetaDataTypeH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataValueH;
import fr.hd3d.model.persistence.impl.hibernate.query.AbstractHibernateQuery;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;


public class DynMetaDataValueHandler
{
    final private static String FROM = "from ";
    final private static String ALL = "*";
    final private static String WHERE = " where ";

    public static void remove(final IDynMetaDataValue value) throws Hd3dException
    {
        if (value != null)
        {
            value.disable();
        }
    }

    public static void remove(final Collection<IDynMetaDataValue> values) throws Hd3dException
    {
        CollectUtils.removeNull(values);
        if (CollectUtils.isNotEmpty(values))
        {
            for (IDynMetaDataValue v : values)
            {
                remove(v);
            }
        }
    }

    /**
     * For each business entity, creates the DynMetaDataValues that apply to it (the business entity's type and
     * predicate match)
     * 
     * @param object
     * @throws Hd3dPersistenceException
     */
    public static void createDynMetaDataValuesForMatchingObjects(final IBase object) throws Hd3dException
    {
        if (object != null)
        {
            /* list of all related ClassDynMetaDataType */
            final List<IClassDynMetaDataType> classDynMetaDataTypes = getRelatedClassDynMetaDataTypes(object);

            if (CollectUtils.isNotEmpty(classDynMetaDataTypes))
            {
                /* filter it by predicate. Only keep the IClassDynMetaDataType that apply to the given object */
                final Collection<IClassDynMetaDataType> matchingClassDynMetaDataTypes = org.apache.commons.collections15.CollectionUtils
                        .select(classDynMetaDataTypes, new ClassDynMetaDataTypePredicate(object));

                if (CollectUtils.isNotEmpty(matchingClassDynMetaDataTypes))
                {
                    /*
                     * if a DynMetaDataValue of this object is not already bound to a ClassDynMetaDataType of this
                     * object, create it
                     */
                    for (IClassDynMetaDataType c : matchingClassDynMetaDataTypes)
                    {
                        IDynMetaDataValue dynValue = new DynMetaDataValueH(c, object.getId(), object.entityName());
                        dynValue.doPrePersist();
                        Persistors.dynmetadatavalue.persist(dynValue);
                        dynValue.doPostPersist();
                    }
                }
            }
        }
    }

    /**
     * retrieve ClassDynMetaDataTypes related to this business entity's class.
     * 
     * @param <T>
     * @param object
     * @return
     * @throws Hd3dPersistenceException
     */
    public static <T extends IBase> List<IClassDynMetaDataType> getRelatedClassDynMetaDataTypes(final T object)
            throws Hd3dPersistenceException
    {
        List<IClassDynMetaDataType> ret = null;

        if (object != null)
        {
            /* list of all the ClassDynMetaDataTypes related to the given object's class */
            ret = (List<IClassDynMetaDataType>) BaseH.CLASSDYNMETADATATYPES_MAP.getCollection(object.entityName());
        }

        return ret;
    }

    public static void disableDynMetaDataValuesForNonMatchingObjects(final IBase object) throws Hd3dException
    {
        if (object != null)
        {
            /* list of all ClassDynMetaDataType */
            final List<IClassDynMetaDataType> classDynMetaDataTypes = getRelatedClassDynMetaDataTypes(object);
            if (CollectUtils.isNotEmpty(classDynMetaDataTypes))
            {
                /* filter it by predicate only keep the IClassDynMetaDataType that DO NOT apply to the given object */
                final Collection<IClassDynMetaDataType> matchingClassDynMetaDataTypes = org.apache.commons.collections15.CollectionUtils
                        .selectRejected(classDynMetaDataTypes, new ClassDynMetaDataTypePredicate(object));

                if (CollectUtils.isNotEmpty(matchingClassDynMetaDataTypes))
                {
                    /* for the matching types, create the corresponding value */
                    for (IClassDynMetaDataType c : matchingClassDynMetaDataTypes)
                    {
                        disableDynMetaDataValues(c, object);
                    }
                }
            }
        }
    }

    public static IDynMetaDataValue createDynMetaDataValue(final IClassDynMetaDataType c, final IBase object)
            throws Hd3dPersistenceException
    {
        if (c == null || object == null)
            return null;

        return new DynMetaDataValueH(c, object.getId(), object.entityName());
    }

    Collection<IDynMetaDataValue> createDynMetaDataValues(final IClassDynMetaDataType type,
            final Collection<IBase> objects) throws Hd3dPersistenceException
    {
        List<IDynMetaDataValue> ret = null;

        if (type != null && CollectUtils.isNotEmpty(objects))
        {
            ret = new ArrayList<IDynMetaDataValue>(objects.size());
            for (IBase o : objects)
            {
                ret.add(createDynMetaDataValue(type, o));
            }
        }

        return ret;
    }

    public static void disableDynMetaDataValues(final IClassDynMetaDataType type, final IBase object)
            throws Hd3dException
    {
        if (type != null && object != null)
        {
            /* find out the DynMetaDataValues related to object */
            Persistors.dynmetadatavalue.disableCollection(object.getDynMetaDataValues(type));
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends IBase> Collection<T> getMatchingEntityInstances(IClassDynMetaDataType classDyn)
    {
        if (classDyn == null)
            return Collections.emptyList();

        final String hqlChunk = classDyn.getPredicate();

        /* blank means "apply to none" of the instances of the bound class */
        if (StringUtils.isBlank(hqlChunk))
            return Collections.emptyList();

        final String className = EntitiesMaps.withEntityName(classDyn.getClassName()).getFullClassName();

        final StringBuilder predicate = new StringBuilder(FROM).append(className).append(" as x ");
        /* if hqlchunk is different from "*" (="apply to all instances"), apply the constraint */
        if (!ALL.equals(hqlChunk))
        {
            predicate.append(WHERE).append(hqlChunk);
        }
        predicate.trimToSize();
        return HibernateUtil.currentSession().createQuery(predicate.toString()).list();
    }

    /**
     * use the predicate (hql) and the object id to retrieve the business entity instance. If an instance is found, it
     * means it does match the predicate. Then create the corresponding DynMetaDataValue.
     */
    static class ClassDynMetaDataTypePredicate implements
            org.apache.commons.collections15.Predicate<IClassDynMetaDataType>
    {
        IBase object;

        public <T extends IBase> ClassDynMetaDataTypePredicate(T object)
        {
            this.object = object;
        }

        public boolean evaluate(IClassDynMetaDataType classDyn)
        {
            boolean match;
            if (classDyn == null)
            {
                match = false;
            }
            else
            {
                final String predicate = StringUtils.trim(classDyn.getPredicate());

                /* null, "" means not apply */
                if (StringUtils.isBlank(predicate))
                {
                    match = false;
                }
                else
                /* "*" means apply to all */
                if ("*".equals(predicate))
                {
                    match = true;
                }
                else
                {
                    /*
                     * query the updated object passing its id and the predicate (hql chunk). If a non null object is
                     * returned, it means it does match the predicate. Note: createQuery(...).setParameter(...) must not
                     * be used because setParameter fails when the parameter value contains special characters (such as
                     * chinese characters), better use a simple StringBuffer
                     */
                    final StringBuilder buf = new StringBuilder("from ").append(object.getClass().getCanonicalName())
                            .append(" where internalStatus=0 and id=").append(object.getId()).append(" and ").append(
                                    predicate);

                    buf.trimToSize();
                    org.hibernate.Query query = HibernateUtil.currentSession().createQuery(buf.toString());
                    IBase updated = (IBase) query.uniqueResult();

                    match = updated != null;// the object matches the predicate
                }
            }

            return match;
        }
    }

    @SuppressWarnings("unchecked")
    public static void deleteRelatedDynMetaDataValues(final IClassDynMetaDataType type) throws Hd3dException
    {
        /* find out the instances of the class related to the given ClassDynMetaDataType */
        final class GetCollectionCmd extends AbstractHibernateQuery<IBase>
        {
            List<IBase> result;

            public void execute(Session session) throws Hd3dException
            {
                result = getCriteria(session).list();
            }

            @Override
            protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
            {
                final Criteria query = session.createCriteria(Persistors.getPersistor(type.getClassName())
                        .getPersistedClass());
                return query;
            }
        }

        GetCollectionCmd cmd = new GetCollectionCmd();
        HibernateUtil.safeQuery(cmd);
        final List<IBase> relatedInstances = cmd.result;

        if (CollectUtils.isNotEmpty(relatedInstances))
        {
            /* for the found instances, find out the DynMetaDataValues of the given type and remove them */
            HibernateUtil.safeTransaction(new IHibernateTransaction() {
                public void execute(org.hibernate.Session session) throws Hd3dException
                {
                    for (IBase o : relatedInstances)
                    {
                        o.removeDynMetaDataValues(type);
                    }
                }
            });
        }
    }

    public static void deleteRelatedClassDynMetaDataTypes(final IDynMetaDataType object) throws Hd3dException
    {
        final List<IClassDynMetaDataType> result = Persistors.classdynmetadatatype.getByValue(null,
                Const.DYNMETATDATTYPE, object);

        Persistors.classdynmetadatatype.disableCollection(result);
    }

    public static void deleteRelatedDynMetaDataValues(final IDynMetaDataType object) throws Hd3dException
    {
        final List<IDynMetaDataValue> result = Persistors.dynmetadatavalue.getByValue(null, Const.DYNMETATDATTYPE,
                object);
        Persistors.dynmetadatavalue.disableCollection(result);
    }

    public static void loadClassDynMetaDataTypes() throws Hd3dException
    {
        Hibernate.initialize(ClassDynMetaDataTypeH.class);

        List<IClassDynMetaDataType> types = Persistors.classdynmetadatatype.getAllCollection();
        if (types != null)
        {
            BaseH.setClassDynMetaDataTypes(types);
        }
    }

}
