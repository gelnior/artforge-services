package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import static fr.hd3d.utils.Const.NAME;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.CollectUtils;


public class ProjectPrePersistChecker extends AbstractPrePersistChecker<IProject>
{
    public ProjectPrePersistChecker(IProject object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUnique())
        {
            ret = MessageFormat.format(ERR_3, object.getName());
        }

        return ret;
    }

    private boolean isUnique() throws Hd3dException
    {
        boolean ret = false;

        if (object != null)
        {
            ret = CollectUtils.isEmpty(getSimilarProjects(object.getName(), object.getHook()));
        }

        return ret;
    }

    public static List<IProject> getSimilarProjects(String name, String hook) throws Hd3dException
    {
        /* name must be unique */
        return Persistors.project.getByValue_NoPermCheck(NAME, name, LockMode.OPTIMISTIC);
    }
}
