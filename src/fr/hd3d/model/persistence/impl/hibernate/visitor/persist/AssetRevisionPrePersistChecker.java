package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import static fr.hd3d.utils.Const.KEY;
import static fr.hd3d.utils.Const.NAME;
import static fr.hd3d.utils.Const.PROJECT;
import static fr.hd3d.utils.Const.REVISION;
import static fr.hd3d.utils.Const.VARIATION;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class AssetRevisionPrePersistChecker extends AbstractPrePersistChecker<IAssetRevision>
{
    public AssetRevisionPrePersistChecker(IAssetRevision object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueWithinProject())
        {
            ret = MessageFormat.format(ERR_10, object.getName(), object.getKey(), object.getVariation(), object
                    .getRevision());
        }

        return ret;
    }

    private boolean isUniqueWithinProject() throws Hd3dException
    {
        boolean ret = true;

        if (object != null && object.getProject() != null)
        {
            // within a project, an AssetRevision name must be unique
            // the triplet (key,variation,revision) must be unique as well
            List<IAssetRevision> assetrevisions;

            class GetCollectionCmd implements IHibernateQuery
            {
                List<IAssetRevision> result;

                @SuppressWarnings("unchecked")
                public void execute(Session session) throws Hd3dPersistenceException
                {
                    HibernatePersist persistor = (HibernatePersist) Persistors.assetrevision;
                    // DetachedCriteria subQuery = persistor.basicSubCriteria(session);

                    // Note: DO NOT APPLY ANY FILTER

                    final Criteria query = persistor.basicCriteria(session);
                    query.setLockMode(LockMode.OPTIMISTIC);// VERY IMPORTANT

                    if (object.getRevision() == 1)
                    {
                        Conjunction sameNameInProject = Restrictions.conjunction();
                        sameNameInProject.add(object.getProject() != null ? Restrictions.eq(PROJECT, object
                                .getProject()) : Restrictions.isNull(PROJECT));
                        sameNameInProject.add(Restrictions.eq(NAME, object.getName()));
                        // Same name BUT different KEY/VARIATION, or else other revisions of the same assets will be
                        // returned
                        Disjunction or = Restrictions.disjunction();
                        or.add(Restrictions.ne(KEY, object.getKey()));
                        or.add(Restrictions.ne(VARIATION, object.getVariation()));
                        sameNameInProject.add(or);
                        query.add(sameNameInProject);
                    }
                    Conjunction sameIdentifier = Restrictions.conjunction();
                    sameIdentifier.add(Restrictions.eq(KEY, object.getKey()));
                    sameIdentifier.add(Restrictions.eq(VARIATION, object.getVariation()));
                    sameIdentifier.add(Restrictions.eq(REVISION, object.getRevision()));

                    query.add(sameIdentifier);

                    result = query.list();
                }
            }

            GetCollectionCmd cmd = new GetCollectionCmd();
            HibernateUtil.safeQuery(cmd);
            assetrevisions = cmd.result;

            ret = CollectUtils.isEmpty(assetrevisions);
        }

        return ret;
    }
}
