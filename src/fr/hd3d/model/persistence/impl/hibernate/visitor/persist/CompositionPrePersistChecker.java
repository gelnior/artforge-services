package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import java.text.MessageFormat;
import java.util.List;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.CompositionH;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class CompositionPrePersistChecker extends AbstractPrePersistChecker<IComposition>
{
    public CompositionPrePersistChecker(IComposition object)
    {
        super(object);
    }

    public String check() throws Hd3dPersistenceException
    {
        String ret = null;

        if (object != null && object.getConstituent() == null)
        {
            ret = ERR_19;
        }
        else if (!isUniqueWithinProject())
        {
            ret = MessageFormat.format(ERR_8, object.getName());
        }

        return ret;
    }

    private boolean isUniqueWithinProject() throws Hd3dPersistenceException
    {
        boolean ret = true;

        // if the composition does not (yet) belong to a project, no problem
        if (object != null && object.getProjectIdString() != null)
        {
            // find out all Composition related to this project
            BooleanQuery bq = new BooleanQuery();

            TermQuery internalStatusTq = new TermQuery(new Term("internalstatus", "0"));
            bq.add(new BooleanClause(internalStatusTq, BooleanClause.Occur.MUST));

            TermQuery projectIdTq = new TermQuery(new Term("project_id", object.getProjectIdString()));
            bq.add(new BooleanClause(projectIdTq, BooleanClause.Occur.MUST));

            TermQuery compoNameTq = new TermQuery(new Term("composition_name", object.getName()));
            bq.add(new BooleanClause(compoNameTq, BooleanClause.Occur.MUST));

            FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());

            // wrap Lucene query in a org.hibernate.Query
            org.hibernate.Query hibQuery = fullTextSession.createFullTextQuery(bq, CompositionH.class);

            // execute search
            List<IComposition> result = hibQuery.list();

            if (CollectUtils.isNotEmpty(result))
            {
                return false;
            }
            String[] properties = { "shot", "sequence", "constituent" };
            Object[] values = { object.getShot(), object.getSequence(), object.getConstituent() };
            try
            {
                List<IComposition> composition = Persistors.composition.getByValues(properties, values, "AND");

                if (CollectUtils.isNotEmpty(composition))
                {
                    return false;
                }
            }
            catch (Hd3dException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return ret;
    }
}
