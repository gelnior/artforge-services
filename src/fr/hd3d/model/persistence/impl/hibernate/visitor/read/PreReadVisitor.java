package fr.hd3d.model.persistence.impl.hibernate.visitor.read;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionFileRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IConstituentLink;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IEvent;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IFact;
import fr.hd3d.model.persistence.IFactType;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IFileRevisionAnnotation;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.IHint;
import fr.hd3d.model.persistence.IInventoryItem;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskBase;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.services.security.dao.IUserAccount;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class PreReadVisitor extends BasePersistenceVisitor
{
    public PreReadVisitor()
    {}

    public void visit(IBase object) throws Hd3dPersistenceException
    {}

    public void visit(IActivity object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevision object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevisionFileRevision object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevisionGroup object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IAssetRevisionLink object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IPerson object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IUserAccount object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IProject object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ICategory object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IConstituent object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ISequence object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IShot object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IComposition object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ISheet object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(final IClassDynMetaDataType object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IDynMetaDataValue object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(final IDynMetaDataType object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IComputer object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IConstituentLink object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IDevice object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IFact object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IFactType object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IFileAttribute object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IFileRevision object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IFileRevisionLink object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IHint object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IInventoryItem object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IItem object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IItemGroup object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IPlanning object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IPool object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IRole object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IResourceGroup object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IScreen object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IScript object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ISimpleActivity object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ISoftware object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IStudioMap object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ITag object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ITagCategory object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ITask object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ITaskActivity object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ITaskBase object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ITaskChanges object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ITaskGroup object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ILicense object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IApprovalNote object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IAbsence object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IEvent object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IMileStone object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IExtraLine object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(ITaskType object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IEntityTaskLink object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IApprovalNoteType object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IProjectType object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IWorkingTime object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IStep object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IPlayListEdit object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IPlayListRevision object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IFileRevisionAnnotation object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IListValues object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IDynamicPath object) throws Hd3dPersistenceException
    {
        visit((IBase) object);
    }

    public void visit(IGraphicalAnnotation object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IMountPoint object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProxyType object) throws Hd3dException
    {
        visit((IBase) object);
    }

    public void visit(IProxy object) throws Hd3dException
    {
        visit((IBase) object);
    }

	public void visit(IPlayListRevisionGroup object) throws Hd3dException
    {
        visit((IBase) object);
    }
    
    public void visit(IHd3dACL object) throws Hd3dException
    {
        visit((IBase) object);
    }
}
