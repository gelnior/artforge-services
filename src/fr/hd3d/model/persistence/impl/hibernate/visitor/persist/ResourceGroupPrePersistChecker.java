package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import static fr.hd3d.utils.Const.NAME;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.CollectUtils;


public class ResourceGroupPrePersistChecker extends AbstractPrePersistChecker<IResourceGroup>
{
    public ResourceGroupPrePersistChecker(IResourceGroup object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUnique())
        {
            ret = MessageFormat.format(ERR_16, object.getName());
        }

        return ret;
    }

    private boolean isUnique() throws Hd3dException
    {
        boolean ret = false;

        if (object != null)
        {
            ret = CollectUtils.isEmpty(getSimilarResourceGroups(object.getName()));
        }

        return ret;
    }

    public static List<IResourceGroup> getSimilarResourceGroups(String name) throws Hd3dException
    {
        return Persistors.resourcegroup.getByValue_NoPermCheck(NAME, name, LockMode.OPTIMISTIC);
    }
}
