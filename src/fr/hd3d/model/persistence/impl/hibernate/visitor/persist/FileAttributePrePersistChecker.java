package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import java.text.MessageFormat;
import java.util.Set;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IFileAttribute;


public class FileAttributePrePersistChecker extends AbstractPrePersistChecker<IFileAttribute>
{
    public FileAttributePrePersistChecker(IFileAttribute object)
    {
        super(object);
    }

    public String check() throws Hd3dPersistenceException
    {
        String ret = null;

        if (!isUniqueForSameFileRevision())
        {
            ret = MessageFormat.format(ERR_15, object.getName());
        }

        return ret;
    }

    private boolean isUniqueForSameFileRevision() throws Hd3dPersistenceException
    {
        boolean ret = true;

        if (object != null && object.getFileRevision() != null)
        {
            // For a same FileRevision, a FileAttribute's name must be unique
            Set<IFileAttribute> fileAttributes = object.getFileRevision().getAttributes();
            for (IFileAttribute fa : fileAttributes)
            {
                if (fa.getName() != null && fa.getName().equals(object.getName()))
                {
                    ret = false;
                    break;
                }
            }
        }

        return ret;
    }
}
