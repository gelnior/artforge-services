package fr.hd3d.model.persistence.impl.hibernate.visitor.delete;

import java.text.MessageFormat;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.Persistors;


public class ApprovalNoteTypePreDeleteChecker extends AbstractPreDeleteChecker<IApprovalNoteType>
{
    public ApprovalNoteTypePreDeleteChecker(IApprovalNoteType object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        final int numberOfReferencesOnThis = nbReferences();
        if (numberOfReferencesOnThis > 0)
        {
            ret = MessageFormat.format(ERR_2, numberOfReferencesOnThis, object.getId());
        }

        return ret;
    }

    private int nbReferences() throws Hd3dException
    {
        return Persistors.approvalnote.getByValue("approvalNoteType", object).size();
    }
}
