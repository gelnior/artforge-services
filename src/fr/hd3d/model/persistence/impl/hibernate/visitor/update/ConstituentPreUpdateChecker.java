package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import static fr.hd3d.utils.Const.CATEGORY;
import static fr.hd3d.utils.Const.ID;
import static fr.hd3d.utils.Const.LABEL;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class ConstituentPreUpdateChecker extends AbstractPreUpdateChecker<ILConstituent, IConstituent>
{
    public ConstituentPreUpdateChecker(IConstituent object, ILConstituent light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueWithinCategory())
        {
            ret = MessageFormat.format(ERR_5, light.getLabel());
        }

        return ret;
    }

    private boolean isUniqueWithinCategory() throws Hd3dException
    {
        boolean ret = true;

        // if the constituent does not (yet) belong to a category, no problem
        if (light != null
                && light.getCategory() != null
                && (!object.getCategory().getId().equals(light.getCategory()) || !object.getLabel().equals(
                        light.getLabel())))
        {
            // Within a category, a constituent's label or hook must be unique
            List<IConstituent> constituents;

            class GetCollectionCmd implements IHibernateQuery
            {
                List<IConstituent> result;

                @SuppressWarnings("unchecked")
                public void execute(Session session) throws Hd3dException
                {
                    HibernatePersist persistor = (HibernatePersist) Persistors.constituent;
                    IPersist<ICategory> catpersistor = Persistors.category;
                    // DetachedCriteria subQuery = persistor.basicSubCriteria(session);

                    // Note: DO NOT APPLY ANY FILTER

                    final Criteria query = persistor.basicCriteria(session);
                    query.setLockMode(LockMode.OPTIMISTIC);// VERY IMPORTANT
                    query.add(Restrictions.ne(ID, light.getId()));
                    query.add(Restrictions.eq(CATEGORY, catpersistor.getById(light.getCategory())));
                    query.add(Restrictions.eq(LABEL, light.getLabel()));

                    result = query.list();
                }
            }

            GetCollectionCmd cmd = new GetCollectionCmd();
            HibernateUtil.safeQuery(cmd);
            constituents = cmd.result;

            ret = CollectUtils.isEmpty(constituents);
        }

        return ret;
    }
}
