package fr.hd3d.model.persistence.impl.hibernate.visitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.client.Sheet;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteTypeH;
import fr.hd3d.model.persistence.impl.hibernate.ItemH;
import fr.hd3d.model.persistence.impl.hibernate.query.Queries;
import fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;


/**
 * ApprovalNoteHandler provides utility methods to perform operation on approval like updating task status of tasks
 * which have the same task type and are linked to the same work object.
 * 
 * @author HD3D
 */
public class ApprovalNoteHandler
{
    private static final String DEFAULT_PROJECT_CONSTITUENT_VALIDATION_SHEET_NAME = "Default constituent sheet (validations)";
    private static final String DEFAULT_PROJECT_SHOT_VALIDATION_SHEET_NAME = "Default shot sheet (validations)";
    private static final String DEFAULT_NOTE_GROUP_NAME = "Validations";

    /** Approval on which operations are performed. */
    private IApprovalNote approvalNote;

    public ApprovalNoteHandler(IApprovalNote anApprovalNote)
    {
        this.approvalNote = anApprovalNote;
    }

    public void updateApprovalNoteRelatedTasks() throws Hd3dException
    {
        final boolean updateTasks = !approvalNote.isUpdatedByTask() && approvalNote.getApprovalNoteType() != null
                && approvalNote.getApprovalNoteType().getTaskType() != null;
        if (updateTasks)
        {
            updateRelatedTasksStatus();
        }
    }

    private void updateRelatedTasksStatus() throws Hd3dException
    {
        /* update the related tasks only if the approval note is the most recent */
        List<IApprovalNote> mostRecentApprovalNotes = getSameType_SameWO_MoreRecentApprovalNotes();
        if (CollectUtils.isNotEmpty(mostRecentApprovalNotes))
        {
            final IApprovalNote mostRecent = mostRecentApprovalNotes.get(0);
            if (mostRecent != null)
            {
                final ETaskStatus approvalNoteStatus = ETaskStatus.valueOf(mostRecent.getStatus());
                List<ITask> tasksToUpdate = approvalNoteRelatedTasks();

                if (CollectUtils.isNotEmpty(tasksToUpdate))
                {
                    List<ITask> updatedTasks = new ArrayList<ITask>();
                    for (ITask task : tasksToUpdate)
                    {
                        if (task.getStatus() != approvalNoteStatus)
                        {
                            task.setStatus(approvalNoteStatus);
                            task.setUpdatedByApprovalNote(true);
                            updatedTasks.add(task);
                        }
                    }

                    if (CollectUtils.isNotEmpty(updatedTasks))
                    {
                        for (ITask task : updatedTasks)
                        {
                            task.doPreUpdate();
                        }
                        Persistors.task.mergeCollection(updatedTasks);
                        for (ITask task : updatedTasks)
                        {
                            task.doPostUpdate();
                        }
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private List<ITask> approvalNoteRelatedTasks() throws Hd3dPersistenceException
    {
        final ITaskType approvalNoteRelatedTaskType = approvalNote.getApprovalNoteType().getTaskType();

        String query = "select t from " + Const.CANONICALNAME_TASK
                + " t inner join t.boundEntityTaskLinks as links where " + "t.taskType.id="
                + approvalNoteRelatedTaskType.getId() + " and links.boundEntity=" + approvalNote.getBoundEntity()
                + " and links.boundEntityName='" + approvalNote.getBoundEntityName() + "'" + " and t.internalStatus=0";
        return (List<ITask>) HibernateUtil.currentSession().createQuery(query).list();
    }

    private List<IApprovalNote> getSameType_SameWO_MoreRecentApprovalNotes()
    {
        /* work object ids */
        final List<Long> woIds;
        {
            woIds = new ArrayList<Long>();
            woIds.add(approvalNote.getBoundEntity());
        }

        /* approval note type id */
        final Long approvalNoteType = approvalNote.getApprovalNoteType() == null ? null : approvalNote
                .getApprovalNoteType().getId();

        return Queries.getMoreRecentApprovalNotesByType(woIds, approvalNoteType, approvalNote.getBoundEntityName(),
                null);
    }

    // private boolean isTheMostRecentApprovalNote()
    // {
    // List<IApprovalNote> moreRecentApprovalNotes = getSameType_SameWO_theMostRecentApprovalNote();
    // return moreRecentApprovalNotes.isEmpty();
    // }

    /**
     * For a given project, synchronize task type linked to the project with the note types linked to the project.
     * 
     * @param project
     *            The project of which task type and note type synchronization is needed.
     * @param taskTypeMap
     * @param taskTypes
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    public static void updateApprovalNoteTypeForProject(IProject project, Set<ITaskType> taskTypes,
            Map<Long, ITaskType> taskTypeMap) throws Hd3dException
    {
        List<IApprovalNoteType> noteTypes = Persistors.approvalnotetype.getByValue(null, Const.PROJECT + Const._ID,
                project.getId());
        Map<Long, IApprovalNoteType> noteTypeMap = getNoteTypeMap(noteTypes);

        deleteUnusedNoteTypes(project, noteTypes, taskTypeMap);
        createMissingNoteTypes(project, taskTypes, noteTypeMap);
    }

    /**
     * @param noteTypes
     *            The list to convert to map.
     * @return Build a map of from a note type list with this entry : key = ID of the task type link to the note type
     *         and value = note type.
     */
    private static Map<Long, IApprovalNoteType> getNoteTypeMap(List<IApprovalNoteType> noteTypes)
    {
        Map<Long, IApprovalNoteType> noteTypeMap = new HashMap<Long, IApprovalNoteType>();
        for (IApprovalNoteType noteType : noteTypes)
        {
            if (noteType.getTaskType() != null)
                noteTypeMap.put(noteType.getTaskType().getId(), noteType);
        }

        return noteTypeMap;
    }

    /**
     * For each task type link to the project, if there is no not type corresponding the note type is created.
     * 
     * @param project
     *            Project for which note types are needed.
     * @param taskTypes
     *            The task types to synchronize with note types
     * @param noteTypeMap
     *            The map to easily find not types corresponding to a given task type ID.
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    private static void createMissingNoteTypes(IProject project, Set<ITaskType> taskTypes,
            Map<Long, IApprovalNoteType> noteTypeMap) throws Hd3dException
    {
        if (CollectUtils.isEmpty(taskTypes))
            return;

        /* find out task types for which there is no note type */
        List<ITaskType> taskTypesWithoutNoteType = new ArrayList<ITaskType>();
        IApprovalNoteType noteType;
        for (ITaskType taskType : taskTypes)
        {
            noteType = noteTypeMap.get(taskType.getId());
            if (noteType == null)
            {
                taskTypesWithoutNoteType.add(taskType);
            }
        }

        List<IApprovalNoteType> newNoteTypes = new ArrayList<IApprovalNoteType>();
        for (final ITaskType taskType : taskTypesWithoutNoteType)
        {
            IApprovalNoteType toCreate = new ApprovalNoteTypeH();
            toCreate.setName(taskType.getName());
            toCreate.setTaskType(taskType);
            toCreate.setProject(project);
            newNoteTypes.add(toCreate);
        }

        Persistors.approvalnotetype.persistCollection(newNoteTypes);
        HibernateUtil.currentSession().flush();

        String sheetName;
        ISheet sheet;
        IItem itemApproval;
        for (final IApprovalNoteType newNoteType : newNoteTypes)
        {
            // Add note type column to default sheet.

            sheetName = DEFAULT_PROJECT_CONSTITUENT_VALIDATION_SHEET_NAME;
            if ("Shot".equals(newNoteType.getTaskType().getEntityName()))
                sheetName = DEFAULT_PROJECT_SHOT_VALIDATION_SHEET_NAME;

            sheet = getSheet(project, sheetName);
            if (sheet != null)
            {
                IItemGroup approvalGroup = getItemGroupByName(sheet, DEFAULT_NOTE_GROUP_NAME);
                if (approvalGroup != null)
                {
                    itemApproval = new ItemH(newNoteType.getTaskType().getName(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                            ApprovalNotesCollectionQuery.class.getCanonicalName(), null, approvalGroup, "ApprovalNote",
                            null, newNoteType.getId().toString());
                    itemApproval.setRenderer(Renderer.APPROVAL);
                    itemApproval.setEditor(Renderer.APPROVAL);

                    approvalGroup.addItem(itemApproval);
                    Persistors.itemgroup.merge(approvalGroup);
                }
            }
        }
    }

    /**
     * For every note type linked to the project, if there is no corresponding task type linked to the project and no
     * notes has been set for this note type, the note type is deleted. Moreover, when an note type is created, a note
     * type column is added to default validation sheet. If a note type is removed, it is also removed from the default
     * validation sheet.
     * 
     * @param project
     *            The project of the note types to delete.
     * @param noteTypes
     *            The note types to be checked.
     * @param taskTypeMap
     *            The map used to easily find a task type with its ID.
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    private static void deleteUnusedNoteTypes(IProject project, List<IApprovalNoteType> noteTypes,
            Map<Long, ITaskType> taskTypeMap) throws Hd3dException
    {

        CollectUtils.removeNull(noteTypes);
        for (IApprovalNoteType noteType : noteTypes)
        {
            ITaskType noteTypeTaskType = noteType.getTaskType();
            if (noteTypeTaskType != null && taskTypeMap.get(noteTypeTaskType.getId()) == null)
            {
                List<IApprovalNote> notes = Persistors.approvalnote.getByValue(null, Const.APPROVALNOTETYPE_ID,
                        noteTypeTaskType.getId());

                if (CollectionUtils.isEmpty(notes))
                {
                    // Delete approval note type
                    Persistors.approvalnotetype.disable(noteType);

                    // Delete corresponding note type column in default sheet.
                    String sheetName = DEFAULT_PROJECT_CONSTITUENT_VALIDATION_SHEET_NAME;
                    if ("Shot".equals(noteType.getTaskType().getEntityName()))
                        sheetName = DEFAULT_PROJECT_SHOT_VALIDATION_SHEET_NAME;

                    ISheet sheet = getSheet(project, sheetName);
                    if (sheet != null)
                    {
                        IItemGroup approvalGroup = getItemGroupByName(sheet, DEFAULT_NOTE_GROUP_NAME);
                        if (approvalGroup != null)
                        {
                            List<IItem> itemsToDelete = new ArrayList<IItem>();
                            List<IItem> items = approvalGroup.getItems();
                            for (IItem item : items)
                            {
                                if (item != null)
                                {
                                    String parameter = item.getTransformerParameters();
                                    if (parameter != null && parameter.length() > 0
                                            && StringUtils.isNumeric(item.getTransformerParameters()))
                                        if (noteType.getId().equals(Long.valueOf(item.getTransformerParameters())))
                                        {
                                            item.setItemGroup(null);
                                            itemsToDelete.add(item);
                                        }
                                }
                            }

                            for (IItem item : itemsToDelete)
                            {
                                if (item != null)
                                {
                                    approvalGroup.getItems().remove(item);
                                    Persistors.itemgroup.persist(approvalGroup);
                                    Persistors.item.disable(item);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param project
     *            The project linked to the sheet to retrieved.
     * @param sheetName
     *            Name of the sheet to retrieve from DB
     * @return Sheet of which name is equal to sheet name and that is linked to project.
     * @throws Hd3dException
     *             Error raise in case of error while doing requests to database.
     */
    private static ISheet getSheet(IProject project, String sheetName) throws Hd3dException
    {
        String[] fields = { "name", "project.id" };
        Object[] values = { sheetName, project.getId() };

        return CollectUtils.getFirstOrNull(Persistors.sheet.getByValues(fields, values, "and"));
    }

    /**
     * @param sheet
     *            The sheet linked to the sheet to retrieved.
     * @param groupName
     *            Name of the item group to retrieve from DB
     * @return Item group of which name is equal to groupName and that is linked to the given sheet.
     */
    private static IItemGroup getItemGroupByName(ISheet sheet, String groupName)
    {
        List<IItemGroup> groups = sheet.getItemGroups();

        IItemGroup stepGroup = null;
        for (IItemGroup group : groups)
        {
            if (groupName.equals(group.getName()))
            {
                stepGroup = group;
                break;
            }
        }
        return stepGroup;
    }

}
