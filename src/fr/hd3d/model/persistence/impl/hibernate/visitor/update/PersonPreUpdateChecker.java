package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import static fr.hd3d.utils.Const.LOGIN;

import java.text.MessageFormat;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;


public class PersonPreUpdateChecker extends AbstractPreUpdateChecker<ILPerson, IPerson>
{
    public PersonPreUpdateChecker(IPerson object, ILPerson light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!sameLogin())
        {
            ret = MessageFormat.format(ERR_1, light.getLogin());
        }

        return ret;
    }

    private boolean sameLogin() throws Hd3dException
    {
        boolean ret = true;

        if (light != null && light.getLogin() != null && !light.getLogin().equals(object.getLogin()))
        {
            // login must be unique (for enabled entities)
            return Persistors.person.getObjectByValue_NoPermCheck(LOGIN, light.getLogin()) != null;
        }

        return ret;
    }
}
