package fr.hd3d.model.persistence.impl.hibernate.visitor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.type.Type;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteH;
import fr.hd3d.model.persistence.impl.hibernate.EntityTaskLinkH;
import fr.hd3d.services.resources.collectionquery.TaskBoundEntityApprovalNotesCollectionQuery;
import fr.hd3d.services.resources.collectionquery.TaskBoundEntityLinksCollectionQuery;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


/**
 * TaskHandler provides utility methods to perform operation on task like creating an entity task link when an entity is
 * given with the task.
 * 
 * @author HD3D
 */
public class TaskHandler
{
    private ITask task;
    /* previous object state */
    private Serializable id;
    private Object[] currentState;
    private Object[] previousState;
    private String[] propertyNames;
    private Type[] types;

    public TaskHandler(ITask aTask)
    {
        this.task = aTask;
    }

    public TaskHandler(ITask aTask, Serializable id, Object[] currentState, Object[] previousState,
            String[] propertyNames, Type[] types)
    {
        this.task = aTask;
        this.id = id;
        this.currentState = currentState;
        this.previousState = previousState;
        this.propertyNames = propertyNames;
        this.types = types;
    }

    public void createEntityTaskLinkForTask() throws Hd3dException
    {
        if (this.task != null && this.task.getBoundEntityId() != null && this.task.getBoundEntityName() != null)
        {
            IEntityTaskLink etl = new EntityTaskLinkH(this.task.getBoundEntityId(), this.task.getBoundEntityName(),
                    this.task);
            this.task.setBoundEntityTaskLink(etl);
            etl.doPrePersist();
            Persistors.entitytasklink.persist(etl);
            etl.doPostPersist();
        }
    }

    /**
     * Creates approval notes corresponding to task : for each approval note type having same task type as task a note
     * is created for this task type. The created note is linked to the same work object (entity) as current task. This
     * method must be used after a task update.<br>
     * <br>
     * Update is performed only if previous state is not set (= null) or if previous state is set, previous state has a
     * valid status and previous status is different of new status.
     * 
     * @throws Hd3dException
     */
    public void createApprovalNotesForTaskOnUpdate() throws Hd3dException
    {
        int statusIndex = ArrayUtils.indexOf(propertyNames, fr.hd3d.utils.Const.STATUS);

        if (ArrayUtils.INDEX_NOT_FOUND != statusIndex
                && (previousState == null || (ETaskStatus) previousState[statusIndex] != (ETaskStatus) currentState[statusIndex]))
        {
            createApprovalNotes();
        }
    }

    /**
     * Creates approval notes corresponding to task : for each approval note type having same task type as task a note
     * is created for this task type. The created note is linked to the same work object (entity) as current task. This
     * method must be used after a task creation.
     * 
     * @throws Hd3dException
     */
    public void createApprovalNotesForTaskOnCreate() throws Hd3dException
    {
        createApprovalNotes();
    }

    /**
     * Creates an approval note if the task is bound to a work object.
     * 
     * @throws Hd3dException
     */
    private void createApprovalNotes() throws Hd3dException
    {
        if (!boundEntityExists())
            return;

        /* create an approval note if the task is bound to a work object */
        List<IApprovalNote> newApprovalNotes = new ArrayList<IApprovalNote>();

        for (IApprovalNoteType type : CollectUtils.nullSafe(taskTypeRelatedApprovalNoteTypes()))
        {
            CollectUtils.addIfNotNull(newApprovalNotes, createApprovalNote(type));
        }

        persistApprovalNotes(newApprovalNotes);
    }

    private boolean boundEntityExists() throws Hd3dException
    {
        return task != null && task.boundEntity() != null && task.boundEntity().entityName() != null
                && task.boundEntity().getId() != null;
    }

    /**
     * Save given approval note list to database. But it does not perform post persist operation on create notes.
     * Because post persist for approval
     * 
     * @param approvalNotes
     * @throws Hd3dException
     */
    private void persistApprovalNotes(List<IApprovalNote> approvalNotes) throws Hd3dException
    {
        if (CollectUtils.isEmpty(approvalNotes))
            return;

        for (IApprovalNote approvalNote : approvalNotes)
        {
            approvalNote.doPrePersist();
        }

        Persistors.approvalnote.persistCollection(approvalNotes);

        for (IApprovalNote approvalNote : approvalNotes)
        {
            approvalNote.doPostPersist();
        }
    }

    /**
     * @return Note type corresponding to current task : it means note type with project id corresponding to task
     *         project and task type id corresponding to curennt task task type.
     * @throws Hd3dException
     */
    private List<IApprovalNoteType> taskTypeRelatedApprovalNoteTypes() throws Hd3dException
    {
        if (task.getTaskType() == null || task.getProject() == null)
            return Collections.emptyList();

        String[] properties = new String[] { "taskType.id", "project.id" };
        Object[] values = new Object[] { task.getTaskType().getId(), task.getProject().getId() };
        return Persistors.approvalnotetype.getByValues(properties, values, "AND");
    }

    /**
     * Creates (but do not persist it) a note of which type is equal to given note type.Then created approval is linked
     * to current task work object.
     * 
     * @param type
     * @return created approval.
     * @throws Hd3dException
     */
    private IApprovalNote createApprovalNote(IApprovalNoteType type) throws Hd3dException
    {
        IApprovalNote approvalNote = null;

        if (boundEntityExists())
        {
            approvalNote = new ApprovalNoteH();
            approvalNote.setBoundEntityName(task.boundEntity().entityName());
            approvalNote.setBoundEntity(task.boundEntity().getId());
            approvalNote.setStatus(task.getStatus().toString());
            approvalNote.setApprover(AuthenticationUtil.getCurrentUser());
            approvalNote.setApprovalDate(new Date());
            approvalNote.setApprovalNoteType(type);
            approvalNote.setComment(task.getCommentForApprovalNote());
            approvalNote.setFilerevisions(task.getFileRevisionsForApprovalNote());
            approvalNote.setUpdatedByTask(true);
        }

        return approvalNote;
    }

    public static void populateEntityTaskLink(ITask task)
    {
        if (task == null)
            return;

        try
        {
            List<IEntityTaskLink> etls = Persistors.entitytasklink.getByValue("task.id", task.getId());
            if (CollectUtils.isNotEmpty(etls))
            {
                task.setBoundEntityTaskLink(etls.get(0));
            }
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
    }

    public static void populateEntityTaskLink(List<ITask> tasks)
    {
        if (CollectUtils.isEmpty(tasks))
            return;

        try
        {
            TaskBoundEntityLinksCollectionQuery cq = new TaskBoundEntityLinksCollectionQuery(tasks, null);
            List<List<IEntityTaskLink>> allEtls = cq.doQuery();
            for (int i = 0; i < tasks.size(); i++)
            {
                List<IEntityTaskLink> etls = allEtls.get(i);
                if (CollectUtils.isNotEmpty(etls))
                {
                    tasks.get(i).setBoundEntityTaskLink(etls.get(0));
                }
            }
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
    }

    public static void reIndexBoundTask(IBase object)
    {
        if (object == null || !object.isTaskable() || CollectUtils.isEmpty(object.boundTasks()))
            return;

        /* re-index bound tasks */
        FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
        for (ITask task : object.boundTasks())
        {
            fullTextSession.index(task);
        }
        fullTextSession.flushToIndexes();
    }

    public List<IApprovalNote> getRelatedApprovalNotes() throws Hd3dItemFilterException
    {
        List<ITask> tasks = new ArrayList<ITask>();
        tasks.add(task);

        TaskBoundEntityApprovalNotesCollectionQuery cq = new TaskBoundEntityApprovalNotesCollectionQuery(tasks, null);
        List<List<IApprovalNote>> approvalNotes = cq.doQuery();

        return approvalNotes.isEmpty() ? new ArrayList<IApprovalNote>(0) : approvalNotes.get(0);
    }
}
