package fr.hd3d.model.persistence.impl.hibernate.visitor.update;

import static fr.hd3d.utils.Const.ID;
import static fr.hd3d.utils.Const.NAME;
import static fr.hd3d.utils.Const.SHEET;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILItemGroup;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class ItemGroupPreUpdateChecker extends AbstractPreUpdateChecker<ILItemGroup, IItemGroup>
{
    public ItemGroupPreUpdateChecker(IItemGroup object, ILItemGroup light)
    {
        super(object, light);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueWithinSheet())
        {
            ret = MessageFormat.format(ERR_14, light.getName());
        }

        return ret;
    }

    private boolean isUniqueWithinSheet() throws Hd3dException
    {
        boolean ret = true;

        if (light != null && light.getSheet() != null && !object.getSheet().getId().equals(light.getSheet())
                && !object.getName().equals(light.getName()))
        {
            // within a sheet, an ItemGroup name or hook must be unique
            List<IItemGroup> itemGroups;

            class GetCollectionCmd implements IHibernateQuery
            {
                List<IItemGroup> result;

                @SuppressWarnings("unchecked")
                public void execute(Session session) throws Hd3dException
                {
                    HibernatePersist persistor = (HibernatePersist) Persistors.itemgroup;
                    // DetachedCriteria subQuery = persistor.basicSubCriteria(session);

                    // Note: DO NOT APPLY ANY FILTER

                    final Criteria query = persistor.basicCriteria(session);
                    query.setLockMode(LockMode.OPTIMISTIC);// VERY IMPORTANT
                    query.add(Restrictions.ne(ID, light.getId()));
                    query.add(Restrictions.eq(SHEET, Persistors.sheet.getById(light.getSheet())));
                    query.add(Restrictions.eq(NAME, light.getName()));

                    result = query.list();
                }
            }

            GetCollectionCmd cmd = new GetCollectionCmd();
            HibernateUtil.safeQuery(cmd);
            itemGroups = cmd.result;

            ret = CollectUtils.isEmpty(itemGroups);
        }

        return ret;
    }
}
