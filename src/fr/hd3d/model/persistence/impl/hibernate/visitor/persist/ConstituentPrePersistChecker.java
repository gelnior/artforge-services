package fr.hd3d.model.persistence.impl.hibernate.visitor.persist;

import static fr.hd3d.utils.Const.CATEGORY;
import static fr.hd3d.utils.Const.LABEL;

import java.text.MessageFormat;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class ConstituentPrePersistChecker extends AbstractPrePersistChecker<IConstituent>
{
    public ConstituentPrePersistChecker(IConstituent object)
    {
        super(object);
    }

    public String check() throws Hd3dException
    {
        String ret = null;

        if (!isUniqueWithinCategory())
        {
            ret = MessageFormat.format(ERR_5, object.getLabel());
        }

        return ret;
    }

    private boolean isUniqueWithinCategory() throws Hd3dException
    {
        boolean ret = true;

        // if the constituent does not (yet) belong to a category, no problem
        if (object != null && object.getCategory() != null)
        {
            // Within a category, a constituent's label
            List<IConstituent> constituents;

            class GetCollectionCmd implements IHibernateQuery
            {
                List<IConstituent> result;

                @SuppressWarnings("unchecked")
                public void execute(Session session) throws Hd3dException
                {
                    HibernatePersist persistor = (HibernatePersist) Persistors.constituent;
                    // DetachedCriteria subQuery = persistor.basicSubCriteria(session);

                    // Note: DO NOT APPLY ANY FILTER

                    final Criteria query = persistor.basicCriteria(session);
                    query.setLockMode(LockMode.OPTIMISTIC);// VERY IMPORTANT
                    query.add(Restrictions.eq(CATEGORY, object.getCategory()));
                    query.add(Restrictions.eq(LABEL, object.getLabel()));

                    result = query.list();
                }
            }

            GetCollectionCmd cmd = new GetCollectionCmd();
            HibernateUtil.safeQuery(cmd);
            constituents = cmd.result;

            ret = CollectUtils.isEmpty(constituents);
        }

        return ret;
    }
}
