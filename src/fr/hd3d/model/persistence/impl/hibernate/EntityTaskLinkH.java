package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILEntityTaskLink;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IPath;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


@Entity
@Table(name = "hd3d_entitytasklink")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class EntityTaskLinkH extends BaseH implements IEntityTaskLink
{
    private static final long serialVersionUID = 1L;

    private Long boundEntity;
    private String boundEntityName;
    private ITask task;
    private String extra;
    /**/
    private String woPath;
    private String woName;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_entitytasklink"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public EntityTaskLinkH()
    {}

    public EntityTaskLinkH(Long id, Timestamp version, Long boundEntity, String entityName, ITask task)
    {
        super(id, version);
        this.boundEntity = boundEntity;
        this.boundEntityName = entityName;
        this.task = task;
    }

    public EntityTaskLinkH(Long boundEntity, String entityName, ITask task)
    {
        this(null, null, boundEntity, entityName, task);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "entitytasklink_boundentity")
    @Index(name = "entitytasklink_boundentity_idx")
    public Long getBoundEntity()
    {
        return boundEntity;
    }

    @Column(name = "entitytasklink_boundentityname")
    @Index(name = "entitytasklink_boundentityname_idx")
    public String getBoundEntityName()
    {
        return boundEntityName;
    }

    @ManyToOne(targetEntity = TaskH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinColumn(name = "entitytasklink_task")
    @BatchSize(size = 50)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    @NotFound(action = NotFoundAction.IGNORE)
    public ITask getTask()
    {
        return task;
    }

    @Column(name = "entitytasklink_extra")
    public String getExtra()
    {
        return extra;
    }

    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    public void setBoundEntity(Long boundEntity)
    {
        this.boundEntity = boundEntity;
    }

    public void setBoundEntityName(String boundEntityName)
    {
        this.boundEntityName = boundEntityName;
    }

    public void setTask(ITask task)
    {
        this.task = task;
    }

    @Column(name = "task_wo_path")
    public String getWoPath()
    {
        return woPath;
    }

    @Column(name = "task_wo_name")
    public String getWoName()
    {
        return woName;
    }

    public void setWoPath(String woPath)
    {
        this.woPath = woPath;
    }

    public void setWoName(String woName)
    {
        this.woName = woName;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((boundEntity == null) ? 0 : boundEntity.hashCode());
        result = prime * result + ((boundEntityName == null) ? 0 : boundEntityName.hashCode());
        result = prime * result + ((task == null) ? 0 : task.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        EntityTaskLinkH other = (EntityTaskLinkH) obj;
        if (boundEntity == null)
        {
            if (other.boundEntity != null)
                return false;
        }
        else if (!boundEntity.equals(other.boundEntity))
            return false;
        if (boundEntityName == null)
        {
            if (other.boundEntityName != null)
                return false;
        }
        else if (!boundEntityName.equals(other.boundEntityName))
            return false;
        if (task == null)
        {
            if (other.task != null)
                return false;
        }
        else if (!task.getId().equals(other.task.getId()))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    public void completePrePersistOrUpdate()
    {
        setWoInfo();
    }

    @Transient
    public void setWoInfo()
    {
        /**/
        if (this.boundEntity != null && this.boundEntityName != null)
        {
            try
            {
                IBase wo = Persistors.getPersistor(this.boundEntityName).getById(this.boundEntity);
                this.woName = NameOrLabelOrTitleHandler.getNameOrLabelOrTitle(wo);
                if (wo instanceof IPath)
                {
                    this.woPath = ((IPath) wo).getPath();
                }
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
        }
    }

    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    public IBaseTranslator<ILEntityTaskLink, IEntityTaskLink> defaultTranslator()
    {
        return Translators.entitytasklink;
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        ToStringBuilder b = new ToStringBuilder(this).appendSuper(super.toString());
        b.append("boundEntity", boundEntity).append("boundEntityName", boundEntityName);
        b.append("taskId", task == null ? "null" : task.getId());
        b.append("extra", extra);

        return b.toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
