package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IOrganization;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.OrganizationH;
import fr.hd3d.utils.ACLUtils;


public class OrganizationACLHandler extends ACLHandler
{

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getOrganizationRelatedCollections(roleIds,
                Persistors.organization.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getOrganizationRelatedCollections(roleIds,
                Persistors.organization.getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getOrganizationRelatedCollections(roleIds,
                Persistors.organization.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getOrganizationRelatedCollections(roleIds,
                Persistors.organization.getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getOrganizationRelatedCollections(roleIds,
                Persistors.organization.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getOrganizationRelatedCollections(roleIds,
                Persistors.organization.getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, OrganizationH.class, readPermittedCols.get("children"), readBansCols.get("children"),
                updatePermittedCols.get("children"), udpateBansCols.get("children"), deletePermittedCols
                        .get("children"), deleteBansCols.get("children"));

        ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Organization"));
        propagate("Organization", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getOrganizationRelatedCollections(List<Long> roleIds,
            List<IOrganization> organizations) throws Hd3dException
    {
        Set<IOrganization> children = new HashSet<IOrganization>();
        for (IOrganization organization : organizations)
        {
            children.addAll(organization.getChildren());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("children", children);
        return map;
    }

}
