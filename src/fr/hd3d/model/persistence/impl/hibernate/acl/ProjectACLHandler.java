package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.CategoryH;
import fr.hd3d.model.persistence.impl.hibernate.DynamicPathH;
import fr.hd3d.model.persistence.impl.hibernate.PlanningH;
import fr.hd3d.model.persistence.impl.hibernate.PlayListRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.PlayListRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.ResourceGroupH;
import fr.hd3d.model.persistence.impl.hibernate.SequenceH;
import fr.hd3d.model.persistence.impl.hibernate.SheetH;
import fr.hd3d.model.persistence.impl.hibernate.SimpleActivityH;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.utils.ACLUtils;


public class ProjectACLHandler extends ACLHandler
{

    public ProjectACLHandler()
    {}

    public ProjectACLHandler(IProject project)
    {
        super(project);
    }

    public void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        /* depending on permissions for the projects, change the other entities permissions */
        Map<String, Set<? extends IBase>> readPermittedCols = getProjectRelatedCollections(roleIds, Persistors.project
                .getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getProjectRelatedCollections(roleIds, Persistors.project
                .getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getProjectRelatedCollections(roleIds,
                Persistors.project.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getProjectRelatedCollections(roleIds, Persistors.project
                .getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getProjectRelatedCollections(roleIds,
                Persistors.project.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getProjectRelatedCollections(roleIds, Persistors.project
                .getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, ResourceGroupH.class, readPermittedCols.get("resourceGroups"), readBansCols
                .get("resourceGroups"), updatePermittedCols.get("resourceGroups"),
                udpateBansCols.get("resourceGroups"), deletePermittedCols.get("resourceGroups"), deleteBansCols
                        .get("resourceGroups"));

        updateOrCreateACL(info, SimpleActivityH.class, readPermittedCols.get("simpleActivities"), readBansCols
                .get("simpleActivities"), updatePermittedCols.get("simpleActivities"), udpateBansCols
                .get("simpleActivities"), deletePermittedCols.get("simpleActivities"), deleteBansCols
                .get("simpleActivities"));

        updateOrCreateACL(info, CategoryH.class, readPermittedCols.get("categories"), readBansCols.get("categories"),
                updatePermittedCols.get("categories"), udpateBansCols.get("categories"), deletePermittedCols
                        .get("categories"), deleteBansCols.get("categories"));

        updateOrCreateACL(info, SequenceH.class, readPermittedCols.get("sequences"), readBansCols.get("sequences"),
                updatePermittedCols.get("sequences"), udpateBansCols.get("sequences"), deletePermittedCols
                        .get("sequences"), deleteBansCols.get("sequences"));

        updateOrCreateACL(info, SheetH.class, readPermittedCols.get("sheets"), readBansCols.get("sheets"),
                updatePermittedCols.get("sheets"), udpateBansCols.get("sheets"), deletePermittedCols.get("sheets"),
                deleteBansCols.get("sheets"));

        updateOrCreateACL(info, AssetRevisionGroupH.class, readPermittedCols.get("assetRevisionGroups"), readBansCols
                .get("assetRevisionGroups"), updatePermittedCols.get("assetRevisionGroups"), udpateBansCols
                .get("assetRevisionGroups"), deletePermittedCols.get("assetRevisionGroups"), deleteBansCols
                .get("assetRevisionGroups"));

        updateOrCreateACL(info, AssetRevisionH.class, readPermittedCols.get("assetRevisions"), readBansCols
                .get("assetRevisions"), updatePermittedCols.get("assetRevisions"),
                udpateBansCols.get("assetRevisions"), deletePermittedCols.get("assetRevisions"), deleteBansCols
                        .get("assetRevisions"));

        updateOrCreateACL(info, DynamicPathH.class, readPermittedCols.get("dynamicPaths"), readBansCols
                .get("dynamicPaths"), updatePermittedCols.get("dynamicPaths"), udpateBansCols.get("dynamicPaths"),
                deletePermittedCols.get("dynamicPaths"), deleteBansCols.get("dynamicPaths"));

        updateOrCreateACL(info, PlanningH.class, readPermittedCols.get("plannings"), readBansCols.get("plannings"),
                updatePermittedCols.get("plannings"), udpateBansCols.get("plannings"), deletePermittedCols
                        .get("plannings"), deleteBansCols.get("plannings"));

        updateOrCreateACL(info, PlayListRevisionGroupH.class, readPermittedCols.get("playListRevisionGroups"),
                readBansCols.get("playListRevisionGroups"), updatePermittedCols.get("playListRevisionGroups"),
                udpateBansCols.get("playListRevisionGroups"), deletePermittedCols.get("playListRevisionGroups"),
                deleteBansCols.get("playListRevisionGroups"));

        updateOrCreateACL(info, PlayListRevisionH.class, readPermittedCols.get("playListRevisions"), readBansCols
                .get("playListRevisions"), updatePermittedCols.get("playListRevisions"), udpateBansCols
                .get("playListRevisions"), deletePermittedCols.get("playListRevisions"), deleteBansCols
                .get("playListRevisions"));

        updateOrCreateACL(info, TaskH.class, readPermittedCols.get("tasks"), readBansCols.get("tasks"),
                updatePermittedCols.get("tasks"), udpateBansCols.get("tasks"), deletePermittedCols.get("tasks"),
                deleteBansCols.get("tasks"));

        // updateOrCreateACL(info, TaskTypeH.class, readPermittedCols.get("projectsTaskTypes"), readBansCols
        // .get("projectsTaskTypes"), updatePermittedCols.get("projectsTaskTypes"), udpateBansCols
        // .get("projectsTaskTypes"), deletePermittedCols.get("projectsTaskTypes"), deleteBansCols
        // .get("projectsTaskTypes"));

        ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "ResourceGroup"));
        propagate("ResourceGroup", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "SimpleActivity"));
        propagate("SimpleActivity", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Category"));
        propagate("Category", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Sequence"));
        propagate("Sequence", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Sheet"));
        propagate("Sheet", relatedEntityACLInfo, roleIds);
        // relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "TaskType"));
        // propagate("TaskType", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "AssetRevisionGroup"));
        propagate("AssetRevisionGroup", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "AssetRevision"));
        propagate("AssetRevision", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "DynamicPath"));
        propagate("DynamicPath", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Planning"));
        propagate("Planning", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "PlayListRevisionGroup"));
        propagate("PlayListRevisionGroup", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "PlayListRevision"));
        propagate("PlayListRevision", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Task"));
        propagate("Task", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getProjectRelatedCollections(List<Long> roleIds, List<IProject> projects)
            throws Hd3dException
    {
        Set<IResourceGroup> resourceGroups = new HashSet<IResourceGroup>();
        Set<ISimpleActivity> simpleActivities = new HashSet<ISimpleActivity>();
        Set<ICategory> categories = new HashSet<ICategory>();
        Set<ISequence> sequences = new HashSet<ISequence>();
        Set<ISheet> sheets = new HashSet<ISheet>();
        // Set<ITaskType> projectsTaskTypes = new HashSet<ITaskType>();
        Set<IAssetRevisionGroup> assetRevisionGroups = new HashSet<IAssetRevisionGroup>();
        Set<IAssetRevision> assetRevisions = new HashSet<IAssetRevision>();
        Set<IDynamicPath> dynamicPaths = new HashSet<IDynamicPath>();
        Set<IPlanning> plannings = new HashSet<IPlanning>();
        Set<IPlayListRevisionGroup> playListRevisionGroups = new HashSet<IPlayListRevisionGroup>();
        Set<IPlayListRevision> playListRevisions = new HashSet<IPlayListRevision>();
        Set<ITask> tasks = new HashSet<ITask>();

        for (IProject project : projects)
        {
            resourceGroups.addAll(project.getResourceGroups());
            simpleActivities.addAll(project.getSimpleActivities());
            categories.addAll(project.getCategories());
            sequences.addAll(project.getSequences());
            sheets.addAll(project.getSheets());
            // projectsTaskTypes.addAll(project.getTaskTypes());
            assetRevisionGroups.addAll(Persistors.assetrevisiongroup.getByValue("project", project));
            assetRevisions.addAll(Persistors.assetrevision.getByValue("project", project));
            dynamicPaths.addAll(Persistors.dynamicpath.getByValue("project", project));
            plannings.addAll(Persistors.planning.getByValue("project", project));
            playListRevisionGroups.addAll(Persistors.playlistrevisiongroup.getByValue("project", project));
            playListRevisions.addAll(Persistors.playlistrevision.getByValue("project", project));
            tasks.addAll(Persistors.task.getByValue("project", project));
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("resourceGroups", resourceGroups);
        map.put("simpleActivities", simpleActivities);
        map.put("categories", categories);
        map.put("sequences", sequences);
        map.put("sheets", sheets);
        // map.put("projectsTaskTypes", projectsTaskTypes);
        map.put("assetRevisionGroups", assetRevisionGroups);
        map.put("assetRevisions", assetRevisions);
        map.put("dynamicPaths", dynamicPaths);
        map.put("plannings", plannings);
        map.put("playListRevisionGroups", playListRevisionGroups);
        map.put("playListRevisions", playListRevisions);
        map.put("tasks", tasks);

        return map;
    }
}
