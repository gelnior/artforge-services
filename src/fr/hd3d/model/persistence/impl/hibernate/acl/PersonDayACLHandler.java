package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.WorkingTimeH;


public class PersonDayACLHandler extends ACLHandler
{

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getPersonDayRelatedCollections(roleIds,
                Persistors.personDay.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getPersonDayRelatedCollections(roleIds, Persistors.personDay
                .getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getPersonDayRelatedCollections(roleIds,
                Persistors.personDay.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getPersonDayRelatedCollections(roleIds, Persistors.personDay
                .getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getPersonDayRelatedCollections(roleIds,
                Persistors.personDay.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getPersonDayRelatedCollections(roleIds, Persistors.personDay
                .getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, WorkingTimeH.class, readPermittedCols.get("workingTimes"), readBansCols
                .get("workingTimes"), updatePermittedCols.get("workingTimes"), udpateBansCols.get("workingTimes"),
                deletePermittedCols.get("workingTimes"), deleteBansCols.get("workingTimes"));

        // ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "WorkingTime"));
        // propagate("WorkingTime", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getPersonDayRelatedCollections(List<Long> roleIds,
            List<IPersonDay> personDays) throws Hd3dException
    {
        Set<IWorkingTime> workingTimes = new HashSet<IWorkingTime>();
        for (IPersonDay personDay : personDays)
        {
            workingTimes.addAll(personDay.getWorkingTimes());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("workingTimes", workingTimes);
        return map;
    }

}
