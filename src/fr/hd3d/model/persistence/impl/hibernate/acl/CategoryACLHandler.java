package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.CategoryH;
import fr.hd3d.model.persistence.impl.hibernate.ConstituentH;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.utils.ACLUtils;
import fr.hd3d.utils.CollectUtils;


public class CategoryACLHandler extends ACLHandler
{
    public CategoryACLHandler()
    {}

    public CategoryACLHandler(ICategory category)
    {
        super(category);
    }

    @Override
    protected IHd3dACL lastACL(ACLInfo info)
    {
        final String toAdd = ' ' + String.valueOf(object.getId());

        final ICategory parent = ((ICategory) object).getParent();

        IHd3dACL lastACL = CollectUtils.getLastOrNull(info.getAcls());
        lastACL.setCreatePermitted("*");

        if (parent == null || (parent != null && info.getReadPermittedIds().contains(parent.getId())))
        {
            lastACL.setReadPermitted(StringUtils.defaultString(lastACL.getReadPermitted()) + toAdd);
        }
        if (parent == null || (parent != null && info.getReadNotPermittedIds().contains(parent.getId())))
        {
            lastACL.setReadBans(StringUtils.defaultString(lastACL.getReadBans()) + toAdd);
        }
        if (parent == null || (parent != null && info.getUpdatePermittedIds().contains(parent.getId())))
        {
            lastACL.setUpdatePermitted(StringUtils.defaultString(lastACL.getUpdatePermitted()) + toAdd);
        }
        if (parent == null || (parent != null && info.getUpdateNotPermittedIds().contains(parent.getId())))
        {
            lastACL.setUpdateBans(StringUtils.defaultString(lastACL.getUpdateBans()) + toAdd);
        }
        if (parent == null || (parent != null && info.getDeletePermittedIds().contains(parent.getId())))
        {
            lastACL.setDeletePermitted(StringUtils.defaultString(lastACL.getDeletePermitted()) + toAdd);
        }
        if (parent == null || (parent != null && info.getDeleteNotPermittedIds().contains(parent.getId())))
        {
            lastACL.setDeleteBans(StringUtils.defaultString(lastACL.getDeleteBans()) + toAdd);
        }

        return lastACL;
    }

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getCategoryRelatedCollections(roleIds,
                Persistors.category.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getCategoryRelatedCollections(roleIds, Persistors.category
                .getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getCategoryRelatedCollections(roleIds,
                Persistors.category.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getCategoryRelatedCollections(roleIds, Persistors.category
                .getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getCategoryRelatedCollections(roleIds,
                Persistors.category.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getCategoryRelatedCollections(roleIds, Persistors.category
                .getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, CategoryH.class, readPermittedCols.get("children"), readBansCols.get("children"),
                updatePermittedCols.get("children"), udpateBansCols.get("children"), deletePermittedCols
                        .get("children"), deleteBansCols.get("children"));

        updateOrCreateACL(info, ConstituentH.class, readPermittedCols.get("constituents"), readBansCols
                .get("constituents"), updatePermittedCols.get("constituents"), udpateBansCols.get("constituents"),
                deletePermittedCols.get("constituents"), deleteBansCols.get("constituents"));

        /* No need to propagate to category children, they are already processed above */
        // ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Category"));
        // propagate("Category", relatedEntityACLInfo, roleIds);
        ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Constituent"));
        propagate("Constituent", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getCategoryRelatedCollections(List<Long> roleIds,
            List<ICategory> categories) throws Hd3dException
    {
        Set<ICategory> children = new HashSet<ICategory>();
        Set<IConstituent> constituents = new HashSet<IConstituent>();
        for (ICategory category : categories)
        {
            children.addAll(category.allChildren(false));
            constituents.addAll(category.getConstituents());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("children", children);
        map.put("constituents", constituents);
        return map;
    }

}
