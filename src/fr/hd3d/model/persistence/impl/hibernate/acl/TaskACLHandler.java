package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.TaskActivityH;
import fr.hd3d.model.persistence.impl.hibernate.TaskChangesH;
import fr.hd3d.utils.ACLUtils;


public class TaskACLHandler extends ACLHandler
{

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getTaskRelatedCollections(roleIds, Persistors.task
                .getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getTaskRelatedCollections(roleIds, Persistors.task
                .getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getTaskRelatedCollections(roleIds, Persistors.task
                .getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getTaskRelatedCollections(roleIds, Persistors.task
                .getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getTaskRelatedCollections(roleIds, Persistors.task
                .getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getTaskRelatedCollections(roleIds, Persistors.task
                .getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, TaskActivityH.class, readPermittedCols.get("taskActivities"), readBansCols
                .get("taskActivities"), updatePermittedCols.get("taskActivities"),
                udpateBansCols.get("taskActivities"), deletePermittedCols.get("taskActivities"), deleteBansCols
                        .get("taskActivities"));

        updateOrCreateACL(info, TaskChangesH.class, readPermittedCols.get("taskChanges"), readBansCols
                .get("taskChanges"), updatePermittedCols.get("taskChanges"), udpateBansCols.get("taskChanges"),
                deletePermittedCols.get("taskChanges"), deleteBansCols.get("taskChanges"));

        ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "TaskActivity"));
        propagate("TaskActivity", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "TaskChanges"));
        propagate("TaskChanges", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getTaskRelatedCollections(List<Long> roleIds, List<ITask> tasks)
            throws Hd3dException
    {
        Set<ITaskActivity> taskActivities = new HashSet<ITaskActivity>();
        Set<ITaskChanges> taskChanges = new HashSet<ITaskChanges>();
        for (ITask task : tasks)
        {
            taskActivities.addAll(task.getTaskActivities());
            taskChanges.addAll(task.getTaskChanges());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("taskActivities", taskActivities);
        map.put("taskChanges", taskChanges);
        return map;
    }

}
