package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteH;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteTypeH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionLinkH;
import fr.hd3d.model.persistence.impl.hibernate.ClassDynMetaDataTypeH;
import fr.hd3d.model.persistence.impl.hibernate.CompositionH;
import fr.hd3d.model.persistence.impl.hibernate.ComputerH;
import fr.hd3d.model.persistence.impl.hibernate.ComputerModelH;
import fr.hd3d.model.persistence.impl.hibernate.ComputerTypeH;
import fr.hd3d.model.persistence.impl.hibernate.ConstituentLinkH;
import fr.hd3d.model.persistence.impl.hibernate.DeviceH;
import fr.hd3d.model.persistence.impl.hibernate.DeviceModelH;
import fr.hd3d.model.persistence.impl.hibernate.DeviceTypeH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataTypeH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataValueH;
import fr.hd3d.model.persistence.impl.hibernate.EntityTaskLinkH;
import fr.hd3d.model.persistence.impl.hibernate.EventH;
import fr.hd3d.model.persistence.impl.hibernate.FactTypeH;
import fr.hd3d.model.persistence.impl.hibernate.HintH;
import fr.hd3d.model.persistence.impl.hibernate.LicenseH;
import fr.hd3d.model.persistence.impl.hibernate.ListValuesH;
import fr.hd3d.model.persistence.impl.hibernate.ManufacturerH;
import fr.hd3d.model.persistence.impl.hibernate.PoolH;
import fr.hd3d.model.persistence.impl.hibernate.ProcessorH;
import fr.hd3d.model.persistence.impl.hibernate.ProjectTypeH;
import fr.hd3d.model.persistence.impl.hibernate.ProxyTypeH;
import fr.hd3d.model.persistence.impl.hibernate.QualificationH;
import fr.hd3d.model.persistence.impl.hibernate.RoleH;
import fr.hd3d.model.persistence.impl.hibernate.RoomH;
import fr.hd3d.model.persistence.impl.hibernate.ScreenH;
import fr.hd3d.model.persistence.impl.hibernate.ScreenModelH;
import fr.hd3d.model.persistence.impl.hibernate.SkillH;
import fr.hd3d.model.persistence.impl.hibernate.SkillLevelH;
import fr.hd3d.model.persistence.impl.hibernate.SoftwareH;
import fr.hd3d.model.persistence.impl.hibernate.StudioMapH;
import fr.hd3d.model.persistence.impl.hibernate.TagCategoryH;
import fr.hd3d.model.persistence.impl.hibernate.TagH;
import fr.hd3d.model.persistence.impl.hibernate.TaskTypeH;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.security.dao.Hd3dACLH;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.utils.ACLUtils;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public abstract class ACLHandler
{
    public static Set<Class<?>> publicEntities = new HashSet<Class<?>>();

    static
    {
        publicEntities.add(DynMetaDataValueH.class);
        publicEntities.add(DynMetaDataTypeH.class);
        publicEntities.add(ClassDynMetaDataTypeH.class);
        publicEntities.add(TagH.class);
        publicEntities.add(TagCategoryH.class);
        publicEntities.add(EntityTaskLinkH.class);
        publicEntities.add(ConstituentLinkH.class);
        publicEntities.add(TaskTypeH.class);
        publicEntities.add(ManufacturerH.class);
        publicEntities.add(PoolH.class);
        publicEntities.add(ProcessorH.class);
        publicEntities.add(LicenseH.class);
        publicEntities.add(ComputerH.class);
        publicEntities.add(ComputerModelH.class);
        publicEntities.add(ComputerTypeH.class);
        publicEntities.add(DeviceH.class);
        publicEntities.add(DeviceModelH.class);
        publicEntities.add(DeviceTypeH.class);
        publicEntities.add(ListValuesH.class);
        publicEntities.add(ProxyTypeH.class);
        publicEntities.add(QualificationH.class);
        publicEntities.add(RoleH.class);
        publicEntities.add(RoomH.class);
        publicEntities.add(ScreenH.class);
        publicEntities.add(ScreenModelH.class);
        publicEntities.add(SkillH.class);
        publicEntities.add(SkillLevelH.class);
        publicEntities.add(SoftwareH.class);
        publicEntities.add(StudioMapH.class);
        publicEntities.add(AssetRevisionLinkH.class);
        publicEntities.add(EventH.class);
        publicEntities.add(FactTypeH.class);
        publicEntities.add(ProjectTypeH.class);
        publicEntities.add(HintH.class);
        publicEntities.add(ApprovalNoteH.class);
        publicEntities.add(ApprovalNoteTypeH.class);
        publicEntities.add(CompositionH.class);
    }

    protected IBase object;

    public ACLHandler()
    {}

    public ACLHandler(IBase object)
    {
        this.object = object;
    }

    public static boolean isPublic(IBase object)
    {
        if (object == null)
            return true;

        return ACLHandler.publicEntities.contains(object.getClass());
    }

    public static boolean isPublic(Class<?> clazz)
    {
        return ACLHandler.publicEntities.contains(clazz);
    }

    public IHd3dACL buildACL() throws Hd3dException
    {
        if (object == null)
            return null;

        ACLInfo info = ACLUtils.getACLInfo(object.entityName(), CollectUtils.getIds(AuthenticationUtil
                .getCurrentUserRoles()));

        if (info == null)
            return null;

        if (info.isEmpty())
        {
            /* create new ACL */
            IHd3dACL newACL = newACL(info);
            Persistors.acl.persist(newACL);
            return newACL;
        }
        else
        {
            /* update existing ACL */
            IHd3dACL lastACL = lastACL(info);
            Persistors.acl.merge(lastACL);
            return lastACL;
        }
    }

    protected IHd3dACL newACL(ACLInfo info)
    {
        final String toAdd = ' ' + String.valueOf(object.getId());

        IHd3dACL newACL = new Hd3dACLH();
        newACL.setRoles(info.getACLRoles());
        newACL.setEntityName(object.entityName());
        newACL.setCreatePermitted("*");
        newACL.setReadPermitted(toAdd);
        // newACL.setReadBans(readBans);
        newACL.setUpdatePermitted(toAdd);
        // newACL.setUpdateBans(updateBans);
        newACL.setDeletePermitted(toAdd);
        // newACL.setDeleteBans(deleteBans);
        return newACL;
    }

    protected IHd3dACL lastACL(ACLInfo info)
    {
        final String toAdd = ' ' + String.valueOf(object.getId());

        IHd3dACL lastACL = CollectUtils.getLastOrNull(info.getAcls());
        lastACL.setCreatePermitted("*");
        lastACL.setReadPermitted(StringUtils.defaultString(lastACL.getReadPermitted()) + toAdd);
        // lastACL.setReadBans(StringUtils.defaultString(lastACL.getReadBans()) + toAdd);
        lastACL.setUpdatePermitted(StringUtils.defaultString(lastACL.getUpdatePermitted()) + toAdd);
        // lastACL.setUpdateBans(StringUtils.defaultString(lastACL.getUpdateBans()) + toAdd);
        lastACL.setDeletePermitted(StringUtils.defaultString(lastACL.getDeletePermitted()) + toAdd);
        // lastACL.setDeleteBans(StringUtils.defaultString(lastACL.getDeleteBans()) + toAdd);

        return lastACL;
    }

    public static void propagatePermissions(IHd3dACL acl) throws Hd3dException
    {
        if (acl == null)
            return;

        final String entityName = acl.getEntityName();
        final List<Long> roleIds = CollectUtils.getIds(acl.getRoles());

        ACLInfo info = new ACLInfo(ACLUtils.getACLs(roleIds, entityName));
        propagate(entityName, info, roleIds);
    }

    public static void updateOrCreateACL(ACLInfo info, Class<?> relatedClass,
            Collection<? extends IBase> readPermittedCollection, Collection<? extends IBase> readBansCollection,
            Collection<? extends IBase> updatePermittedCollection, Collection<? extends IBase> updateBansCollection,
            Collection<? extends IBase> deletePermittedCollection, Collection<? extends IBase> deleteBansCollection)
            throws Hd3dException
    {
        if (relatedClass != null && info != null && CollectUtils.isNotEmpty(info.getACLRoles()))
        {
            ACLInfo relatedEntityACL = ACLUtils.getACLInfo(relatedClass, CollectUtils.getIds(info.getACLRoles()));
            IHd3dACL lastEntityACL = CollectUtils.getLastOrNull(relatedEntityACL.getAcls());
            IHd3dACL newACL = null;

            /* create permitted */
            if (lastEntityACL != null)
            {
                if (info.isCreatePermitted())
                    lastEntityACL.setCreatePermitted("*");
            }
            else
            {
                newACL = new Hd3dACLH();
                newACL.setRoles(info.getACLRoles());
                newACL.setEntityName(EntitiesMaps.withClass(relatedClass).getEntityName());
                newACL.setCreatePermitted("*");
            }

            /* read permitted */
            if (CollectUtils.isNotEmpty(readPermittedCollection))
            {
                /* merge previous and current ids */
                Set<Long> newReadPermittedIds = new TreeSet<Long>(relatedEntityACL.getReadPermittedIds());
                newReadPermittedIds.addAll(CollectUtils.getIds(readPermittedCollection));

                if (CollectUtils.isNotEmpty(newReadPermittedIds))
                {
                    final String entityIdsToAddInACLString = StringUtils.join(newReadPermittedIds, ' ');
                    if (lastEntityACL != null)
                    {
                        lastEntityACL.setReadPermitted(entityIdsToAddInACLString);
                    }
                    else
                    {
                        if (newACL == null)
                        {
                            newACL = new Hd3dACLH();
                        }
                        newACL.setRoles(info.getACLRoles());
                        newACL.setEntityName(EntitiesMaps.withClass(relatedClass).getEntityName());
                        newACL.setReadPermitted(entityIdsToAddInACLString);
                    }
                }
            }

            /* read bans */
            if (CollectUtils.isNotEmpty(readBansCollection))
            {
                Set<Long> newReadBansIds = new TreeSet<Long>(relatedEntityACL.getReadNotPermittedIds());
                newReadBansIds.addAll(CollectUtils.getIds(readBansCollection));

                if (CollectUtils.isNotEmpty(newReadBansIds))
                {
                    final String entityIdsToAddInACLString = StringUtils.join(newReadBansIds, ' ');
                    if (lastEntityACL != null)
                    {
                        lastEntityACL.setReadBans(entityIdsToAddInACLString);
                    }
                    else
                    {
                        if (newACL == null)
                        {
                            newACL = new Hd3dACLH();
                        }
                        newACL.setRoles(info.getACLRoles());
                        newACL.setEntityName(EntitiesMaps.withClass(relatedClass).getEntityName());
                        newACL.setReadBans(entityIdsToAddInACLString);
                    }
                }
            }

            /* update permitted */
            if (CollectUtils.isNotEmpty(updatePermittedCollection))
            {
                Set<Long> newUpdatePermittedIds = new TreeSet<Long>(relatedEntityACL.getUpdatePermittedIds());
                newUpdatePermittedIds.addAll(CollectUtils.getIds(updatePermittedCollection));

                if (CollectUtils.isNotEmpty(newUpdatePermittedIds))
                {
                    final String entityIdsToAddInACLString = StringUtils.join(newUpdatePermittedIds, ' ');
                    if (lastEntityACL != null)
                    {
                        lastEntityACL.setUpdatePermitted(entityIdsToAddInACLString);
                    }
                    else
                    {
                        if (newACL == null)
                        {
                            newACL = new Hd3dACLH();
                        }
                        newACL.setRoles(info.getACLRoles());
                        newACL.setEntityName(EntitiesMaps.withClass(relatedClass).getEntityName());
                        newACL.setUpdatePermitted(entityIdsToAddInACLString);
                    }
                }
            }

            /* update bans */
            if (CollectUtils.isNotEmpty(updateBansCollection))
            {
                Set<Long> newUpdateBansIds = new TreeSet<Long>(relatedEntityACL.getUpdateNotPermittedIds());
                newUpdateBansIds.addAll(CollectUtils.getIds(updateBansCollection));

                if (CollectUtils.isNotEmpty(newUpdateBansIds))
                {
                    final String entityIdsToAddInACLString = StringUtils.join(newUpdateBansIds, ' ');
                    if (lastEntityACL != null)
                    {
                        lastEntityACL.setUpdateBans(entityIdsToAddInACLString);
                    }
                    else
                    {
                        if (newACL == null)
                        {
                            newACL = new Hd3dACLH();
                        }
                        newACL.setRoles(info.getACLRoles());
                        newACL.setEntityName(EntitiesMaps.withClass(relatedClass).getEntityName());
                        newACL.setUpdateBans(entityIdsToAddInACLString);
                    }
                }
            }

            /* delete permitted */
            if (CollectUtils.isNotEmpty(deletePermittedCollection))
            {
                Set<Long> newDeletePermittedIds = new TreeSet<Long>(relatedEntityACL.getDeletePermittedIds());
                newDeletePermittedIds.addAll(CollectUtils.getIds(deletePermittedCollection));

                if (CollectUtils.isNotEmpty(newDeletePermittedIds))
                {
                    final String entityIdsToAddInACLString = StringUtils.join(newDeletePermittedIds, ' ');
                    if (lastEntityACL != null)
                    {
                        lastEntityACL.setDeletePermitted(entityIdsToAddInACLString);
                    }
                    else
                    {
                        if (newACL == null)
                        {
                            newACL = new Hd3dACLH();
                        }
                        newACL.setRoles(info.getACLRoles());
                        newACL.setEntityName(EntitiesMaps.withClass(relatedClass).getEntityName());
                        newACL.setDeletePermitted(entityIdsToAddInACLString);
                    }
                }
            }

            /* delete bans */
            if (CollectUtils.isNotEmpty(deleteBansCollection))
            {
                Set<Long> newDeleteBansIds = new TreeSet<Long>(relatedEntityACL.getDeleteNotPermittedIds());
                newDeleteBansIds.addAll(CollectUtils.getIds(deleteBansCollection));

                if (CollectUtils.isNotEmpty(newDeleteBansIds))
                {
                    final String entityIdsToAddInACLString = StringUtils.join(newDeleteBansIds, ' ');
                    if (lastEntityACL != null)
                    {
                        lastEntityACL.setDeleteBans(entityIdsToAddInACLString);
                    }
                    else
                    {
                        if (newACL == null)
                        {
                            newACL = new Hd3dACLH();
                        }
                        newACL.setRoles(info.getACLRoles());
                        newACL.setEntityName(EntitiesMaps.withClass(relatedClass).getEntityName());
                        newACL.setDeleteBans(entityIdsToAddInACLString);
                    }
                }
            }

            /* update existing ACL or create new one */
            if (lastEntityACL != null)
            {
                Persistors.acl.merge(lastEntityACL);
                HibernateUtil.currentSession().flush();
            }
            if (newACL != null)
            {
                Persistors.acl.persist(newACL);
                HibernateUtil.currentSession().flush();
            }
        }
    }

    protected static void propagate(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        if ("Project".equals(entityName))
        {
            new ProjectACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("Category".equals(entityName))
        {
            new CategoryACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("Sequence".equals(entityName))
        {
            new SequenceACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("Sheet".equals(entityName))
        {
            new SheetACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("ItemGroup".equals(entityName))
        {
            new ItemGroupACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("AssetRevisionGroup".equals(entityName))
        {
            new AssetRevisionGroupACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("AssetRevision".equals(entityName))
        {
            new AssetRevisionACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("DynamicPath".equals(entityName))
        {
            new DynamicPathACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("Organization".equals(entityName))
        {
            new OrganizationACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("PersonDay".equals(entityName))
        {
            new PersonDayACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("Person".equals(entityName))
        {
            new PersonACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("Planning".equals(entityName))
        {
            new PlanningACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("PlayListRevisionGroup".equals(entityName))
        {
            new PlayListRevisionGroupACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("PlayListRevision".equals(entityName))
        {
            new PlayListRevisionACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("ResourceGroup".equals(entityName))
        {
            new ResourceGroupACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("TaskGroup".equals(entityName))
        {
            new TaskGroupACLHandler().propagatePermissions(entityName, info, roleIds);
        }
        else if ("Task".equals(entityName))
        {
            new TaskACLHandler().propagatePermissions(entityName, info, roleIds);
        }
    }

}
