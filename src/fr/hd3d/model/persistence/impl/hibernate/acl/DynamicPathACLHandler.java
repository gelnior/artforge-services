package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.DynamicPathH;


public class DynamicPathACLHandler extends ACLHandler
{

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getDynamicPathRelatedCollections(roleIds,
                Persistors.dynamicpath.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getDynamicPathRelatedCollections(roleIds,
                Persistors.dynamicpath.getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getDynamicPathRelatedCollections(roleIds,
                Persistors.dynamicpath.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getDynamicPathRelatedCollections(roleIds,
                Persistors.dynamicpath.getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getDynamicPathRelatedCollections(roleIds,
                Persistors.dynamicpath.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getDynamicPathRelatedCollections(roleIds,
                Persistors.dynamicpath.getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, DynamicPathH.class, readPermittedCols.get("children"), readBansCols.get("children"),
                updatePermittedCols.get("children"), udpateBansCols.get("children"), deletePermittedCols
                        .get("children"), deleteBansCols.get("children"));

        /* No need to propagate to sequence children, they are already processed above */
        // ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "DynamicPath"));
        // propagate("DynamicPath", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getDynamicPathRelatedCollections(List<Long> roleIds,
            List<IDynamicPath> dynamicPaths) throws Hd3dException
    {
        Set<IDynamicPath> children = new HashSet<IDynamicPath>();
        for (IDynamicPath dynamicPath : dynamicPaths)
        {
            children.addAll(dynamicPath.allChildren(false));
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("children", children);
        return map;
    }

}
