package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ItemH;


public class ItemGroupACLHandler extends ACLHandler
{

    public void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        /* depending on permissions for the projects, change the other entities permissions */
        Map<String, Set<? extends IBase>> readPermittedCols = getItemGroupRelatedCollections(roleIds,
                Persistors.itemgroup.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getItemGroupRelatedCollections(roleIds, Persistors.itemgroup
                .getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getItemGroupRelatedCollections(roleIds,
                Persistors.itemgroup.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getItemGroupRelatedCollections(roleIds, Persistors.itemgroup
                .getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getItemGroupRelatedCollections(roleIds,
                Persistors.itemgroup.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getItemGroupRelatedCollections(roleIds, Persistors.itemgroup
                .getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, ItemH.class, readPermittedCols.get("items"), readBansCols.get("items"),
                updatePermittedCols.get("items"), udpateBansCols.get("items"), deletePermittedCols.get("items"),
                deleteBansCols.get("items"));

        // ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Item"));
        // propagate("Item", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getItemGroupRelatedCollections(List<Long> roleIds,
            List<IItemGroup> itemGroups) throws Hd3dException
    {
        Set<IItem> items = new HashSet<IItem>();

        for (IItemGroup itemGroup : itemGroups)
        {
            items.addAll(itemGroup.getItems());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("items", items);
        return map;
    }
}
