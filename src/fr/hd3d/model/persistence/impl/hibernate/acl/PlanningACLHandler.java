package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.TaskChangesH;
import fr.hd3d.model.persistence.impl.hibernate.TaskGroupH;
import fr.hd3d.utils.ACLUtils;


public class PlanningACLHandler extends ACLHandler
{

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getPlanningRelatedCollections(roleIds,
                Persistors.planning.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getPlanningRelatedCollections(roleIds, Persistors.planning
                .getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getPlanningRelatedCollections(roleIds,
                Persistors.planning.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getPlanningRelatedCollections(roleIds, Persistors.planning
                .getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getPlanningRelatedCollections(roleIds,
                Persistors.planning.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getPlanningRelatedCollections(roleIds, Persistors.planning
                .getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, TaskGroupH.class, readPermittedCols.get("taskGroups"), readBansCols.get("taskGroups"),
                updatePermittedCols.get("taskGroups"), udpateBansCols.get("taskGroups"), deletePermittedCols
                        .get("taskGroups"), deleteBansCols.get("taskGroups"));

        updateOrCreateACL(info, TaskChangesH.class, readPermittedCols.get("taskChanges"), readBansCols
                .get("taskChanges"), updatePermittedCols.get("taskChanges"), udpateBansCols.get("taskChanges"),
                deletePermittedCols.get("taskChanges"), deleteBansCols.get("taskChanges"));

        ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "TaskGroup"));
        propagate("TaskGroup", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "TaskChanges"));
        propagate("TaskChanges", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getPlanningRelatedCollections(List<Long> roleIds,
            List<IPlanning> plannings) throws Hd3dException
    {
        Set<ITaskGroup> taskGroups = new HashSet<ITaskGroup>();
        Set<ITaskChanges> taskChanges = new HashSet<ITaskChanges>();
        for (IPlanning planning : plannings)
        {
            taskGroups.addAll(planning.getTaskGroups());
            taskChanges.addAll(planning.getTaskChanges());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("taskGroups", taskGroups);
        map.put("taskChanges", taskChanges);
        return map;
    }

}
