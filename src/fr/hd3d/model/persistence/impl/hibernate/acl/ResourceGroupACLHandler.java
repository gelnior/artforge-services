package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IResource;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ResourceGroupH;
import fr.hd3d.model.persistence.impl.hibernate.ResourceH;
import fr.hd3d.utils.ACLUtils;


public class ResourceGroupACLHandler extends ACLHandler
{

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getResourceGroupRelatedCollections(roleIds,
                Persistors.resourcegroup.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getResourceGroupRelatedCollections(roleIds,
                Persistors.resourcegroup.getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getResourceGroupRelatedCollections(roleIds,
                Persistors.resourcegroup.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getResourceGroupRelatedCollections(roleIds,
                Persistors.resourcegroup.getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getResourceGroupRelatedCollections(roleIds,
                Persistors.resourcegroup.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getResourceGroupRelatedCollections(roleIds,
                Persistors.resourcegroup.getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, ResourceGroupH.class, readPermittedCols.get("children"), readBansCols.get("children"),
                updatePermittedCols.get("children"), udpateBansCols.get("children"), deletePermittedCols
                        .get("children"), deleteBansCols.get("children"));

        updateOrCreateACL(info, ResourceH.class, readPermittedCols.get("resources"), readBansCols.get("resources"),
                updatePermittedCols.get("resources"), udpateBansCols.get("resources"), deletePermittedCols
                        .get("resources"), deleteBansCols.get("resources"));

        /* No need to propagate to resourcegroup children, they are already processed above */
        // ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "ResourceGroup"));
        // if (relatedEntityACLInfo.isNotEmpty())
        // propagate("ResourceGroup", relatedEntityACLInfo, roleIds);

        ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Resource"));
        if (relatedEntityACLInfo.isNotEmpty())
            propagate("Resource", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getResourceGroupRelatedCollections(List<Long> roleIds,
            List<IResourceGroup> resourceGroups) throws Hd3dException
    {
        Set<IResourceGroup> children = new HashSet<IResourceGroup>();
        Set<IResource> resources = new HashSet<IResource>();
        for (IResourceGroup resourceGroup : resourceGroups)
        {
            children.addAll(resourceGroup.allChildren(false));
            resources.addAll(resourceGroup.getResources());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("children", children);
        map.put("resources", resources);
        return map;
    }

}
