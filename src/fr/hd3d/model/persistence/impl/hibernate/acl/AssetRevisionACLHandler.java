package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionH;


public class AssetRevisionACLHandler extends ACLHandler
{

    public void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getAssetRevisionRelatedCollections(roleIds,
                Persistors.assetrevision.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getAssetRevisionRelatedCollections(roleIds,
                Persistors.assetrevision.getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getAssetRevisionRelatedCollections(roleIds,
                Persistors.assetrevision.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getAssetRevisionRelatedCollections(roleIds,
                Persistors.assetrevision.getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getAssetRevisionRelatedCollections(roleIds,
                Persistors.assetrevision.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getAssetRevisionRelatedCollections(roleIds,
                Persistors.assetrevision.getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, FileRevisionH.class, readPermittedCols.get("fileRevisions"), readBansCols
                .get("fileRevisions"), updatePermittedCols.get("fileRevisions"), udpateBansCols.get("fileRevisions"),
                deletePermittedCols.get("fileRevisions"), deleteBansCols.get("fileRevisions"));
    }

    protected Map<String, Set<? extends IBase>> getAssetRevisionRelatedCollections(List<Long> roleIds,
            List<IAssetRevision> assetRevisions) throws Hd3dException
    {
        Set<IFileRevision> fileRevisions = new HashSet<IFileRevision>();

        for (IAssetRevision assetRevision : assetRevisions)
        {
            fileRevisions.addAll(assetRevision.getFileRevisions());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("fileRevisions", fileRevisions);
        return map;
    }
}
