package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.SequenceH;
import fr.hd3d.model.persistence.impl.hibernate.ShotH;
import fr.hd3d.utils.ACLUtils;


public class SequenceACLHandler extends ACLHandler
{

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getSequenceRelatedCollections(roleIds,
                Persistors.sequence.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getSequenceRelatedCollections(roleIds, Persistors.sequence
                .getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getSequenceRelatedCollections(roleIds,
                Persistors.sequence.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getSequenceRelatedCollections(roleIds, Persistors.sequence
                .getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getSequenceRelatedCollections(roleIds,
                Persistors.sequence.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getSequenceRelatedCollections(roleIds, Persistors.sequence
                .getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, SequenceH.class, readPermittedCols.get("children"), readBansCols.get("children"),
                updatePermittedCols.get("children"), udpateBansCols.get("children"), deletePermittedCols
                        .get("children"), deleteBansCols.get("children"));

        updateOrCreateACL(info, ShotH.class, readPermittedCols.get("shots"), readBansCols.get("shots"),
                updatePermittedCols.get("shots"), udpateBansCols.get("shots"), deletePermittedCols.get("shots"),
                deleteBansCols.get("shots"));

        /* No need to propagate to sequence children, they are already processed above */
        // ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Sequence"));
        // propagate("Sequence", relatedEntityACLInfo, roleIds);

        ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Shot"));
        propagate("Shot", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getSequenceRelatedCollections(List<Long> roleIds,
            List<ISequence> sequences) throws Hd3dException
    {
        Set<ISequence> children = new HashSet<ISequence>();
        Set<IShot> shots = new HashSet<IShot>();
        for (ISequence category : sequences)
        {
            children.addAll(category.allChildren(false));
            shots.addAll(category.getShots());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("children", children);
        map.put("shots", shots);
        return map;
    }

}
