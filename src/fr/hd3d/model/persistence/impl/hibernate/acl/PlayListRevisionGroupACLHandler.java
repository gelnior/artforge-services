package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.PlayListRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.PlayListRevisionH;
import fr.hd3d.utils.ACLUtils;


public class PlayListRevisionGroupACLHandler extends ACLHandler
{

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getPlayListRevisionGroupRelatedCollections(roleIds,
                Persistors.playlistrevisiongroup.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getPlayListRevisionGroupRelatedCollections(roleIds,
                Persistors.playlistrevisiongroup.getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getPlayListRevisionGroupRelatedCollections(roleIds,
                Persistors.playlistrevisiongroup.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getPlayListRevisionGroupRelatedCollections(roleIds,
                Persistors.playlistrevisiongroup.getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getPlayListRevisionGroupRelatedCollections(roleIds,
                Persistors.playlistrevisiongroup.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getPlayListRevisionGroupRelatedCollections(roleIds,
                Persistors.playlistrevisiongroup.getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, PlayListRevisionGroupH.class, readPermittedCols.get("children"), readBansCols
                .get("children"), updatePermittedCols.get("children"), udpateBansCols.get("children"),
                deletePermittedCols.get("children"), deleteBansCols.get("children"));

        updateOrCreateACL(info, PlayListRevisionH.class, readPermittedCols.get("playListRevisions"), readBansCols
                .get("playListRevisions"), updatePermittedCols.get("playListRevisions"), udpateBansCols
                .get("playListRevisions"), deletePermittedCols.get("playListRevisions"), deleteBansCols
                .get("playListRevisions"));

        /* No need to propagate to PlayListRevisionGroup children, they are already processed above */
        // ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "PlayListRevisionGroup"));
        // propagate("PlayListRevisionGroup", relatedEntityACLInfo, roleIds);

        ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "PlayListRevision"));
        propagate("PlayListRevision", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getPlayListRevisionGroupRelatedCollections(List<Long> roleIds,
            List<IPlayListRevisionGroup> playListRevisionGroups) throws Hd3dException
    {
        Set<IPlayListRevisionGroup> children = new HashSet<IPlayListRevisionGroup>();
        Set<IPlayListRevision> playListRevisions = new HashSet<IPlayListRevision>();
        for (IPlayListRevisionGroup playListRevisionGroup : playListRevisionGroups)
        {
            children.addAll(playListRevisionGroup.allChildren(false));
            playListRevisions.addAll(playListRevisionGroup.getPlayListRevisions());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("children", children);
        map.put("playListRevisions", playListRevisions);
        return map;
    }

}
