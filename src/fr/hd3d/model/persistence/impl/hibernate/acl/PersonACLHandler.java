package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.AbsenceH;
import fr.hd3d.model.persistence.impl.hibernate.ActivityH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.PersonDayH;
import fr.hd3d.model.persistence.impl.hibernate.ResourceGroupH;
import fr.hd3d.model.persistence.impl.hibernate.TaskChangesH;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.utils.ACLUtils;


public class PersonACLHandler extends ACLHandler
{

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getPersonRelatedCollections(roleIds, Persistors.person
                .getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getPersonRelatedCollections(roleIds, Persistors.person
                .getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getPersonRelatedCollections(roleIds, Persistors.person
                .getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getPersonRelatedCollections(roleIds, Persistors.person
                .getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getPersonRelatedCollections(roleIds, Persistors.person
                .getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getPersonRelatedCollections(roleIds, Persistors.person
                .getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, PersonDayH.class, readPermittedCols.get("personDays"), readBansCols.get("personDays"),
                updatePermittedCols.get("personDays"), udpateBansCols.get("personDays"), deletePermittedCols
                        .get("personDays"), deleteBansCols.get("personDays"));

        updateOrCreateACL(info, ResourceGroupH.class, readPermittedCols.get("ledResourceGroups"), readBansCols
                .get("ledResourceGroups"), updatePermittedCols.get("ledResourceGroups"), udpateBansCols
                .get("ledResourceGroups"), deletePermittedCols.get("ledResourceGroups"), deleteBansCols
                .get("ledResourceGroups"));

        updateOrCreateACL(info, AbsenceH.class, readPermittedCols.get("absences"), readBansCols.get("absences"),
                updatePermittedCols.get("absences"), udpateBansCols.get("absences"), deletePermittedCols
                        .get("absences"), deleteBansCols.get("absences"));

        updateOrCreateACL(info, ActivityH.class, readPermittedCols.get("activities"), readBansCols.get("activities"),
                updatePermittedCols.get("activities"), udpateBansCols.get("activities"), deletePermittedCols
                        .get("activities"), deleteBansCols.get("activities"));

        updateOrCreateACL(info, AssetRevisionH.class, readPermittedCols.get("assetRevisions"), readBansCols
                .get("assetRevisions"), updatePermittedCols.get("assetRevisions"),
                udpateBansCols.get("assetRevisions"), deletePermittedCols.get("assetRevisions"), deleteBansCols
                        .get("assetRevisions"));

        updateOrCreateACL(info, TaskChangesH.class, readPermittedCols.get("taskChanges"), readBansCols
                .get("taskChanges"), updatePermittedCols.get("taskChanges"), udpateBansCols.get("taskChanges"),
                deletePermittedCols.get("taskChanges"), deleteBansCols.get("taskChanges"));

        updateOrCreateACL(info, TaskH.class, readPermittedCols.get("tasks"), readBansCols.get("tasks"),
                updatePermittedCols.get("tasks"), udpateBansCols.get("tasks"), deletePermittedCols.get("tasks"),
                deleteBansCols.get("tasks"));

        // map.put("absences", absences);
        // map.put("activities", activities);
        // map.put("assetRevisions", assetRevisions);
        // map.put("taskChanges", taskChanges);
        // map.put("tasks", tasks);

        ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "PersonDay"));
        propagate("PersonDay", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "ResourceGroup"));
        propagate("ResourceGroup", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Absence"));
        propagate("Absence", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Absence"));
        propagate("Absence", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Activity"));
        propagate("Activity", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "AssetRevision"));
        propagate("AssetRevision", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "TaskChanges"));
        propagate("TaskChanges", relatedEntityACLInfo, roleIds);
        relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "Task"));
        propagate("Task", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getPersonRelatedCollections(List<Long> roleIds, List<IPerson> persons)
            throws Hd3dException
    {
        Set<IPersonDay> personDays = new HashSet<IPersonDay>();
        Set<IResourceGroup> ledResourceGroups = new HashSet<IResourceGroup>();
        Set<IAbsence> absences = new HashSet<IAbsence>();
        Set<IActivity> activities = new HashSet<IActivity>();
        Set<IAssetRevision> assetRevisions = new HashSet<IAssetRevision>();
        Set<ITaskChanges> taskChanges = new HashSet<ITaskChanges>();
        Set<ITask> tasks = new HashSet<ITask>();

        for (IPerson person : persons)
        {
            personDays.addAll(person.getDays());
            ledResourceGroups.addAll(person.getLedResourceGroups());
            absences.addAll(Persistors.absence.getByValue("worker", person));
            activities.addAll(Persistors.activity.getByValue("filledBy", person));
            assetRevisions.addAll(Persistors.assetrevision.getByValue("creator", person));
            taskChanges.addAll(Persistors.taskChanges.getByValue("worker", person));
            tasks.addAll(Persistors.task.getByValue("worker", person));
            tasks.addAll(Persistors.task.getByValue("creator", person));
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("personDays", personDays);
        map.put("ledResourceGroups", ledResourceGroups);
        map.put("absences", absences);
        map.put("activities", activities);
        map.put("assetRevisions", assetRevisions);
        map.put("taskChanges", taskChanges);
        map.put("tasks", tasks);

        return map;
    }

}
