package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.utils.ACLUtils;
import fr.hd3d.utils.CollectUtils;


/**
 * Parses ACLs and give access to permissions in a convenient way.
 * 
 * @author tlam
 * 
 */
public class ACLInfo
{

    public static enum EACL_Permission
    {
        READ_PERMITTED, READ_BANS, CREATE_PERMITTED, UPDATE_PERMITTED, UPDATE_BANS, DELETE_PERMITTED, DELETE_BANS;
    }

    private Collection<IHd3dACL> acls;

    private boolean allReadPermitted = false;
    private boolean allUpdatePermitted = false;
    private boolean allDeletePermitted = false;

    private List<Long> readPermittedIds = new ArrayList<Long>();
    private List<Long> readNotPermittedIds = new ArrayList<Long>();

    private Set<String> createPermittedString;
    private boolean createPermitted;

    private List<Long> updatePermittedIds = new ArrayList<Long>();
    private List<Long> updateNotPermittedIds = new ArrayList<Long>();
    private List<Long> deletePermittedIds = new ArrayList<Long>();
    private List<Long> deleteNotPermittedIds = new ArrayList<Long>();

    private List<Criterion> readPermittedCriterionList = new ArrayList<Criterion>();
    private List<Criterion> readNotPermittedCriterionList = new ArrayList<Criterion>();
    private List<Criterion> updatePermittedCriterionList = new ArrayList<Criterion>();
    private List<Criterion> updateNotPermittedCriterionList = new ArrayList<Criterion>();
    private List<Criterion> deletePermittedCriterionList = new ArrayList<Criterion>();
    private List<Criterion> deleteNotPermittedCriterionList = new ArrayList<Criterion>();

    public ACLInfo()
    {}

    public ACLInfo(Collection<IHd3dACL> acls) throws Hd3dException
    {
        if (CollectUtils.isNotEmpty(acls))
        {
            checkUniqueEntityName(acls);

            this.acls = acls;

            this.createPermittedString = ACLUtils.getOperationPermissions(acls, EACL_Permission.CREATE_PERMITTED);
            this.createPermitted = inferCreatePermitted(createPermittedString);

            process(readPermittedIds, readPermittedCriterionList, EACL_Permission.READ_PERMITTED);
            process(readNotPermittedIds, readNotPermittedCriterionList, EACL_Permission.READ_BANS);
            process(updatePermittedIds, updatePermittedCriterionList, EACL_Permission.UPDATE_PERMITTED);
            process(updateNotPermittedIds, updateNotPermittedCriterionList, EACL_Permission.UPDATE_BANS);
            process(deletePermittedIds, deletePermittedCriterionList, EACL_Permission.DELETE_PERMITTED);
            process(deleteNotPermittedIds, deleteNotPermittedCriterionList, EACL_Permission.DELETE_BANS);
        }
    }

    private void checkUniqueEntityName(Collection<IHd3dACL> acls) throws Hd3dException
    {
        /* check if entityName is the same for all acls */
        Collection<String> entityNames = CollectionUtils.collect(acls, new Transformer<IHd3dACL, String>() {

            public String transform(IHd3dACL acl)
            {
                return acl.getEntityName();
            }
        });

        boolean entityNameIsUnique = new HashSet<String>(entityNames).size() == 1;
        if (!entityNameIsUnique)
        {
            throw new Hd3dException("entityName  is not unique");
        }
    }

    private boolean inferCreatePermitted(Set<String> createPermittedString)
    {
        for (String createPermission : createPermittedString)
        {
            if (StringUtils.contains(createPermission, "*"))
            {
                return true;
            }
        }
        return false;
    }

    public boolean isAllReadPermitted()
    {
        return allReadPermitted;
    }

    public boolean isAllUpdatePermitted()
    {
        return allUpdatePermitted;
    }

    public boolean isAllDeletePermitted()
    {
        return allDeletePermitted;
    }

    public Collection<IHd3dACL> getAcls()
    {
        return acls;
    }

    public boolean isCreatePermitted()
    {
        return createPermitted;
    }

    public Collection<Long> getReadPermittedIds()
    {
        return readPermittedIds;
    }

    public void setReadPermittedIds(List<Long> readPermittedIds)
    {
        this.readPermittedIds = readPermittedIds;
    }

    public Collection<Long> getReadNotPermittedIds()
    {
        return readNotPermittedIds;
    }

    public void setReadNotPermittedIds(List<Long> readNotPermittedIds)
    {
        this.readNotPermittedIds = readNotPermittedIds;
    }

    public List<Long> getUpdatePermittedIds()
    {
        return updatePermittedIds;
    }

    public void setUpdatePermittedIds(List<Long> updatePermittedIds)
    {
        this.updatePermittedIds = updatePermittedIds;
    }

    public List<Long> getUpdateNotPermittedIds()
    {
        return updateNotPermittedIds;
    }

    public void setUpdateNotPermittedIds(List<Long> updateNotPermittedIds)
    {
        this.updateNotPermittedIds = updateNotPermittedIds;
    }

    public List<Long> getDeletePermittedIds()
    {
        return deletePermittedIds;
    }

    public void setDeletePermittedIds(List<Long> deletePermittedIds)
    {
        this.deletePermittedIds = deletePermittedIds;
    }

    public List<Long> getDeleteNotPermittedIds()
    {
        return deleteNotPermittedIds;
    }

    public void setDeleteNotPermittedIds(List<Long> deleteNotPermittedIds)
    {
        this.deleteNotPermittedIds = deleteNotPermittedIds;
    }

    public List<Criterion> getReadPermittedCriterionList()
    {
        return readPermittedCriterionList;
    }

    public List<Criterion> getUpdatePermittedCriterionList()
    {
        return updatePermittedCriterionList;
    }

    public List<Criterion> getDeletePermittedCriterionList()
    {
        return deletePermittedCriterionList;
    }

    public List<Criterion> getReadNotPermittedCriterionList()
    {
        return readNotPermittedCriterionList;
    }

    public List<Criterion> getUpdateNotPermittedCriterionList()
    {
        return updateNotPermittedCriterionList;
    }

    public List<Criterion> getDeleteNotPermittedCriterionList()
    {
        return deleteNotPermittedCriterionList;
    }

    public void setAllReadPermitted(boolean allReadPermitted)
    {
        this.allReadPermitted = allReadPermitted;
    }

    public void setCreatePermitted(boolean createPermitted)
    {
        this.createPermitted = createPermitted;
    }

    public void setReadPermittedCriterionList(List<Criterion> readPermittedCriterionList)
    {
        this.readPermittedCriterionList = readPermittedCriterionList;
    }

    public void setReadNotPermittedCriterionList(List<Criterion> readNotPermittedCriterionList)
    {
        this.readNotPermittedCriterionList = readNotPermittedCriterionList;
    }

    public void setUpdatePermittedCriterionList(List<Criterion> updatePermittedCriterionList)
    {
        this.updatePermittedCriterionList = updatePermittedCriterionList;
    }

    public void setUpdateNotPermittedCriterionList(List<Criterion> updateNotPermittedCriterionList)
    {
        this.updateNotPermittedCriterionList = updateNotPermittedCriterionList;
    }

    public void setDeletePermittedCriterionList(List<Criterion> deletePermittedCriterionList)
    {
        this.deletePermittedCriterionList = deletePermittedCriterionList;
    }

    public void setDeleteNotPermittedCriterionList(List<Criterion> deleteNotPermittedCriterionList)
    {
        this.deleteNotPermittedCriterionList = deleteNotPermittedCriterionList;
    }

    private void process(List<Long> permissionIds, List<Criterion> criterionList, EACL_Permission permissionType)
    {
        /* find out permitted/not permitted read ids */
        List<String> tokens;
        for (String permission : ACLUtils.getOperationPermissions(acls, permissionType))
        {
            tokens = Arrays.asList(StringUtils.split(permission, ' '));

            /* all allowed ? */
            if (permissionType == EACL_Permission.READ_PERMITTED)
            {
                allReadPermitted = tokens.contains("*");
            }
            else if (permissionType == EACL_Permission.UPDATE_PERMITTED)
            {
                allUpdatePermitted = tokens.contains("*");
            }
            else if (permissionType == EACL_Permission.DELETE_PERMITTED)
            {
                allDeletePermitted = tokens.contains("*");
            }

            /* allowed ids */
            permissionIds.addAll(CollectUtils.toLong(CollectionUtils.select(tokens, new Predicate<String>() {
                public boolean evaluate(String id)
                {
                    return !"*".equals(id);
                }
            })));
        }

        /* build Criterion based on ids */
        // if (CollectUtils.isNotEmpty(permissionIds))
        // {
        // if (permissionType == EACL_Permission.READ_PERMITTED || permissionType == EACL_Permission.UPDATE_PERMITTED
        // || permissionType == EACL_Permission.DELETE_PERMITTED)
        // {
        // criterionList.add(Restrictions.in("id", permissionIds));
        // }
        // if (permissionType == EACL_Permission.READ_BANS || permissionType == EACL_Permission.UPDATE_BANS
        // || permissionType == EACL_Permission.DELETE_BANS)
        // {
        // criterionList.add(Restrictions.not(Restrictions.in("id", permissionIds)));
        // }
        // }
    }

    public Set<IRole> getACLRoles()
    {
        Set<IRole> roles = new HashSet<IRole>();
        if (CollectUtils.isNotEmpty(acls))
        {
            for (IHd3dACL acl : acls)
            {
                roles.addAll(acl.getRoles());
            }
        }
        return roles;
    }

    public boolean isEmpty()
    {
        return CollectUtils.isEmpty(readPermittedIds) && CollectUtils.isEmpty(readNotPermittedIds)
                && CollectUtils.isEmpty(createPermittedString) && CollectUtils.isEmpty(updatePermittedIds)
                && CollectUtils.isEmpty(updateNotPermittedIds) && CollectUtils.isEmpty(deletePermittedIds)
                && CollectUtils.isEmpty(deleteNotPermittedIds);
    }

    public boolean isNotEmpty()
    {
        return !isEmpty();
    }
}
