package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ExtraLineH;
import fr.hd3d.model.persistence.impl.hibernate.TaskGroupH;
import fr.hd3d.utils.ACLUtils;


public class TaskGroupACLHandler extends ACLHandler
{

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getTaskGroupRelatedCollections(roleIds,
                Persistors.taskGroup.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getTaskGroupRelatedCollections(roleIds, Persistors.taskGroup
                .getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getTaskGroupRelatedCollections(roleIds,
                Persistors.taskGroup.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getTaskGroupRelatedCollections(roleIds, Persistors.taskGroup
                .getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getTaskGroupRelatedCollections(roleIds,
                Persistors.taskGroup.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getTaskGroupRelatedCollections(roleIds, Persistors.taskGroup
                .getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, TaskGroupH.class, readPermittedCols.get("children"), readBansCols.get("children"),
                updatePermittedCols.get("children"), udpateBansCols.get("children"), deletePermittedCols
                        .get("children"), deleteBansCols.get("children"));

        updateOrCreateACL(info, ExtraLineH.class, readPermittedCols.get("extraLines"), readBansCols.get("extraLines"),
                updatePermittedCols.get("extraLines"), udpateBansCols.get("extraLines"), deletePermittedCols
                        .get("extraLines"), deleteBansCols.get("extraLines"));

        /* No need to propagate to resourcegroup children, they are already processed above */
        // ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "TaskGroup"));
        // propagate("TaskGroup", relatedEntityACLInfo, roleIds);
        ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "ExtraLine"));
        propagate("ExtraLine", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getTaskGroupRelatedCollections(List<Long> roleIds,
            List<ITaskGroup> taskGroups) throws Hd3dException
    {
        Set<ITaskGroup> children = new HashSet<ITaskGroup>();
        Set<IExtraLine> extraLines = new HashSet<IExtraLine>();
        for (ITaskGroup taskGroup : taskGroups)
        {
            children.addAll(taskGroup.allChildren(false));
            extraLines.addAll(taskGroup.getExtraLines());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("children", children);
        map.put("extraLines", extraLines);
        return map;
    }

}
