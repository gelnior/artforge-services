package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.List;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;


public class DefaultACLHandler extends ACLHandler
{
    public DefaultACLHandler()
    {}

    public DefaultACLHandler(IBase object)
    {
        super(object);
    }

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
    /* no propagation */
    }
}
