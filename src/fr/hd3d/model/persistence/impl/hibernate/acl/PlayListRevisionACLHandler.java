package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.PlayListEditH;
import fr.hd3d.utils.ACLUtils;


public class PlayListRevisionACLHandler extends ACLHandler
{

    protected void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        Map<String, Set<? extends IBase>> readPermittedCols = getPlayListRevisionRelatedCollections(roleIds,
                Persistors.playlistrevision.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getPlayListRevisionRelatedCollections(roleIds,
                Persistors.playlistrevision.getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getPlayListRevisionRelatedCollections(roleIds,
                Persistors.playlistrevision.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getPlayListRevisionRelatedCollections(roleIds,
                Persistors.playlistrevision.getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getPlayListRevisionRelatedCollections(roleIds,
                Persistors.playlistrevision.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getPlayListRevisionRelatedCollections(roleIds,
                Persistors.playlistrevision.getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, PlayListEditH.class, readPermittedCols.get("playListEdits"), readBansCols
                .get("playListEdits"), updatePermittedCols.get("playListEdits"), udpateBansCols.get("playListEdits"),
                deletePermittedCols.get("playListEdits"), deleteBansCols.get("playListEdits"));

        ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "PlayListEdit"));
        propagate("PlayListEdit", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getPlayListRevisionRelatedCollections(List<Long> roleIds,
            List<IPlayListRevision> playListRevisions) throws Hd3dException
    {
        Set<IPlayListEdit> playListEdits = new HashSet<IPlayListEdit>();
        for (IPlayListRevision playListRevision : playListRevisions)
        {
            playListEdits.addAll(playListRevision.getPlayListEdits());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("playListEdits", playListEdits);
        return map;
    }

}
