package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionH;


public class AssetRevisionGroupACLHandler extends ACLHandler
{

    public void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        /* depending on permissions for the projects, change the other entities permissions */
        Map<String, Set<? extends IBase>> readPermittedCols = getAssetRevisionGroupRelatedCollections(roleIds,
                Persistors.assetrevisiongroup.getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getAssetRevisionGroupRelatedCollections(roleIds,
                Persistors.assetrevisiongroup.getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getAssetRevisionGroupRelatedCollections(roleIds,
                Persistors.assetrevisiongroup.getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getAssetRevisionGroupRelatedCollections(roleIds,
                Persistors.assetrevisiongroup.getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getAssetRevisionGroupRelatedCollections(roleIds,
                Persistors.assetrevisiongroup.getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getAssetRevisionGroupRelatedCollections(roleIds,
                Persistors.assetrevisiongroup.getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, AssetRevisionH.class, readPermittedCols.get("assetRevisions"), readBansCols
                .get("assetRevisions"), updatePermittedCols.get("assetRevisions"),
                udpateBansCols.get("assetRevisions"), deletePermittedCols.get("assetRevisions"), deleteBansCols
                        .get("assetRevisions"));
    }

    protected Map<String, Set<? extends IBase>> getAssetRevisionGroupRelatedCollections(List<Long> roleIds,
            List<IAssetRevisionGroup> assetRevisionGroups) throws Hd3dException
    {
        Set<IAssetRevision> assetRevisions = new HashSet<IAssetRevision>();

        for (IAssetRevisionGroup assetRevisionGroup : assetRevisionGroups)
        {
            assetRevisions.addAll(assetRevisionGroup.getAssetRevisions());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("assetRevisions", assetRevisions);
        return map;
    }
}
