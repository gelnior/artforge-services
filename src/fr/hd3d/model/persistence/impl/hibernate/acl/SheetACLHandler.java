package fr.hd3d.model.persistence.impl.hibernate.acl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ItemGroupH;
import fr.hd3d.utils.ACLUtils;


public class SheetACLHandler extends ACLHandler
{

    public void propagatePermissions(String entityName, ACLInfo info, List<Long> roleIds) throws Hd3dException
    {
        if (info.isEmpty())
        {
            return;
        }

        /* depending on permissions for the projects, change the other entities permissions */
        Map<String, Set<? extends IBase>> readPermittedCols = getSheetRelatedCollections(roleIds, Persistors.sheet
                .getByIds_NoPermCheck(info.getReadPermittedIds()));
        Map<String, Set<? extends IBase>> readBansCols = getSheetRelatedCollections(roleIds, Persistors.sheet
                .getByIds_NoPermCheck(info.getReadNotPermittedIds()));
        Map<String, Set<? extends IBase>> updatePermittedCols = getSheetRelatedCollections(roleIds, Persistors.sheet
                .getByIds_NoPermCheck(info.getUpdatePermittedIds()));
        Map<String, Set<? extends IBase>> udpateBansCols = getSheetRelatedCollections(roleIds, Persistors.sheet
                .getByIds_NoPermCheck(info.getUpdateNotPermittedIds()));
        Map<String, Set<? extends IBase>> deletePermittedCols = getSheetRelatedCollections(roleIds, Persistors.sheet
                .getByIds_NoPermCheck(info.getDeletePermittedIds()));
        Map<String, Set<? extends IBase>> deleteBansCols = getSheetRelatedCollections(roleIds, Persistors.sheet
                .getByIds_NoPermCheck(info.getDeleteNotPermittedIds()));

        updateOrCreateACL(info, ItemGroupH.class, readPermittedCols.get("itemGroups"), readBansCols.get("itemGroups"),
                updatePermittedCols.get("itemGroups"), udpateBansCols.get("itemGroups"), deletePermittedCols
                        .get("itemGroups"), deleteBansCols.get("itemGroups"));

        ACLInfo relatedEntityACLInfo = new ACLInfo(ACLUtils.getACLs(roleIds, "ItemGroup"));
        propagate("ItemGroup", relatedEntityACLInfo, roleIds);
    }

    protected Map<String, Set<? extends IBase>> getSheetRelatedCollections(List<Long> roleIds, List<ISheet> sheets)
            throws Hd3dException
    {
        Set<IItemGroup> itemGroups = new HashSet<IItemGroup>();

        for (ISheet sheet : sheets)
        {
            itemGroups.addAll(sheet.getItemGroups());
        }

        Map<String, Set<? extends IBase>> map = new HashMap<String, Set<? extends IBase>>();
        map.put("itemGroups", itemGroups);
        return map;
    }
}
