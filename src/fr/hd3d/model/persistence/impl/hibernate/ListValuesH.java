package fr.hd3d.model.persistence.impl.hibernate;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILListValues;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;


/**
 * @author Try LAM
 */
@Entity
@Table(name = "hd3d_listvalues")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ListValuesH extends BaseH implements IListValues
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String separator;
    private String values;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_listvalues"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ListValuesH()
    {}

    public ListValuesH(Long id, Timestamp version, String separator, String values)
    {
        super(id, version);
        setSeparator(separator);
        this.values = values;
    }

    public ListValuesH(String separator, String values)
    {
        this(null, null, separator, values);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "listvalues_separator", nullable = false)
    @NotEmpty
    public String getSeparator()
    {
        return separator;
    }

    public void setSeparator(String separator)
    {
        this.separator = separator == null ? Const.DYNMETADATATYPE_LIST_DEFAULT_SEP : separator;
    }

    @Column(name = "listvalues_values", columnDefinition = "TEXT")
    public String getValues()
    {
        return values;
    }

    public void setValues(String values)
    {
        this.values = values;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((separator == null) ? 0 : separator.hashCode());
        result = prime * result + ((values == null) ? 0 : values.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ListValuesH other = (ListValuesH) obj;
        if (separator == null)
        {
            if (other.separator != null)
                return false;
        }
        else if (!separator.equals(other.separator))
            return false;
        if (values == null)
        {
            if (other.values != null)
                return false;
        }
        else if (!values.equals(other.values))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    public IBaseTranslator<ILListValues, IListValues> defaultTranslator()
    {
        return Translators.listvalues;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return new ToStringBuilder(this).appendSuper(super.toString()).append("separator", separator).append("values",
                values).toString();
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
