package fr.hd3d.model.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dExposed;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;
import fr.hd3d.model.translator.IBaseTranslator;


/**
 * 
 * <b>Base interface</b> for all hd3d <b>persisted</b> business classes
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public interface IBase extends Serializable, Persistent
{
    String entityName();

    boolean isTaskable();

    boolean isApprovable();

    String getInternalUUID();

    void setInternalUUID(final String uuid);

    Integer getInternalStatus();

    void setInternalStatus(Integer internalStatus);

    Set<TagParent> getTagParents();

    void setTagParents(Set<TagParent> parents) throws Hd3dException;

    void addTag(Long tagId) throws Hd3dException;

    void removeTag(Long tagId);

    List<ITag> tags();

    void mergeTags(List<Long> tagIds) throws Hd3dException;

    <T extends IBase> void merge(Collection<T> coll, Collection<Long> ids, Class<T> clazz) throws Hd3dException;

    Set<IClassDynMetaDataType> classDynMetaDataTypes();

    List<IDynMetaDataValue> getDynMetaDataValues();

    IDynMetaDataValue getDynMetaDataValue(Long metaDataId);

    void removeDynMetaDataValue(IDynMetaDataValue dynMetaDataValue) throws Hd3dException;

    void setDynMetaDataValue(Long metaDataId, String value) throws Hd3dException;

    void doPreRead() throws Hd3dException;

    void doPostRead() throws Hd3dException;

    void doPrePersist() throws Hd3dException;

    void doPostPersist() throws Hd3dException;

    <L extends ILBase> void doPreUpdate(L lobject) throws Hd3dException;

    void doPreUpdate() throws Hd3dException;

    void doPostUpdate() throws Hd3dException;

    // <L extends ILBase> void doPostUpdate(L lobject) throws Hd3dException;

    void doPreDelete() throws Hd3dException;

    void doPostDelete() throws Hd3dException;

    Collection<IDynMetaDataValue> getDynMetaDataValues(IClassDynMetaDataType type);

    void removeDynMetaDataValues(IClassDynMetaDataType type) throws Hd3dException;

    List<IApprovalNote> getApprovalNotes();

    @Deprecated
    List<IApprovalNote> getApprovalNotesByTypes(String[] types);

    List<IApprovalNote> getApprovalNotesByApprovalType(Long approvalTypeId);

    void setApprovalNotes(List<IApprovalNote> approvalNotes) throws Hd3dException;

    List<IEntityTaskLink> getEntityTaskLinks();

    void setEntityTaskLinks(List<IEntityTaskLink> entityTaskLinks) throws Hd3dException;

    @Hd3dExposed
    List<ITask> boundTasks();

    int hashCode();

    boolean equals(Object obj);

    abstract IBaseTranslator<?, ?> defaultTranslator();

    void completePrePersistOrUpdate();

    void enable();

    void disable() throws Hd3dException;

    boolean enabled();

    void postDisable() throws Hd3dException;

    boolean canRead();

    boolean canUpdate();

    boolean canDelete();

    boolean canPersist();

    String defaultPath();

    // APPROVAL NOTE RELATED METHOD EXPOSED
    @Hd3dExposed
    List<IApprovalNote> approvalNotes(String approvalTypeId);

    // TASK RELATED METHOD EXPOSED
    @Hd3dExposed
    Long tasksEstimation(String taskTypeId);

    @Hd3dExposed
    Long tasksActivitiesDuration(String taskTypeId);

    @Hd3dExposed
    Long tasksDurationGap(String taskTypeId);

    @Hd3dExposed
    String tasksStatus(String taskTypeId);

    @Hd3dExposed
    List<IPerson> tasksWorkers(String taskTypeId);

    @Hd3dExposed
    Long activitiesDuration();

}
