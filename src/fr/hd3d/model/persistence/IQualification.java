package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for CPU informations needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Qualification")
public interface IQualification extends ISimple
{
    Set<ISkill> getSkills();

    void setSkills(Set<ISkill> skills);

    Set<IContract> getContracts();

    void setContracts(Set<IContract> contracts);
}
