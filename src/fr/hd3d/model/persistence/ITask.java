package fr.hd3d.model.persistence;

import java.util.Date;
import java.util.List;
import java.util.Set;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dEntityName;
import fr.hd3d.model.annotation.Hd3dExposed;


/**
 * A34 time sheet implementation https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("Task")
public interface ITask extends ITaskBase, Project, Name
{
    ETaskStatus getStatus();

    Byte getCompletion();

    Date getCompletionDate();

    void setStatus(ETaskStatus status);

    void setCompletion(Byte completion);

    void setCompletionDate(Date completionDate);

    Set<ITaskActivity> getTaskActivities();

    Set<ITaskChanges> getTaskChanges();

    IPerson getCreator();

    IProject getProject();

    void setTaskActivities(Set<ITaskActivity> tasks);

    void setTaskChanges(Set<ITaskChanges> taskChanges);

    void setCreator(IPerson creator);

    void setWorker(IPerson worker);

    void setProject(IProject project);

    Date getDeadLine();

    void setDeadLine(Date deadLineDate);

    Date getActualStartDate();

    void setActualStartDate(Date actualStartDate);

    Date getActualEndDate();

    void setActualEndDate(Date actualEndDate);

    Boolean getStartable();

    void setStartable(Boolean startable);

    Boolean getConfirmed();

    void setConfirmed(Boolean confirmed);

    Boolean getStartup();

    void setStartup(Boolean startup);

    ITaskType getTaskType();

    void setTaskType(ITaskType taskType);

    IEntityTaskLink getBoundEntityTaskLink();

    void setBoundEntityTaskLink(IEntityTaskLink entityTaskLink);

    List<IEntityTaskLink> getBoundEntityTaskLinks();

    void setBoundEntityTaskLinks(List<IEntityTaskLink> entityTaskLinks);

    boolean hasActivities();

    boolean hasNotEmptyActivities();

    String getCommentForApprovalNote();

    void setCommentForApprovalNote(String commentForApprovalNote);

    Set<IFileRevision> getFileRevisionsForApprovalNote();

    void setFileRevisionsForApprovalNote(Set<IFileRevision> fileRevision);

    @Hd3dExposed
    IBase boundEntity() throws Hd3dException;

    Long getBoundEntityId();

    void setBoundEntityId(Long boundEntityId);

    String getBoundEntityName() throws Hd3dException;

    void setBoundEntityName(String boundEntityName);

    String getBoundEntityNameLowerCase() throws Hd3dException;

    boolean isUpdatedByApprovalNote();

    void setUpdatedByApprovalNote(boolean isUpdatedByApprovalNote);

    Long getTotalActivitiesDuration();

    void setTotalActivitiesDuration(Long totalActivitiesDuration);

}
