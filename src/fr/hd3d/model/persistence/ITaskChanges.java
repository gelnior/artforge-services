package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author michael.guiral
 * 
 */
@Hd3dEntityName("TaskChanges")
public interface ITaskChanges extends ITaskBase
{
    ITask getTask();

    void setTask(ITask task);

    IPlanning getPlanning();

    void setPlanning(IPlanning planning);

    void updateTask();

    Integer getIndex();

    void setIndex(Integer index);
}
