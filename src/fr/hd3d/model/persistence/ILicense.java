package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.common.client.enums.ELicenseType;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for software license informations needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("License")
public interface ILicense extends IInventoryItem
{
    Integer getNumber();

    ELicenseType getType();

    ISoftware getSoftware();

    Set<IComputer> getComputers();

    void setNumber(Integer number);

    void setType(ELicenseType type);

    void setSoftware(ISoftware software);

    void setComputers(Set<IComputer> computers);
}
