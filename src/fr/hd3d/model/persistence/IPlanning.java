package fr.hd3d.model.persistence;

import java.util.List;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("Planning")
public interface IPlanning extends IBase, IDurable, Project, Name
{

    void setProject(IProject project);

    IProject getProject();

    Boolean isMaster();

    void setMaster(Boolean master);

    List<ITaskGroup> getTaskGroups();

    void setTaskGroups(List<ITaskGroup> taskGroups);

    List<ITaskChanges> getTaskChanges();

    void setTaskChanges(List<ITaskChanges> taskChanges);

    void addTaskGroup(ITaskGroup taskGroup);

    void addTaskChanges(ITaskChanges taskChanges);

    void cleanDates() throws Hd3dException;
}
