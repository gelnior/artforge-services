package fr.hd3d.model.persistence;

import java.util.Date;
import java.util.Set;

import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.common.client.enums.EFileStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Try LAM
 */
@Hd3dEntityName("FileRevision")
public interface IFileRevision extends IBase
{
    String getUriScheme();

    void setUriScheme(String uriScheme);

    Set<IFileRevisionLink> getFileRevisionLinks_ThisAsInput() throws Hd3dException;

    Set<IFileRevisionLink> getFileRevisionLinks_ThisAsOutput() throws Hd3dException;

    Set<IFileRevisionLink> getFileRevisionLinks() throws Hd3dException;

    // Set<IAssetRevisionFileRevision> getAssetRevisionFileRevisions() throws Hd3dException;

    Date getCreationDate();

    String getKey();

    String getVariation();

    Integer getRevision();

    IPerson getCreator();

    String getFormat();

    Long getSize();

    String getChecksum();

    String getLocation();

    String getRelativePath();

    EFileStatus getStatus();

    Date getLastOperationDate();

    IPerson getLastUser();

    String getComment();

    String getValidity();

    String getSequence();

    String getFullPath();

    Set<IFileAttribute> getAttributes();

    IAssetRevision getAssetRevision();

    EFileState getState();

    Set<IProxy> getProxies();

    String getMountPoint();

    void setMountPoint(String mountPoint);

    void addAttribute(IFileAttribute attribute);

    void removeAttribute(IFileAttribute attribute);

    void setCreationDate(Date creationDate);

    void setKey(String key);

    void setVariation(String variation);

    void setRevision(Integer revision);

    void setCreator(IPerson creator);

    void setFormat(String format);

    void setSize(Long size);

    void setChecksum(String checksum);

    void setLocation(String location);

    void setRelativePath(String relativePath);

    void setStatus(EFileStatus status);

    void setLastOperationDate(Date lastOperationDate);

    void setLastUser(IPerson lastUser);

    void setComment(String comment);

    void setValidity(String validity);

    void setSequence(String sequence);

    void setAttributes(Set<IFileAttribute> attributes);

    void setAssetRevision(IAssetRevision assetRevision);

    void setState(EFileState state);

    void setProxies(Set<IProxy> proxies);
}
