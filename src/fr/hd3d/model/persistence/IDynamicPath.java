package fr.hd3d.model.persistence;

import java.util.Collection;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * 
 * @author HD3D
 * 
 */
@Hd3dEntityName("DynamicPath")
public interface IDynamicPath extends IBase, Name
{
    IProject getProject();

    Long getBoundEntityId();

    String getBoundEntityName();

    ITaskType getTaskType();

    String getWipDynamicPath();

    String getPublishDynamicPath();

    String getOldDynamicPath();

    IDynamicPath getParent();

    String getType();

    Collection<IDynamicPath> getChildren();

    void setType(String type);

    void setProject(final IProject project);

    void setBoundEntityId(final Long boundEntityId);

    void setBoundEntityName(final String boundEntityName);

    void setTaskType(final ITaskType taskType);

    void setWipDynamicPath(final String wipDynamicPath);

    void setPublishDynamicPath(final String publishDynamicPath);

    void setOldDynamicPath(final String oldDynamicPath);

    void setParent(IDynamicPath parent);

    void setChildren(Collection<IDynamicPath> children);

    IBase boundEntity() throws Hd3dException;

    Collection<IDynamicPath> allChildren(boolean includeSelf);

}
