/**
 * 
 */
package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Try LAM
 * 
 */
@Hd3dEntityName("ProjectType")
public interface IProjectType extends IBase, Name
{

    String getColor();

    String getDescription();

    void setColor(String color);

    void setDescription(String description);

}
