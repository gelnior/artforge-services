package fr.hd3d.model.persistence;

import java.util.Collection;
import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * a category has one and only one parent.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("Category")
public interface ICategory extends IBase, Project, Name, IPath
{
    ICategory getParent();

    IProject getProject();

    Set<ICategory> parents();

    String getParentIds();

    Collection<ICategory> getChildren();

    Collection<ICategory> allChildren(boolean includeSelf);

    Set<IConstituent> getConstituents();

    void setParent(ICategory parent);

    void setProject(IProject project);

    void setChildren(Collection<ICategory> children);

    void setConstituents(Set<IConstituent> constituents);

    boolean isRoot();

    String getPath();

    String path(String separator, boolean includeProject);
}
