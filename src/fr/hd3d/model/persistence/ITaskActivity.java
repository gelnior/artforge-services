package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("TaskActivity")
public interface ITaskActivity extends IActivity
{
    ITask getTask();

    void setTask(ITask task);
}
