package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;
import fr.hd3d.model.annotation.Hd3dExposed;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("Shot")
public interface IShot extends IBase, IPath, Hook
{
    ISequence getSequence();

    String getLabel();

    String getDescription();

    Integer getDifficulty();

    Integer getTrust();

    Boolean getMattePainting();

    Boolean getLayout();

    Boolean getAnimation();

    Boolean getExport();

    Boolean getTracking();

    Boolean getDressing();

    Boolean getLighting();

    Boolean getRendering();

    Boolean getCompositing();

    Integer getCompletion();

    Integer getNbFrame();

    Integer getStartFrame();

    Integer getEndFrame();

    String path(String separator, boolean includeProject);

    void setSequence(ISequence sequence);

    void setLabel(String label);

    void setDescription(String desc);

    void setDifficulty(Integer difficulty);

    void setTrust(Integer trust);

    void setMattePainting(Boolean mattePainting);

    void setLayout(Boolean layout);

    void setAnimation(Boolean animation);

    void setExport(Boolean export);

    void setTracking(Boolean tracking);

    void setDressing(Boolean dressing);

    void setLighting(Boolean lighting);

    void setRendering(Boolean rendering);

    void setCompositing(Boolean compositing);

    void setCompletion(Integer completion);

    void setNbFrame(Integer nBFrame);

    void setStartFrame(Integer startFrame);

    void setEndFrame(Integer endFrame);

    @Hd3dExposed
    Long shotStartFrameEpisode();

    @Hd3dExposed
    String thumbnail();

}
