package fr.hd3d.model.persistence;

import java.util.Date;
import java.util.List;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Try LAM
 */
@Hd3dEntityName("ApprovalNote")
public interface IApprovalNote extends IBase
{
    Long getBoundEntity();

    String getBoundEntityName();

    Date getApprovalDate();

    String getStatus();

    String getComment();

    IPerson getApprover();

    Set<IFileRevision> getFilerevisions();

    String getType();

    IApprovalNoteType getApprovalNoteType();

    String getMedia();

    Set<IGraphicalAnnotation> getGraphicalAnnotations();

    void setBoundEntity(Long boundEntity);

    void setBoundEntityName(String boundEntityName);

    void setApprovalDate(Date approvalDate);

    void setStatus(String status);

    void setComment(String comment);

    void setApprover(IPerson approver);

    void setFilerevisions(Set<IFileRevision> filerevisions);

    void setType(String type);

    void setApprovalNoteType(IApprovalNoteType approvalNoteType);

    void setMedia(String media);

    List<IAssetRevision> assetRevisions() throws Hd3dException;

    void setGraphicalAnnotations(Set<IGraphicalAnnotation> graphicalAnnotations);

    boolean isUpdatedByTask();

    void setUpdatedByTask(boolean isUpdatedByTask);
}
