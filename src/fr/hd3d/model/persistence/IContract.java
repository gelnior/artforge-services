package fr.hd3d.model.persistence;

import java.util.Date;

import fr.hd3d.common.client.enums.EContractType;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for contract informations needed by the resource application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Contract")
public interface IContract extends ISimple
{

    Date getStartDate();

    void setStartDate(Date startDate);

    Date getEndDate();

    void setEndDate(Date endDate);

    Date getRealEndDate();

    void setRealEndDate(Date realEndDate);

    String getDocPath();

    void setDocPath(String docPath);

    EContractType getType();

    void setType(EContractType type);

    IPerson getPerson();

    void setPerson(IPerson person);

    IQualification getQualification();

    void setQualification(IQualification qualification);

    IOrganization getOrganization();

    void setOrganization(IOrganization organization);

    Float getSalary();

    void setSalary(Float salary);

}
