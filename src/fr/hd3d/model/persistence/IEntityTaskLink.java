package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("EntityTaskLink")
public interface IEntityTaskLink extends IBase
{
    Long getBoundEntity();

    String getBoundEntityName();

    ITask getTask();

    String getExtra();

    void setBoundEntity(Long boundEntity);

    void setBoundEntityName(String boundEntityName);

    void setTask(ITask task);

    void setExtra(String extra);

    String getWoPath();

    String getWoName();

    void setWoPath(String woPath);

    void setWoName(String woName);
}
