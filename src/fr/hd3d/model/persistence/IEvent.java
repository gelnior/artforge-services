/**
 * 
 */
package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author thomas-eskenazi
 * 
 */
@Hd3dEntityName("Event")
public interface IEvent extends IDurable, IBase
{
    IPlanning getPlanning();

    String getTitle();

    String getDescription();

    void setPlanning(IPlanning planning);

    void setTitle(String title);

    void setDescription(String description);
}
