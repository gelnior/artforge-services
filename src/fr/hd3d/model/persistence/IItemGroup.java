package fr.hd3d.model.persistence;

import java.util.List;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("ItemGroup")
public interface IItemGroup extends IBase, Hook, Name
{

    List<IItem> getItems();

    void addItem(IItem item);

    IItem itemByName(String name);

    ISheet getSheet();

    void setItems(List<IItem> items);

    void setSheet(ISheet sheet);

    Integer getIndex();

    void setIndex(Integer index);
}
