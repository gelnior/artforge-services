package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;
import fr.hd3d.model.lightweight.ILRole;


/**
 * 
 * @author florent-della-valle
 * 
 */
@Hd3dEntityName("Role")
public interface IRole extends IBase, Name
{

    Set<String> getPermissions();

    Set<String> getBans();

    Set<IResourceGroup> getResourceGroups();

    void setPermissions(Set<String> permissions);

    void setBans(Set<String> bans);

    void setResourceGroups(Set<IResourceGroup> resourceGroups);

    void add(String permission);

    void addBan(String ban);

    void add(IResourceGroup resourceGroup);

    /**
     * Checks whether the role can be updated from the specified light object. In other words, whether the newly added
     * or deleted permissions and bans are legal, if any.
     * 
     * @param lRole
     * @return
     */
    boolean canBeUpdatedFrom(ILRole lRole);

}
