package fr.hd3d.model.persistence;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import fr.hd3d.common.client.enums.EAssetStatus;
import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("AssetRevision")
public interface IAssetRevision extends IBase, Project, Name
{
    // Set<IAssetRevisionFileRevision> getAssetRevisionFileRevisions() throws Hd3dException;

    Set<IAssetRevisionLink> getAssetRevisionLinks_ThisAsInput() throws Hd3dException;

    Set<IAssetRevisionLink> getAssetRevisionLinks_ThisAsOutput() throws Hd3dException;

    Set<IAssetRevisionLink> getAssetRevisionLinks() throws Hd3dException;

    IProject getProject();

    Set<IAssetRevisionGroup> getAssetRevisionGroups();

    void addAssetRevisionGroup(IAssetRevisionGroup group);

    void removeAssetRevisionGroup(IAssetRevisionGroup group);

    Date getCreationDate();

    String getKey();

    String getVariation();

    Integer getRevision();

    IPerson getCreator();

    ITaskType getTaskType();

    EAssetStatus getStatus();

    Date getLastOperationDate();

    IPerson getLastUser();

    String getComment();

    String getValidity();

    String getWipPath();

    String getPublishPath();

    String getOldPath();

    String getMountPointWipPath();

    String getMountPointPublishPath();

    String getMountPointOldPath();

    List<IFileRevision> getFileRevisions();

    Collection<IFileRevision> getLastFileRevisions();

    void setFileRevisions(List<IFileRevision> fileRevisions);

    void setProject(IProject project);

    void setAssetRevisionGroups(Set<IAssetRevisionGroup> assetRevisionGroups);

    void setCreationDate(Date creationDate);

    void setKey(String key);

    void setVariation(String variation);

    void setRevision(Integer revision);

    void setCreator(IPerson creator);

    void setTaskType(ITaskType type);

    void setStatus(EAssetStatus status);

    void setLastOperationDate(Date since);

    void setLastUser(IPerson lastUser);

    void setComment(String comment);

    void setValidity(String validity);

    void setWipPath(String wipPath);

    void setPublishPath(String publishPath);

    void setOldPath(String oldPath);

    void setMountPointWipPath(String mountPointWipPath);

    void setMountPointPublishPath(String mountPointPublishPath);

    void setMountPointOldPath(String mountPointOldPath);

    boolean belongsTo(IAssetRevisionGroup group);

    String getMountPointByState(EFileState state);

}
