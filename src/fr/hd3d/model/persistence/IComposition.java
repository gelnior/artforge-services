package fr.hd3d.model.persistence;

import java.util.List;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * a category has one and only one parent.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("Composition")
public interface IComposition extends IBase, Name
{

    String getDescription();

    IConstituent getConstituent();

    IShot getShot();

    ISequence getSequence();

    List<IFact> getFacts();

    Integer getNbOccurence();

    void setDescription(String description);

    void setConstituent(IConstituent constituent);

    void setShot(IShot shot);

    void setSequence(ISequence sequence);

    void setFacts(List<IFact> facts);

    void setNbOccurence(Integer nbOccurence);

    String getFactsIdString();

    String getProjectIdString();

}
