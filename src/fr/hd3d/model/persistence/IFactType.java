package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("FactType")
public interface IFactType extends IBase, Name
{

    String getDescription();

    void setDescription(String description);

}
