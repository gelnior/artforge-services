package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for CPU informations needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Room")
public interface IRoom extends ISimple
{
    Set<IComputer> getComputers();

    void setComputers(Set<IComputer> computers);
}
