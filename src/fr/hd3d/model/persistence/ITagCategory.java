package fr.hd3d.model.persistence;

import java.util.List;
import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("TagCategory")
public interface ITagCategory extends IBase, Name
{
    ITagCategory getParent();

    void setParent(ITagCategory parent);

    Set<ITagCategory> getParents();

    List<ITagCategory> getChildren();

    List<ITagCategory> allChildren(boolean includeSelf);

    void setChildren(List<ITagCategory> children);

    List<ITag> getBoundTags();

    void setBoundTags(List<ITag> boundTags);

}
