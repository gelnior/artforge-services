package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * A34 time sheet implementation https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("Person")
public interface IPerson extends IResource
{
    String getLogin();

    void setLogin(String login);

    String getFirstName();

    String getLastName();

    /**
     * Helper function if firstName = john and lastName = doe getName() will return John Doe
     */
    String fullName();

    Set<ISkillLevel> getSkillLevels();

    Set<IPersonDay> getDays();

    void setFirstName(String firstName);

    void setLastName(String lastName);

    String getEmail();

    void setEmail(String email);

    String getPhone();

    void setPhone(String phone);

    String getPhotoPath();

    void setPhotoPath(String photoPath);

    void setSkillLevels(Set<ISkillLevel> skillLevels);

    void setDays(Set<IPersonDay> days);

    Set<IContract> getContracts();

    void setContracts(Set<IContract> contracts);

    Set<IResourceGroup> getLedResourceGroups();

    void setLedResourceGroups(Set<IResourceGroup> resourceGroups);

    String toString();
}
