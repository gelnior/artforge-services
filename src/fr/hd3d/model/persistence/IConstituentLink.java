package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * a category has one and only one parent.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("ConstituentLink")
public interface IConstituentLink extends IBase
{
    IConstituent getMaster();

    IConstituent getSlave();

    String getType();

    void setMaster(IConstituent master);

    void setSlave(IConstituent slave);

    void setType(String type);

}
