package fr.hd3d.model.persistence;

import java.util.Collection;
import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.criterion.ProjectionList;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.services.resources.ResourceContext;


/**
 * Provides objects retrieval and CRUD methods.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 * 
 * @param <E>
 *            Persisted class
 */
public interface IPersist<E extends IBase>
{
    List<E> getAllCollection() throws Hd3dException;

    List<E> getAllCollection_NoPermCheck() throws Hd3dException;

    /**
     * returns a list of objects filtered by <i>pager</i> constraint.
     * 
     * @param pager
     *            query constraint
     * @return list of objects filtered by <i>pager</i> constraint.
     * @throws Hd3dPersistenceException
     */
    ResultCollection<E> getCollection(final ResourceContext<?, ?> context) throws Hd3dException;

    ResultCollection<E> getCollection(final ResourceContext<?, ?> context, ProjectionList projectionList)
            throws Hd3dException;

    ResultCollection<E> getCollection(final ResourceContext<?, ?> context, boolean idOnly) throws Hd3dException;

    public ResultCollection<E> getCollectionWithTotal(final ResourceContext<?, ?> context) throws Hd3dException;

    public ResultCollection<E> getCollectionWithTotal(final ResourceContext<?, ?> context, ProjectionList projectionList)
            throws Hd3dException;

    public ResultCollection<E> getCollectionWithTotal(final ResourceContext<?, ?> context, boolean idOnly)
            throws Hd3dException;

    ResultCollection<E> getCollection_NoPermCheck(final ResourceContext<?, ?> context) throws Hd3dException;

    ResultCollection<E> getCollection_NoPermCheck(final ResourceContext<?, ?> context, ProjectionList projectionList)
            throws Hd3dException;

    ResultCollection<E> getCollection_NoPermCheck(final ResourceContext<?, ?> context, boolean idOnly)
            throws Hd3dException;

    /**
     * Count query with constraint.
     * 
     * @param pager
     *            Query constraint
     * @return Count Result
     * @throws Hd3dPersistenceException
     */
    int getCollectionCount(final ResourceContext<?, ?> context) throws Hd3dException;

    /**
     * Returns null if object is not found
     */
    E getById(final ResourceContext<?, ?> context, Long id) throws Hd3dException;

    E getById(final ResourceContext<?, ?> context, Long id, LockMode lockMode) throws Hd3dException;

    E getById(Long id) throws Hd3dException;

    E getById(Long id, LockMode lockMode) throws Hd3dException;

    E getById_NoPermCheck(final ResourceContext<?, ?> context, Long id) throws Hd3dException;

    E getById_NoPermCheck(final ResourceContext<?, ?> context, Long id, LockMode lockMode) throws Hd3dException;

    E getById_NoPermCheck(Long id) throws Hd3dException;

    E getById_NoPermCheck(Long id, LockMode lockMode) throws Hd3dException;

    /**
     * Throws an exception is the object is not found
     */
    E loadById(final ResourceContext<?, ?> context, Long id) throws Hd3dException;

    E loadById(Long id) throws Hd3dException;

    List<E> getByIds(final ResourceContext<?, ?> context, Collection<Long> ids) throws Hd3dException;

    List<E> getByIds(final ResourceContext<?, ?> context, final Collection<Long> ids, final LockMode lockMode)
            throws Hd3dException;

    List<E> getByIds(Collection<Long> ids) throws Hd3dException;

    public List<E> getByIds(final Collection<Long> ids, final LockMode lockMode) throws Hd3dException;

    List<E> getByIds_NoPermCheck(final ResourceContext<?, ?> context, Collection<Long> ids) throws Hd3dException;

    List<E> getByIds_NoPermCheck(final ResourceContext<?, ?> context, final Collection<Long> ids,
            final LockMode lockMode) throws Hd3dException;

    List<E> getByIds_NoPermCheck(Collection<Long> ids) throws Hd3dException;

    List<E> getByIds_NoPermCheck(final Collection<Long> ids, final LockMode lockMode) throws Hd3dException;

    List<E> getByValue(final ResourceContext<?, ?> context, String property, Object value) throws Hd3dException;

    List<E> getByValue(String property, Object value) throws Hd3dException;

    E getObjectByValue(final ResourceContext<?, ?> context, String property, Object value) throws Hd3dException;

    E getObjectByValue(String property, Object value) throws Hd3dException;

    E getObjectByValue(String property, Object value, LockMode lockMode) throws Hd3dException;

    E getObjectByValue(final ResourceContext<?, ?> context, String property, Object value, LockMode lockMode)
            throws Hd3dException;

    E getObjectByValue_NoPermcheck(final ResourceContext<?, ?> context, String property, Object value)
            throws Hd3dException;

    E getObjectByValue_NoPermCheck(String property, Object value) throws Hd3dException;

    E getObjectByValue_NoPermCheck(final ResourceContext<?, ?> context, String property, Object value, LockMode lockMode)
            throws Hd3dException;

    E getObjectByValue_NoPermCheck(String property, Object value, LockMode lockMode) throws Hd3dException;

    List<E> getByValue_NoPermCheck(final ResourceContext<?, ?> context, String property, Object value)
            throws Hd3dException;

    List<E> getByValue_NoPermCheck(String property, Object value) throws Hd3dException;

    List<E> getByValue(final ResourceContext<?, ?> context, String property, Object value, LockMode lockMode)
            throws Hd3dException;

    List<E> getByValue(String property, Object value, LockMode lockMode) throws Hd3dException;

    List<E> getByValue_NoPermCheck(final ResourceContext<?, ?> context, String property, Object value, LockMode lockMode)
            throws Hd3dException;

    List<E> getByValue_NoPermCheck(String property, Object value, LockMode lockMode) throws Hd3dException;

    List<E> getByValues(final ResourceContext<?, ?> context, String[] properties, Object[] values, String operator)
            throws Hd3dException;

    List<E> getByValues(String[] properties, Object[] values, String operator) throws Hd3dException;

    List<E> getByValues_NoPermCheck(final ResourceContext<?, ?> context, String[] properties, Object[] values,
            String operator) throws Hd3dException;

    List<E> getByValues_NoPermCheck(String[] properties, Object[] values, String operator) throws Hd3dException;

    List<E> getByValues(final ResourceContext<?, ?> context, String[] properties, Object[] values, String operator,
            LockMode lockMode) throws Hd3dException;

    List<E> getByValues(String[] properties, Object[] values, String operator, LockMode lockMode) throws Hd3dException;

    List<E> getByValues_NoPermCheck(final ResourceContext<?, ?> context, String[] properties, Object[] values,
            String operator, LockMode lockMode) throws Hd3dException;

    List<E> getByValues_NoPermCheck(String[] properties, Object[] values, String operator, LockMode lockMode)
            throws Hd3dException;

    List<E> getByValuesIn(final ResourceContext<?, ?> context, final String[] properties, final Object[] values,
            final String operator) throws Hd3dException;

    List<E> getByValuesIn(final String[] properties, final Object[] values, final String operator) throws Hd3dException;

    List<E> getByValuesIn_NoPermCheck(final ResourceContext<?, ?> context, final String[] properties,
            final Object[] values, final String operator) throws Hd3dException;

    List<E> getByValuesIn_NoPermCheck(final String[] properties, final Object[] values, final String operator)
            throws Hd3dException;

    List<E> getByValuesIn_NoPermCheck(final String[] properties, final Object[] values, final String operator,
            LockMode lockMode) throws Hd3dException;

    List<E> getByValuesIn(final ResourceContext<?, ?> context, final String[] properties, final Object[] values,
            final String operator, LockMode lockMode) throws Hd3dException;

    List<E> getByValuesIn(final String[] properties, final Object[] values, final String operator, LockMode lockMode)
            throws Hd3dException;

    List<E> getByValuesIn_NoPermCheck(final ResourceContext<?, ?> context, final String[] properties,
            final Object[] values, final String operator, LockMode lockMode) throws Hd3dException;

    List<E> getByValueIn(final ResourceContext<?, ?> context, String property, List<?> value) throws Hd3dException;

    List<E> getByValueIn(String property, List<?> value) throws Hd3dException;

    List<E> getByValueIn_NoPermCheck(final ResourceContext<?, ?> context, String property, List<?> value)
            throws Hd3dException;

    List<E> getByValueIn_NoPermCheck(String property, List<?> value) throws Hd3dException;

    void persist(final E object) throws Hd3dException;

    void persistCollection(final Iterable<E> objects) throws Hd3dException;

    // void update(final E object) throws Hd3dException;

    void merge(final E object) throws Hd3dException;

    void mergeCollection(final Iterable<E> objects) throws Hd3dException;

    void delete(final E object) throws Hd3dException;

    void disable(final E object) throws Hd3dException;

    void disableCollection(final Iterable<E> objects) throws Hd3dException;

    Class<?> getPersistedClass();

    E newInstance();
}
