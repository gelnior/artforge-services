package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("FileRevisionLink")
public interface IFileRevisionLink extends IBase
{

    IFileRevision getInputFileRevision();

    IFileRevision getOutputFileRevision();

    String getType();

    void setInputFileRevision(IFileRevision inputFileRevision);

    void setOutputFileRevision(IFileRevision outputFileRevision);

    void setType(String type);

}
