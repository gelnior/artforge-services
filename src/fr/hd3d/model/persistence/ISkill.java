package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for skill informations needed by the human resources application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Skill")
public interface ISkill extends ISimple
{
    ISoftware getSoftware();

    void setSoftware(ISoftware software);

    Set<IQualification> getQualifications();

    void setQualifications(Set<IQualification> qualifications);
}
