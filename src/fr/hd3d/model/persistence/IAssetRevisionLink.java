package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("AssetRevisionLink")
public interface IAssetRevisionLink extends IBase
{

    IAssetRevision getInputAsset();

    IAssetRevision getOutputAsset();

    String getType();

    void setInputAsset(IAssetRevision inputAsset);

    void setOutputAsset(IAssetRevision outputAsset);

    void setType(String type);

}
