package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("FileRevisionAnnotation")
public interface IFileRevisionAnnotation extends IBase, Name
{
    Integer getMarkIn();

    Integer getMarkOut();

    String getComment();

    IFileRevision getGraphicData();

    IFileRevision getAnnotatedFile();

    IPerson getAnnotator();

    void setMarkIn(Integer markIn);

    void setMarkOut(Integer markOut);

    void setComment(String comment);

    void setGraphicData(IFileRevision graphicData);

    void setAnnotatedFile(IFileRevision annotatedFile);

    void setAnnotator(IPerson annotator);
}
