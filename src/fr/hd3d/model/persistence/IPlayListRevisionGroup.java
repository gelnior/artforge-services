package fr.hd3d.model.persistence;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * 
 * @author HD3D
 */
@Hd3dEntityName("PlayListRevisionGroup")
public interface IPlayListRevisionGroup extends IBase, Name
{
    String getName();

    void setName(String name);

    IProject getProject();

    void setProject(IProject project);

    IPlayListRevisionGroup getParent();

    void setParent(IPlayListRevisionGroup parent);

    Collection<IPlayListRevisionGroup> getChildren();

    Collection<IPlayListRevisionGroup> allChildren(boolean includeSelf);

    void setChildren(Collection<IPlayListRevisionGroup> children);

    Set<IPlayListRevisionGroup> parents();

    String getParentIds();

    List<IPlayListRevision> getPlayListRevisions();

    void setPlayListRevisions(List<IPlayListRevision> playListRevisions);
}
