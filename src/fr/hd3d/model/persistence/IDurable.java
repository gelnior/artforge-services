/**
 * 
 */
package fr.hd3d.model.persistence;

import java.util.Date;


/**
 * @author thomas-eskenazi
 * 
 */
public interface IDurable extends IBase
{
    Date getStartDate();

    Date getEndDate();

    void setStartDate(Date startDate);

    void setEndDate(Date endDate);

}
