package fr.hd3d.model.persistence;

import java.util.Collection;
import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author michael.guiral
 * 
 */
@Hd3dEntityName("TaskGroup")
public interface ITaskGroup extends ITaskBase, Name
{

    void setColor(String color);

    String getColor();

    void setPlanning(IPlanning planning);

    IPlanning getPlanning();

    void setTaskType(ITaskType taskType);

    ITaskType getTaskType();

    String getFilter();

    void setFilter(String filter);

    Set<IExtraLine> getExtraLines();

    Collection<ITaskGroup> getChildren();

    void setChildren(Collection<ITaskGroup> children);

    Collection<ITaskGroup> allChildren(boolean includeSelf);

    Integer getIndex();

    void setIndex(Integer index);
}
