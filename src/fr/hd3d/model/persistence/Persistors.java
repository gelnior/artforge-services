package fr.hd3d.model.persistence;

import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.collections15.MapUtils;
import org.apache.commons.lang.ClassUtils;
import org.hibernate.annotations.Any;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dCollectionQuery;
import fr.hd3d.model.annotation.Hd3dEntityName;
import fr.hd3d.model.annotation.Hd3dExposed;
import fr.hd3d.model.persistence.impl.hibernate.AbsenceH;
import fr.hd3d.model.persistence.impl.hibernate.ActivityH;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteH;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteTypeH;
import fr.hd3d.model.persistence.impl.hibernate.AssetAbstractH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionLinkH;
import fr.hd3d.model.persistence.impl.hibernate.CategoryH;
import fr.hd3d.model.persistence.impl.hibernate.ClassDynMetaDataTypeH;
import fr.hd3d.model.persistence.impl.hibernate.CompositionH;
import fr.hd3d.model.persistence.impl.hibernate.ComputerH;
import fr.hd3d.model.persistence.impl.hibernate.ComputerModelH;
import fr.hd3d.model.persistence.impl.hibernate.ComputerTypeH;
import fr.hd3d.model.persistence.impl.hibernate.ConstituentH;
import fr.hd3d.model.persistence.impl.hibernate.ConstituentLinkH;
import fr.hd3d.model.persistence.impl.hibernate.ContractH;
import fr.hd3d.model.persistence.impl.hibernate.DeviceH;
import fr.hd3d.model.persistence.impl.hibernate.DeviceModelH;
import fr.hd3d.model.persistence.impl.hibernate.DeviceTypeH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataTypeH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataValueH;
import fr.hd3d.model.persistence.impl.hibernate.DynamicPathH;
import fr.hd3d.model.persistence.impl.hibernate.EntityTaskLinkH;
import fr.hd3d.model.persistence.impl.hibernate.EventH;
import fr.hd3d.model.persistence.impl.hibernate.ExtraLineH;
import fr.hd3d.model.persistence.impl.hibernate.FactH;
import fr.hd3d.model.persistence.impl.hibernate.FactTypeH;
import fr.hd3d.model.persistence.impl.hibernate.FileAttributeH;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionAnnotationH;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionLinkH;
import fr.hd3d.model.persistence.impl.hibernate.GraphicalAnnotationH;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.HintH;
import fr.hd3d.model.persistence.impl.hibernate.InventoryItemH;
import fr.hd3d.model.persistence.impl.hibernate.ItemGroupH;
import fr.hd3d.model.persistence.impl.hibernate.ItemH;
import fr.hd3d.model.persistence.impl.hibernate.LicenseH;
import fr.hd3d.model.persistence.impl.hibernate.ListValuesH;
import fr.hd3d.model.persistence.impl.hibernate.ManufacturerH;
import fr.hd3d.model.persistence.impl.hibernate.MileStoneH;
import fr.hd3d.model.persistence.impl.hibernate.MountPointH;
import fr.hd3d.model.persistence.impl.hibernate.OrganizationH;
import fr.hd3d.model.persistence.impl.hibernate.PersonDayH;
import fr.hd3d.model.persistence.impl.hibernate.PersonH;
import fr.hd3d.model.persistence.impl.hibernate.PlanningH;
import fr.hd3d.model.persistence.impl.hibernate.PlayListEditH;
import fr.hd3d.model.persistence.impl.hibernate.PlayListRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.PlayListRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.PoolH;
import fr.hd3d.model.persistence.impl.hibernate.ProcessorH;
import fr.hd3d.model.persistence.impl.hibernate.ProjectH;
import fr.hd3d.model.persistence.impl.hibernate.ProjectTypeH;
import fr.hd3d.model.persistence.impl.hibernate.ProxyH;
import fr.hd3d.model.persistence.impl.hibernate.ProxyTypeH;
import fr.hd3d.model.persistence.impl.hibernate.QualificationH;
import fr.hd3d.model.persistence.impl.hibernate.ResourceGroupH;
import fr.hd3d.model.persistence.impl.hibernate.ResourceH;
import fr.hd3d.model.persistence.impl.hibernate.RoleH;
import fr.hd3d.model.persistence.impl.hibernate.RoomH;
import fr.hd3d.model.persistence.impl.hibernate.ScreenH;
import fr.hd3d.model.persistence.impl.hibernate.ScreenModelH;
import fr.hd3d.model.persistence.impl.hibernate.ScriptH;
import fr.hd3d.model.persistence.impl.hibernate.SequenceH;
import fr.hd3d.model.persistence.impl.hibernate.SheetH;
import fr.hd3d.model.persistence.impl.hibernate.ShotH;
import fr.hd3d.model.persistence.impl.hibernate.SimpleActivityH;
import fr.hd3d.model.persistence.impl.hibernate.SkillH;
import fr.hd3d.model.persistence.impl.hibernate.SkillLevelH;
import fr.hd3d.model.persistence.impl.hibernate.SoftwareH;
import fr.hd3d.model.persistence.impl.hibernate.StepH;
import fr.hd3d.model.persistence.impl.hibernate.StudioMapH;
import fr.hd3d.model.persistence.impl.hibernate.TagCategoryH;
import fr.hd3d.model.persistence.impl.hibernate.TagH;
import fr.hd3d.model.persistence.impl.hibernate.TaskActivityH;
import fr.hd3d.model.persistence.impl.hibernate.TaskBaseH;
import fr.hd3d.model.persistence.impl.hibernate.TaskChangesH;
import fr.hd3d.model.persistence.impl.hibernate.TaskGroupH;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.model.persistence.impl.hibernate.TaskTypeH;
import fr.hd3d.model.persistence.impl.hibernate.WorkingTimeH;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.security.dao.Hd3dACLH;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.services.security.dao.IUserAccount;
import fr.hd3d.services.security.dao.UserAccountH;
import fr.hd3d.services.security.templates.IProjectSecurityTemplate;
import fr.hd3d.services.security.templates.IRoleTemplate;
import fr.hd3d.services.security.templates.ProjectSecurityTemplateH;
import fr.hd3d.services.security.templates.RoleTemplateH;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.Log;


/**
 * Hub concentrator of object persistors.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 * 
 */
public final class Persistors
{
    private Persistors()
    {}

    /* temporary maps */
    private static Map<String, IPersist<?>> _interfaceToPersistorMap = new HashMap<String, IPersist<?>>();
    private static Map<String, IPersist<?>> _classToPersistorMap = new HashMap<String, IPersist<?>>();
    private static Map<String, String> _entityNameToInterfaceNameMap = new HashMap<String, String>();
    private static Map<String, Class<?>> _entityNameToInterfaceMap = new HashMap<String, Class<?>>();
    private static Map<String, String> _entityNameToClassNameMap = new HashMap<String, String>();
    private static Map<String, Class<?>> _entityNameToClassMap = new HashMap<String, Class<?>>();
    private static Map<Object, String> _classUpHierarchyNamesMap = new MultiValueMap();
    private static Map<Object, Class<?>> _classUpHierarchyMap = new MultiValueMap();
    private static Map<String, String> _classDownHierarchyNamesMap = new MultiValueMap();
    private static Map<String, Class<?>> _classDownHierarchyMap = new MultiValueMap();
    private static Map<String, String> _entityExposedMethodsMap = new MultiValueMap();
    private static Map<String, Class<?>> _classAttributeTypesMap = new HashMap<String, Class<?>>();
    private static Map<String, Class<?>> _attributeCollectionParameterTypesMap = new MultiValueMap();
    private static Map<String, Annotation> _classAttributeAssociationsMap = new HashMap<String, Annotation>();
    private static Map<String, Class<?>> _classAttributeAssociationTargetsMap = new HashMap<String, Class<?>>();
    private static Map<String, Map<String, String>> _classAttributeHd3dCollectionQueryAnnotationMap = new HashMap<String, Map<String, String>>();

    // for testing purpose
    public static void main(String[] args) throws Hd3dException
    {
        init();
    }

    public static void shutdown()
    {
        _interfaceToPersistorMap.clear();
        _classToPersistorMap.clear();
        _entityNameToInterfaceNameMap.clear();
        _entityNameToInterfaceMap.clear();
        _entityNameToClassNameMap.clear();
        _entityNameToClassMap.clear();
        _classUpHierarchyNamesMap.clear();
        _classUpHierarchyMap.clear();
        _classDownHierarchyNamesMap.clear();
        _classDownHierarchyMap.clear();
        _entityExposedMethodsMap.clear();
        _classAttributeTypesMap.clear();
        _attributeCollectionParameterTypesMap.clear();
        _classAttributeAssociationsMap.clear();
        _classAttributeAssociationTargetsMap.clear();
        EntitiesMaps.clear();
    }

    /**
     * given a entity interface or class, returns the corresponding persistor ex: "fr.hd3d.model.persistence.IPerson"
     * and "fr.hd3d.model.persistence.impl.hibernate.PersonH" returns Persistors.person
     * 
     * @param entityNameOrInterfaceOrClass
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T extends IBase> IPersist<T> getPersistor(String entityNameOrInterfaceOrClass)
    {
        IPersist<T> ret;

        /* by entity */
        IPersist p = EntitiesMaps.withEntityName(entityNameOrInterfaceOrClass).getPersistor();
        if (p != null)
        {
            ret = p;
        }
        else
        {
            /* by interface */
            p = EntitiesMaps.withInterfaceName(entityNameOrInterfaceOrClass).getPersistor();
            if (p != null)
            {
                ret = p;
            }
            else
            {
                /* by class */
                ret = (IPersist<T>) EntitiesMaps.withClassName(entityNameOrInterfaceOrClass).getPersistor();
            }
        }

        return ret;
    }

    /**
     * given a class, retrieve its properties types. key=<className>.<property> value=property type
     * 
     * @param clazz
     * @return
     */
    public static Map<String, Map<String, ?>> propertiesTypes(Class<?> clazz)
    {
        Map<String, Class<?>> attributeTypesMap = new HashMap<String, Class<?>>();
        Map<String, Class<?>> attributeCollectionParameterTypesMap = new MultiValueMap();
        Map<String, Annotation> attributeAssociationsMap = new HashMap<String, Annotation>();
        Map<String, Class<?>> attributeAssociationTargetsMap = new HashMap<String, Class<?>>();
        Map<String, Map<String, String>> classAttributeHd3dCollectionQueryAnnotationMap = new HashMap<String, Map<String, String>>();
        /* Note: PropertyUtils.getPropertyDescriptors retrieves the properties of the super classes as well */
        PropertyDescriptor[] props = PropertyUtils.getPropertyDescriptors(clazz);
        for (PropertyDescriptor p : props)
        {
            /* store property type */
            final String key = clazz.getCanonicalName() + '.' + p.getName();

            attributeTypesMap.put(key, p.getPropertyType());

            /* if property is a parameterized Collection, store the parameter types */
            if (Collection.class.isAssignableFrom(p.getPropertyType()))
            {
                Class<?> currentClass = clazz;
                Field field;

                while (currentClass != null)
                {
                    try
                    {
                        field = currentClass.getDeclaredField(p.getName());
                        Type genericFieldType = field.getGenericType();

                        if (genericFieldType instanceof ParameterizedType)
                        {
                            ParameterizedType aType = (ParameterizedType) genericFieldType;
                            Type[] fieldArgTypes = aType.getActualTypeArguments();
                            for (Type fieldArgType : fieldArgTypes)
                            {
                                Class<?> fieldArgClass = (Class<?>) fieldArgType;
                                attributeCollectionParameterTypesMap.put(key, fieldArgClass);
                            }
                        }
                        currentClass = currentClass.getSuperclass();
                    }
                    catch (SecurityException e)
                    {
                        /* nothing to do: the attribute may belong to a super class */
                        // Log.LOGGER.info("Error when retrieving Entities attributes types", e);
                        currentClass = currentClass.getSuperclass();
                    }
                    catch (NoSuchFieldException e)
                    {
                        /* nothing to do: the attribute may belong to a super class */
                        // Log.LOGGER.info("Error when retrieving Entities attributes types", e);
                        currentClass = currentClass.getSuperclass();
                    }
                }

            }

            /* store whether the property is an association */
            final Method getter = p.getReadMethod();
            if (getter == null)
                continue;
            Annotation annotation = null;
            Class<?> associatedClass = null;

            if (getter.getAnnotation(OneToOne.class) != null)
            {
                annotation = getter.getAnnotation(OneToOne.class);
                associatedClass = ((OneToOne) annotation).targetEntity();
            }
            else if (getter.getAnnotation(OneToMany.class) != null)
            {
                annotation = getter.getAnnotation(OneToMany.class);
                associatedClass = ((OneToMany) annotation).targetEntity();
            }
            else if (getter.getAnnotation(ManyToOne.class) != null)
            {
                annotation = getter.getAnnotation(ManyToOne.class);
                associatedClass = ((ManyToOne) annotation).targetEntity();
            }
            else if (getter.getAnnotation(ManyToMany.class) != null)
            {
                annotation = getter.getAnnotation(ManyToMany.class);
                associatedClass = ((ManyToMany) annotation).targetEntity();
            }
            else if (getter.getAnnotation(Any.class) != null)
            {
                annotation = getter.getAnnotation(Any.class);
                // Note: TODO associatedClass
            }

            if (annotation != null)
                attributeAssociationsMap.put(key, annotation);
            if (associatedClass != null)
                attributeAssociationTargetsMap.put(key, associatedClass);
            // System.out.println(p.getName() + "," + p.getReadMethod() + "," + p.getReadMethod().getAnnotations());

            /* if the field is "@Hd3dCollectionQuery" annotated, store <field name, the annotation> */
            try
            {
                Field field = clazz.getDeclaredField(p.getName());
                Hd3dCollectionQuery hd3dCollectionQueryAnno = field.getAnnotation(Hd3dCollectionQuery.class);
                if (hd3dCollectionQueryAnno != null)
                {
                    getInnerMap(classAttributeHd3dCollectionQueryAnnotationMap, clazz.getCanonicalName()).put(
                            field.getName(), hd3dCollectionQueryAnno.value().getCanonicalName());
                }
            }
            catch (SecurityException e)
            {

            }
            catch (NoSuchFieldException e)
            {

            }
        }

        // This map's purpose is just to return several maps
        Map<String, Map<String, ?>> ret = new HashMap<String, Map<String, ?>>();
        ret.put("attributeTypesMap", attributeTypesMap);
        ret.put("attributeCollectionParameterTypesMap", attributeCollectionParameterTypesMap);
        ret.put("attributeAssociationsMap", attributeAssociationsMap);
        ret.put("attributeAssociationTargetsMap", attributeAssociationTargetsMap);
        ret.put("classAttributeHd3dCollectionQueryAnnotationMap", classAttributeHd3dCollectionQueryAnnotationMap);

        return ret;
    }

    private static Map<String, String> getInnerMap(Map<String, Map<String, String>> containerMap, String key)
    {
        if (containerMap.get(key) == null)
        {
            Map<String, String> innerMap = new HashMap<String, String>();
            containerMap.put(key, innerMap);
            return innerMap;
        }
        else
        {
            return containerMap.get(key);
        }
    }

    @SuppressWarnings("unchecked")
    public static void init() throws Hd3dException
    {
        // Note: even nothing here, this method has to be called to trigger the static fields instantiation
        // this method is called after all static classes instantiation

        // parse the static IPersist<T> classes here and link them to the interface name T
        // key = T
        // value = the bound IPersist<T>
        Field[] fields = Persistors.class.getFields();
        for (Field f : fields)
        {
            Type genericFieldType = f.getGenericType();

            if (!(genericFieldType instanceof ParameterizedType))
                continue;

            ParameterizedType aType = (ParameterizedType) genericFieldType;
            Type[] fieldArgTypes = aType.getActualTypeArguments();
            for (Type fieldArgType : fieldArgTypes)
            {
                if (fieldArgType instanceof ParameterizedType)
                    continue;// case of Collection<String> for instance

                Class<?> fieldArgClass = (Class<?>) fieldArgType;
                List<?> interfaces = ClassUtils.getAllInterfaces(f.getType());

                if (f.getType() != IPersist.class && !interfaces.contains(IPersist.class))
                    continue;

                final String interfaceName = fieldArgClass.getCanonicalName();
                Log.LOGGER.info("interface = {}", interfaceName);
                try
                {
                    IPersist<?> persistor = (IPersist<?>) f.get(null);// null means static field

                    Map<String, Map<String, ?>> propertiesTypes = propertiesTypes(persistor.getPersistedClass());
                    _classAttributeTypesMap.putAll((Map<String, Class<?>>) propertiesTypes.get("attributeTypesMap"));
                    _attributeCollectionParameterTypesMap.putAll((Map<String, Class<?>>) propertiesTypes
                            .get("attributeCollectionParameterTypesMap"));
                    _classAttributeAssociationsMap.putAll((Map<String, Annotation>) propertiesTypes
                            .get("attributeAssociationsMap"));
                    _classAttributeAssociationTargetsMap.putAll((Map<String, Class<?>>) propertiesTypes
                            .get("attributeAssociationTargetsMap"));
                    _classAttributeHd3dCollectionQueryAnnotationMap
                            .putAll((Map<? extends String, ? extends Map<String, String>>) propertiesTypes
                                    .get("classAttributeHd3dCollectionQueryAnnotationMap"));

                    final String persistedClassName = persistor.getPersistedClass().getCanonicalName();

                    // Key=interface, Value=Persistor
                    _interfaceToPersistorMap.put(interfaceName, persistor);
                    //
                    // if it is an interface extending IBaseObject
                    if (fieldArgClass.isInterface() && IBase.class.isAssignableFrom(fieldArgClass))
                    {
                        Hd3dEntityName ann = (Hd3dEntityName) fieldArgClass.getAnnotation(Hd3dEntityName.class);
                        if (ann != null)
                        {
                            // space in entity names is not allowed
                            String annotatonValue = ann.value();
                            if (annotatonValue.contains(" "))
                                throw new Hd3dException("EntityName with space is not allowed in Interface annotation");

                            if (_entityNameToInterfaceNameMap.containsKey(annotatonValue))
                                throw new Hd3dException("Duplicate EntityName in Interface annotation:"
                                        + annotatonValue);

                            _entityNameToInterfaceNameMap.put(ann.value(), interfaceName);
                            _entityNameToClassNameMap.put(ann.value(), persistedClassName);

                            // store the classes directly as well
                            try
                            {
                                _entityNameToInterfaceMap.put(ann.value(), ClassUtils.getClass(interfaceName, false));
                            }
                            catch (ClassNotFoundException e)
                            {
                                Log.LOGGER.error("Cannot find class for interfacename={}", interfaceName);
                            }
                            try
                            {
                                _entityNameToClassMap.put(ann.value(), ClassUtils.getClass(persistedClassName, false));
                            }
                            catch (ClassNotFoundException e)
                            {
                                Log.LOGGER.error("Cannot find class for classname={}", persistedClassName);
                            }

                            // retrieve @Hd3dExposed methods
                            for (Method m : fieldArgClass.getMethods())
                            {
                                Hd3dExposed exposedAnn = (Hd3dExposed) m.getAnnotation(Hd3dExposed.class);
                                if (exposedAnn != null)
                                    _entityExposedMethodsMap.put(ann.value(), m.getName());
                            }
                        }
                    }
                    // Key=class, Value=ObjectProvider
                    _classToPersistorMap.put(persistedClassName, persistor);
                    //

                    // cache classes hierarchy, including abstract classes
                    List<?> superClasses = ClassUtils.getAllInterfaces(persistor.getPersistedClass());
                    for (Object c : superClasses)
                    {
                        if (c == Object.class)// Object is not interesting, pass
                            continue;

                        final Class<?> superClass = ((Class<?>) c);
                        String superClassName = superClass.getCanonicalName();
                        // Key=class name, Value=super classes names
                        if ("fr.hd3d.model.persistence.impl.hibernate.BaseH".equals(superClassName))
                            superClassName = Const.ALL_CLASSES;// base abstract class

                        _classUpHierarchyNamesMap.put(persistedClassName, superClassName);
                        _classUpHierarchyMap.put(persistedClassName, superClass);
                        // Key=class name, Value=sub classes names
                        _classDownHierarchyNamesMap.put(superClassName, persistedClassName);
                        _classDownHierarchyMap.put(superClassName, superClass);
                    }
                }
                catch (IllegalArgumentException e)
                {// nothing to do
                }
                catch (IllegalAccessException e)
                {// nothing to do
                }
                // }
            }
        }

        // populate maps with tempory maps, making them unmodifiable.
        EntitiesMaps.setInterfaceToPersistorMap(Collections.unmodifiableMap(_interfaceToPersistorMap));
        EntitiesMaps.setClassToPersistorMap(Collections.unmodifiableMap(_classToPersistorMap));

        EntitiesMaps.setEntityNameToInterfaceNameMap(Collections.unmodifiableMap(_entityNameToInterfaceNameMap));

        EntitiesMaps.setInterfaceNameToEntityNameMap(Collections.unmodifiableMap(MapUtils
                .invertMap(_entityNameToInterfaceNameMap)));

        EntitiesMaps.setEntityNameToInterfaceMap(Collections.unmodifiableMap(_entityNameToInterfaceMap));

        EntitiesMaps.setEntityNameToClassNameMap(Collections.unmodifiableMap(_entityNameToClassNameMap));

        EntitiesMaps.setEntityNameToClassMap(Collections.unmodifiableMap(_entityNameToClassMap));

        EntitiesMaps.setClassAttributeTypesMap(Collections.unmodifiableMap(_classAttributeTypesMap));

        EntitiesMaps.setClassAttributeCollectionParameterTypesMap(MultiValueMap.decorate(Collections
                .unmodifiableMap((_attributeCollectionParameterTypesMap))));

        EntitiesMaps.setClassAttributeAssociationsMap(Collections.unmodifiableMap(_classAttributeAssociationsMap));

        EntitiesMaps.setClassAttributeAssociationTargetsMap(Collections
                .unmodifiableMap(_classAttributeAssociationTargetsMap));

        EntitiesMaps.setClassNameToEntityNameMap(Collections.unmodifiableMap(MapUtils
                .invertMap(_entityNameToClassNameMap)));

        EntitiesMaps.setClassUpHierarchyNamesMap(MultiValueMap.decorate(Collections
                .unmodifiableMap(_classUpHierarchyNamesMap)));

        EntitiesMaps.setClassUpHierarchyMap(MultiValueMap.decorate(Collections.unmodifiableMap(_classUpHierarchyMap)));

        EntitiesMaps.setClassDownHierarchyNamesMap(MultiValueMap.decorate(Collections
                .unmodifiableMap(_classDownHierarchyNamesMap)));

        EntitiesMaps.setClassDownHierarchyMap(MultiValueMap.decorate(Collections
                .unmodifiableMap(_classDownHierarchyMap)));

        EntitiesMaps.setEntityExposedMethodsMap(MultiValueMap.decorate(Collections
                .unmodifiableMap(_entityExposedMethodsMap)));

        EntitiesMaps.setClassAttributeHd3dCollectionQueryAnnotationMap(MultiValueMap.decorate(Collections
                .unmodifiableMap(_classAttributeHd3dCollectionQueryAnnotationMap)));
    }

    /**
     * A34 Persistors
     */
    public static final IPersist<IPerson> person = new HibernatePersist<IPerson, PersonH>(PersonH.class);
    public static final IPersist<IProject> project = new HibernatePersist<IProject, ProjectH>(ProjectH.class);
    public static final IPersist<ISimpleActivity> simpleActivity = new HibernatePersist<ISimpleActivity, SimpleActivityH>(
            SimpleActivityH.class);
    public static final IPersist<ITaskActivity> taskActivity = new HibernatePersist<ITaskActivity, TaskActivityH>(
            TaskActivityH.class);
    public static final IPersist<IActivity> activity = new HibernatePersist<IActivity, ActivityH>(ActivityH.class);
    public static final IPersist<ITask> task = new HibernatePersist<ITask, TaskH>(TaskH.class);
    public static final IPersist<ITaskBase> taskbase = new HibernatePersist<ITaskBase, TaskBaseH>(TaskBaseH.class);
    public static final IPersist<IPersonDay> personDay = new HibernatePersist<IPersonDay, PersonDayH>(PersonDayH.class);
    public static final IPersist<IWorkingTime> workingTime = new HibernatePersist<IWorkingTime, WorkingTimeH>(
            WorkingTimeH.class);
    public static final IPersist<ITaskGroup> taskGroup = new HibernatePersist<ITaskGroup, TaskGroupH>(TaskGroupH.class);
    public static final IPersist<IPlanning> planning = new HibernatePersist<IPlanning, PlanningH>(PlanningH.class);
    public static final IPersist<ITaskChanges> taskChanges = new HibernatePersist<ITaskChanges, TaskChangesH>(
            TaskChangesH.class);
    public static final IPersist<IEvent> event = new HibernatePersist<IEvent, EventH>(EventH.class);
    public static final IPersist<IMileStone> mileStone = new HibernatePersist<IMileStone, MileStoneH>(MileStoneH.class);
    public static final IPersist<ITaskType> tasktype = new HibernatePersist<ITaskType, TaskTypeH>(TaskTypeH.class);
    public static final IPersist<IProjectType> projecttype = new HibernatePersist<IProjectType, ProjectTypeH>(
            ProjectTypeH.class);
    public static final IPersist<IAbsence> absence = new HibernatePersist<IAbsence, AbsenceH>(AbsenceH.class);
    public static final IPersist<IExtraLine> extraline = new HibernatePersist<IExtraLine, ExtraLineH>(ExtraLineH.class);

    /**
     * A12 Persistors
     */
    public static final IPersist<IConstituent> constituent = new HibernatePersist<IConstituent, ConstituentH>(
            ConstituentH.class);
    public static final IPersist<IConstituentLink> constituentlink = new HibernatePersist<IConstituentLink, ConstituentLinkH>(
            ConstituentLinkH.class);
    public static final IPersist<ICategory> category = new HibernatePersist<ICategory, CategoryH>(CategoryH.class);
    public static final IPersist<ISequence> sequence = new HibernatePersist<ISequence, SequenceH>(SequenceH.class);
    public static final IPersist<IShot> shot = new HibernatePersist<IShot, ShotH>(ShotH.class);
    public static final IPersist<IHint> hint = new HibernatePersist<IHint, HintH>(HintH.class);
    public static final IPersist<IComposition> composition = new HibernatePersist<IComposition, CompositionH>(
            CompositionH.class);
    public static final IPersist<IFact> fact = new HibernatePersist<IFact, FactH>(FactH.class);
    public static final IPersist<IFactType> facttype = new HibernatePersist<IFactType, FactTypeH>(FactTypeH.class);
    public static final IPersist<ISheet> sheet = new HibernatePersist<ISheet, SheetH>(SheetH.class);
    public static final IPersist<IItemGroup> itemgroup = new HibernatePersist<IItemGroup, ItemGroupH>(ItemGroupH.class);
    public static final IPersist<IItem> item = new HibernatePersist<IItem, ItemH>(ItemH.class);
    public static final IPersist<ITag> tag = new HibernatePersist<ITag, TagH>(TagH.class);
    public static final IPersist<ITagCategory> tagcategory = new HibernatePersist<ITagCategory, TagCategoryH>(
            TagCategoryH.class);
    public static final IPersist<IClassDynMetaDataType> classdynmetadatatype = new HibernatePersist<IClassDynMetaDataType, ClassDynMetaDataTypeH>(
            ClassDynMetaDataTypeH.class);
    public static final IPersist<IDynMetaDataType> dynmetadatatype = new HibernatePersist<IDynMetaDataType, DynMetaDataTypeH>(
            DynMetaDataTypeH.class);
    public static final IPersist<IDynMetaDataValue> dynmetadatavalue = new HibernatePersist<IDynMetaDataValue, DynMetaDataValueH>(
            DynMetaDataValueH.class);
    public static final IPersist<IScript> script = new HibernatePersist<IScript, ScriptH>(ScriptH.class);

    // F Persistors
    public static final IPersist<IResource> resource = new HibernatePersist<IResource, ResourceH>(ResourceH.class);
    public static final IPersist<IResourceGroup> resourcegroup = new HibernatePersist<IResourceGroup, ResourceGroupH>(
            ResourceGroupH.class);
    public static final IPersist<ISkillLevel> skillLevel = new HibernatePersist<ISkillLevel, SkillLevelH>(
            SkillLevelH.class);
    public static final IPersist<IQualification> qualification = new HibernatePersist<IQualification, QualificationH>(
            QualificationH.class);
    public static final IPersist<ISkill> skill = new HibernatePersist<ISkill, SkillH>(SkillH.class);
    public static final IPersist<IOrganization> organization = new HibernatePersist<IOrganization, OrganizationH>(
            OrganizationH.class);
    public static final IPersist<IContract> contract = new HibernatePersist<IContract, ContractH>(ContractH.class);

    public static final IPersist<IInventoryItem> inventoryitem = new HibernatePersist<IInventoryItem, InventoryItemH>(
            InventoryItemH.class);
    public static final IPersist<ISoftware> software = new HibernatePersist<ISoftware, SoftwareH>(SoftwareH.class);
    public static final IPersist<IComputer> computer = new HibernatePersist<IComputer, ComputerH>(ComputerH.class);
    public static final IPersist<IRoom> room = new HibernatePersist<IRoom, RoomH>(RoomH.class);
    public static final IPersist<IStudioMap> studiomap = new HibernatePersist<IStudioMap, StudioMapH>(StudioMapH.class);
    public static final IPersist<IComputerType> computerType = new HibernatePersist<IComputerType, ComputerTypeH>(
            ComputerTypeH.class);
    public static final IPersist<IDevice> device = new HibernatePersist<IDevice, DeviceH>(DeviceH.class);
    public static final IPersist<IDeviceType> deviceType = new HibernatePersist<IDeviceType, DeviceTypeH>(
            DeviceTypeH.class);
    public static final IPersist<IScreen> screen = new HibernatePersist<IScreen, ScreenH>(ScreenH.class);
    public static final IPersist<IPool> pool = new HibernatePersist<IPool, PoolH>(PoolH.class);
    public static final IPersist<ILicense> license = new HibernatePersist<ILicense, LicenseH>(LicenseH.class);
    public static final IPersist<IManufacturer> manufacturer = new HibernatePersist<IManufacturer, ManufacturerH>(
            ManufacturerH.class);
    public static final IPersist<IProcessor> processor = new HibernatePersist<IProcessor, ProcessorH>(ProcessorH.class);
    public static final IPersist<IDeviceModel> deviceModel = new HibernatePersist<IDeviceModel, DeviceModelH>(
            DeviceModelH.class);
    public static final IPersist<IScreenModel> screenModel = new HibernatePersist<IScreenModel, ScreenModelH>(
            ScreenModelH.class);
    public static final IPersist<IComputerModel> computerModel = new HibernatePersist<IComputerModel, ComputerModelH>(
            ComputerModelH.class);

    public static final IPersist<IAssetRevision> assetrevision = new HibernatePersist<IAssetRevision, AssetRevisionH>(
            AssetRevisionH.class);
    public static final IPersist<IAssetRevisionLink> assetrevisionlink = new HibernatePersist<IAssetRevisionLink, AssetRevisionLinkH>(
            AssetRevisionLinkH.class);
    public static final IPersist<IAssetRevisionGroup> assetrevisiongroup = new HibernatePersist<IAssetRevisionGroup, AssetRevisionGroupH>(
            AssetRevisionGroupH.class);
    public static final IPersist<IFileRevision> filerevision = new HibernatePersist<IFileRevision, FileRevisionH>(
            FileRevisionH.class);
    // public static final IPersist<IAssetRevisionFileRevision> assetrevisionfilerevision = new
    // HibernatePersist<IAssetRevisionFileRevision, AssetRevisionFileRevisionH>(
    // AssetRevisionFileRevisionH.class);
    public static final IPersist<IFileRevisionLink> filerevisionlink = new HibernatePersist<IFileRevisionLink, FileRevisionLinkH>(
            FileRevisionLinkH.class);
    public static final IPersist<IAssetAbstract> assetabstract = new HibernatePersist<IAssetAbstract, AssetAbstractH>(
            AssetAbstractH.class);

    public static final IPersist<IPlayListRevision> playlistrevision = new HibernatePersist<IPlayListRevision, PlayListRevisionH>(
            PlayListRevisionH.class);
    public static final IPersist<IPlayListEdit> playlistedit = new HibernatePersist<IPlayListEdit, PlayListEditH>(
            PlayListEditH.class);
    public static final IPersist<IFileRevisionAnnotation> filerevisionannotation = new HibernatePersist<IFileRevisionAnnotation, FileRevisionAnnotationH>(
            FileRevisionAnnotationH.class);

    public static final IPersist<IFileAttribute> fileattribute = new HibernatePersist<IFileAttribute, FileAttributeH>(
            FileAttributeH.class);
    public static final IPersist<IRole> role = new HibernatePersist<IRole, RoleH>(RoleH.class);
    public static final IPersist<IUserAccount> useraccount = new HibernatePersist<IUserAccount, UserAccountH>(
            UserAccountH.class);
    public static final IPersist<IApprovalNote> approvalnote = new HibernatePersist<IApprovalNote, ApprovalNoteH>(
            ApprovalNoteH.class);
    public static final IPersist<IApprovalNoteType> approvalnotetype = new HibernatePersist<IApprovalNoteType, ApprovalNoteTypeH>(
            ApprovalNoteTypeH.class);
    public static final IPersist<IEntityTaskLink> entitytasklink = new HibernatePersist<IEntityTaskLink, EntityTaskLinkH>(
            EntityTaskLinkH.class);
    public static final IPersist<IStep> step = new HibernatePersist<IStep, StepH>(StepH.class);
    public static final IPersist<IListValues> listvalues = new HibernatePersist<IListValues, ListValuesH>(
            ListValuesH.class);
    public static final IPersist<IGraphicalAnnotation> graphicalannotation = new HibernatePersist<IGraphicalAnnotation, GraphicalAnnotationH>(
            GraphicalAnnotationH.class);

    public static final IPersist<IRoleTemplate> roletemplates = new HibernatePersist<IRoleTemplate, RoleTemplateH>(
            RoleTemplateH.class);
    public static final IPersist<IProjectSecurityTemplate> securitytemplate = new HibernatePersist<IProjectSecurityTemplate, ProjectSecurityTemplateH>(
            ProjectSecurityTemplateH.class);
    public static final IPersist<IDynamicPath> dynamicpath = new HibernatePersist<IDynamicPath, DynamicPathH>(
            DynamicPathH.class);

    public static final IPersist<IMountPoint> mountpoint = new HibernatePersist<IMountPoint, MountPointH>(
            MountPointH.class);

    public static final IPersist<IProxyType> proxytype = new HibernatePersist<IProxyType, ProxyTypeH>(ProxyTypeH.class);

    public static final IPersist<IProxy> proxy = new HibernatePersist<IProxy, ProxyH>(ProxyH.class);

	public static final IPersist<IPlayListRevisionGroup> playlistrevisiongroup = new HibernatePersist<IPlayListRevisionGroup, PlayListRevisionGroupH>(
            PlayListRevisionGroupH.class);

    public static final IPersist<IHd3dACL> acl = new HibernatePersist<IHd3dACL, Hd3dACLH>(Hd3dACLH.class);

}
