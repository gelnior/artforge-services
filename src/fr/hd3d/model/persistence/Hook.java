package fr.hd3d.model.persistence;

public interface Hook
{
    String getHook();

    void setHook(final String hook);
}
