package fr.hd3d.model.persistence;

public interface Project
{
    IProject getProject();
}
