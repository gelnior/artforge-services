package fr.hd3d.model.persistence;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("Step")
public interface IStep extends IBase
{
    Long getBoundEntity();

    String getBoundEntityName();

    ITaskType getTaskType();

    Boolean getCreateTasknAsset();

    Integer getEstimatedDuration();

    void setBoundEntity(Long boundEntity);

    void setBoundEntityName(String boundEntityName);

    void setTaskType(ITaskType task);

    void setCreateTasknAsset(Boolean createTasknAsset);

    void setEstimatedDuration(Integer estimatedDuration);

    IBase boundEntity() throws Hd3dException;
}
