package fr.hd3d.model.persistence;

/**
 * Free Query.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public interface IFreeQuery extends IBase
{
    // Nothing to do, just acts as a tag
}
