package fr.hd3d.model.persistence;

import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("SimpleActivity")
public interface ISimpleActivity extends IActivity
{
    ESimpleActivityType getType();

    void setWorker(IPerson worker);

    void setProject(IProject project);

    void setType(ESimpleActivityType type);
}
