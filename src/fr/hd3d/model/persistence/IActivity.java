package fr.hd3d.model.persistence;

import java.util.Date;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("Activity")
public interface IActivity extends IBase, Project
{
    String getComment();

    Long getDuration();

    IPersonDay getDay();

    void setComment(String comment);

    void setDuration(Long duration);

    void setDay(IPersonDay personDay);

    IPerson getWorker();

    String taskName();

    IProject getProject();

    IPerson getFilledBy();

    Date getFilledDate();

    void setFilledBy(IPerson filledBy);

    void setFilledDate(Date filledDate);
}
