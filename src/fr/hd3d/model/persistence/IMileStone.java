package fr.hd3d.model.persistence;

import java.util.Date;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author thomas-eskenazi
 * 
 */
@Hd3dEntityName("MileStone")
public interface IMileStone extends IBase
{
    IPlanning getPlanning();

    Date getDate();

    String getTitle();

    String getDescription();

    String getColor();

    void setPlanning(IPlanning planning);

    void setDate(Date date);

    void setTitle(String title);

    void setDescription(String description);

    void setColor(String color);

}
