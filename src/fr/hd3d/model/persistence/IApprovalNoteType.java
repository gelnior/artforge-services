package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("ApprovalNoteType")
public interface IApprovalNoteType extends IBase, Project, Name
{
    IProject getProject();

    ITaskType getTaskType();

    void setTaskType(ITaskType taskType);

    void setProject(IProject project);

}
