package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("Fact")
public interface IFact extends IBase, Name
{

    String getDescription();

    void setDescription(String description);

    IFactType getFactType();

    void setFactType(IFactType facttype);

    IComposition getComposition();

    void setComposition(IComposition composition);

}
