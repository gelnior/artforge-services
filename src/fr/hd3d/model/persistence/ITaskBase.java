package fr.hd3d.model.persistence;

import java.util.Date;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author michael.guiral
 * 
 */
@Hd3dEntityName("TaskBase")
public interface ITaskBase extends IBase, IDurable
{
    Long getDuration();

    void setDuration(Long duration);

    Date getStartDate();

    Date getEndDate();

    void setEndDate(Date endDate);

    void setStartDate(Date startDate);

    ITaskGroup getTaskGroup();

    void setTaskGroup(ITaskGroup taskGroup);

    IPerson getWorker();

    void setWorker(IPerson worker);

    IExtraLine getExtraLine();

    void setExtraLine(IExtraLine extraLine);

}
