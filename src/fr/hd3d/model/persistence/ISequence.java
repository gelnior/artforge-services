package fr.hd3d.model.persistence;

import java.util.Collection;
import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;
import fr.hd3d.model.annotation.Hd3dExposed;


/**
 * a sequence has one and only one parent.
 * 
 * @author try.lam
 * 
 */
@Hd3dEntityName("Sequence")
public interface ISequence extends IBase, IPath, Hook, Project, Name
{
    String getTitle();

    ISequence getParent();

    IProject getProject();

    Set<IShot> getShots();

    Collection<ISequence> getChildren();

    Set<ISequence> parents();

    String getParentIds();

    Collection<ISequence> allChildren(boolean includeSelf);

    void setTitle(String title);

    void setParent(ISequence parent);

    void setProject(IProject project);

    void setChildren(Collection<ISequence> children);

    boolean isRoot();

    String path(String separator, boolean includeProject);

    @Hd3dExposed
    Double shotsApprovalCompletion(String approvalTypeId);

    @Hd3dExposed
    Long sequenceLength();

}
