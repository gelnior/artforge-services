package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for device informations needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Device")
public interface IDevice extends IInventoryItem
{
    Float getSize();

    IDeviceType getDeviceType();

    Set<IComputer> getComputers();

    IDeviceModel getDeviceModel();

    void setSize(Float size);

    void setDeviceType(IDeviceType type);

    void setComputers(Set<IComputer> computers);

    void setDeviceModel(IDeviceModel deviceModel);
}
