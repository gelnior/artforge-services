package fr.hd3d.model.persistence;

import java.util.Date;
import java.util.List;
import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("PersonDay")
public interface IPersonDay extends IBase
{
    Date getDate();

    void setDate(Date date);

    IPerson getPerson();

    void setPerson(IPerson person);

    Set<IActivity> getActivities();

    void setActivities(Set<IActivity> activities);

    IPerson getActivitiesApprovedBy();

    void setActivitiesApprovedBy(IPerson activitiesApprovedBy);

    Date getActivitiesApprovedDate();

    void setActivitiesApprovedDate(Date activitiesApprovedDate);

    List<IWorkingTime> getWorkingTimes();

    void setWorkingTimes(List<IWorkingTime> workingTimes);
}
