package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for software informations needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Software")
public interface ISoftware extends ISimple
{
    Set<ISkill> getSkills();

    void setSkills(Set<ISkill> skills);

    Set<IComputer> getComputers();

    void setComputers(Set<IComputer> computers);

    Set<ILicense> getLicenses();

    void setLicenses(Set<ILicense> licenses);
}
