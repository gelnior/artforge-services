package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("FileAttribute")
public interface IFileAttribute extends IBase, Name
{
    IFileRevision getFileRevision();

    String getValue();

    void setFileRevision(IFileRevision fileRevision);

    void setValue(String value);

}
