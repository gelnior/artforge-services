package fr.hd3d.model.persistence;

import java.util.List;

import fr.hd3d.common.client.enums.EFrameRate;
import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("PlayListRevision")
public interface IPlayListRevision extends IBase, Hook, Project, Name, Copy
{
    IProject getProject();

    EFrameRate getFrameRate();

    Integer getRevision();

    String getComment();

    List<IPlayListEdit> getPlayListEdits();

    void setProject(IProject project);

    void setFrameRate(EFrameRate frameRate);

    void setRevision(Integer revision);

    void setComment(String comment);

    void setPlayListEdits(List<IPlayListEdit> playListEdits);

    IPlayListRevisionGroup getPlayListRevisionGroup();

    void setPlayListRevisionGroup(IPlayListRevisionGroup playListRevisionGroup);
}
