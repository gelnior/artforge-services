package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * 
 * @author HD3D
 * 
 */
@Hd3dEntityName("MountPoint")
public interface IMountPoint extends IBase
{
    String getName();

    String getOperatingSystem();

    String getMountPointPath();

    void setName(String storageName);

    void setOperatingSystem(String operatingSystem);

    void setMountPointPath(String mountPointPath);
}
