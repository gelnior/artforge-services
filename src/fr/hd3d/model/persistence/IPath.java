package fr.hd3d.model.persistence;

/**
 * All classes implementing this interface are those which have some kind of structure: tree... Ex: Constituent's path
 * is all its parent Categories (root Category > sub Category...>).
 * 
 * @author try.lam
 * 
 */
public interface IPath
{
    /*
     * Note: do not change method name: it must start with "get" to be indexed by hibernate search
     */
    String getPath();
}
