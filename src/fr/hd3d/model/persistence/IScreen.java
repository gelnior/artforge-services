package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.common.client.enums.EScreenQualification;
import fr.hd3d.common.client.enums.EScreenType;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for device informations needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Screen")
public interface IScreen extends IInventoryItem
{
    Float getSize();

    EScreenType getType();

    EScreenQualification getQualification();

    IScreenModel getScreenModel();

    Set<IComputer> getComputers();

    void setSize(Float size);

    void setType(EScreenType type);

    void setQualification(EScreenQualification qualification);

    void setScreenModel(IScreenModel screenModel);

    void setComputers(Set<IComputer> computers);
}
