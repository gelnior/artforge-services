package fr.hd3d.model.persistence;

import java.util.Collection;


public interface IProxyType extends IBase, Name
{
    String getOpenType();// mime-type

    String getDescription();

    Collection<IProxy> getProxies();

    void setProxies(Collection<IProxy> proxies);

    void setOpenType(String openType);

    void setDescription(String description);
}
