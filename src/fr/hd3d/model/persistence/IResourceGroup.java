package fr.hd3d.model.persistence;

import java.util.Collection;
import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for CPU informations needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("ResourceGroup")
public interface IResourceGroup extends ISimple
{
    IResourceGroup getParent();

    void setParent(IResourceGroup parent);

    Collection<IResourceGroup> getChildren();

    Collection<IResourceGroup> allChildren(boolean includeSelf);

    void setChildren(Collection<IResourceGroup> children);

    IPerson getLeader();

    void setLeader(IPerson leader);

    Set<IResource> getResources();

    void setResources(Set<IResource> ressources);

    Set<IProject> getProjects();

    void setProjects(Set<IProject> projects);

    Set<IOrganization> getOrganizations();

    void setOrganizations(Set<IOrganization> organizations);

    Set<IRole> getRoles();

    void setRoles(Set<IRole> roles);

    void add(IRole role);

    void addPerson(IPerson person);

    IResource getResourceById(Long id);

    Set<IPerson> getStaff();

    Set<IRole> parentsRoles(boolean includeSelf);
}
