/**
 * 
 */
package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author thomas-eskenazi
 * 
 */
@Hd3dEntityName("ExtraLine")
public interface IExtraLine extends IBase, Name
{
    Set<ITaskBase> getTaskBases();

    ITaskGroup getTaskGroup();

    IPerson getHiddenPerson();

    void setTaskGroup(ITaskGroup taskGroup);

    void setHiddenPerson(IPerson person);

}
