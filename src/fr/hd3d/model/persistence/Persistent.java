package fr.hd3d.model.persistence;

public interface Persistent
{
    Long getId();

    void setId(final Long id);

    java.sql.Timestamp getVersion();

    void setVersion(final java.sql.Timestamp version);
}
