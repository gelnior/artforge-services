package fr.hd3d.model.persistence;

import java.util.List;

import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("Sheet")
public interface ISheet extends IBase, Project, Name
{

    String getDescription();

    IProject getProject();

    String getBoundClassName();

    List<IItemGroup> getItemGroups();

    void addItemGroup(IItemGroup itemGroup);

    ESheetType getType();

    void setDescription(String description);

    void setProject(IProject project); // can be null

    void setBoundClassName(String boundclassname);

    void setItemGroups(List<IItemGroup> itemgroups);

    void setType(ESheetType type);

}
