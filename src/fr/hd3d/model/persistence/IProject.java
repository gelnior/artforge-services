package fr.hd3d.model.persistence;

import java.util.Date;
import java.util.List;
import java.util.Set;

import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 * 
 */
@Hd3dEntityName("Project")
public interface IProject extends IBase, Hook, Name
{
    String getColor();

    Date getStartDate();

    Date getEndDate();

    EProjectStatus getStatus();

    Set<IResourceGroup> getResourceGroups();

    void addResourceGroup(IResourceGroup resourceGroup);

    void setResourceGroups(Set<IResourceGroup> resourceGroups);

    Set<ITaskType> getTaskTypes();

    void setTaskTypes(Set<ITaskType> taskTypes);

    Set<ISimpleActivity> getSimpleActivities();

    void setColor(String color);

    void setStartDate(Date startDate);

    void setEndDate(Date endDate);

    void setStatus(EProjectStatus status);

    void setSimpleActivities(Set<ISimpleActivity> simpleActivities);

    Set<ICategory> getCategories();

    void setCategories(Set<ICategory> categories);

    void addCategory(ICategory category);

    ICategory rootCategory();

    Set<ISequence> getSequences();

    void setSequences(Set<ISequence> sequences);

    void addSequence(ISequence sequence);

    ISequence rootSequence();

    void addSheet(ISheet sheet);

    Set<IPerson> getStaff();

    IProjectType getProjectType();

    void setProjectType(IProjectType projectType);

    List<ISheet> getSheets();

    void setSheets(List<ISheet> sheets);
}
