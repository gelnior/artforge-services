package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;
import fr.hd3d.model.annotation.Hd3dExposed;


/**
 * Interface describing the model entity for CPU informations needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Resource")
public interface IResource extends IBase
{
    Set<IResourceGroup> getResourceGroups();

    void setResourceGroups(Set<IResourceGroup> resourceGroups);

    String getResourceName();

    @Hd3dExposed
    Set<IProject> getProjects();
}
