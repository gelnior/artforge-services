package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author michael.guiral
 * 
 */
@Hd3dEntityName("Absence")
public interface IAbsence extends IBase, IDurable
{

    IPerson getWorker();

    void setWorker(IPerson person);

    String getComment();

    void setComment(String comment);

    String getType();

    void setType(String type);

}
