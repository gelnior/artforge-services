package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("PlayListEdit")
public interface IPlayListEdit extends IBase, Name
{

    Integer getSrcIn();

    Integer getSrcOut();

    Integer getRecIn();

    Integer getRecOut();

    // IFileRevision getFileRevision();
    IProxy getProxy();

    IPlayListRevision getPlayListRevision();

    void setSrcIn(Integer srcIn);

    void setSrcOut(Integer srcOut);

    void setRecIn(Integer recIn);

    void setRecOut(Integer recOut);

    // void setFileRevision(IFileRevision fileRevision);
    void setProxy(IProxy proxy);

    void setPlayListRevision(IPlayListRevision playListRevision);

}
