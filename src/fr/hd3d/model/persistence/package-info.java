/**
  * A34 time sheet implementation<br>
  * <i>https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html</i><br>
  * High level classes and (mainly) interfaces for Hd3d objects&#46;
  * Implementors are in separate packages.
  */
package fr.hd3d.model.persistence;

