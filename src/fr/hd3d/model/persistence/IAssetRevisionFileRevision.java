package fr.hd3d.model.persistence;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dEntityName;


@Hd3dEntityName("AssetRevisionFileRevision")
public interface IAssetRevisionFileRevision extends IBase
{
    String getAssetKey();

    String getFileKey();

    String getAssetVariation();

    String getFileVariation();

    Integer getAssetRevision();

    Integer getFileRevision();

    void setAssetKey(String assetKey);

    void setFileKey(String fileKey);

    void setAssetVariation(String assetVariation);

    void setFileVariation(String fileVariation);

    void setAssetRevision(Integer assetRevision);

    void setFileRevision(Integer fileRevision);

    IAssetRevision getAssetRevisionObject() throws Hd3dException;

    IFileRevision getFileRevisionObject() throws Hd3dException;

    IFileRevision getFileRevisionObject(boolean permCheck) throws Hd3dException;
}
