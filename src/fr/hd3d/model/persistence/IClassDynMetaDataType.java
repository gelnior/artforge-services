package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("ClassDynMetaDataType")
public interface IClassDynMetaDataType extends IBase, Name
{
    IDynMetaDataType getDynMetaDataType();

    String getClassName();

    String getPredicate();

    String getExtra();

    void setDynMetaDataType(IDynMetaDataType dynmetadatetype);

    void setClassName(String className);

    void setPredicate(String predicate);

    void setExtra(String extra);
}
