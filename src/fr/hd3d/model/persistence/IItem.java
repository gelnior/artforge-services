package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("Item")
public interface IItem extends IBase, Name
{
    String getRenderer();

    String getEditor();

    String getType();

    String getQuery();

    String getMetaType();

    String getControlContent();

    IItemGroup getItemGroup();

    String getTransformer();

    String getTransformerParameters();

    void setType(String type);

    void setQuery(String query);

    void setControlContent(String controlcontent);

    void setItemGroup(IItemGroup itemgroup);

    void setMetaType(String metatype);

    void setRenderer(String renderer);

    void setEditor(String editor);

    void setTransformer(String transformer);

    void setTransformerParameters(String transformerParameters);

    Integer getIndex();

    void setIndex(Integer index);
}
