package fr.hd3d.model.persistence;

import java.util.Set;

import fr.hd3d.common.client.enums.EWorkerStatus;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for computer informations needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Computer")
public interface IComputer extends IInventoryItem
{
    Long getInventoryId();

    String getDnsName();

    String getIpAdress();

    String getMacAdress();

    Integer getProcFrequency();

    Integer getNumberOfProcs();

    Integer getNumberOfCores();

    Integer getRamQuantity();

    EWorkerStatus getWorkerStatus();

    /* Note: "x" is in lower case on purpose, otherwise will be confusing for reflection api */
    Integer getxPosition();

    /* Note: "x" is in lower case on purpose, otherwise will be confusing for reflection api */
    Integer getyPosition();

    IProcessor getProcessor();

    IComputerType getComputerType();

    IComputerModel getComputerModel();

    IStudioMap getStudioMap();

    IRoom getRoom();

    IPerson getPerson();

    Set<IPool> getPools();

    Set<ISoftware> getSoftwares();

    Set<ILicense> getLicenses();

    Set<IDevice> getDevices();

    Set<IScreen> getScreens();

    void setInventoryId(Long inventoryId);

    void setDnsName(String dnsName);

    void setMacAdress(String macAdress);

    void setIpAdress(String ipAdress);

    void setProcFrequency(Integer procFrequency);

    void setNumberOfProcs(Integer numberOfProcs);

    void setNumberOfCores(Integer numberOfCores);

    void setRamQuantity(Integer ramQuantity);

    void setWorkerStatus(EWorkerStatus status);

    /* Note: "x" is in lower case on purpose, otherwise will be confusing for reflection api */
    void setxPosition(Integer xPosition);

    /* Note: "x" is in lower case on purpose, otherwise will be confusing for reflection api */
    void setyPosition(Integer yPosition);

    void setStudioMap(IStudioMap studioMap);

    void setRoom(IRoom room);

    void setProcessor(IProcessor procssor);

    void setComputerType(IComputerType computerType);

    void setComputerModel(IComputerModel computerModel);

    void setPerson(IPerson person);

    void setPools(Set<IPool> pool);

    void setSoftwares(Set<ISoftware> softwares);

    void setLicenses(Set<ILicense> licenses);

    void setDevices(Set<IDevice> devices);

    void setScreens(Set<IScreen> screens);
}
