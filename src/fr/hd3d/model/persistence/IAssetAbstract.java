package fr.hd3d.model.persistence;

import java.util.Date;
import java.util.Set;


public interface IAssetAbstract extends IBase, Project, Name
{
    Date getCreationDate();

    IPerson getCreator();

    String getKey();

    IProject getProject();

    Set<IAssetRevision> getRevisions();

    ITaskType getTaskType();

    String getVariation();

    void setCreationDate(Date creationDate);

    void setCreator(IPerson creator);

    void setKey(String key);

    void setProject(IProject project);

    void setTaskType(ITaskType type);

    void setVariation(String variation);

}
