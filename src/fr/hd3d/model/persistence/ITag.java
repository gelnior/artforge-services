package fr.hd3d.model.persistence;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.multimap.MultiHashMap;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dEntityName;
import fr.hd3d.model.lightweight.impl.LTag;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("Tag")
public interface ITag extends IBase, Name
{

    <T extends IBase> void addParent(T object);

    <T extends IBase> void removeParent(T object);

    <T extends IBase> void removeParents(Collection<T> objects);

    void removeInverseTagParent(Long parentId, String parentType);

    List<LTag.LTagParentIdType> parentsIdType();

    boolean containsParent(Long id, String type);

    <T extends IBase> Collection<T> parentsByType(String type);

    List<TagParent> getInverseTagParents();

    void setInverseTagParents(List<TagParent> inverseTagParents);

    List<ITagCategory> getCategories();

    void setCategories(List<ITagCategory> categories);

    <T extends IBase> List<T> taggedEntities() throws Hd3dException;

    MultiHashMap<String, Long> taggedEntitiesIds() throws Hd3dException;
}
