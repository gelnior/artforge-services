package fr.hd3d.model.persistence;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * 
 * @author Try LAM
 * 
 */
@Hd3dEntityName("Script")
public interface IScript extends IBase
{
    String getTrigger();

    String getName();

    String getDescription();

    String getLocation();

    Integer getExitCode();

    java.util.Date getLastRun();

    Integer getLastExitCode();

    String getLastRunMessage();

    void setTrigger(String trigger);

    void setName(String name) throws Hd3dException;

    void setDescription(String description);

    void setLocation(String location);

    void setExitCode(Integer exitCode);

    void setLastRun(java.util.Date lastRun);

    void setLastExitCode(Integer lastExitCode);

    void setLastRunMessage(String lastRunMessage);
}
