package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;
import fr.hd3d.model.annotation.Hd3dExposed;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dEntityName("Constituent")
public interface IConstituent extends IBase, IPath, Hook
{
    ICategory getCategory();

    String getLabel();

    String getDescription();

    Integer getDifficulty();

    Integer getTrust();

    Boolean getDesign();

    Boolean getModeling();

    Boolean getSetup();

    Boolean getTexturing();

    Boolean getShading();

    Boolean getHairs();

    Integer getCompletion();

    String getPath(String separator, boolean includeProject);

    ICategory[] parents();

    @Hd3dExposed
    String thumbnail();

    // setters
    void setCategory(ICategory category);

    void setLabel(String label);

    void setDescription(String desc);

    void setDifficulty(Integer difficulty);

    void setTrust(Integer trust);

    void setDesign(Boolean design);

    void setModeling(Boolean modeling);

    void setSetup(Boolean setup);

    void setTexturing(Boolean texturing);

    void setShading(Boolean shading);

    void setHairs(Boolean hairs);

    void setCompletion(Integer completion);

    void addComposition(IComposition compo);

}
