package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * @author Try LAM
 */
@Hd3dEntityName("ListValues")
public interface IListValues extends IBase
{
    String getSeparator();

    void setSeparator(String separator);

    String getValues();

    void setValues(String values);
}
