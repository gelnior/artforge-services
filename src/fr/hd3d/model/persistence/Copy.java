package fr.hd3d.model.persistence;

public interface Copy
{
    <P extends Object> P copy();
}
