package fr.hd3d.model.persistence;

import java.util.Collection;
import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Interface describing the model entity for CPU informations needed by the computer inventory application.
 * 
 * @author HD3D
 */
@Hd3dEntityName("Organization")
public interface IOrganization extends ISimple
{
    final static String ROOT = "ROOT";

    IOrganization getParent();

    void setParent(IOrganization parent);

    Collection<IOrganization> getChildren();

    Collection<IOrganization> allChildren(boolean includeSelf);

    void setChildren(Collection<IOrganization> children);

    Set<IContract> getContracts();

    void setContracts(Set<IContract> contracts);

    Set<IResourceGroup> getResourceGroups();

    void setResourceGroups(Set<IResourceGroup> ressources);
}
