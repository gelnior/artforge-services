/**
 * 
 */
package fr.hd3d.model.persistence;

import fr.hd3d.model.annotation.Hd3dEntityName;


/**
 * Task type describes a build step for artforge projects. It makes the link between the main entities of a project :
 * tasks, assets, notes...
 * 
 * @author HD3D
 */
@Hd3dEntityName("TaskType")
public interface ITaskType extends IBase, Project, Name
{

    IProject getProject();

    String getColor();

    String getDescription();

    String getEntityName();

    void setProject(IProject project);

    void setColor(String color);

    void setDescription(String description);

    void setEntityName(String entityName);

}
