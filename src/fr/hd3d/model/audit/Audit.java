package fr.hd3d.model.audit;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;

import fr.hd3d.model.persistence.impl.hibernate.IComplexTypeProvider;


/**
 * Logs any CUD (Creation, Update, Delete) operation on business entities. Implementation note: this class must not
 * subclass BaseHibernateImpl to no consume uselessly id sequences.
 * 
 * @author try.lam
 * 
 */

@Entity
@Table(name = "hd3d_audit")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true, dynamicInsert = true, dynamicUpdate = true, optimisticLock = OptimisticLockType.DIRTY)
public class Audit implements Serializable
{
    private static final long serialVersionUID = -7213204302814136485L;

    private static final String DATE_FIELD = "date";
    public static final char MODIFICATION_SEP = '|';
    public static final String CREATE = "CREATE";
    public static final String UPDATE = "UPDATE";
    public static final String DELETE = "DELETE";

    public final static String IS_NOW = " is now";
    public final static String NULL = " null";
    public final static String WAS = " was ";
    public final static String NOW = ", now ";

    private String uuid;
    private Timestamp date;
    private Long dateMs;// mysql does not handle milliseconds in dates, so better store date as ms.
    private Long person;// use id instead of entity for decoupling
    private String login;
    private String firstName;
    private String lastName;
    private String entityName;
    private Long entityId;
    private String operation;
    private String modification = "";

    private StringBuilder buf = new StringBuilder(50);

    public Audit()
    {}

    public Audit(final Long date, final Long person, final String entityName, final Long entityId,
            final String operation, final String modification)
    {
        this.dateMs = date;
        this.person = person;
        this.entityName = entityName;
        this.entityId = entityId;
        this.operation = operation;
        this.modification = modification;
        uuid = java.util.UUID.randomUUID().toString();
    }

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getUuid()
    {
        return uuid;
    }

    @Column(name = "audit_date", updatable = false)
    public Long getDateMs()
    {
        return dateMs;
    }

    @Transient
    public Date getDate()
    {
        return date;
    }

    public void setDate(final Timestamp date)
    {
        this.date = date;
        setDateMs(date.getTime());
    }

    @Column(name = "audit_person", updatable = false)
    public Long getPerson()
    {
        return person;
    }

    @Column(name = "audit_login", updatable = false)
    public String getLogin()
    {
        return login;
    }

    @Column(name = "audit_firstname", updatable = false)
    public String getFirstName()
    {
        return firstName;
    }

    @Column(name = "audit_lastname", updatable = false)
    public String getLastName()
    {
        return lastName;
    }

    @Column(name = "audit_entityname", updatable = false)
    public String getEntityName()
    {
        return entityName;
    }

    @Column(name = "audit_entityid", updatable = false)
    public Long getEntityId()
    {
        return entityId;
    }

    @Column(name = "audit_operation", updatable = false)
    public String getOperation()
    {
        return operation;
    }

    @Column(name = "audit_modification", length = 1000, updatable = false)
    public String getModification()
    {
        return modification;
    }

    public void setUuid(final String uuid)
    {
        this.uuid = uuid;
    }

    public void setDateMs(final Long date)
    {
        this.dateMs = date;
        this.date = new Timestamp(date);
    }

    public void setPerson(final Long person)
    {
        this.person = person;
    }

    public void setLogin(final String login)
    {
        this.login = login;
    }

    public void setFirstName(final String firstName)
    {
        this.firstName = firstName;
    }

    public void setLastName(final String lastName)
    {
        this.lastName = lastName;
    }

    public void setEntityName(final String entityName)
    {
        this.entityName = entityName;
    }

    public void setEntityId(final Long entityId)
    {
        this.entityId = entityId;
    }

    public void setOperation(final String operation)
    {
        this.operation = operation;
    }

    public void setModification(final String modification)
    {
        this.modification = modification;
    }

    public void addModification(final String modification)
    {
        this.buf.append(modification);
    }

    public void flushModification()
    {
        String log = buf.toString();
        if (log.length() > 1000)
        {/* length defined in Audit.class, audit_modification field */
            log = log.substring(0, 1000);
        }
        setModification(log.trim());
        buf = new StringBuilder(100);
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateMs == null) ? 0 : dateMs.hashCode());
        result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
        result = prime * result + ((entityName == null) ? 0 : entityName.hashCode());
        result = prime * result + ((modification == null) ? 0 : modification.hashCode());
        result = prime * result + ((operation == null) ? 0 : operation.hashCode());
        result = prime * result + ((person == null) ? 0 : person.hashCode());
        result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Audit other = (Audit) obj;
        if (dateMs == null)
        {
            if (other.dateMs != null)
                return false;
        }
        else if (!dateMs.equals(other.dateMs))
            return false;
        if (entityId == null)
        {
            if (other.entityId != null)
                return false;
        }
        else if (!entityId.equals(other.entityId))
            return false;
        if (entityName == null)
        {
            if (other.entityName != null)
                return false;
        }
        else if (!entityName.equals(other.entityName))
            return false;
        if (modification == null)
        {
            if (other.modification != null)
                return false;
        }
        else if (!modification.equals(other.modification))
            return false;
        if (operation == null)
        {
            if (other.operation != null)
                return false;
        }
        else if (!operation.equals(other.operation))
            return false;
        if (person == null)
        {
            if (other.person != null)
                return false;
        }
        else if (!person.equals(other.person))
            return false;
        if (uuid == null)
        {
            if (other.uuid != null)
                return false;
        }
        else if (!uuid.equals(other.uuid))
            return false;
        return true;
    }

    public static class AuditComplexTypeProvider implements IComplexTypeProvider
    {
        public List<String> getValidEnum(final String field)
        {
            return Collections.emptyList();
        }

        public Object getValue(final String field, final String value)
        {
            if (!isValidTypeField(field))
                return null;

            return fr.hd3d.utils.StringUtils.parseDate(value);
        }

        public boolean isValidTypeField(final String field)
        {
            return DATE_FIELD.equals(field);
        }
    }

}
