package fr.hd3d.model.audit;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.Parameter;

import fr.hd3d.model.persistence.Persistent;


/**
 * 
 * @author try.lam
 * 
 */

@Entity
@Table(name = "hd3d_updatestack")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class UpdatesStack implements Serializable, Persistent
{
    private static final long serialVersionUID = 1L;
    private Long id;
    protected java.sql.Timestamp version;
    private String operation;

    public UpdatesStack()
    {}

    public UpdatesStack(final String operation)
    {
        super();
        this.operation = operation;
    }

    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_updatestack"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    @Version
    public java.sql.Timestamp getVersion()
    {
        return version;
    }

    @Column(name = "updatestack_operation")
    public String getOperation()
    {
        return operation;
    }

    public void setOperation(String operation)
    {
        this.operation = operation;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setVersion(final java.sql.Timestamp version)
    {
        this.version = version;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((operation == null) ? 0 : operation.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UpdatesStack other = (UpdatesStack) obj;
        if (operation == null)
        {
            if (other.operation != null)
                return false;
        }
        else if (!operation.equals(other.operation))
            return false;
        return true;
    }

    public String toString()
    {
        return new ToStringBuilder(this).append("operation", operation).toString();
    }
}
