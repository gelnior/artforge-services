package fr.hd3d.model.audit;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.hd3d.common.client.DateFormat;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


public final class Purge
{
    private Purge()
    {}

    private static final String AUDITFILENAME = "audits_{0}.csv";
    private static final String SEP = Conf.getAuditPurgeSeparator();
    private static final String AUDIT_DIR = Conf.getAuditPurgeDir();
    private static final Long AUDIT_PERIOD;
    private static final String UPDATESSTACKSFILENAME = "updatesstacks_{0}.csv";
    private static final String UPDATESSTACKS_DIR = Conf.getUpdatesStackPurgeDir();
    private static final Long UPDATESSTACKS_PERIOD;
    static
    {
        final Long auditPeriod = NumberUtils.toLong(Conf.getAuditPurgePeriod());
        AUDIT_PERIOD = auditPeriod > 0 ? auditPeriod : 3600L;// default period is one hour
        final Long updatesStacksPeriod = NumberUtils.toLong(Conf.getUpdatesStackPurgePeriod());
        UPDATESSTACKS_PERIOD = updatesStacksPeriod > 0 ? updatesStacksPeriod : 3600L;// default period is one hour
    }

    private static final ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(2);

    public static void purge()
    {
        final Runnable auditPurger = new Runnable() {
            public void run()
            {
                // purge oldest audits
                Log.LOGGER.info("purging audits...");
                List<Audit> audits = audits();
                try
                {
                    String filePath = auditsToCSV(audits);
                    Log.LOGGER.info("purge done: output file {}", filePath);
                    removeAudits(audits);
                }
                catch (IOException e)
                {
                    Log.LOGGER.error("Error occured when flushing audits into file.");
                    Log.LOGGER.error(ExceptUtils.format(e));
                }
                finally
                {
                    if (audits != null)
                    {
                        audits.clear();
                    }
                }
            }
        };

        final Runnable updatesStacksPurger = new Runnable() {
            public void run()
            {
                // purge oldest updatesStacks
                Log.LOGGER.info("purging updatesStacks...");
                List<UpdatesStack> stacks = updatesStacks();
                try
                {
                    String filePath = updatesStacksToCSV(stacks);
                    Log.LOGGER.info("purge updatesStacks done: output file {}", filePath);
                    removeUpdatesStacks(stacks);
                }
                catch (IOException e)
                {
                    Log.LOGGER.error("Error occured when flushing updatesStacks into file.");
                    Log.LOGGER.error(ExceptUtils.format(e));
                }
                finally
                {
                    if (stacks != null)
                    {
                        stacks.clear();
                    }
                }
            }
        };

        SCHEDULER.scheduleWithFixedDelay(auditPurger, 5, AUDIT_PERIOD, SECONDS);
        SCHEDULER.scheduleWithFixedDelay(updatesStacksPurger, 5, UPDATESSTACKS_PERIOD, SECONDS);
    }

    public static void stop()
    {
        SCHEDULER.shutdown();
    }

    @SuppressWarnings("unchecked")
    private static List<Audit> audits()
    {
        return HibernateUtil.currentSession().createCriteria(Audit.class).setLockMode(LockMode.READ).list();
    }

    @SuppressWarnings("unchecked")
    private static List<UpdatesStack> updatesStacks()
    {
        return HibernateUtil.currentSession().createCriteria(UpdatesStack.class).setLockMode(LockMode.OPTIMISTIC)
                .list();
    }

    private static void removeAudits(final List<Audit> audits)
    {
        if (CollectionUtils.isNotEmpty(audits))
        {
            final Session session = HibernateUtil.currentSession();
            Transaction tx = session.beginTransaction();
            try
            {
                HibernatePersist.deleteRawCollection(audits);
                session.flush();
                tx.commit();
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
            finally
            {
                if (tx != null)
                {
                    tx = null;
                }
            }
        }
    }

    private static void removeUpdatesStacks(final List<UpdatesStack> stacks)
    {
        if (CollectionUtils.isNotEmpty(stacks))
        {
            final Session session = HibernateUtil.currentSession();
            Transaction tx = session.beginTransaction();
            try
            {
                HibernatePersist.deleteRawCollection(stacks);
                session.flush();
                tx.commit();
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
            finally
            {
                if (tx != null)
                {
                    tx = null;
                }
            }
        }
    }

    private static String getAuditFileName()
    {
        return MessageFormat.format(AUDITFILENAME, DateFormatUtils.format(new Date(),
                DateFormat.AUDIT_PURGE_FILE_TIMESTAMP_FORMAT));
    }

    private static String getUpdatesStacksFileName()
    {
        return MessageFormat.format(UPDATESSTACKSFILENAME, DateFormatUtils.format(new Date(),
                DateFormat.AUDIT_PURGE_FILE_TIMESTAMP_FORMAT));
    }

    private static String auditsToCSV(final List<Audit> audits) throws IOException
    {
        String ret = null;

        // Note: do not test for empty list: to be output to file even empty
        if (audits != null)
        {
            final File file = new File(AUDIT_DIR + File.separatorChar + getAuditFileName());

            if (file != null)
            {
                FileWriter fstream = null;
                BufferedWriter out = null;
                try
                {
                    fstream = new FileWriter(file);
                    out = new BufferedWriter(fstream);

                    CollectUtils.removeNull(audits);

                    Object[] fields = new Object[9];
                    for (Audit audit : audits)
                    {
                        fields[0] = audit.getDate();
                        fields[1] = audit.getEntityId();
                        fields[2] = audit.getEntityName();
                        fields[3] = audit.getOperation();
                        fields[4] = audit.getModification();
                        fields[5] = audit.getPerson();
                        fields[6] = audit.getLogin();
                        fields[7] = audit.getFirstName();
                        fields[8] = audit.getLastName();

                        out.write(StringUtils.join(fields, SEP) + SystemUtils.LINE_SEPARATOR);
                    }
                }
                finally
                {
                    if (out != null)
                    {
                        out.close();
                    }
                    if (fstream != null)
                    {
                        fstream.close();
                    }
                }

                ret = file.getAbsolutePath().toString();
            }
        }

        return ret;
    }

    private static String updatesStacksToCSV(final List<UpdatesStack> stacks) throws IOException
    {
        String ret = null;

        // Note: do not test for empty list: to be output to file even empty
        if (stacks != null)
        {
            final File file = new File(UPDATESSTACKS_DIR + File.separatorChar + getUpdatesStacksFileName());

            if (file != null)
            {
                FileWriter fstream = null;
                BufferedWriter out = null;
                try
                {
                    fstream = new FileWriter(file);
                    out = new BufferedWriter(fstream);

                    CollectUtils.removeNull(stacks);

                    Object[] fields = new Object[3];
                    for (UpdatesStack stack : stacks)
                    {
                        fields[0] = stack.getId();
                        fields[1] = stack.getVersion();
                        fields[2] = stack.getOperation();

                        out.write(StringUtils.join(fields, ';') + SystemUtils.LINE_SEPARATOR);
                    }
                }
                finally
                {
                    if (out != null)
                    {
                        out.close();
                    }
                    if (fstream != null)
                    {
                        fstream.close();
                    }
                }

                ret = file.getAbsolutePath().toString();
            }
        }

        return ret;
    }
}
