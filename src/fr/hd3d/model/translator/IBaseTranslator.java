package fr.hd3d.model.translator;

import java.util.Collection;
import java.util.List;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.impl.ILPersistent;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.services.resources.ResourceContext;


/**
 * Base interface for object translators.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 * @param <L>
 *            Lightweight object
 * @param <P>
 *            Regular object
 */
public interface IBaseTranslator<L extends ILPersistent, P extends Persistent>
{

    /**
     * translates a regular object to a lightweight object
     * 
     * @param object
     *            SOURCE Regular object
     * @param map
     * @return DESTINATION lightweight object
     * @throws Hd3dTranslationException
     * @throws Hd3dException
     */
    public L toLightweight(final P object, ResourceContext<?, ?> context) throws Hd3dException;

    /**
     * translates a lightweight object to a regular object
     * 
     * @param light
     *            SOURCE Lightweight object
     * @return Regular DESTINATION regular object
     * @throws Hd3dTranslationException
     * @throws Hd3dException
     */
    public P fromLightweight(final L light, ResourceContext<?, ?> context) throws Hd3dException;

    /**
     * updates a regular object with a lightweight object
     * 
     * @param light
     *            SOURCE lightweight object
     * @param object
     *            DESTINATION regular object
     * @throws Hd3dTranslationException
     */
    public void updateFromLightweight(final L light, final P object, ResourceContext<?, ?> context)
            throws Hd3dException;

    public List<P> fromLightweightCollection(Collection<L> lights, ResourceContext<?, ?> context) throws Hd3dException;

    public List<L> toLightweightCollection(Collection<P> objects, ResourceContext<?, ?> context) throws Hd3dException;

    public void updateFromLightweightCollection(final List<L> lights, final List<P> objects,
            ResourceContext<?, ?> context) throws Hd3dException;

}
