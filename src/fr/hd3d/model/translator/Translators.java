package fr.hd3d.model.translator;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ClassUtils;

import fr.hd3d.model.lightweight.ILAbsence;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.lightweight.ILApprovalNoteType;
import fr.hd3d.model.lightweight.ILAssetAbstract;
import fr.hd3d.model.lightweight.ILAssetRevision;
import fr.hd3d.model.lightweight.ILAssetRevisionGroup;
import fr.hd3d.model.lightweight.ILAssetRevisionLink;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.lightweight.ILClassDynMetaDataType;
import fr.hd3d.model.lightweight.ILComposition;
import fr.hd3d.model.lightweight.ILComputer;
import fr.hd3d.model.lightweight.ILComputerModel;
import fr.hd3d.model.lightweight.ILComputerType;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.lightweight.ILConstituentLink;
import fr.hd3d.model.lightweight.ILContract;
import fr.hd3d.model.lightweight.ILDevice;
import fr.hd3d.model.lightweight.ILDeviceModel;
import fr.hd3d.model.lightweight.ILDeviceType;
import fr.hd3d.model.lightweight.ILDynMetaDataType;
import fr.hd3d.model.lightweight.ILDynMetaDataValue;
import fr.hd3d.model.lightweight.ILDynamicPath;
import fr.hd3d.model.lightweight.ILEntityTaskLink;
import fr.hd3d.model.lightweight.ILEvent;
import fr.hd3d.model.lightweight.ILExtraLine;
import fr.hd3d.model.lightweight.ILFact;
import fr.hd3d.model.lightweight.ILFactType;
import fr.hd3d.model.lightweight.ILFileAttribute;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.lightweight.ILFileRevisionAnnotation;
import fr.hd3d.model.lightweight.ILFileRevisionLink;
import fr.hd3d.model.lightweight.ILGraphicalAnnotation;
import fr.hd3d.model.lightweight.ILHd3dACL;
import fr.hd3d.model.lightweight.ILHint;
import fr.hd3d.model.lightweight.ILItem;
import fr.hd3d.model.lightweight.ILItemGroup;
import fr.hd3d.model.lightweight.ILLicense;
import fr.hd3d.model.lightweight.ILListValues;
import fr.hd3d.model.lightweight.ILManufacturer;
import fr.hd3d.model.lightweight.ILMileStone;
import fr.hd3d.model.lightweight.ILMountPoint;
import fr.hd3d.model.lightweight.ILOrganization;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.lightweight.ILPlayListEdit;
import fr.hd3d.model.lightweight.ILPlayListRevision;
import fr.hd3d.model.lightweight.ILPlayListRevisionGroup;
import fr.hd3d.model.lightweight.ILPool;
import fr.hd3d.model.lightweight.ILProcessor;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILProjectSecurityTemplate;
import fr.hd3d.model.lightweight.ILProjectType;
import fr.hd3d.model.lightweight.ILProxy;
import fr.hd3d.model.lightweight.ILProxyType;
import fr.hd3d.model.lightweight.ILQualification;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.lightweight.ILRoom;
import fr.hd3d.model.lightweight.ILScreen;
import fr.hd3d.model.lightweight.ILScreenModel;
import fr.hd3d.model.lightweight.ILScript;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.lightweight.ILSheet;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.lightweight.ILSkill;
import fr.hd3d.model.lightweight.ILSkillLevel;
import fr.hd3d.model.lightweight.ILSoftware;
import fr.hd3d.model.lightweight.ILStep;
import fr.hd3d.model.lightweight.ILStudioMap;
import fr.hd3d.model.lightweight.ILTag;
import fr.hd3d.model.lightweight.ILTagCategory;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.lightweight.ILTaskBase;
import fr.hd3d.model.lightweight.ILTaskChanges;
import fr.hd3d.model.lightweight.ILTaskGroup;
import fr.hd3d.model.lightweight.ILTaskType;
import fr.hd3d.model.lightweight.ILWorkingTime;
import fr.hd3d.model.lightweight.impl.LSheetFilter;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IAssetAbstract;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IComputerModel;
import fr.hd3d.model.persistence.IComputerType;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IConstituentLink;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.IDeviceModel;
import fr.hd3d.model.persistence.IDeviceType;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IEvent;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IFact;
import fr.hd3d.model.persistence.IFactType;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IFileRevisionAnnotation;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.IHint;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.IManufacturer;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.IOrganization;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.IProcessor;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.IQualification;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.IRoom;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.IScreenModel;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ISkill;
import fr.hd3d.model.persistence.ISkillLevel;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskBase;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.preferences.SheetFilter;
import fr.hd3d.model.translator.impl.AbsenceTranslator;
import fr.hd3d.model.translator.impl.ActivityTranslator;
import fr.hd3d.model.translator.impl.ApprovalNoteTranslator;
import fr.hd3d.model.translator.impl.ApprovalNoteTypeTranslator;
import fr.hd3d.model.translator.impl.AssetAbstractTranslator;
import fr.hd3d.model.translator.impl.AssetRevisionGroupTranslator;
import fr.hd3d.model.translator.impl.AssetRevisionLinkTranslator;
import fr.hd3d.model.translator.impl.AssetRevisionTranslator;
import fr.hd3d.model.translator.impl.CategoryTranslator;
import fr.hd3d.model.translator.impl.ClassDynMetaDataTranslator;
import fr.hd3d.model.translator.impl.CompositionTranslator;
import fr.hd3d.model.translator.impl.ComputerModelTranslator;
import fr.hd3d.model.translator.impl.ComputerTranslator;
import fr.hd3d.model.translator.impl.ComputerTypeTranslator;
import fr.hd3d.model.translator.impl.ConstituentLinkTranslator;
import fr.hd3d.model.translator.impl.ConstituentTranslator;
import fr.hd3d.model.translator.impl.ContractTranslator;
import fr.hd3d.model.translator.impl.DeviceModelTranslator;
import fr.hd3d.model.translator.impl.DeviceTranslator;
import fr.hd3d.model.translator.impl.DeviceTypeTranslator;
import fr.hd3d.model.translator.impl.DynMetaDataTypeTranslator;
import fr.hd3d.model.translator.impl.DynMetaDataValueTranslator;
import fr.hd3d.model.translator.impl.DynamicPathTranslator;
import fr.hd3d.model.translator.impl.EntityTaskLinkTranslator;
import fr.hd3d.model.translator.impl.EventTranslator;
import fr.hd3d.model.translator.impl.ExtraLineTranslator;
import fr.hd3d.model.translator.impl.FactTranslator;
import fr.hd3d.model.translator.impl.FactTypeTranslator;
import fr.hd3d.model.translator.impl.FileAttributeTranslator;
import fr.hd3d.model.translator.impl.FileRevisionAnnotationTranslator;
import fr.hd3d.model.translator.impl.FileRevisionLinkTranslator;
import fr.hd3d.model.translator.impl.FileRevisionTranslator;
import fr.hd3d.model.translator.impl.GraphicalAnnotationTranslator;
import fr.hd3d.model.translator.impl.Hd3dACLTranslator;
import fr.hd3d.model.translator.impl.HintTranslator;
import fr.hd3d.model.translator.impl.ItemGroupTranslator;
import fr.hd3d.model.translator.impl.ItemTranslator;
import fr.hd3d.model.translator.impl.LicenseTranslator;
import fr.hd3d.model.translator.impl.ListValuesTranslator;
import fr.hd3d.model.translator.impl.ManufacturerTranslator;
import fr.hd3d.model.translator.impl.MileStoneTranslator;
import fr.hd3d.model.translator.impl.MountPointTranslator;
import fr.hd3d.model.translator.impl.OrganizationTranslator;
import fr.hd3d.model.translator.impl.PersonDayTranslator;
import fr.hd3d.model.translator.impl.PersonTranslator;
import fr.hd3d.model.translator.impl.PlanningTranslator;
import fr.hd3d.model.translator.impl.PlayListEditTranslator;
import fr.hd3d.model.translator.impl.PlayListRevisionGroupTranslator;
import fr.hd3d.model.translator.impl.PlayListRevisionTranslator;
import fr.hd3d.model.translator.impl.PoolTranslator;
import fr.hd3d.model.translator.impl.ProcessorTranslator;
import fr.hd3d.model.translator.impl.ProjectSecurityTemplateTranslator;
import fr.hd3d.model.translator.impl.ProjectTranslator;
import fr.hd3d.model.translator.impl.ProjectTypeTranslator;
import fr.hd3d.model.translator.impl.ProxyTranslator;
import fr.hd3d.model.translator.impl.ProxyTypeTranslator;
import fr.hd3d.model.translator.impl.QualificationTranslator;
import fr.hd3d.model.translator.impl.ResourceGroupTranslator;
import fr.hd3d.model.translator.impl.RoleTemplateTranslator;
import fr.hd3d.model.translator.impl.RoleTranslator;
import fr.hd3d.model.translator.impl.RoomTranslator;
import fr.hd3d.model.translator.impl.ScreenModelTranslator;
import fr.hd3d.model.translator.impl.ScreenTranslator;
import fr.hd3d.model.translator.impl.ScriptTranslator;
import fr.hd3d.model.translator.impl.SequenceTranslator;
import fr.hd3d.model.translator.impl.SheetFilterTranslator;
import fr.hd3d.model.translator.impl.SheetTranslator;
import fr.hd3d.model.translator.impl.ShotTranslator;
import fr.hd3d.model.translator.impl.SimpleActivityTranslator;
import fr.hd3d.model.translator.impl.SkillLevelTranslator;
import fr.hd3d.model.translator.impl.SkillTranslator;
import fr.hd3d.model.translator.impl.SoftwareTranslator;
import fr.hd3d.model.translator.impl.StepTranslator;
import fr.hd3d.model.translator.impl.StudioMapTranslator;
import fr.hd3d.model.translator.impl.TagCategoryTranslator;
import fr.hd3d.model.translator.impl.TagTranslator;
import fr.hd3d.model.translator.impl.TaskActivityTranslator;
import fr.hd3d.model.translator.impl.TaskBaseTranslator;
import fr.hd3d.model.translator.impl.TaskChangesTranslator;
import fr.hd3d.model.translator.impl.TaskGroupTranslator;
import fr.hd3d.model.translator.impl.TaskTranslator;
import fr.hd3d.model.translator.impl.TaskTypeTranslator;
import fr.hd3d.model.translator.impl.WorkingTimeTranslator;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.services.security.templates.IProjectSecurityTemplate;
import fr.hd3d.services.security.templates.IRoleTemplate;
import fr.hd3d.utils.Log;


/**
 * hub concentrator for all ITranslator implementors.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class Translators
{
    private Translators()
    {}

    /*
     * Note: even nothing here, this method has to be called to trigger the static fields instantiation this method is
     * called after all static classes instantiation
     */
    public static void init()
    {
        Map<String, IBaseTranslator<?, ?>> tempMap = new HashMap<String, IBaseTranslator<?, ?>>();

        /* get all the static IObjectProvider<T> classes here and link them to the interface name T */
        /* key = T */
        /* value = the bound IObjectProvider<T> */
        Field[] fields = Translators.class.getFields();
        for (Field f : fields)
        {
            Type genericFieldType = f.getGenericType();
            if (genericFieldType instanceof ParameterizedType)
            {
                ParameterizedType aType = (ParameterizedType) genericFieldType;
                Type[] fieldArgTypes = aType.getActualTypeArguments();
                for (Type fieldArgType : fieldArgTypes)
                {
                    Class<?> fieldArgClass = (Class<?>) fieldArgType;
                    List<?> interfaces = ClassUtils.getAllInterfaces(f.getType());
                    if (f.getType() == IBaseTranslator.class || interfaces.contains(IBaseTranslator.class))
                    {
                        Log.LOGGER.debug("fieldArgClass = {}", fieldArgClass.getCanonicalName());
                        try
                        {
                            tempMap.put(fieldArgClass.getCanonicalName(), (IBaseTranslator<?, ?>) f.get(null));
                        }
                        catch (IllegalArgumentException e)
                        {/* nothing to do */
                        }
                        catch (IllegalAccessException e)
                        {/* nothing to do */
                        }
                    }
                }
            }
        }
        EntitiesMaps.setInterfaceToTranslatorMap(Collections.unmodifiableMap(tempMap));
    }

    @SuppressWarnings("unchecked")
    public static <L extends ILBase, P extends IBase> IBaseTranslator<L, P> getTranslator(String entityNameOrInterface)
    {
        /* search by entity name */
        IBaseTranslator<L, P> t = null;

        t = (IBaseTranslator<L, P>) EntitiesMaps.withEntityName(entityNameOrInterface).getTranslator();
        if (t != null)
            return t;

        /* search by interface */
        t = (IBaseTranslator<L, P>) EntitiesMaps.withInterfaceName(entityNameOrInterface).getTranslator();

        return t;
    }

    /**
     * A34
     */
    public static final IBaseTranslator<ILProject, IProject> project = new ProjectTranslator();
    public static final IBaseTranslator<ILPerson, IPerson> person = new PersonTranslator();
    public static final IBaseTranslator<ILTask, ITask> task = new TaskTranslator();
    public static final IBaseTranslator<ILSimpleActivity, ISimpleActivity> simpleActivity = new SimpleActivityTranslator();
    public static final IBaseTranslator<ILTaskActivity, ITaskActivity> taskActivity = new TaskActivityTranslator();
    public static final IBaseTranslator<ILActivity, IActivity> activity = new ActivityTranslator();
    public static final IBaseTranslator<ILPersonDay, IPersonDay> personDay = new PersonDayTranslator();
    public static final IBaseTranslator<ILWorkingTime, IWorkingTime> workingTime = new WorkingTimeTranslator();
    public static final IBaseTranslator<ILTaskGroup, ITaskGroup> taskGroup = new TaskGroupTranslator();
    public static final IBaseTranslator<ILPlanning, IPlanning> planning = new PlanningTranslator();
    public static final IBaseTranslator<ILTaskChanges, ITaskChanges> taskChanges = new TaskChangesTranslator();
    public static final IBaseTranslator<ILTaskBase, ITaskBase> taskBase = new TaskBaseTranslator();
    public static final IBaseTranslator<ILTaskType, ITaskType> taskType = new TaskTypeTranslator();
    public static final IBaseTranslator<ILProjectType, IProjectType> projecttype = new ProjectTypeTranslator();
    public static final IBaseTranslator<ILEvent, IEvent> event = new EventTranslator();
    public static final IBaseTranslator<ILMileStone, IMileStone> mileStone = new MileStoneTranslator();
    public static final IBaseTranslator<ILAbsence, IAbsence> absence = new AbsenceTranslator();
    public static final IBaseTranslator<ILExtraLine, IExtraLine> extraLine = new ExtraLineTranslator();

    /**
     * A12
     */
    public static final IBaseTranslator<ILConstituent, IConstituent> constituent = new ConstituentTranslator();
    public static final IBaseTranslator<ILConstituentLink, IConstituentLink> constituentlink = new ConstituentLinkTranslator();
    public static final IBaseTranslator<ILCategory, ICategory> category = new CategoryTranslator();
    public static final IBaseTranslator<ILSequence, ISequence> sequence = new SequenceTranslator();
    public static final IBaseTranslator<ILShot, IShot> shot = new ShotTranslator();
    public static final IBaseTranslator<ILHint, IHint> hint = new HintTranslator();
    public static final IBaseTranslator<ILFactType, IFactType> facttype = new FactTypeTranslator();
    public static final IBaseTranslator<ILFact, IFact> fact = new FactTranslator();
    public static final IBaseTranslator<ILComposition, IComposition> composition = new CompositionTranslator();
    public static final IBaseTranslator<ILSheet, ISheet> sheet = new SheetTranslator();
    public static final IBaseTranslator<ILItemGroup, IItemGroup> itemGroup = new ItemGroupTranslator();
    public static final IBaseTranslator<ILItem, IItem> item = new ItemTranslator();
    public static final IBaseTranslator<LSheetFilter, SheetFilter> sheetfilter = new SheetFilterTranslator();
    public static final IBaseTranslator<ILTag, ITag> tag = new TagTranslator();
    public static final IBaseTranslator<ILTagCategory, ITagCategory> tagcategory = new TagCategoryTranslator();
    public static final IBaseTranslator<ILClassDynMetaDataType, IClassDynMetaDataType> classdynmetadata = new ClassDynMetaDataTranslator();
    public static final IBaseTranslator<ILDynMetaDataType, IDynMetaDataType> dynmetadatatype = new DynMetaDataTypeTranslator();
    public static final IBaseTranslator<ILDynMetaDataValue, IDynMetaDataValue> dynmetadatavalue = new DynMetaDataValueTranslator();
    public static final IBaseTranslator<ILScript, IScript> script = new ScriptTranslator();

    // Resources
    public static final IBaseTranslator<ILResourceGroup, IResourceGroup> resourcegroup = new ResourceGroupTranslator();
    public static final IBaseTranslator<ILSkill, ISkill> skill = new SkillTranslator();
    public static final IBaseTranslator<ILOrganization, IOrganization> organization = new OrganizationTranslator();
    public static final IBaseTranslator<ILSkillLevel, ISkillLevel> skillLevel = new SkillLevelTranslator();
    public static final IBaseTranslator<ILQualification, IQualification> qualification = new QualificationTranslator();
    public static final IBaseTranslator<ILContract, IContract> contract = new ContractTranslator();

    // F
    public static final IBaseTranslator<ILSoftware, ISoftware> software = new SoftwareTranslator();
    public static final IBaseTranslator<ILLicense, ILicense> license = new LicenseTranslator();
    public static final IBaseTranslator<ILComputer, IComputer> computer = new ComputerTranslator();
    public static final IBaseTranslator<ILComputerType, IComputerType> computerType = new ComputerTypeTranslator();
    public static final IBaseTranslator<ILDevice, IDevice> device = new DeviceTranslator();
    public static final IBaseTranslator<ILDeviceType, IDeviceType> deviceType = new DeviceTypeTranslator();
    public static final IBaseTranslator<ILScreen, IScreen> screen = new ScreenTranslator();
    public static final IBaseTranslator<ILPool, IPool> pool = new PoolTranslator();
    public static final IBaseTranslator<ILStudioMap, IStudioMap> studiomap = new StudioMapTranslator();
    public static final IBaseTranslator<ILManufacturer, IManufacturer> manufacturer = new ManufacturerTranslator();
    public static final IBaseTranslator<ILProcessor, IProcessor> processor = new ProcessorTranslator();
    public static final IBaseTranslator<ILRoom, IRoom> room = new RoomTranslator();
    public static final IBaseTranslator<ILComputerModel, IComputerModel> computerModel = new ComputerModelTranslator();
    public static final IBaseTranslator<ILDeviceModel, IDeviceModel> deviceModel = new DeviceModelTranslator();
    public static final IBaseTranslator<ILScreenModel, IScreenModel> screenModel = new ScreenModelTranslator();

    public static final IBaseTranslator<ILAssetRevision, IAssetRevision> assetrevision = new AssetRevisionTranslator();
    public static final IBaseTranslator<ILAssetRevisionLink, IAssetRevisionLink> assetrevisionlink = new AssetRevisionLinkTranslator();
    public static final IBaseTranslator<ILAssetRevisionGroup, IAssetRevisionGroup> assetrevisiongroup = new AssetRevisionGroupTranslator();
    public static final IBaseTranslator<ILFileRevision, IFileRevision> filerevision = new FileRevisionTranslator();
    // public static final IBaseTranslator<ILAssetRevisionFileRevision, IAssetRevisionFileRevision>
    // assetrevisionfilerevision = new AssetRevisionFileRevisionTranslator();
    public static final IBaseTranslator<ILFileRevisionLink, IFileRevisionLink> filerevisionlink = new FileRevisionLinkTranslator();
    public static final IBaseTranslator<ILAssetAbstract, IAssetAbstract> assetabstract = new AssetAbstractTranslator();
    public static final IBaseTranslator<ILFileAttribute, IFileAttribute> fileattribute = new FileAttributeTranslator();
    public static final IBaseTranslator<ILRole, IRole> role = new RoleTranslator();
    public static final IBaseTranslator<ILApprovalNote, IApprovalNote> approvalnote = new ApprovalNoteTranslator();
    public static final IBaseTranslator<ILApprovalNoteType, IApprovalNoteType> approvalnotetype = new ApprovalNoteTypeTranslator();
    public static final IBaseTranslator<ILEntityTaskLink, IEntityTaskLink> entitytasklink = new EntityTaskLinkTranslator();
    public static final IBaseTranslator<ILStep, IStep> step = new StepTranslator();
    public static final IBaseTranslator<ILPlayListRevision, IPlayListRevision> playlistrevision = new PlayListRevisionTranslator();
    public static final IBaseTranslator<ILPlayListEdit, IPlayListEdit> playlistedit = new PlayListEditTranslator();
    public static final IBaseTranslator<ILFileRevisionAnnotation, IFileRevisionAnnotation> filerevisionannotation = new FileRevisionAnnotationTranslator();
    public static final IBaseTranslator<ILListValues, IListValues> listvalues = new ListValuesTranslator();
    public static final IBaseTranslator<ILGraphicalAnnotation, IGraphicalAnnotation> graphicalannotation = new GraphicalAnnotationTranslator();

    public static final IBaseTranslator<ILRole, IRoleTemplate> roletemplates = new RoleTemplateTranslator();
    public static final IBaseTranslator<ILProjectSecurityTemplate, IProjectSecurityTemplate> securitytemplate = new ProjectSecurityTemplateTranslator();
    public static final IBaseTranslator<ILDynamicPath, IDynamicPath> dynamicpath = new DynamicPathTranslator();
    public static final IBaseTranslator<ILMountPoint, IMountPoint> mountpoint = new MountPointTranslator();
    public static final IBaseTranslator<ILProxyType, IProxyType> proxytype = new ProxyTypeTranslator();
    public static final IBaseTranslator<ILProxy, IProxy> proxy = new ProxyTranslator();
    public static final IBaseTranslator<ILPlayListRevisionGroup, IPlayListRevisionGroup> playlistrevisiongroup = new PlayListRevisionGroupTranslator();
    public static final IBaseTranslator<ILHd3dACL, IHd3dACL> acl = new Hd3dACLTranslator();

}
