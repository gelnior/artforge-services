/**
  * Handles object conversions.
  * Objects need sometimes to be converted into another format.
  * An example is regular object to lightweight object conversion, when client
  * does not need all the data in the server side object.  
  */
package fr.hd3d.model.translator;

