package fr.hd3d.model.translator.utils;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public final class TransitionDifferences
{
    public final Set<Long> toAdd;
    public final Set<Long> toRemove;

    /**
     * @param toAdd
     * @param toRemove
     */
    private TransitionDifferences(final Set<Long> toAdd, final Set<Long> toRemove)
    {
        this.toAdd = toAdd;
        this.toRemove = toRemove;
    }

    public static TransitionDifferences findTransition(final Collection<Long> from, final Collection<Long> to)
    {
        TransitionDifferences ret;

        final Set<Long> toAdd = new LinkedHashSet<Long>();
        final Set<Long> toRemove = new LinkedHashSet<Long>();

        if (from == null || to == null || from.equals(to))
        {
            ret = new TransitionDifferences(toAdd, toRemove);
        }
        else
        {
            // to - from = the ids to add
            toAdd.addAll(to);
            toAdd.removeAll(from);
            // from - to = the ids to remove
            toRemove.addAll(from);
            toRemove.removeAll(to);

            ret = new TransitionDifferences(toAdd, toRemove);
        }

        return ret;
    }
}
