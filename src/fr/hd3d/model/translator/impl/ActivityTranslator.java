package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.SimpleActivityH;
import fr.hd3d.model.persistence.impl.hibernate.TaskActivityH;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.ResourceContext;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ActivityTranslator extends BaseTranslator<ILActivity, IActivity>
{
    public IActivity fromLightweight(ILActivity light, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (light == null)
            return null;

        if (light instanceof ILSimpleActivity)
        {
            context.setPersistedClass(SimpleActivityH.class);
            context.setPersistor(Persistors.simpleActivity);
            return Translators.simpleActivity.fromLightweight((ILSimpleActivity) light,
                    (ResourceContext<ILSimpleActivity, ISimpleActivity>) context);
        }
        else if (light instanceof ILTaskActivity)
        {
            context.setPersistedClass(TaskActivityH.class);
            context.setPersistor(Persistors.taskActivity);
            return Translators.taskActivity.fromLightweight((ILTaskActivity) light,
                    (ResourceContext<ILTaskActivity, ITaskActivity>) context);
        }
        throw new Hd3dTranslationException("object is not an activity of any kind");
    }

    public ILActivity toLightweight(IActivity object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        if (object instanceof ISimpleActivity)
        {
            return Translators.simpleActivity.toLightweight((ISimpleActivity) object, context);
        }
        else if (object instanceof ITaskActivity)
        {
            return Translators.taskActivity.toLightweight((ITaskActivity) object, context);
        }
        throw new Hd3dTranslationException("object is not an activity of any kind");
    }

    public void updateFromLightweight(ILActivity light, IActivity object, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        if (light == null || object == null)
            return;

        if (light instanceof ILSimpleActivity)
        {
            Translators.simpleActivity.updateFromLightweight((ILSimpleActivity) light, (ISimpleActivity) object,
                    context);
        }
        else if (light instanceof ILTaskActivity)
        {
            Translators.taskActivity.updateFromLightweight((ILTaskActivity) light, (ITaskActivity) object, context);
        }
    }
}
