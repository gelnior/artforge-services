package fr.hd3d.model.translator.impl;

import java.text.MessageFormat;
import java.util.Set;

import org.apache.commons.lang.ObjectUtils;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILExtraLine;
import fr.hd3d.model.lightweight.impl.LExtraLine;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.ITaskBase;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ExtraLineH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


public class ExtraLineTranslator extends BaseTranslator<ILExtraLine, IExtraLine>
{
    public IExtraLine fromLightweight(ILExtraLine light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        final IExtraLine extraLine = new ExtraLineH();
        extraLine.setName(trim(light.getName()));
        if (BaseH.isValidId(light.getTaskGroupID()))
        {
            ITaskGroup taskGroup = Persistors.taskGroup.getById(light.getTaskGroupID());
            if (taskGroup == null)
            {
                throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "TaskGroup", light.getTaskGroupID()));
            }
            extraLine.setTaskGroup(taskGroup);
        }
        else
        {
            throw new Hd3dTranslationException("TaskGroupID is null. This field is mandatory.");
        }
        if (BaseH.isValidId(light.getHiddenPersonID()))
        {
            IPerson hiddenPerson = Persistors.person.getById(light.getHiddenPersonID());
            if (hiddenPerson == null)
            {
                throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "HiddenPerson", light
                        .getHiddenPersonID()));

            }
            extraLine.setHiddenPerson(hiddenPerson);
        }
        TranslatorUtil.fromLightWeightCommonFields(extraLine, light, context);
        return extraLine;
    }

    public ILExtraLine toLightweight(IExtraLine object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILExtraLine extraLine = new LExtraLine();
        extraLine.setId(object.getId());
        extraLine.setVersion(object.getVersion());
        extraLine.setName(object.getName());
        if (object.getTaskGroup() != null)
        {
            extraLine.setTaskGroupID(nullSafeId(object.getTaskGroup()));
        }
        else
        {
            throw new Hd3dTranslationException("TaskGroup is null. This field is mandatory.");
        }

        Set<ITaskBase> taskBases = object.getTaskBases();
        CollectUtils.removeNull(taskBases);
        if (taskBases != null && !taskBases.isEmpty())
        {
            for (ITaskBase taskBase : taskBases)
                extraLine.getTaskBaseIDs().add(taskBase.getId());
        }

        extraLine.setHiddenPersonID(nullSafeId(object.getHiddenPerson()));

        TranslatorUtil.toLightWeightCommonFields(object, extraLine, context);
        return extraLine;
    }

    public void updateFromLightweight(final ILExtraLine light, final IExtraLine object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILExtraLine, IExtraLine> updater = new TranslatorUtil<ILExtraLine, IExtraLine>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        final Long hiddenPersonID = light.getHiddenPersonID();
        if (hiddenPersonID != null)
        {
            if (!Const.ID_RESET.equals(hiddenPersonID))
            {
                IPerson hiddenPerson = Persistors.person.getById(hiddenPersonID);
                if (hiddenPerson == null)
                {
                    throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "HiddenPerson", hiddenPersonID));
                }
                object.setHiddenPerson(hiddenPerson);
            }
            else
            {
                object.setHiddenPerson(null);
            }
        }

        final Long taskGroupID = light.getTaskGroupID();
        if (taskGroupID == null)
        {
            throw new Hd3dTranslationException("getTaskGroupID is null. This field is mandatory.");
        }
        else
        {
            if (!ObjectUtils.equals(nullSafeId(object.getTaskGroup()), taskGroupID))
            {
                final ITaskGroup taskGroup = Persistors.taskGroup.getById(taskGroupID);
                if (taskGroup == null)
                {
                    throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "TaskGroup", taskGroupID));
                }
                object.setTaskGroup(taskGroup);
            }

        }

        updater.updateField("taskBases").fromField("taskBaseIDs").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILExtraLine light, IExtraLine object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (org.apache.commons.lang.StringUtils.isEmpty(light.getName()))
            resp.logErr("Name is null. This field is mandatory.");

        return resp;
    }
}
