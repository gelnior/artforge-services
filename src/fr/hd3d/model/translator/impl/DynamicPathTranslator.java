package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILDynamicPath;
import fr.hd3d.model.lightweight.impl.LDynamicPath;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.DynamicPathH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class DynamicPathTranslator extends BaseTranslator<ILDynamicPath, IDynamicPath>
{
    public ILDynamicPath toLightweight(IDynamicPath object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        ILDynamicPath dynamicPath = LDynamicPath.getNewInstance(object.getId(), object.getVersion(), null, object
                .getName(), object.getType(), object.getBoundEntityId(), object.getBoundEntityName(), nullSafeId(object
                .getTaskType()), object.getWipDynamicPath(), object.getPublishDynamicPath(),
                object.getOldDynamicPath(), nullSafeId(object.getProject()), nullSafeId(object.getParent()));

        if (object.getTaskType() != null)
        {
            dynamicPath.setTaskTypeName(object.getTaskType().getName());
            dynamicPath.setTaskTypeColor(object.getTaskType().getColor());
        }
        IBase boundEntity = object.boundEntity();
        if (boundEntity != null)
        {
            if (IProject.class.isInstance(boundEntity))
            {
                final IProject project = (IProject) boundEntity;
                dynamicPath.setWorkObjectName(project.getName());
                dynamicPath.setWorkObjectId(project.getId());
            }
            else if (IConstituent.class.isInstance(boundEntity))
            {
                final IConstituent constituent = (IConstituent) boundEntity;
                dynamicPath.setWorkObjectName(constituent.getLabel());
                dynamicPath.setWorkObjectId(constituent.getId());
            }
            else if (IShot.class.isInstance(boundEntity))
            {
                final IShot shot = (IShot) boundEntity;
                dynamicPath.setWorkObjectName(shot.getLabel());
                dynamicPath.setWorkObjectId(shot.getId());
            }
            else if (ISequence.class.isInstance(boundEntity))
            {
                final ISequence sequence = (ISequence) boundEntity;
                dynamicPath.setWorkObjectName(sequence.getName());
                dynamicPath.setWorkObjectId(sequence.getId());
            }
            else if (ICategory.class.isInstance(boundEntity))
            {
                final ICategory category = (ICategory) boundEntity;
                dynamicPath.setWorkObjectName(category.getName());
                dynamicPath.setWorkObjectId(category.getId());
            }
        }

        TranslatorUtil.toLightWeightCommonFields(object, dynamicPath, context);
        return dynamicPath;
    }

    public IDynamicPath fromLightweight(ILDynamicPath light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ITaskType taskType = null;
        if (BaseH.isValidId(light.getTaskTypeId()))
            taskType = Persistors.tasktype.getById(light.getTaskTypeId());

        IProject project = null;
        if (BaseH.isValidId(light.getProjectId()))
            project = Persistors.project.getById(light.getProjectId());

        IDynamicPath parent = null;
        if (BaseH.isValidId(light.getParent()))
            parent = Persistors.dynamicpath.getById(light.getParent());

        IDynamicPath dynamicPath = new DynamicPathH(trim(light.getName()), trim(light.getType()), light
                .getWorkObjectId(), light.getBoundEntityName(), taskType, trim(light.getWipDynamicPath()), trim(light
                .getPublishDynamicPath()), trim(light.getOldDynamicPath()), project, parent);

        TranslatorUtil.fromLightWeightCommonFields(dynamicPath, light, context);
        return dynamicPath;
    }

    private void updateFields(final TranslatorUtil<ILDynamicPath, IDynamicPath> updater) throws Hd3dException
    {
        updater.updateField("project").fromField("projectId").update();

        updater.updateField("boundEntityId").fromField("workObjectId").update();

        updater.updateField("boundEntityName").update();

        updater.acceptNull().updateField("taskType").fromField("taskTypeId").update();

        updater.updateField("name").update();
        updater.updateField("type").update();

        updater.acceptNull().updateField("wipDynamicPath").update();

        updater.acceptNull().updateField("publishDynamicPath").update();

        updater.acceptNull().updateField("oldDynamicPath").update();

        updater.acceptNull().updateField("parent").update();
    }

    public void updateFromLightweight(final ILDynamicPath light, final IDynamicPath object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILDynamicPath, IDynamicPath> updater = new TranslatorUtil<ILDynamicPath, IDynamicPath>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updateFields(updater);
    }

    @Override
    public ValidationResponse validateLightWeight(ILDynamicPath light, IDynamicPath object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        return resp;
    }

}
