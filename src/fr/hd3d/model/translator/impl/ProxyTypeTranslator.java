package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILProxyType;
import fr.hd3d.model.lightweight.impl.LProxyType;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.impl.hibernate.ProxyTypeH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class ProxyTypeTranslator extends BaseTranslator<ILProxyType, IProxyType>
{
    public ILProxyType toLightweight(IProxyType object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        ILProxyType proxyType = LProxyType.getNewInstance(object.getId(), object.getVersion(), object.getName(), object
                .getOpenType(), object.getDescription());

        TranslatorUtil.toLightWeightCommonFields(object, proxyType, context);
        return proxyType;
    }

    public IProxyType fromLightweight(ILProxyType light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IProxyType proxyType = new ProxyTypeH(light.getName(), light.getOpenType(), light.getDescription());

        TranslatorUtil.fromLightWeightCommonFields(proxyType, light, context);
        return proxyType;
    }

    private void updateFields(final TranslatorUtil<ILProxyType, IProxyType> updater) throws Hd3dException
    {

        updater.updateField("name").update();

        updater.acceptNull().updateField("openType").update();

        updater.acceptNull().updateField("description").update();
    }

    public void updateFromLightweight(final ILProxyType light, final IProxyType object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILProxyType, IProxyType> updater = new TranslatorUtil<ILProxyType, IProxyType>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updateFields(updater);
    }

    @Override
    public ValidationResponse validateLightWeight(ILProxyType light, IProxyType object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        return resp;
    }

}
