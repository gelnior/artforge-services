package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILEntityTaskLink;
import fr.hd3d.model.lightweight.impl.LEntityTaskLink;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.EntityTaskLinkH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class EntityTaskLinkTranslator extends BaseTranslator<ILEntityTaskLink, IEntityTaskLink>
{

    public IEntityTaskLink fromLightweight(ILEntityTaskLink light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IEntityTaskLink entityTaskLink = new EntityTaskLinkH();

        entityTaskLink.setBoundEntity(light.getBoundEntity());
        entityTaskLink.setBoundEntityName(trim(light.getBoundEntityName()));
        if (BaseH.isValidId(light.getTask()))
            entityTaskLink.setTask(Persistors.task.getById(light.getTask()));
        entityTaskLink.setExtra(light.getExtra());

        TranslatorUtil.fromLightWeightCommonFields(entityTaskLink, light, context);
        return entityTaskLink;
    }

    public ILEntityTaskLink toLightweight(IEntityTaskLink object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        ILEntityTaskLink note = LEntityTaskLink.getNewInstance(object.getId(), object.getVersion(), null, object
                .getBoundEntity(), object.getBoundEntityName(), nullSafeId(object.getTask()), object.getExtra());

        TranslatorUtil.toLightWeightCommonFields(object, note, context);
        return note;
    }

    // public List<ILEntityTaskLink> toLightweightCollection(Collection<IEntityTaskLink> objects,
    // ResourceContext<?, ?> context) throws Hd3dException
    // {
    // List<ILEntityTaskLink> ret = new ArrayList<ILEntityTaskLink>();
    // if (CollectUtils.isEmpty(objects))
    // return ret;
    //
    // ILEntityTaskLink link;
    // for (IEntityTaskLink etl : objects)
    // {
    // link = new LEntityTaskLink();
    // link.setId(etl.getId());
    // link.setVersion(etl.getVersion());
    // link.setBoundEntity(etl.getBoundEntity());
    // link.setBoundEntityName(etl.getBoundEntityName());
    // link.setExtra(etl.getExtra());
    // TranslatorUtil.toLightWeightCommonFields(etl, link, context);
    // ret.add(link);
    // }
    //        
    //
    // return ret;
    // }

    public void updateFromLightweight(final ILEntityTaskLink light, final IEntityTaskLink object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILEntityTaskLink, IEntityTaskLink> updater = new TranslatorUtil<ILEntityTaskLink, IEntityTaskLink>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("boundEntity").update();

        updater.updateField("boundEntityName").update();

        updater.updateField("task").update();

        updater.updateField("extra").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILEntityTaskLink light, IEntityTaskLink object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getBoundEntity() == null)
            resp.logErr("Invalid bound entity");

        if (light.getBoundEntityName() == null)
            resp.logErr("Invalid bound entity name");

        if (light.getTask() == null)
            resp.logErr("Invalid task");

        return resp;
    }

}
