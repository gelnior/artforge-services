package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILDevice;
import fr.hd3d.model.lightweight.impl.LDevice;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.IDeviceModel;
import fr.hd3d.model.persistence.IDeviceType;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.DeviceH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted device object to Lightweight device object and vice-versa. It also handles update from
 * lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class DeviceTranslator extends BaseTranslator<ILDevice, IDevice>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IDevice fromLightweight(ILDevice light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        EInventoryStatus inventoryStatus = EInventoryStatus.UNKNOWN;
        if (trim(light.getInventoryStatus()) != null)
        {
            inventoryStatus = EnumUtils.getEnumValueIgnoreCase(EInventoryStatus.class, light.getInventoryStatus());
        }

        IDeviceType deviceType = null;
        if (BaseH.isValidId(light.getDeviceTypeId()))
        {
            deviceType = Persistors.deviceType.getById(light.getDeviceTypeId());
        }

        IDeviceModel deviceModel = null;
        if (BaseH.isValidId(light.getDeviceModelId()))
        {
            deviceModel = Persistors.deviceModel.getById(light.getDeviceModelId());
        }

        ArrayList<IResourceGroup> resourceGroups = new ArrayList<IResourceGroup>();
        final List<Long> resourceGroupIDs = light.getResourceGroupIDs();
        CollectUtils.removeNull(resourceGroupIDs);
        if (BaseH.areValidIds(resourceGroupIDs))
        {
            resourceGroups.ensureCapacity(resourceGroupIDs.size());
            resourceGroups = (ArrayList<IResourceGroup>) Persistors.resourcegroup.getByIds(null, resourceGroupIDs);
        }

        ArrayList<IComputer> computers = new ArrayList<IComputer>();
        final List<Long> computerIDs = light.getComputerIDs();
        CollectUtils.removeNull(computerIDs);
        if (BaseH.areValidIds(computerIDs))
        {
            computers.ensureCapacity(computerIDs.size());
            computers = (ArrayList<IComputer>) Persistors.computer.getByIds(null, computerIDs);
        }

        IDevice device = new DeviceH(trim(light.getName()), trim(light.getSerial()), trim(light.getBillingReference()),
                light.getPurchaseDate(), light.getWarrantyEnd(), inventoryStatus, light.getSize(), deviceType,
                deviceModel, new HashSet<IComputer>(computers), new HashSet<IResourceGroup>(resourceGroups));

        TranslatorUtil.fromLightWeightCommonFields(device, light, context);
        return device;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILDevice toLightweight(IDevice object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        IDeviceModel deviceModel = object.getDeviceModel();
        IDeviceType deviceType = object.getDeviceType();

        String inventoryStatus = EInventoryStatus.UNKNOWN.toString();
        if (object.getInventoryStatus() != null)
        {
            inventoryStatus = object.getInventoryStatus().toString();
        }

        final ILDevice device = LDevice.getNewInstance(object.getId(), object.getVersion(), null, object.getName(),
                object.getSerial(), object.getBillingReference(), object.getPurchaseDate(), object.getWarrantyEnd(),
                inventoryStatus, object.getSize());

        if (deviceModel != null)
        {
            device.setDeviceModelId(deviceModel.getId());
            device.setDeviceModelName(deviceModel.getName());
        }

        if (deviceType != null)
        {
            device.setDeviceTypeId(deviceType.getId());
            device.setDeviceTypeName(deviceType.getName());
        }

        for (IResourceGroup resourceGroup : object.getResourceGroups())
        {
            device.addResourceGroupID(resourceGroup.getId());
            device.addResourceGroupName(resourceGroup.getName());
        }

        for (IComputer computer : object.getComputers())
        {
            device.addComputerID(computer.getId());
            device.addComputerName(computer.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, device, context);
        return device;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILDevice light, final IDevice object, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILDevice, IDevice> updater = new TranslatorUtil<ILDevice, IDevice>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("serial").update();

        updater.updateField("billingReference").update();

        updater.updateField("purchaseDate").update();

        updater.updateField("warrantyEnd").update();

        updater.updateField("inventoryStatus").update();

        updater.updateField("size").update();

        updater.updateField("deviceType").fromField("deviceTypeId").update();

        updater.updateField("deviceModel").fromField("deviceModelId").update();

        updater.updateField("resourceGroups").fromField("resourceGroupIDs").update();

        updater.updateField("computers").fromField("computerIDs").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILDevice light, IDevice object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid device name");

        if (light.getSize() != null && light.getSize() < 0)
            resp.logErr("Invalid size");

        return resp;
    }
}
