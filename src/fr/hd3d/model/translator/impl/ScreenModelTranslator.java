package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILScreenModel;
import fr.hd3d.model.lightweight.impl.LScreenModel;
import fr.hd3d.model.persistence.IManufacturer;
import fr.hd3d.model.persistence.IScreenModel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ScreenModelH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted screen model object to Lightweight screen model object and vice-versa. It also handles update
 * from lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class ScreenModelTranslator extends BaseTranslator<ILScreenModel, IScreenModel>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IScreenModel fromLightweight(ILScreenModel light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IManufacturer manufacturer = null;
        if (BaseH.isValidId(light.getManufacturerId()))
        {
            manufacturer = Persistors.manufacturer.getById(light.getManufacturerId());
        }

        IScreenModel screenModel = new ScreenModelH(trim(light.getName()), manufacturer);

        TranslatorUtil.fromLightWeightCommonFields(screenModel, light, context);
        return screenModel;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILScreenModel toLightweight(IScreenModel object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        IManufacturer manufacturer = object.getManufacturer();

        final ILScreenModel screenModel = LScreenModel.getNewInstance(object.getId(), object.getVersion(), null, object
                .getName());

        if (manufacturer != null)
        {
            screenModel.setManufacturerId(manufacturer.getId());
            screenModel.setManufacturerName(manufacturer.getName());
        }
        TranslatorUtil.toLightWeightCommonFields(object, screenModel, context);
        return screenModel;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILScreenModel light, final IScreenModel object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILScreenModel, IScreenModel> updater = new TranslatorUtil<ILScreenModel, IScreenModel>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("manufacturer").fromField("manufacturerId").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILScreenModel light, IScreenModel object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid screen model name");
        }

        return resp;
    }
}
