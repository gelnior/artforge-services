package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILProxy;
import fr.hd3d.model.lightweight.impl.LProxy;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ProxyH;
import fr.hd3d.services.resolver.MountPointResolver;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class ProxyTranslator extends BaseTranslator<ILProxy, IProxy>
{
    public ILProxy toLightweight(IProxy object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        ILProxy proxy = LProxy.getNewInstance(object.getId(), object.getVersion(),
                nullSafeId(object.getFileRevision()), nullSafeId(object.getProxyType()), object.getLocation(), object
                        .getFilename(), object.getDescription(), object.getFramerate());

        if (object.getProxyType() != null)
        {
            proxy.setProxyTypeName(object.getProxyType().getName());
            proxy.setProxyTypeOpenType(object.getProxyType().getOpenType());
        }

        if (object.getMountPoint() != null)
        {
            proxy.setMountPoint(MountPointResolver.getResolvedMountPoint(context.getResource().getClientInfo()
                    .getAgent(), object.getMountPoint()));
        }

        TranslatorUtil.toLightWeightCommonFields(object, proxy, context);
        return proxy;
    }

    public IProxy fromLightweight(ILProxy light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IFileRevision fileRevision = null;
        if (BaseH.isValidId(light.getFileRevisionId()))
            fileRevision = Persistors.filerevision.getById(light.getFileRevisionId());

        IProxyType proxyType = null;
        if (BaseH.isValidId(light.getProxyTypeId()))
            proxyType = Persistors.proxytype.getById(light.getProxyTypeId());

        IProxy proxy = new ProxyH(fileRevision, proxyType, light.getMountPoint(), light.getLocation(), light
                .getFilename(), light.getDescription(), light.getFramerate());

        TranslatorUtil.fromLightWeightCommonFields(proxy, light, context);
        return proxy;
    }

    private void updateFields(final TranslatorUtil<ILProxy, IProxy> updater) throws Hd3dException
    {
        updater.updateField("fileRevision").fromField("fileRevisionId").update();
        updater.updateField("proxyType").fromField("proxyTypeId").update();
        updater.updateField("location").update();
        updater.updateField("filename").update();
        updater.acceptNull().updateField("description").update();
        updater.dontAcceptNull().updateField("framerate").update();
    }

    public void updateFromLightweight(final ILProxy light, final IProxy object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILProxy, IProxy> updater = new TranslatorUtil<ILProxy, IProxy>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updateFields(updater);
    }

    @Override
    public ValidationResponse validateLightWeight(ILProxy light, IProxy object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        return resp;
    }

}
