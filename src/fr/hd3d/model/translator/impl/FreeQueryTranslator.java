package fr.hd3d.model.translator.impl;

import java.util.Collection;
import java.util.List;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILFreeQuery;
import fr.hd3d.model.persistence.IFreeQuery;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.services.resources.ResourceContext;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class FreeQueryTranslator implements IBaseTranslator<ILFreeQuery, IFreeQuery>
{
    public IFreeQuery fromLightweight(ILFreeQuery object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        return null;
    }

    public ILFreeQuery toLightweight(IFreeQuery object, ResourceContext<?, ?> context)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void updateFromLightweight(ILFreeQuery object, IFreeQuery object2, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
    // TODO Auto-generated method stub

    }

    public List<IFreeQuery> fromLightweightCollection(Collection<ILFreeQuery> lights, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<ILFreeQuery> toLightweightCollection(Collection<IFreeQuery> objects, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void updateFromLightweightCollection(List<ILFreeQuery> lights, List<IFreeQuery> objects,
            ResourceContext<?, ?> context) throws Hd3dException
    {
    // TODO Auto-generated method stub

    }

}
