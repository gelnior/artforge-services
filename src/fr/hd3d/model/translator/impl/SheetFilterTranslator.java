package fr.hd3d.model.translator.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.impl.LSheetFilter;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.preferences.SheetFilter;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;


/**
 * Transform Object in their Lightweight form and vice-versa. Also handle database update from Lightweight form.
 * 
 * @author Try LAM
 */
public class SheetFilterTranslator implements IBaseTranslator<LSheetFilter, SheetFilter>
{
    public static String ERRMSG_0 = "{0} with id={1} does not exist";

    public static String trim(String s)
    {
        return StringUtils.trim(s);
    }

    public SheetFilter fromLightweight(LSheetFilter light, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (light == null)
            return null;

        final ISheet sheet = Persistors.sheet.getById(light.getSheetId());
        SheetFilter sheetFilterList = new SheetFilter(BaseTranslator.nullSafeId(sheet), light.getFilter(), light
                .getName());

        return sheetFilterList;
    }

    public LSheetFilter toLightweight(SheetFilter object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        LSheetFilter sheetFilterList = new LSheetFilter(object.getId(), object.getVersion(), object.getSheet(), object
                .getFilter(), object.getName());
        return sheetFilterList;
    }

    public void updateFromLightweight(final LSheetFilter light, final SheetFilter object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        if (light.getSheetId() != null)
        {
            final ISheet sheet = Persistors.sheet.getById(light.getSheetId());
            if (sheet == null)
            {
                throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "Sheet", light.getSheetId()));
            }
            object.setSheet(sheet.getId());
        }

        final String filters = trim(light.getFilter());
        if (filters != null)
            if (!ObjectUtils.equals(object.getFilter(), filters))
            {
                object.setFilter(filters);
            }

        final String name = trim(light.getName());
        if (name != null)
            if (!ObjectUtils.equals(object.getName(), name))
            {
                object.setName(name);
            }
    }

    public List<SheetFilter> fromLightweightCollection(Collection<LSheetFilter> lights, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        List<SheetFilter> ret = new ArrayList<SheetFilter>();

        CollectUtils.removeNull(lights);

        if (CollectUtils.isNotEmpty(lights))
        {
            for (LSheetFilter light : lights)
            {
                ret.add(fromLightweight(light, context));
            }
        }

        return ret;
    }

    public List<LSheetFilter> toLightweightCollection(Collection<SheetFilter> objects, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        List<LSheetFilter> ret = new ArrayList<LSheetFilter>();

        CollectUtils.removeNull(objects);

        if (CollectUtils.isNotEmpty(objects))
        {
            for (SheetFilter object : objects)
            {
                ret.add(toLightweight(object, context));
            }
        }

        return ret;
    }

    public void updateFromLightweightCollection(List<LSheetFilter> lights, List<SheetFilter> objects,
            ResourceContext<?, ?> context) throws Hd3dException
    {
        CollectUtils.removeNull(lights);
        CollectUtils.removeNull(objects);

        if (CollectUtils.isNotEmpty(lights) && CollectUtils.isNotEmpty(objects))
        {
            if (lights.size() != objects.size())
            {
                throw new Hd3dException(
                        "updating entities: lights objects collection does not have the same size than entities collection.");
            }

            for (int i = 0; i < objects.size(); i++)
            {
                updateFromLightweight(lights.get(i), objects.get(i), context);
            }
        }
    }
}
