package fr.hd3d.model.translator.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILSheet;
import fr.hd3d.model.lightweight.impl.LSheet;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.SheetH;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class SheetTranslator extends BaseTranslator<ILSheet, ISheet>
{
    public ISheet fromLightweight(ILSheet light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        // retrieve Project when it is set
        IProject project = null;
        if (BaseH.isValidId(light.getProject()))
        {
            project = Persistors.project.getById(light.getProject());
            Log.LOGGER.info("Project found with id=" + light.getProject());
        }
        else
            Log.LOGGER.warn("Invalid Project id=" + light.getProject());
        // retrieve ItemGroups
        List<IItemGroup> itemGroups = null;
        if (light.getItemGroups() != null)
        {
            final List<Long> itemGroupIds = light.getItemGroups();

            CollectUtils.removeNull(itemGroupIds);

            itemGroups = new ArrayList<IItemGroup>(itemGroupIds.size());
            // Note: Order does matter here.
            // Do not use PersistedObjectProviders.itemgroup.getObjectsWithIds here.
            // It will not return the list in the expected order
            for (Long id : itemGroupIds)
                itemGroups.add(Persistors.itemgroup.getById(id));
        }

        ISheet sheet = new SheetH(trim(light.getName()), trim(light.getDescription()), project, trim(light
                .getBoundClassName()), itemGroups);

        sheet.setType(EnumUtils.getEnumValueIgnoreCase(ESheetType.class, trim(light.getType())));

        TranslatorUtil.fromLightWeightCommonFields(sheet, light, context);

        Log.LOGGER.info("New Sheet to Create=" + sheet.toString());

        return sheet;
    }

    public ILSheet toLightweight(ISheet object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        List<Long> itemGroupIds = null;

        final List<IItemGroup> itemGroups = object.getItemGroups();
        if (itemGroups != null && !itemGroups.isEmpty())
        {
            itemGroupIds = new ArrayList<Long>(itemGroups.size());

            for (IItemGroup itemGroupId : itemGroups)
                if (itemGroupId != null)
                    CollectionUtils.addIgnoreNull(itemGroupIds, itemGroupId.getId());
        }

        ILSheet sheet = LSheet.getNewInstance(object.getId(), object.getVersion(), null, object.getName(), object
                .getDescription(), nullSafeId(object.getProject()), object.getBoundClassName(), itemGroupIds);

        if (object.getType() != null)
            sheet.setType(object.getType().toString());

        TranslatorUtil.toLightWeightCommonFields(object, sheet, context);

        return sheet;
    }

    public void updateFromLightweight(final ILSheet light, final ISheet object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILSheet, ISheet> updater = new TranslatorUtil<ILSheet, ISheet>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("description").update();

        updater.updateField("project").update();

        updater.updateField("boundClassName").update();

        List<Long> newItemGroupIds = light.getItemGroups();
        CollectUtils.removeNull(newItemGroupIds);
        if (newItemGroupIds != null)
        {
            List<IItemGroup> itemGroups = object.getItemGroups();
            List<Long> itemGroupIds = CollectUtils.getIds(itemGroups);
            if (!itemGroupIds.equals(newItemGroupIds))
            {
                itemGroups.clear();

                // Note: Order does matter here.
                // Do not use PersistedObjectProviders.itemgroup.getObjectsWithIds here.
                // It will not return the list in the expected order
                for (Long id : newItemGroupIds)
                    object.addItemGroup(Persistors.itemgroup.getById(id));
            }
        }

        updater.updateField("type").update();
    }

    // TODO make it a base abstract method
    public String validateLightweightObject(ILSheet light)
    {
        StringBuilder buf = new StringBuilder();
        if (EntitiesMaps.isNotEntity(light.getBoundClassName()))
        {
            buf.append(MessageFormat.format(ERRMSG_1, light.getBoundClassName()));
        }
        buf.trimToSize();
        return buf.toString();
    }

    @Override
    public ValidationResponse validateLightWeight(ILSheet light, ISheet object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        final String boundClassName = light.getBoundClassName();
        if (boundClassName != null)
        {
            if (org.apache.commons.lang.StringUtils.isBlank(boundClassName))
                resp.logErr(MessageFormat.format(ERRMSG_1, boundClassName));

            if (EntitiesMaps.isNotEntity(boundClassName))
                resp.logErr(MessageFormat.format(ERRMSG_1, boundClassName));
        }

        if (light.getName() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid sheet name");

        return resp;
    }
}
