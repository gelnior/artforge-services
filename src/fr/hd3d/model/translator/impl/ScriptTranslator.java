package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILScript;
import fr.hd3d.model.lightweight.impl.LScript;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.impl.hibernate.ScriptH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Try LAM
 */
public class ScriptTranslator extends BaseTranslator<ILScript, IScript>
{
    public IScript fromLightweight(ILScript light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IScript script = new ScriptH();
        script.setTrigger(trim(light.getTrigger()));
        try
        {
            script.setName(trim(light.getName()));
        }
        catch (Hd3dException e)
        {
            throw new Hd3dTranslationException(e);
        }
        script.setDescription(trim(light.getDescription()));
        script.setLocation(trim(light.getLocation()));
        script.setExitCode(light.getExitCode());

        TranslatorUtil.fromLightWeightCommonFields(script, light, context);
        return script;
    }

    public ILScript toLightweight(IScript object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILScript script = LScript.getNewInstance(object.getId(), object.getVersion(), null, object.getTrigger(), object
                .getName(), object.getDescription(), object.getLocation(), object.getExitCode());
        script.setLastRun(object.getLastRun());
        script.setLastExitCode(object.getLastExitCode());
        script.setLastRunMessage(object.getLastRunMessage());
        TranslatorUtil.toLightWeightCommonFields(object, script, context);
        return script;
    }

    public void updateFromLightweight(final ILScript light, final IScript object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILScript, IScript> updater = new TranslatorUtil<ILScript, IScript>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("trigger").update();

        updater.updateField("name").update();

        updater.updateField("description").update();

        updater.updateField("location").update();

        updater.updateField("exitCode").update();

        // Note: lastRun, lastExitcode and lastRunMessage are to be overriden by external scripts
        updater.updateField("lastRun").update();

        updater.updateField("lastExitCode").update();

        updater.updateField("lastRunMessage").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILScript light, IScript object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getTrigger() != null)
            if (StringUtils.isBlank(light.getTrigger()))
                resp.logErr("Invalid trigger");

        if (light.getLocation() != null)
            if (StringUtils.isBlank(light.getLocation()))
                resp.logErr("Invalid location");

        return resp;
    }
}
