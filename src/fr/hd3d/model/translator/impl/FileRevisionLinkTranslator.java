package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILFileRevisionLink;
import fr.hd3d.model.lightweight.impl.LFileRevisionLink;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionLinkH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class FileRevisionLinkTranslator extends BaseTranslator<ILFileRevisionLink, IFileRevisionLink>
{

    public IFileRevisionLink fromLightweight(ILFileRevisionLink light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        IFileRevisionLink filelink = new FileRevisionLinkH();

        if (BaseH.isValidId(light.getInputFile()))
            filelink.setInputFileRevision(Persistors.filerevision.getById(light.getInputFile()));
        if (BaseH.isValidId(light.getOutputFile()))
            filelink.setOutputFileRevision(Persistors.filerevision.getById(light.getOutputFile()));
        filelink.setType(trim(light.getType()));

        TranslatorUtil.fromLightWeightCommonFields(filelink, light, context);
        return filelink;
    }

    public ILFileRevisionLink toLightweight(IFileRevisionLink object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILFileRevisionLink filelink = LFileRevisionLink
                .getNewInstance(object.getId(), object.getVersion(), null, nullSafeId(object.getInputFileRevision()),
                        nullSafeId(object.getOutputFileRevision()), object.getType());

        TranslatorUtil.toLightWeightCommonFields(object, filelink, context);
        return filelink;
    }

    public void updateFromLightweight(final ILFileRevisionLink light, final IFileRevisionLink object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILFileRevisionLink, IFileRevisionLink> updater = new TranslatorUtil<ILFileRevisionLink, IFileRevisionLink>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("inputFileRevision").fromField("inputFile").update();

        updater.updateField("outputFileRevision").fromField("outputFile").update();

        updater.updateField("type").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILFileRevisionLink light, IFileRevisionLink object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getInputFile() != null)
            if (BaseH.isNotValidId(light.getInputFile()))
                resp.logErr("Invalid input file id");

        if (light.getOutputFile() != null)
            if (BaseH.isNotValidId(light.getOutputFile()))
                resp.logErr("Invalid output file id");

        return resp;
    }
}
