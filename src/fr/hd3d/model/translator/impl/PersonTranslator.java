package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.impl.LPerson;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.ISkillLevel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.PersonH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.utils.UserAccountsUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class PersonTranslator extends BaseTranslator<ILPerson, IPerson>
{
    public IPerson fromLightweight(ILPerson light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ArrayList<IResourceGroup> resourceGroups = new ArrayList<IResourceGroup>();
        final List<Long> resourceGroupIDs = light.getResourceGroupIDs();
        CollectUtils.removeNull(resourceGroupIDs);
        if (BaseH.areValidIds(resourceGroupIDs))
        {
            resourceGroups.ensureCapacity(resourceGroupIDs.size());
            resourceGroups = (ArrayList<IResourceGroup>) Persistors.resourcegroup.getByIds(null, resourceGroupIDs);
        }
        else
            resourceGroups = new ArrayList<IResourceGroup>();

        ArrayList<IContract> contracts = new ArrayList<IContract>();
        final List<Long> contractIDs = light.getContractIDs();
        CollectUtils.removeNull(contractIDs);
        if (BaseH.areValidIds(contractIDs))
        {
            contracts.ensureCapacity(contractIDs.size());
            contracts = (ArrayList<IContract>) Persistors.contract.getByIds(null, contractIDs);
        }
        else
            contracts = new ArrayList<IContract>();

        IPerson person = new PersonH(light.getLogin(), light.getFirstName(), light.getLastName(), light.getEmail(),
                light.getPhone(), light.getPhotoPath(), new HashSet<IResourceGroup>(resourceGroups),
                new HashSet<IContract>(contracts));

        final List<Long> skillLevelIds = light.getSkillLevelIDs();
        CollectUtils.removeNull(skillLevelIds);
        if (BaseH.areValidIds(skillLevelIds))
        {
            final List<ISkillLevel> skillLevels = Persistors.skillLevel.getByIds(null, skillLevelIds);
            person.setSkillLevels(new HashSet<ISkillLevel>(skillLevels));
        }
        else
            person.setSkillLevels(new HashSet<ISkillLevel>());

        TranslatorUtil.fromLightWeightCommonFields(person, light, context);
        return person;
    }

    public ILPerson toLightweight(IPerson object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILPerson person = LPerson.getNewInstance(object.getId(), object.getVersion(), null, object.getLogin(),
                object.getFirstName(), object.getLastName(), object.getEmail(), object.getPhone(), object
                        .getPhotoPath(), UserAccountsUtil.getUserAccountStatus(object.getLogin()));

        for (IResourceGroup resourceGroup : object.getResourceGroups())
        {
            person.addResourceGroupID(resourceGroup.getId());
            person.addResourceGroupName(resourceGroup.getName());
        }

        for (IContract contract : object.getContracts())
        {
            person.addContractID(contract.getId());
            person.addContractName(contract.getName());
        }

        for (ISkillLevel skillLevel : object.getSkillLevels())
        {
            person.addSkillLevelID(skillLevel.getId());
            if (skillLevel.getSkill() != null)
            {
                person.addSkillLevelName(skillLevel.getSkill().getName());
            }
        }

        for (IProject project : object.getProjects())
        {
            person.addProjectID(project.getId());
            person.addProjectName(project.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, person, context);
        return person;
    }

    public void updateFromLightweight(final ILPerson light, final IPerson object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILPerson, IPerson> updater = new TranslatorUtil<ILPerson, IPerson>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("firstName").update();

        updater.updateField("lastName").update();

        updater.updateField("login").update();

        updater.updateField("email").update();

        updater.updateField("phone").update();

        updater.updateField("photoPath").update();

        updater.updateField("resourceGroups").fromField("resourceGroupIDs").update();

        updater.updateField("skillLevels").fromField("skillLevelIDs").update();

        updater.updateField("contracts").fromField("contractIDs").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILPerson light, IPerson object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getLogin() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getLogin()))
                resp.logErr("Invalid login");

        if (light.getFirstName() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getFirstName()))
                resp.logErr("Invalid firstname");

        if (light.getLastName() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getLastName()))
                resp.logErr("Invalid lastname");

        return resp;
    }
}
