package fr.hd3d.model.translator.impl;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILComposition;
import fr.hd3d.model.lightweight.impl.LComposition;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.CompositionH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class CompositionTranslator extends BaseTranslator<ILComposition, IComposition>
{

    public IComposition fromLightweight(ILComposition light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IConstituent constituent = null;
        if (light.getConstituent() != null)
        {
            constituent = Persistors.constituent.getById(light.getConstituent());
        }

        IShot shot = null;
        if (BaseH.isValidId(light.getShot()))
        {
            shot = Persistors.shot.getById(light.getShot());
        }

        ISequence sequence = null;
        if (BaseH.isValidId(light.getSequence()))
        {
            sequence = Persistors.sequence.getById(light.getSequence());
        }

        IComposition composition = new CompositionH(trim(light.getName()), trim(light.getDescription()), constituent,
                shot, sequence, light.getNbOccurence());

        TranslatorUtil.fromLightWeightCommonFields(composition, light, context);
        return composition;
    }

    public ILComposition toLightweight(IComposition object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        Long constituentId = Const.ID_RESET;// no constituent
        String constituentName = "";
        Long categoryId = Const.ID_RESET;
        if (object.getConstituent() != null)
        {
            constituentId = object.getConstituent().getId();
            constituentName = object.getConstituent().getLabel();
            categoryId = object.getConstituent().getCategory().getId();
        }

        Long shot = Const.ID_RESET;// no shot
        if (object.getShot() != null)
        {
            shot = object.getShot().getId();
        }

        Long sequence = Const.ID_RESET;// no shot
        if (object.getSequence() != null)
        {
            sequence = object.getSequence().getId();
        }

        ILComposition composition = LComposition.getNewInstance(object.getId(), object.getVersion(), null, object
                .getName(), object.getDescription(), constituentId, constituentName, shot, sequence, object
                .getNbOccurence(), categoryId);
        TranslatorUtil.toLightWeightCommonFields(object, composition, context);
        return composition;
    }

    public void updateFromLightweight(final ILComposition light, final IComposition object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILComposition, IComposition> updater = new TranslatorUtil<ILComposition, IComposition>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("description").update();

        updater.updateField("constituent").update();

        updater.updateField("shot").update();

        updater.updateField("sequence").update();

        updater.updateField("nbOccurence").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILComposition light, IComposition object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getConstituent() == null)
            resp.logErr("Constituent must not be null");

        return resp;
    }
}
