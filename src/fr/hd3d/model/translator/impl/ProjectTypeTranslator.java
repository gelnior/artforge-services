/**
 * 
 */
package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILProjectType;
import fr.hd3d.model.lightweight.impl.LProjectType;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.impl.hibernate.ProjectTypeH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Try LAM
 * 
 */
public class ProjectTypeTranslator extends BaseTranslator<ILProjectType, IProjectType>
{

    public IProjectType fromLightweight(ILProjectType light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        final IProjectType projectType = new ProjectTypeH(trim(light.getColor()), trim(light.getName()), trim(light
                .getDescription()));

        TranslatorUtil.fromLightWeightCommonFields(projectType, light, context);
        return projectType;
    }

    public ILProjectType toLightweight(IProjectType object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILProjectType projectType = new LProjectType();
        projectType.setId(object.getId());
        projectType.setVersion(object.getVersion());
        projectType.setName(object.getName());
        projectType.setColor(object.getColor());
        projectType.setDescription(object.getDescription());
        TranslatorUtil.toLightWeightCommonFields(object, projectType, context);
        return projectType;
    }

    public void updateFromLightweight(final ILProjectType light, final IProjectType object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILProjectType, IProjectType> updater = new TranslatorUtil<ILProjectType, IProjectType>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("color").update();

        updater.updateField("description").update();

        updater.updateField("name").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILProjectType light, IProjectType object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() == null)
            resp.logErr("Invalid name");

        return resp;
    }
}
