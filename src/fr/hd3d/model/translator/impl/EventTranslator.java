/**
 * 
 */
package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILEvent;
import fr.hd3d.model.lightweight.impl.LEvent;
import fr.hd3d.model.persistence.IEvent;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.EventH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author thomas-eskenazi
 * 
 */
public class EventTranslator extends BaseTranslator<ILEvent, IEvent>
{

    public IEvent fromLightweight(ILEvent light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);
        final IEvent event = new EventH();

        event.setTitle(trim(light.getTitle()));
        event.setDescription(trim(light.getDescription()));
        event.setStartDate(light.getStartDate());
        event.setEndDate(light.getEndDate());
        if (light.getPlanning() != null)
        {
            IPlanning planning = null;
            if (BaseH.isValidId(light.getPlanningID()))
                planning = Persistors.planning.getById(light.getPlanningID());
            if (planning != null)
            {
                event.setPlanning(planning);
            }
        }
        TranslatorUtil.fromLightWeightCommonFields(event, light, context);
        return event;
    }

    public ILEvent toLightweight(IEvent object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILEvent event = new LEvent();
        event.setId(object.getId());
        event.setStartDate(object.getStartDate());
        event.setEndDate(object.getEndDate());
        event.setTitle(object.getTitle());
        event.setDescription(object.getDescription());
        event.setPlanningID(nullSafeId(object.getPlanning()));

        TranslatorUtil.toLightWeightCommonFields(object, event, context);
        return event;
    }

    public void updateFromLightweight(final ILEvent light, final IEvent object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILEvent, IEvent> updater = new TranslatorUtil<ILEvent, IEvent>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("description").update();

        updater.updateField("startDate").update();

        updater.updateField("endDate").update();

        updater.updateField("title").update();

        updater.updateField("planning").fromField("planningID").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILEvent light, IEvent object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getTitle() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getTitle()))
                resp.logErr("Invalid title");

        if (light.getStartDate() == null)
            resp.logErr("Invalid start date");

        if (light.getEndDate() == null)
            resp.logErr("Invalid end date");

        return resp;
    }
}
