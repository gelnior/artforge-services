package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILFactType;
import fr.hd3d.model.lightweight.impl.LFactType;
import fr.hd3d.model.persistence.IFactType;
import fr.hd3d.model.persistence.impl.hibernate.FactTypeH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class FactTypeTranslator extends BaseTranslator<ILFactType, IFactType>
{
    public IFactType fromLightweight(ILFactType light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IFactType facttype = new FactTypeH(trim(light.getName()), trim(light.getDescription()));

        TranslatorUtil.fromLightWeightCommonFields(facttype, light, context);
        return facttype;
    }

    public ILFactType toLightweight(IFactType object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILFactType facttype = LFactType.getNewInstance(object.getId(), object.getVersion(), null, object.getName(),
                object.getDescription());

        TranslatorUtil.toLightWeightCommonFields(object, facttype, context);
        return facttype;
    }

    public void updateFromLightweight(final ILFactType light, final IFactType object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILFactType, IFactType> updater = new TranslatorUtil<ILFactType, IFactType>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("description").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILFactType light, IFactType object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid facttype name");

        return resp;
    }
}
