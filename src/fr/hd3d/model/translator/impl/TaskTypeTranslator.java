/**
 * 
 */
package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILTaskType;
import fr.hd3d.model.lightweight.impl.LTaskType;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.impl.hibernate.TaskTypeH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates task type object into its lightweight representation and vice-versa. Moreover it can updates task type
 * object from lightweight object.
 * 
 * @author HD3D
 */
public class TaskTypeTranslator extends BaseTranslator<ILTaskType, ITaskType>
{

    public ILTaskType toLightweight(ITaskType object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILTaskType taskType = new LTaskType();
        taskType.setId(object.getId());
        taskType.setVersion(object.getVersion());
        taskType.setName(object.getName());
        taskType.setEntityName(object.getEntityName());
        taskType.setColor(object.getColor());
        taskType.setDescription(object.getDescription());
        taskType.setProjectID(nullSafeId(object.getProject()));

        TranslatorUtil.toLightWeightCommonFields(object, taskType, context);

        return taskType;
    }

    public ITaskType fromLightweight(ILTaskType light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        final ITaskType taskType = new TaskTypeH();

        final TranslatorUtil<ILTaskType, ITaskType> updater = new TranslatorUtil<ILTaskType, ITaskType>(this)
                .withObject(taskType).withLightObject(light);

        updateFields(updater);

        TranslatorUtil.fromLightWeightCommonFields(taskType, light, context);

        return taskType;
    }

    private void updateFields(final TranslatorUtil<ILTaskType, ITaskType> updater) throws Hd3dException
    {
        updater.updateField("color").update();

        updater.updateField("description").update();

        updater.updateField("name").update();

        updater.updateField("entityName").update();

        updater.updateField("project").fromField("projectID").update();

    }

    public void updateFromLightweight(final ILTaskType light, final ITaskType object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILTaskType, ITaskType> updater = new TranslatorUtil<ILTaskType, ITaskType>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updateFields(updater);
    }

    @Override
    public ValidationResponse validateLightWeight(ILTaskType light, ITaskType object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() == null)
            resp.logErr("Invalid name");

        return resp;
    }
}
