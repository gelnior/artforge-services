package fr.hd3d.model.translator.impl;

import java.text.MessageFormat;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.lightweight.impl.LTaskActivity;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.TaskActivityH;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class TaskActivityTranslator extends BaseTranslator<ILTaskActivity, ITaskActivity>
{
    public ITaskActivity fromLightweight(ILTaskActivity light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ITask task = null;
        if (BaseH.isValidId(light.getTaskID()))
            task = Persistors.task.getById(light.getTaskID());

        IPerson filledBy = null;
        if (BaseH.isValidId(light.getFilledByID()))
            filledBy = Persistors.person.getById(light.getFilledByID());

        IPersonDay day = null;
        if (BaseH.isValidId(light.getDayID()))
            day = Persistors.personDay.getById(light.getDayID());

        if (filledBy == null)
            throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "User Filled By", light.getFilledByID()));
        if (task == null)
            throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "Task", light.getTaskID()));
        if (day == null)
            throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "Person Day", light.getDayID()));

        final ITaskActivity activity = new TaskActivityH(task, day, light.getDuration(), light.getComment(), filledBy,
                light.getFilledDate());

        TranslatorUtil.fromLightWeightCommonFields(activity, light, context);
        return activity;
    }

    public ILTaskActivity toLightweight(ITaskActivity object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        final IProject project = object.getProject();
        final IPerson worker = object.getWorker();
        final ITask task = object.getTask();
        final ILTask lTask = Translators.task.toLightweight(task, context);
        final IPerson filledBy = object.getFilledBy();
        final IPersonDay day = object.getDay();

        final LTaskActivity lActivity = LTaskActivity.getNewInstance(object.getId(), object.getVersion(), null,
                nullSafeId(object.getDay()), (day == null) ? null : day.getDate(), object.getDuration(), object
                        .getComment(), lTask, nullSafeId(worker), nullSafeId(project), nullSafeId(filledBy), object
                        .getFilledDate());
        if (object.getDay() != null)
            lActivity.setDayDate(object.getDay().getDate());
        if (project != null)
        {
            lActivity.setProjectColor(project.getColor());
            lActivity.setProjectName(project.getName());
            IProjectType projectType = project.getProjectType();
            if (projectType != null)
            {
                lActivity.setProjectTypeName(projectType.getName());
            }
        }
        if (worker != null)
        {
            lActivity.setWorkerName(worker.fullName());
            lActivity.setWorkerLogin(worker.getLogin());
        }
        if (filledBy != null)
        {
            lActivity.setFilledByLogin(filledBy.getLogin());
            lActivity.setFilledByName(filledBy.fullName());
        }

        if (task != null)
        {
            IBase boundEntity = task.boundEntity();
            if (boundEntity != null)
            {
                if (IConstituent.class.isInstance(boundEntity))
                {
                    final IConstituent constituent = (IConstituent) boundEntity;
                    lActivity.setWorkObjectId(constituent.getId());
                    lActivity.setWorkObjectName(constituent.getLabel());
                    lActivity.setWorkObjectParentsNames(constituent.getPath());
                    lActivity.setWorkObjectEntity(Const.CONSTITUENT);

                }
                else if (IShot.class.isInstance(boundEntity))
                {
                    final IShot shot = (IShot) boundEntity;
                    lActivity.setWorkObjectId(shot.getId());
                    lActivity.setWorkObjectName(shot.getLabel());
                    lActivity.setWorkObjectParentsNames(shot.getPath());
                    lActivity.setWorkObjectEntity(Const.SHOT);
                }
            }
            ETaskStatus status = task.getStatus();
            if (status != null)
            {
                lActivity.setTaskStatus(status.toString());
            }
        }

        TranslatorUtil.toLightWeightCommonFields(object, lActivity, context);
        return lActivity;
    }

    public void updateFromLightweight(final ILTaskActivity light, final ITaskActivity object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILTaskActivity, ITaskActivity> updater = new TranslatorUtil<ILTaskActivity, ITaskActivity>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("duration").update();

        updater.updateField("comment").update();

        updater.updateField("day").fromField("dayID").update();

        updater.updateField("filledBy").fromField("filledByID").update();

        updater.updateField("filledDate").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILTaskActivity light, ITaskActivity object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);
        if (light.getProjectID() == null || light.getFilledByID() == null || light.getTaskID() == null)
            resp.logErr("Object should have taskId, filledById and projectId");

        return resp;
    }
}
