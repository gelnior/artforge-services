package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILRoom;
import fr.hd3d.model.lightweight.impl.LRoom;
import fr.hd3d.model.persistence.IRoom;
import fr.hd3d.model.persistence.impl.hibernate.RoomH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted device object to Lightweight device object and vice-versa. It also handles update from
 * lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class RoomTranslator extends BaseTranslator<ILRoom, IRoom>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IRoom fromLightweight(ILRoom light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IRoom room = new RoomH(trim(light.getName()));

        TranslatorUtil.fromLightWeightCommonFields(room, light, context);
        return room;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILRoom toLightweight(IRoom object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILRoom room = LRoom.getNewInstance(object.getId(), object.getVersion(), null, object.getName());

        TranslatorUtil.toLightWeightCommonFields(object, room, context);
        return room;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILRoom light, final IRoom object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILRoom, IRoom> updater = new TranslatorUtil<ILRoom, IRoom>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILRoom light, IRoom object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid room name");
        }

        return resp;
    }
}
