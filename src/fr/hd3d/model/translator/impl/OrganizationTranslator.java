package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILOrganization;
import fr.hd3d.model.lightweight.impl.LOrganization;
import fr.hd3d.model.persistence.IOrganization;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.OrganizationH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted organization object to lightweight organization object and vice-versa. It also handles update
 * from lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class OrganizationTranslator extends BaseTranslator<ILOrganization, IOrganization>
{
    public IOrganization fromLightweight(ILOrganization light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ArrayList<IResourceGroup> resourceGroups = new ArrayList<IResourceGroup>();
        final List<Long> resourceGroupIds = light.getResourceGroupIds();
        CollectUtils.removeNull(resourceGroupIds);
        if (BaseH.areValidIds(resourceGroupIds))
        {
            resourceGroups.ensureCapacity(resourceGroupIds.size());
            resourceGroups = (ArrayList<IResourceGroup>) Persistors.resourcegroup.getByIds(null, resourceGroupIds);
        }
        else
            resourceGroups = new ArrayList<IResourceGroup>();

        IOrganization organization = new OrganizationH();
        organization.setName(trim(light.getName()));
        organization.setResourceGroups(new HashSet<IResourceGroup>(resourceGroups));

        Long parentId = resolveParentId(light);

        IOrganization organizationParent = null;
        if (BaseH.isValidId(parentId))
        {
            organizationParent = Persistors.organization.getById(parentId);
        }
        organization.setParent(organizationParent);

        TranslatorUtil.fromLightWeightCommonFields(organization, light, context);
        return organization;
    }

    public ILOrganization toLightweight(IOrganization object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILOrganization organization = LOrganization.getNewInstance(object.getId(), object.getVersion(), object
                .getName(), resolveParentId(object));

        for (IResourceGroup resourceGroup : object.getResourceGroups())
        {
            organization.addResourceGroupID(resourceGroup.getId());
            organization.addResourceGroupName(resourceGroup.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, organization, context);
        return organization;
    }

    private Long resolveParentId(ILOrganization lOrganization)
    {
        if (lOrganization == null || lOrganization.getParent() == null)
            return null;

        return lOrganization.getParent();
    }

    private Long resolveParentId(IOrganization organization)
    {
        if (organization == null || organization.getParent() == null)
        {
            return null;
        }

        return organization.getParent().getId();
    }

    public void updateFromLightweight(final ILOrganization light, final IOrganization object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILOrganization, IOrganization> updater = new TranslatorUtil<ILOrganization, IOrganization>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("parent").update();

        updater.updateField("resourceGroups").fromField("resourceGroupIds").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILOrganization light, IOrganization object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid organization name");

        if (light.getId() != null)
            if (light.getId().equals(light.getParent()))
                resp.logErr("the organization parent cannot be itself");

        if (object != null)
        {
            if (CollectUtils.getIds(object.allChildren(true)).contains(light.getParent()))
                resp.logErr("the organization parent cannot be one of its children");
        }

        return resp;
    }
}
