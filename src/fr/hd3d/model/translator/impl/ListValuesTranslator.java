package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILListValues;
import fr.hd3d.model.lightweight.impl.LListValues;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.impl.hibernate.ListValuesH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Try LAM
 */
public class ListValuesTranslator extends BaseTranslator<ILListValues, IListValues>
{
    public IListValues fromLightweight(ILListValues light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IListValues listValues = new ListValuesH();

        listValues.setSeparator(trim(light.getSeparator()));
        listValues.setValues(trim(light.getValues()));

        TranslatorUtil.fromLightWeightCommonFields(listValues, light, context);

        return listValues;
    }

    public ILListValues toLightweight(IListValues object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILListValues listValues = LListValues.getNewInstance(object.getId(), object.getVersion(), null, object
                .getSeparator(), object.getValues());

        TranslatorUtil.toLightWeightCommonFields(object, listValues, context);

        return listValues;
    }

    public void updateFromLightweight(final ILListValues light, final IListValues object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILListValues, IListValues> updater = new TranslatorUtil<ILListValues, IListValues>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("separator").update();

        updater.updateField("values").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILListValues light, IListValues object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getSeparator() != null)
            if (StringUtils.isBlank(light.getSeparator()))
                resp.logErr("Invalid listvalues separator");

        return resp;
    }
}
