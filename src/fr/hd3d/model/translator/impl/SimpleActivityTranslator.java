package fr.hd3d.model.translator.impl;

import java.text.MessageFormat;

import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.lightweight.impl.LSimpleActivity;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.SimpleActivityH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class SimpleActivityTranslator extends BaseTranslator<ILSimpleActivity, ISimpleActivity>
{
    public ISimpleActivity fromLightweight(ILSimpleActivity light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        // FIXME should those two calls to database be in a session ?
        IPerson worker = null;
        if (BaseH.isValidId(light.getWorkerID()))
            worker = Persistors.person.getById(light.getWorkerID());
        if (worker == null)
            throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "Worker", light.getWorkerID()));

        IProject project = null;
        if (BaseH.isValidId(light.getProjectID()))
            project = Persistors.project.getById(light.getProjectID());
        if (project == null)
            throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "Project", light.getProjectID()));

        ESimpleActivityType type = ESimpleActivityType.OTHER;
        try
        {
            type = ESimpleActivityType.valueOf(trim(light.getType()));
        }
        catch (Exception e)
        {
            type = ESimpleActivityType.OTHER;
        }

        IPerson filledBy = null;
        if (BaseH.isValidId(light.getFilledByID()))
            filledBy = Persistors.person.getById(light.getFilledByID());
        if (filledBy == null)
            throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "User Filled By", light.getFilledByID()));

        IPersonDay day = null;
        if (BaseH.isValidId(light.getDayID()))
            day = Persistors.personDay.getById(light.getDayID());
        if (day == null)
            throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "Person Day", light.getDayID()));

        final ISimpleActivity simpleActivity = new SimpleActivityH(day, light.getDuration(), trim(light.getComment()),
                type, project, worker, filledBy, light.getFilledDate());

        TranslatorUtil.fromLightWeightCommonFields(simpleActivity, light, context);
        return simpleActivity;
    }

    public ILSimpleActivity toLightweight(ISimpleActivity object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final IProject project = object.getProject();
        final IPerson worker = object.getWorker();
        final IPerson filledBy = object.getFilledBy();
        final LSimpleActivity lActivity = LSimpleActivity.getNewInstance(object.getId(), object.getVersion(), null,
                object.getType(), nullSafeId(object.getDay()), object.getDuration(), object.getComment(),
                nullSafeId(worker), nullSafeId(project), nullSafeId(filledBy), object.getFilledDate());
        lActivity.setDayDate(object.getDay().getDate());
        lActivity.setProjectColor(project.getColor());
        lActivity.setProjectName(project.getName());
        IProjectType projectType = project.getProjectType();
        if (projectType != null)
        {
            lActivity.setProjectTypeName(projectType.getName());
        }

        lActivity.setWorkerName(worker.fullName());
        lActivity.setWorkerLogin(worker.getLogin());
        lActivity.setFilledByLogin(filledBy.getLogin());
        lActivity.setFilledByName(filledBy.fullName());
        TranslatorUtil.toLightWeightCommonFields(object, lActivity, context);
        return lActivity;
    }

    public void updateFromLightweight(final ILSimpleActivity light, final ISimpleActivity object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILSimpleActivity, ISimpleActivity> updater = new TranslatorUtil<ILSimpleActivity, ISimpleActivity>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("duration").update();

        updater.updateField("comment").update();

        updater.updateField("type").update();

        updater.updateField("day").fromField("dayID").update();

        updater.updateField("worker").fromField("workerID").update();

        updater.updateField("project").fromField("projectID").update();

        updater.updateField("filledBy").fromField("filledByID").update();

        updater.updateField("filledDate").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILSimpleActivity light, ISimpleActivity object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getProjectID() == null || light.getWorkerID() == null)
            resp.logErr("Object should have workerId and projectId");

        return resp;
    }
}
