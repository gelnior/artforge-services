package fr.hd3d.model.translator.impl;

import java.util.HashSet;
import java.util.Set;

import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILAssetAbstract;
import fr.hd3d.model.lightweight.impl.LAssetAbstract;
import fr.hd3d.model.persistence.IAssetAbstract;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.services.resources.ResourceContext;


public class AssetAbstractTranslator extends BaseTranslator<ILAssetAbstract, IAssetAbstract>
{

    public IAssetAbstract fromLightweight(ILAssetAbstract light, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        return null;
    }

    public ILAssetAbstract toLightweight(IAssetAbstract object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        Set<Integer> revisionSet = new HashSet<Integer>();
        Integer lastRev = -1;
        for (IAssetRevision a : object.getRevisions())
        {
            revisionSet.add(a.getRevision());
            if (a.getRevision() > lastRev)
                lastRev = a.getRevision();
        }
        Long creatorId = nullSafeId(object.getCreator());
        Long projectId = nullSafeId(object.getProject());
        final ITaskType taskType = object.getTaskType();
        ILAssetAbstract lightweight = new LAssetAbstract(projectId, object.getId(), object.getKey(), object
                .getVariation(), object.getName(), nullSafeId(object.getTaskType()), (taskType == null ? null
                : taskType.getName()), object.getCreationDate(), creatorId, lastRev, revisionSet);
        lightweight.setVersion(object.getVersion());
        return lightweight;
    }

    public void updateFromLightweight(ILAssetAbstract light, IAssetAbstract object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        return;
    }

}
