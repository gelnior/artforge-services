package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LProject;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ProjectH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;
import fr.hd3d.utils.StringUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Transform Object in their Lightweight form and vice-versa. Also handle database update from Lightweight form.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ProjectTranslator extends BaseTranslator<ILProject, IProject>
{
    public ILProject toLightweight(IProject object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        String projectTypeName = null;
        if (object.getProjectType() != null)
        {
            projectTypeName = object.getProjectType().getName();
        }
        final ILProject project = LProject.getNewInstance(object.getId(), object.getVersion(), null, object.getName(),
                object.getStatus().toString(), object.getStartDate(), object.getEndDate(), object.getColor(), object
                        .getHook(), nullSafeId(object.getProjectType()), projectTypeName);

        if (object.rootCategory() != null)
        {
            project.setRootCategoryId(object.rootCategory().getId());
        }

        if (object.rootSequence() != null)
        {
            project.setRootSequenceId(object.rootSequence().getId());
        }

        for (IResourceGroup resourceGroup : object.getResourceGroups())
        {
            project.addResourceGroupID(resourceGroup.getId());
            project.addResourceGroupName(resourceGroup.getName());
        }

        for (ITaskType taskType : object.getTaskTypes())
        {
            project.addTaskTypeID(taskType.getId());
            project.addTaskTypeName(taskType.getName());

            String color = taskType.getColor();
            if (StringUtils.isNotValidColor(color))
                project.addTaskTypeColor("#FFF");
            else
                project.addTaskTypeColor(color);
        }

        TranslatorUtil.toLightWeightCommonFields(object, project, context);
        return project;
    }

    public IProject fromLightweight(ILProject light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        // IProject project = new ProjectH();

        // updateFields(new TranslatorUtil<ILProject, IProject>(this).withObject(project).withLightObject(light));

        List<IResourceGroup> resourceGroups = new ArrayList<IResourceGroup>();
        final Set<Long> resourceGroupIDs = light.getResourceGroupIDs();
        CollectUtils.removeNull(resourceGroupIDs);
        if (BaseH.areValidIds(resourceGroupIDs))
        {
            ((ArrayList<IResourceGroup>) resourceGroups).ensureCapacity(resourceGroupIDs.size());
            resourceGroups = Persistors.resourcegroup.getByIds(null, resourceGroupIDs);
        }
        else
            resourceGroups = new ArrayList<IResourceGroup>();

        List<ITaskType> taskTypes = new ArrayList<ITaskType>();
        final Set<Long> taskTypeIDs = light.getTaskTypeIDs();
        CollectUtils.removeNull(taskTypeIDs);
        if (BaseH.areValidIds(taskTypeIDs))
        {
            ((ArrayList<ITaskType>) taskTypes).ensureCapacity(taskTypeIDs.size());
            taskTypes.addAll(Persistors.tasktype.getByIds(null, taskTypeIDs));
        }

        IProjectType projectType = null;
        if (BaseH.isValidId(light.getProjectTypeId()))
            projectType = Persistors.projecttype.getById(light.getProjectTypeId());

        IProject project = new ProjectH(trim(light.getName()), light.getStartDate(), light.getEndDate(), trim(light
                .getColor()), EnumUtils.getEnumValueIgnoreCase(EProjectStatus.class, trim(light.getStatus())),
                new HashSet<IResourceGroup>(resourceGroups), trim(light.getHook()));

        project.setProjectType(projectType);

        TranslatorUtil.fromLightWeightCommonFields(project, light, context);
        return project;
    }

    private void updateFields(final TranslatorUtil<ILProject, IProject> updater) throws Hd3dException
    {
        updater.updateField("name").update();

        updater.updateField("startDate").update();

        updater.updateField("endDate").update();

        updater.updateField("status").update();

        updater.updateField("color").update();

        updater.updateField("hook").update();

        updater.updateField("resourceGroups").fromField("resourceGroupIDs").update();

        updater.updateField("taskTypes").fromField("taskTypeIDs").update();

        updater.updateField("projectType").fromField("projectTypeId").update();
    }

    public void updateFromLightweight(final ILProject light, final IProject object, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        // Check if the lightweight object is conform.
        validate(light, object);

        final TranslatorUtil<ILProject, IProject> updater = new TranslatorUtil<ILProject, IProject>(this).withObject(
                object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updateFields(updater);
    }

    @Override
    public ValidationResponse validateLightWeight(ILProject light, IProject object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid project name");

        if (light.getColor() != null)
            if (StringUtils.isNotValidColor(light.getColor()))
                resp.logErr("Invalid color");

        if (light.getStatus() != null)
            try
            {
                EnumUtils.getEnumValueIgnoreCase(EProjectStatus.class, light.getStatus());
            }
            catch (Exception e)
            {
                resp.logErr("Invalid status " + light.getStatus() + " for project " + light.getName());
            }

        return resp;
    }
}
