package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILPlayListEdit;
import fr.hd3d.model.lightweight.impl.LPlayListEdit;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.PlayListEditH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class PlayListEditTranslator extends BaseTranslator<ILPlayListEdit, IPlayListEdit>
{
    public IPlayListEdit fromLightweight(ILPlayListEdit light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IPlayListEdit playListEdit = new PlayListEditH();
        playListEdit.setName(light.getName());
        playListEdit.setSrcIn(light.getSrcIn());
        playListEdit.setSrcOut(light.getSrcOut());
        playListEdit.setRecIn(light.getRecIn());
        playListEdit.setRecOut(light.getRecOut());
        // playListEdit.setFileRevision(Persistors.filerevision.getById(light.getFileRevision()));
        playListEdit.setProxy(Persistors.proxy.getById(light.getProxy()));
        playListEdit.setPlayListRevision(Persistors.playlistrevision.getById(light.getPlayListRevision()));

        TranslatorUtil.fromLightWeightCommonFields(playListEdit, light, context);
        return playListEdit;
    }

    public ILPlayListEdit toLightweight(IPlayListEdit object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILPlayListEdit playListEdit = LPlayListEdit.getNewInstance(object.getId(), object.getVersion(), null, object
                .getSrcIn(), object.getSrcOut(), object.getRecIn(), object.getRecOut(), nullSafeId(object.getProxy()),
                nullSafeId(object.getPlayListRevision()), object.getName());

        TranslatorUtil.toLightWeightCommonFields(object, playListEdit, context);
        return playListEdit;
    }

    public void updateFromLightweight(final ILPlayListEdit light, final IPlayListEdit object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILPlayListEdit, IPlayListEdit> updater = new TranslatorUtil<ILPlayListEdit, IPlayListEdit>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("srcIn").update();

        updater.updateField("srcOut").update();

        updater.updateField("recIn").update();

        updater.updateField("recOut").update();

        // updater.updateField("fileRevision").update();

        updater.updateField("proxy").update();

        updater.updateField("playListRevision").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILPlayListEdit light, IPlayListEdit object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        return resp;
    }

}
