package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILAssetRevisionLink;
import fr.hd3d.model.lightweight.impl.LAssetRevisionLink;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionLinkH;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class AssetRevisionLinkTranslator extends BaseTranslator<ILAssetRevisionLink, IAssetRevisionLink>
{

    public IAssetRevisionLink fromLightweight(ILAssetRevisionLink light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        IAssetRevisionLink assetlink = new AssetRevisionLinkH();

        if (BaseH.isValidId(light.getInputAsset()))
            assetlink.setInputAsset(Persistors.assetrevision.getById(light.getInputAsset()));
        if (BaseH.isValidId(light.getOutputAsset()))
            assetlink.setOutputAsset(Persistors.assetrevision.getById(light.getOutputAsset()));
        assetlink.setType(trim(light.getType()));

        TranslatorUtil.fromLightWeightCommonFields(assetlink, light, context);
        return assetlink;
    }

    public ILAssetRevisionLink toLightweight(IAssetRevisionLink object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILAssetRevisionLink assetlink = LAssetRevisionLink.getNewInstance(object.getId(), object.getVersion(), null,
                nullSafeId(object.getInputAsset()), nullSafeId(object.getOutputAsset()), object.getType());

        TranslatorUtil.toLightWeightCommonFields(object, assetlink, context);
        return assetlink;
    }

    public void updateFromLightweight(final ILAssetRevisionLink light, final IAssetRevisionLink object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILAssetRevisionLink, IAssetRevisionLink> updater = new TranslatorUtil<ILAssetRevisionLink, IAssetRevisionLink>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("inputAsset").update();

        updater.updateField("outputAsset").update();

        updater.updateField("type").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILAssetRevisionLink light, IAssetRevisionLink object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getInputAsset() != null)
            if (BaseH.isNotValidId(light.getInputAsset()))
                resp.logErr("Invalid input asset id");

        if (light.getOutputAsset() != null)
            if (BaseH.isNotValidId(light.getOutputAsset()))
                resp.logErr("Invalid output asset id");

        return resp;
    }
}
