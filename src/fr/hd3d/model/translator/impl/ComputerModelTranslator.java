package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILComputerModel;
import fr.hd3d.model.lightweight.impl.LComputerModel;
import fr.hd3d.model.persistence.IComputerModel;
import fr.hd3d.model.persistence.IManufacturer;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ComputerModelH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted device object to Lightweight device object and vice-versa. It also handles update from
 * lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class ComputerModelTranslator extends BaseTranslator<ILComputerModel, IComputerModel>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IComputerModel fromLightweight(ILComputerModel light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IManufacturer manufacturer = null;
        if (BaseH.isValidId(light.getManufacturerId()))
        {
            manufacturer = Persistors.manufacturer.getById(light.getManufacturerId());
        }

        IComputerModel computerModel = new ComputerModelH(trim(light.getName()), manufacturer);

        TranslatorUtil.fromLightWeightCommonFields(computerModel, light, context);
        return computerModel;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILComputerModel toLightweight(IComputerModel object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        IManufacturer manufacturer = object.getManufacturer();

        final ILComputerModel computerModel = LComputerModel.getNewInstance(object.getId(), object.getVersion(), null,
                object.getName());

        if (manufacturer != null)
        {
            computerModel.setManufacturerId(manufacturer.getId());
            computerModel.setManufacturerName(manufacturer.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, computerModel, context);
        return computerModel;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILComputerModel light, final IComputerModel object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILComputerModel, IComputerModel> updater = new TranslatorUtil<ILComputerModel, IComputerModel>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("manufacturer").fromField("manufacturerId").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILComputerModel light, IComputerModel object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid computer model name");
        }

        return resp;
    }
}
