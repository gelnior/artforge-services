package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILPool;
import fr.hd3d.model.lightweight.impl.LPool;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.PoolH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted pool object to Lightweight pool object and vice-versa. It also handles update from lightweight
 * to persisted device object.
 * 
 * @author HD3D
 */
public class PoolTranslator extends BaseTranslator<ILPool, IPool>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IPool fromLightweight(ILPool light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ArrayList<IComputer> computers = new ArrayList<IComputer>();
        final List<Long> computerIDs = light.getComputerIDs();
        CollectUtils.removeNull(computerIDs);
        if (BaseH.areValidIds(computerIDs))
        {
            computers.ensureCapacity(computerIDs.size());
            computers = (ArrayList<IComputer>) Persistors.computer.getByIds(null, computerIDs);
        }
        else
            computers = new ArrayList<IComputer>();

        IPool pool = new PoolH(trim(light.getName()), light.getIsDispatcherAllowed(), new HashSet<IComputer>(computers));

        TranslatorUtil.fromLightWeightCommonFields(pool, light, context);
        return pool;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILPool toLightweight(IPool object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILPool pool = LPool.getNewInstance(object.getId(), object.getVersion(), null, object.getName(), object
                .getIsDispatcherAllowed());

        for (IComputer computer : object.getComputers())
        {
            pool.addComputerID(computer.getId());
            pool.addComputerName(computer.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, pool, context);
        return pool;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILPool light, final IPool object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILPool, IPool> updater = new TranslatorUtil<ILPool, IPool>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("isDispatcherAllowed").update();

        updater.updateField("computers").fromField("computerIDs").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILPool light, IPool object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid pool name");
        }

        return resp;
    }
}
