/**
 * 
 */
package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILAbsence;
import fr.hd3d.model.lightweight.impl.LAbsence;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.impl.hibernate.AbsenceH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author thomas-eskenazi
 * 
 */
public class AbsenceTranslator extends BaseTranslator<ILAbsence, IAbsence>
{
    public ILAbsence toLightweight(IAbsence object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILAbsence absence = new LAbsence();
        absence.setId(object.getId());
        absence.setComment(object.getComment());
        absence.setType(object.getType());
        absence.setStartDate(object.getStartDate());
        absence.setEndDate(object.getEndDate());
        absence.setWorkerID(nullSafeId(object.getWorker()));

        TranslatorUtil.toLightWeightCommonFields(object, absence, context);
        return absence;
    }

    public IAbsence fromLightweight(ILAbsence light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        final IAbsence absence = new AbsenceH();

        updateFields(new TranslatorUtil<ILAbsence, IAbsence>(this).withObject(absence).withLightObject(light));

        TranslatorUtil.fromLightWeightCommonFields(absence, light, context);
        return absence;

    }

    private void updateFields(final TranslatorUtil<ILAbsence, IAbsence> updater) throws Hd3dException
    {
        updater.updateField("type").update();

        updater.updateField("comment").update();

        updater.updateField("startDate").update();

        updater.updateField("endDate").update();

        updater.updateField("worker").fromField("workerID").update();
    }

    public void updateFromLightweight(final ILAbsence light, final IAbsence object, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILAbsence, IAbsence> updater = new TranslatorUtil<ILAbsence, IAbsence>(this).withObject(
                object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updateFields(updater);

    }

    @Override
    public ValidationResponse validateLightWeight(ILAbsence light, IAbsence object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getStartDate() == null)
            resp.logErr("Invalid start date");

        if (light.getEndDate() == null)
            resp.logErr("Invalid end date");

        return resp;
    }
}
