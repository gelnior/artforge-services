package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILComputerType;
import fr.hd3d.model.lightweight.impl.LComputerType;
import fr.hd3d.model.persistence.IComputerType;
import fr.hd3d.model.persistence.impl.hibernate.ComputerTypeH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted device object to Lightweight device object and vice-versa. It also handles update from
 * lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class ComputerTypeTranslator extends BaseTranslator<ILComputerType, IComputerType>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IComputerType fromLightweight(ILComputerType light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IComputerType computerType = new ComputerTypeH(light.getId(), light.getVersion(), light.getName());

        TranslatorUtil.fromLightWeightCommonFields(computerType, light, context);
        return computerType;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILComputerType toLightweight(IComputerType object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILComputerType computerType = LComputerType.getNewInstance(object.getId(), object.getVersion(), null,
                object.getName());

        TranslatorUtil.toLightWeightCommonFields(object, computerType, context);
        return computerType;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILComputerType light, final IComputerType object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILComputerType, IComputerType> updater = new TranslatorUtil<ILComputerType, IComputerType>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILComputerType light, IComputerType object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid computerType name");
        }

        return resp;
    }
}
