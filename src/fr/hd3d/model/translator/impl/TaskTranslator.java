package fr.hd3d.model.translator.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.impl.LTask;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.resources.collectionquery.TaskBoundEntityLinksCollectionQuery;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class TaskTranslator extends BaseTranslator<ILTask, ITask>
{
    public ITask fromLightweight(ILTask light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        try
        {
            final ITask task = new TaskH();
            task.setName(trim(light.getName()));
            task.setStatus(EnumUtils.getEnumValueIgnoreCase(ETaskStatus.class, trim(light.getStatus())));
            task.setCompletion(light.getCompletion());
            task.setCompletionDate(light.getCompletionDate());
            if (BaseH.isValidId(light.getProjectID()))
                task.setProject(Persistors.project.getById(light.getProjectID()));
            task.setActualEndDate(light.getActualEndDate());
            task.setActualStartDate(light.getActualStartDate());
            task.setConfirmed(light.getConfirmed());
            task.setDeadLine(light.getDeadLine());
            task.setDuration(light.getDuration());
            task.setEndDate(light.getEndDate());
            task.setStartable(light.getStartable());
            task.setStartDate(light.getStartDate());
            task.setStartup(light.getStartup());

            if (BaseH.isValidId(light.getCreatorID()))
                task.setCreator(Persistors.person.getById(light.getCreatorID()));
            else
            {
                IPerson creator = AuthenticationUtil.getCurrentUser();
                task.setCreator(creator);
            }

            if (BaseH.isValidId(light.getWorkerID()))
            {
                task.setWorker(Persistors.person.getById(light.getWorkerID()));
            }
            if (BaseH.isValidId(light.getTaskGroupID()))
            {
                task.setTaskGroup(Persistors.taskGroup.getById(light.getId()));
            }
            if (BaseH.isValidId(light.getTaskTypeID()))
            {
                task.setTaskType(Persistors.tasktype.getById(light.getTaskTypeID()));
            }
            if (BaseH.isValidId(light.getExtraLineID()))
            {
                task.setExtraLine(Persistors.extraline.getById(light.getExtraLineID()));
            }
            if (BaseH.isValidId(light.getBoundEntityTaskLink()))
            {
                task.setBoundEntityTaskLink(Persistors.entitytasklink.getById(light.getBoundEntityTaskLink()));
            }
            else if (BaseH.isValidId(light.getWorkObjectId()) && StringUtils.isNotBlank(light.getBoundEntityName()))
            {
                task.setBoundEntityName(light.getBoundEntityName());
                task.setBoundEntityId(light.getWorkObjectId());
            }

            task.setCommentForApprovalNote(light.getCommentForApprovalNote());

            List<IFileRevision> fileRevisionList = Persistors.filerevision.getByIds(light
                    .getFileRevisionsForApprovalNote());
            task.setFileRevisionsForApprovalNote(new HashSet<IFileRevision>(fileRevisionList));

            TranslatorUtil.fromLightWeightCommonFields(task, light, context);
            return task;
        }
        catch (Hd3dPersistenceException e)
        {
            throw new Hd3dTranslationException(e.getMessage());
        }
    }

    public ILTask toLightweight(ITask object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        final LTask task = new LTask();
        task.setId(object.getId());
        task.setVersion(object.getVersion());
        task.setName(object.getName());
        task.setStatus(object.getStatus().toString());
        task.setCompletion(object.getCompletion());
        task.setCompletionDate(object.getCompletionDate());
        task.setProjectID(nullSafeId(object.getProject()));

        if (object.getProject() != null)
        {
            IProject project = object.getProject();
            task.setProjectName(project.getName());
            task.setProjectColor(project.getColor());
            IProjectType projectType = project.getProjectType();
            if (projectType != null)
            {
                task.setProjectTypeName(projectType.getName());
            }
        }
        task.setCreatorID(nullSafeId(object.getCreator()));
        if (object.getCreator() != null)
        {
            task.setCreatorName(object.getCreator().fullName());
        }
        task.setActualEndDate(object.getActualEndDate());
        task.setActualStartDate(object.getActualStartDate());
        task.setConfirmed(object.getConfirmed());
        task.setDeadLine(object.getDeadLine());
        task.setDuration(object.getDuration());
        task.setEndDate(object.getEndDate());
        task.setStartable(object.getStartable());
        task.setStartDate(object.getStartDate());
        task.setStartup(object.getStartup());
        if (object.getWorker() != null)
        {
            task.setWorkerID(object.getWorker().getId());
            task.setWorkerName(object.getWorker().fullName());
        }
        if (object.getTaskGroup() != null)
        {
            task.setTaskGroupID(object.getTaskGroup().getId());
        }
        if (object.getTaskType() != null)
        {
            task.setTaskType(Translators.taskType.toLightweight(object.getTaskType(), context));
            task.setTaskTypeID(object.getTaskType().getId());
            task.setColor(object.getTaskType().getColor());
            task.setTaskTypeName(object.getTaskType().getName());
        }
        if (object.getExtraLine() != null)
        {
            task.setExtraLineID(object.getExtraLine().getId());
        }

        IBase boundEntity = object.boundEntity();
        if (boundEntity != null)
        {
            if (IProject.class.isInstance(boundEntity))
            {
                final IProject project = (IProject) boundEntity;
                task.setWorkObjectName(project.getName());
                task.setWorkObjectId(project.getId());
            }
            else if (IConstituent.class.isInstance(boundEntity))
            {
                final IConstituent constituent = (IConstituent) boundEntity;
                task.setWorkObjectName(constituent.getLabel());
                task.setWorkObjectId(constituent.getId());
                task.setWorkObjectParentsNames(constituent.getPath());
            }
            else if (IShot.class.isInstance(boundEntity))
            {
                final IShot shot = (IShot) boundEntity;
                task.setWorkObjectName(shot.getLabel());
                task.setWorkObjectId(shot.getId());
                task.setWorkObjectParentsNames(shot.getPath());
            }
            else if (ISequence.class.isInstance(boundEntity))
            {
                final ISequence sequence = (ISequence) boundEntity;
                task.setWorkObjectName(sequence.getName());
                task.setWorkObjectId(sequence.getId());
                task.setWorkObjectParentsNames(sequence.getPath());
            }
            else if (ICategory.class.isInstance(boundEntity))
            {
                final ICategory category = (ICategory) boundEntity;
                task.setWorkObjectName(category.getName());
                task.setWorkObjectId(category.getId());
                task.setWorkObjectParentsNames(category.getPath());
            }
            task.setBoundEntityName(boundEntity.entityName());
        }

        if (object.getBoundEntityTaskLink() != null)
        {
            task.setBoundEntityTaskLink(object.getBoundEntityTaskLink().getId());
            task.setBoundEntityName(object.getBoundEntityTaskLink().getBoundEntityName());
        }

        if (object.getTotalActivitiesDuration() != null)
        {
            task.setTotalActivitiesDuration(object.getTotalActivitiesDuration());
        }

        TranslatorUtil.toLightWeightCommonFields(object, task, context);
        return task;
    }

    public List<ILTask> toLightweightCollection(Collection<ITask> objects, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        CollectUtils.removeNull(objects);

        List<ILTask> ret = new ArrayList<ILTask>();
        if (CollectUtils.isEmpty(objects))
        {
            return ret;
        }

        for (ITask object : objects)
        {
            if (object == null)
                return null;

            final LTask task = new LTask();
            task.setId(object.getId());
            task.setVersion(object.getVersion());
            task.setName(object.getName());
            task.setStatus(object.getStatus().toString());
            task.setCompletion(object.getCompletion());
            task.setCompletionDate(object.getCompletionDate());
            task.setProjectID(nullSafeId(object.getProject()));

            if (object.getProject() != null)
            {
                IProject project = object.getProject();
                task.setProjectName(project.getName());
                task.setProjectColor(project.getColor());
                IProjectType projectType = project.getProjectType();
                if (projectType != null)
                {
                    task.setProjectTypeName(projectType.getName());
                }
            }
            task.setCreatorID(nullSafeId(object.getCreator()));
            if (object.getCreator() != null)
            {
                task.setCreatorName(object.getCreator().fullName());
            }
            task.setActualEndDate(object.getActualEndDate());
            task.setActualStartDate(object.getActualStartDate());
            task.setConfirmed(object.getConfirmed());
            task.setDeadLine(object.getDeadLine());
            task.setDuration(object.getDuration());
            task.setEndDate(object.getEndDate());
            task.setStartable(object.getStartable());
            task.setStartDate(object.getStartDate());
            task.setStartup(object.getStartup());
            if (object.getWorker() != null)
            {
                task.setWorkerID(object.getWorker().getId());
                task.setWorkerName(object.getWorker().fullName());
            }
            if (object.getTaskGroup() != null)
            {
                task.setTaskGroupID(object.getTaskGroup().getId());
            }
            if (object.getTaskType() != null)
            {
                task.setTaskType(Translators.taskType.toLightweight(object.getTaskType(), context));
                task.setTaskTypeID(object.getTaskType().getId());
                task.setColor(object.getTaskType().getColor());
                task.setTaskTypeName(object.getTaskType().getName());
            }
            if (object.getExtraLine() != null)
            {
                task.setExtraLineID(object.getExtraLine().getId());
            }

            if (object.getTotalActivitiesDuration() != null)
            {
                task.setTotalActivitiesDuration(object.getTotalActivitiesDuration());
            }

            TranslatorUtil.toLightWeightCommonFields(object, task, context);

            ret.add(task);
        }

        /* complete missing fields with collection queries */
        //
        TaskBoundEntityLinksCollectionQuery cq0 = new TaskBoundEntityLinksCollectionQuery();
        cq0.setEntities(new ArrayList<ITask>(objects));
        List<List<IEntityTaskLink>> allLinks = cq0.doQuery();
        for (int i = 0; i < allLinks.size(); i++)
        {
            ILTask lTask = ret.get(i);
            if (lTask == null)
                continue;
            List<IEntityTaskLink> links = allLinks.get(i);
            if (CollectUtils.isEmpty(links))
                continue;

            lTask.setBoundEntityTaskLink(links.get(0).getId());
            lTask.setWorkObjectName(links.get(0).getWoName());
            lTask.setBoundEntityName(links.get(0).getBoundEntityName());
            lTask.setWorkObjectId(links.get(0).getBoundEntity());
            lTask.setWorkObjectParentsNames(links.get(0).getWoPath());
        }

        // TaskBoundEntityCollectionQuery cq = new TaskBoundEntityCollectionQuery();
        // cq.setEntities(new ArrayList<ITask>(objects));
        //
        // List<? extends IBase> boundEntities = cq.doQuery();
        // for (int i = 0; i < boundEntities.size(); i++)
        // {
        // IBase boundEntity = boundEntities.get(i);
        // if (boundEntity == null)
        // continue;
        // ILTask lTask = ret.get(i);
        // if (lTask == null)
        // continue;
        //
        // lTask.setWorkObjectName(boundEntity);
        // lTask.setWorkObjectId(project.getId());
        //
        // if (IProject.class.isInstance(boundEntity))
        // {
        // final IProject project = (IProject) boundEntity;
        // lTask.setWorkObjectName(project.getName());
        // lTask.setWorkObjectId(project.getId());
        // }
        // else if (IConstituent.class.isInstance(boundEntity))
        // {
        // final IConstituent constituent = (IConstituent) boundEntity;
        // lTask.setWorkObjectName(constituent.getLabel());
        // lTask.setWorkObjectId(constituent.getId());
        // lTask.setWorkObjectParentsNames(constituent.getPath());
        // }
        // else if (IShot.class.isInstance(boundEntity))
        // {
        // final IShot shot = (IShot) boundEntity;
        // lTask.setWorkObjectName(shot.getLabel());
        // lTask.setWorkObjectId(shot.getId());
        // lTask.setWorkObjectParentsNames(shot.getPath());
        // }
        // else if (ISequence.class.isInstance(boundEntity))
        // {
        // final ISequence sequence = (ISequence) boundEntity;
        // lTask.setWorkObjectName(sequence.getName());
        // lTask.setWorkObjectId(sequence.getId());
        // lTask.setWorkObjectParentsNames(sequence.getPath());
        // }
        // else if (ICategory.class.isInstance(boundEntity))
        // {
        // final ICategory category = (ICategory) boundEntity;
        // lTask.setWorkObjectName(category.getName());
        // lTask.setWorkObjectId(category.getId());
        // lTask.setWorkObjectParentsNames(category.getPath());
        // }
        // lTask.setBoundEntityName(boundEntity.entityName());
        // }

        return ret;
    }

    public void updateFromLightweight(final ILTask light, final ITask object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILTask, ITask> updater = new TranslatorUtil<ILTask, ITask>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("completion").update();

        updater.updateField("status").update();

        /* completion date is updated when status is updated, but it may be changed */
        updater.updateField("completionDate").update();

        updater.updateField("creator").fromField("creatorID").update();

        if (light.getWorkerID() != null)
        {
            if (light.getWorkerID().equals(Const.ID_RESET))
            {
                object.setWorker(null);
            }
            else
            {
                if (!ObjectUtils.equals(nullSafeId(object.getWorker()), light.getWorkerID()))
                {
                    final IPerson worker = light.getWorkerID() == null ? null : Persistors.person.getById(light
                            .getWorkerID());
                    object.setWorker(worker);
                }
            }
        }

        updater.updateField("project").fromField("projectID").update();

        updater.updateField("actualEndDate").update();

        updater.updateField("actualStartDate").update();

        updater.updateField("confirmed").update();

        updater.updateField("deadLine").update();

        updater.updateField("duration").update();

        updater.updateField("endDate").update();

        updater.updateField("startable").update();

        updater.updateField("startup").update();

        updater.updateField("startDate").update();

        updater.updateField("taskGroup").fromField("taskGroupID").update();

        updater.updateField("taskType").fromField("taskTypeID").update();

        if (light.getExtraLineID() != null)
        {
            /* disable */
            if (light.getExtraLineID() != -1)
            {
                final IExtraLine extraLine = Persistors.extraline.getById(light.getExtraLineID());
                if (extraLine == null)
                {
                    throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "ExtraLine", light
                            .getExtraLineID()));
                }
                object.setExtraLine(extraLine);
            }
            else
            {
                object.setExtraLine(null);
            }
        }

        updater.updateField("boundEntityTaskLink").update();

        updater.updateField("commentForApprovalNote").update();

        updater.updateField("fileRevisionsForApprovalNote").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILTask light, ITask object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        try
        {
            ETaskStatus.valueOf(light.getStatus());
        }
        catch (Exception e)
        {
            resp.logErr("Invalid status");
        }
        // final Byte completion = light.getCompletion();
        // if (completion == null || completion < 0 || completion > 100)
        // resp.logErr("Invalid completion : " + ((completion != null) ? completion.toString() : ""));

        return resp;
    }
}
