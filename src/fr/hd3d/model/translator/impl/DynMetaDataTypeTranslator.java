package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILDynMetaDataType;
import fr.hd3d.model.lightweight.impl.LDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataTypeH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class DynMetaDataTypeTranslator extends BaseTranslator<ILDynMetaDataType, IDynMetaDataType>
{
    public IDynMetaDataType fromLightweight(ILDynMetaDataType light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        IDynMetaDataType dynMetaDataType = new DynMetaDataTypeH();

        dynMetaDataType.setName(trim(light.getName()));
        dynMetaDataType.setNature(trim(light.getNature()));
        dynMetaDataType.setType(trim(light.getType()));
        dynMetaDataType.setDefaultValue(trim(light.getDefaultValue()));
        dynMetaDataType.setStructName(trim(light.getStructName()));
        dynMetaDataType.setStructId(light.getStructId());

        TranslatorUtil.fromLightWeightCommonFields(dynMetaDataType, light, context);

        return dynMetaDataType;
    }

    public ILDynMetaDataType toLightweight(IDynMetaDataType object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILDynMetaDataType dynMetaDataType = LDynMetaDataType.getNewInstance(object.getId(), object.getVersion(), null,
                object.getDefaultValue(), object.getName(), object.getNature(), object.getType());
        dynMetaDataType.setStructName(object.getStructName());
        dynMetaDataType.setStructId(object.getStructId());

        TranslatorUtil.toLightWeightCommonFields(object, dynMetaDataType, context);

        return dynMetaDataType;
    }

    public void updateFromLightweight(final ILDynMetaDataType light, final IDynMetaDataType object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILDynMetaDataType, IDynMetaDataType> updater = new TranslatorUtil<ILDynMetaDataType, IDynMetaDataType>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();
        updater.updateField("defaultValue").update();

        /* DO NOT ALLOW OTHER CHANGES THAN NAME AND DEFAULT_VALUE */
    }

    @Override
    public ValidationResponse validateLightWeight(ILDynMetaDataType light, IDynMetaDataType object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid dynmetadatatype name");

        if (light.getNature() != null)
            if (StringUtils.isBlank(light.getNature()))
                resp.logErr("Invalid dynmetadatatype nature");

        if (light.getType() != null)
            if (StringUtils.isBlank(light.getType()))
                resp.logErr("Invalid dynmetadatatype type");

        return resp;
    }
}
