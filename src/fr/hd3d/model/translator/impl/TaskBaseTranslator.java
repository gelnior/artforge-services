package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskBase;
import fr.hd3d.model.lightweight.ILTaskChanges;
import fr.hd3d.model.lightweight.ILTaskGroup;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskBase;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.ResourceContext;


public class TaskBaseTranslator extends BaseTranslator<ILTaskBase, ITaskBase>
{

    public ITaskBase fromLightweight(ILTaskBase light, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (light == null)
            return null;

        if (light instanceof ILTask)
        {
            return Translators.task.fromLightweight((ILTask) light, context);
        }
        else if (light instanceof ILTaskGroup)
        {
            return Translators.taskGroup.fromLightweight((ILTaskGroup) light, context);
        }
        else if (light instanceof ILTaskChanges)
        {
            return Translators.taskChanges.fromLightweight((ILTaskChanges) light, context);
        }
        throw new Hd3dTranslationException("object is not a task base of any kind");
    }

    public ILTaskBase toLightweight(ITaskBase object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        if (object instanceof ITask)
        {
            return Translators.task.toLightweight((ITask) object, context);
        }
        else if (object instanceof ITaskGroup)
        {
            return Translators.taskGroup.toLightweight((ITaskGroup) object, context);
        }
        else if (object instanceof ITaskChanges)
        {
            return Translators.taskChanges.toLightweight((ITaskChanges) object, context);
        }
        throw new Hd3dTranslationException("object is not a task base of any kind");
    }

    public void updateFromLightweight(ILTaskBase light, ITaskBase object, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        if (light == null || object == null)
            return;

        if (light instanceof ILTask)
        {
            Translators.task.updateFromLightweight((ILTask) light, (ITask) object, context);
        }
        else if (light instanceof ILTaskGroup)
        {
            Translators.taskGroup.updateFromLightweight((ILTaskGroup) light, (ITaskGroup) object, context);
        }
        else if (light instanceof ILTaskChanges)
        {
            Translators.taskChanges.updateFromLightweight((ILTaskChanges) light, (ITaskChanges) object, context);
        }
    }

}
