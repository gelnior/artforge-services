package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.common.client.enums.EScreenQualification;
import fr.hd3d.common.client.enums.EScreenType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILScreen;
import fr.hd3d.model.lightweight.impl.LScreen;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.IScreenModel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ScreenH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted device object to Lightweight device object and vice-versa. It also handles update from
 * lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class ScreenTranslator extends BaseTranslator<ILScreen, IScreen>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IScreen fromLightweight(ILScreen light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        EScreenType type = EScreenType.UNKNOWN;
        if (trim(light.getType()) != null)
        {
            // type = EScreenType.valueOf(light.getType());
            type = EnumUtils.getEnumValueIgnoreCase(EScreenType.class, light.getType());
        }

        EScreenQualification qualification = EScreenQualification.UNKNOWN;
        if (trim(light.getQualification()) != null)
        {
            // qualification = EScreenQualification.valueOf(light.getQualification());
            qualification = EnumUtils.getEnumValueIgnoreCase(EScreenQualification.class, light.getQualification());
        }

        EInventoryStatus inventoryStatus = EInventoryStatus.UNKNOWN;
        if (trim(light.getInventoryStatus()) != null)
        {
            inventoryStatus = EnumUtils.getEnumValueIgnoreCase(EInventoryStatus.class, light.getInventoryStatus());
        }

        IScreenModel screenModel = null;
        if (BaseH.isValidId(light.getScreenModelId()))
        {
            screenModel = Persistors.screenModel.getById(light.getScreenModelId());
        }

        ArrayList<IResourceGroup> resourceGroups = new ArrayList<IResourceGroup>();
        final List<Long> resourceGroupIDs = light.getResourceGroupIDs();
        CollectUtils.removeNull(resourceGroupIDs);
        if (BaseH.areValidIds(resourceGroupIDs))
        {
            resourceGroups.ensureCapacity(resourceGroupIDs.size());
            resourceGroups = (ArrayList<IResourceGroup>) Persistors.resourcegroup.getByIds(null, resourceGroupIDs);
        }
        else
            resourceGroups = new ArrayList<IResourceGroup>();

        ArrayList<IComputer> computers = new ArrayList<IComputer>();
        final List<Long> computerIDs = light.getComputerIDs();
        CollectUtils.removeNull(computerIDs);
        if (BaseH.areValidIds(computerIDs))
        {
            computers.ensureCapacity(computerIDs.size());
            computers = (ArrayList<IComputer>) Persistors.computer.getByIds(null, computerIDs);
        }
        else
            computers = new ArrayList<IComputer>();

        IScreen screen = new ScreenH(trim(light.getName()), trim(light.getSerial()), trim(light.getBillingReference()),
                light.getPurchaseDate(), light.getWarrantyEnd(), inventoryStatus, light.getSize(), type, qualification,
                screenModel, new HashSet<IComputer>(computers), new HashSet<IResourceGroup>(resourceGroups));

        TranslatorUtil.fromLightWeightCommonFields(screen, light, context);
        return screen;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILScreen toLightweight(IScreen object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        IScreenModel screenModel = object.getScreenModel();

        String type = EScreenType.UNKNOWN.toString();
        if (object.getType() != null)
        {
            type = object.getType().toString();
        }

        String qualification = EScreenQualification.UNKNOWN.toString();
        if (object.getQualification() != null)
        {
            qualification = object.getQualification().toString();
        }

        String inventoryStatus = EInventoryStatus.UNKNOWN.toString();
        if (object.getInventoryStatus() != null)
        {
            inventoryStatus = object.getInventoryStatus().toString();
        }

        final ILScreen screen = LScreen.getNewInstance(object.getId(), object.getVersion(), null, object.getName(),
                object.getSerial(), object.getBillingReference(), object.getPurchaseDate(), object.getWarrantyEnd(),
                inventoryStatus, object.getSize(), type, qualification);

        if (screenModel != null)
        {
            screen.setScreenModelId(screenModel.getId());
            screen.setScreenModelName(screenModel.getName());
        }

        for (IResourceGroup resourceGroup : object.getResourceGroups())
        {
            screen.addResourceGroupID(resourceGroup.getId());
            screen.addResourceGroupName(resourceGroup.getName());
        }

        for (IComputer computer : object.getComputers())
        {
            screen.addComputerID(computer.getId());
            screen.addComputerName(computer.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, screen, context);
        return screen;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILScreen light, final IScreen object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILScreen, IScreen> updater = new TranslatorUtil<ILScreen, IScreen>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("serial").update();

        updater.updateField("billingReference").update();

        updater.updateField("purchaseDate").update();

        updater.updateField("warrantyEnd").update();

        updater.updateField("inventoryStatus").update();

        updater.updateField("size").update();

        updater.updateField("type").update();

        updater.updateField("qualification").update();

        updater.updateField("screenModel").fromField("screenModelId").update();

        updater.updateField("resourceGroups").fromField("resourceGroupIDs").update();

        updater.updateField("computers").fromField("computerIDs").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILScreen light, IScreen object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null && StringUtils.isBlank(light.getName()))
        {
            resp.logErr("Invalid screen name");
        }

        if (light.getSize() != null && light.getSize() < 0)
        {
            resp.logErr("Invalid screen size");
        }

        return resp;
    }
}
