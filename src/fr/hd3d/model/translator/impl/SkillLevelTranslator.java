package fr.hd3d.model.translator.impl;

import fr.hd3d.common.client.enums.ESkillLevel;
import fr.hd3d.common.client.enums.ESkillMotivation;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILSkillLevel;
import fr.hd3d.model.lightweight.impl.LSkillLevel;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.ISkill;
import fr.hd3d.model.persistence.ISkillLevel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.SkillLevelH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted skillLevel object to Lightweight skillLevel object and vice-versa. It also handles update from
 * lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class SkillLevelTranslator extends BaseTranslator<ILSkillLevel, ISkillLevel>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public ISkillLevel fromLightweight(ILSkillLevel light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ESkillLevel level = ESkillLevel.UNKNOWN;

        final String levelStr = trim(light.getLevel());
        if (levelStr != null)
        {
            level = ESkillLevel.valueOf(levelStr);
        }

        ESkillMotivation motivation = ESkillMotivation.UNKNOWN;
        final String motivationStr = trim(light.getMotivation());
        if (motivationStr != null)
        {
            motivation = ESkillMotivation.valueOf(motivationStr);
        }

        IPerson person = null;
        if (BaseH.isValidId(light.getPersonId()))
        {
            person = Persistors.person.getById(light.getPersonId());
        }

        ISkill skill = null;
        if (BaseH.isValidId(light.getSkillId()))
        {
            skill = Persistors.skill.getById(light.getSkillId());
        }

        ISkillLevel skillLevel = new SkillLevelH(level, motivation, person, skill);

        TranslatorUtil.fromLightWeightCommonFields(skillLevel, light, context);
        return skillLevel;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILSkillLevel toLightweight(ISkillLevel object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        IPerson person = object.getPerson();
        ISkill skill = object.getSkill();

        String level = ESkillLevel.UNKNOWN.toString();
        if (object.getLevel() != null)
        {
            level = object.getLevel().toString();
        }

        String motivation = ESkillMotivation.UNKNOWN.toString();
        if (object.getMotivation() != null)
        {
            motivation = object.getMotivation().toString();
        }

        final ILSkillLevel skillLevel = LSkillLevel.getNewInstance(object.getId(), object.getVersion(), level,
                motivation);

        if (person != null)
        {
            skillLevel.setPersonId(person.getId());
            skillLevel.setPersonName(person.fullName());
        }

        if (skill != null)
        {
            skillLevel.setSkillId(skill.getId());
            skillLevel.setSkillName(skill.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, skillLevel, context);
        return skillLevel;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILSkillLevel light, final ISkillLevel object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILSkillLevel, ISkillLevel> updater = new TranslatorUtil<ILSkillLevel, ISkillLevel>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("level").update();

        updater.updateField("motivation").update();

        updater.updateField("person").fromField("personId").update();

        updater.updateField("skill").fromField("skillId").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILSkillLevel light, ISkillLevel object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        return resp;
    }
}
