package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILFileAttribute;
import fr.hd3d.model.lightweight.impl.LFileAttribute;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.FileAttributeH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class FileAttributeTranslator extends BaseTranslator<ILFileAttribute, IFileAttribute>
{

    public IFileAttribute fromLightweight(ILFileAttribute light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IFileAttribute filelink = new FileAttributeH();

        if (BaseH.isValidId(light.getFile()))
            filelink.setFileRevision(Persistors.filerevision.getById(light.getFile()));
        filelink.setName(trim(light.getName()));
        filelink.setValue(trim(light.getValue()));

        TranslatorUtil.fromLightWeightCommonFields(filelink, light, context);
        return filelink;
    }

    public ILFileAttribute toLightweight(IFileAttribute object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILFileAttribute fileattribute = LFileAttribute.getNewInstance(object.getId(), object.getVersion(), null,
                nullSafeId(object.getFileRevision()), object.getName(), object.getValue());

        TranslatorUtil.toLightWeightCommonFields(object, fileattribute, context);
        return fileattribute;
    }

    public void updateFromLightweight(final ILFileAttribute light, final IFileAttribute object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILFileAttribute, IFileAttribute> updater = new TranslatorUtil<ILFileAttribute, IFileAttribute>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("fileRevision").fromField("file").update();

        updater.updateField("name").update();

        updater.updateField("value").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILFileAttribute light, IFileAttribute object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getFile() != null)
            if (BaseH.isNotValidId(light.getFile()))
                resp.logErr("Invalid file id");

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid name");

        return resp;
    }
}
