package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.Entity;
import fr.hd3d.common.client.Sheet;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILItem;
import fr.hd3d.model.lightweight.impl.LItem;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataTypeConverter;
import fr.hd3d.model.persistence.impl.hibernate.ItemH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ItemTranslator extends BaseTranslator<ILItem, IItem>
{
    public IItem fromLightweight(ILItem light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IItemGroup itemGroup = null;
        if (BaseH.isValidId(light.getItemGroup()))
            itemGroup = Persistors.itemgroup.getById(light.getItemGroup());

        IItem item = new ItemH(trim(light.getName()), trim(light.getType()), trim(light.getQuery()), trim(light
                .getControlContent()), itemGroup, trim(light.getMetaType()), trim(light.getTransformer()), trim(light
                .getTransformerParameters()));

        if (light.getEditor() != null)
            item.setEditor(light.getEditor());
        else
            item.setEditor("");

        if (light.getRenderer() != null)
            item.setRenderer(light.getRenderer());
        else
            item.setRenderer("");

        TranslatorUtil.fromLightWeightCommonFields(item, light, context);
        return item;
    }

    public ILItem toLightweight(IItem object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        Long itemGroupId = Const.ID_NULL;// no factype
        if (object.getItemGroup() != null)
        {
            itemGroupId = object.getItemGroup().getId();
        }
        ILItem item = LItem.getNewInstance(object.getId(), object.getVersion(), null, object.getName(), object
                .getType(), object.getQuery(), object.getControlContent(), itemGroupId,
                object.getMetaType().toString(), object.getTransformer(), object.getTransformerParameters());

        item.setEditor(object.getEditor());
        item.setRenderer(object.getRenderer());

        if (Sheet.ITEMTYPE_METADATA.equals(item.getType()))
        {
            /* query is supposed to contain ClassDynMetaDataType id */
            Long metaDataId = Long.parseLong(item.getQuery());
            IClassDynMetaDataType classDynMetaDataType = Persistors.classdynmetadatatype.getById(metaDataId);
            if (classDynMetaDataType != null)
            {
                final String structName = classDynMetaDataType.getDynMetaDataType().getStructName();

                if (Entity.LISTVALUES.equals(structName))
                {
                    DynMetaDataTypeConverter converter = new DynMetaDataTypeConverter(classDynMetaDataType
                            .getDynMetaDataType());
                    item.setListValues(converter.getDynMetaDataTypeList());
                }
            }
        }

        TranslatorUtil.toLightWeightCommonFields(object, item, context);
        return item;
    }

    public void updateFromLightweight(final ILItem light, final IItem object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILItem, IItem> updater = new TranslatorUtil<ILItem, IItem>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("type").update();

        updater.updateField("query").update();

        updater.updateField("controlContent").update();

        updater.updateField("itemGroup").update();

        updater.updateField("metaType").update();

        updater.updateField("editor").update();

        updater.updateField("renderer").update();

        updater.updateField("transformer").update();

        updater.updateField("transformerParameters").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILItem light, IItem object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid item name");

        if (light.getItemGroup() != null)
            if (BaseH.isNotValidId(light.getItemGroup()))
                resp.logErr("Invalid itemgroup id");

        return resp;
    }
}
