/**
 * 
 */
package fr.hd3d.model.translator.impl;

import java.text.MessageFormat;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILWorkingTime;
import fr.hd3d.model.lightweight.impl.LWorkingTime;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.WorkingTimeH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Try LAM
 * 
 */
public class WorkingTimeTranslator extends BaseTranslator<ILWorkingTime, IWorkingTime>
{
    public IWorkingTime fromLightweight(ILWorkingTime light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        final IWorkingTime workingTime = new WorkingTimeH();
        workingTime.setStartDate(light.getStartDate());
        workingTime.setEndDate(light.getEndDate());
        if (BaseH.isValidId(light.getPersonDayId()))
        {
            IPersonDay personDay = Persistors.personDay.getById(light.getPersonDayId());
            if (personDay == null)
            {
                throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "PersonDay", light.getPersonDayId()));
            }
            workingTime.setPersonDay(personDay);
        }

        TranslatorUtil.fromLightWeightCommonFields(workingTime, light, context);
        return workingTime;
    }

    public ILWorkingTime toLightweight(IWorkingTime object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILWorkingTime workingTime = new LWorkingTime();
        workingTime.setId(object.getId());
        workingTime.setStartDate(object.getStartDate());
        workingTime.setEndDate(object.getEndDate());
        workingTime.setPersonDayId(nullSafeId(object.getPersonDay()));

        TranslatorUtil.toLightWeightCommonFields(object, workingTime, context);
        return workingTime;
    }

    public void updateFromLightweight(final ILWorkingTime light, final IWorkingTime object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILWorkingTime, IWorkingTime> updater = new TranslatorUtil<ILWorkingTime, IWorkingTime>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("startDate").update();

        updater.updateField("endDate").update();

        updater.updateField("personDay").fromField("personDayId").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILWorkingTime light, IWorkingTime object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getStartDate() == null)
            resp.logErr("Invalid start date");

        if (light.getEndDate() == null)
            resp.logErr("Invalid end date");

        if (light.getStartDate().after(light.getEndDate()))
            resp.logErr("Start Date must be BEFORE End Date");

        return resp;
    }
}
