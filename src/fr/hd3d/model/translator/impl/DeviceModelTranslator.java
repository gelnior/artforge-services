package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILDeviceModel;
import fr.hd3d.model.lightweight.impl.LDeviceModel;
import fr.hd3d.model.persistence.IDeviceModel;
import fr.hd3d.model.persistence.IManufacturer;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.DeviceModelH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted device object to Lightweight device object and vice-versa. It also handles update from
 * lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class DeviceModelTranslator extends BaseTranslator<ILDeviceModel, IDeviceModel>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IDeviceModel fromLightweight(ILDeviceModel light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IManufacturer manufacturer = null;
        if (BaseH.isValidId(light.getManufacturerId()))
        {
            manufacturer = Persistors.manufacturer.getById(light.getManufacturerId());
        }

        IDeviceModel deviceModel = new DeviceModelH(trim(light.getName()), manufacturer);

        TranslatorUtil.fromLightWeightCommonFields(deviceModel, light, context);
        return deviceModel;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILDeviceModel toLightweight(IDeviceModel object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        IManufacturer manufacturer = object.getManufacturer();

        final ILDeviceModel deviceModel = LDeviceModel.getNewInstance(object.getId(), object.getVersion(), null, object
                .getName());

        if (manufacturer != null)
        {
            deviceModel.setManufacturerId(manufacturer.getId());
            deviceModel.setManufacturerName(manufacturer.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, deviceModel, context);
        return deviceModel;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILDeviceModel light, final IDeviceModel object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILDeviceModel, IDeviceModel> updater = new TranslatorUtil<ILDeviceModel, IDeviceModel>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("manufacturer").fromField("manufacturerId").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILDeviceModel light, IDeviceModel object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid device model name");
        }

        return resp;
    }
}
