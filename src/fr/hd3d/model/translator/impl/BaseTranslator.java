package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public abstract class BaseTranslator<L extends ILBase, P extends IBase> implements IBaseTranslator<L, P>
{
    public static String ERRMSG_0 = "{0} with id={1} does not exist";
    public static String ERRMSG_1 = "{0} is not a valid entity!";

    public List<P> fromLightweightCollection(Collection<L> lights, ResourceContext<?, ?> context) throws Hd3dException
    {
        List<P> ret = new ArrayList<P>();

        CollectUtils.removeNull(lights);

        if (CollectUtils.isNotEmpty(lights))
        {
            for (L light : lights)
            {
                ret.add(fromLightweight(light, context));
            }
        }

        return ret;
    }

    public List<L> toLightweightCollection(Collection<P> objects, ResourceContext<?, ?> context) throws Hd3dException
    {
        List<L> ret = new ArrayList<L>();

        CollectUtils.removeNull(objects);

        if (CollectUtils.isNotEmpty(objects))
        {
            for (P object : objects)
            {
                ret.add(toLightweight(object, context));
            }
        }

        return ret;
    }

    public void updateFromLightweightCollection(final List<L> lights, final List<P> objects,
            ResourceContext<?, ?> context) throws Hd3dException
    {
        CollectUtils.removeNull(lights);
        CollectUtils.removeNull(objects);

        if (CollectUtils.isNotEmpty(lights) && CollectUtils.isNotEmpty(objects))
        {
            if (lights.size() != objects.size())
            {
                throw new Hd3dException(
                        "updating entities: lights objects collection does not have the same size than entities collection.");
            }

            for (int i = 0; i < objects.size(); i++)
            {
                updateFromLightweight(lights.get(i), objects.get(i), context);
            }
        }
    }

    public static String trim(final String s)
    {
        return StringUtils.trim(s);
    }

    protected class ValidationResponse
    {
        private boolean ok = true;
        private StringBuilder err = new StringBuilder();

        public void logErr(String s)
        {
            ok = false;
            this.err.append(s == null ? "null" : s).append(SystemUtils.LINE_SEPARATOR);
        }

        public String error()
        {
            return this.err.toString();
        }

        public boolean isOk()
        {
            return ok;
        }

        public boolean isNotOk()
        {
            return !isOk();
        }
    };

    /**
     * Validate the lightweight object before updating or creating the persisted object. To be overridden for class
     * dependent validation
     * 
     * @param light
     *            the lightweight to validate.
     * @return true when the lightweight object is valid.
     * @throws Hd3dTranslationException
     */
    protected ValidationResponse validateLightWeight(final L light, final P object) throws Hd3dTranslationException
    {
        ValidationResponse resp = new ValidationResponse();

        if (light == null)
        {
            resp.ok = false;
            resp.logErr("lightweight object is null");
            throw new Hd3dTranslationException(resp.error());
        }

        /* Note: cannot check for special character because foreign acceptable character may be rejected */
        if (light instanceof fr.hd3d.model.lightweight.Hook)
        {
            String hook = ((fr.hd3d.model.lightweight.Hook) light).getHook();
            if (hook != null && !fr.hd3d.utils.StringUtils.isProperHook(hook))
            {
                resp.ok = false;
                resp.logErr("lightweight hook contains special characters.");
                throw new Hd3dTranslationException(resp.error());
            }
        }
        return resp;
    }

    protected void validate(final L light, final P object) throws Hd3dTranslationException
    {
        ValidationResponse resp = validateLightWeight(light, object);
        if (resp.isNotOk())
        {
            throw new Hd3dTranslationException(resp.error());
        }
    }

    public static Long nullSafeId(final IBase object)
    {
        return CollectUtils.nullSafeId(object);
    }
}
