package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILQualification;
import fr.hd3d.model.lightweight.impl.LQualification;
import fr.hd3d.model.persistence.IQualification;
import fr.hd3d.model.persistence.ISkill;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.QualificationH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted qualification object to Lightweight qualification object and vice-versa. It also handles update
 * from lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class QualificationTranslator extends BaseTranslator<ILQualification, IQualification>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IQualification fromLightweight(ILQualification light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ArrayList<ISkill> skills = new ArrayList<ISkill>();
        final List<Long> skillIDs = light.getSkillIDs();
        CollectUtils.removeNull(skillIDs);
        if (BaseH.areValidIds(skillIDs))
        {
            skills.ensureCapacity(skillIDs.size());
            skills = (ArrayList<ISkill>) Persistors.skill.getByIds(null, skillIDs);
        }
        else
            skills = new ArrayList<ISkill>();

        IQualification qualification = new QualificationH(trim(light.getName()), new HashSet<ISkill>(skills));

        TranslatorUtil.fromLightWeightCommonFields(qualification, light, context);
        return qualification;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILQualification toLightweight(IQualification object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILQualification qualification = LQualification.getNewInstance(object.getId(), object.getVersion(), object
                .getName());

        for (ISkill skill : object.getSkills())
        {
            qualification.addSkillID(skill.getId());
            qualification.addSkillName(skill.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, qualification, context);
        return qualification;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILQualification light, final IQualification object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILQualification, IQualification> updater = new TranslatorUtil<ILQualification, IQualification>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("skills").fromField("skillIDs").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILQualification light, IQualification object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid qualification name");
        }

        return resp;
    }
}
