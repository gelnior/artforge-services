package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILDynMetaDataValue;
import fr.hd3d.model.lightweight.impl.LDynMetaDataValue;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataValueH;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class DynMetaDataValueTranslator extends BaseTranslator<ILDynMetaDataValue, IDynMetaDataValue>
{
    // TOD actually, this method is useless because it is not allowed to create directly a DynMetaDataValue
    public IDynMetaDataValue fromLightweight(ILDynMetaDataValue light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        IDynMetaDataValue dynMetaDataValue = new DynMetaDataValueH();

        if (BaseH.isValidId(light.getClassDynMetaDataType()))
            dynMetaDataValue.setClassDynMetaDataType(Persistors.classdynmetadatatype.getById(light
                    .getClassDynMetaDataType()));

        final String parentType = trim(light.getParentType());
        dynMetaDataValue.setParentType(parentType);
        IBase parent = (IBase) Persistors.getPersistor(parentType).getById(light.getParent());
        dynMetaDataValue.setParent(nullSafeId(parent));
        if (light.getValue() != null)
        {
            dynMetaDataValue.setValue(light.getValue().toString());
        }

        return dynMetaDataValue;
    }

    public ILDynMetaDataValue toLightweight(IDynMetaDataValue object, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        if (object == null)
            return null;

        LDynMetaDataValue dynMetaDataValue = LDynMetaDataValue.getNewInstance(object.getId(), object.getVersion(),
                null, nullSafeId(object.getClassDynMetaDataType()), object.getParent(), object.getParentType(),
                getProperValue(object, context), object.getDynMetaDataType_Type());

        IClassDynMetaDataType type = object.getClassDynMetaDataType();
        if (type != null)
            dynMetaDataValue.setClassDynMetaDataTypeName(type.getName());

        return dynMetaDataValue;
    }

    @SuppressWarnings("unchecked")
    private <P extends IBase> Object getProperValue(IDynMetaDataValue object, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        if (object == null)
            return null;

        if (!object.isEntity())
            return object.getValue();

        final String type = object.getDynMetaDataType_Type();
        Class<P> interfaceClass = (Class<P>) EntitiesMaps.withEntityName(type).getInterface();

        if (interfaceClass == null)
            return object.getValue();

        return Translators.getTranslator(type).toLightweight((P) interfaceClass.cast(object.getObjectValue()), context);
    }

    public void updateFromLightweight(final ILDynMetaDataValue light, final IDynMetaDataValue object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILDynMetaDataValue, IDynMetaDataValue> updater = new TranslatorUtil<ILDynMetaDataValue, IDynMetaDataValue>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        /* do NOT allow other changes except value */
        updater.acceptNull().updateField("value").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILDynMetaDataValue light, IDynMetaDataValue object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getParentType() != null)
            if (StringUtils.isBlank(light.getParentType()))
                resp.logErr("Invalid parent type name");

        if (light.getClassDynMetaDataType() == null)
            resp.logErr("Invalid classdynmetadatatype");

        if (light.getClassDynMetaDataType() != null)
            if (BaseH.isNotValidId(light.getClassDynMetaDataType()))
                resp.logErr("Invalid classdynmetadatatype");
        return resp;
    }
}
