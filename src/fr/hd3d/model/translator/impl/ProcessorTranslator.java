package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILProcessor;
import fr.hd3d.model.lightweight.impl.LProcessor;
import fr.hd3d.model.persistence.IProcessor;
import fr.hd3d.model.persistence.impl.hibernate.ProcessorH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted device object to Lightweight device object and vice-versa. It also handles update from
 * lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class ProcessorTranslator extends BaseTranslator<ILProcessor, IProcessor>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IProcessor fromLightweight(ILProcessor light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IProcessor processor = new ProcessorH(trim(light.getName()));

        TranslatorUtil.fromLightWeightCommonFields(processor, light, context);
        return processor;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILProcessor toLightweight(IProcessor object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILProcessor processor = LProcessor.getNewInstance(object.getId(), object.getVersion(), null, object
                .getName());

        TranslatorUtil.toLightWeightCommonFields(object, processor, context);
        return processor;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILProcessor light, final IProcessor object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILProcessor, IProcessor> updater = new TranslatorUtil<ILProcessor, IProcessor>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILProcessor light, IProcessor object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid processor name");
        }

        return resp;
    }
}
