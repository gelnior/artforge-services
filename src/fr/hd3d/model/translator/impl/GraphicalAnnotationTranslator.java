package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILGraphicalAnnotation;
import fr.hd3d.model.lightweight.impl.LGraphicalAnnotation;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.impl.hibernate.GraphicalAnnotationH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


public class GraphicalAnnotationTranslator extends BaseTranslator<ILGraphicalAnnotation, IGraphicalAnnotation>
{
    public ILGraphicalAnnotation toLightweight(IGraphicalAnnotation object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILGraphicalAnnotation annotation = new LGraphicalAnnotation();
        annotation.setAnnotationSvg(object.getAnnotationSvg());
        annotation.setMarkIn(object.getMarkIn());
        annotation.setMarkOut(object.getMarkOut());
        annotation.setComment(object.getComment());
        annotation.setAuthor(CollectUtils.nullSafeId(object.getAuthor()));
        annotation.setProxy(CollectUtils.nullSafeId(object.getProxy()));
        annotation.setApprovalNote(CollectUtils.nullSafeId(object.getApprovalNote()));
        annotation.setDate(object.getDate());

        TranslatorUtil.toLightWeightCommonFields(object, annotation, context);
        return annotation;
    }

    public IGraphicalAnnotation fromLightweight(ILGraphicalAnnotation light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        IGraphicalAnnotation note = new GraphicalAnnotationH();

        updateFields(new TranslatorUtil<ILGraphicalAnnotation, IGraphicalAnnotation>(this).withObject(note)
                .withLightObject(light));

        TranslatorUtil.fromLightWeightCommonFields(note, light, context);
        return note;
    }

    private void updateFields(final TranslatorUtil<ILGraphicalAnnotation, IGraphicalAnnotation> updater)
            throws Hd3dException
    {
        updater.updateField("annotationSvg").update();

        updater.updateField("markIn").update();

        updater.updateField("markOut").update();

        updater.updateField("comment").update();

        updater.updateField("author").update();

        updater.updateField("proxy").update();

        updater.updateField("date").update();

        updater.updateField("approvalNote").update();
    }

    public void updateFromLightweight(final ILGraphicalAnnotation light, final IGraphicalAnnotation object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILGraphicalAnnotation, IGraphicalAnnotation> updater = new TranslatorUtil<ILGraphicalAnnotation, IGraphicalAnnotation>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updateFields(updater);
    }

    @Override
    public ValidationResponse validateLightWeight(ILGraphicalAnnotation light, IGraphicalAnnotation object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (StringUtils.isBlank(light.getAnnotationSvg()))
        {
            resp.logErr("Invalid annotation svg");
        }

        if (light.getProxy() == null)
        {
            resp.logErr("Invalid proxy");
        }

        return resp;
    }
}
