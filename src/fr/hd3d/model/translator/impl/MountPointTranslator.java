package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILMountPoint;
import fr.hd3d.model.lightweight.impl.LMountPoint;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.impl.hibernate.MountPointH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class MountPointTranslator extends BaseTranslator<ILMountPoint, IMountPoint>
{
    public ILMountPoint toLightweight(IMountPoint object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        ILMountPoint mountPoint = LMountPoint.getNewInstance(object.getId(), object.getVersion(), object.getName(),
                object.getOperatingSystem(), object.getMountPointPath());

        TranslatorUtil.toLightWeightCommonFields(object, mountPoint, context);
        return mountPoint;
    }

    public IMountPoint fromLightweight(ILMountPoint light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IMountPoint mountPoint = new MountPointH();

        updateFields(new TranslatorUtil<ILMountPoint, IMountPoint>(this).withObject(mountPoint).withLightObject(light));

        TranslatorUtil.fromLightWeightCommonFields(mountPoint, light, context);
        return mountPoint;
    }

    private void updateFields(final TranslatorUtil<ILMountPoint, IMountPoint> updater) throws Hd3dException
    {
        updater.updateField("name").fromField("storageName").update();

        updater.updateField("operatingSystem").update();

        updater.updateField("mountPointPath").update();

    }

    public void updateFromLightweight(final ILMountPoint light, final IMountPoint object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILMountPoint, IMountPoint> updater = new TranslatorUtil<ILMountPoint, IMountPoint>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updateFields(updater);
    }

    @Override
    public ValidationResponse validateLightWeight(ILMountPoint light, IMountPoint object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        return resp;
    }

}
