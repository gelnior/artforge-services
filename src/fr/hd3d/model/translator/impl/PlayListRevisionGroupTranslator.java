package fr.hd3d.model.translator.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILPlayListRevisionGroup;
import fr.hd3d.model.lightweight.impl.LPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.PlayListRevisionGroupH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author HD3D
 */
public class PlayListRevisionGroupTranslator extends BaseTranslator<ILPlayListRevisionGroup, IPlayListRevisionGroup>
{
    public IPlayListRevisionGroup fromLightweight(ILPlayListRevisionGroup light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        IPlayListRevisionGroup playListRevisionGroup = new PlayListRevisionGroupH();
        playListRevisionGroup.setName(trim(light.getName()));
        if (BaseH.isValidId(light.getProject()))
        {
            playListRevisionGroup.setProject(Persistors.project.getById(light.getProject()));
        }
        if (BaseH.isValidId(light.getParent()))
        {
            playListRevisionGroup.setParent(Persistors.playlistrevisiongroup.getById(light.getParent()));
        }

        final List<Long> playListRevisionIds = light.getPlayListRevisions();
        CollectUtils.removeNull(playListRevisionIds);
        if (BaseH.areValidIds(playListRevisionIds))
        {
            playListRevisionGroup.setPlayListRevisions(Persistors.playlistrevision.getByIds(null, playListRevisionIds));
        }

        TranslatorUtil.fromLightWeightCommonFields(playListRevisionGroup, light, context);
        return playListRevisionGroup;
    }

    public ILPlayListRevisionGroup toLightweight(IPlayListRevisionGroup object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILPlayListRevisionGroup playListRevisionGroup = new LPlayListRevisionGroup(object.getId(), object.getVersion(),
                object.getName(), nullSafeId(object.getProject()), nullSafeId(object.getParent()),
                CollectUtils.getIds(object.getChildren()), CollectUtils.getIds(object.getPlayListRevisions()));

        TranslatorUtil.toLightWeightCommonFields(object, playListRevisionGroup, context);
        return playListRevisionGroup;
    }

    public void updateFromLightweight(final ILPlayListRevisionGroup light, final IPlayListRevisionGroup object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILPlayListRevisionGroup, IPlayListRevisionGroup> updater = new TranslatorUtil<ILPlayListRevisionGroup, IPlayListRevisionGroup>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("project").update();

        updater.updateField("parent").update();

        updater.updateField("playListRevisions").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILPlayListRevisionGroup light, IPlayListRevisionGroup object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid resource group name");

        if (light.getId() != null)
            if (light.getId().equals(light.getParent()))
                resp.logErr("the playlistrevision group parent cannot be itself");

        if (object != null)
        {
            if (CollectUtils.getIds(object.allChildren(true)).contains(light.getParent()))
                resp.logErr("the playlistrevision group parent cannot be one of its children");
        }

        return resp;
    }
}
