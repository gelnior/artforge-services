/**
 * 
 */
package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILMileStone;
import fr.hd3d.model.lightweight.impl.LMileStone;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.MileStoneH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author thomas-eskenazi
 * 
 */
public class MileStoneTranslator extends BaseTranslator<ILMileStone, IMileStone>
{

    public IMileStone fromLightweight(ILMileStone light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);
        final IMileStone mileStone = new MileStoneH();
        mileStone.setDate(light.getDate());
        mileStone.setDescription(trim(light.getDescription()));
        mileStone.setTitle(trim(light.getTitle()));
        mileStone.setColor(trim(light.getColor()));
        if (BaseH.isValidId(light.getPlanningID()))
        {
            IPlanning planning = Persistors.planning.getById(light.getPlanningID());
            if (planning != null)
            {
                mileStone.setPlanning(planning);
            }
        }
        TranslatorUtil.fromLightWeightCommonFields(mileStone, light, context);
        return mileStone;
    }

    public ILMileStone toLightweight(IMileStone object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILMileStone mileStone = new LMileStone();
        mileStone.setId(object.getId());
        mileStone.setDate(object.getDate());
        mileStone.setDescription(object.getDescription());
        mileStone.setTitle(object.getTitle());
        mileStone.setColor(object.getColor());
        if (object.getPlanning() != null)
        {
            mileStone.setPlanningID(object.getPlanning().getId());
        }
        TranslatorUtil.toLightWeightCommonFields(object, mileStone, context);
        return mileStone;
    }

    public void updateFromLightweight(final ILMileStone light, final IMileStone object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILMileStone, IMileStone> updater = new TranslatorUtil<ILMileStone, IMileStone>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("title").update();

        updater.updateField("description").update();

        updater.updateField("color").update();

        updater.updateField("date").update();

        updater.updateField("planning").fromField("planningID").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILMileStone light, IMileStone object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getTitle() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getTitle()))
                resp.logErr("Invalid title");

        if (light.getDate() == null)
            resp.logErr("Invalid date");

        return resp;
    }

}
