package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILManufacturer;
import fr.hd3d.model.lightweight.impl.LManufacturer;
import fr.hd3d.model.persistence.IManufacturer;
import fr.hd3d.model.persistence.impl.hibernate.ManufacturerH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted device object to Lightweight device object and vice-versa. It also handles update from
 * lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class ManufacturerTranslator extends BaseTranslator<ILManufacturer, IManufacturer>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IManufacturer fromLightweight(ILManufacturer light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IManufacturer manufacturer = new ManufacturerH(trim(light.getName()));

        TranslatorUtil.fromLightWeightCommonFields(manufacturer, light, context);
        return manufacturer;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILManufacturer toLightweight(IManufacturer object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILManufacturer manufacturer = LManufacturer.getNewInstance(object.getId(), object.getVersion(), null,
                object.getName());

        TranslatorUtil.toLightWeightCommonFields(object, manufacturer, context);
        return manufacturer;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILManufacturer light, final IManufacturer object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILManufacturer, IManufacturer> updater = new TranslatorUtil<ILManufacturer, IManufacturer>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILManufacturer light, IManufacturer object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid manufacturer name");
        }

        return resp;
    }
}
