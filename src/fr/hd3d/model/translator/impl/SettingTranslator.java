package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.impl.LSetting;
import fr.hd3d.model.preferences.Setting;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;


/**
 * Transform Object in their Lightweight form and vice-versa. Also handle database update from Lightweight form.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class SettingTranslator implements IBaseTranslator<LSetting, Setting>
{
    public static String trim(String s)
    {
        return StringUtils.trim(s);
    }

    public Setting fromLightweight(LSetting light, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (light == null)
            return null;

        Setting setting = new Setting(trim(light.getLogin()), light.getPerson(), trim(light.getKey()), trim(light
                .getValue()));

        return setting;
    }

    public LSetting toLightweight(Setting object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        LSetting setting = new LSetting(object.getId(), object.getVersion(), object.getLogin(), object.getPerson(),
                object.getKey(), object.getValue());
        return setting;
    }

    public void updateFromLightweight(final LSetting light, final Setting object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (light == null)
        {
            throw new Hd3dTranslationException("lightweight Setting is null");
        }

        final String login = trim(light.getLogin());
        if (login != null)
            if (!ObjectUtils.equals(object.getLogin(), login))
            {
                object.setLogin(login);
            }

        final Long person = light.getPerson();
        if (person != null)
            if (!ObjectUtils.equals(object.getPerson(), person))
            {
                object.setPerson(person);
            }

        final String key = trim(light.getKey());
        if (key != null)
            if (!ObjectUtils.equals(object.getKey(), key))
            {
                object.setKey(key);
            }

        final String value = trim(light.getValue());
        if (value != null)
            if (!ObjectUtils.equals(object.getValue(), value))
            {
                object.setValue(value);
            }
    }

    public String audit()
    {
        return null;
    }

    public List<Setting> fromLightweightCollection(Collection<LSetting> lights, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        List<Setting> ret = new ArrayList<Setting>();

        CollectUtils.removeNull(lights);

        if (CollectUtils.isNotEmpty(lights))
        {
            for (LSetting light : lights)
            {
                ret.add(fromLightweight(light, context));
            }
        }

        return ret;
    }

    public List<LSetting> toLightweightCollection(Collection<Setting> objects, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        List<LSetting> ret = new ArrayList<LSetting>();

        CollectUtils.removeNull(objects);

        if (CollectUtils.isNotEmpty(objects))
        {
            for (Setting object : objects)
            {
                ret.add(toLightweight(object, context));
            }
        }

        return ret;
    }

    public void updateFromLightweightCollection(List<LSetting> lights, List<Setting> objects,
            ResourceContext<?, ?> context) throws Hd3dException
    {
        CollectUtils.removeNull(lights);
        CollectUtils.removeNull(objects);

        if (CollectUtils.isNotEmpty(lights) && CollectUtils.isNotEmpty(objects))
        {
            if (lights.size() != objects.size())
            {
                throw new Hd3dException(
                        "updating entities: lights objects collection does not have the same size than entities collection.");
            }

            for (int i = 0; i < objects.size(); i++)
            {
                updateFromLightweight(lights.get(i), objects.get(i), context);
            }
        }
    }
}
