package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILTagCategory;
import fr.hd3d.model.lightweight.impl.LTagCategory;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.TagCategoryH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted TagCategory object to lightweight organization object and vice-versa. It also handles update
 * from lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class TagCategoryTranslator extends BaseTranslator<ILTagCategory, ITagCategory>
{
    public ITagCategory fromLightweight(ILTagCategory light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        // find out bound tags
        ArrayList<ITag> boundTags = new ArrayList<ITag>();
        final Collection<Long> lBoundTags = light.getBoundTags();
        CollectUtils.removeNull(lBoundTags);
        if (BaseH.areValidIds(lBoundTags))
        {
            boundTags.ensureCapacity(lBoundTags.size());
            boundTags = (ArrayList<ITag>) Persistors.tag.getByIds(null, lBoundTags);
        }
        else
            boundTags = new ArrayList<ITag>();

        // find out tag category parent
        ITagCategory tagCategoryParent = null;
        if (BaseH.isValidId(light.getParent()))
            tagCategoryParent = Persistors.tagcategory.getById(light.getParent());

        // build the object
        ITagCategory tagCategory = new TagCategoryH();
        tagCategory.setName(trim(light.getName()));
        tagCategory.setBoundTags(boundTags);
        tagCategory.setParent(tagCategoryParent);

        // Note: do not handle setChildren. The lightWeight setChildren is just for display purpose

        TranslatorUtil.fromLightWeightCommonFields(tagCategory, light, context);
        return tagCategory;
    }

    public ILTagCategory toLightweight(ITagCategory object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILTagCategory tagCategory = LTagCategory.getNewInstance(object.getId(), object.getVersion(), null, object
                .getName(), nullSafeId(object.getParent()), CollectUtils.getIds(object.getChildren()), CollectUtils
                .getIds(object.getBoundTags()));

        TranslatorUtil.toLightWeightCommonFields(object, tagCategory, context);
        return tagCategory;
    }

    public void updateFromLightweight(final ILTagCategory light, final ITagCategory object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILTagCategory, ITagCategory> updater = new TranslatorUtil<ILTagCategory, ITagCategory>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.acceptNull().updateField("parent").update();

        updater.acceptNull().updateField("boundTags").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILTagCategory light, ITagCategory object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid tag category name");

        if (light.getId() != null)
            if (light.getId().equals(light.getParent()))
                resp.logErr("the tag category parent cannot be itself");

        if (object != null)
        {
            if (CollectUtils.getIds(object.allChildren(true)).contains(light.getParent()))
                resp.logErr("the tag category parent cannot be one of its children");
        }

        return resp;
    }
}
