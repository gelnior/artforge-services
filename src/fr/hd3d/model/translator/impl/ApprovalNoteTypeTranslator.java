package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILApprovalNoteType;
import fr.hd3d.model.lightweight.impl.LApprovalNoteType;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteTypeH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class ApprovalNoteTypeTranslator extends BaseTranslator<ILApprovalNoteType, IApprovalNoteType>
{
    public ILApprovalNoteType toLightweight(IApprovalNoteType object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        LApprovalNoteType note = LApprovalNoteType.getNewInstance(object.getId(), object.getVersion(), null, object
                .getName(), nullSafeId(object.getTaskType()), nullSafeId(object.getProject()));
        ITaskType taskType = object.getTaskType();
        if (taskType != null)
        {
            note.setTaskTypeName(taskType.getName());
            note.setTaskTypeColor(taskType.getColor());
        }
        TranslatorUtil.toLightWeightCommonFields(object, note, context);
        return note;
    }

    public IApprovalNoteType fromLightweight(ILApprovalNoteType light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        IApprovalNoteType noteType = new ApprovalNoteTypeH();

        updateFields(new TranslatorUtil<ILApprovalNoteType, IApprovalNoteType>(this).withObject(noteType)
                .withLightObject(light));

        TranslatorUtil.fromLightWeightCommonFields(noteType, light, context);
        return noteType;
    }

    private void updateFields(final TranslatorUtil<ILApprovalNoteType, IApprovalNoteType> updater) throws Hd3dException
    {
        updater.updateField("name").update();

        updater.updateField("taskType").acceptNull().update();

        updater.updateField("project").update();
    }

    public void updateFromLightweight(final ILApprovalNoteType light, final IApprovalNoteType object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILApprovalNoteType, IApprovalNoteType> updater = new TranslatorUtil<ILApprovalNoteType, IApprovalNoteType>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updateFields(updater);
    }

    @Override
    public ValidationResponse validateLightWeight(ILApprovalNoteType light, IApprovalNoteType object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() == null)
            resp.logErr("Invalid approvalnoteType name");

        return resp;
    }

}
