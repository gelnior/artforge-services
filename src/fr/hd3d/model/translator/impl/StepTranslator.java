package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILStep;
import fr.hd3d.model.lightweight.impl.LStep;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.StepH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class StepTranslator extends BaseTranslator<ILStep, IStep>
{
    public IStep fromLightweight(ILStep light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IStep step = new StepH();

        step.setBoundEntity(light.getBoundEntity());
        step.setBoundEntityName(trim(light.getBoundEntityName()));
        if (BaseH.isValidId(light.getTaskType()))
            step.setTaskType(Persistors.tasktype.getById(light.getTaskType()));
        step.setCreateTasknAsset(light.getCreateTasknAsset());
        step.setEstimatedDuration(light.getEstimatedDuration());

        TranslatorUtil.fromLightWeightCommonFields(step, light, context);
        return step;
    }

    public ILStep toLightweight(IStep object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ITaskType taskType = object.getTaskType();
        ILStep note = LStep.getNewInstance(object.getId(), object.getVersion(), null, object.getBoundEntity(), object
                .getBoundEntityName(), nullSafeId(object.getTaskType()),
                (taskType == null ? null : taskType.getName()), object.getCreateTasknAsset(), object
                        .getEstimatedDuration());

        TranslatorUtil.toLightWeightCommonFields(object, note, context);
        return note;
    }

    public void updateFromLightweight(final ILStep light, final IStep object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILStep, IStep> updater = new TranslatorUtil<ILStep, IStep>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("boundEntity").update();

        updater.updateField("boundEntityName").update();

        updater.updateField("taskType").update();

        updater.updateField("createTasknAsset").update();

        updater.updateField("estimatedDuration").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILStep light, IStep object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getBoundEntity() == null)
            resp.logErr("Invalid bound entity");

        if (light.getBoundEntityName() == null)
            resp.logErr("Invalid bound entity name");

        if (light.getTaskType() == null)
            resp.logErr("Invalid task type");

        return resp;
    }

}
