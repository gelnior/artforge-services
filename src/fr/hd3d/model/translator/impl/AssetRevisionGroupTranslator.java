package fr.hd3d.model.translator.impl;

import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILAssetRevisionGroup;
import fr.hd3d.model.lightweight.impl.LAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


public class AssetRevisionGroupTranslator extends BaseTranslator<ILAssetRevisionGroup, IAssetRevisionGroup>
{

    public IAssetRevisionGroup fromLightweight(ILAssetRevisionGroup light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        IAssetRevisionGroup assetgroup = new AssetRevisionGroupH();

        assetgroup.setName(trim(light.getName()));

        List<Long> assetRevisions = light.getAssetRevisions();
        CollectUtils.removeNull(assetRevisions);

        if (BaseH.areValidIds(assetRevisions))
            assetgroup.setAssetRevisions(new HashSet<IAssetRevision>(Persistors.assetrevision.getByIds(null,
                    assetRevisions)));

        assetgroup.setCriteria(trim(light.getCriteria()));
        assetgroup.setValue(trim(light.getValue()));

        if (BaseH.isValidId(light.getProject()))
            assetgroup.setProject(Persistors.project.getById(light.getProject()));

        TranslatorUtil.fromLightWeightCommonFields(assetgroup, light, context);
        return assetgroup;
    }

    public ILAssetRevisionGroup toLightweight(IAssetRevisionGroup object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILAssetRevisionGroup assetgroup = LAssetRevisionGroup.getNewInstance(object.getId(), object.getVersion(), null,
                object.getName(), CollectUtils.getIds(object.getAssetRevisions()), object.getCriteria(), object
                        .getValue(), nullSafeId(object.getProject()));

        TranslatorUtil.toLightWeightCommonFields(object, assetgroup, context);
        return assetgroup;
    }

    public void updateFromLightweight(final ILAssetRevisionGroup light, final IAssetRevisionGroup object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILAssetRevisionGroup, IAssetRevisionGroup> updater = new TranslatorUtil<ILAssetRevisionGroup, IAssetRevisionGroup>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        if (light.getAssetRevisions() != null)
        {
            object.merge(object.getAssetRevisions(), light.getAssetRevisions(), IAssetRevision.class);
        }

        updater.updateField("criteria").update();

        updater.updateField("value").update();

        updater.updateField("project").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILAssetRevisionGroup light, IAssetRevisionGroup object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid asset group name");

        return resp;
    }
}
