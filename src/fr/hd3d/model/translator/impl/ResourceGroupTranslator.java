package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.lightweight.impl.LResourceGroup;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IResource;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ResourceGroupH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted resource group object to lightweight resource group object and vice-versa. It also handles
 * update from lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class ResourceGroupTranslator extends BaseTranslator<ILResourceGroup, IResourceGroup>
{
    public IResourceGroup fromLightweight(ILResourceGroup light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ArrayList<IResource> resources = new ArrayList<IResource>();
        final List<Long> resourceIds = light.getResourceIDs();
        CollectUtils.removeNull(resourceIds);
        if (BaseH.areValidIds(resourceIds))
        {
            resources.ensureCapacity(resourceIds.size());
            resources = (ArrayList<IResource>) Persistors.resource.getByIds(null, resourceIds);
        }
        else
            resources = new ArrayList<IResource>();

        ArrayList<IProject> projects = new ArrayList<IProject>();
        final List<Long> projectIds = light.getProjectIDs();
        CollectUtils.removeNull(projectIds);
        if (BaseH.areValidIds(projectIds))
        {
            projects.ensureCapacity(projectIds.size());
            projects = (ArrayList<IProject>) Persistors.project.getByIds(null, projectIds);
        }
        else
            projects = new ArrayList<IProject>();

        ArrayList<IRole> roles = new ArrayList<IRole>();
        final List<Long> roleIds = light.getRoleIDs();
        CollectUtils.removeNull(roleIds);
        if (BaseH.areValidIds(roleIds))
        {
            roles.ensureCapacity(roleIds.size());
            roles = (ArrayList<IRole>) Persistors.role.getByIds(null, roleIds);
        }
        else
            roles = new ArrayList<IRole>();

        IPerson leader = null;
        if (BaseH.isValidId(light.getLeaderId()))
        {
            leader = Persistors.person.getById(light.getLeaderId());
        }

        IResourceGroup resourceGroup = new ResourceGroupH();
        resourceGroup.setName(trim(light.getName()));
        resourceGroup.setResources(new HashSet<IResource>(resources));
        resourceGroup.setProjects(new HashSet<IProject>(projects));
        resourceGroup.setRoles(new HashSet<IRole>(roles));
        resourceGroup.setLeader(leader);

        Long parentId = resolveParentId(light);

        IResourceGroup resourceGroupParent = null;
        if (BaseH.isValidId(parentId))
        {
            resourceGroupParent = Persistors.resourcegroup.getById(parentId);
        }
        resourceGroup.setParent(resourceGroupParent);

        TranslatorUtil.fromLightWeightCommonFields(resourceGroup, light, context);
        return resourceGroup;
    }

    public ILResourceGroup toLightweight(IResourceGroup object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        IPerson leader = object.getLeader();

        ILResourceGroup resourceGroup = LResourceGroup.getNewInstance(object.getId(), object.getVersion(), null, object
                .getName(), resolveParentId(object));

        if (leader != null)
        {
            resourceGroup.setLeaderId(leader.getId());
            resourceGroup.setLeaderName(leader.getResourceName());
        }

        for (IResource resource : object.getResources())
        {
            resourceGroup.addResourceID(resource.getId());
            resourceGroup.addResourceName(resource.getResourceName());
        }

        for (IProject project : object.getProjects())
        {
            resourceGroup.addProjectID(project.getId());
            resourceGroup.addProjectName(project.getName());
        }

        for (IRole role : object.getRoles())
        {
            resourceGroup.addRoleID(role.getId());
            resourceGroup.addRoleName(role.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, resourceGroup, context);
        return resourceGroup;
    }

    private Long resolveParentId(ILResourceGroup lResourceGroup)
    {
        if (lResourceGroup == null || lResourceGroup.getParent() == null)
            return null;

        return lResourceGroup.getParent();
    }

    private Long resolveParentId(IResourceGroup resourceGroup)
    {
        if (resourceGroup == null || resourceGroup.getParent() == null)
        {
            return null;
        }

        return resourceGroup.getParent().getId();
    }

    public void updateFromLightweight(final ILResourceGroup light, final IResourceGroup object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILResourceGroup, IResourceGroup> updater = new TranslatorUtil<ILResourceGroup, IResourceGroup>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("resources").fromField("resourceIDs").update();

        updater.updateField("projects").fromField("projectIDs").update();

        updater.updateField("roles").fromField("roleIDs").update();

        updater.updateField("leader").fromField("leaderId").update();

        /*
         * Note: it is important to set the parent at the end because "resources", "projects", "roles" will inherit
         * parent's ones.
         */
        final Long parent = light.getParent();
        if (parent != null)
        {
            if (!ObjectUtils.equals(nullSafeId(object.getParent()), parent))
            {
                if (Const.ID_NULL.equals(parent))
                {
                    object.setParent(null);
                }
                else
                {
                    object.setParent(Persistors.resourcegroup.getById(parent));
                }
            }
        }

    }

    @Override
    public ValidationResponse validateLightWeight(ILResourceGroup light, IResourceGroup object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid resource group name");

        if (light.getId() != null)
            if (light.getId().equals(light.getParent()))
                resp.logErr("the resource group parent cannot be itself");

        if (object != null)
        {
            if (CollectUtils.getIds(object.allChildren(true)).contains(light.getParent()))
                resp.logErr("the resource group parent cannot be one of its children");
        }

        return resp;
    }
}
