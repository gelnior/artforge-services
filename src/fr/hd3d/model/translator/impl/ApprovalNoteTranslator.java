package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.lightweight.impl.LApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.TranslatorUtil;


public class ApprovalNoteTranslator extends BaseTranslator<ILApprovalNote, IApprovalNote>
{
    public ILApprovalNote toLightweight(IApprovalNote object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILApprovalNote note = LApprovalNote.getNewInstance(object.getId(), object.getVersion(), null, object
                .getApprovalDate(), object.getStatus(), object.getComment(), nullSafeId(object.getApprover()), object
                .getBoundEntity(), object.getBoundEntityName(), object.getType(), nullSafeId(object
                .getApprovalNoteType()), object.getMedia());

        if (object.getApprover() != null)
        {
            note.setApproverName(object.getApprover().fullName());
        }
        if (object.getApprovalNoteType() != null)
        {
            note.setApprovalNoteTypeName(object.getApprovalNoteType().getName());
        }

        if (object.getApprovalNoteType() != null && object.getApprovalNoteType().getTaskType() != null)
        {
            final ITaskType taskType = object.getApprovalNoteType().getTaskType();
            note.setTaskTypeId(taskType.getId());
            note.setTaskTypeName(taskType.getName());
        }

        for (IFileRevision fileRevision : object.getFilerevisions())
        {
            note.getFilerevisions().add(fileRevision.getId());
        }
        if (Conf.isGraphicalAnnotationOn())
        {
            for (IGraphicalAnnotation graphicalAnnotation : object.getGraphicalAnnotations())
            {
                note.getGraphicalAnnotationIDs().add(graphicalAnnotation.getId());
            }
        }

        TranslatorUtil.toLightWeightCommonFields(object, note, context);
        return note;
    }

    public IApprovalNote fromLightweight(ILApprovalNote light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IApprovalNote note = new ApprovalNoteH();

        updateFields(new TranslatorUtil<ILApprovalNote, IApprovalNote>(this).withObject(note).withLightObject(light));

        /* Note: taskTypeId and taskTypeName are READ-ONLY */

        TranslatorUtil.fromLightWeightCommonFields(note, light, context);
        return note;
    }

    private void updateFields(final TranslatorUtil<ILApprovalNote, IApprovalNote> updater) throws Hd3dException
    {
        updater.updateField("boundEntity").update();

        updater.updateField("boundEntityName").update();

        updater.updateField("approvalDate").update();

        updater.updateField("status").update();

        updater.updateField("comment").update();

        updater.updateField("approver").update();

        updater.updateField("filerevisions").update();

        updater.updateField("type").update();

        updater.updateField("approvalNoteType").update();

        updater.updateField("media").update();

        if (Conf.isGraphicalAnnotationOn())
        {
            updater.updateField("graphicalAnnotations").fromField("graphicalAnnotationIDs").update();
        }

        /* Note: taskTypeId and taskTypeName are READ-ONLY */
    }

    public void updateFromLightweight(final ILApprovalNote light, final IApprovalNote object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILApprovalNote, IApprovalNote> updater = new TranslatorUtil<ILApprovalNote, IApprovalNote>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updateFields(updater);
    }

    @Override
    public ValidationResponse validateLightWeight(ILApprovalNote light, IApprovalNote object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getApprovalDate() == null)
            resp.logErr("Invalid approval date");

        if (light.getStatus() != null)
            if (StringUtils.isBlank(light.getStatus()))
                resp.logErr("Invalid status");

        return resp;
    }

}
