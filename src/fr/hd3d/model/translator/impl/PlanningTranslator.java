package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.lightweight.impl.LPlanning;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.PlanningH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class PlanningTranslator extends BaseTranslator<ILPlanning, IPlanning>
{

    public IPlanning fromLightweight(ILPlanning light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        final IPlanning planning = new PlanningH();
        planning.setName(trim(light.getName()));
        planning.setMaster(light.getMaster());
        planning.setStartDate(light.getStartDate());
        planning.setEndDate(light.getEndDate());
        IProject project = null;
        if (BaseH.isValidId(light.getProject()))
        {
            project = Persistors.project.getById(light.getProject());
            if (project == null)
                throw new Hd3dTranslationException("Project is null");
            planning.setProject(project);
        }
        TranslatorUtil.fromLightWeightCommonFields(planning, light, context);
        return planning;
    }

    public ILPlanning toLightweight(IPlanning object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final LPlanning planning = new LPlanning();
        planning.setId(object.getId());
        planning.setVersion(object.getVersion());
        planning.setName(object.getName());
        planning.setMaster(object.isMaster());
        planning.setStartDate(object.getStartDate());
        planning.setEndDate(object.getEndDate());
        if (object.getProject() != null)
        {
            planning.setProject(object.getProject().getId());
        }
        TranslatorUtil.toLightWeightCommonFields(object, planning, context);
        return planning;
    }

    public void updateFromLightweight(final ILPlanning light, final IPlanning object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILPlanning, IPlanning> updater = new TranslatorUtil<ILPlanning, IPlanning>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        // TODO, udpater fails dur to boolean getter isXXX
        object.setMaster(light.getMaster());

        updater.updateField("startDate").update();

        updater.updateField("endDate").update();

        updater.updateField("name").update();

        updater.updateField("project").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILPlanning light, IPlanning object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid name");

        return resp;
    }

}
