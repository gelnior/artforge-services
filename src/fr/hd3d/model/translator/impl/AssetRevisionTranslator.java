package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.enums.EAssetStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILAssetRevision;
import fr.hd3d.model.lightweight.impl.LAssetRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


public class AssetRevisionTranslator extends BaseTranslator<ILAssetRevision, IAssetRevision>
{

    public IAssetRevision fromLightweight(ILAssetRevision light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IAssetRevision asset = new AssetRevisionH();
        if (BaseH.isValidId(light.getProject()))
            asset.setProject(Persistors.project.getById(light.getProject()));
        asset.setCreationDate(light.getCreationDate());
        asset.setKey(trim(light.getKey()));
        asset.setVariation(trim(light.getVariation()));
        asset.setRevision(light.getRevision());
        if (BaseH.isValidId(light.getCreator()))
            asset.setCreator(Persistors.person.getById(light.getCreator()));
        asset.setTaskType(Persistors.tasktype.getById(light.getTaskType()));
        asset.setName(trim(light.getName()));
        asset.setStatus(EAssetStatus.valueOf(trim(light.getStatus())));
        asset.setLastOperationDate(light.getLastOperationDate());
        if (BaseH.isValidId(light.getLastUser()))
            asset.setLastUser(Persistors.person.getById(light.getLastUser()));
        asset.setComment(trim(light.getComment()));
        asset.setValidity(trim(light.getValidity()));

        asset.setWipPath(light.getWipPath());
        asset.setPublishPath(light.getPublishPath());
        asset.setOldPath(light.getOldPath());

        Set<Long> assetRevisions = light.getAssetRevisionGroups();
        CollectUtils.removeNull(assetRevisions);
        if (BaseH.areValidIds(assetRevisions))
            asset.setAssetRevisionGroups(new HashSet<IAssetRevisionGroup>(Persistors.assetrevisiongroup.getByIds(null,
                    assetRevisions)));

        Set<Long> fileRevisions = light.getFileRevisions();
        CollectUtils.removeNull(fileRevisions);
        if (BaseH.areValidIds(fileRevisions))
            asset.setFileRevisions(new ArrayList<IFileRevision>(Persistors.filerevision.getByIds(null, fileRevisions)));

        TranslatorUtil.fromLightWeightCommonFields(asset, light, context);
        return asset;
    }

    public ILAssetRevision toLightweight(IAssetRevision object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        final ITaskType taskType = object.getTaskType();
        ILAssetRevision asset = LAssetRevision.getNewInstance(object.getId(), object.getVersion(), null,
                nullSafeId(object.getProject()), object.getCreationDate(), object.getKey(), object.getVariation(),
                object.getRevision(), nullSafeId(object.getCreator()), nullSafeId(object.getTaskType()),
                (taskType == null ? null : taskType.getName()), object.getName(), object.getStatus().toString(), object
                        .getLastOperationDate(), nullSafeId(object.getLastUser()), object.getComment(), object
                        .getValidity(), object.getWipPath(), object.getPublishPath(), object.getOldPath());
        asset.setAssetRevisionGroups(new HashSet<Long>(CollectUtils.getIds(object.getAssetRevisionGroups())));

        if (taskType != null)
            asset.setTaskTypeColor(taskType.getColor());
        if (object.getCreator() != null)
            asset.setCreatorName(object.getCreator().fullName());
        if (object.getLastUser() != null)
            asset.setLastUserName(object.getLastUser().fullName());
        if (object.getMountPointWipPath() != null)
        {
            asset.setMountPointWipPath(object.getMountPointWipPath());
        }
        if (object.getMountPointPublishPath() != null)
        {
            asset.setMountPointPublishPath(object.getMountPointPublishPath());
        }
        if (object.getMountPointOldPath() != null)
        {
            asset.setMountPointOldPath(object.getMountPointOldPath());
        }
        if (CollectionUtils.isNotEmpty(object.getAssetRevisionGroups()))
        {
            IAssetRevisionGroup group = object.getAssetRevisionGroups().iterator().next();
            IBase boundEntity = group.getAssociatedEntity();

            if (IConstituent.class.isInstance(boundEntity))
            {
                final IConstituent constituent = (IConstituent) boundEntity;
                asset.setWorkObjectName(constituent.getLabel());
                asset.setWorkObjectPath(constituent.getPath());
                asset.setWorkObjectId(constituent.getId());
                asset.setBoundEntityName(constituent.entityName());

            }
            else if (IShot.class.isInstance(boundEntity))
            {
                final IShot shot = (IShot) boundEntity;
                asset.setWorkObjectName(shot.getLabel());
                asset.setWorkObjectPath(shot.getPath());
                asset.setWorkObjectId(shot.getId());
                asset.setBoundEntityName(shot.entityName());
            }
        }
        if (CollectionUtils.isNotEmpty(object.getFileRevisions()))
        {
            asset.setFileRevisions(new LinkedHashSet<Long>(CollectUtils.getIds(object.getFileRevisions())));
        }

        TranslatorUtil.toLightWeightCommonFields(object, asset, context);
        return asset;
    }

    public void updateFromLightweight(final ILAssetRevision light, final IAssetRevision object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILAssetRevision, IAssetRevision> updater = new TranslatorUtil<ILAssetRevision, IAssetRevision>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("project").update();

        updater.updateField("creationDate").update();

        updater.updateField("key").update();

        updater.updateField("variation").update();

        updater.updateField("revision").update();

        updater.updateField("creator").update();

        updater.updateField("taskType").update();

        updater.updateField("name").update();

        updater.updateField("status").update();

        updater.updateField("lastOperationDate").update();

        updater.updateField("lastUser").update();

        updater.updateField("comment").update();

        updater.updateField("validity").update();

        updater.acceptNull().updateField("wipPath").update();
        updater.acceptNull().updateField("publishPath").update();
        updater.acceptNull().updateField("oldPath").update();
        updater.acceptNull().updateField("mountPointWipPath").update();
        updater.acceptNull().updateField("mountPointPublishPath").update();
        updater.acceptNull().updateField("mountPointOldPath").update();

        updater.updateField("assetRevisionGroups").update();

        updater.updateField("fileRevisions").acceptNull().update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILAssetRevision light, IAssetRevision object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getProject() != null)
            if (BaseH.isNotValidId(light.getProject()))
                resp.logErr("Invalid project id");

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid asset name");

        if (light.getKey() != null)
            if (StringUtils.isBlank(light.getKey()))
                resp.logErr("Invalid asset key");

        if (light.getTaskType() != null)
            if (BaseH.isNotValidId(light.getTaskType()))
                resp.logErr("Invalid task type");

        if (light.getStatus() != null)
            if (StringUtils.isBlank(light.getStatus()))
                resp.logErr("Invalid asset status");

        if (light.getValidity() != null)
            if (StringUtils.isBlank(light.getValidity()))
                resp.logErr("Invalid asset validity");

        if (light.getCreator() != null)
            if (BaseH.isNotValidId(light.getCreator()))
                resp.logErr("Invalid creator id");

        if (light.getLastUser() != null)
            if (BaseH.isNotValidId(light.getLastUser()))
                resp.logErr("Invalid last user id");

        return resp;
    }

}
