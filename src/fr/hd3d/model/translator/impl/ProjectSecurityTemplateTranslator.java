package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILProjectSecurityTemplate;
import fr.hd3d.model.lightweight.impl.LProjectSecurityTemplate;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.templates.IProjectSecurityTemplate;
import fr.hd3d.services.security.templates.IRoleTemplate;
import fr.hd3d.services.security.templates.ProjectSecurityTemplateH;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


public class ProjectSecurityTemplateTranslator extends
        BaseTranslator<ILProjectSecurityTemplate, IProjectSecurityTemplate>
{

    public IProjectSecurityTemplate fromLightweight(ILProjectSecurityTemplate light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        ArrayList<IRoleTemplate> roles = new ArrayList<IRoleTemplate>();
        final Set<Long> roleIds = new HashSet<Long>(light.getRoleIds());
        CollectUtils.removeNull(roleIds);
        if (BaseH.areValidIds(roleIds))
        {
            roles.ensureCapacity(roleIds.size());
            roles = (ArrayList<IRoleTemplate>) Persistors.roletemplates.getByIds(null, roleIds);
        }
        else
            roles = new ArrayList<IRoleTemplate>();

        IProjectSecurityTemplate template = new ProjectSecurityTemplateH(trim(light.getName()),
                new HashSet<IRoleTemplate>(roles));
        template.setComment(trim(light.getComment()));

        TranslatorUtil.fromLightWeightCommonFields(template, light, context);
        return template;
    }

    public ILProjectSecurityTemplate toLightweight(IProjectSecurityTemplate object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILProjectSecurityTemplate template = LProjectSecurityTemplate.getNewInstance(object.getId(), object
                .getVersion(), null, object.getName(), object.getComment());
        template.setRoleIds(new ArrayList<Long>());
        template.setRoleNames(new ArrayList<String>());
        for (IRoleTemplate role : object.getRoles())
        {
            template.getRoleIds().add(role.getId());
            template.getRoleNames().add(role.getName());
        }
        TranslatorUtil.toLightWeightCommonFields(object, template, context);
        return template;
    }

    public void updateFromLightweight(final ILProjectSecurityTemplate light, final IProjectSecurityTemplate object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        final TranslatorUtil<ILProjectSecurityTemplate, IProjectSecurityTemplate> updater = new TranslatorUtil<ILProjectSecurityTemplate, IProjectSecurityTemplate>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("comment").update();

        updater.updateField("roles").fromField("roleIds").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILProjectSecurityTemplate light, IProjectSecurityTemplate object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid template name");

        return resp;
    }

}
