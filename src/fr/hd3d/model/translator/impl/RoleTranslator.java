package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.lightweight.impl.LRole;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.RoleH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


public class RoleTranslator extends BaseTranslator<ILRole, IRole>
{

    public IRole fromLightweight(ILRole light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ArrayList<IResourceGroup> resourceGroups = new ArrayList<IResourceGroup>();
        final Set<Long> resourceGroupIDs = light.getResourceGroupIDs();
        CollectUtils.removeNull(resourceGroupIDs);
        if (BaseH.areValidIds(resourceGroupIDs))
        {
            resourceGroups.ensureCapacity(resourceGroupIDs.size());
            resourceGroups = (ArrayList<IResourceGroup>) Persistors.resourcegroup.getByIds(null, resourceGroupIDs);
        }
        else
            resourceGroups = new ArrayList<IResourceGroup>();

        List<String> permissions = light.getPermissions();
        if (permissions == null)
            permissions = new ArrayList<String>();

        List<String> bans = light.getBans();
        if (bans == null)
            bans = new ArrayList<String>(0);

        IRole role = new RoleH(trim(light.getName()), new HashSet<IResourceGroup>(resourceGroups), new HashSet<String>(
                permissions), new HashSet<String>(bans));

        TranslatorUtil.fromLightWeightCommonFields(role, light, context);
        return role;
    }

    public ILRole toLightweight(IRole object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILRole role = LRole.getNewInstance(object.getId(), object.getVersion(), null, object.getName());
        for (IResourceGroup resourceGroup : object.getResourceGroups())
        {
            role.addResourceGroupID(resourceGroup.getId());
            role.addResourceGroupName(resourceGroup.getName());
        }
        role.addPermissions(new ArrayList<String>(object.getPermissions()));
        role.addBans(new ArrayList<String>(object.getBans()));
        TranslatorUtil.toLightWeightCommonFields(object, role, context);
        return role;
    }

    public void updateFromLightweight(final ILRole light, final IRole object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        final TranslatorUtil<ILRole, IRole> updater = new TranslatorUtil<ILRole, IRole>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("resourceGroups").fromField("resourceGroupIDs").update();

        // updater.updateField("permissions").update();
        if (light.getPermissions() != null)
        {
            object.setPermissions(new LinkedHashSet<String>(light.getPermissions()));
        }

        // updater.updateField("bans").update();
        if (light.getBans() != null)
        {
            object.setBans(new LinkedHashSet<String>(light.getBans()));
        }
    }

    @Override
    public ValidationResponse validateLightWeight(ILRole light, IRole object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid role name");

        return resp;
    }

}
