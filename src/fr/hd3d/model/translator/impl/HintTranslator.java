package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILHint;
import fr.hd3d.model.lightweight.impl.LHint;
import fr.hd3d.model.persistence.IHint;
import fr.hd3d.model.persistence.impl.hibernate.HintH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class HintTranslator extends BaseTranslator<ILHint, IHint>
{
    public IHint fromLightweight(ILHint light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IHint hint = new HintH(trim(light.getName()), trim(light.getDescription()));
        TranslatorUtil.fromLightWeightCommonFields(hint, light, context);
        return hint;
    }

    public ILHint toLightweight(IHint object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILHint hint = LHint.getNewInstance(object.getId(), object.getVersion(), null, object.getName(), object
                .getDescription());
        TranslatorUtil.toLightWeightCommonFields(object, hint, context);
        return hint;
    }

    public void updateFromLightweight(final ILHint light, final IHint object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILHint, IHint> updater = new TranslatorUtil<ILHint, IHint>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("description").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILHint light, IHint object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid hint name");

        return resp;
    }
}
