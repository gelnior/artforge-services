package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILFact;
import fr.hd3d.model.lightweight.impl.LFact;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IFact;
import fr.hd3d.model.persistence.IFactType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.FactH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class FactTranslator extends BaseTranslator<ILFact, IFact>
{
    public IFact fromLightweight(ILFact light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IFactType facttype = null;
        if (BaseH.isValidId(light.getFactType()))
            facttype = Persistors.facttype.getById(light.getFactType());

        IComposition composition = null;
        if (BaseH.isValidId(light.getComposition()))
            composition = Persistors.composition.getById(light.getComposition());

        IFact fact = new FactH(trim(light.getName()), trim(light.getDescription()), facttype, composition);

        TranslatorUtil.fromLightWeightCommonFields(fact, light, context);
        return fact;
    }

    public ILFact toLightweight(IFact object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        Long facttype = Const.ID_NULL;// no factype
        if (object.getFactType() != null)
        {
            facttype = object.getFactType().getId();
        }
        ILFact fact = LFact.getNewInstance(object.getId(), object.getVersion(), null, object.getName(), object
                .getDescription(), facttype);

        TranslatorUtil.toLightWeightCommonFields(object, fact, context);
        return fact;
    }

    public void updateFromLightweight(final ILFact light, final IFact object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILFact, IFact> updater = new TranslatorUtil<ILFact, IFact>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("description").update();

        updater.updateField("factType").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILFact light, IFact object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid fact name");

        return resp;
    }

}
