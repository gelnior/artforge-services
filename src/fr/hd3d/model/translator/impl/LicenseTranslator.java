package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.common.client.enums.ELicenseType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILLicense;
import fr.hd3d.model.lightweight.impl.LLicense;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.LicenseH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted device object to Lightweight device object and vice-versa. It also handles update from
 * lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class LicenseTranslator extends BaseTranslator<ILLicense, ILicense>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public ILicense fromLightweight(ILLicense light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ELicenseType type = ELicenseType.CLASSIC;
        if (trim(light.getType()) != null)
        {
            // type = ELicenseType.valueOf(light.getType());
            type = EnumUtils.getEnumValueIgnoreCase(ELicenseType.class, light.getType());
        }

        EInventoryStatus inventoryStatus = EInventoryStatus.UNKNOWN;
        if (trim(light.getInventoryStatus()) != null)
        {
            inventoryStatus = EnumUtils.getEnumValueIgnoreCase(EInventoryStatus.class, light.getInventoryStatus());
        }

        ISoftware software = null;
        if (BaseH.isValidId(light.getSoftwareId()))
        {
            software = Persistors.software.getById(light.getSoftwareId());
        }

        ArrayList<IResourceGroup> resourceGroups = new ArrayList<IResourceGroup>();
        final List<Long> resourceGroupIDs = light.getResourceGroupIDs();
        CollectUtils.removeNull(resourceGroupIDs);
        if (BaseH.areValidIds(resourceGroupIDs))
        {
            resourceGroups.ensureCapacity(resourceGroupIDs.size());
            resourceGroups = (ArrayList<IResourceGroup>) Persistors.resourcegroup.getByIds(null, resourceGroupIDs);
        }
        else
            resourceGroups = new ArrayList<IResourceGroup>();

        ArrayList<IComputer> computers = new ArrayList<IComputer>();
        final List<Long> computerIDs = light.getComputerIDs();
        CollectUtils.removeNull(computerIDs);
        if (BaseH.areValidIds(computerIDs))
        {
            computers.ensureCapacity(computerIDs.size());
            computers = (ArrayList<IComputer>) Persistors.computer.getByIds(null, computerIDs);
        }
        else
            computers = new ArrayList<IComputer>();

        ILicense license = new LicenseH(trim(light.getName()), trim(light.getSerial()), trim(light
                .getBillingReference()), light.getPurchaseDate(), light.getWarrantyEnd(), inventoryStatus, light
                .getNumber(), type, software, new HashSet<IComputer>(computers), new HashSet<IResourceGroup>(
                resourceGroups));

        TranslatorUtil.fromLightWeightCommonFields(license, light, context);

        return license;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILLicense toLightweight(ILicense object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ISoftware software = object.getSoftware();
        String type = "CLASSIC";

        if (object.getType() != null)
        {
            type = object.getType().toString();
        }

        String inventoryStatus = EInventoryStatus.UNKNOWN.toString();
        if (object.getInventoryStatus() != null)
        {
            inventoryStatus = object.getInventoryStatus().toString();
        }

        final ILLicense license = LLicense.getNewInstance(object.getId(), object.getVersion(), null, object.getName(),
                object.getSerial(), object.getBillingReference(), object.getPurchaseDate(), object.getWarrantyEnd(),
                inventoryStatus, object.getNumber(), type);

        for (IResourceGroup resourceGroup : object.getResourceGroups())
        {
            license.addResourceGroupID(resourceGroup.getId());
            license.addResourceGroupName(resourceGroup.getName());
        }
        if (software != null)
        {
            license.setSoftwareId(software.getId());
            license.setSoftwareName(software.getName());
        }

        for (IComputer computer : object.getComputers())
        {
            license.addComputerID(computer.getId());
            license.addComputerName(computer.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, license, context);

        return license;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILLicense light, final ILicense object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILLicense, ILicense> updater = new TranslatorUtil<ILLicense, ILicense>(this).withObject(
                object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("serial").update();

        updater.updateField("billingReference").update();

        updater.updateField("purchaseDate").update();

        updater.updateField("warrantyEnd").update();

        updater.updateField("inventoryStatus").update();

        updater.updateField("number").update();

        updater.updateField("type").update();

        updater.updateField("software").fromField("softwareId").update();

        updater.updateField("resourceGroups").fromField("resourceGroupIDs").update();

        updater.updateField("computers").fromField("computerIDs").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILLicense light, ILicense object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
            {
                resp.logErr("Invalid license name");
            }
        }

        if (light.getNumber() != null && light.getNumber() < 0)
        {
            resp.logErr("Invalid number");
        }
        return resp;
    }
}
