package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILStudioMap;
import fr.hd3d.model.lightweight.impl.LStudioMap;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.StudioMapH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


public class StudioMapTranslator extends BaseTranslator<ILStudioMap, IStudioMap>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IStudioMap fromLightweight(ILStudioMap light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ArrayList<IComputer> computers = new ArrayList<IComputer>();
        final List<Long> computerIDs = light.getComputerIDs();
        CollectUtils.removeNull(computerIDs);
        if (BaseH.areValidIds(computerIDs))
        {
            computers.ensureCapacity(computerIDs.size());
            computers = (ArrayList<IComputer>) Persistors.computer.getByIds(null, computerIDs);
        }
        else
            computers = new ArrayList<IComputer>();

        IStudioMap studiomap = new StudioMapH(trim(light.getName()), trim(light.getImagePath()), light.getSizeX(),
                light.getSizeY(), new HashSet<IComputer>(computers));

        TranslatorUtil.fromLightWeightCommonFields(studiomap, light, context);
        return studiomap;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILStudioMap toLightweight(IStudioMap object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILStudioMap studiomap = LStudioMap.getNewInstance(object.getId(), object.getVersion(), null, object
                .getName(), object.getImagePath(), object.getSizeX(), object.getSizeY());

        for (IComputer computer : object.getComputers())
        {
            studiomap.addComputerID(computer.getId());
            studiomap.addComputerName(computer.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, studiomap, context);
        return studiomap;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILStudioMap light, final IStudioMap object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILStudioMap, IStudioMap> updater = new TranslatorUtil<ILStudioMap, IStudioMap>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("imagePath").update();

        updater.updateField("sizeX").update();

        updater.updateField("sizeY").update();

        if (light.getComputerIDs() != null)
        {
            for (IComputer computer : object.getComputers())
            {
                computer.setStudioMap(null);
            }

            object.merge(object.getComputers(), light.getComputerIDs(), IComputer.class);

            for (IComputer computer : object.getComputers())
            {
                computer.setStudioMap(object);
            }
        }
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILStudioMap light, IStudioMap object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
            {
                resp.logErr("Invalid name");
            }
        }

        if (light.getImagePath() != null)
        {
            if (StringUtils.isBlank(light.getImagePath()))
            {
                resp.logErr("Invalid image path");
            }
        }

        return resp;
    }
}
