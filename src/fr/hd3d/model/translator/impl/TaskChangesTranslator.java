package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILTaskChanges;
import fr.hd3d.model.lightweight.impl.LTaskChanges;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.TaskChangesH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class TaskChangesTranslator extends BaseTranslator<ILTaskChanges, ITaskChanges>
{

    public ITaskChanges fromLightweight(ILTaskChanges light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);
        try
        {
            final ITaskChanges taskChanges = new TaskChangesH();
            taskChanges.setStartDate(light.getStartDate());
            taskChanges.setEndDate(light.getEndDate());
            taskChanges.setDuration(light.getDuration());
            if (BaseH.isValidId(light.getWorkerID()))
            {
                IPerson person = Persistors.person.getById(light.getWorkerID());
                taskChanges.setWorker(person);
            }
            if (BaseH.isValidId(light.getPlanningID()))
            {
                IPlanning planning = Persistors.planning.getById(light.getPlanningID());
                taskChanges.setPlanning(planning);
            }
            if (BaseH.isValidId(light.getTaskGroupID()))
            {
                ITaskGroup taskGroup = Persistors.taskGroup.getById(light.getTaskGroupID());
                taskChanges.setTaskGroup(taskGroup);
            }
            if (BaseH.isValidId(light.getTaskID()))
            {
                ITask task = Persistors.task.getById(light.getTaskID());
                taskChanges.setTask(task);
            }
            if (BaseH.isValidId(light.getExtraLineID()))
            {
                IExtraLine extraLine = Persistors.extraline.getById(light.getExtraLineID());
                taskChanges.setExtraLine(extraLine);
            }
            TranslatorUtil.fromLightWeightCommonFields(taskChanges, light, context);
            return taskChanges;
        }
        catch (Hd3dPersistenceException e)
        {
            throw new Hd3dTranslationException(e.getMessage());
        }
    }

    public ILTaskChanges toLightweight(ITaskChanges object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        final LTaskChanges taskChanges = new LTaskChanges();
        taskChanges.setId(object.getId());
        taskChanges.setVersion(object.getVersion());
        taskChanges.setStartDate(object.getStartDate());
        taskChanges.setEndDate(object.getEndDate());
        taskChanges.setDuration(object.getDuration());
        if (object.getWorker() != null)
        {
            taskChanges.setWorkerID(object.getWorker().getId());
            taskChanges.setPersonName(object.getWorker().fullName());
        }
        taskChanges.setPlanningID(nullSafeId(object.getPlanning()));
        taskChanges.setTaskGroupID(nullSafeId(object.getTaskGroup()));
        if (object.getTask() != null)
        {
            taskChanges.setTaskID(object.getTask().getId());
            taskChanges.setTaskName(object.getTask().getName());
            if (object.getTask().getTaskType() != null)
            {
                taskChanges.setColor(object.getTask().getTaskType().getColor());
            }
            IBase boundEntity = object.getTask().boundEntity();
            if (boundEntity != null)
            {
                if (IConstituent.class.isInstance(boundEntity))
                {
                    final IConstituent constituent = (IConstituent) boundEntity;
                    taskChanges.setWorkObjectName(constituent.getLabel());
                }
                else if (IShot.class.isInstance(boundEntity))
                {
                    final IShot shot = (IShot) boundEntity;
                    taskChanges.setWorkObjectName(shot.getLabel());
                }
            }
        }
        if (object.getExtraLine() != null)
        {
            taskChanges.setExtraLineID(object.getExtraLine().getId());
        }
        TranslatorUtil.toLightWeightCommonFields(object, taskChanges, context);
        return taskChanges;
    }

    public void updateFromLightweight(final ILTaskChanges light, final ITaskChanges object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILTaskChanges, ITaskChanges> updater = new TranslatorUtil<ILTaskChanges, ITaskChanges>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("startDate").update();

        updater.updateField("endDate").update();

        updater.updateField("duration").update();

        updater.updateField("worker").fromField("workerID").update();

        updater.updateField("planning").fromField("planningID").update();

        updater.updateField("taskGroup").fromField("taskGroupID").update();

        updater.updateField("task").fromField("taskID").update();

        updater.updateField("extraLine").fromField("extraLineID").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILTaskChanges light, ITaskChanges object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getTaskID() != null)
            if (BaseH.isNotValidId(light.getTaskID()))
                resp.logErr("Invalid task id");

        if (light.getPlanningID() != null)
            if (BaseH.isNotValidId(light.getPlanningID()))
                resp.logErr("Invalid planning id");

        return resp;
    }

}
