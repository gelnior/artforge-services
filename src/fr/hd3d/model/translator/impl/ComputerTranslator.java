package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.common.client.enums.EWorkerStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILComputer;
import fr.hd3d.model.lightweight.impl.LComputer;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IComputerModel;
import fr.hd3d.model.persistence.IComputerType;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.IProcessor;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRoom;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ComputerH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.EnumUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted computer object to Lightweight computer object and vice-versa. It also handles update from
 * lightweight to persisted software object.
 * 
 * @author HD3D
 */
public class ComputerTranslator extends BaseTranslator<ILComputer, IComputer>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IComputer fromLightweight(ILComputer light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        EWorkerStatus workerStatus = EWorkerStatus.NO_WORKER;
        if (trim(light.getWorkerStatus()) != null)
        {
            // workerStatus = EWorkerStatus.valueOf(light.getWorkerStatus());
            workerStatus = EnumUtils.getEnumValueIgnoreCase(EWorkerStatus.class, light.getWorkerStatus());
        }

        EInventoryStatus inventoryStatus = EInventoryStatus.UNKNOWN;
        if (trim(light.getInventoryStatus()) != null)
        {
            inventoryStatus = EnumUtils.getEnumValueIgnoreCase(EInventoryStatus.class, light.getInventoryStatus());
        }

        ArrayList<IResourceGroup> resourceGroups = (ArrayList<IResourceGroup>) Persistors.resourcegroup.getByIds(null,
                light.getResourceGroupIDs());

        ArrayList<IPool> pools = (ArrayList<IPool>) Persistors.pool.getByIds(null, light.getPoolIDs());

        ArrayList<ISoftware> softwares = (ArrayList<ISoftware>) Persistors.software.getByIds(null, light
                .getSoftwareIDs());

        ArrayList<ILicense> licenses = (ArrayList<ILicense>) Persistors.license.getByIds(null, light.getLicenseIDs());

        ArrayList<IDevice> devices = (ArrayList<IDevice>) Persistors.device.getByIds(null, light.getDeviceIDs());

        ArrayList<IScreen> screens = (ArrayList<IScreen>) Persistors.screen.getByIds(null, light.getScreenIDs());

        IStudioMap studioMap = null;
        if (light.getStudioMapId() != null)
        {
            studioMap = Persistors.studiomap.getById(light.getStudioMapId());
        }

        IRoom room = null;
        if (light.getRoomId() != null)
        {
            room = Persistors.room.getById(light.getRoomId());
        }

        IPerson person = null;
        if (light.getPersonId() != null)
        {
            person = Persistors.person.getById(light.getPersonId());
        }

        IProcessor processor = null;
        if (light.getProcessorId() != null)
        {
            processor = Persistors.processor.getById(light.getProcessorId());
        }

        IComputerType computerType = null;
        if (light.getComputerTypeId() != null)
        {
            computerType = Persistors.computerType.getById(light.getComputerTypeId());
        }

        IComputerModel computerModel = null;
        if (light.getComputerModelId() != null)
        {
            computerModel = Persistors.computerModel.getById(light.getComputerModelId());
        }

        IComputer computer = new ComputerH(light.getId(), light.getVersion(), light.getName(), light.getSerial(), light
                .getBillingReference(), light.getPurchaseDate(), light.getWarrantyEnd(), inventoryStatus, light
                .getInventoryId(), light.getDnsName(), light.getIpAdress(), light.getMacAdress(), light
                .getProcFrequency(), light.getNumberOfProcs(), light.getNumberOfCores(), light.getRamQuantity(),
                workerStatus, light.getxPosition(), light.getyPosition(), processor, computerType, computerModel,
                studioMap, room, new HashSet<IPool>(pools), new HashSet<ISoftware>(softwares), new HashSet<ILicense>(
                        licenses), new HashSet<IDevice>(devices), new HashSet<IScreen>(screens),
                new HashSet<IResourceGroup>(resourceGroups));
        computer.setPerson(person);

        TranslatorUtil.fromLightWeightCommonFields(computer, light, context);
        return computer;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILComputer toLightweight(IComputer object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        IStudioMap studioMap = object.getStudioMap();
        IRoom room = object.getRoom();
        IPerson person = object.getPerson();
        IProcessor processor = object.getProcessor();
        IComputerModel computerModel = object.getComputerModel();
        IComputerType computerType = object.getComputerType();

        String workerStatus = EWorkerStatus.NO_WORKER.toString();
        if (object.getWorkerStatus() != null)
        {
            workerStatus = object.getWorkerStatus().toString();
        }

        String inventoryStatus = EInventoryStatus.UNKNOWN.toString();
        if (object.getInventoryStatus() != null)
        {
            inventoryStatus = object.getInventoryStatus().toString();
        }

        final ILComputer computer = LComputer.getNewInstance(object.getId(), object.getVersion(), null, object
                .getName(), object.getSerial(), object.getBillingReference(), object.getPurchaseDate(), object
                .getWarrantyEnd(), inventoryStatus, object.getInventoryId(), object.getDnsName(), object.getIpAdress(),
                object.getMacAdress(), object.getProcFrequency(), object.getNumberOfProcs(), object.getNumberOfCores(),
                object.getRamQuantity(), workerStatus, object.getxPosition(), object.getyPosition());

        if (studioMap != null)
        {
            computer.setStudioMapId(studioMap.getId());
            computer.setStudioMapName(studioMap.getName());
        }

        if (room != null)
        {
            computer.setRoomId(room.getId());
            computer.setRoomName(room.getName());
        }

        if (person != null)
        {
            computer.setPersonId(person.getId());
            computer.setPersonName(person.fullName());
        }

        if (processor != null)
        {
            computer.setProcessorId(processor.getId());
            computer.setProcessorName(processor.getName());
        }

        if (computerModel != null)
        {
            computer.setComputerModelId(computerModel.getId());
            computer.setComputerModelName(computerModel.getName());
        }

        if (computerType != null)
        {
            computer.setComputerTypeId(computerType.getId());
            computer.setComputerTypeName(computerType.getName());
        }

        for (IResourceGroup resourceGroup : object.getResourceGroups())
        {
            computer.getResourceGroupIDs().add(resourceGroup.getId());
            computer.getResourceGroupNames().add(resourceGroup.getName());
        }

        for (IPool pool : object.getPools())
        {
            computer.getPoolIDs().add(pool.getId());
            computer.getPoolNames().add(pool.getName());
        }

        for (ISoftware software : object.getSoftwares())
        {
            computer.getSoftwareIDs().add(software.getId());
            computer.getSoftwareNames().add(software.getName());
        }

        for (ILicense license : object.getLicenses())
        {
            computer.getLicenseIDs().add(license.getId());
            computer.getLicenseNames().add(license.getName());
        }

        for (IDevice device : object.getDevices())
        {
            computer.getDeviceIDs().add(device.getId());
            computer.getDeviceNames().add(device.getName());
        }

        for (IScreen screen : object.getScreens())
        {
            computer.getScreenIDs().add(screen.getId());
            computer.getScreenNames().add(screen.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, computer, context);
        return computer;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILComputer light, final IComputer object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILComputer, IComputer> updater = new TranslatorUtil<ILComputer, IComputer>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("serial").update();

        updater.updateField("billingReference").update();

        updater.updateField("purchaseDate").update();

        updater.updateField("warrantyEnd").update();

        updater.updateField("inventoryStatus").update();

        updater.updateField("inventoryId").update();

        updater.updateField("dnsName").update();

        updater.updateField("macAdress").update();

        updater.updateField("ipAdress").update();

        updater.updateField("procFrequency").update();

        updater.updateField("numberOfProcs").update();

        updater.updateField("numberOfCores").update();

        updater.updateField("ramQuantity").update();

        updater.updateField("xPosition").update();

        updater.updateField("yPosition").update();

        updater.updateField("workerStatus").update();

        updater.updateField("processor").fromField("processorId").update();

        updater.updateField("computerType").fromField("computerTypeId").update();

        updater.updateField("computerModel").fromField("computerModelId").update();

        updater.acceptNull();
        updater.updateField("studioMap").fromField("studioMapId").update();

        updater.updateField("room").fromField("roomId").update();

        updater.updateField("person").fromField("personId").update();

        updater.updateField("resourceGroups").fromField("resourceGroupIDs").update();

        updater.updateField("pools").fromField("poolIDs").update();

        updater.updateField("softwares").fromField("softwareIDs").update();

        updater.updateField("licenses").fromField("licenseIDs").update();

        updater.updateField("devices").fromField("deviceIDs").update();

        updater.updateField("screens").fromField("screenIDs").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILComputer light, IComputer object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid name");

        if (light.getProcFrequency() != null && light.getProcFrequency() < 0)
            resp.logErr("Invalid number of processor frequency");

        if (light.getNumberOfProcs() != null && light.getNumberOfProcs() < 0)
            resp.logErr("Invalid number of processors");

        if (light.getNumberOfCores() != null && light.getNumberOfCores() < 0)
            resp.logErr("Invalid number of cores");

        if (light.getRamQuantity() != null && light.getRamQuantity() < 0)
            resp.logErr("Invalid quantity of ram");

        if (light.getxPosition() != null && light.getxPosition() < 0)
            resp.logErr("Invalid X position");

        if (light.getyPosition() != null && light.getyPosition() < 0)
            resp.logErr("Invalid Y position");

        return resp;
    }
}
