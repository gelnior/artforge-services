package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.lightweight.impl.LShot;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ShotH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ShotTranslator extends BaseTranslator<ILShot, IShot>
{
    public IShot fromLightweight(ILShot light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ISequence sequence = null;
        if (BaseH.isValidId(light.getSequence()))
            sequence = Persistors.sequence.getById(light.getSequence());

        IShot shot = new ShotH(sequence, light.getCompletion(), trim(light.getDescription()), light.getDifficulty(),
                trim(light.getHook()), trim(light.getLabel()), light.getTrust(), light.getMattePainting(), light
                        .getLayout(), light.getAnimation(), light.getExport(), light.getTracking(),
                light.getDressing(), light.getLighting(), light.getRendering(), light.getCompositing(), light
                        .getNbFrame(), light.getStartFrame(), light.getEndFrame());

        TranslatorUtil.fromLightWeightCommonFields(shot, light, context);
        return shot;
    }

    public ILShot toLightweight(IShot object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILShot shot = LShot.getNewInstance(object.getId(), object.getVersion(), null, null, nullSafeId(object
                .getSequence()), object.getCompletion(), object.getDescription(), object.getDifficulty(), object
                .getHook(), object.getLabel(), object.getTrust(), object.getMattePainting(), object.getLayout(), object
                .getAnimation(), object.getExport(), object.getTracking(), object.getDressing(), object.getLighting(),
                object.getRendering(), object.getCompositing(), object.getNbFrame(), object.getPath(), object
                        .getStartFrame(), object.getEndFrame());

        TranslatorUtil.toLightWeightCommonFields(object, shot, context);
        return shot;
    }

    public void updateFromLightweight(final ILShot light, final IShot object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILShot, IShot> updater = new TranslatorUtil<ILShot, IShot>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("sequence").update();

        updater.updateField("completion").update();

        updater.updateField("description").update();

        updater.updateField("difficulty").update();

        updater.updateField("hook").update();

        updater.updateField("label").update();

        updater.updateField("trust").update();

        updater.updateField("mattePainting").update();

        updater.updateField("layout").update();

        updater.updateField("animation").update();

        updater.updateField("export").update();

        updater.updateField("tracking").update();

        updater.updateField("dressing").update();

        updater.updateField("lighting").update();

        updater.updateField("rendering").update();

        updater.updateField("compositing").update();

        updater.updateField("nbFrame").update();
        updater.updateField("startFrame").update();
        updater.updateField("endFrame").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILShot light, IShot object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getSequence() != null)
            if (BaseH.isNotValidId(light.getSequence()))
                resp.logErr("Invalid sequence id");

        if (light.getLabel() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getLabel()))
                resp.logErr("Invalid label name");

        return resp;
    }
}
