package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.enums.EContractType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILContract;
import fr.hd3d.model.lightweight.impl.LContract;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.IOrganization;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IQualification;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ContractH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted contract object to Lightweight contract object and vice-versa. It also handles update from
 * lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class ContractTranslator extends BaseTranslator<ILContract, IContract>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IContract fromLightweight(ILContract light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        EContractType type = EContractType.UNKNOWN;
        if (light.getType() != null)
        {
            type = EContractType.valueOf(light.getType());
        }

        IPerson person = null;
        if (BaseH.isValidId(light.getPersonId()))
        {
            person = Persistors.person.getById(light.getPersonId());
        }

        IQualification qualification = null;
        if (BaseH.isValidId(light.getQualificationId()))
        {
            qualification = Persistors.qualification.getById(light.getQualificationId());
        }

        IOrganization organization = null;
        if (BaseH.isValidId(light.getOrganizationId()))
        {
            organization = Persistors.organization.getById(light.getOrganizationId());
        }

        IContract contract = new ContractH(trim(light.getName()), light.getStartDate(), light.getEndDate(), light
                .getRealEndDate(), trim(light.getDocPath()), type, person, qualification, organization, light
                .getSalary());

        TranslatorUtil.fromLightWeightCommonFields(contract, light, context);
        return contract;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILContract toLightweight(IContract object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        IPerson person = object.getPerson();
        IQualification qualification = object.getQualification();
        IOrganization organization = object.getOrganization();

        final ILContract contract = LContract.getNewInstance(object.getId(), object.getVersion(), object.getName(),
                object.getStartDate(), object.getEndDate(), object.getRealEndDate(), object.getDocPath(), object
                        .getType().toString(), object.getSalary());

        if (person != null)
        {
            contract.setPersonId(person.getId());
            contract.setPersonName(person.getResourceName());
        }

        if (qualification != null)
        {
            contract.setQualificationId(qualification.getId());
            contract.setQualificationName(qualification.getName());
        }

        if (organization != null)
        {
            contract.setOrganizationId(organization.getId());
            contract.setOrganizationName(organization.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, contract, context);
        return contract;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILContract light, final IContract object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILContract, IContract> updater = new TranslatorUtil<ILContract, IContract>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("startDate").update();

        updater.updateField("endDate").update();

        updater.updateField("realEndDate").update();

        updater.updateField("docPath").update();

        updater.updateField("type").update();

        updater.updateField("person").fromField("personId").update();

        updater.updateField("qualification").fromField("qualificationId").update();

        updater.updateField("organization").fromField("organizationId").update();

        updater.updateField("salary").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILContract light, IContract object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
            {
                resp.logErr("Invalid device name");
            }
        }

        return resp;
    }
}
