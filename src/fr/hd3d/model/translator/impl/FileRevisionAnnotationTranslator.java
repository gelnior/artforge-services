package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILFileRevisionAnnotation;
import fr.hd3d.model.lightweight.impl.LFileRevisionAnnotation;
import fr.hd3d.model.persistence.IFileRevisionAnnotation;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionAnnotationH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class FileRevisionAnnotationTranslator extends BaseTranslator<ILFileRevisionAnnotation, IFileRevisionAnnotation>
{

    public IFileRevisionAnnotation fromLightweight(ILFileRevisionAnnotation light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        IFileRevisionAnnotation fileRevisionAnnotation = new FileRevisionAnnotationH();

        fileRevisionAnnotation.setMarkIn(light.getMarkIn());
        fileRevisionAnnotation.setMarkOut(light.getMarkOut());
        fileRevisionAnnotation.setName(light.getName());
        fileRevisionAnnotation.setComment(light.getComment());
        fileRevisionAnnotation.setGraphicData(Persistors.filerevision.getById(light.getGraphicData()));
        fileRevisionAnnotation.setAnnotatedFile(Persistors.filerevision.getById(light.getAnnotatedFile()));

        TranslatorUtil.fromLightWeightCommonFields(fileRevisionAnnotation, light, context);
        return fileRevisionAnnotation;
    }

    public ILFileRevisionAnnotation toLightweight(IFileRevisionAnnotation object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILFileRevisionAnnotation fileRevisionAnnotation = LFileRevisionAnnotation.getNewInstance(object.getId(), object
                .getVersion(), null, object.getMarkIn(), object.getMarkOut(), object.getName(), object.getComment(),
                nullSafeId(object.getGraphicData()), nullSafeId(object.getAnnotatedFile()), nullSafeId(object
                        .getAnnotator()));

        TranslatorUtil.toLightWeightCommonFields(object, fileRevisionAnnotation, context);
        return fileRevisionAnnotation;
    }

    public void updateFromLightweight(final ILFileRevisionAnnotation light, final IFileRevisionAnnotation object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILFileRevisionAnnotation, IFileRevisionAnnotation> updater = new TranslatorUtil<ILFileRevisionAnnotation, IFileRevisionAnnotation>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("markIn").update();

        updater.updateField("markOut").update();

        updater.updateField("name").update();

        updater.updateField("comment").update();

        updater.updateField("graphicData").update();

        updater.updateField("annotatedFile").update();

        updater.updateField("annotator").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILFileRevisionAnnotation light, IFileRevisionAnnotation object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        return resp;
    }

}
