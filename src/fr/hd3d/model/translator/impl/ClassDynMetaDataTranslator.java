package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILClassDynMetaDataType;
import fr.hd3d.model.lightweight.impl.LClassDynMetaDataType;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ClassDynMetaDataTypeH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ClassDynMetaDataTranslator extends BaseTranslator<ILClassDynMetaDataType, IClassDynMetaDataType>
{
    public IClassDynMetaDataType fromLightweight(ILClassDynMetaDataType light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        IClassDynMetaDataType classDynMetaData = new ClassDynMetaDataTypeH();
        classDynMetaData.setName(trim(light.getName()));
        classDynMetaData.setClassName(trim(light.getClassName()));
        classDynMetaData.setPredicate(trim(light.getPredicate()));
        final Long dynMetaDatatypeId = light.getDynMetaDataType();
        if (BaseH.isValidId(dynMetaDatatypeId))
            classDynMetaData.setDynMetaDataType(Persistors.dynmetadatatype.getById(dynMetaDatatypeId));
        classDynMetaData.setExtra(trim(light.getExtra()));
        TranslatorUtil.fromLightWeightCommonFields(classDynMetaData, light, context);
        return classDynMetaData;
    }

    public ILClassDynMetaDataType toLightweight(IClassDynMetaDataType object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILClassDynMetaDataType classDynMetaData = LClassDynMetaDataType.getNewInstance(object.getId(), object
                .getVersion(), null, object.getName(), nullSafeId(object.getDynMetaDataType()), object.getPredicate(),
                object.getClassName(), object.getExtra());
        TranslatorUtil.toLightWeightCommonFields(object, classDynMetaData, context);

        if (object.getDynMetaDataType() != null)
        {
            classDynMetaData.setDynMetaDataTypeName(object.getDynMetaDataType().getName());
        }

        return classDynMetaData;
    }

    public void updateFromLightweight(final ILClassDynMetaDataType light, final IClassDynMetaDataType object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILClassDynMetaDataType, IClassDynMetaDataType> updater = new TranslatorUtil<ILClassDynMetaDataType, IClassDynMetaDataType>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("className").update();

        updater.updateField("predicate").update();

        updater.updateField("dynMetaDataType").update();

        updater.updateField("extra").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILClassDynMetaDataType light, IClassDynMetaDataType object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getDynMetaDataType() == null)
            resp.logErr("metadatatype must not be null");

        if (light.getDynMetaDataType() != null)
            if (BaseH.isNotValidId(light.getDynMetaDataType()))
                resp.logErr("Invalid metadatatype id");

        return resp;
    }
}
