package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILItemGroup;
import fr.hd3d.model.lightweight.impl.LItemGroup;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ItemGroupH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ItemGroupTranslator extends BaseTranslator<ILItemGroup, IItemGroup>
{
    public IItemGroup fromLightweight(ILItemGroup light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        // sheet
        ISheet sheet = Persistors.sheet.getById(light.getSheet());

        // items
        List<IItem> items = null;

        List<Long> lItems = light.getItems();
        CollectUtils.removeNull(lItems);
        if (lItems != null)
        {
            // Note: do not pass this directly as argument of new ItemGroupHibernateImpl.
            // PersistedObjectProviders.item.getObjectsWithIds may pass an Collections.EmptyList (not-modifiable)
            // as response if nothing found. Thus, adding in this collection will raise an
            // "UnsupportedOperationException"
            // items = PersistedObjectProviders.item.getObjectsWithIds(null, light.getItems());
            items = new ArrayList<IItem>(lItems.size());
            // Note: Order does matter here.
            // Do not use PersistedObjectProviders.itemgroup.getObjectsWithIds here.
            // It will not return the list in the expected order
            for (Long id : lItems)
                CollectionUtils.addIgnoreNull(items, Persistors.item.getById(id));
        }

        IItemGroup itemGroup = new ItemGroupH(trim(light.getName()), items, sheet, trim(light.getHook()));

        TranslatorUtil.fromLightWeightCommonFields(itemGroup, light, context);
        return itemGroup;
    }

    public ILItemGroup toLightweight(IItemGroup object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null || object.getItems() == null)
            return null;

        // items
        List<IItem> items = object.getItems();
        CollectUtils.removeNull(items);

        List<Long> itemIds = new ArrayList<Long>(items.size());

        for (IItem item : items)
            itemIds.add(item.getId());

        ILItemGroup itemgroup = LItemGroup.getNewInstance(object.getId(), object.getVersion(), null, object.getName(),
                itemIds, nullSafeId(object.getSheet()), object.getHook());
        TranslatorUtil.toLightWeightCommonFields(object, itemgroup, context);
        return itemgroup;
    }

    public void updateFromLightweight(final ILItemGroup light, final IItemGroup object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILItemGroup, IItemGroup> updater = new TranslatorUtil<ILItemGroup, IItemGroup>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        List<Long> itemIds = light.getItems();
        CollectUtils.removeNull(itemIds);
        if (itemIds != null)
        {
            List<IItem> items = object.getItems();
            List<Long> itemsIds = CollectUtils.getIds(items);
            if (!itemsIds.equals(itemIds))
            {
                items.clear();
                // Note: Order does matter here.
                // Do not use PersistedObjectProviders.item.getObjectsWithIds here.
                // It will not return the list in the expected order
                for (Long id : itemIds)
                    object.addItem(Persistors.item.getById(id));
            }
        }

        updater.updateField("sheet").update();

        updater.updateField("hook").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILItemGroup light, IItemGroup object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid itemgroup name");

        if (light.getSheet() != null)
            if (BaseH.isNotValidId(light.getSheet()))
                resp.logErr("Invalid sheet id");

        return resp;
    }
}
