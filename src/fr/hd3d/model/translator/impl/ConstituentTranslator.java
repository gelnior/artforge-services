package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.lightweight.impl.LConstituent;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ConstituentH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ConstituentTranslator extends BaseTranslator<ILConstituent, IConstituent>
{
    public IConstituent fromLightweight(ILConstituent light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ICategory category = null;
        if (BaseH.isValidId(light.getCategory()))
            category = Persistors.category.getById(light.getCategory());

        IConstituent constituent = new ConstituentH(category, light.getCompletion(), trim(light.getDescription()),
                light.getDifficulty(), trim(light.getHook()), trim(light.getLabel()), light.getTrust(), light
                        .getDesign(), light.getModeling(), light.getSetup(), light.getTexturing(), light.getShading(),
                light.getHairs());

        TranslatorUtil.fromLightWeightCommonFields(constituent, light, context);
        return constituent;
    }

    public ILConstituent toLightweight(IConstituent object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ICategory category = object.getCategory();

        ILConstituent constituent = LConstituent.getNewInstance(object.getId(), object.getVersion(), null, null,
                nullSafeId(object.getCategory()), (category == null) ? null : category.getName(), object
                        .getCompletion(), object.getDescription(), object.getDifficulty(), object.getHook(), object
                        .getLabel(), object.getTrust(), object.getDesign(), object.getModeling(), object.getSetup(),
                object.getTexturing(), object.getShading(), object.getHairs(), object.getPath());

        TranslatorUtil.toLightWeightCommonFields(object, constituent, context);
        return constituent;
    }

    public void updateFromLightweight(final ILConstituent light, final IConstituent object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILConstituent, IConstituent> updater = new TranslatorUtil<ILConstituent, IConstituent>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("category").update();

        updater.updateField("completion").update();

        updater.updateField("description").update();

        updater.updateField("difficulty").update();

        updater.updateField("hook").update();

        updater.updateField("label").update();

        updater.updateField("trust").update();

        updater.updateField("design").update();

        updater.updateField("modeling").update();

        updater.updateField("setup").update();

        updater.updateField("texturing").update();

        updater.updateField("shading").update();

        updater.updateField("hairs").update();
    }
}
