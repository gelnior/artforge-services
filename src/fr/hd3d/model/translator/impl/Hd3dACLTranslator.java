package fr.hd3d.model.translator.impl;

import java.util.HashSet;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILHd3dACL;
import fr.hd3d.model.lightweight.impl.LHd3dACL;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.dao.Hd3dACLH;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Try LAM
 */
public class Hd3dACLTranslator extends BaseTranslator<ILHd3dACL, IHd3dACL>
{
    public ILHd3dACL toLightweight(IHd3dACL object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILHd3dACL acl = new LHd3dACL();
        acl.setRoles(new HashSet<Long>(CollectUtils.getIds(object.getRoles())));
        acl.setEntityName(object.getEntityName());
        acl.setReadPermitted(object.getReadPermitted());
        acl.setCreatePermitted(object.getCreatePermitted());
        acl.setUpdatePermitted(object.getUpdatePermitted());
        acl.setDeletePermitted(object.getDeletePermitted());
        acl.setReadBans(object.getReadBans());
        acl.setUpdateBans(object.getUpdateBans());
        acl.setDeleteBans(object.getDeleteBans());

        TranslatorUtil.toLightWeightCommonFields(object, acl, context);
        return acl;
    }

    public IHd3dACL fromLightweight(ILHd3dACL light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        Set<IRole> roles = new HashSet<IRole>();
        final Set<Long> roleIDs = light.getRoles();
        CollectUtils.removeNull(roleIDs);
        if (BaseH.areValidIds(roleIDs))
        {
            roles.addAll(Persistors.role.getByIds(context, roleIDs));
        }
        else
        {
            roles = new HashSet<IRole>();
        }

        IHd3dACL acl = new Hd3dACLH(roles, trim(light.getEntityName()), light.getReadPermitted(), light
                .getCreatePermitted(), light.getUpdatePermitted(), light.getDeletePermitted(), light.getReadBans(),
                light.getUpdateBans(), light.getDeleteBans());

        TranslatorUtil.fromLightWeightCommonFields(acl, light, context);
        return acl;
    }

    private void updateFields(final TranslatorUtil<ILHd3dACL, IHd3dACL> updater) throws Hd3dException
    {
        updater.updateField("roles").update();

        updater.updateField("entityName").update();

        updater.updateField("readPermitted").update();

        updater.updateField("createPermitted").update();

        updater.updateField("updatePermitted").update();

        updater.updateField("deletePermitted").update();

        updater.updateField("readBans").update();

        updater.updateField("updateBans").update();

        updater.updateField("deleteBans").update();
    }

    public void updateFromLightweight(final ILHd3dACL light, final IHd3dACL object, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILHd3dACL, IHd3dACL> updater = new TranslatorUtil<ILHd3dACL, IHd3dACL>(this).withObject(
                object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updateFields(updater);
    }

    @Override
    public ValidationResponse validateLightWeight(ILHd3dACL light, IHd3dACL object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getEntityName() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getEntityName()))
                resp.logErr("Invalid entity name");

        return resp;
    }
}
