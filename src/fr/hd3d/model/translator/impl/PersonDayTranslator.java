package fr.hd3d.model.translator.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.ObjectUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.lightweight.impl.LPersonDay;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.PersonDayH;
import fr.hd3d.model.persistence.impl.hibernate.WorkingTimeH;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class PersonDayTranslator extends BaseTranslator<ILPersonDay, IPersonDay>
{
    public IPersonDay fromLightweight(ILPersonDay light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        final Long personId = light.getPersonID();
        IPerson person = null;
        if (BaseH.isValidId(personId))
            person = Persistors.person.getById(personId);
        if (person == null)
            throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "User", personId));

        Calendar day1 = Calendar.getInstance();
        day1.setTime(light.getDate());
        day1.set(Calendar.HOUR, 0);
        day1.set(Calendar.MINUTE, 0);
        day1.set(Calendar.MILLISECOND, 0);
        IPersonDay day = new PersonDayH(day1.getTime(), person);

        final Set<Long> activityIds = light.getActivitiesIDs();
        CollectUtils.removeNull(activityIds);
        if (BaseH.areValidIds(activityIds))
        {
            final List<IActivity> activities = Persistors.activity.getByIds(null, light.getActivitiesIDs());
            day.getActivities().addAll(activities);
        }

        final Long activitiesApprovedById = light.getActivitiesApprovedByID();
        final Date activitiesApprovedDate = light.getActivitiesApprovedDate();
        if (BaseH.isValidId(activitiesApprovedById) && activitiesApprovedDate != null)
        {
            day.setActivitiesApprovedBy(Persistors.person.getById(activitiesApprovedById));
            day.setActivitiesApprovedDate(activitiesApprovedDate);
        }

        final List<Long> workingTimeIds = light.getWorkingTimesIDs();
        CollectUtils.removeNull(workingTimeIds);
        if (BaseH.areValidIds(workingTimeIds))
        {
            final List<IWorkingTime> workingTimes = Persistors.workingTime.getByIds(null, light.getWorkingTimesIDs());
            day.getWorkingTimes().addAll(workingTimes);
        }
        // workaround for mcguff
        final Date startDate1 = light.getStartDate1();
        final Date endDate1 = light.getEndDate1();
        final IWorkingTime workingTime1 = new WorkingTimeH(day, startDate1, endDate1);
        final Date startDate2 = light.getStartDate2();
        final Date endDate2 = light.getEndDate2();
        final IWorkingTime workingTime2 = new WorkingTimeH(day, startDate2, endDate2);
        day.setWorkingTimes(Arrays.asList(workingTime1, workingTime2));

        TranslatorUtil.fromLightWeightCommonFields(day, light, context);
        return day;
    }

    public ILPersonDay toLightweight(IPersonDay object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        final IBaseTranslator<ILActivity, IActivity> activityTranslator = Translators.activity;
        final ILPersonDay day = LPersonDay.getNewInstance(object.getId(), object.getVersion(), null, object.getDate(),
                nullSafeId(object.getPerson()));
        day.setPersonName(object.getPerson().fullName());
        final Set<IActivity> activities = object.getActivities();
        for (IActivity activity : activities)
        {
            Long id = activity.getId();
            day.addActivityID(id);
            day.addActivity(activityTranslator.toLightweight(activity, context));
        }
        if (object.getActivitiesApprovedBy() != null && object.getActivitiesApprovedDate() != null)
        {
            day.setActivitiesApprovedByID(object.getActivitiesApprovedBy().getId());
            day.setActivitiesApprovedByLogin(object.getActivitiesApprovedBy().fullName());
            day.setActivitiesApprovedDate(object.getActivitiesApprovedDate());
        }

        for (IWorkingTime workingTime : object.getWorkingTimes())
        {
            Long id = workingTime.getId();
            day.addWorkingTimeID(id);
            day.addWorkingTime(Translators.workingTime.toLightweight(workingTime, context));
        }

        // workaround for mcguff
        if (CollectUtils.isNotEmpty(object.getWorkingTimes()))
        {
            if (object.getWorkingTimes().get(0) != null)
            {
                day.setStartDate1(object.getWorkingTimes().get(0).getStartDate());
                day.setEndDate1(object.getWorkingTimes().get(0).getEndDate());
            }
            if (object.getWorkingTimes().get(1) != null)
            {
                day.setStartDate2(object.getWorkingTimes().get(1).getStartDate());
                day.setEndDate2(object.getWorkingTimes().get(1).getEndDate());
            }
        }

        TranslatorUtil.toLightWeightCommonFields(object, day, context);
        return day;
    }

    public void updateFromLightweight(final ILPersonDay light, final IPersonDay object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILPersonDay, IPersonDay> updater = new TranslatorUtil<ILPersonDay, IPersonDay>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        final Long activitiesApprovedById = light.getActivitiesApprovedByID();
        final Date activitiesApprovedDate = light.getActivitiesApprovedDate();

        if (BaseH.isValidId(activitiesApprovedById) && activitiesApprovedDate != null)
        {
            if (!ObjectUtils.equals(nullSafeId(object.getActivitiesApprovedBy()), activitiesApprovedById))
            {
                final IPerson approvedBy = Persistors.person.getById(activitiesApprovedById);
                if (approvedBy == null)
                    throw new Hd3dTranslationException(MessageFormat.format(ERRMSG_0, "User", activitiesApprovedById));

                object.setActivitiesApprovedBy(approvedBy);
            }
            if (!ObjectUtils.equals(object.getActivitiesApprovedDate(), activitiesApprovedDate))
            {
                object.setActivitiesApprovedDate(activitiesApprovedDate);
            }
        }
        else
        {
            object.setActivitiesApprovedBy(null);
            object.setActivitiesApprovedDate(null);
        }

        updater.updateField("activities").fromField("activitiesIDs").update();

        updater.updateField("workingTimes").fromField("workingTimesIDs").update();

        // workaround for mcguff
        if (light.getStartDate1() != null)
            if (CollectUtils.isNotEmpty(object.getWorkingTimes()))
            {
                if (object.getWorkingTimes().get(0) != null)
                {
                    if (!ObjectUtils.equals(object.getWorkingTimes().get(0).getStartDate(), light.getStartDate1()))
                    {
                        object.getWorkingTimes().get(0).setStartDate(light.getStartDate1());
                    }
                }
            }
            else
            {/* create a new working time */
                List<IWorkingTime> workingTimes = new ArrayList<IWorkingTime>(2);
                workingTimes.add(new WorkingTimeH(object, light.getStartDate1(), light.getEndDate1()));
                workingTimes.add(new WorkingTimeH(object, null, null));

                object.setWorkingTimes(workingTimes);
            }

        if (light.getEndDate1() != null)
            if (CollectUtils.isNotEmpty(object.getWorkingTimes()))
            {
                if (object.getWorkingTimes().get(0) != null)
                    if (!ObjectUtils.equals(object.getWorkingTimes().get(0).getEndDate(), light.getEndDate1()))
                    {
                        object.getWorkingTimes().get(0).setEndDate(light.getEndDate1());
                    }
            }
            else
            {
                /* create a new working time */
                List<IWorkingTime> workingTimes = new ArrayList<IWorkingTime>(2);
                workingTimes.add(new WorkingTimeH(object, light.getStartDate1(), light.getEndDate1()));
                workingTimes.add(new WorkingTimeH(object, null, null));

                object.setWorkingTimes(workingTimes);
            }

        if (light.getStartDate2() != null)
            if (CollectUtils.isNotEmpty(object.getWorkingTimes()))
            {
                if (object.getWorkingTimes().get(1) != null)
                    if (!ObjectUtils.equals(object.getWorkingTimes().get(1).getStartDate(), light.getStartDate2()))
                    {
                        object.getWorkingTimes().get(1).setStartDate(light.getStartDate2());
                    }
            }
            else
            {
                /* create a new working time */
                List<IWorkingTime> workingTimes = new ArrayList<IWorkingTime>(2);
                workingTimes.add(new WorkingTimeH(object, null, null));
                workingTimes.add(new WorkingTimeH(object, light.getStartDate2(), light.getEndDate2()));

                object.setWorkingTimes(workingTimes);
            }

        if (light.getEndDate2() != null)
            if (CollectUtils.isNotEmpty(object.getWorkingTimes()))
            {
                if (object.getWorkingTimes().get(1) != null)
                    if (!ObjectUtils.equals(object.getWorkingTimes().get(1).getEndDate(), light.getEndDate2()))
                    {
                        object.getWorkingTimes().get(1).setEndDate(light.getEndDate2());
                    }
            }
            else
            {
                /* create a new working time */
                List<IWorkingTime> workingTimes = new ArrayList<IWorkingTime>(2);
                workingTimes.add(new WorkingTimeH(object, null, null));
                workingTimes.add(new WorkingTimeH(object, light.getStartDate2(), light.getEndDate2()));

                object.setWorkingTimes(workingTimes);
            }
    }

    @Override
    public ValidationResponse validateLightWeight(ILPersonDay light, IPersonDay object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getPersonID() != null)
            if (BaseH.isNotValidId(light.getPersonID()))
                resp.logErr("Invalid person id");

        return resp;
    }
}
