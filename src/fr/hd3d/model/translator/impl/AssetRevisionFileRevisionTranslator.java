// package fr.hd3d.model.translator.impl;
//
// import java.util.Map;
//
// import fr.hd3d.exception.Hd3dException;
// import fr.hd3d.exception.Hd3dTranslationException;
// import fr.hd3d.model.lightweight.ILAssetRevisionFileRevision;
// import fr.hd3d.model.lightweight.impl.LAssetRevisionFileRevision;
// import fr.hd3d.model.persistence.IAssetRevisionFileRevision;
// import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionFileRevisionH;
// import fr.hd3d.utils.TranslatorUtil;
//
//
// public class AssetRevisionFileRevisionTranslator extends
// BaseTranslator<ILAssetRevisionFileRevision, IAssetRevisionFileRevision>
// {
//
// public IAssetRevisionFileRevision fromLightweight(ILAssetRevisionFileRevision light, Map<String, String> map)
// throws Hd3dException
// {
// validate(light, null);
//
// IAssetRevisionFileRevision assetfile = new AssetRevisionFileRevisionH();
//
// assetfile.setAssetKey(trim(light.getAssetKey()));
// assetfile.setFileKey(trim(light.getFileKey()));
// assetfile.setAssetVariation(trim(light.getAssetVariation()));
// assetfile.setFileVariation(trim(light.getFileVariation()));
// assetfile.setAssetRevision(light.getAssetRevision());
// assetfile.setFileRevision(light.getFileRevision());
//
// TranslatorUtil.fromLightWeightCommonFields(assetfile, light, map);
// return assetfile;
// }
//
// public ILAssetRevisionFileRevision toLightweight(IAssetRevisionFileRevision object, Map<String, String> map)
// throws Hd3dTranslationException
// {
// if (object == null)
// return null;
//
// ILAssetRevisionFileRevision assetfile = LAssetRevisionFileRevision.getNewInstance(object.getId(), object
// .getVersion(), null, object.getAssetKey(), object.getFileKey(), object.getAssetVariation(), object
// .getFileVariation(), object.getAssetRevision(), object.getFileRevision());
//
// TranslatorUtil.toLightWeightCommonFields(object, assetfile, map);
// return assetfile;
// }
//
// public void updateFromLightweight(final ILAssetRevisionFileRevision light, final IAssetRevisionFileRevision object,
// final Map<String, String> map) throws Hd3dException
// {
// validate(light, object);
//
// final TranslatorUtil<ILAssetRevisionFileRevision, IAssetRevisionFileRevision> updater = new
// TranslatorUtil<ILAssetRevisionFileRevision, IAssetRevisionFileRevision>(
// this).withObject(object).withLightObject(light);
//
// TranslatorUtil.updateCommonFields(object, light, map);
//
// updater.updateField("assetKey").update();
//
// updater.updateField("fileKey").update();
//
// updater.updateField("assetVariation").update();
//
// updater.updateField("fileVariation").update();
//
// updater.updateField("assetRevision").update();
//
// updater.updateField("fileRevision").update();
// }
//
// @Override
// public ValidationResponse validateLightWeight(ILAssetRevisionFileRevision light, IAssetRevisionFileRevision object)
// throws Hd3dTranslationException
// {
// ValidationResponse resp = super.validateLightWeight(light, object);
//
// return resp;
// }
// }
