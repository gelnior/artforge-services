package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILSkill;
import fr.hd3d.model.lightweight.impl.LSkill;
import fr.hd3d.model.persistence.ISkill;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.SkillH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted skill object to Lightweight skill object and vice-versa. It also handles update from lightweight
 * to persisted device object.
 * 
 * @author HD3D
 */
public class SkillTranslator extends BaseTranslator<ILSkill, ISkill>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public ISkill fromLightweight(ILSkill light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ISoftware software = null;
        if (BaseH.isValidId(light.getSoftwareId()))
        {
            software = Persistors.software.getById(light.getSoftwareId());
        }

        ISkill skill = new SkillH(trim(light.getName()), software, null);

        TranslatorUtil.fromLightWeightCommonFields(skill, light, context);
        return skill;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILSkill toLightweight(ISkill object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        ISoftware software = object.getSoftware();

        final ILSkill skill = LSkill.getNewInstance(object.getId(), object.getVersion(), object.getName());

        if (software != null)
        {
            skill.setSoftwareId(software.getId());
            skill.setSoftwareName(software.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, skill, context);
        return skill;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILSkill light, final ISkill object, final ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILSkill, ISkill> updater = new TranslatorUtil<ILSkill, ISkill>(this).withObject(object)
                .withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("software").fromField("softwareId").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILSkill light, ISkill object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid skill name");
        }

        return resp;
    }
}
