package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.Date;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILTaskGroup;
import fr.hd3d.model.lightweight.impl.LTaskGroup;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.TaskGroupH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.StringUtils;
import fr.hd3d.utils.TranslatorUtil;


public class TaskGroupTranslator extends BaseTranslator<ILTaskGroup, ITaskGroup>
{

    public ITaskGroup fromLightweight(ILTaskGroup light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        final ITaskGroup taskGroup = new TaskGroupH();

        final String color = trim(light.getColor());
        if (color != null)
            taskGroup.setColor(color);

        final Date endDate = light.getEndDate();
        if (endDate != null)
            taskGroup.setEndDate(endDate);

        final Date startDate = light.getStartDate();
        if (startDate != null)
            taskGroup.setStartDate(startDate);

        final String name = trim(light.getName());
        if (name != null)
            taskGroup.setName(name);

        if (BaseH.isValidId(light.getTaskGroupID()))
            taskGroup.setTaskGroup(Persistors.taskGroup.getById(light.getTaskGroupID()));

        if (BaseH.isValidId(light.getTaskTypeId()))
            taskGroup.setTaskType(Persistors.tasktype.getById(light.getTaskTypeId()));

        // planning cannot be null here (validated before)
        taskGroup.setPlanning(Persistors.planning.getById(light.getPlanning()));
        taskGroup.setFilter(trim(light.getFilter()));

        TranslatorUtil.fromLightWeightCommonFields(taskGroup, light, context);
        return taskGroup;
    }

    public ILTaskGroup toLightweight(ITaskGroup object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILTaskGroup taskGroup = new LTaskGroup();
        taskGroup.setId(object.getId());
        taskGroup.setVersion(object.getVersion());
        if (object.getTaskType() != null && object.getTaskType().getColor() != null)
            taskGroup.setColor(object.getTaskType().getColor());
        else
            taskGroup.setColor(object.getColor());
        taskGroup.setName(object.getName());
        taskGroup.setStartDate(object.getStartDate());
        taskGroup.setEndDate(object.getEndDate());
        taskGroup.setTaskGroupID(nullSafeId(object.getTaskGroup()));
        taskGroup.setPlanning(nullSafeId(object.getPlanning()));
        taskGroup.setTaskTypeId(nullSafeId(object.getTaskType()));
        taskGroup.setFilter(object.getFilter());
        if (object.getExtraLines() != null)
        {
            taskGroup.setExtraLineIDs(new ArrayList<Long>());
            for (IExtraLine extraLine : object.getExtraLines())
            {
                taskGroup.getExtraLineIDs().add(extraLine.getId());
            }

        }
        TranslatorUtil.toLightWeightCommonFields(object, taskGroup, context);

        return taskGroup;
    }

    public void updateFromLightweight(final ILTaskGroup light, final ITaskGroup object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILTaskGroup, ITaskGroup> updater = new TranslatorUtil<ILTaskGroup, ITaskGroup>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("color").update();

        updater.updateField("endDate").update();

        updater.updateField("startDate").update();

        updater.updateField("taskGroup").fromField("taskGroupID").update();

        updater.updateField("planning").update();

        updater.updateField("taskType").fromField("taskTypeId").update();

        updater.updateField("filter").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILTaskGroup light, ITaskGroup object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getColor() != null)
            if (StringUtils.isNotValidColor(light.getColor()))
                resp.logErr("Invalid color");

        if (light.getName() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid name");

        if (light.getPlanning() != null)
            if (BaseH.isNotValidId(light.getPlanning()))
                resp.logErr("Planning is null");

        return resp;
    }

}
