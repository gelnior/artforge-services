package fr.hd3d.model.translator.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.enums.EFrameRate;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILPlayListRevision;
import fr.hd3d.model.lightweight.impl.LPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.PlayListRevisionH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.EnumUtils;
import fr.hd3d.utils.TranslatorUtil;


public class PlayListRevisionTranslator extends BaseTranslator<ILPlayListRevision, IPlayListRevision>
{

    public IPlayListRevision fromLightweight(ILPlayListRevision light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        IPlayListRevision playListRevision = new PlayListRevisionH();

        if (BaseH.isValidId(light.getProject()))
            playListRevision.setProject(Persistors.project.getById(light.getProject()));

        playListRevision.setFrameRate(EnumUtils.getEnumValueIgnoreCase(EFrameRate.class, light.getFrameRate()));
        playListRevision.setName(trim(light.getName()));
        playListRevision.setRevision(light.getRevision());
        playListRevision.setHook(light.getHook());
        playListRevision.setComment(light.getComment());

        List<Long> playListEdits = light.getPlayListEdits();
        CollectUtils.removeNull(playListEdits);
        if (BaseH.areValidIds(playListEdits))
            playListRevision.setPlayListEdits(Persistors.playlistedit.getByIds(null, playListEdits));

        if (BaseH.isValidId(light.getPlayListRevisionGroup()))
            playListRevision.setPlayListRevisionGroup(Persistors.playlistrevisiongroup.getById(light
                    .getPlayListRevisionGroup()));

        TranslatorUtil.fromLightWeightCommonFields(playListRevision, light, context);
        return playListRevision;
    }

    public ILPlayListRevision toLightweight(IPlayListRevision object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILPlayListRevision playListRevision = LPlayListRevision.getNewInstance(object.getId(), object.getVersion(),
                null, nullSafeId(object.getProject()), object.getFrameRate().toString(), object.getName(),
                object.getRevision(), object.getHook(), object.getComment(),
                CollectUtils.getIds(object.getPlayListEdits()), null);

        TranslatorUtil.toLightWeightCommonFields(object, playListRevision, context);
        return playListRevision;
    }

    public void updateFromLightweight(final ILPlayListRevision light, final IPlayListRevision object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILPlayListRevision, IPlayListRevision> updater = new TranslatorUtil<ILPlayListRevision, IPlayListRevision>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("project").update();

        updater.updateField("frameRate").update();

        updater.updateField("name").update();

        updater.updateField("revision").update();

        updater.updateField("hook").update();

        updater.updateField("comment").update();

        updater.updateField("playListEdits").update();

        updater.updateField("playListRevisionGroup").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILPlayListRevision light, IPlayListRevision object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid playlist revision name");

        return resp;
    }
}
