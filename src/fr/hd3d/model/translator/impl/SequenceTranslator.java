package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.lightweight.impl.LSequence;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.SequenceH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class SequenceTranslator extends BaseTranslator<ILSequence, ISequence>
{
    public ISequence fromLightweight(ILSequence light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ISequence sequence = new SequenceH();

        sequence.setName(trim(light.getName()));
        sequence.setTitle(trim(light.getTitle()));
        // Project
        Long projectId = light.getProject();
        if (BaseH.isValidId(projectId))
            sequence.setProject(Persistors.project.getById(projectId));

        // Parent sequence
        Long parentId = resolveParentId(light);
        if (BaseH.isValidId(parentId))
            sequence.setParent(Persistors.sequence.getById(parentId));
        else
            sequence.setParent(null);

        sequence.setHook(trim(light.getHook()));

        TranslatorUtil.fromLightWeightCommonFields(sequence, light, context);
        return sequence;
    }

    public ILSequence toLightweight(ISequence object, ResourceContext<?, ?> context) throws Hd3dException
    {
        if (object == null)
            return null;

        // Sequence parentId
        // may be 0
        // TODO find out a better representation for "no parent"
        ILSequence sequence = LSequence.getNewInstance(object.getId(), object.getVersion(), null, object.getName(),
                object.getTitle(), resolveParentId(object), nullSafeId(object.getProject()), object.getHook());
        TranslatorUtil.toLightWeightCommonFields(object, sequence, context);
        return sequence;
    }

    private Long resolveParentId(ILSequence light)
    {
        if (light == null || light.getParent() == null)
            return null;

        return light.getParent();
    }

    private Long resolveParentId(ISequence object)
    {
        if (object == null || object.getParent() == null)
            return null;

        return object.getParent().getId();
    }

    public void updateFromLightweight(final ILSequence light, final ISequence object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILSequence, ISequence> updater = new TranslatorUtil<ILSequence, ISequence>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("title").update();

        updater.updateField("parent").update();

        updater.updateField("project").update();

        updater.updateField("hook").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILSequence light, ISequence object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid sequence name");

        if (light.getProject() != null)
            if (BaseH.isNotValidId(light.getProject()))
                resp.logErr("Invalid project id");

        // a sequence parent cannot be itself or one of its children
        if (light.getId() != null)
            if (light.getId().equals(light.getParent()))
                resp.logErr("the sequence parent cannot be itself");

        if (object != null)
        {
            // a sequence parent cannot be one of its children
            if (CollectUtils.getIds(object.allChildren(false)).contains(light.getParent()))
                resp.logErr("the sequence parent cannot be one of its children");
        }

        return resp;
    }
}
