package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.audit.UpdatesStack;
import fr.hd3d.model.lightweight.impl.LUpdatesStack;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;


/**
 * Transform Object in their Lightweight form and vice-versa. Also handle database update from Lightweight form.
 * 
 * @author Try LAM
 */
public class UpdatesStackTranslator implements IBaseTranslator<LUpdatesStack, UpdatesStack>
{
    public static String trim(String s)
    {
        return StringUtils.trim(s);
    }

    public UpdatesStack fromLightweight(LUpdatesStack light, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        // if (light == null)
        // return null;
        //
        // UpdatesStack updatesStack = new UpdatesStack(light.getTransactionUUID(), light.getOperation());

        /* not allowed to create from outside */
        return null;
    }

    public LUpdatesStack toLightweight(UpdatesStack object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        LUpdatesStack updatesStack = new LUpdatesStack(object.getId(), object.getVersion(), object.getOperation());

        return updatesStack;
    }

    public void updateFromLightweight(final LUpdatesStack light, final UpdatesStack object,
            final ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
    /* not allowed to update */
    }

    public List<UpdatesStack> fromLightweightCollection(Collection<LUpdatesStack> lights, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        /* not allowed to create from outside */
        return null;
    }

    public List<LUpdatesStack> toLightweightCollection(Collection<UpdatesStack> objects, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        List<LUpdatesStack> ret = new ArrayList<LUpdatesStack>();

        CollectUtils.removeNull(objects);

        if (CollectUtils.isNotEmpty(objects))
        {
            for (UpdatesStack object : objects)
            {
                ret.add(toLightweight(object, context));
            }
        }

        return ret;
    }

    public void updateFromLightweightCollection(List<LUpdatesStack> lights, List<UpdatesStack> objects,
            ResourceContext<?, ?> context) throws Hd3dException
    {
    /* not allowed to update */
    }

}
