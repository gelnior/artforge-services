package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILSoftware;
import fr.hd3d.model.lightweight.impl.LSoftware;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.SoftwareH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted software object to Lightweight software object and vice-versa. It also handles update from
 * lightweight to persisted software object.
 * 
 * @author HD3D
 */
public class SoftwareTranslator extends BaseTranslator<ILSoftware, ISoftware>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public ISoftware fromLightweight(ILSoftware light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ArrayList<ILicense> licenses = new ArrayList<ILicense>();
        final List<Long> licenseIDs = light.getLicenseIDs();
        CollectUtils.removeNull(licenseIDs);
        if (BaseH.areValidIds(licenseIDs))
        {
            licenses.ensureCapacity(licenseIDs.size());
            licenses = (ArrayList<ILicense>) Persistors.license.getByIds(null, licenseIDs);
        }
        else
            licenses = new ArrayList<ILicense>();

        ArrayList<IComputer> computers = new ArrayList<IComputer>();
        final List<Long> computerIDs = light.getComputerIDs();
        CollectUtils.removeNull(computerIDs);
        if (BaseH.areValidIds(computerIDs))
        {
            computers.ensureCapacity(computerIDs.size());
            computers = (ArrayList<IComputer>) Persistors.computer.getByIds(null, computerIDs);
        }
        else
            computers = new ArrayList<IComputer>();

        ISoftware software = new SoftwareH(trim(light.getName()), new HashSet<ILicense>(licenses),
                new HashSet<IComputer>(computers), null);

        TranslatorUtil.fromLightWeightCommonFields(software, light, context);

        return software;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILSoftware toLightweight(ISoftware object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILSoftware software = LSoftware.getNewInstance(object.getId(), object.getVersion(), null, object
                .getName());

        for (IComputer computer : object.getComputers())
        {
            software.addComputerID(computer.getId());
            software.addComputerName(computer.getName());
        }

        for (ILicense license : object.getLicenses())
        {
            software.addLicenseID(license.getId());
            software.addLicenseName(license.getName());
        }

        TranslatorUtil.toLightWeightCommonFields(object, software, context);

        return software;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILSoftware light, final ISoftware object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILSoftware, ISoftware> updater = new TranslatorUtil<ILSoftware, ISoftware>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        if (light.getLicenseIDs() != null)
        {
            for (ILicense license : object.getLicenses())
            {
                license.setSoftware(null);
            }

            object.merge(object.getLicenses(), light.getLicenseIDs(), ILicense.class);

            for (ILicense license : object.getLicenses())
            {
                license.setSoftware(object);
            }
        }

        updater.updateField("computers").fromField("computerIDs").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILSoftware light, ISoftware object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid name");

        return resp;
    }
}
