package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILTag;
import fr.hd3d.model.lightweight.impl.LTag;
import fr.hd3d.model.lightweight.impl.LTag.LTagParentIdType;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.TagH;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class TagTranslator extends BaseTranslator<ILTag, ITag>
{
    public ITag fromLightweight(ILTag light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ITag tag = new TagH();
        tag.setName(trim(light.getName()));
        tag.setInverseTagParents(new ArrayList<TagParent>(newTagParents(light.getParents(), tag)));// workaround
        TranslatorUtil.fromLightWeightCommonFields(tag, light, context);
        return tag;
    }

    public ILTag toLightweight(ITag object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILTag tag = LTag.getNewInstance(object.getId(), object.getVersion(), null, object.getName(), object
                .parentsIdType());
        TranslatorUtil.toLightWeightCommonFields(object, tag, context);
        return tag;
    }

    public void updateFromLightweight(final ILTag light, final ITag object, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILTag, ITag> updater = new TranslatorUtil<ILTag, ITag>(this).withObject(object)
                .withLightObject(light);

        updater.updateField("name").update();

        Collection<LTag.LTagParentIdType> updatedParentsIdTypes = light.getParents();
        if (updatedParentsIdTypes != null)
        {
            // remove parents that are no more in ltag
            if (CollectUtils.isNotEmpty(object.parentsIdType()))
            {
                for (LTag.LTagParentIdType oldParentIdType : object.parentsIdType())
                {
                    if (updatedParentsIdTypes.contains(oldParentIdType))
                        continue;
                    String oldParentType = oldParentIdType.getParentType();
                    Long oldParentId = oldParentIdType.getParentId();
                    object.removeInverseTagParent(oldParentId, oldParentType);
                }
            }
            /* when one wants to remove all tags from an object */
            else if (object.parentsIdType() != null && object.parentsIdType().isEmpty())
            {
                object.getInverseTagParents().clear();
            }
            // add new parents
            List<LTag.LTagParentIdType> oldParentsIdTypes = object.parentsIdType();
            for (LTag.LTagParentIdType newParent : light.getParents())
            {
                if (oldParentsIdTypes.contains(newParent))
                    continue;
                IBase parent = getParentObject(newParent.getParentType(), newParent.getParentId());
                object.addParent(parent);
            }
        }
    }

    private Set<TagParent> newTagParents(Collection<LTagParentIdType> lParents, ITag tag)
            throws Hd3dTranslationException
    {
        Set<TagParent> parents = new HashSet<TagParent>();

        if (lParents == null || lParents.isEmpty() || tag == null)
            return parents;

        for (LTagParentIdType t : lParents)
            CollectionUtils.addIgnoreNull(parents, newTagParent(t, tag));

        return parents;
    }

    private TagParent newTagParent(LTagParentIdType lParent, ITag tag) throws Hd3dTranslationException
    {
        if (lParent == null || tag == null)
            return null;

        TagParent tagparent = new TagParent();
        Long parentId = lParent.getParentId();// id of the parent
        tagparent.setParentType(lParent.getParentType());
        tagparent.setTag(tag);
        tagparent.setParent(parentId);
        return tagparent;
    }

    // given the id and the type of the parent, retrieve the parent object
    private IBase getParentObject(String type, Long id) throws Hd3dTranslationException
    {
        IBase parent = null;
        try
        {
            IPersist<?> persistor = Persistors.getPersistor(type);
            parent = (IBase) persistor.getById(id);
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            throw new Hd3dTranslationException("unable to retrieve tag parent object: id=" + id + ", type=" + type);
        }
        return parent;
    }

    @Override
    public ValidationResponse validateLightWeight(ILTag light, ITag object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid tag name");

        return resp;
    }
}
