package fr.hd3d.model.translator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.lightweight.impl.LRole;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.templates.IRoleTemplate;
import fr.hd3d.services.security.templates.RoleTemplateH;
import fr.hd3d.utils.TranslatorUtil;


public class RoleTemplateTranslator extends BaseTranslator<ILRole, IRoleTemplate>
{

    public IRoleTemplate fromLightweight(ILRole light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        List<String> permissions = light.getPermissions();
        if (permissions == null)
            permissions = new ArrayList<String>(0);

        List<String> bans = light.getBans();
        if (bans == null)
            bans = new ArrayList<String>(0);

        IRoleTemplate role = new RoleTemplateH(light.getName(), new HashSet<String>(permissions), new HashSet<String>(
                bans));

        TranslatorUtil.fromLightWeightCommonFields(role, light, context);
        return role;
    }

    public ILRole toLightweight(IRoleTemplate object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILRole role = LRole.getNewInstance(object.getId(), object.getVersion(), null, object.getName());
        role.addPermissions(new ArrayList<String>(object.getPermissions()));
        role.addBans(new ArrayList<String>(object.getBans()));
        TranslatorUtil.toLightWeightCommonFields(object, role, context);
        return role;
    }

    public void updateFromLightweight(final ILRole light, final IRoleTemplate object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        final TranslatorUtil<ILRole, IRoleTemplate> updater = new TranslatorUtil<ILRole, IRoleTemplate>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        // updater.updateField("permissions").update();
        if (light.getPermissions() != null)
        {
            object.setPermissions(new LinkedHashSet<String>(light.getPermissions()));
        }

        // updater.updateField("bans").update();
        if (light.getBans() != null)
        {
            object.setBans(new LinkedHashSet<String>(light.getBans()));
        }
    }

    @Override
    public ValidationResponse validateLightWeight(ILRole light, IRoleTemplate object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (org.apache.commons.lang.StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid role name");

        return resp;
    }

}
