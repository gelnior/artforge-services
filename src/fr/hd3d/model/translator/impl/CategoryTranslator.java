package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.lightweight.impl.LCategory;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.CategoryH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class CategoryTranslator extends BaseTranslator<ILCategory, ICategory>
{
    public ICategory fromLightweight(ILCategory light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        ICategory category = new CategoryH();
        category.setName(trim(light.getName()));
        // Project
        Long projectId = light.getProject();
        if (BaseH.isValidId(projectId))
            category.setProject(Persistors.project.getById(projectId));

        // Parent category
        Long parentId = resolveParentId(light);

        ICategory categoryParent = null;
        if (BaseH.isValidId(parentId))
            categoryParent = Persistors.category.getById(parentId);
        category.setParent(categoryParent);

        TranslatorUtil.fromLightWeightCommonFields(category, light, context);
        return category;
    }

    public ILCategory toLightweight(ICategory object, ResourceContext<?, ?> context) throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        // Category parentId
        // may be 0
        // TODO find out a better representation for "no parent"
        // Long parentId = (object.getParent() == null) ? 0 : object.getParent()
        // .getId();
        ILCategory category = LCategory.getNewInstance(object.getId(), object.getVersion(), null, object.getName(),
                resolveParentId(object), nullSafeId(object.getProject()));
        TranslatorUtil.toLightWeightCommonFields(object, category, context);
        return category;
    }

    private Long resolveParentId(ILCategory lCategory)
    {
        if (lCategory == null || lCategory.getParent() == null)
            return null;

        return lCategory.getParent();
    }

    private Long resolveParentId(ICategory category)
    {
        if (category == null || category.getParent() == null)
            return null;

        return category.getParent().getId();
    }

    public void updateFromLightweight(final ILCategory light, final ICategory object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILCategory, ICategory> updater = new TranslatorUtil<ILCategory, ICategory>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();

        updater.updateField("parent").update();

        updater.updateField("project").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILCategory light, ICategory object) throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid category name");

        if (light.getProject() != null)
            if (BaseH.isNotValidId(light.getProject()))
                resp.logErr("Invalid project id");

        // a category parent cannot be itself or one of its children
        if (light.getId() != null)
            if (light.getId().equals(light.getParent()))
                resp.logErr("the category parent cannot be itself");

        if (object != null)
        {
            // a category parent cannot be one of its children
            if (CollectUtils.getIds(object.allChildren(true)).contains(light.getParent()))
                resp.logErr("the category parent cannot be one of its children");
        }

        return resp;
    }
}
