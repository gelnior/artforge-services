package fr.hd3d.model.translator.impl;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.common.client.enums.EFileStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.lightweight.impl.LFileRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionH;
import fr.hd3d.services.resolver.MountPointResolver;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TranslatorUtil;


public class FileRevisionTranslator extends BaseTranslator<ILFileRevision, IFileRevision>
{

    public IFileRevision fromLightweight(ILFileRevision light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IFileRevision file = new FileRevisionH();

        file.setCreationDate(light.getCreationDate());
        file.setKey(trim(light.getKey()));
        file.setVariation(trim(light.getVariation()));
        file.setRevision(light.getRevision());
        if (BaseH.isValidId(light.getCreator()))
            file.setCreator(Persistors.person.getById(light.getCreator()));
        file.setFormat(trim(light.getFormat()));
        file.setSize(light.getSize());
        file.setChecksum(trim(light.getChecksum()));
        file.setLocation(trim(light.getLocation()));
        file.setRelativePath(trim(light.getRelativePath()));
        file.setStatus(EFileStatus.valueOf(trim(light.getStatus())));
        file.setLastOperationDate(light.getLastOperationDate());
        if (BaseH.isValidId(light.getLastUser()))
            file.setLastUser(Persistors.person.getById(light.getLastUser()));
        file.setComment(trim(light.getComment()));
        file.setValidity(trim(light.getValidity()));
        file.setSequence(trim(light.getSequence()));
        final Set<Long> attributes = light.getAttributes();
        CollectUtils.removeNull(attributes);
        if (BaseH.areValidIds(attributes))
            file.setAttributes(new HashSet<IFileAttribute>(Persistors.fileattribute.getByIds(null, attributes)));
        file.setUriScheme(light.getUriScheme());
        if (BaseH.isValidId(light.getAssetRevision()))
        {
            IAssetRevision assetRevision = Persistors.assetrevision.getById(light.getAssetRevision());
            file.setAssetRevision(assetRevision);
        }

        file.setState(EFileState.valueOf(trim(light.getState())));

        final Set<Long> proxies = light.getProxies();
        CollectUtils.removeNull(attributes);
        if (BaseH.areValidIds(proxies))
            file.setProxies(new HashSet<IProxy>(Persistors.proxy.getByIds(null, proxies)));

        TranslatorUtil.fromLightWeightCommonFields(file, light, context);
        return file;
    }

    public ILFileRevision toLightweight(IFileRevision object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILFileRevision file = LFileRevision.getNewInstance(object.getId(), object.getVersion(), null, object
                .getCreationDate(), object.getKey(), object.getVariation(), object.getRevision(), nullSafeId(object
                .getCreator()), object.getFormat(), object.getSize(), object.getChecksum(), object.getLocation(),
                object.getRelativePath(), object.getStatus() == null ? null : object.getStatus().toString(), object
                        .getLastOperationDate(), nullSafeId(object.getLastUser()), object.getComment(), object
                        .getValidity(), object.getSequence(), nullSafeId(object.getAssetRevision()),
                object.getState() == null ? null : object.getState().toString(), object.getMountPoint());

        if (object.getCreator() != null)
            file.setCreatorName(object.getCreator().fullName());
        if (object.getLastUser() != null)
            file.setLastUserName(object.getLastUser().fullName());
        file.setAttributes(new HashSet<Long>(CollectUtils.getIds(object.getAttributes())));
        file.setUriScheme(object.getUriScheme());

        file.setProxies(new HashSet<Long>(CollectUtils.getIds(object.getProxies())));

        if (object.getAssetRevision() != null)
        {
            file.setTaskTypeName(object.getAssetRevision().getTaskType().getName());
            file.setMountPoint(MountPointResolver.getResolvedMountPoint(context.getResource().getClientInfo()
                    .getAgent(), object.getAssetRevision().getMountPointByState(object.getState())));
        }

        TranslatorUtil.toLightWeightCommonFields(object, file, context);
        return file;
    }

    public void updateFromLightweight(final ILFileRevision light, final IFileRevision object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILFileRevision, IFileRevision> updater = new TranslatorUtil<ILFileRevision, IFileRevision>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("creationDate").update();

        updater.updateField("key").update();

        updater.updateField("variation").update();

        updater.updateField("revision").update();

        updater.updateField("creator").update();

        updater.updateField("format").update();

        updater.updateField("size").update();

        updater.updateField("checkSum").update();

        updater.updateField("location").update();

        updater.updateField("relativePath").update();

        updater.updateField("status").update();

        updater.updateField("lastOperationDate").update();

        updater.updateField("lastUser").update();

        updater.updateField("comment").update();

        updater.updateField("validity").update();

        updater.updateField("sequence").update();

        updater.updateField("assetRevision").acceptNull().update();

        updater.updateField("state").update();

        final Set<Long> proxies = light.getProxies();
        CollectUtils.removeNull(proxies);
        if (light.getProxies() != null)
        {
            object.merge(object.getProxies(), light.getProxies(), IProxy.class);
        }

        // updater.updateField("attributes").update();
        final Set<Long> attributes = light.getAttributes();
        CollectUtils.removeNull(attributes);
        if (light.getAttributes() != null)
        {
            object.merge(object.getAttributes(), light.getAttributes(), IFileAttribute.class);
        }

        updater.updateField("uriScheme").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILFileRevision light, IFileRevision object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getKey() != null)
            if (StringUtils.isBlank(light.getKey()))
                resp.logErr("Invalid file key");

        if (light.getVariation() != null)
            if (StringUtils.isBlank(light.getVariation()))
                resp.logErr("Invalid file variation");

        if (light.getFormat() != null)
            if (StringUtils.isBlank(light.getFormat()))
                resp.logErr("Invalid file format");

        if (light.getLocation() != null)
            if (StringUtils.isBlank(light.getLocation()))
                resp.logErr("Invalid file location");

        if (light.getRelativePath() != null)
            if (StringUtils.isBlank(light.getRelativePath()))
                resp.logErr("Invalid file relative path");

        if (light.getCreator() != null)
            if (BaseH.isNotValidId(light.getCreator()))
                resp.logErr("Invalid creator id");

        if (light.getLastUser() != null)
            if (BaseH.isNotValidId(light.getLastUser()))
                resp.logErr("Invalid last user id");

        if (light.getState() != null)
            if (StringUtils.isBlank(light.getState()))
                resp.logErr("Invalid file state");

        return resp;
    }
}
