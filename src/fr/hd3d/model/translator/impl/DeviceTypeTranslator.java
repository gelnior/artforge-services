package fr.hd3d.model.translator.impl;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILDeviceType;
import fr.hd3d.model.lightweight.impl.LDeviceType;
import fr.hd3d.model.persistence.IDeviceType;
import fr.hd3d.model.persistence.impl.hibernate.DeviceTypeH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


/**
 * Translates Persisted device object to Lightweight device object and vice-versa. It also handles update from
 * lightweight to persisted device object.
 * 
 * @author HD3D
 */
public class DeviceTypeTranslator extends BaseTranslator<ILDeviceType, IDeviceType>
{
    /**
     * @see fr.hd3d.model.translator.ITranslator#fromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    public IDeviceType fromLightweight(ILDeviceType light, ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, null);

        IDeviceType deviceType = new DeviceTypeH(trim(light.getName()));

        TranslatorUtil.fromLightWeightCommonFields(deviceType, light, context);
        return deviceType;
    }

    /**
     * @see fr.hd3d.model.translator.ITranslator#toLightweight(fr.hd3d.model.persistence.IBaseObject)
     */
    public ILDeviceType toLightweight(IDeviceType object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        final ILDeviceType deviceType = LDeviceType.getNewInstance(object.getId(), object.getVersion(), null, object
                .getName());

        TranslatorUtil.toLightWeightCommonFields(object, deviceType, context);
        return deviceType;
    }

    /**
     * @throws Hd3dException
     * @see fr.hd3d.model.translator.ITranslator#updateFromLightweight(fr.hd3d.model.lightweight.ILightweightBaseObject,
     *      fr.hd3d.model.persistence.IBaseObject)
     */
    public void updateFromLightweight(final ILDeviceType light, final IDeviceType object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILDeviceType, IDeviceType> updater = new TranslatorUtil<ILDeviceType, IDeviceType>(this)
                .withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("name").update();
    }

    /**
     * @see fr.hd3d.model.translator.impl.BaseTranslator#validateLightWeight(fr.hd3d.model.lightweight.ILightweightBaseObject)
     */
    @Override
    public ValidationResponse validateLightWeight(ILDeviceType light, IDeviceType object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getName() != null)
        {
            if (StringUtils.isBlank(light.getName()))
                resp.logErr("Invalid deviceType name");
        }

        return resp;
    }
}
