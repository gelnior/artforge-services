package fr.hd3d.model.translator.impl;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILConstituentLink;
import fr.hd3d.model.lightweight.impl.LConstituentLink;
import fr.hd3d.model.persistence.IConstituentLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ConstituentLinkH;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.utils.TranslatorUtil;


public class ConstituentLinkTranslator extends BaseTranslator<ILConstituentLink, IConstituentLink>
{

    public IConstituentLink fromLightweight(ILConstituentLink light, ResourceContext<?, ?> context)
            throws Hd3dException
    {
        validate(light, null);

        IConstituentLink constituentlink = new ConstituentLinkH();

        if (BaseH.isValidId(light.getMaster()))
            constituentlink.setMaster(Persistors.constituent.getById(light.getMaster()));
        if (BaseH.isValidId(light.getSlave()))
            constituentlink.setSlave(Persistors.constituent.getById(light.getSlave()));
        constituentlink.setType(trim(light.getType()));

        TranslatorUtil.fromLightWeightCommonFields(constituentlink, light, context);
        return constituentlink;
    }

    public ILConstituentLink toLightweight(IConstituentLink object, ResourceContext<?, ?> context)
            throws Hd3dTranslationException
    {
        if (object == null)
            return null;

        ILConstituentLink constituentlink = LConstituentLink.getNewInstance(object.getId(), object.getVersion(), null,
                nullSafeId(object.getMaster()), nullSafeId(object.getSlave()), object.getType());

        TranslatorUtil.toLightWeightCommonFields(object, constituentlink, context);
        return constituentlink;
    }

    public void updateFromLightweight(final ILConstituentLink light, final IConstituentLink object,
            final ResourceContext<?, ?> context) throws Hd3dException
    {
        validate(light, object);

        final TranslatorUtil<ILConstituentLink, IConstituentLink> updater = new TranslatorUtil<ILConstituentLink, IConstituentLink>(
                this).withObject(object).withLightObject(light);

        TranslatorUtil.updateCommonFields(object, light, context);

        updater.updateField("master").update();

        updater.updateField("slave").update();

        updater.updateField("type").update();
    }

    @Override
    public ValidationResponse validateLightWeight(ILConstituentLink light, IConstituentLink object)
            throws Hd3dTranslationException
    {
        ValidationResponse resp = super.validateLightWeight(light, object);

        if (light.getMaster() != null)
            if (BaseH.isNotValidId(light.getMaster()))
                resp.logErr("Invalid Master constituent id");

        if (light.getSlave() != null)
            if (BaseH.isNotValidId(light.getSlave()))
                resp.logErr("Invalid Slave constituent  id");

        return resp;
    }
}
