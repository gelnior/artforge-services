package fr.hd3d.model.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import fr.hd3d.services.resources.collectionquery.AbstractCollectionQuery;


/**
 * 
 * 
 * @author Try LAM
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Hd3dCollectionQuery
{
    Class<? extends AbstractCollectionQuery> value();
}
