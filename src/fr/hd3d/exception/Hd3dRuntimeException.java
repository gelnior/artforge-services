/**
 * 
 */
package fr.hd3d.exception;

/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class Hd3dRuntimeException extends RuntimeException
{
    /**
     * 
     */
    private static final long serialVersionUID = 1138711140322722104L;

    /**
     * 
     * @param msg
     */
    public Hd3dRuntimeException(final String msg)
    {
        super(msg);
    }
}
