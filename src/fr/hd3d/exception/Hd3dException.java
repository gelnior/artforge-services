package fr.hd3d.exception;

/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class Hd3dException extends Exception
{
    private static final long serialVersionUID = -7517751668247317645L;
    protected boolean retry = true;

    /**
     * @param message
     * @param cause
     */
    public Hd3dException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public Hd3dException(final String message, final Throwable cause, final boolean retry)
    {
        super(message, cause);
        this.retry = retry;
    }

    /**
     * @param message
     */
    public Hd3dException(String message)
    {
        super(message);
    }

    public Hd3dException(String message, boolean retry)
    {
        super(message);
        this.retry = retry;
    }

    public Hd3dException(Hd3dException exception)
    {
        this(exception.getMessage(), exception, exception.isRetry());
    }

    /**
     * @param cause
     */
    public Hd3dException(Throwable cause)
    {
        super(cause);
        if (cause instanceof Hd3dException)
        {
            this.setRetry(((Hd3dException) cause).isRetry());
        }
    }

    public Hd3dException(Throwable cause, boolean retry)
    {
        super(cause);
        this.retry = retry;
    }

    public boolean isRetry()
    {
        return retry;
    }

    public void setRetry(boolean retry)
    {
        this.retry = retry;
    }
}
