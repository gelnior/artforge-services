package fr.hd3d.exception;

public class Hd3dAuthorizationException extends Hd3dException
{
    private static final long serialVersionUID = -3220910559851938485L;

    /**
     * @param message
     * @param cause
     */
    public Hd3dAuthorizationException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * @param message
     */
    public Hd3dAuthorizationException(final String message)
    {
        super(message);
    }

    /**
     * @param cause
     */
    public Hd3dAuthorizationException(final Throwable cause)
    {
        super(cause);
    }
}
