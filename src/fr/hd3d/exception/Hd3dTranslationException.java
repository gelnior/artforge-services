package fr.hd3d.exception;

/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class Hd3dTranslationException extends Hd3dException
{
    private static final long serialVersionUID = -3220910559861938484L;

    /**
     * @param message
     * @param cause
     */
    public Hd3dTranslationException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * @param message
     */
    public Hd3dTranslationException(final String message)
    {
        super(message);
    }

    /**
     * @param cause
     */
    public Hd3dTranslationException(final Throwable cause)
    {
        super(cause);
    }
}
