package fr.hd3d.exception;

/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class Hd3dPersistenceException extends Hd3dTranslationException
{
    private static final long serialVersionUID = -1800418353799849959L;

    /**
     * @param message
     * @param cause
     */
    public Hd3dPersistenceException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * @param message
     */
    public Hd3dPersistenceException(final String message)
    {
        super(message);
    }

    /**
     * @param cause
     */
    public Hd3dPersistenceException(final Throwable cause)
    {
        super(cause);
    }
}
