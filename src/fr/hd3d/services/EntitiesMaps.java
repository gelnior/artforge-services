package fr.hd3d.services;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.utils.CollectUtils;


/**
 * Contains maps for miscellaneous conversions. These maps are populated on start
 * 
 * @author Try LAM
 * 
 */
public final class EntitiesMaps
{
    private String entityName;
    private String interfaceName;
    private Class<?> clazz;
    private String fullClassName;

    /* key = T (interface), value = the bound IPersist<T> */
    private static Map<String, IPersist<?>> interfaceToPersistorMap;
    /* key = T (class), value = the bound IPersist<T> */
    private static Map<String, IPersist<?>> classNameToPersistorMap;

    /* key = T (class), value = super classes name */
    private static MultiValueMap classUpHierarchyNamesMap;
    private static MultiValueMap classUpHierarchyMap;

    /* key = T (class), value = derived classes name */
    private static MultiValueMap classDownHierarchyNamesMap;
    private static MultiValueMap classDownHierarchyMap;

    /* key = entity name, value = corresponding class name */
    private static Map<String, String> entityNameToClassNameMap;
    private static Map<String, Class<?>> entityNameToClassMap;
    /* key = class name, value = corresponding entity name */
    private static Map<String, String> classNameToEntityNameMap;
    private static Map<String, String> entityNameToInterfaceNameMap;
    private static Map<String, Class<?>> entityNameToInterfaceMap;
    private static Map<String, String> interfaceNameToEntityNameMap;
    /* Key=<canonical class name>.<property name>, value = property type */
    private static Map<String, Class<?>> classAttributeTypesMap;

    /* Key=<canonical class name>.<property name>, value = property parameter types */
    private static MultiValueMap classAttributeCollectionParameterTypesMap;

    /* Key=<canonical class name>.<property name>, value = hibernate association annotation (OneToOne, OneToMany...) */
    private static Map<String, Annotation> classAttributeAssociationsMap;
    /* Key=<canonical class name>.<property name>, value = hibernate association target entity */
    private static Map<String, Class<?>> classAttributeAssociationTargetsMap;

    private static Map<String, Map<String, String>> classAttributeHd3dCollectionQueryAnnotationMap;

    /* key = entity name, value = exposed (callable from IItem) */
    private static MultiValueMap entityExposedMethodsMap;

    private static Map<String, IBaseTranslator<?, ?>> interfaceToTranslatorMap;

    public static void clear()
    {
        /* Note: do not clear() EntitiesMaps's maps since they are unmodifiable (raises an exception) */
        EntitiesMaps.interfaceToPersistorMap = null;
        EntitiesMaps.classNameToPersistorMap = null;
        EntitiesMaps.classUpHierarchyNamesMap = null;
        EntitiesMaps.classUpHierarchyMap = null;
        EntitiesMaps.classDownHierarchyNamesMap = null;
        EntitiesMaps.classDownHierarchyMap = null;
        EntitiesMaps.entityNameToClassNameMap = null;
        EntitiesMaps.entityNameToClassMap = null;
        EntitiesMaps.classNameToEntityNameMap = null;
        EntitiesMaps.entityNameToInterfaceNameMap = null;
        EntitiesMaps.entityNameToInterfaceMap = null;
        EntitiesMaps.interfaceNameToEntityNameMap = null;
        EntitiesMaps.classAttributeTypesMap = null;
        EntitiesMaps.classAttributeCollectionParameterTypesMap = null;
        EntitiesMaps.classAttributeAssociationsMap = null;
        EntitiesMaps.classAttributeAssociationTargetsMap = null;
        EntitiesMaps.classAttributeHd3dCollectionQueryAnnotationMap = null;
        EntitiesMaps.entityExposedMethodsMap = null;
        EntitiesMaps.classAttributeHd3dCollectionQueryAnnotationMap = null;
    }

    public static EntitiesMaps withEntityName(final String entityName)
    {
        EntitiesMaps ret = new EntitiesMaps();
        ret.entityName = entityName;
        return ret;
    }

    public static EntitiesMaps withInterfaceName(final String interfaceName)
    {
        EntitiesMaps ret = new EntitiesMaps();
        ret.interfaceName = interfaceName;
        return ret;
    }

    public static EntitiesMaps withClass(final Class<?> clazz)
    {
        EntitiesMaps ret = new EntitiesMaps();
        ret.clazz = clazz;
        return ret;
    }

    public static EntitiesMaps withClassName(final String className)
    {
        EntitiesMaps ret = new EntitiesMaps();
        ret.fullClassName = className;
        return ret;
    }

    public IPersist<?> getPersistor()
    {
        IPersist<?> ret = null;

        if (this.entityName != null)
        {
            ret = entityNameToPersistor(entityName);
        }
        else if (this.interfaceName != null)
        {
            ret = interfaceNameToPersistor(interfaceName);
        }
        else if (this.fullClassName != null)
        {
            ret = classNameToPersistor(fullClassName);
        }
        else if (this.clazz != null)
        {
            ret = classNameToPersistor(clazz.getCanonicalName());
        }
        return ret;
    }

    public IBaseTranslator<?, ?> getTranslator()
    {
        IBaseTranslator<?, ?> ret = null;

        if (this.entityName != null)
        {
            ret = entityNameToTranslator(entityName);
        }
        else if (this.interfaceName != null)
        {
            ret = interfaceNameToTranslator(interfaceName);
        }
        else if (this.fullClassName != null)
        {
            ret = entityNameToTranslator(classNameToEntityName(this.fullClassName));
        }
        else if (this.clazz != null)
        {
            ret = entityNameToTranslator(classNameToEntityName(this.clazz.getCanonicalName()));
        }
        return ret;
    }

    public String getEntityName()
    {
        String ret = null;

        if (this.entityName != null)
        {
            ret = this.entityName;
        }
        else if (this.interfaceName != null)
        {
            ret = interfaceNameToEntityName(interfaceName);
        }
        else if (this.fullClassName != null)
        {
            ret = classNameToEntityName(fullClassName);
        }
        else if (this.clazz != null)
        {
            ret = classNameToEntityName(this.clazz.getCanonicalName());
        }
        return ret;
    }

    public String getFullClassName()
    {
        String ret = null;

        if (this.entityName != null)
        {
            ret = entityNameToClassName(entityName);
        }
        else if (this.interfaceName != null)
        {
            ret = entityNameToClassName(interfaceNameToEntityName(interfaceName));
        }
        else if (this.fullClassName != null)
        {
            ret = this.fullClassName;
        }
        else if (this.clazz != null)
        {
            ret = this.clazz.getCanonicalName();
        }
        return ret;
    }

    public Class<?> getInterface()
    {
        Class<?> ret = null;

        if (this.entityName != null)
        {
            ret = entityNameToInterface(entityName);
        }
        else if (this.interfaceName != null)
        {
            ret = entityNameToInterface(interfaceNameToEntityName(this.interfaceName));
        }
        else if (this.fullClassName != null)
        {
            ret = entityNameToInterface(classNameToEntityName(this.fullClassName));
        }
        else if (this.clazz != null)
        {
            ret = entityNameToInterface(classNameToEntityName(this.clazz.getCanonicalName()));
        }
        return ret;
    }

    public Class<?> getTypeOf(final String attribute)
    {
        Class<?> ret = null;

        if (this.entityName != null)
        {
            ret = classAttributeType(entityNameToClass(entityName), attribute);
        }
        else if (this.interfaceName != null)
        {
            ret = classAttributeType(entityNameToInterface(interfaceNameToEntityName(this.interfaceName)), attribute);
        }
        else if (this.fullClassName != null)
        {
            ret = classAttributeType(entityNameToClass(classNameToEntityName(this.fullClassName)), attribute);
        }
        else if (this.clazz != null)
        {
            ret = classAttributeType(this.clazz, attribute);
        }

        return ret;
    }

    public List<Class<?>> getCollectionParameterTypeOf(final String attribute)
    {
        List<Class<?>> ret = null;

        if (this.entityName != null)
        {
            ret = classAttributeCollectionParameterType(entityNameToClass(entityName), attribute);
        }
        else if (this.interfaceName != null)
        {
            ret = classAttributeCollectionParameterType(
                    entityNameToInterface(interfaceNameToEntityName(this.interfaceName)), attribute);
        }
        else if (this.fullClassName != null)
        {
            ret = classAttributeCollectionParameterType(entityNameToClass(classNameToEntityName(this.fullClassName)),
                    attribute);
        }
        else if (this.clazz != null)
        {
            ret = classAttributeCollectionParameterType(this.clazz, attribute);
        }

        return ret;
    }

    public Class<?> getTypeOfAssociationTarget(final String attribute)
    {
        Class<?> ret = null;

        if (this.entityName != null)
        {
            ret = classAttributeAssociationTargetType(entityNameToClass(entityName), attribute);
        }
        else if (this.interfaceName != null)
        {
            ret = classAttributeAssociationTargetType(
                    entityNameToInterface(interfaceNameToEntityName(this.interfaceName)), attribute);
        }
        else if (this.fullClassName != null)
        {
            ret = classAttributeAssociationTargetType(entityNameToClass(classNameToEntityName(this.fullClassName)),
                    attribute);
        }
        else if (this.clazz != null)
        {
            ret = classAttributeAssociationTargetType(this.clazz, attribute);
        }

        return ret;
    }

    public boolean isAssociation(final String attribute)
    {
        return classAttributeAssociationTargetType(clazz, attribute) != null;
    }

    public static void setInterfaceToPersistorMap(Map<String, IPersist<?>> interfaceToPersistorMap)
    {
        EntitiesMaps.interfaceToPersistorMap = interfaceToPersistorMap;
    }

    public static void setClassToPersistorMap(Map<String, IPersist<?>> classToPersistorMap)
    {
        EntitiesMaps.classNameToPersistorMap = classToPersistorMap;
    }

    public static void setClassUpHierarchyNamesMap(MultiValueMap classUpHierarchyNamesMap)
    {
        EntitiesMaps.classUpHierarchyNamesMap = classUpHierarchyNamesMap;
    }

    public static void setClassUpHierarchyMap(MultiValueMap classUpHierarchyMap)
    {
        EntitiesMaps.classUpHierarchyMap = classUpHierarchyMap;
    }

    public static void setClassDownHierarchyNamesMap(MultiValueMap classDownHierarchyNamesMap)
    {
        EntitiesMaps.classDownHierarchyNamesMap = classDownHierarchyNamesMap;
    }

    public static void setClassDownHierarchyMap(MultiValueMap classDownHierarchyMap)
    {
        EntitiesMaps.classDownHierarchyMap = classDownHierarchyMap;
    }

    public static void setEntityNameToClassNameMap(Map<String, String> entityNameToClassNameMap)
    {
        EntitiesMaps.entityNameToClassNameMap = entityNameToClassNameMap;
    }

    public static void setEntityNameToClassMap(Map<String, Class<?>> entityNameToClassMap)
    {
        EntitiesMaps.entityNameToClassMap = entityNameToClassMap;
    }

    public static void setClassNameToEntityNameMap(Map<String, String> classNameToEntityNameMap)
    {
        EntitiesMaps.classNameToEntityNameMap = classNameToEntityNameMap;
    }

    public static void setEntityNameToInterfaceNameMap(Map<String, String> entityNameToInterfaceNameMap)
    {
        EntitiesMaps.entityNameToInterfaceNameMap = entityNameToInterfaceNameMap;
    }

    public static void setEntityNameToInterfaceMap(Map<String, Class<?>> entityNameToInterfaceMap)
    {
        EntitiesMaps.entityNameToInterfaceMap = entityNameToInterfaceMap;
    }

    public static void setInterfaceNameToEntityNameMap(Map<String, String> interfaceNameToEntityNameMap)
    {
        EntitiesMaps.interfaceNameToEntityNameMap = interfaceNameToEntityNameMap;
    }

    public static void setClassAttributeTypesMap(Map<String, Class<?>> classAttributeTypesMap)
    {
        EntitiesMaps.classAttributeTypesMap = classAttributeTypesMap;
    }

    public static void setClassAttributeCollectionParameterTypesMap(
            MultiValueMap classAttributeCollectionParameterTypesMap)
    {
        EntitiesMaps.classAttributeCollectionParameterTypesMap = classAttributeCollectionParameterTypesMap;
    }

    public static void setClassAttributeAssociationsMap(Map<String, Annotation> classAttributeAssociationsMap)
    {
        EntitiesMaps.classAttributeAssociationsMap = classAttributeAssociationsMap;
    }

    public static void setClassAttributeAssociationTargetsMap(Map<String, Class<?>> classAttributeAssociationTargetsMap)
    {
        EntitiesMaps.classAttributeAssociationTargetsMap = classAttributeAssociationTargetsMap;
    }

    public static void setEntityExposedMethodsMap(MultiValueMap entityExposedMethodsMap)
    {
        EntitiesMaps.entityExposedMethodsMap = entityExposedMethodsMap;
    }

    public static void setInterfaceToTranslatorMap(Map<String, IBaseTranslator<?, ?>> interfaceToTranslatorMap)
    {
        EntitiesMaps.interfaceToTranslatorMap = interfaceToTranslatorMap;
    }

    public static void setClassAttributeHd3dCollectionQueryAnnotationMap(
            Map<String, Map<String, String>> classAttributeHd3dCollectionQueryAnnotationMap)
    {
        EntitiesMaps.classAttributeHd3dCollectionQueryAnnotationMap = classAttributeHd3dCollectionQueryAnnotationMap;
    }

    public static Set<String> allEntityClassNames()
    {
        return EntitiesMaps.classNameToPersistorMap.keySet();
    }

    public static Set<String> allEntityNames()
    {
        return EntitiesMaps.entityNameToClassNameMap.keySet();
    }

    public static boolean isExposed(String className, String methodName)
    {
        if (StringUtils.isBlank(className))
            return false;
        return entityExposedMethodsMap.getCollection(classNameToEntityNameMap.get(className)).contains(methodName);
    }

    public static boolean isNotExposed(String className, String methodName)
    {
        return !isExposed(className, methodName);
    }

    private static IPersist<?> entityNameToPersistor(String entityName)
    {
        return interfaceToPersistorMap.get(entityNameToInterfaceNameMap.get(entityName));
    }

    private static String entityNameToClassName(String entityName)
    {
        return EntitiesMaps.entityNameToClassNameMap.get(entityName);
    }

    private static Class<?> entityNameToClass(String entityName)
    {
        return EntitiesMaps.entityNameToClassMap.get(entityName);
    }

    private static Class<?> entityNameToInterface(String interfaceName)
    {
        return EntitiesMaps.entityNameToInterfaceMap.get(interfaceName);
    }

    private static IPersist<?> interfaceNameToPersistor(String interfaceName)
    {
        return interfaceToPersistorMap.get(interfaceName);
    }

    private static String interfaceNameToEntityName(String interfaceName)
    {
        return interfaceNameToEntityNameMap.get(interfaceName);
    }

    private static IPersist<?> classNameToPersistor(String className)
    {
        return classNameToPersistorMap.get(className);
    }

    private static String classNameToEntityName(String className)
    {
        return EntitiesMaps.classNameToEntityNameMap.get(className);
    }

    private static Class<?> classAttributeType(final Class<?> clazz, final String attribute)
    {
        final String key = new StringBuilder(clazz.getCanonicalName()).append('.').append(attribute).toString();
        return EntitiesMaps.classAttributeTypesMap.get(key);
    }

    private static List<Class<?>> classAttributeCollectionParameterType(final Class<?> clazz, final String attribute)
    {
        final String key = clazz.getCanonicalName() + '.' + attribute;
        return (List<Class<?>>) EntitiesMaps.classAttributeCollectionParameterTypesMap.get(key);
    }

    private static Class<?> classAttributeAssociationTargetType(final Class<?> clazz, final String attribute)
    {
        final String key = new StringBuilder(clazz.getCanonicalName()).append('.').append(attribute).toString();
        return EntitiesMaps.classAttributeAssociationTargetsMap.get(key);
    }

    private static IBaseTranslator<?, ?> entityNameToTranslator(String entityName)
    {
        return interfaceToTranslatorMap.get(entityNameToInterfaceNameMap.get(entityName));
    }

    private static IBaseTranslator<?, ?> interfaceNameToTranslator(String interfaceName)
    {
        return interfaceToTranslatorMap.get(interfaceName);
    }

    public static boolean isEntity(String entity)
    {
        return entityNameToInterfaceNameMap.keySet().contains(entity)
                || interfaceNameToEntityNameMap.keySet().contains(entity)
                || classNameToEntityNameMap.keySet().contains(entity);
    }

    public static boolean isNotEntity(String entity)
    {
        return !isEntity(entity);
    }

    public static Class<?> lastAttribbuteClass(Class<?> initialClass, String associationPath)
    {
        if (initialClass == null || StringUtils.isBlank(associationPath))
            return initialClass;

        /* if no '.' in associationPath, it is a final attribute and not an association */
        if (!associationPath.contains("."))
        {
            StringBuilder key = new StringBuilder(initialClass.getCanonicalName()).append('.').append(associationPath);
            key.trimToSize();
            return EntitiesMaps.classAttributeTypesMap.get(key.toString());
        }

        /* recursively find out the type of each attribute */
        String[] parts = org.apache.commons.lang.StringUtils.split(associationPath, '.');

        StringBuilder key = new StringBuilder(initialClass.getCanonicalName()).append('.').append(parts[0]);
        key.trimToSize();
        final Class<?> type = classAttributeAssociationTargetsMap.get(key.toString());

        return lastAttribbuteClass(type, StringUtils.join(CollectUtils.tail(parts), '.'));
    }

    public static Collection<String> classUpHierarchyNames(final String className)
    {
        return EntitiesMaps.classUpHierarchyNamesMap.getCollection(className);
    }

    public static Map<String, String> classAttributeHd3dCollectionQueryAnnotation(final Class<?> clazz)
    {
        return EntitiesMaps.classAttributeHd3dCollectionQueryAnnotationMap.get(clazz.getCanonicalName());
    }

}
