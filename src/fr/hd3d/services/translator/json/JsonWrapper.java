package fr.hd3d.services.translator.json;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * JsonWrapper helps to build proper Json code from the resource serialization. The serialization issued from the
 * JsonWrapper is formed like : { "numberRecords" : x, "records" : [ resource_01, ressource_02... resource_n ] } where [
 * resource_01, resource_02... resource_n] are the initial resources.
 * 
 * @author Frank Rousseau
 */
public class JsonWrapper
{

    /** Collection of objects to store in the json object */
    private Collection<Object> records;
    /** Size of records */
    private int nbRecords;
    /** True when the initial object is not a Collection */
    private boolean isSingleton;

    /**
     * Default constructor (empty wrapper).
     * 
     * @param object
     *            Object to encapsulate
     */
    public JsonWrapper()
    {
        this.nbRecords = 0;
        this.isSingleton = false;
    }

    /**
     * Set up records with parameter and automatically set number of records.
     * 
     * @param object
     *            Object to encapsulate
     */
    public JsonWrapper(final Object object)
    {
        if (object == null)
        {
            this.records = new ArrayList<Object>();
            this.nbRecords = 0;
        }
        else
        {
            this.setRecordsFromObject(object);
            this.nbRecords = this.records.size();
        }
    }

    /**
     * Set up records and number of records with parameters
     * 
     * @param object
     *            Object to wrap
     * @param nbRecords
     *            Number of records of the query without limit parameters
     */
    public JsonWrapper(final Object object, final int nbRecords)
    {
        if (object == null)
        {
            this.records = new ArrayList<Object>();
            this.nbRecords = 0;
        }
        else
        {
            this.setRecordsFromObject(object);
            this.nbRecords = nbRecords;
        }
    }

    /**
     * Number of records getter.
     * 
     * @return The number of records returned by the query
     */
    public int getNbRecords()
    {
        return nbRecords;
    }

    /**
     * Set the number of records.
     * 
     * @param nbRecords
     *            Number of records to set.
     */
    public void setNbRecords(final int nbRecords)
    {
        this.nbRecords = nbRecords;
    }

    /**
     * Records getter.
     * 
     * @return records in the Json object.
     */
    public Collection<Object> getRecords()
    {
        return this.records;
    }

    /**
     * IsSingle getter.
     * 
     * @return records in the json object.
     */
    public boolean getIsSingleton()
    {
        return this.isSingleton;
    }

    /**
     * IsSingle Setter.
     * 
     * @param nbRecords
     *            Number of records to set.
     */
    public void setIsSingleton(final boolean isSingleton)
    {
        this.isSingleton = isSingleton;
    }

    /**
     * Tells when the initial is a not a Collection.
     * 
     * @return true if the Initial object is not a Collection
     */
    public boolean isSingleton()
    {
        return this.isSingleton;
    }

    /**
     * Set the object list to add in the record field.
     * 
     * @param objects_list
     *            The list to set as the records.
     */
    public void setRecords(final List<Object> objects_list)
    {
        this.records = objects_list;
    }

    /**
     * Build the records list from a given object : 1. Set The list if the object is a Collection. 2. Create a new list
     * if the object is not a Collection. 3. Set the isSingle attribute.
     * 
     * @param object
     *            Object to wrap
     */
    @SuppressWarnings("unchecked")
    private void setRecordsFromObject(final Object object)
    {
        // If the object is already a collection, directly set records
        if (object instanceof Collection)
        {
            this.records = (Collection<Object>) object;
            this.isSingleton = false;
        }
        else
        {
            // Else add it to the records list.
            this.records = new ArrayList<Object>();
            this.records.add(object);
            this.isSingleton = true;
        }
    }
}
