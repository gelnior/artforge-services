package fr.hd3d.services.translator.json;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.sojo.interchange.AbstractSerializer;
import net.sf.sojo.interchange.Serializer;
import net.sf.sojo.interchange.json.JsonSerializer;
import fr.hd3d.common.client.DateFormat;


/**
 * Serializes into Json Code or unserializes from Json Code single objects or objects collection.
 * 
 * @author Thomas Eskénazi, Frank Rousseau
 */
public class Hd3dJsonSerializer
{
    public static Serializer getSerializer()
    {
        Serializer serializer = new JsonSerializer();
        /* set format date */
        ((AbstractSerializer) serializer).getObjectUtil().addFormatterForType(
                new SimpleDateFormat(DateFormat.DATE_TIME_STRING), Date.class);

        return serializer;
    }

    /**
     * Serializes a java object into conform jsonCode.
     * 
     * @param obj
     *            Single object or collection to serialize.
     * @return jsonCode expression of parameter object
     */
    public static String serialize(Object obj)
    {
        // Wrap the object to get proper Json Code.
        // Serialize the wrapped object.
        return getSerializer().serialize(new JsonWrapper(obj)).toString();
    }

    public static String serialize(Object obj, int nbRecords)
    {
        // Wrap the object to get proper Json Code.
        // Serialize the wrapped object.
        return getSerializer().serialize(new JsonWrapper(obj, nbRecords)).toString();
    }

    /**
     * Unserializes a jsonObject into a java Object.
     * 
     * @param jsonObject
     *            JSON object to unserialize.
     * @return Java object built from the jsonCode.
     */
    @SuppressWarnings("unchecked")
    public static <E> E unserialize(String jsonObject)
    {
        // Build the java object from the json Object.
        final Object object = getSerializer().deserialize(jsonObject);

        E ret;
        // When object is a JsonWrapper Object, it extracts the records.
        if (object instanceof JsonWrapper)
        {
            final JsonWrapper wrapper = (JsonWrapper) object;

            // Unwrap Object
            if (wrapper.isSingleton())
            {
                // Case when the initial object is not a collection.
                ret = (E) wrapper.getRecords().iterator().next();
            }
            else
            {
                // Case when the initial object is a collection.
                ret = (E) wrapper.getRecords();
            }
        }
        else
        {
            ret = (E) object;
        }

        // When it is not a JsonWrapper Object, return it without change.
        return ret;
    }

    /**
     * Get number of records field from a query.
     * 
     * @param jsonObject
     *            JSON object to unserialize.
     * @return Number of records return from the query without paginatin constraints
     */
    public static int unserializeNbRecords(String jsonObject)
    {
        int ret = 0;

        Object object = getJavaObjectFromJson(jsonObject);

        // When object is a JsonWrapper Object, it extracts the number of records.
        if (object instanceof JsonWrapper)
        {
            ret = ((JsonWrapper) object).getNbRecords();
        }

        // Else 0 is returned
        return ret;
    }

    /**
     * Transform a Json object into a java object.
     * 
     * @param jsonObject
     *            JSON object to unserialize.
     * @return Java Object from the JSON object.
     */
    public static Object getJavaObjectFromJson(String jsonObject)
    {
        return getSerializer().deserialize(jsonObject);
    }
}
