package fr.hd3d.services.translator.json;

import java.util.List;

import org.restlet.data.Form;
import org.restlet.representation.Representation;

import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public final class JSonFormSerializer
{
    private JSonFormSerializer()
    {}

    public static final String OBJECT = "object";

    public static Form serialize(final Object obj)
    {
        final Form form = new Form();
        form.add(OBJECT, Hd3dJsonSerializer.serialize(obj));
        return form;
    }

    @SuppressWarnings("unchecked")
    public static <E> E deserialize(final Representation entity)
    {
        E ret = null;

        try
        {
            ret = (E) Hd3dJsonSerializer.unserialize(entity.getText());
        }
        catch (Exception e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    public static <E> List<E> deserializeList(final Representation entity)
    {
        List<E> ret = null;

        try
        {
            ret = (List<E>) Hd3dJsonSerializer.unserialize(entity.getText());
        }
        catch (Exception e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }

        return ret;
    }
}
