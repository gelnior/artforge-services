package fr.hd3d.services;



public final class LicenseManager
{
    // private static RlmHandle handle = null;
    // private static RlmLicense license = null;
    // private static transient volatile boolean isLicenseOK = true;
    // private final static String PRODUCT = "artforge";
    // private final static String VER = "1.0";
    // private final static int COUNT = 1;
    //
    // private static final Long HEARBEAT_CHECKING_PERIOD = 14400L;// in seconds
    // private static final Long EXPIRATION_CHECKING_PERIOD = 86400L;// in seconds
    //
    // private static final ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(2);
    //
    // static
    // {
    // Log.LOGGER.info("In License Manager, PRODUCT=" + PRODUCT);
    // Log.LOGGER.info("In License Manager, VER=" + VER);
    // Log.LOGGER.info("In License Manager, COUNT=" + COUNT);
    // Log.LOGGER.info("In License Manager, hd3d.rlm.license.path=" + Conf.getRLMLicensePath());
    // Log.LOGGER.info("In License Manager, hd3d.rlm.server=" + Conf.getRLMServer());
    // }
    //
    // private LicenseManager()
    // {}
    //
    // public static boolean isLicenseOK()
    // {
    // return isLicenseOK;
    // }
    //
    // public static void checkout() throws Hd3dException
    // {
    // try
    // {
    // Log.LOGGER.info("Obtaining a license from license server (check out).");
    // handle = new RlmHandle(Conf.getRLMLicensePath(), Conf.getRLMServer(), "");
    // // handle = new RlmHandle(Conf.getRLMLicensePath(), Conf.getRLMLicensePath(), Conf.getRLMLicensePath());
    // license = new RlmLicense(handle, PRODUCT, VER, COUNT);
    // isLicenseOK = true;
    // Log.LOGGER.info("License obtained from license server (checked out).");
    // // checkLicenseHealth();
    // // checkExpiration();
    // }
    // catch (RlmException e)
    // {
    // Log.LOGGER.error("FAILED to obtain a license from license server.");
    // Log.LOGGER.error(ExceptUtils.format(e));
    // throw new Hd3dException(MessageFormat.format("Cannot checkout license: {0}", ExceptUtils.format(e)));
    // }
    // }
    //
    // public static void checkLicenseHealth()
    // {
    // final Runnable checker = new Runnable() {
    // public void run()
    // {
    // try
    // {
    // Log.LOGGER.info("Checking current license health status from license server.");
    // isLicenseOK = license.getAttrHealth() == 0;
    // if (isLicenseOK)
    // {
    // Log.LOGGER.info("Current license health : OK.");
    // }
    // else
    // {
    // Log.LOGGER.info("Current license health : NOT OK.");
    // }
    // }
    // catch (RlmException e)
    // {
    // Log.LOGGER.error("FAILED to obtain current license health status from license server.");
    // isLicenseOK = false;
    // }
    // }
    // };
    //
    // SCHEDULER.scheduleWithFixedDelay(checker, 0, HEARBEAT_CHECKING_PERIOD, SECONDS);
    // }
    //
    // public static void checkExpiration()
    // {
    // final Runnable checker = new Runnable() {
    // public void run()
    // {
    // Log.LOGGER.info("Checking current license expiration.");
    // final int daysToExpiration = license.daysToExpiration();
    // if (daysToExpiration < 0)
    // {
    // Log.LOGGER.info("Current license expired.");
    // checkin();
    // }
    // }
    // };
    //
    // SCHEDULER.scheduleWithFixedDelay(checker, 0, EXPIRATION_CHECKING_PERIOD, SECONDS);
    // }
    //
    // public static void checkin()
    // {
    // Log.LOGGER.info("Releasing license to license server (check in).");
    //
    // /* Check in the license */
    // if (license != null)
    // {
    // license.checkin();
    // }
    //
    // /* Close the handle */
    // if (handle != null)
    // {
    // handle.close();
    // }
    //
    // isLicenseOK = false;
    //
    // SCHEDULER.shutdown();
    //
    // Log.LOGGER.info("License Released to license server (checked in).");
    // }
}
