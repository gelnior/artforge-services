package fr.hd3d.services.resolver;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;


public class DynamicPathResolver
{
    private static IBase workObjectActive = null;
    private static ITaskType taskTypeActive = null;

    public static String getDynamicPathResolved(IFileRevision currentFile, boolean isProxy, String proxyName)
            throws Hd3dException
    {
        String directory = null;

        List<IDynamicPath> dynamicPaths = getDynamicPathsFromFileRevision(currentFile, isProxy);
        if (CollectionUtils.isEmpty(dynamicPaths))
        {
            throw new Hd3dException("no dynamic path found!!");
        }
        IDynamicPath dynamicPath = dynamicPaths.get(0);
        if (isProxy)
        {
            for (IDynamicPath dp : dynamicPaths)
            {
                if (dp.getName().equals(proxyName))
                {
                    dynamicPath = dp;
                }
            }
        }

        switch (currentFile.getState())
        {
            case WIP:
                directory = dynamicPath.getWipDynamicPath();
                break;
            case PUBLISH:
                directory = dynamicPath.getPublishDynamicPath();
                break;
            case OLD:
                directory = dynamicPath.getOldDynamicPath();
                break;
        }
        if (directory == null)
        {
            throw new Hd3dException(" No proxy dynamic path to the file => " + currentFile.getKey() + ", state =>"
                    + currentFile.getState());
        }
        directory = resolvePath(directory, dynamicPath);
        return directory;
    }

    private static String resolvePath(String path, IDynamicPath dynamicPath) throws Hd3dException
    {
        if (path == null)
        {
            throw new Hd3dException("resolvePath : The path is null!!");
        }
        String directory = null;

        directory = path.replace("${project}", dynamicPath.getProject().getName());
        if (workObjectActive != null)
        {
            if (IConstituent.class.isInstance(workObjectActive))
            {
                final IConstituent constituent = (IConstituent) workObjectActive;
                directory = directory.replace("${category}", constituent.getCategory().getName());
                directory = directory.replace("${constituent}", constituent.getLabel());
            }
            else if (IShot.class.isInstance(workObjectActive))
            {
                final IShot shot = (IShot) workObjectActive;
                directory = directory.replace("${shot}", shot.getLabel());
                directory = directory.replace("${sequence}", shot.getSequence().getName());
            }

        }
        if (taskTypeActive != null)
        {
            directory = directory.replace("${tasktype}", taskTypeActive.getName());
        }

        return directory;
    }

    private static List<IDynamicPath> getDynamicPathsFromFileRevision(IFileRevision currentFile, boolean isProxy)
            throws Hd3dException
    {
        String type = "file";
        if (isProxy)
        {
            type = "proxy";
        }
        IAssetRevision assetRevision = currentFile.getAssetRevision();
        if (assetRevision == null)
        {
            return null;
        }

        Set<IAssetRevisionGroup> groups = assetRevision.getAssetRevisionGroups();
        if (CollectionUtils.isEmpty(groups))
        {
            return null;
        }

        IAssetRevisionGroup group = groups.iterator().next();

        IBase workObject = group.getAssociatedEntity();
        workObjectActive = workObject;
        taskTypeActive = assetRevision.getTaskType();
        return getDynamicPathsRecursive(workObject, assetRevision.getTaskType(), type);
    }

    private static List<IDynamicPath> getDynamicPathsRecursive(IBase workObject, ITaskType taskType, String type)
            throws Hd3dException
    {

        List<IDynamicPath> dynamicPaths = getDynamicPaths(workObject.getId(), workObject.entityName(), taskType, type);

        if (CollectionUtils.isEmpty(dynamicPaths))
        {
            dynamicPaths = getDynamicPaths(workObject.getId(), workObject.entityName(), null, type);
            if (CollectionUtils.isEmpty(dynamicPaths))
            {
                String boundEntityName = workObject.entityName();
                workObject = getNextWorkObject(workObject);
                if (workObject != null)
                {
                    dynamicPaths = getDynamicPathsRecursive(workObject, taskType, type);
                }
                else
                {
                    dynamicPaths = getDynamicPaths(-1L, boundEntityName, taskType, type);
                    if (CollectionUtils.isEmpty(dynamicPaths))
                    {
                        dynamicPaths = getDynamicPaths(-1L, boundEntityName, null, type);
                    }
                }
            }

        }
        return dynamicPaths;
    }

    private static IBase getNextWorkObject(IBase workObject)
    {
        IBase parent = null;
        if (IConstituent.class.isInstance(workObject))
        {
            final IConstituent constituent = (IConstituent) workObject;
            parent = constituent.getCategory();
        }
        else if (IShot.class.isInstance(workObject))
        {
            final IShot shot = (IShot) workObject;
            parent = shot.getSequence();
        }
        else if (ISequence.class.isInstance(workObject))
        {
            final ISequence sequence = (ISequence) workObject;
            parent = sequence.getParent();
        }
        else if (ICategory.class.isInstance(workObject))
        {
            final ICategory category = (ICategory) workObject;
            parent = category.getParent();
        }
        return parent;
    }

    private static List<IDynamicPath> getDynamicPaths(Long workObjectid, String workObjectEntityName,
            ITaskType taskType, String type) throws Hd3dException
    {
        String[] properties = { "boundEntityId", "boundEntityName", "taskType", "type" };
        Object[] values = { workObjectid, workObjectEntityName, taskType, type };
        return Persistors.dynamicpath.getByValues(properties, values, "AND");
    }
}
