package fr.hd3d.services.resolver;

import org.apache.commons.lang.math.NumberUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.Persistors;


/**
 * This class acts as a link between AssetRevisionGroup and the bound entity
 * 
 * @author try.lam
 * 
 */
public class AssetGroupToEntityResolver
{
    private IAssetRevisionGroup assetRevisionGroup;

    /**
     * Empty constructor to be used when dynamically instanciated
     */
    public AssetGroupToEntityResolver()
    {
    /* Empty constructor to be used when dynamically instanciated */
    }

    public AssetGroupToEntityResolver(final IAssetRevisionGroup assetRevisionGroup)
    {
        this.assetRevisionGroup = assetRevisionGroup;
    }

    public IAssetRevisionGroup getAssetRevisionGroup()
    {
        return assetRevisionGroup;
    }

    public void setAssetRevisionGroup(final IAssetRevisionGroup assetRevisionGroup)
    {
        this.assetRevisionGroup = assetRevisionGroup;
    }

    public IBase getEntity() throws Hd3dException
    {
        IBase ret = null;

        if (assetRevisionGroup != null)
        {
            /* criteria field must contain the entity name */
            final IPersist<?> persistor = Persistors.getPersistor(assetRevisionGroup.getCriteria());
            /* value field must contain the entity instance id */
            if (persistor != null)
            {
                ret = (IBase) persistor.getById(NumberUtils.toLong(assetRevisionGroup.getValue()));
            }
        }

        return ret;
    }
}
