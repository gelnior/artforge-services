package fr.hd3d.services.resolver;

import java.util.List;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Log;


public class MountPointResolver
{
    /**
     * Return the resolved mount point according to the given operatingSystem. To retrieve the resolved path, it uses
     * 'like' to compare the given operating system and the operating system in the base.
     * 
     * @param operatingSystem
     *            the operating system or a string containing the operating system
     * @return the resolved path
     * @throws Hd3dException
     */
    public static String getResolvedMountPoint(String operatingSystem, String storageName)
    {
        String path = "";

        if (storageName != null)
        {
            List<IMountPoint> mountPoints;
            try
            {
                mountPoints = Persistors.mountpoint.getByValue("name", storageName);

                if (CollectUtils.isNotEmpty(mountPoints))
                {
                    for (IMountPoint mountPoint : mountPoints)
                    {
                        if (operatingSystem.contains(mountPoint.getOperatingSystem()))
                        {
                            path = mountPoint.getMountPointPath();
                            break;
                        }
                    }
                }
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error(e.getMessage());
            }
        }

        return path;
    }
}
