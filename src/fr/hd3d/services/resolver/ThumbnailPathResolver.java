package fr.hd3d.services.resolver;

import java.io.File;

import org.apache.commons.lang.SystemUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.RelatedProjectHandler;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


public final class ThumbnailPathResolver
{
    private ThumbnailPathResolver()
    {}

    private static final String THUMBNAIL_EXTENSION = ".png";

    public static final String THUMBNAILS_PATH = Conf.getImagePath() + "thumbnails";
    public static final String NO_PICTURE_SEGMENT = "no-picture";

    public static final String PROJECT_PATH = "project-";
    public static final String CONSTITUENTS_SEGMENT = "constituents";
    public static final String CONSTITUENTS_PATH = "constituents";
    public static final String SHOTS_SEGMENT = "shots";
    public static final String SHOTS_PATH = "shots";
    public static final String PERSONS_SEGMENT = "persons";
    public static final String PERSONS_PATH = "persons";
    public static final String PROJECTS_SEGMENT = "projects";
    public static final String PROJECTS_PATH = "projects";

    public static final String IMAGE_FIELD_NAME = "image";
    public static final String ID_FIELD_NAME = "id";
    public static final String NO_PICTURE_FILE = "no_picture.png";

    public static String getTypePath(final String type)
    {
        String path = null;

        if (NO_PICTURE_SEGMENT.equals(type))
        {
            path = getNoPicPath();
        }
        else if (CONSTITUENTS_SEGMENT.equals(type))
        {
            path = CONSTITUENTS_PATH;
        }
        else if (SHOTS_SEGMENT.equals(type))
        {
            path = SHOTS_PATH;
        }
        else if (PERSONS_SEGMENT.equals(type))
        {
            path = PERSONS_PATH;
        }
        else if (PROJECTS_SEGMENT.equals(type))
        {
            path = PROJECTS_PATH;
        }

        return path;
    }

    public static File getFile(final String path, final boolean isWorkObject, final String id)
    {
        final StringBuilder buf = new StringBuilder(path);

        /* path = {entityName}/project-{id}/ */
        if (isWorkObject && id != null)
        {
            buf.append(SystemUtils.FILE_SEPARATOR).append(PROJECT_PATH).append(getProjectId(Long.parseLong(id), path));
        }

        return new File(buildThumbnailPath(buf.toString(), buildImageFileName(id, THUMBNAIL_EXTENSION)));
    }

    /* {thumbnailsPath}/{path}/{imageFileName} */
    private static String buildThumbnailPath(final String path, final String imageFileName)
    {
        return new StringBuilder(THUMBNAILS_PATH).append(SystemUtils.FILE_SEPARATOR).append(path).append(
                SystemUtils.FILE_SEPARATOR).append(imageFileName).toString();
    }

    public static String getFolderPath(final String id, final String type)
    {
        String ret = null;

        if (type != null)
        {
            final StringBuilder folder = new StringBuilder(type).append(SystemUtils.FILE_SEPARATOR);

            final Long projectId = getProjectId(Long.parseLong(id), type);
            if (projectId != null)
            {
                folder.append(PROJECT_PATH).append(projectId.toString()).append(SystemUtils.FILE_SEPARATOR);
            }

            ret = THUMBNAILS_PATH + SystemUtils.FILE_SEPARATOR + folder.toString();
        }

        return ret;
    }

    public static String getPath(final String id, final File folderFile, final String extension)
    {
        String ret = null;

        if (folderFile != null)
        {
            ret = new StringBuilder(folderFile.getPath()).append(SystemUtils.FILE_SEPARATOR).append(
                    buildImageFileName(id, extension)).toString();
        }

        return ret;
    }

    public static String buildImageFileName(final String id, final String extension)
    {
        String ret = null;

        if (id != null)
        {
            ret = id + extension;
        }

        return ret;
    }

    public static String getNoPicPath()
    {
        return THUMBNAILS_PATH + SystemUtils.FILE_SEPARATOR + NO_PICTURE_FILE;
    }

    private static Long getProjectId(final long id, final String type)
    {
        Long ret = null;

        try
        {
            Persistent entity = null;
            IProject project = null;
            if (CONSTITUENTS_PATH.equals(type))
            {
                entity = Persistors.constituent.getById(id);
            }
            else if (SHOTS_PATH.equals(type))
            {
                entity = Persistors.shot.getById(id);
            }

            project = RelatedProjectHandler.getProject(entity);
            if (project != null)
            {
                ret = project.getId();
            }
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }

        return ret;
    }
}
