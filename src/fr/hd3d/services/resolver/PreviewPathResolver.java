package fr.hd3d.services.resolver;

import java.io.File;

import org.apache.commons.lang.SystemUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


public final class PreviewPathResolver
{
    private PreviewPathResolver()
    {}

    public static final String THUMBNAILS_PATH = Conf.getImagePath() + "thumbnails";
    public static final String NO_PICTURE_SEGMENT = "no-picture";

    public static final String PROJECT_PATH = "project-";
    public static final String CONSTITUENTS_SEGMENT = "constituents";
    public static final String CONSTITUENTS_PATH = "constituents";
    public static final String SHOTS_SEGMENT = "shots";
    public static final String SHOTS_PATH = "shots";
    public static final String PERSONS_SEGMENT = "persons";
    public static final String PERSONS_PATH = "persons";
    public static final String PROJECTS_SEGMENT = "projects";
    public static final String PROJECTS_PATH = "projects";

    public static final String IMAGE_FIELD_NAME = "image";
    public static final String ID_FIELD_NAME = "id";

    private static final String PREVIEW_EXTENSION = "_preview.png";

    public static final String NO_PICTURE_FILE = "no_picture_preview.png";

    public static String getTypePath(final String type)
    {
        String path = null;

        if (NO_PICTURE_SEGMENT.equals(type))
        {
            path = getNoPicPath();
        }
        else if (CONSTITUENTS_SEGMENT.equals(type))
        {
            path = CONSTITUENTS_PATH;
        }
        else if (SHOTS_SEGMENT.equals(type))
        {
            path = SHOTS_PATH;
        }
        else if (PERSONS_SEGMENT.equals(type))
        {
            path = PERSONS_PATH;
        }
        else if (PROJECTS_SEGMENT.equals(type))
        {
            path = PROJECTS_PATH;
        }

        return path;
    }

    public static File getFile(final String path, final boolean isWorkObject, final String id)
    {
        final String imageFileName = buildImageFileName(id, PREVIEW_EXTENSION);

        final StringBuilder buf = new StringBuilder(path);

        /* path = {entityName}/project-{id}/ */
        if (isWorkObject && id != null && !"null".equals(id))
        {
            buf.append(SystemUtils.FILE_SEPARATOR).append(PROJECT_PATH).append(getProjectId(Long.parseLong(id), path));
        }

        return new File(buildThumbnailPath(buf.toString(), imageFileName));
    }

    /* {thumbnailsPath}/{path}/{imageFileName} */
    private static String buildThumbnailPath(final String path, final String imageFileName)
    {
        return THUMBNAILS_PATH + SystemUtils.FILE_SEPARATOR + path + SystemUtils.FILE_SEPARATOR + imageFileName;
    }

    public static String buildImageFileName(final String id, final String extension)
    {
        return id == null ? null : id + extension;
    }

    public static String getNoPicPath()
    {
        return THUMBNAILS_PATH + SystemUtils.FILE_SEPARATOR + NO_PICTURE_FILE;
    }

    private static Long getProjectId(final long id, final String type)
    {
        Long ret = null;

        try
        {
            if (CONSTITUENTS_PATH.equals(type))
            {
                final IConstituent constituent = Persistors.constituent.getById(id);

                if (constituent != null && constituent.getCategory() != null
                        && constituent.getCategory().getProject() != null)
                {
                    ret = constituent.getCategory().getProject().getId();
                }
            }
            else if (SHOTS_PATH.equals(type))
            {
                final IShot shot = Persistors.shot.getById(id);

                if (shot != null && shot.getSequence() != null && shot.getSequence().getProject() != null)
                {
                    ret = shot.getSequence().getProject().getId();
                }
            }
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }

        return ret;
    }
}
