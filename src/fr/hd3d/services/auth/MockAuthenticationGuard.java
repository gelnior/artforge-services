/**
 * 
 */
package fr.hd3d.services.auth;

import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.ChallengeRequest;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.Status;
import org.restlet.security.Guard;


/**
 * @author Thomas ESKENAZI
 */
@Deprecated
public class MockAuthenticationGuard extends Guard
{
    boolean authorizeResult;

    /**
     * A Mock implementation on guard.
     * 
     * @param context
     * @param authorizeResult
     *            the result to return with the authorize() method
     */
    public MockAuthenticationGuard(Context context, boolean authorizeResult)
    {
        super(context, ChallengeScheme.CUSTOM, null);
        this.authorizeResult = authorizeResult;
        // setSecretResolver(new MockSecretResolver(authorizeResult));
    }

    @Override
    public int authenticate(Request request)
    {
        return authorizeResult ? AUTHENTICATION_VALID : AUTHENTICATION_INVALID;
    }

    /**
     * Challenges the client by adding a challenge request to the response and by setting the status to
     * CLIENT_ERROR_UNAUTHORIZED.
     * 
     * @param response
     *            The response to update.
     * @param stale
     *            Indicates if the new challenge is due to a stale response.
     */
    @Override
    public void challenge(Response response, boolean stale)
    {
        response.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
        response.setChallengeRequest(new ChallengeRequest(this.getScheme(), this.getRealm()));
    }
}
