/**
 * 
 */
package fr.hd3d.services.auth;

import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.ChallengeRequest;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.Cookie;
import org.restlet.data.Status;

import fr.hd3d.exception.Hd3dException;


/**
 * @author Thomas ESKENAZI
 */
// TODO Ovveride doHandle() to manage redirect?
public class FDTAuthenticationGuard extends org.restlet.security.Guard
{
    public FDTAuthenticationGuard(Context context)
    {
        super(context, ChallengeScheme.CUSTOM, null);
    }

    public int authenticate(Request request)
    {
        for (Cookie cookie : request.getCookies())
        {
            try
            {
                if (FDTAuthUtils.validate(cookie))
                {
                    return AUTHENTICATION_VALID;
                }
                else
                {
                    return AUTHENTICATION_INVALID;
                }
            }
            catch (Hd3dException e)
            {
                // TODO log
                e.printStackTrace();
                return AUTHENTICATION_MISSING;
            }
        }
        return AUTHENTICATION_MISSING;
    }

    /*
     * we need to override the challenge method to ensure that the CUSTOM Scheme is allowed. It's not really clean FIXME
     * Find a nicer way to do this.
     */
    public void challenge(Response response, boolean stale)
    {
        if (stale)
        {
            super.challenge(response, true);
        }
        else
        {
            response.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
            response.setChallengeRequest(new ChallengeRequest(getScheme(), getRealm()));
        }
    }
}
