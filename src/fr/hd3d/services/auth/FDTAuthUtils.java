/**
 * 
 */
package fr.hd3d.services.auth;

import java.security.NoSuchAlgorithmException;

import org.restlet.data.Cookie;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.services.security.dao.IUserAccount;
import fr.hd3d.utils.Base64Coder;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;


/**
 * @author Thomas ESKENAZI
 */
public class FDTAuthUtils
{
    private static final String AUTH_COOKIE = Conf.getAuthCookie();

    public static boolean validate(final Cookie cookie) throws Hd3dException
    {
        if (cookie == null)
            return false;

        if (!AUTH_COOKIE.equals(cookie.getName()))
        {
            return false;
        }

        String[] credentials = cookie.getValue().split(":");
        if (credentials.length > 1)
        {
            /* credentials[0] = User; */
            /* credentials[1] = cryptedPassword; */
            return checkEncryptedPassword(credentials[0], credentials[1]);
        }
        else
        {
            return false;
        }
    }

    public static boolean checkEncryptedPassword(final String user, final String encryptedPassword)
            throws Hd3dException
    {
        if (encryptedPassword == null || user == null)
        {
            return false;
        }

        IUserAccount person = Persistors.useraccount.getObjectByValue_NoPermCheck(Const.PRINCIPAL, user);

        return encryptedPassword.equals(person.getSecret());
    }

    // public static boolean checkPassorwd(String user, String password) throws Hd3dPersistenceException
    // {
    // if (password == null || user == null)
    // {
    // return false;
    // }
    // IUserAccount person = Persistors.useraccount.getByValue(Const.PRINCIPAL, user);
    // try
    // {
    // password = encrypt(password);
    // }
    // catch (Hd3dRuntimeException e)
    // {
    // Log.logger.error(ExceptUtils.format(e));
    // return false;
    // }
    // return password.equals(person.getSecret());
    // }

    public static boolean checkPassword(final String user, final String password) throws Hd3dException
    {
        if (password == null || user == null)
            return false;

        return checkEncryptedPassword(user, encrypt(password));
    }

    /**
     * A SHA-256 crypto method
     * 
     * @param password
     *            the password to encrypt
     * @return the base 64 encoded encrypted password
     */
    public static String encrypt(final String password)
    {
        if (password == null)
        {
            throw new Hd3dRuntimeException("password cannot be null");
        }

        try
        {
            java.security.MessageDigest digest = java.security.MessageDigest.getInstance("SHA-256");
            digest.reset();
            digest.update(password.getBytes());

            return String.valueOf(Base64Coder.encode(digest.digest()));
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new Hd3dRuntimeException("unable to encrypt:" + ExceptUtils.format(e));
        }
    }
}
