package fr.hd3d.services;

import static fr.hd3d.common.client.ServicesURI.LOGIN;
import static fr.hd3d.common.client.ServicesURI.LOGOUT;
import static fr.hd3d.common.client.ServicesURI.SERVERTIME;
import static fr.hd3d.common.client.ServicesURI.VERSION;

import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.restlet.Application;
import org.restlet.Context;
import org.restlet.Restlet;
import org.restlet.routing.Router;
import org.restlet.routing.Template;
import org.restlet.security.Guard;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.audit.Purge;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ComplexTypeProvider;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.auth.MockAuthenticationGuard;
import fr.hd3d.services.resources.ServerTimeResource;
import fr.hd3d.services.security.customs.Hd3dAuthenticationGuard;
import fr.hd3d.services.security.resources.LoginResource;
import fr.hd3d.services.security.resources.LogoutResource;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.services.security.utils.AuthorizationUtil;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


/**
 * Entry point of the RESTLET based application. Defines all mapping URL <-> Resource.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 * 
 */
public class Hd3dApplication extends Application
{
    // public static Injector injector;

    public Hd3dApplication(Context context) throws Hd3dException
    {
        super(context);
        Context.getCurrentLogger().setLevel(Level.OFF);
    }

    @Override
    public void start() throws Exception
    {
        Logger.getLogger("hd3d").setAdditivity(false);// avoid to print twice due to rootLogger additivity
        Log.LOGGER.info("STARTING HD3D SERVICES...");
        initialize();// must be done before super.start()
        super.start();
        Log.LOGGER.info("HD3D SERVICES STARTED");
    }

    public void initialize() throws Hd3dException
    {
        // Guice.createInjector(new ServletModule());
        // injector = Guice.createInjector(new ExceptionModule());

        // LicenseManager.checkout();
        Persistors.init();
        Translators.init();
        BaseH.init();
        AuthenticationUtil.init();
        AuthorizationUtil.init();
        Indexer.init();

        /* run audit purge thread */
        if (Conf.isInProductionState())
            Purge.purge();
    }

    @Override
    public void stop() throws Exception
    {
        Log.LOGGER.info("STOPPING HD3DSERVICES...");

        // LicenseManager.checkin();

        if (Conf.isInProductionState())
            Purge.stop();

        HibernateUtil.shutdown();
        BaseH.shutdown();
        Persistors.shutdown();
        ComplexTypeProvider.shutdown();

        super.stop();

        Log.LOGGER.info("HD3DSERVICES STOPPED");

        Logger.getLogger("hd3d").setAdditivity(true);
    }

    /**
     * Callback method for RESTLET based application. No explicit call needed.
     */
    @Override
    public Restlet createInboundRoot()
    {
        Router authRouter = new Router(getContext());
        Router resourceRouter = new Router(getContext());
        authRouter.setDefaultMatchingMode(Template.MODE_STARTS_WITH);
        resourceRouter.setDefaultMatchingMode(Template.MODE_STARTS_WITH);
        resourceRouter.setRoutingMode(Router.MODE_BEST_MATCH);

        // creating a guard for authentication.
        if (Conf.isAuthEnable())
        {
            Hd3dAuthenticationGuard authGuard = new Hd3dAuthenticationGuard(getContext());
            authRouter.attach("/" + VERSION, authGuard);
            authGuard.setNext(resourceRouter);
        }
        else
        {
            Guard authGuard = new MockAuthenticationGuard(getContext(), true);
            authRouter.attach("/" + VERSION, authGuard);
            authGuard.setNext(resourceRouter);
        }

        /**
         * ADD-ON: New Login & Logout resources insertion
         */
        authRouter.attach('/' + LOGIN, LoginResource.class);
        authRouter.attach('/' + LOGOUT, LogoutResource.class);

        authRouter.attach('/' + SERVERTIME, ServerTimeResource.class);

        /**
         * Production routes are set here
         */
        UriBinder binder = new UriBinder(resourceRouter, getContext());
        binder.bind();

        return authRouter;
    }
}
