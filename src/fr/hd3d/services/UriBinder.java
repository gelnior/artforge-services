package fr.hd3d.services;

import static fr.hd3d.common.client.ServicesURI.*;

import org.restlet.Context;
import org.restlet.resource.ServerResource;
import org.restlet.routing.Router;
import org.restlet.security.Authorizer;

import fr.hd3d.services.applications.ApplicationResource;
import fr.hd3d.services.applications.ApplicationsResource;
import fr.hd3d.services.resources.*;
import fr.hd3d.services.security.customs.GetAndPostAuthorizer;
import fr.hd3d.services.security.customs.GetAndPutAuthorizer;
import fr.hd3d.services.security.customs.GetAuthorizer;
import fr.hd3d.services.security.customs.Hd3dAuthorizer;
import fr.hd3d.services.security.resources.ApplyTemplateResource;
import fr.hd3d.services.security.resources.BansResource;
import fr.hd3d.services.security.resources.PermissionsResource;
import fr.hd3d.services.security.resources.Ping;
import fr.hd3d.services.security.resources.RoleTemplateResource;
import fr.hd3d.services.security.resources.RoleTemplatesResource;
import fr.hd3d.services.security.resources.SecurityTemplateResource;
import fr.hd3d.services.security.resources.SecurityTemplatesResource;
import fr.hd3d.services.security.resources.ServicesURLsResource;
import fr.hd3d.services.security.resources.UserAccountsResource;
import fr.hd3d.services.security.resources.WhoisResource;
import fr.hd3d.services.security.utils.AuthorizationUtil;
import fr.hd3d.utils.Conf;


public class UriBinder
{
    Router resourceRouter;
    Context context;

    public UriBinder(Router resourceRouter, Context context)
    {
        this.resourceRouter = resourceRouter;
        this.context = context;
    }

    public void bind()
    {
        appendRessource(resourceRouter, "", GreetingResource.class, "", ALLOW_GET);

        appendRessource(resourceRouter, '/' + SERVERTIME, ServerTimeResource.class, "Misc", ALLOW_GET);

        appendRessource(resourceRouter, '/' + MAXRECORD, MaxRecordResource.class, "Misc", ALLOW_GET);

        appendRessource(resourceRouter, '/' + DISPLAY_ASSET_MANAGER, AssetManagerActivatedResource.class, "Misc",
                ALLOW_GET);
        /**
         * A34 Set https://wiki.mikrosimage.fr:7443/daisy/hd3d-iio/g2/g1/g1/2198.html
         */
        // Access to all days
        appendRessource(resourceRouter, '/' + DAYS_AND_ACTIVITIES, DaysResource.class, "Task");
        // GROUP / PERSONS
        // get all persons in a specific group
        appendRessource(resourceRouter, '/' + RESOURCEGROUP_PERSONS, GroupPersonsResource.class, "Resource");

        appendRessource(resourceRouter, '/' + RESOURCEGROUP_PERSONS + '/' + ALL, ResouceGroupPersonsFullResource.class,
                "Resource");
        // Access to a specific person in a specific group
        appendRessource(resourceRouter, '/' + RESOURCEGROUP_PERSON, GroupPersonResource.class, "Resource");
        // GROUP/ ROLES
        // gets all the roles affected to a specific group
        appendRessource(resourceRouter, '/' + ROLESINGROUP, GroupRolesResource.class, "Permission");
        // accesses to one specific role affected to one specific group
        appendRessource(resourceRouter, '/' + ROLEINGROUP, GroupRoleResource.class, "Permission");
        // TASKTYPES
        // gets all the taskTypes
        appendRessource(resourceRouter, '/' + TASK_TYPES, TaskTypesResource.class, "Task");
        // get a specific taskType
        appendRessource(resourceRouter, '/' + TASKTYPE, TaskTypeResource.class, "Task");
        // PROJECTTYPES
        appendRessource(resourceRouter, '/' + PROJECT_TYPES, ProjectTypesResource.class, "Production");
        // get a specific taskType
        appendRessource(resourceRouter, '/' + PROJECT_TYPE, ProjectTypeResource.class, "Production");
        // ACTIVITIES
        // Access to all the activities
        appendRessource(resourceRouter, '/' + ACTIVITIES, ActivitiesResource.class, "Task");
        // Access to a specific activity
        appendRessource(resourceRouter, '/' + ACTIVITY, ActivityResource.class, "Task");
        appendRessource(resourceRouter, '/' + SIMPLE_ACTIVITIES, SimpleActivitiesResource.class, "Task");
        appendRessource(resourceRouter, '/' + SIMPLE_ACTIVITY, SimpleActivityResource.class, "Task");
        appendRessource(resourceRouter, '/' + TASK_ACTIVITIES, TaskActivitiesResource.class, "Task");
        appendRessource(resourceRouter, '/' + TASK_ACTIVITY, TaskActivityResource.class, "Task");

        // TASKS and DUPLICATE PLANNING
        // access to all tasks
        appendRessource(resourceRouter, '/' + TASKS, TasksResource.class, "Task");
        appendRessource(resourceRouter, '/' + TASK, TaskResource.class, "Task");
        appendRessource(resourceRouter, MY + '/' + TASKS, MyTasksResource.class, "Task");
        appendRessource(resourceRouter, MY + '/' + TASK, MyTaskResource.class, "Task");

        // PERSONS
        // Access to all the persons
        appendRessource(resourceRouter, '/' + PERSONS, PersonsResource.class, "Resource");
        // Access to a specific person
        appendRessource(resourceRouter, '/' + PERSON, PersonResource.class, "Resource");
        // PERSONS / ABSENCES
        // Access to all specific person's tasks
        appendRessource(resourceRouter, '/' + PERSON_ABSENCES, PersonAbsencesResource.class, "Resource");
        // Access to a specific specific person's tasks
        appendRessource(resourceRouter, '/' + PERSON_ABSENCE, PersonAbsenceResource.class, "Resource");
        // PERSONS / TASKS
        // Access to all specific person's tasks
        appendRessource(resourceRouter, '/' + PERSON_TASKS, PersonTasksResource.class, "Task");
        // Access to a specific specific person's tasks
        appendRessource(resourceRouter, '/' + PERSON_TASK, PersonTaskResource.class, "Task");

        // PERSONS / ACTIVITIES
        // Access to all specific person's activities
        appendRessource(resourceRouter, '/' + PERSON_ACTIVITIES, PersonActivitiesResource.class, "Task");

        appendRessource(resourceRouter, '/' + PERSON_ACTIVITIES + "/" + DURATION,
                ActivityDurationPerUserResource.class, "Task");
        appendRessource(resourceRouter, '/' + MY + "/" + DURATION, MyActivityDurationResource.class, "Task");
        // Access to a specific specific person's activity
        appendRessource(resourceRouter, '/' + PERSON_ACTIVITY, PersonActivityResource.class, "Task");

        // Access to all specific person's task activities
        appendRessource(resourceRouter, '/' + PERSON_TASKACTIVITIES, PersonTaskActivitiesResource.class, "Task");
        appendRessource(resourceRouter, '/' + PERSON_TASKACTIVITY, PersonTaskActivityResource.class, "Task");
        appendRessource(resourceRouter, '/' + MY_TASKACTIVITIES, MyTaskActivitiesResource.class, "Task");
        appendRessource(resourceRouter, '/' + MY_TASKACTIVIY, MyTaskActivityResource.class, "Task");
        // Access to all specific person's simple activities
        appendRessource(resourceRouter, '/' + PERSON_SIMPLEACTIVITIES, PersonSimpleActivitiesResource.class, "Task");
        appendRessource(resourceRouter, '/' + PERSON_SIMPLEACTIVITY, PersonSimpleActivityResource.class, "Task");
        appendRessource(resourceRouter, '/' + MY_SIMPLEACTIVITIES, MySimpleActivitiesResource.class, "Task");
        appendRessource(resourceRouter, '/' + MY_SIMPLEACTIVITY, MySimpleActivityResource.class, "Task");

        /* working time */
        appendRessource(resourceRouter, '/' + WORKING_TIMES, WorkingTimesResource.class, "Resource");
        // Access to a specific day
        appendRessource(resourceRouter, '/' + WORKING_TIME, WorkingTimeResource.class, "Resource");

        // PERSON / DAY
        // Access to all days of a specific person
        appendRessource(resourceRouter, '/' + PERSON_DAYS_AND_ACTIVITIES, PersonDaysResource.class, "Task");
        appendRessource(resourceRouter, '/' + PERSON_DAY_AND_ACTIVITIES, PersonDayResource.class, "Task");
        appendRessource(resourceRouter, '/' + MY + "/" + DAYS_AND_ACTIVITIES, MyDaysResource.class, "Task");
        appendRessource(resourceRouter, '/' + MY + "/" + DAYS_AND_ACTIVITIES + "/{" + DAY_ID_ATTRIBUTE + '}',
                MyDayResource.class, "Task");

        // PERSONS / PROJECTS
        // Access to all specific person's projects
        appendRessource(resourceRouter, '/' + PERSON_PROJECTS, PersonProjectsResource.class, "Resource");
        // Access to a specific project
        appendRessource(resourceRouter, '/' + PERSON_PROJECT, PersonProjectResource.class, "Resource");
        // PROJECT
        // Access to all the projects
        // final String projects = PROJECTS + "/{-list|,|" + PROJECT_ID_ATTRIBUTE + '}';
        appendRessource(resourceRouter, '/' + PROJECTS, ProjectsResource.class, "Resource");

        // Task type of a given project.
        appendRessource(resourceRouter, '/' + PROJECT + '/' + TASK_TYPES, ProjectTaskTypesResource.class, "Production");

        // Absence
        appendRessource(resourceRouter, '/' + ABSENCES, AbsencesResource.class, "Resource");
        appendRessource(resourceRouter, '/' + ABSENCE, AbsenceResource.class, "Resource");
        // MileStones
        appendRessource(resourceRouter, '/' + MILESTONES, MileStonesResource.class, "Task");
        appendRessource(resourceRouter, '/' + MILESTONE, MileStoneResource.class, "Task");

        appendRessource(resourceRouter, PROJECT_PLANNING + '/' + MILESTONES, MileStonesResource.class, "Task");
        appendRessource(resourceRouter, PROJECT_PLANNING + '/' + MILESTONE, MileStoneResource.class, "Task");
        // Events
        appendRessource(resourceRouter, '/' + EVENTS, EventsResource.class, "Task");
        appendRessource(resourceRouter, '/' + EVENT, EventResource.class, "Task");

        // Access to a specific project
        appendRessource(resourceRouter, '/' + PROJECT, ProjectResource.class, "Resource");
        // duplicate a master planning for a specific project
        appendRessource(resourceRouter, '/' + DUPLICATE_MASTER_PLANNING, DuplicatePlanningResource.class, "Task");
        // switch master planning
        appendRessource(resourceRouter, '/' + SWITCH_MASTER_PLANNING, ProjectPlanningSwitchMasterResource.class, "Task");

        // Access to first level work object tree of the given planning and the given category.
        appendRessource(resourceRouter, '/' + PLANNING_WORKOBJECTS_RANGE_CATEGORY,
                PlanningWorkObjectsCategoryRangeResource.class, "Resource");
        // Access to first level work object tree of the given planning and the given sequence.
        appendRessource(resourceRouter, '/' + PLANNING_WORKOBJECTS_RANGE_SEQUENCE,
                PlanningWorkObjectsTemporalRangeResource.class, "Resource");
        // Access to the export csv resource for the given data
        appendRessource(resourceRouter, '/' + EXPORT_ODS, ExportCsvResource.class, "Resource");

        // Access to the export csv resource for the given data
        appendRessource(resourceRouter, '/' + EXPORT_STAT_ODS, ExportStatOdsResource.class, "Resource");

        // Access to the export ods resource for the given data
        appendRessource(resourceRouter, '/' + EXPORT_TASK_ODS, ExportTaskOdsResource.class, "Resource");

        // Access to the export planning resource for the given planning
        appendRessource(resourceRouter, '/' + PROJECT_PLANNING_EXPORT_ODS, PlanningExportOdsResource.class, "Resource");

        appendRessource(resourceRouter, '/' + EXPORT_CASTING, ExportCastingOdsResource.class, "Resource");

        // Access to all planning in project
        appendRessource(resourceRouter, '/' + PROJECT_PLANNINGS, ProjectPlanningsResource.class, "Task");
        // Access to a specific plannning
        appendRessource(resourceRouter, '/' + PROJECT_PLANNING, ProjectPlanningResource.class, "Task");
        // Access to all unassigned tasks in one planning
        appendRessource(resourceRouter, '/' + PROJECT_PLANNING_PLANNED_TASKS, PlanningPlannedTasksResource.class,
                "Task");
        // filteres task group tasks by project
        appendRessource(resourceRouter, '/' + PROJECT_FILTERED_TASKGROUP_TASKS, FilteredTaskGroupTasksResource.class,
                "Task");
        // Access to all task group in planning
        appendRessource(resourceRouter, '/' + PLANNING_TASKGROUPS, PlanningTaskGroupsResource.class, "Task");
        // Access to all task changes in planning
        appendRessource(resourceRouter, '/' + PLANNING_TASKCHANGES, PlanningTaskChangesResource.class, "Task");
        // Access to a specific task change in planning
        appendRessource(resourceRouter, '/' + PLANNING_TASKCHANGE, PlanningTaskChangeResource.class, "Task");
        // Access to a specific task group in planning
        appendRessource(resourceRouter, '/' + PLANNING_TASKGROUP, PlanningTaskGroupResource.class, "Task");
        // Acces to all extra lines in one task group
        appendRessource(resourceRouter, '/' + TASKGROUP_EXTRALINES, ExtraLinesResource.class, "Task");
        // Acces to a specific extra line in one task group
        appendRessource(resourceRouter, '/' + TASKGROUP_EXTRALINE, ExtraLineResource.class, "Task");

        // PROJECT / ACTIVITIES
        // Access to all Activities of a specific project
        appendRessource(resourceRouter, '/' + PROJECT_ACTIVITIES, ProjectActivitiesResource.class, "Task");
        // Access to a specific activity of a specific project
        appendRessource(resourceRouter, '/' + PROJECT_ACTIVITY, ProjectActivityResource.class, "Task");

        appendRessource(resourceRouter, '/' + PROJECT + '/' + TASK_ACTIVITIES, ProjectTaskActivitiesResource.class,
                "Task");
        appendRessource(resourceRouter, '/' + PROJECT + '/' + TASK_ACTIVITY, ProjectTaskActivityResource.class, "Task");
        appendRessource(resourceRouter, '/' + PROJECT + '/' + SIMPLE_ACTIVITIES, ProjectSimpleActivitiesResource.class,
                "Task");
        appendRessource(resourceRouter, '/' + PROJECT + '/' + SIMPLE_ACTIVITY, ProjectSimpleActivityResource.class,
                "Task");

        // PROJECT / TASKS
        // Access to all Tasks of a specific project
        appendRessource(resourceRouter, '/' + PROJECT_TASKS, ProjectTasksResource.class, "Task");
        // Access to a specific task of a specific project
        appendRessource(resourceRouter, '/' + PROJECT_TASK, ProjectTaskResource.class, "Task");
        // PROJECT / RESOURCEGROUPS
        // Access to all Resource groups of a specific project
        appendRessource(resourceRouter, '/' + PROJECT_RESOURCEGROUPS, ProjectResourceGroupsResource.class, "Task");
        // PROJECT / PERSONS
        // Access to all the persons of a specific project
        appendRessource(resourceRouter, '/' + PROJECT_PERSONS, ProjectPersonsResource.class, "Resource");
        // Access to a specific person of a specific project
        appendRessource(resourceRouter, '/' + PROJECT_PERSON, ProjectPersonResource.class, "Resource");
        // PROJECT / PERSON / TASKS
        // Access to all Tasks of a specific person of a specific project
        appendRessource(resourceRouter, '/' + PROJECT_PERSON_TASKS, ProjectPersonTasksResource.class, "Task");
        // Access to a specific task of a specific person of a specific project
        appendRessource(resourceRouter, '/' + PROJECT_PERSON_TASK, ProjectPersonTaskResource.class, "Task");
        // PROJECT / PERSON / ACTIVITIES
        // Access to all Activities of a specific person of a specific project
        appendRessource(resourceRouter, '/' + PROJECT_PERSON_ACTIVITIES, ProjectPersonActivitiesResource.class, "Task");
        // Access to a specific task of a specific person of a specific project
        appendRessource(resourceRouter, '/' + PROJECT_PERSON_ACTIVITY, ProjectPersonActivityResource.class, "Task");
        /**
         * A12 set
         */
        // Categories in projects
        appendRessource(resourceRouter, '/' + PROJECT_CATEGORIES, ProjectCategoriesResource.class, "Production");

        appendRessource(resourceRouter, '/' + ALL_PROJECT_CATEGORIES, CategoryAllResource.class, "Production");

        // Category in category
        appendRessource(resourceRouter, '/' + CATEGORY_CHILDREN, CategoriesResource.class, "Production");
        // Category
        appendRessource(resourceRouter, '/' + PROJECT_CATEGORY, CategoryResource.class, "Production");
        // Constituents in categories
        appendRessource(resourceRouter, '/' + CATEGORY_CONSTITUENTS, ConstituentsResource.class, "Production");
        // Constituent
        appendRessource(resourceRouter, '/' + CATEGORY_CONSTITUENT, ConstituentResource.class, "Production");
        // Constituent assets
        appendRessource(resourceRouter, '/' + CONSTITUENT_ASSETS, ConstituentAssetsResource.class, "Asset");
        // Constituents
        appendRessource(resourceRouter, '/' + CONSTITUENTLINKS, ConstituentLinksResource.class, "Production");
        appendRessource(resourceRouter, '/' + CONSTITUENTLINK, ConstituentLinkResource.class, "Production");
        // Constituent shortcut
        appendRessource(resourceRouter, '/' + CONSTITUENT, ConstituentResource.class, "Production");

        // Constituent in project
        appendRessource(resourceRouter, '/' + PROJECT_CONSTITUENTS, ConstituentsResource.class, "Production");
        appendRessource(resourceRouter, '/' + PROJECT_CONSTITUENT, ConstituentResource.class, "Production");

        // Compositions
        appendRessource(resourceRouter, '/' + COMPOSITIONS, CompositionsResource.class, "Production");
        appendRessource(resourceRouter, '/' + COMPOSITION, CompositionResource.class, "Production");
        // Compositions in constituent
        appendRessource(resourceRouter, '/' + CONSTITUENT_SHOTS, ConstituentShotsResource.class, "Production");
        appendRessource(resourceRouter, '/' + CONSTITUENT_COMPOSITIONS, ConstituentCompositionsResource.class,
                "Production");
        appendRessource(resourceRouter, '/' + CONSTITUENT_SHOTS_TASKS, ConstituentCompositionTasksResource.class,
                "Production");

        appendRessource(resourceRouter, '/' + CONSTITUENT_TASKS, ConstituentTasksResource.class, "Production");
        appendRessource(resourceRouter, '/' + CATEGORY_CONSTITUENT_TASKS, ConstituentTasksResource.class, "Production");
        appendRessource(resourceRouter, '/' + CONSTITUENT_NOTES, ConstituentNotesResource.class, "Production");
        appendRessource(resourceRouter, '/' + CATEGORY_CONSTITUENT_NOTES, ConstituentNotesResource.class, "Production");
        // Composition in constituent
        appendRessource(resourceRouter, '/' + CONSTITUENT_COMPOSITION, CompositionResource.class, "Production");

        // Sequences in projects
        appendRessource(resourceRouter, '/' + PROJECT_SEQUENCES, ProjectSequencesResource.class, "Production");
        appendRessource(resourceRouter, '/' + PROJECT_SEQUENCES_ALL, SequenceAllResource.class, "Production");

        appendRessource(resourceRouter, '/' + PROJECT_SHOTS, ProjectShotsResource.class, "Production");

        appendRessource(resourceRouter, PROJECT + '/' + COMPOSITIONS, CompositionsResource.class, "Production");
        appendRessource(resourceRouter, PROJECT + '/' + COMPOSITION, CompositionResource.class, "Production");

        // Sequence in Sequence
        appendRessource(resourceRouter, '/' + PROJECT_SEQUENCE_CHILDREN, SequencesResource.class, "Production");
        // all the Sequences of a project
        appendRessource(resourceRouter, '/' + ALL_SEQUENCES, ProjectAllSequencesResource.class, "Production");

        // All sequence in Sequence
        appendRessource(resourceRouter, '/' + SEQUENCE_ALL_CHILDREN, SequenceAllSequencesResource.class, "Production");
        // Sequence
        appendRessource(resourceRouter, '/' + PROJECT_SEQUENCE, SequenceResource.class, "Production");

        // Compositions in sequence
        appendRessource(resourceRouter, '/' + SEQUENCE_COMPOSITIONS, SequenceCompositionsResource.class, "Production");

        // Casting in Sequence
        appendRessource(resourceRouter, '/' + SEQUENCE_CASTING, CastingSequenceResource.class, "Production");

        // Shots in sequences
        appendRessource(resourceRouter, '/' + SEQUENCE_SHOTS, SequenceShotsResource.class, "Production");
        // shot
        appendRessource(resourceRouter, '/' + SEQUENCE_SHOT, SequenceShotResource.class, "Production");

        // Shot assets
        appendRessource(resourceRouter, '/' + SHOT_ASSETS, ShotAssetsResource.class, "Asset");

        // Casting in shot
        appendRessource(resourceRouter, '/' + SHOT_CASTING, CastingResource.class, "Production");

        // Shot
        appendRessource(resourceRouter, '/' + SHOTS, ShotsResource.class, "Production");
        // shot
        appendRessource(resourceRouter, '/' + SHOT, ShotResource.class, "Production");
        appendRessource(resourceRouter, '/' + PROJECT_SHOT, ShotResource.class, "Production");
        // Compositions in shot
        appendRessource(resourceRouter, '/' + SEQUENCE_SHOT_COMPOSITIONS, ShotCompositionsResource.class, "Production");
        // Constituents linked to shot via composition
        appendRessource(resourceRouter, '/' + PROJECT_SHOT_CONSTITUENTS, ShotConstituentsResource.class, "Production");
        appendRessource(resourceRouter, '/' + PROJECT_SHOT_CONSTITUENTS_TASKS, ShotCompositionTasksResource.class,
                "Production");
        // Tasks linked to shot.
        appendRessource(resourceRouter, '/' + SHOT_TASKS, ShotTasksResource.class, "Production");
        appendRessource(resourceRouter, '/' + SEQUENCE_SHOT_TASKS, ShotTasksResource.class, "Production");
        appendRessource(resourceRouter, '/' + SHOT_NOTES, ShotNotesResource.class, "Production");
        appendRessource(resourceRouter, '/' + SEQUENCE_SHOT_NOTES, ShotNotesResource.class, "Production");

        // sheets in a project
        appendRessource(resourceRouter, '/' + PROJECT_SHEETS, SheetsResource.class, "Misc");

        // sheets not in a projects
        appendRessource(resourceRouter, '/' + SHEETS, SheetsResource.class, "Misc");

        // sheet
        appendRessource(resourceRouter, '/' + SHEET, SheetResource.class, "Misc");
        appendRessource(resourceRouter, '/' + PROJECT_SHEET, SheetResource.class, "Misc");
        appendRessource(resourceRouter, '/' + PROJECT_SHEET_DATA, SheetResource.class, "Misc");
        appendRessource(resourceRouter, '/' + PROJECT_SHEET_DATA_CSV, SheetCsvResource.class, "Misc");

        appendRessource(resourceRouter, '/' + SHEET_DATA, SheetResource.class, "Misc");

        appendRessource(resourceRouter, '/' + SHEET_DATA_CSV, SheetCsvResource.class, "Misc");

        // items
        appendRessource(resourceRouter, '/' + SHEET_ITEMS, SheetItemsResource.class, "Misc");
        // itemGroups
        appendRessource(resourceRouter, '/' + SHEET_ITEMGROUPS, ItemGroupsResource.class, "Misc");
        // itemGroup
        appendRessource(resourceRouter, '/' + SHEET_ITEMGROUP, ItemGroupResource.class, "Misc");
        // items
        appendRessource(resourceRouter, '/' + SHEET_ITEMGROUP_ITEMS, ItemsResource.class, "Misc");
        // item
        appendRessource(resourceRouter, '/' + SHEET_ITEMGROUP_ITEM, ItemResource.class, "Misc");

        // itemGroups
        appendRessource(resourceRouter, '/' + PROJECT_SHEET_ITEMGROUPS, ItemGroupsResource.class, "Misc");
        // itemGroup
        appendRessource(resourceRouter, '/' + PROJECT_SHEET_ITEMGROUP, ItemGroupResource.class, "Misc");
        // items
        appendRessource(resourceRouter, '/' + PROJECT_SHEET_ITEMGROUP_ITEMS, ItemsResource.class, "Misc");
        // item
        appendRessource(resourceRouter, '/' + PROJECT_SHEET_ITEMGROUP_ITEM, ItemResource.class, "Misc");

        // upload
        appendRessource(resourceRouter, '/' + DOWNLOAD, DownloadResource.class, "Asset");
        appendRessource(resourceRouter, '/' + DELETE, DeleteFileResource.class, "Asset");
        appendRessource(resourceRouter, '/' + UPLOAD, UploadResource.class, "Asset");
        appendRessource(resourceRouter, '/' + MY_UPLOAD, UploadResource.class, "Asset");
        appendRessource(resourceRouter, '/' + NEXT_FILEREVISION_UPLOAD, NextFileRevisionUploadResource.class, "Asset");
        appendRessource(resourceRouter, '/' + NEXT_FILEREVISION, NextFileRevisionResource.class, "Asset");

        appendRessource(resourceRouter, '/' + REUSEUPLOAD, ReuseUploadResource.class, "Asset");
        appendRessource(resourceRouter, '/' + TESTUPLOAD, TestUploadResource.class, "Asset");

        // Resource
        appendRessource(resourceRouter, '/' + RESOURCEGROUP_ROOTS, ResourceGroupRootsResource.class, "Resource");
        appendRessource(resourceRouter, '/' + RESOURCEGROUPS, ResourceGroupsResource.class, "Resource");
        appendRessource(resourceRouter, '/' + RESOURCEGROUP, ResourceGroupResource.class, "Resource");
        appendRessource(resourceRouter, '/' + RESOURCEGROUP_CHILDREN, ResourceGroupChildrenResource.class, "Resource");

        appendRessource(resourceRouter, '/' + SKILLS, SkillsResource.class, "Resource");
        appendRessource(resourceRouter, '/' + SKILL, SkillResource.class, "Resource");
        appendRessource(resourceRouter, '/' + SKILLLEVELS, SkillLevelsResource.class, "Resource");
        appendRessource(resourceRouter, '/' + SKILLLEVEL, SkillLevelResource.class, "Resource");
        appendRessource(resourceRouter, '/' + QUALIFICATIONS, QualificationsResource.class, "Resource");
        appendRessource(resourceRouter, '/' + QUALIFICATION, QualificationResource.class, "Resource");
        appendRessource(resourceRouter, '/' + ORGANIZATIONS, OrganizationsResource.class, "Resource");
        appendRessource(resourceRouter, '/' + ORGANIZATION, OrganizationResource.class, "Resource");
        appendRessource(resourceRouter, '/' + CONTRACTS, ContractsResource.class, "Resource");

        appendRessource(resourceRouter, '/' + CONTRACT, ContractResource.class, "Resource");

        // Computer inventory application
        // Computer
        appendRessource(resourceRouter, '/' + COMPUTERS, ComputersResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + COMPUTER, ComputerResource.class, "Inventory");
        // Device
        appendRessource(resourceRouter, '/' + DEVICES, DevicesResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + DEVICE, DeviceResource.class, "Inventory");
        // Screen
        appendRessource(resourceRouter, '/' + SCREENS, ScreensResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + SCREEN, ScreenResource.class, "Inventory");
        // Software
        appendRessource(resourceRouter, '/' + SOFTWARES, SoftwaresResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + SOFTWARE, SoftwareResource.class, "Inventory");
        // Software
        appendRessource(resourceRouter, '/' + LICENSES, LicensesResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + LICENSE, LicenseResource.class, "Inventory");
        // Studio map
        appendRessource(resourceRouter, '/' + STUDIOMAPS, StudioMapsResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + STUDIOMAP, StudioMapResource.class, "Inventory");
        // Pools
        appendRessource(resourceRouter, '/' + ROOMS, RoomsResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + ROOM, RoomResource.class, "Inventory");
        // Pools
        appendRessource(resourceRouter, '/' + POOLS, PoolsResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + POOL, PoolResource.class, "Inventory");
        // Manufacturers
        appendRessource(resourceRouter, '/' + MANUFACTURERS, ManufacturersResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + MANUFACTURER, ManufacturerResource.class, "Inventory");
        // Processors
        appendRessource(resourceRouter, '/' + PROCESSORS, ProcessorsResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + PROCESSOR, ProcessorResource.class, "Inventory");
        // Screen Models
        appendRessource(resourceRouter, '/' + SCREEN_MODELS, ScreenModelsResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + SCREEN_MODEL, ScreenModelResource.class, "Inventory");
        // Device Models
        appendRessource(resourceRouter, '/' + DEVICE_MODELS, DeviceModelsResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + DEVICE_MODEL, DeviceModelResource.class, "Inventory");
        // Screen Models
        appendRessource(resourceRouter, '/' + COMPUTER_MODELS, ComputerModelsResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + COMPUTER_MODEL, ComputerModelResource.class, "Inventory");
        // Device Types
        appendRessource(resourceRouter, '/' + DEVICE_TYPES, DeviceTypesResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + DEVICE_TYPE, DeviceTypeResource.class, "Inventory");
        // Screen Types
        appendRessource(resourceRouter, '/' + COMPUTER_TYPES, ComputerTypesResource.class, "Inventory");
        appendRessource(resourceRouter, '/' + COMPUTER_TYPE, ComputerTypeResource.class, "Inventory");

        // tag
        appendRessource(resourceRouter, '/' + TAGS, TagsResource.class, "Misc");
        appendRessource(resourceRouter, '/' + TAG, TagResource.class, "Misc");
        // tag categories
        appendRessource(resourceRouter, '/' + TAG_CATEGORIES, TagCategoriesResource.class, "Misc");
        appendRessource(resourceRouter, '/' + TAG_CATEGORIES_ALL_CHILDREN, TagCategoriesResource.class, "Misc");
        appendRessource(resourceRouter, '/' + TAG_CATEGORY, TagCategoryResource.class, "Misc");
        appendRessource(resourceRouter, '/' + TAG_CATEGORY_CHILDREN, TagCategoryChildrenResource.class, "Misc");
        appendRessource(resourceRouter, '/' + TAG_CATEGORY_TAGS, TagCategoryTagsResource.class, "Misc");

        // classDynMetaDataTypes
        appendRessource(resourceRouter, '/' + CLASSDYNMETADATATYPES, ClassDynMetaDataTypesResource.class, "Misc");
        appendRessource(resourceRouter, '/' + CLASSDYNMETADATATYPE, ClassDynMetaDataTypeResource.class, "Misc");

        appendRessource(resourceRouter, '/' + CLASSDYNMETADATATYPE_CHECK,
                ClassDynMetaDataTypeValueCheckerResource.class, "Misc");

        // dynMetaDataTypes
        appendRessource(resourceRouter, '/' + DYNMETADATATYPES, DynMetaDataTypesResource.class, "Misc");
        appendRessource(resourceRouter, '/' + DYNMETADATATYPE, DynMetaDataTypeResource.class, "Misc");

        // dynMetaDataTypes
        appendRessource(resourceRouter, '/' + LISTS_VALUES, ListsValuesResource.class, "Misc");
        appendRessource(resourceRouter, '/' + LIST_VALUES, ListValuesResource.class, "Misc");

        // dynMetaDataValues
        appendRessource(resourceRouter, '/' + DYNMETADATAVALUES, DynMetaDataValuesResource.class, "Misc");
        appendRessource(resourceRouter, '/' + DYNMETADATAVALUE, DynMetaDataValueResource.class, "Misc");

        // search Constituents
        // final String search_constituents = SEARCH_CONSTITUENTS + "?{-join|&|projs,tags,shots,cats,seqs}";
        // appendRessource(resourceRouter, search_constituents, ConstituentsSearchResource.class, "Asset");

        // search Files
        appendRessource(resourceRouter, '/' + SEARCH_FILES, FilesSearchResource.class, "Asset");

        // scripts
        appendRessource(resourceRouter, '/' + SCRIPTS, ScriptsResource.class, "Technical");
        appendRessource(resourceRouter, '/' + SCRIPT, ScriptResource.class, "Technical");

        // audits
        appendRessource(resourceRouter, '/' + AUDITS, AuditsResource.class, "Technical");

        // settings
        appendRessource(resourceRouter, '/' + SETTINGS, SettingsResource.class, "Misc");
        appendRessource(resourceRouter, '/' + SELF_SETTING, SettingsResource.class, "Misc");
        appendRessource(resourceRouter, '/' + SETTING, SettingResource.class, "Misc");

        // sheet filter list
        appendRessource(resourceRouter, '/' + SHEET_FILTERS, SheetFiltersResource.class, "Misc");
        appendRessource(resourceRouter, '/' + SHEET_FILTER, SheetFilterResource.class, "Misc");
        appendRessource(resourceRouter, '/' + SHEET_FILTERS_BY_SHEET, SheetSheetFiltersResource.class, "Misc");

        // updatesstack
        appendRessource(resourceRouter, '/' + UPDATESSTACK, UpdatesStacksResource.class, "Misc");

        // asset revisions links
        appendRessource(resourceRouter, '/' + ASSETREVISIONLINKS, AssetRevisionLinksResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISIONLINK, AssetRevisionLinkResource.class, "Asset");

        // asset revisions at root level
        appendRessource(resourceRouter, '/' + ASSETREVISIONS, AssetRevisionsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + MY_ASSETREVISIONS, MyAssetRevisionsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISION, AssetRevisionResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISION_FILEREVISIONS,
                AssetRevisionLastFileRevisionsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISION_FILEREVISION, FileRevisionResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISION_LAST_FILEREVISION,
                AssetRevisionLastFileRevisionsResource.class, "Asset");

        appendRessource(resourceRouter, '/' + ASSETREVISION_UPSTREAM, AssetRevisionUpstreamResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISION_DOWNSTREAM, AssetRevisionDownstreamResource.class, "Asset");

        // asset revision groups
        appendRessource(resourceRouter, '/' + ASSETREVISIONGROUPS, AssetRevisionGroupsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISIONGROUP, AssetRevisionGroupResource.class, "Asset");

        // asset groups at project level
        appendRessource(resourceRouter, '/' + PROJECT_ASSETREVISIONGROUPS, ProjectAssetRevisionGroupsResource.class,
                "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETREVISIONGROUP, AssetRevisionGroupResource.class, "Asset");

        // asset abstracts at root level
        appendRessource(resourceRouter, '/' + ASSETABSTRACTS, AssetAbstractsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETABSTRACT, AssetAbstractResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETABSTRACT_REVISIONS, AssetAbstractRevisionsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETABSTRACT_REVISION, AssetAbstractRevisionResource.class, "Asset");
        // appendRessource(resourceRouter, '/' + ASSETABSTRACT_REVISION_LAST_FILEREVISIONS,
        // AssetRevisionLastFileRevisionsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETABSTRACT_REVISION_FILEREVISION, FileRevisionResource.class, "Asset");

        appendRessource(resourceRouter, '/' + ASSETABSTRACT_REVISION_UPSTREAM, AssetRevisionUpstreamResource.class,
                "Asset");
        appendRessource(resourceRouter, '/' + ASSETABSTRACT_REVISION_DOWNSTREAM, AssetRevisionDownstreamResource.class,
                "Asset");

        // asset abstracts in projects
        appendRessource(resourceRouter, '/' + PROJECT_ASSETABSTRACTS, AssetAbstractsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETABSTRACT, AssetAbstractResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETABSTRACT_ASSETREVISIONS,
                AssetAbstractRevisionsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETABSTRACT_ASSETREVISION, AssetAbstractRevisionResource.class,
                "Asset");
        // appendRessource(resourceRouter, '/' + PROJECT_ASSETABSTRACT_ASSETREVISION_FILEREVISIONS,
        // AssetAbstractRevisionFileRevisionsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETABSTRACT_ASSETREVISION_FILEREVISION,
                FileRevisionResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETABSTRACT_ASSETREVISION_UPSTREAM,
                AssetRevisionUpstreamResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETABSTRACT_ASSETREVISION_DOWNSTREAM,
                AssetRevisionDownstreamResource.class, "Asset");

        // asset abstracts in assetgroups
        appendRessource(resourceRouter, '/' + ASSETREVISIONGROUP_ASSETABSTRACTS, AssetAbstractsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISIONGROUP_ASSETABSTRACT, AssetAbstractResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISIONS,
                AssetAbstractRevisionsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION,
                AssetAbstractRevisionResource.class, "Asset");
        // appendRessource(resourceRouter, '/' + ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_FILEREVISIONS,
        // AssetAbstractRevisionFileRevisionsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_FILEREVISION,
                FileRevisionResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_UPSTREAM,
                AssetRevisionUpstreamResource.class, "Asset");
        appendRessource(resourceRouter, '/' + ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_DOWNSTREAM,
                AssetRevisionDownstreamResource.class, "Asset");

        // asset abstracts in asset groups in projects
        appendRessource(resourceRouter, '/' + PROJECT_ASSETREVISIONGROUP_ASSETABSTRACTS, AssetAbstractsResource.class,
                "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT, AssetAbstractResource.class,
                "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISIONS,
                AssetAbstractRevisionsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION,
                AssetAbstractRevisionResource.class, "Asset");
        // appendRessource(resourceRouter, '/' + PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_FILEREVISIONS,
        // AssetAbstractRevisionFileRevisionsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_FILEREVISION,
                FileRevisionResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_UPSTREAM,
                AssetRevisionUpstreamResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_ASSETREVISIONGROUP_ASSETABSTRACT_ASSETREVISION_DOWNSTREAM,
                AssetRevisionDownstreamResource.class, "Asset");

        // play list revisions
        appendRessource(resourceRouter, '/' + PLAYLISTREVISIONS, PlayListRevisionsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PLAYLISTREVISION, PlayListRevisionResource.class, "Asset");

        appendRessource(resourceRouter, '/' + PLAYLISTREVISIONGROUPS, PlayListRevisionGroupsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PLAYLISTREVISIONGROUP, PlayListRevisionGroupResource.class, "Asset");

        appendRessource(resourceRouter, '/' + PROJECT_PLAYLISTREVISIONGROUPS,
                ProjectPlayListRevisionGroupsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_PLAYLISTREVISIONGROUP,
                ProjectPlayListRevisionGroupsResource.class, "Asset");

        appendRessource(resourceRouter, '/' + PROJECT_PLAYLISTREVISIONGROUP_CHILDREN,
                PlayListRevisionGroupChildrenResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROJECT_PLAYLISTREVISIONGROUP_PLAYLISTREVISIONS,
                ProjectPlayListRevisionGroupPlayListRevisionsResource.class, "Asset");

        // play list edits
        appendRessource(resourceRouter, '/' + PLAYLISTEDITS, PlayListEditsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PLAYLISTEDIT, PlayListEditResource.class, "Asset");

        // file revision annotation
        appendRessource(resourceRouter, '/' + FILEREVISION_ANNOTATIONS, FileRevisionAnnotationsResource.class, "Asset");
        appendRessource(resourceRouter, '/' + FILEREVISION_ANNOTATION, FileRevisionAnnotationResource.class, "Asset");

        // graphical annotation
        appendRessource(resourceRouter, '/' + GRAPHICAL_ANNOTATIONS, GraphicalAnnotationsResource.class, "Production");
        appendRessource(resourceRouter, '/' + GRAPHICAL_ANNOTATION, GraphicalAnnotationResource.class, "Production");

        // file revisions
        appendRessource(resourceRouter, '/' + FILEREVISIONS, FilesRevisionResource.class, "Asset");
        appendRessource(resourceRouter, '/' + MY_FILEREVISIONS, MyFilesRevisionResource.class, "Asset");
        appendRessource(resourceRouter, '/' + FILEREVISION, FileRevisionResource.class, "Asset");
        appendRessource(resourceRouter, '/' + FILEREVISION_DOWNLOAD, FileRevisionDownloadResource.class, "Asset");
        appendRessource(resourceRouter, '/' + FILEREVISION_DOWNSTREAM, FileRevisionDownstreamResource.class, "Asset");
        appendRessource(resourceRouter, '/' + FILEREVISION_UPSTREAM, FileRevisionUpstreamResource.class, "Asset");

        // // asset revision to file revision links
        // appendRessource(resourceRouter, '/' + ASSETREVISIONFILEREVISIONS, AssetRevisionFileRevisionsResource.class,
        // "Asset");
        // appendRessource(resourceRouter, '/' + ASSETREVISIONFILEREVISION, AssetRevisionFileRevisionResource.class,
        // "Asset");

        // file revision links
        appendRessource(resourceRouter, '/' + FILEREVISIONLINKS, FileRevisionLinksResource.class, "Asset");
        appendRessource(resourceRouter, '/' + FILEREVISIONLINK, FileRevisionLinkResource.class, "Asset");

        /* Delete Filerevisionlink by input output */
        appendRessource(resourceRouter, '/' + DELETE_FILEREVISION_BY_INPUT_OUTPUT,
                FileRevisionLinkDeleteByInputOutputResource.class, "Asset");

        // fileattributes
        appendRessource(resourceRouter, '/' + FILE_ATTRIBUTES, FileAttributesResource.class, "Asset");
        appendRessource(resourceRouter, '/' + FILE_ATTRIBUTE, FileAttributeResource.class, "Asset");

        // filesystem
        appendRessource(resourceRouter, '/' + FILESYSTEM, FileSystemResource.class, "Asset");
        appendRessource(resourceRouter, '/' + FILESYSTEM_BY_ID, FileSystemResource.class, "Asset");

        // dynamic path
        appendRessource(resourceRouter, '/' + DYNAMIC_PATH, DynamicPathResource.class, "Asset"); // dynamic path
        appendRessource(resourceRouter, '/' + DYNAMIC_PATHS, DynamicPathsResource.class, "Asset");

        // proxy type
        appendRessource(resourceRouter, '/' + PROXYTYPE, ProxyTypeResource.class, "Asset"); // proxy_type
        appendRessource(resourceRouter, '/' + PROXYTYPES, ProxyTypesResource.class, "Asset");

        // proxy
        appendRessource(resourceRouter, '/' + PROXY, ProxyResource.class, "Asset"); // proxy
        appendRessource(resourceRouter, '/' + PROXIES, ProxiesResource.class, "Asset");
        appendRessource(resourceRouter, '/' + FILE_REVISION_PROXY_UPLOAD, ProxiesUploadResource.class, "Asset");
        appendRessource(resourceRouter, '/' + PROXY_DOWNLOAD, ProxiesDownloadResource.class, "Asset");
        appendRessource(resourceRouter, '/' + LAST_PROXIES_WORKOBJECT_TASKTYPE, LastProxiesResource.class, "Asset");

        // acl
        appendRessource(resourceRouter, '/' + ACLS, AclsResource.class, "Permission");
        appendRessource(resourceRouter, '/' + ACL, AclResource.class, "Permission");

        // approvalnotes
        appendRessource(resourceRouter, '/' + APPROVALNOTES, ApprovalNotesResource.class, "Production");
        appendRessource(resourceRouter, '/' + APPROVALNOTE, ApprovalNoteResource.class, "Production");
        appendRessource(resourceRouter, '/' + MY + '/' + APPROVALNOTE, MyNoteResource.class, "Production");
        appendRessource(resourceRouter, '/' + MY + "/" + APPROVALNOTES, MyNotesResource.class, "Production");
        appendRessource(resourceRouter, '/' + PROJECT + "/" + APPROVALNOTES, ProjectApprovalNotesResource.class,
                "Production");
        appendRessource(resourceRouter, '/' + PROJECT + "/" + APPROVALNOTE, ProjectApprovalNoteResource.class,
                "Production");
        appendRessource(resourceRouter, '/' + PROJECT + "/" + APPROVALNOTE + '/' + TASKS,
                ProjectApprovalNoteTasksResource.class, "Production");

        // approvalnotetypes
        appendRessource(resourceRouter, '/' + APPROVALNOTETYPES, ApprovalNoteTypesResource.class, "Production");
        appendRessource(resourceRouter, '/' + APPROVALNOTETYPE, ApprovalNoteTypeResource.class, "Production");
        appendRessource(resourceRouter, '/' + PROJECT_APPROVALNOTETYPES, ApprovalNoteTypesResource.class, "Production");
        appendRessource(resourceRouter, '/' + PROJECT_APPROVALNOTETYPE, ApprovalNoteTypeResource.class, "Production");

        // entitytasklinks
        appendRessource(resourceRouter, '/' + ENTITYTASKLINKS, EntityTaskLinksResource.class, "Production");
        appendRessource(resourceRouter, '/' + ENTITYTASKLINK, EntityTaskLinkResource.class, "Production");
        appendRessource(resourceRouter, '/' + PROJECT + "/" + ENTITYTASKLINKS, ProjectEntityTaskLinksResource.class,
                "Production");
        appendRessource(resourceRouter, '/' + PROJECT + "/" + ENTITYTASKLINK, EntityTaskLinkResource.class,
                "Production");

        // steps
        appendRessource(resourceRouter, '/' + STEPS, StepsResource.class, "Production");
        appendRessource(resourceRouter, '/' + STEP, StepResource.class, "Production");
        appendRessource(resourceRouter, '/' + PROJECT_STEPS, ProjectStepsResource.class, "Production");

        //
        appendRessource(resourceRouter, '/' + TASK_STATUS_NB_BY_TASKTYPES, TaskStatusNbByTaskTypesResource.class,
                "Production", ALLOW_GET);

        appendRessource(resourceRouter, '/' + APPROVALNOTES_BY_SEQUENCES_IDS_BY_DATE,
                ApprovalNoteBySequenceIdsByDateResource.class, "Production", ALLOW_GET);

        appendRessource(resourceRouter, '/' + APPROVALNOTES_BY_CATEGORIES_IDS_BY_DATE,
                ApprovalNoteByCategoryIdsByDateResource.class, "Production", ALLOW_GET);
        // rule management part (security)
        // ROLE
        // All Services URLs
        appendRessource(resourceRouter, '/' + ALL_SERVICE_URLS, ServicesURLsResource.class, "Permission", ALLOW_GET);
        // Access to all roles
        appendRessource(resourceRouter, '/' + ROLES, RolesResource.class, "Permission");
        // Access to a specific role
        appendRessource(resourceRouter, '/' + ROLE, RoleResource.class, "Permission");
        // For getting the current user
        appendRessource(resourceRouter, '/' + WHOIS, WhoisResource.class, "Permission", ALLOW_GET);
        // Access to user permissions
        appendRessource(resourceRouter, '/' + WHOIS_PERMISSIONS, PermissionsResource.class, "Permission", ALLOW_GET);
        // Access to user bans
        appendRessource(resourceRouter, '/' + WHOIS_BANS, BansResource.class, "Permission", ALLOW_GET);
        // For changing one's password
        appendRessource(resourceRouter, '/' + USER_ACCOUNTS, UserAccountsResource.class, "Permission");
        // Security Templates
        // Project security templates
        appendRessource(resourceRouter, '/' + SECURITY_TEMPLATES_TEMPLATES, SecurityTemplatesResource.class,
                "Permission");
        appendRessource(resourceRouter, '/' + SECURITY_TEMPLATES_TEMPLATE, SecurityTemplateResource.class, "Permission");
        // Role templates
        appendRessource(resourceRouter, '/' + ROLE_TEMPLATES, RoleTemplatesResource.class, "Permission");
        appendRessource(resourceRouter, '/' + ROLE_TEMPLATE, RoleTemplateResource.class, "Permission");
        // Apply templates
        appendRessource(resourceRouter, '/' + APPLY_SECURITY_TEMPLATE, ApplyTemplateResource.class, "Permission");
        // ping resource
        appendRessource(resourceRouter, '/' + PING, Ping.class, "Permission", ALLOW_GET);

        // HD3D -- APPLICATIONS
        appendRessource(resourceRouter, '/' + APPLICATIONS, ApplicationsResource.class, "Permission", ALLOW_GET);
        appendRessource(resourceRouter, '/' + APPLICATION, ApplicationResource.class, "Permission");

        // ices
        appendRessource(resourceRouter, '/' + ICES, ICESResource.class, "Technical");
        appendRessource(resourceRouter, '/' + ICESDOWNLOAD, ICESDownloadResource.class, "Technical");
        appendRessource(resourceRouter, '/' + TESTICES, TestICESResource.class, "Technical");

        // entities description
        appendRessource(resourceRouter, '/' + ENTITIES, EntitiesResource.class, "Production");

        // enums
        appendRessource(resourceRouter, '/' + ENUMS, EnumsResource.class, "Production");

        // entity attributes
        appendRessource(resourceRouter, '/' + ENTITY, EntityResource.class, "Production");

        // collection queries description
        appendRessource(resourceRouter, '/' + COLLECTIONQUERIES, CollectionQueriesResource.class, "Production");

        // item transformers description
        appendRessource(resourceRouter, '/' + ITEMSTRANSFORMERS, ItemsTransformersResource.class, "Production");

        // free query
        appendRessource(resourceRouter, '/' + FREE_QUERY, FreeQueryResource.class, "Misc");

        // images
        appendRessource(resourceRouter, '/' + IMAGES, ImageResource.class, "Misc", ALLOW_GET);
        appendRessource(resourceRouter, '/' + IMAGES_THUMBNAILS, ThumbnailResource.class, "Misc", ALLOW_GETnPOST);
        appendRessource(resourceRouter, '/' + IMAGES_PREVIEWS, PreviewResource.class, "Misc", ALLOW_GET);

        // version & build
        appendRessource(resourceRouter, '/' + VERSION_BUILD, VersionResource.class, "Technical", ALLOW_GET);

        // mount point
        appendRessource(resourceRouter, '/' + MOUNTPOINT, MountPointResource.class, "Misc"); // mount point
        appendRessource(resourceRouter, '/' + MOUNTPOINTS, MountPointsResource.class, "Misc");

        appendRessource(resourceRouter, '/' + CREATE_FILE_TREE, CreateFileTreeResource.class, "Misc");

        /* annotation server */
        appendRessource(resourceRouter, '/' + "annotations-search", AnnotationsSearchResource.class, "Misc");
        appendRessource(resourceRouter, '/' + "assets-search", AssetsSearchResource.class, "Misc");

        appendRessource(resourceRouter, '/' + UPDATE_DB, UpdateDbResource.class, "Technical");

        appendRessource(resourceRouter, '/' + ANNOTATION_INDEXATION, AnnotationIndexationResource.class, "Misc");

        // Database initialization for quick debugging
        if (!Conf.isInProductionState())
        {
            appendRessource(resourceRouter, '/' + ADD_DB_REUSE_BANK, InitializeBankResource.class, "Technical");
            appendRessource(resourceRouter, '/' + INIT_DB, InitializeDbResource.class, "Technical");
            appendRessource(resourceRouter, '/' + INIT_DB_INVENTORY, InitializeInventoryResource.class, "Technical");
            appendRessource(resourceRouter, '/' + INIT_DB_WITH_PASTA, InitializePastaResource.class, "Technical");
            appendRessource(resourceRouter, '/' + INIT_DB_WITH_BUNCH_CONSTITUENTS,
                    InitializeBunchConstituentResource.class, "Technical");
            appendRessource(resourceRouter, '/' + INIT_DB_BUNCH_FILES, InitializeDbBunchFileAssetsResource.class,
                    "Technical");
            appendRessource(resourceRouter, '/' + ERASE_DB, EraseDbResource.class, "Technical");
            appendRessource(resourceRouter, '/' + CLEANUP, CleanupResource.class, "Technical");
            appendRessource(resourceRouter, '/' + LUCENE_TEST, LuceneTestResource.class, "Technical");

        }
    }

    private void appendRessource(final Router router, String url, final Class<? extends ServerResource> resourceClass,
            String section, int authorizationMode)
    {
        if (url.startsWith("/"))
            url = url.substring(1);

        // sets up the proper authorizer
        Authorizer authz = getAuthorizer(authorizationMode);
        authz.setContext(context);
        authz.setNext(resourceClass);

        router.attach(url, authz);

        GreetingResource.appendResource(url, resourceClass, section);

        AuthorizationUtil.registerURL(url, isDataObject(resourceClass));
    }

    private boolean isDataObject(Class<? extends ServerResource> resourceClass)
    {
        return AbstractHd3dCollection.class.isAssignableFrom(resourceClass)
                || AbstractHd3dResource.class.isAssignableFrom(resourceClass);
    }

    private void appendRessource(final Router router, String url, final Class<? extends ServerResource> resourceClass,
            String section)
    {
        if (AbstractHd3dCollectionResource.class.isAssignableFrom(resourceClass))
        {
            appendRessource(router, url, resourceClass, section, ALLOW_GETnPUT);
        }
        else if (AbstractHd3dUploadBaseResource.class.isAssignableFrom(resourceClass))
        {
            appendRessource(router, url, resourceClass, section, ALLOW_GETnPOST);
        }
        else
        {
            appendRessource(router, url, resourceClass, section, RESTRICT);
        }
    }

    /**
     * Sets up an authorizer of the requested type : RESTRICT filters all requests, but ALLOW_GET and ALLOW_GETnPUT will
     * let the allowed methods pass without any preemptive filtering.
     * 
     * @param authorizationMode
     * @return an authorizer of the requested type
     */
    private static Authorizer getAuthorizer(int authorizationMode)
    {
        Authorizer authz;
        switch (authorizationMode)
        {
            case RESTRICT:
                authz = new Hd3dAuthorizer();
                break;
            case ALLOW_GET:
                authz = new GetAuthorizer();
                break;
            case ALLOW_GETnPUT:
                authz = new GetAndPutAuthorizer();
                break;
            case ALLOW_GETnPOST:
                authz = new GetAndPostAuthorizer();
                break;
            default:
                authz = new Hd3dAuthorizer();
        }
        return authz;
    }
}
