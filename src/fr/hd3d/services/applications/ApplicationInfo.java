package fr.hd3d.services.applications;

import static fr.hd3d.common.client.ServicesURI.APPLICATIONS;
import static fr.hd3d.common.client.ServicesURI.VERSION;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.lightweight.ILDynMetaDataValue;
import fr.hd3d.model.lightweight.ILEntityTaskLink;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.AuthorizationUtil;


/**
 * A base lightweight wrapping for application info.
 * 
 * @author HD3D
 * 
 */
public class ApplicationInfo implements ILBase
{

    private static long id_count = -100000;

    private String name;
    private Long id;

    public ApplicationInfo(String name)
    {
        this.name = name;
        this.id = Long.valueOf(id_count--);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @SuppressWarnings("serial")
    public boolean userCanAccess()
    {
        return AuthorizationUtil.canOneAtLeast(new HashSet<String>() {
            {
                add(getPermissionPath());
            }
        });
    }

    public String getPermissionPath()
    {
        String uri = VERSION.replace("/", "") + '/' + APPLICATIONS + "/" + name + "/";
        return uri.replace("/", ":") + Customs.READ;
    }

    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ApplicationInfo other = (ApplicationInfo) obj;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

    public void addTag(Long tagId)
    {}

    public void clear()
    {}

    public Set<ILApprovalNote> getApprovalNotes()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public Set<ILTask> getBoundTasks()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public String getDefaultPath()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public Collection<ILDynMetaDataValue> getDynMetaDataValues()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public Set<ILEntityTaskLink> getEntityTaskLinks()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public Long getId()
    {
        return id;
    }

    public List<String> getTagNames()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<Long> getTags()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean getUserCanDelete()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean getUserCanUpdate()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public Timestamp getVersion()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void setApprovalNotes(Set<ILApprovalNote> approvalNotes)
    {}

    public void setBoundTasks(Set<ILTask> boundTasks)
    {}

    public void setDefaultPath(String path)
    {}

    public void setDynMetaDataValues(Collection<ILDynMetaDataValue> dynMetaDataValues)
    {}

    public void setEntityTaskLinks(Set<ILEntityTaskLink> entityTaskLinks)
    {}

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setTagNames(List<String> tagNames)
    {}

    public void setTags(List<Long> tags)
    {}

    public void setUserCanDelete(boolean canDelete)
    {}

    public void setUserCanUpdate(boolean canUpdate)
    {}

    public void setVersion(Timestamp version)
    {}

    public String getInternalUUID()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void setInternalUUID(String internalUUID)
    {
    // TODO Auto-generated method stub

    }

}
