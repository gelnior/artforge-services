package fr.hd3d.services.applications;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.restlet.data.CharacterSet;
import org.restlet.data.Encoding;
import org.restlet.engine.application.EncodeRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.services.resources.AbstractHd3dBaseResource;
import fr.hd3d.services.resources.serializer.RessourceSerializer;


/**
 * Resource that contains a list of all HD3D application modules. It will only return the list of that which the user
 * can access to.
 * 
 * @author HD3D
 * 
 * @param <L>
 * @param <P>
 */
public class ApplicationsResource<L, P> extends AbstractHd3dBaseResource
{

    public final static String A12_BREAKDOWN = "Beakdown";
    public final static String A12_DATAFLOW = "Assets";
    public final static String A34_HUMAN_RESOURCES = "Teams";
    public final static String A34_TASK_MANAGER = "Tasks";
    public final static String A34_TIME_SHEET = "Timesheet";
    public final static String A34_PLANNING = "Planning";
    public final static String A34_SUIVI_DE_PRODUCTION = "Production";
    public final static String B_ICARE = "Reuse";
    public final static String D_DISPATCHER = "Jobs";
    public final static String E_EXCHANGE = "Exchange";
    public final static String F_ADMIN = "Admin";
    public final static String F_LOGS = "Logs";
    public final static String F_COMPUTERS = "Machines";
    /* to be continued... */

    @SuppressWarnings("serial")
    private static Set<ApplicationInfo> apps = Collections.unmodifiableSet(new HashSet<ApplicationInfo>() {
        {
            add(new ApplicationInfo(A12_BREAKDOWN));
            add(new ApplicationInfo(A12_DATAFLOW));
            add(new ApplicationInfo(A34_HUMAN_RESOURCES));
            add(new ApplicationInfo(A34_PLANNING));
            add(new ApplicationInfo(A34_SUIVI_DE_PRODUCTION));
            add(new ApplicationInfo(A34_TASK_MANAGER));
            add(new ApplicationInfo(A34_TIME_SHEET));
            add(new ApplicationInfo(B_ICARE));
            add(new ApplicationInfo(D_DISPATCHER));
            add(new ApplicationInfo(E_EXCHANGE));
            add(new ApplicationInfo(F_ADMIN));
            add(new ApplicationInfo(F_LOGS));
            add(new ApplicationInfo(F_COMPUTERS));
        }
    });

    public ApplicationsResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
    }

    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public Representation get(Variant v)
    {
        try
        {
            Representation r = RessourceSerializer.getRepresentation(v, getAccessibleModules());
            r.setCharacterSet(CharacterSet.UTF_8);
            if (!fr.hd3d.utils.Conf.compressResponse())
            {
                return r;
            }

            return new EncodeRepresentation(Encoding.GZIP, r);
        }
        finally
        {
            cleanup();
        }
    }

    private static List<ApplicationInfo> getAccessibleModules()
    {
        List<ApplicationInfo> list = new ArrayList<ApplicationInfo>();
        for (ApplicationInfo info : apps)
        {
            if (info.userCanAccess())
            {
                list.add(info);
            }
        }
        return list;
    }

}
