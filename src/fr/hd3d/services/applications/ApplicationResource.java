package fr.hd3d.services.applications;

import static fr.hd3d.common.client.ServicesURI.APPLICATION_ID;

import org.restlet.data.CharacterSet;
import org.restlet.data.Encoding;
import org.restlet.engine.application.EncodeRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.services.resources.AbstractHd3dBaseResource;
import fr.hd3d.services.resources.serializer.RessourceSerializer;


/**
 * A resource that represents a single application module, referenced to by its name
 * 
 * @author HD3D
 * 
 * @param <L>
 * @param <P>
 */
public class ApplicationResource<L, P> extends AbstractHd3dBaseResource
{

    private ApplicationInfo info;

    public ApplicationResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        String name = (String) getRequest().getAttributes().get(APPLICATION_ID);
        if (name == null)
        {
            name = "none";
        }
        info = new ApplicationInfo(name);
    }

    public Representation get(Variant v)
    {
        try
        {
            Representation r = RessourceSerializer.getRepresentation(v, info);
            r.setCharacterSet(CharacterSet.UTF_8);
            if (!fr.hd3d.utils.Conf.compressResponse())
            {
                return r;
            }
            return new EncodeRepresentation(Encoding.GZIP, r);
        }
        finally
        {
            cleanup();
        }
    }

    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
