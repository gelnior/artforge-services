package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.FILEREVISIONS;
import static fr.hd3d.common.client.ServicesURI.FILEREVISION_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.UPLOAD;

import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.hibernate.LockMode;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.common.client.enums.EFileStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.utils.FileRevisionUtils;
import fr.hd3d.utils.HibernateUtil;


@Hd3dComment("Upload d'un fichier existant, correspondant à la Révision actuelle incrémentée de 1. <br>\n"
        + "POST/Multipart: [file]= fichier à uploader;<br>\n" + "[fileRevisionId] = id f<br>\n" + "<br>\r\nPUT:N/A\r\n"
        + "<br>\r\nDELETE:N/A")
public class NextFileRevisionUploadResource extends AbstractHd3dUploadBaseResource<ILFileRevision, IFileRevision>
{

    private final static String FILE = "file";
    private final static String NEW_FILE_REVISION = "newFileRevision";

    /**
     * @param context
     * @param request
     * @param response
     * @return
     * @throws Hd3dException
     */
    public NextFileRevisionUploadResource() throws Hd3dException
    {
        super();
        allowMethods("POST");
    }

    @Override
    protected void parseTextFields(Map<String, Object> resultMap, FileItem fileItem)
    {
    /* Nothing to do: the file revision id is provided in the url and not as a parameter */
    }

    @Override
    protected void parseUploadedFiles(Map<String, Object> resultMap, FileItem fileItem)
    {
        if (resultMap == null || fileItem == null || fileItem.isFormField())
            return;

        final String fileName = fileItem.getFieldName();

        /* parsing uploaded files */
        if (FILE.equals(fileName))
        {
            resultMap.put(FILE, fileItem);
        }
    }

    //
    public Representation get(Variant variant)
    {
        // test
        // Long fileRevisionId = getIdFromUrl(Hd3dApplication.FILEREVISION_ID_ATTRIBUTE);
        // try
        // {
        // final IFileRevision fileRevision = Persistors.filerevision.getById(fileRevisionId, LockMode.UPGRADE);
        //
        // String query = "(select max(revision) from FileRevisionH where key='" + fileRevision.getKey()
        // + "' and variation='" + fileRevision.getVariation() + "')";
        // String query_1 = "from FileRevisionH where key='" + fileRevision.getKey() + "' and variation='"
        // + fileRevision.getVariation() + "' and revision=" + query;
        // // Integer maxfileRevision = (Integer) HibernateUtil.currentSession().createQuery(query).uniqueResult();
        // IFileRevision f = (IFileRevision) HibernateUtil.currentSession().createQuery(query_1).uniqueResult();
        //
        // System.out.println(f);
        // }
        // catch (Hd3dPersistenceException e)
        // {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }

        //
        return new StringRepresentation("A file must be uploaded", MediaType.TEXT_PLAIN);
    }

    @Override
    protected void process(Map<String, Object> resultMap) throws Hd3dException
    {
        Long fileRevisionId = getResourceContext().getIdFromUrl(FILEREVISION_ID_ATTRIBUTE);
        if (BaseH.isNotValidId(fileRevisionId))
            return;

        FileItem file = (FileItem) resultMap.get(FILE);

        /* Retrieve the FileRevision matching the id, with the MAX revision number */
        ResourceContext context = new ResourceContext();

        final IFileRevision fileRevision = Persistors.filerevision
                .getById(context, fileRevisionId, LockMode.OPTIMISTIC);
        if (fileRevision == null)
            return;

        String maxRevisionQuery = "(select max(revision) from FileRevisionH where key='" + fileRevision.getKey()
                + "' and variation='" + fileRevision.getVariation() + "' and assetRevision="
                + fileRevision.getAssetRevision().getId() + ")";
        String query = "from FileRevisionH where key='" + fileRevision.getKey() + "' and variation='"
                + fileRevision.getVariation() + "' and assetRevision=" + fileRevision.getAssetRevision().getId()
                + " and revision=" + maxRevisionQuery;
        IFileRevision maxRevisionFileRevision = (IFileRevision) HibernateUtil.currentSession().createQuery(query)
                .uniqueResult();

        if (maxRevisionFileRevision.getStatus() == EFileStatus.LOCKED)
            throw new Hd3dPersistenceException("File with key=" + fileRevision.getKey() + " and variation="
                    + fileRevision.getVariation() + " is locked!");

        IAssetRevision asset = maxRevisionFileRevision.getAssetRevision();
        String destFolder = "";
        if (asset != null)
        {
            destFolder = asset.getWipPath() + "/";
        }
        /* Persist the uploaded file with Revision incremented */
        IFileRevision newFileRevision = FileRevisionUtils.persistUploadedFile(asset.getMountPointWipPath(), destFolder,
                file, fileRevision.getKey(), fileRevision.getVariation(), maxRevisionFileRevision.getRevision() + 1,
                EFileState.getDefault(), asset);

        resultMap.put(NEW_FILE_REVISION, newFileRevision);
    }

    @Override
    protected Representation buildRepresentation(Map<String, Object> resultMap)
    {
        IFileRevision fileRevision = (IFileRevision) resultMap.get(NEW_FILE_REVISION);

        Representation rep = null;
        if (fileRevision == null)
        {
            rep = new StringRepresentation("0", MediaType.TEXT_PLAIN);
        }
        else
        {
            /* file successfully uploaded */
            rep = new StringRepresentation(String.valueOf(fileRevision.getId()));
            rep.setLocationRef(this.getNewResourceIdentifier(fileRevision));
        }

        return rep;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        StringBuilder buf = new StringBuilder(getRequest().getResourceRef().getBaseRef().toString());
        buf.delete(buf.lastIndexOf('/' + UPLOAD), buf.length());
        buf.append('/' + FILEREVISIONS).append('/').append(((IBase) object).getId());
        return buf.toString();
    }
}
