package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonTaskCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.UniqueResultQuery;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


/**
 * A resource to access to a specific person's activity.
 * 
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'un tache\r\n" + "\r\nPUT:Modification d'une tache\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:N/A")
public class PersonTaskResource extends AbstractHd3dResource<ILTask, ITask>
{
    public PersonTaskResource() throws Hd3dException
    {
        super(Persistors.task, Translators.task);
    }

    @Override
    protected ITask initializeObject(LockMode lockMode) throws Hd3dException
    {
        final UniqueResultQuery<ILTask, ITask> cmd = new GetPersonTaskCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        ITask obj = (ITask) cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }
}
