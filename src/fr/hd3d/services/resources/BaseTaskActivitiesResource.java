package fr.hd3d.services.resources;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.Const;


public class BaseTaskActivitiesResource extends AbstractHd3dCollection<ILTaskActivity, ITaskActivity>
{
    public BaseTaskActivitiesResource() throws Hd3dException
    {
        super(Persistors.taskActivity, Translators.taskActivity);
    }

    // TODO: Optimize for bulk requests
    /**
     * Checks that no activity has been set for current day.
     */
    @Override
    protected void doPrePersist(Representation entity, ITaskActivity newObject) throws Hd3dException
    {
        String[] fields = { "task.id", "filledBy.id", "day.id" };
        Object[] values = { newObject.getTask().getId(), newObject.getFilledBy().getId(), newObject.getDay().getId() };

        List<ITaskActivity> activities = Persistors.taskActivity.getByValues(fields, values, "AND");

        if (CollectionUtils.isEmpty(activities))
        {
            super.doPrePersist(entity, newObject);
        }
        else
        {
            throw new Hd3dException(
                    "An activity already exists for this task, this user and this day. Please modify existing one rather than creating a new one.");
        }
    }

    /**
     * Sets the actual start date of tasks with the date of oldest activity linked to the task of deleted activity.
     * 
     * @throws Hd3dException
     */
    protected void updateTaskActualStartDate(ITaskActivity object) throws Hd3dException
    {
        ITask task = object.getTask();
        List<ITaskActivity> activities = Persistors.taskActivity.getByValue(Const.TASK + Const._ID, object.getTask()
                .getId());

        if (CollectionUtils.isNotEmpty(activities))
        {
            IActivity firstActivity = activities.get(0);
            IActivity lastActivity = activities.get(0);

            for (IActivity activity : activities)
            {
                if (firstActivity.getDay() != null && activity.getDay() != null
                        && activity.getDay().getDate().before(firstActivity.getDay().getDate()))
                    firstActivity = activity;

                if (lastActivity.getDay() != null && activity.getDay() != null
                        && activity.getDay().getDate().after(lastActivity.getDay().getDate()))
                    lastActivity = activity;
            }

            task.setActualStartDate(firstActivity.getDay().getDate());
            task.setActualEndDate(lastActivity.getDay().getDate());
        }
        else
        {
            task.setActualStartDate(null);
            task.setActualEndDate(null);
        }
        task.doPreUpdate();
        Persistors.task.persist(task);
        task.doPostUpdate();
    }
}
