package fr.hd3d.services.resources;

import java.util.List;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILAssetAbstract;
import fr.hd3d.model.persistence.IAssetAbstract;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.AssetAbstractH;
import fr.hd3d.model.persistence.impl.hibernate.query.GetAssetAbstractRevisionsCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.ListResultQuery;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


public class AssetAbstractResource extends AbstractHd3dResource<ILAssetAbstract, IAssetAbstract>
{
    public AssetAbstractResource() throws Hd3dException
    {
        super(Persistors.assetabstract, Translators.assetabstract);
    }

    @Override
    protected IAssetAbstract initializeObject(LockMode lockMode) throws Hd3dException
    {
        final ListResultQuery cmd = new GetAssetAbstractRevisionsCmd((ResourceContext) getResourceContext())
                .wantTotalSize();
        HibernateUtil.safeQuery(cmd);
        List<IAssetAbstract> assetAbstracts = AssetAbstractH.parseIAssets(cmd.result);

        if (assetAbstracts.isEmpty())
            return null;

        if (assetAbstracts.size() > 1)
            throw new Hd3dPersistenceException("Found more than one asset with same identifiers.");
        else
            return assetAbstracts.get(0);
    }
}
