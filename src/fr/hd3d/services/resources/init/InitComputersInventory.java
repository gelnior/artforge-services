package fr.hd3d.services.resources.init;

import java.security.NoSuchAlgorithmException;
import java.util.Date;

import org.hibernate.Session;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.Entity;
import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.client.Sheet;
import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.services.resources.DBInitializer;


public class InitComputersInventory implements IHibernateTransaction
{
    public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
    {
        this.initComputers(session);
        this.initSoftwares(session);
        this.initLicenses(session);
        this.initDevices(session);
        this.initScreens(session);
        this.initPools(session);
    }

    private void initLicenses(Session session) throws Hd3dException
    {
        DBInitializer.createLicense(session, "License 01", "serial", "billing", new Date(), new Date(), "OK",
                10, "RENDER");
        DBInitializer.createLicense(session, "License 02", "serial", "billing", new Date(), new Date(), "OK",
                10, "RENDER");
        DBInitializer.createLicense(session, "License OS 01", "serial", "billing", new Date(), new Date(), "OK",
                100, "CLASSIC");
        DBInitializer.createLicense(session, "License Compo 01", "serial", "billing", new Date(), new Date(),
                "OK", 20, "FLOAT");

        ISheet deviceSheet = DBInitializer.createSheet(session, null, "Licences",
                "Permet d'effectuer un suivi de production pour les licenses", Entity.LICENSE, ESheetType.MACHINE);
        IItemGroup defaultGroup = DBInitializer.createItemGroup(session, deviceSheet.getId(), "Default");

        DBInitializer.createItem(session, "Name", Sheet.ITEMTYPE_ATTRIBUTE, "name", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, Renderer.SHORT_TEXT, Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Software", Sheet.ITEMTYPE_ATTRIBUTE, "software", "software", defaultGroup
                .getId(), Entity.SOFTWARE, Renderer.RECORD, Editor.SOFTWARE);

        DBInitializer.createItem(session, "Type", Sheet.ITEMTYPE_ATTRIBUTE, "type", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, Renderer.LICENSE_TYPE, Editor.LICENSE_TYPE);

        DBInitializer.createItem(session, "Number", Sheet.ITEMTYPE_ATTRIBUTE, "number", "int", defaultGroup.getId(),
                Const.JAVA_LANG_INTEGER, Renderer.INT, Editor.INT);

        DBInitializer.createItem(session, "Serial", Sheet.ITEMTYPE_ATTRIBUTE, "serial", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Billing Reference", Sheet.ITEMTYPE_ATTRIBUTE, "billingReference", "",
                defaultGroup.getId(), Const.JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Purchase date", Sheet.ITEMTYPE_ATTRIBUTE, "purchaseDate", "", defaultGroup
                .getId(), Const.JAVA_UTIL_DATE, Renderer.SHORT_DATE, Editor.DATE);

        DBInitializer.createItem(session, "Warranty end", Sheet.ITEMTYPE_ATTRIBUTE, "warrantyEnd", "", defaultGroup
                .getId(), Const.JAVA_UTIL_DATE, Renderer.SHORT_DATE, Editor.DATE);

        DBInitializer.createItem(session, "Inventory Status", Sheet.ITEMTYPE_ATTRIBUTE, "inventoryStatus", "",
                defaultGroup.getId(), Const.JAVA_LANG_STRING, Renderer.INVENTORY_STATUS, Editor.INVENTORY_STATUS);
    }

    private void initDevices(Session session) throws Hd3dException
    {
        for (int i = 1; i < 10; i++)
        {
            DBInitializer.createDevice(session, "ATI Radeon X1900 0" + i, "Serial_0" + i, "billing_0" + i, new Date(),
                    new Date(), "OK", (float) 512.0, "GRAPHIC_CARD");
        }
        for (int i = 1; i < 10; i++)
        {
            DBInitializer.createDevice(session, "Seagate Barracuda 7200 0" + i, "Serial_0" + i, "billing_0" + i,
                    new Date(), new Date(), "OK", (float) 1.5, "HARD_DRIVE");
        }
        for (int i = 1; i < 3; i++)
        {
            DBInitializer.createDevice(session, "Canon PIXMA iX5000 0" + i, "Serial_0" + i, "billing_0" + i,
                    new Date(), new Date(), "OK", (float) 29.8, "PRINTER");
        }

        DBInitializer.createDeviceType(session, "Hard Drive");
        DBInitializer.createDeviceType(session, "Graphic Card");
        DBInitializer.createDeviceType(session, "Printer");
        DBInitializer.createDeviceType(session, "RAM");
        DBInitializer.createDeviceType(session, "Other");

        DBInitializer.createDeviceModel(session, "ATI Radeon X1900");
        DBInitializer.createDeviceModel(session, "Seagate Barracuda 7200");
        DBInitializer.createDeviceModel(session, "Canon PIXMA iX5000");
        DBInitializer.createDeviceModel(session, "Other");

        ISheet deviceSheet = DBInitializer.createSheet(session, null, "Périphériques",
                "Permet d'effectuer un suivi de production pour les périphériques", Entity.DEVICE, ESheetType.MACHINE);
        // create group
        IItemGroup defaultGroup = DBInitializer.createItemGroup(session, deviceSheet.getId(), "Default");

        // create Items
        DBInitializer.createItem(session, "Name", Sheet.ITEMTYPE_ATTRIBUTE, "name", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, Renderer.SHORT_TEXT, Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Serial", Sheet.ITEMTYPE_ATTRIBUTE, "serial", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Billing Reference", Sheet.ITEMTYPE_ATTRIBUTE, "billingReference", "",
                defaultGroup.getId(), Const.JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Purchase date", Sheet.ITEMTYPE_ATTRIBUTE, "purchaseDate", "", defaultGroup
                .getId(), Const.JAVA_UTIL_DATE, Renderer.SHORT_DATE, Editor.DATE);

        DBInitializer.createItem(session, "Warranty end", Sheet.ITEMTYPE_ATTRIBUTE, "warrantyEnd", "", defaultGroup
                .getId(), Const.JAVA_UTIL_DATE, Renderer.SHORT_DATE, Editor.DATE);

        DBInitializer.createItem(session, "Inventory Status", Sheet.ITEMTYPE_ATTRIBUTE, "inventoryStatus", "",
                defaultGroup.getId(), Const.JAVA_LANG_STRING, Renderer.INVENTORY_STATUS, Editor.INVENTORY_STATUS);

        DBInitializer.createItem(session, "Size", Sheet.ITEMTYPE_ATTRIBUTE, "size", "", defaultGroup.getId(),
                Const.JAVA_LANG_DOUBLE, "", Editor.FLOAT);

        DBInitializer.createItem(session, "Type", Sheet.ITEMTYPE_ATTRIBUTE, "deviceType", "", defaultGroup.getId(),
                Entity.DEVICETYPE, Renderer.RECORD, Editor.DEVICE_TYPE);

        DBInitializer.createItem(session, "Model", Sheet.ITEMTYPE_ATTRIBUTE, "deviceModel", "", defaultGroup.getId(),
                Entity.DEVICEMODEL, Renderer.RECORD, Editor.DEVICE_MODEL);

    }

    private void initScreens(Session session) throws Hd3dException
    {
        for (int i = 1; i < 10; i++)
        {
            DBInitializer.createScreen(session, "Mitsubishi HM204DT 00" + i, "Serial_00" + i, "billing_00" + i,
                    new Date(), new Date(), "OK", (float) 22, "CRT", "COLOR");
        }

        for (int i = 1; i < 10; i++)
        {
            DBInitializer.createScreen(session, "FlexScan SX3031W 00" + i, "Serial_00" + i, "billing_00" + i,
                    new Date(), new Date(), "OK", (float) 29.8, "LCD", "COLOR");
        }
        for (int i = 10; i < 100; i++)
        {
            DBInitializer.createScreen(session, "FlexScan SX3031W 0" + i, "Serial_0" + i, "billing_0" + i, new Date(),
                    new Date(), "OK", (float) 29.8, "LCD", "GRAPHIC");
        }

        for (int i = 1; i < 10; i++)
        {
            DBInitializer.createScreen(session, "NEC multisync 2690 WUXi 00" + i, "Serial_00" + i, "billing_00" + i,
                    new Date(), new Date(), "OK", (float) 26, "LCD", "OUT");
        }
        for (int i = 10; i < 100; i++)
        {
            DBInitializer.createScreen(session, "NEC multisync 2690 WUXi 0" + i, "Serial_0" + i, "billing_0" + i,
                    new Date(), new Date(), "OK", (float) 26, "LCD", "GRAPHIC");
        }
        for (int i = 100; i < 200; i++)
        {
            DBInitializer.createScreen(session, "NEC multisync 2690 WUXi " + i, "Serial_" + i, "billing_" + i,
                    new Date(), new Date(), "OK", (float) 26, "LCD", "GRAPHIC");
        }

        DBInitializer.createScreenModel(session, "Mitsubishi HM204DT");
        DBInitializer.createScreenModel(session, "FlexScan SX3031W 7200");
        DBInitializer.createScreenModel(session, "NEC multisync 2690 WUXi");
        DBInitializer.createScreenModel(session, "Other");

        ISheet screenSheet = DBInitializer.createSheet(session, null, "Ecrans",
                "Permet d'effectuer un suivi de production pour les écrans", Entity.SCREEN, ESheetType.MACHINE);
        // create group
        IItemGroup defaultGroup = DBInitializer.createItemGroup(session, screenSheet.getId(), "Default");

        // create Items
        DBInitializer.createItem(session, "Name", Sheet.ITEMTYPE_ATTRIBUTE, "name", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, Renderer.SHORT_TEXT, Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Serial", Sheet.ITEMTYPE_ATTRIBUTE, "serial", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Billing Reference", Sheet.ITEMTYPE_ATTRIBUTE, "billingReference", "",
                defaultGroup.getId(), Const.JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Purchase date", Sheet.ITEMTYPE_ATTRIBUTE, "purchaseDate", "", defaultGroup
                .getId(), Const.JAVA_UTIL_DATE, Renderer.SHORT_DATE, Editor.DATE);

        DBInitializer.createItem(session, "Warranty end", Sheet.ITEMTYPE_ATTRIBUTE, "warrantyEnd", "", defaultGroup
                .getId(), Const.JAVA_UTIL_DATE, Renderer.SHORT_DATE, Editor.DATE);

        DBInitializer.createItem(session, "Inventory Status", Sheet.ITEMTYPE_ATTRIBUTE, "inventoryStatus", "",
                defaultGroup.getId(), Const.JAVA_LANG_STRING, Renderer.INVENTORY_STATUS, Editor.INVENTORY_STATUS);

        DBInitializer.createItem(session, "Size", Sheet.ITEMTYPE_ATTRIBUTE, "size", "", defaultGroup.getId(),
                Const.JAVA_LANG_DOUBLE, "", Editor.FLOAT);

        DBInitializer.createItem(session, "Type", Sheet.ITEMTYPE_ATTRIBUTE, "type", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, Renderer.SCREEN_TYPE, Editor.SCREEN_TYPE);

        DBInitializer.createItem(session, "Qualification", Sheet.ITEMTYPE_ATTRIBUTE, "qualification", "", defaultGroup
                .getId(), Const.JAVA_LANG_STRING, Renderer.SCREEN_QUALIFICATION, Editor.SCREEN_QUALIFICATION);

        DBInitializer.createItem(session, "Screen Model", Sheet.ITEMTYPE_ATTRIBUTE, "screenModel", "", defaultGroup
                .getId(), Entity.SCREENMODEL, Renderer.RECORD, Editor.SCREEN_MODEL);
    }

    private void initPools(Session session) throws Hd3dException, NoSuchAlgorithmException
    {
        DBInitializer.createPool(session, "Pool 2D", true);
        DBInitializer.createPool(session, "Pool 3D", true);
        DBInitializer.createPool(session, "Pool Bureautique", false);

        ISheet poolSheet = DBInitializer.createSheet(session, null, "Pools",
                "Permet d'effectuer un suivi de production pour les pools", Entity.POOL, ESheetType.MACHINE);
        // create group
        IItemGroup defaultGroup = DBInitializer.createItemGroup(session, poolSheet.getId(), "Default");

        // create Items
        DBInitializer.createItem(session, "Name", Sheet.ITEMTYPE_ATTRIBUTE, "name", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, Renderer.SHORT_TEXT, Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "IsDispatcherAllowed", Sheet.ITEMTYPE_ATTRIBUTE, "isDispatcherAllowed", "",
                defaultGroup.getId(), Const.JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Editor.BOOLEAN);

    }

    private void initSoftwares(Session session) throws Hd3dException, NoSuchAlgorithmException
    {
        DBInitializer.createSoftware(session, "Linux Ubuntu 9.04");
        DBInitializer.createSoftware(session, "Windows XP");
        DBInitializer.createSoftware(session, "Windows Vista");
        DBInitializer.createSoftware(session, "Maya 2009");
        DBInitializer.createSoftware(session, "Maya 2010");
        DBInitializer.createSoftware(session, "Flame");
        DBInitializer.createSoftware(session, "Combustion");
        DBInitializer.createSoftware(session, "Nuke");
        DBInitializer.createSoftware(session, "Photoshop");
        DBInitializer.createSoftware(session, "After Effects");

        ISheet softwareSheet = DBInitializer.createSheet(session, null, "Logiciels", "Permet de lister les logiciels",
                Entity.SOFTWARE, ESheetType.MACHINE);
        // create group
        IItemGroup defaultGroup = DBInitializer.createItemGroup(session, softwareSheet.getId(), "Default");

        // create Items
        DBInitializer.createItem(session, "Name", Sheet.ITEMTYPE_ATTRIBUTE, "name", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, Renderer.SHORT_TEXT, Editor.SHORT_TEXT);

    }

    private void initComputers(Session session) throws Hd3dException, NoSuchAlgorithmException
    {
        for (int i = 1; i < 10; i++)
        {
            DBInitializer.createComputer(session, "Dell Opteron E8400 - 0" + i, "Serial_0" + i, "billing_0" + i,
                    new Date(), new Date(), "OK", Long.valueOf(i), "dns_name_0" + i, "127.0.0." + i,
                    "mac_adress_0" + i, 2600, 1, 2, 8192);
        }
        for (int i = 1; i < 10; i++)
        {
            DBInitializer.createComputer(session, "Dell Lattitude E6400 - 0" + i, "Serial_" + i, "billing_" + i,
                    new Date(), new Date(), "OK", Long.valueOf(i), "dns_name_" + i, "127.0.0." + i,
                    "mac_adress_" + i, 1600, 1, 2, 4096);
        }
        for (int i = 10; i < 100; i++)
        {
            DBInitializer.createComputer(session, "Dell Lattitude E6400 - " + i, "Serial_" + i, "billing_" + i,
                    new Date(), new Date(), "OK", Long.valueOf(i), "dns_name_" + i, "127.0.0." + i,
                    "mac_adress_" + i, 1600, 1, 2, 4096);
        }

        DBInitializer.createComputerType(session, "Bureautique");
        DBInitializer.createComputerType(session, "Graphisme");

        DBInitializer.createComputerModel(session, "Dell Opteron E8400");
        DBInitializer.createComputerModel(session, "Dell Lattitude E6400");
        DBInitializer.createComputerModel(session, "Other");

        ISheet computerSheet = DBInitializer.createSheet(session, null, "Machines",
                "Permet d'effectuer un suivi de production pour les machines", Entity.COMPUTER, ESheetType.MACHINE);

        // create group
        IItemGroup defaultGroup = DBInitializer.createItemGroup(session, computerSheet.getId(), "Default");

        // create Items
        DBInitializer.createItem(session, "Name", Sheet.ITEMTYPE_ATTRIBUTE, "name", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Number of procs", Sheet.ITEMTYPE_ATTRIBUTE, "numberOfProcs", "",
                defaultGroup.getId(), Const.JAVA_LANG_LONG, Renderer.INT, Editor.INT);

        DBInitializer.createItem(session, "Number of cores", Sheet.ITEMTYPE_ATTRIBUTE, "numberOfCores", "",
                defaultGroup.getId(), Const.JAVA_LANG_LONG, Renderer.INT, Editor.INT);

        DBInitializer.createItem(session, "Proc frequency", Sheet.ITEMTYPE_ATTRIBUTE, "procFrequency", "", defaultGroup
                .getId(), Const.JAVA_LANG_STRING, Renderer.INT, Editor.INT);

        DBInitializer.createItem(session, "Ram quantity", Sheet.ITEMTYPE_ATTRIBUTE, "ramQuantity", "", defaultGroup
                .getId(), Const.JAVA_LANG_LONG, Renderer.INT, Editor.INT);

        DBInitializer.createItem(session, "Serial", Sheet.ITEMTYPE_ATTRIBUTE, "serial", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Billing Reference", Sheet.ITEMTYPE_ATTRIBUTE, "billingReference", "",
                defaultGroup.getId(), Const.JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Purchase date", Sheet.ITEMTYPE_ATTRIBUTE, "purchaseDate", "", defaultGroup
                .getId(), Const.JAVA_UTIL_DATE, Renderer.SHORT_DATE, Editor.DATE);

        DBInitializer.createItem(session, "Warranty end", Sheet.ITEMTYPE_ATTRIBUTE, "warrantyEnd", "", defaultGroup
                .getId(), Const.JAVA_UTIL_DATE, Renderer.SHORT_DATE, Editor.DATE);

        DBInitializer.createItem(session, "Inventory Status", Sheet.ITEMTYPE_ATTRIBUTE, "inventoryStatus", "",
                defaultGroup.getId(), Const.JAVA_LANG_STRING, Renderer.INVENTORY_STATUS, Editor.INVENTORY_STATUS);

        DBInitializer.createItem(session, "Inventory ID", Sheet.ITEMTYPE_ATTRIBUTE, "inventoryId", "", defaultGroup
                .getId(), Const.JAVA_LANG_LONG, Renderer.INT, Editor.INT);

        DBInitializer.createItem(session, "Mac Adress", Sheet.ITEMTYPE_ATTRIBUTE, "macAdress", "",
                defaultGroup.getId(), Const.JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "I.P. Adress", Sheet.ITEMTYPE_ATTRIBUTE, "ipAdress", "",
                defaultGroup.getId(), Const.JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "DNS Name", Sheet.ITEMTYPE_ATTRIBUTE, "dnsName", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

        DBInitializer.createItem(session, "Worker", Sheet.ITEMTYPE_ATTRIBUTE, "workerStatus", "", defaultGroup.getId(),
                Const.JAVA_LANG_STRING, Renderer.WORKER_STATUS, Editor.WORKER_STATUS);

        DBInitializer.createItem(session, "Model", Sheet.ITEMTYPE_ATTRIBUTE, "computerModel", "", defaultGroup.getId(),
                "ComputerModel", Renderer.RECORD, Editor.COMPUTER_MODEL);

        DBInitializer.createItem(session, "Type", Sheet.ITEMTYPE_ATTRIBUTE, "computerType", "", defaultGroup.getId(),
                "ComputerType", Renderer.RECORD, Editor.COMPUTER_TYPE);
    }

}
