package fr.hd3d.services.resources;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;


@Hd3dComment("Formulaire web de test d'upload de fichier (=>/tmp)")
public class TestUploadResource<L, P> extends AbstractHd3dBaseResource
{
    public TestUploadResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    public Representation get(Variant variant)
    {
        // Request r = getRequest();
        // String referreUrl = getRequest().getReferrerRef().toString();

        String page = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">"
                + "<html>"
                + "<head>"
                + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
                + "<title>Upload Form page</title>"
                + "</head>"
                + "<body>"
                + "TEST UPLOAD<br>"
                + "<form name='myform' method='POST' action='"
                // + referreUrl
                + "upload' enctype='multipart/form-data'>"
                + "<input type='file' name='file'>"
                + "<input type='submit' value='GO'>" + "</form> " + "</body>" + "</html>";

        Representation rep = new StringRepresentation(page, MediaType.TEXT_HTML);
        return rep;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
