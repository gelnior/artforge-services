package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.common.client.ServicesURI.PLANNING_MASTER_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.BooleanUtils;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.TaskChangesH;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPlanningPlannedTasks;
import fr.hd3d.model.persistence.impl.hibernate.query.ListResultQuery;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.ServerResourceUtils;


/**
 * @author try.lam
 * 
 */
public class ProjectPlanningSwitchMasterResource extends ProjectPlanningResource
{
    public ProjectPlanningSwitchMasterResource() throws Hd3dException
    {
        super();
        allowMethods("PUT");
    }

    @Override
    protected void doPostUpdate(Representation entity) throws Hd3dTranslationException
    {}

    /**
     * Switch from a master planning to another one.
     * 
     * @throws Hd3dException
     */
    @Override
    public Representation put(Representation entity, Variant variant)
    {
        if (false == initObject())
        {
            return getResponseEntity();
        }

        if (object == null)
        {
            /*
             * NOTE: do not set Status to Status.SUCCESS_NO_CONTENT If so, there will be no empty list returned in the
             * HTML screen, which may be confusing
             */
            return getResponseEntity();
        }

        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();

        try
        {
            /* make sure the master planning is really master */
            if (BooleanUtils.isFalse(object.isMaster()))
            {
                doOnError(new Hd3dException("Provided Master planning is not Master."), session, tx,
                        Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                return getResponseEntity();
            }

            /* retrieve the planning to swith to */
            final IPlanning newMaster = Persistors.planning.getById(getResourceContext().getIdFromUrl(
                    PLANNING_MASTER_ID_ATTRIBUTE));
            if (newMaster == null)
            {
                doOnError(new Hd3dException("Cannot find new master planning."), session, tx,
                        Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                return getResponseEntity();
            }

            if (newMaster != null && BooleanUtils.isTrue(newMaster.isMaster()))
            {
                doOnError(new Hd3dException("Planning to switch to is already Master."), session, tx,
                        Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                return getResponseEntity();
            }

            /*--------------------------------------------------------------------------
             * Update the old master's planned tasks with new master's task changes info
             --------------------------------------------------------------------------*/
            final List<ITaskChanges> newMasterTaskChanges = newMaster.getTaskChanges();
            for (final ITaskChanges tc : newMasterTaskChanges)
            {
                tc.updateTask();
            }

            /*------------------------------------------------------------------------------------------------------
             * for all the PLANNED tasks of the old master, if they don't have TaskChanges in the new master, create
             * them in the old master
             ------------------------------------------------------------------------------------------------------*/
            /* retrieve the old master's planned tasks */
            List<ITask> oldMasterPlannedTasks = null;
            {
                ListResultQuery<ILTask, ITask> query = new GetPlanningPlannedTasks();
                HibernateUtil.safeQuery(query);
                oldMasterPlannedTasks = query.result;
            }

            final List<Long> newMasterTaskChangesTaskIds = new ArrayList<Long>(CollectionUtils.collect(
                    newMasterTaskChanges, new Transformer<ITaskChanges, Long>() {

                        public Long transform(ITaskChanges object)
                        {
                            if (object == null || object.getTask() == null)
                                return null;
                            return object.getTask().getId();
                        }
                    }));

            Collection<Long> missingTasksInNewMaster = CollectionUtils.subtract(CollectUtils
                    .getIds(oldMasterPlannedTasks), newMasterTaskChangesTaskIds);

            for (final Long taskId : missingTasksInNewMaster)
            {
                ITask task = null;
                {
                    for (ITask t : oldMasterPlannedTasks)
                    {
                        if (taskId.equals(t.getId()))
                            task = t;
                    }
                }
                if (task == null)
                    continue;

                session.persist(new TaskChangesH(task, object));
            }

            /*--------------------------------------------------------------------------------
             * remove task changes in the new master (a new master does not have task changes)
             --------------------------------------------------------------------------------*/
            for (final ITaskChanges tc : newMasterTaskChanges)
            {
                tc.disable();
            }

            object.setMaster(Boolean.FALSE);
            newMaster.setMaster(Boolean.TRUE);

            /*
             * --------------- build response ----------------
             */
            ILPlanning lObject = getResourceContext().getTranslator().toLightweight(newMaster, getResourceContext());
            Representation r = getLightWeightRepresentation(variant, lObject);

            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            tx.commit();

            r.setCharacterSet(CharacterSet.UTF_8);
            getResponse().setStatus(Status.SUCCESS_OK);

            doPostRead(lObject);

            Log.LOGGER.info(responseInfo());

            getResponse().setEntity(ServerResourceUtils.encodeRepresentation(r));
        }
        catch (HibernateException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
        }
        catch (Hd3dTranslationException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
        }
        catch (Hd3dException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
        }
        finally
        {
            if (tx != null)
                tx = null;
            cleanup();
        }
        return getResponseEntity();
    }
}
