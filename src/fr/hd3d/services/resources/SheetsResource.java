package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSheet;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetSheetsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la listes des fiches d'identités. Une fiche est liée à une INTERFACE de classe métier, "
        + "l'une de celles retournées par http://services.hd3d.fr:11800/Hd3dServices/entities\r\n"
        + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Création d'une nouvelle fiche (et du portal associé)\r\n" + "\r\nDELETE:N/A")
public class SheetsResource extends AbstractHd3dCollection<ILSheet, ISheet>
{
    public SheetsResource() throws Hd3dException
    {
        super(Persistors.sheet, Translators.sheet);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetSheetsCmd(getResourceContext()).wantTotalSize());
    }
}
