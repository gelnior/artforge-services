package fr.hd3d.services.resources.transformer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@SuppressWarnings("unchecked")
public final class TransformersMap
{
    public static Map<String, ItemTransformer> transformers;

    static
    {
        List<Class<? extends ItemTransformer>> allTransformers = Arrays.asList(ApprovalNoteByTypeTransformer.class,
                PersonsByTaskTypeTransformer.class);

        Map<String, ItemTransformer> _temp = new HashMap<String, ItemTransformer>();
        for (Class<? extends ItemTransformer> t : allTransformers)
        {
            try
            {
                _temp.put(t.getCanonicalName(), t.newInstance());
            }
            catch (InstantiationException e)
            {}
            catch (IllegalAccessException e)
            {}
        }
        transformers = org.apache.commons.collections15.MapUtils.unmodifiableMap(_temp);
    }

    public static void init()
    {

    }

    public static ItemTransformer getTransformerInstance(String name)
    {
        return transformers.get(name);
    }
}
