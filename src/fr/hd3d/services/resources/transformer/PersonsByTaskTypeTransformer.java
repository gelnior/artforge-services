package fr.hd3d.services.resources.transformer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IPerson;


public class PersonsByTaskTypeTransformer implements ItemTransformer
{

    public Object transform(Object object, Object attribute, String parameters)
    {
        if (object == null)
            return null;

        IBase obj = (IBase) object;
        if (!obj.isTaskable())
            return null;

        final List<IEntityTaskLink> entityTaskLinks = obj.getEntityTaskLinks();

        /* case where attribute is the variable containing the task type name */
        if (attribute != null && attribute instanceof String)
        {
            return getPersons((String) attribute, entityTaskLinks);
        }
        else if (parameters != null)
        {
            return getPersons((String) parameters, entityTaskLinks);
        }
        else
        {
            return getPersons(null, entityTaskLinks);
        }
    }

    private Object getPersons(String taskType, List<IEntityTaskLink> entityTaskLinks)
    {
        Set<IPerson> persons = new HashSet<IPerson>(entityTaskLinks.size());

        for (IEntityTaskLink e : entityTaskLinks)
        {
            if (taskType != null)
            {
                if (taskType.equals(e.getTask().getTaskType().getName()))
                    persons.add(e.getTask().getWorker());
            }
            else
            {
                persons.add(e.getTask().getWorker());
            }
        }

        return persons;
    }

    public Object transform(Object object)
    {
        return transform(object, null, null);
    }

}
