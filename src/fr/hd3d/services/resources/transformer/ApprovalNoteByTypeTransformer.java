package fr.hd3d.services.resources.transformer;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IDynMetaDataValue;


@Deprecated
public class ApprovalNoteByTypeTransformer implements ItemTransformer
{
    // - object is the current instance
    // - attribute is expected to be a Set of IApprovalNote
    // - parameters is expected to be the list of IApprovalNote.type to retrieve
    // if parameter starts by "dyn:" (i.e "dyn:validationType", the value of the parameter is contained in a
    // DynMetaDataValue named validationType
    public Object transform(Object object, Object attribute, String parameters)
    {
        if (object == null)
            return null;

        // case where attribute is the variable containing the type of the ApprovalNote to retrieve
        if (attribute != null && attribute instanceof String)
            return ((IBase) object).getApprovalNotesByTypes(new String[] { (String) attribute });

        if (parameters == null)
            return ((IBase) object).getApprovalNotes();
        else
        {
            if (!parameters.startsWith("dyn:"))
            {
                String[] types = StringUtils.split(parameters, ',');
                return ((IBase) object).getApprovalNotesByTypes(types);
            }
            else
            {
                String approvalNoteTypeVarName = org.apache.commons.lang.StringUtils.substringAfter(parameters, "dyn:");
                List<IDynMetaDataValue> dynValues = ((IBase) object).getDynMetaDataValues();
                String approvalNoteTypeValue = null;
                for (IDynMetaDataValue v : dynValues)
                    if (v.getClassDynMetaDataType().getName().equals(approvalNoteTypeVarName))
                    {
                        approvalNoteTypeValue = v.getValue();
                        break;
                    }
                return ((IBase) object).getApprovalNotesByTypes(new String[] { approvalNoteTypeValue });
            }
        }
    }

    public Object transform(Object object)
    {
        return transform(object, null, null);
    }

}
