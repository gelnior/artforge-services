package fr.hd3d.services.resources.transformer;

import org.apache.commons.collections.Transformer;


public interface ItemTransformer extends Transformer
{
    /**
     * Do operations using the current object, the attribute and additional parameters
     * 
     * @param object
     *            the object instance
     * @param attribute
     *            the attribute value to be transformed
     * @param parameters
     *            used by transformer
     * @return
     */
    public Object transform(Object object, Object attribute, String parameters);

}
