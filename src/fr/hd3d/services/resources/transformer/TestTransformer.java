package fr.hd3d.services.resources.transformer;

public class TestTransformer implements ItemTransformer
{

    public Object transform(Object object, Object attribute, String parameters)
    {
        System.out.println("Hello, it is TestTransformer");
        return attribute;
    }

    public Object transform(Object object)
    {
        return transform(object, null, null);
    }
}
