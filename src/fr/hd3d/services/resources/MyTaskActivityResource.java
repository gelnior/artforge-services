/**
 * 
 */
package fr.hd3d.services.resources;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.services.security.utils.AuthenticationUtil;


/**
 * Access for modification and delete of an identified task activity. When a task activity is deleted, the linked task
 * actual start date is update with the oldest activity date for this task.
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'une activité de tâche {taskActivityID}\r\n"
        + "\r\nPUT:Modification d'une activité de tâche\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Désactivation d'une activité de tâche")
public class MyTaskActivityResource extends BaseTaskActivityResource
{

    public MyTaskActivityResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected void doPreUpdate(Representation entity, ILTaskActivity taskActivity) throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user == null)
            return;

        if (user.getId().longValue() == object.getFilledBy().getId().longValue())
        {
            super.doPreUpdate(entity, taskActivity);
        }
        else
        {
            throw new Hd3dException("You cannot modify activity you don't own.");
        }
    }

    @Override
    protected void doPreDelete() throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user == null)
            return;
        if (user.getId().longValue() == object.getWorker().getId().longValue())
        {
            super.doPreDelete();
        }
        else
        {
            throw new Hd3dException("You cannot  delete activity you don't own.");
        }
    }

    @Override
    protected void doPostDelete() throws Hd3dException
    {
        super.doPostDelete();
        this.updateTaskActualStartDate();
    }

}
