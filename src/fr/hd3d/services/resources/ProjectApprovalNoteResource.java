/**
 * 
 */
package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.APPROVALNOTE_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import org.hibernate.LockMode;
import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Access for modification and deletion for an identified approval note of a given project.
 * 
 * @author HD3D
 */
@Hd3dComment("GET: Retrieve approval note where ID is equal to {taskActivityID}\r\n"
        + "\r\nPUT: Modify approval note where ID is equal to {taskActivityID} \r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE: Delete approval note where ID is equal to {taskActivityID}")
public class ProjectApprovalNoteResource extends AbstractHd3dResource<ILApprovalNote, IApprovalNote>
{

    public ProjectApprovalNoteResource() throws Hd3dException
    {
        super(Persistors.approvalnote, Translators.approvalnote);
    }

    @Override
    protected IApprovalNote initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(APPROVALNOTE_ID_ATTRIBUTE, lockMode);
    }

    @Override
    protected void doPreUpdate(Representation entity, ILApprovalNote approvalNote) throws Hd3dException
    {
        Long projectId = getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        IApprovalNoteType noteType = Persistors.approvalnotetype.getById(approvalNote.getApprovalNoteType());

        if (projectId != null && projectId.longValue() == noteType.getProject().getId().longValue())
        {
            super.doPreUpdate(entity, approvalNote);
        }
        else
        {
            throw new Hd3dException("You cannot modify approval note from another project.");
        }
    }

    @Override
    protected void doPreDelete() throws Hd3dException
    {
        Long projectId = getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        IApprovalNoteType noteType = object.getApprovalNoteType();

        if (projectId != null && projectId.longValue() == noteType.getProject().getId().longValue())
        {
            super.doPreDelete();
        }
        else
        {
            throw new Hd3dException("You cannot delete approval note from another project.");
        }
    }

}
