package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILDeviceType;
import fr.hd3d.model.persistence.IDeviceType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all device pool objects.<br>
 * URI : /deviceTypes/
 * 
 * @author HD3D
 */
public class DeviceTypesResource extends AbstractHd3dCollection<ILDeviceType, IDeviceType>
{
    public DeviceTypesResource() throws Hd3dException
    {
        super(Persistors.deviceType, Translators.deviceType);
    }
}
