package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSkill;
import fr.hd3d.model.persistence.ISkill;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer skill objects.<br>
 * URI : /skills/
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération des skill de machines\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Création d'un skill de machines\r\n" + "\r\nDELETE:N/A")
public class SkillsResource extends AbstractHd3dCollection<ILSkill, ISkill>
{

    public SkillsResource() throws Hd3dException
    {
        super(Persistors.skill, Translators.skill);
    }
}
