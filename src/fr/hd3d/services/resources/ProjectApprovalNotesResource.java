package fr.hd3d.services.resources;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetApprovalNotesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class ProjectApprovalNotesResource extends AbstractHd3dCollection<ILApprovalNote, IApprovalNote>
{

    public ProjectApprovalNotesResource() throws Hd3dException
    {
        super(Persistors.approvalnote, Translators.approvalnote);
    }

    @Override
    protected ResultCollection<IApprovalNote> getCollectionForRepresentation() throws Hd3dException
    {
        Long projectId = getResourceContext().getIdFromUrl(ServicesURI.PLANNING_ID_ATTRIBUTE);
        List<IApprovalNoteType> types = Persistors.approvalnotetype.getByValue("project.id", projectId);
        List<Long> typeIds = new ArrayList<Long>();
        for (IApprovalNoteType type : types)
        {
            typeIds.add(type.getId());
        }

        return doGetCollection(new GetApprovalNotesCmd(getResourceContext(), typeIds).wantTotalSize());
    }

}
