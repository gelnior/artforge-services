package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.SHEET_FILTER_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.impl.LSheetFilter;
import fr.hd3d.model.persistence.impl.hibernate.query.AbstractHibernateQuery;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.model.preferences.SheetFilter;
import fr.hd3d.model.translator.impl.SheetFilterTranslator;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.services.translator.json.JSonFormSerializer;
import fr.hd3d.utils.HibernateUtil;


@Hd3dComment("GET:Récupération du sheet filter identifié par l'id. " + "\r\nPUT:Mise a jour du sheet filter\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:Effacement du sheet filter")
public class SheetFilterResource extends AbstractHd3dSingleResource<LSheetFilter, SheetFilter>
{
    public SheetFilterResource() throws Hd3dException
    {
        super(SheetFilter.class, new SheetFilterTranslator());
        allowMethods("PUT,DELETE");
    }

    protected SheetFilter initializeObject() throws Hd3dException
    {
        Long id = getResourceContext().getIdFromUrl(SHEET_FILTER_ID_ATTRIBUTE);

        final GetObject<SheetFilter> cmd = new GetObject<SheetFilter>(id);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    protected SheetFilter initializeObject(LockMode lockMode) throws Hd3dException
    {
        Long id = getResourceContext().getIdFromUrl(SHEET_FILTER_ID_ATTRIBUTE);

        final GetObject<SheetFilter> cmd = new GetObject<SheetFilter>(getResourceContext(), id);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    class GetObject<A> extends AbstractHibernateQuery<SheetFilter>
    {
        A result;
        private final Long id;

        public GetObject(Long id)
        {
            this.id = id;
        }

        public GetObject(Long id, LockMode lockMode)
        {
            super(lockMode);
            this.id = id;
        }

        public GetObject(final ResourceContext<?, ?> context, Long id)
        {
            super(context);
            this.id = id;
        }

        public GetObject(final ResourceContext<?, ?> context, Long id, LockMode lockMode)
        {
            super(context, lockMode);
            this.id = id;
        }

        @SuppressWarnings("unchecked")
        public void execute(org.hibernate.Session session) throws Hd3dException
        {
            try
            {
                // final Criteria query = basicIdCriteria(session, id);
                // setLockMode(query);
                result = (A) getCriteria(session).uniqueResult();
            }
            catch (HibernateException e)
            {
                A obj = (A) session.get(SheetFilter.class, id);
                if (obj != null)
                {
                    // if (Const.INTERNALSTATUS_ENABLED.equals(((IBase) obj).getInternalStatus()))
                    // {
                    result = obj;
                    // }
                }
            }
        }

        @Override
        protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dPersistenceException
        {
            final Criteria query = basicIdCriteria(session, id);
            return query;
        }
    }

    public Criteria basicIdCriteria(Session session, Long id)
    {
        return basicIdCriteria(session, id, SheetFilter.class);
    }

    public Criteria basicIdCriteria(Session session, Long id, Class<?> clazz)
    {
        Criteria criteria = session.createCriteria(clazz);
        criteria.add(Restrictions.idEq(id));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria;
    }

    protected Representation getLightWeightRepresentation(Variant variant, LSheetFilter lightweight)
            throws Hd3dTranslationException
    {
        return RessourceSerializer.getRepresentation(variant, lightweight);
    }

    protected boolean preCheck(SheetFilter object)
    {
        if (object == null)
            return false;
        return true;
    }

    protected void doPreDelete() throws Hd3dPersistenceException
    {
    /* nothing to do */
    }

    protected void doPostDelete() throws Hd3dPersistenceException
    {
    /* nothing to do */
    }

    protected void doDelete() throws Hd3dPersistenceException
    {
        HibernateUtil.safeTransaction(new IHibernateTransaction() {

            public void execute(org.hibernate.Session session) throws Hd3dException
            {
                session.delete(object);
                // session.flush();
            }
        });
    }

    protected void auditDelete(SheetFilter object)
    {
    /* no audit for SheetFilterList */
    }

    protected void doPreUpdate(Representation entity, LSheetFilter lObject) throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    protected void doUpdate(LSheetFilter lObject) throws Hd3dException
    {
        SheetFilterTranslator translator = new SheetFilterTranslator();
        translator.updateFromLightweight(lObject, object, getResourceContext());
        HibernateUtil.safeTransaction(new IHibernateTransaction() {
            public void execute(org.hibernate.Session session) throws Hd3dException
            {
                session.merge(object);
            }
        });
    }

    protected void doPostUpdate(Representation entity) throws Hd3dTranslationException
    {
        sucessfulPut(getLightweight(object), entity.getMediaType());
    }

    protected void auditPost(SheetFilter object)
    {
    /* nothing to do */
    }

    protected void auditPost(List<SheetFilter> objects)
    {
    /* nothing to do */
    }

    protected void doPersist(SheetFilter object) throws Hd3dPersistenceException
    {
    /* nothing to do */
    }

    protected void doPostPersist(SheetFilter object) throws Hd3dPersistenceException
    {
    /* nothing to do */
    }

    protected void doPersist(List<SheetFilter> objects) throws Hd3dPersistenceException
    {
    /* nothing to do */
    }

    protected void doPostPersist(List<SheetFilter> objects) throws Hd3dPersistenceException
    {
    /* nothing to do */
    }

    protected LSheetFilter getLightweight(SheetFilter object) throws Hd3dTranslationException
    {
        SheetFilterTranslator translator = new SheetFilterTranslator();
        return translator.toLightweight(object, getResourceContext());
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        final SheetFilter sheetFilter = (SheetFilter) object;
        StringBuilder buf = new StringBuilder(getRequest().getResourceRef().getBaseRef().toString());
        buf.append('/').append(sheetFilter == null ? "null" : sheetFilter.getSheet());
        return buf.toString();
    }

    @Override
    protected void doPrePersist(Representation entity, SheetFilter object) throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    @Override
    protected void doPrePersistCollection(Representation entity, List<SheetFilter> newObjects)
            throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    @Override
    protected SheetFilter newObject(Representation entity) throws Hd3dException
    {
        LSheetFilter lObject = JSonFormSerializer.<LSheetFilter> deserialize(entity);
        if (lObject == null)
            return null;
        SheetFilter object = getResourceContext().getTranslator().fromLightweight(lObject, getResourceContext());
        lObject = null;
        return object;
    }

    @Override
    protected List<SheetFilter> newCollection(Representation entity) throws Hd3dException
    {
        List<LSheetFilter> lObjects = JSonFormSerializer.<LSheetFilter> deserializeList(entity);
        if (lObjects == null)
            return new ArrayList<SheetFilter>();

        List<SheetFilter> objects = new ArrayList<SheetFilter>(lObjects.size());
        for (LSheetFilter light : lObjects)
            CollectionUtils.addIgnoreNull(objects, getResourceContext().getTranslator().fromLightweight(light,
                    getResourceContext()));

        /* clear */
        for (LSheetFilter light : lObjects)
            light.clear();
        lObjects.clear();
        lObjects = null;

        return objects;
    }

    @Override
    protected void doPostRead(LSheetFilter object)
    {
    /* nothing to do */
    }
}
