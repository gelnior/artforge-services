package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ROLE_ID_ATTRIBUTE;

import java.util.Map;

import org.hibernate.LockMode;
import org.restlet.representation.Representation;

import fr.hd3d.common.client.Persist;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.translator.json.JSonFormSerializer;


/**
 * 
 * @author florent-della-valle
 * 
 */
public class RoleResource extends AbstractHd3dResource<ILRole, IRole>
{

    public RoleResource() throws Hd3dException
    {
        super(Persistors.role, Translators.role);
    }

    @Override
    protected IRole initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ROLE_ID_ATTRIBUTE, lockMode);
    }

    protected ILRole doPreUpdate(Representation entity, Map<String, String> map) throws Hd3dException
    {
        final ILRole lObject = JSonFormSerializer.<ILRole> deserialize(entity);

        /* if perms or bans were changed, check whether the user had the right to do so... */
        if (object != null && !object.canBeUpdatedFrom(lObject))
        {
            throw new Hd3dTranslationException("user may not modify the role with Id " + object.getId());
        }
        if (object != null)
        {
            // PreUpdate
            object.doPreUpdate(lObject);
            executeScripts(object, Persist.PREUPDATE, getRequest().getResourceRef().toString());
        }
        return lObject;
    }

}
