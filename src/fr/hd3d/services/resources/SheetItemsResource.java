package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILItem;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetSheetItemsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * Collection resource for items. It returns items linked to given sheet. It handles creation too.
 * 
 * @author HD3D
 */
@Hd3dComment("GET: Returns items linked to given sheet. \r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST: Create new item. \r\n"
        + "\r\nDELETE:N/A")
public class SheetItemsResource extends AbstractHd3dCollection<ILItem, IItem>
{
    public SheetItemsResource() throws Hd3dException
    {
        super(Persistors.item, Translators.item);
    }

    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetSheetItemsCmd(getResourceContext()).wantTotalSize());
    }
}
