package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevisionAnnotation;
import fr.hd3d.model.persistence.IFileRevisionAnnotation;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class FileRevisionAnnotationsResource extends
        AbstractHd3dCollection<ILFileRevisionAnnotation, IFileRevisionAnnotation>
{

    public FileRevisionAnnotationsResource() throws Hd3dException
    {
        super(Persistors.filerevisionannotation, Translators.filerevisionannotation);
    }

}
