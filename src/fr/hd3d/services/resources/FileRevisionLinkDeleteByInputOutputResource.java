package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.INPUT_FILEREVISION_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.OUTPUT_FILEREVISION_ID_ATTRIBUTE;

import java.util.List;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevisionLink;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;


/**
 * @author Try LAM
 */
public class FileRevisionLinkDeleteByInputOutputResource extends
        AbstractHd3dResource<ILFileRevisionLink, IFileRevisionLink>
{

    public FileRevisionLinkDeleteByInputOutputResource() throws Hd3dException
    {
        super(Persistors.filerevisionlink, Translators.filerevisionlink);
        allowMethods("GET,DELETE");
    }

    @Override
    protected IFileRevisionLink initializeObject(LockMode lockMode) throws Hd3dException
    {
        final Long inputFileRevisionId = getResourceContext().getIdFromUrl(INPUT_FILEREVISION_ID_ATTRIBUTE);
        final Long outputFileRevisionId = getResourceContext().getIdFromUrl(OUTPUT_FILEREVISION_ID_ATTRIBUTE);

        final IFileRevision inputFileRevision = Persistors.filerevision.getById(inputFileRevisionId);
        final IFileRevision outputFileRevision = Persistors.filerevision.getById(outputFileRevisionId);

        /* find out FileRevisionLink matching given inputFileRevision & outputFileRevision */
        String[] properties = new String[] { Const.INPUTFILEREVISION, Const.OUTPUTFILEREVISION };
        Object[] values = new Object[] { inputFileRevision, outputFileRevision };
        final List<IFileRevisionLink> fileRevisionLinks = Persistors.filerevisionlink.getByValues(getResourceContext(),
                properties, values, "AND");
        if (CollectUtils.isEmpty(fileRevisionLinks))
            return null;
        return fileRevisionLinks.get(0);// there must be only one
    }
}
