package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILConstituentLink;
import fr.hd3d.model.persistence.IConstituentLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class ConstituentLinksResource extends AbstractHd3dCollection<ILConstituentLink, IConstituentLink>
{

    public ConstituentLinksResource() throws Hd3dException
    {
        super(Persistors.constituentlink, Translators.constituentlink);
    }
}
