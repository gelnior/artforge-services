package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.utils.CollectUtils.getLightIds;
import static fr.hd3d.utils.CollectUtils.removeNull;
import static org.apache.commons.collections15.CollectionUtils.forAllDo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.LockMode;
import org.restlet.data.CharacterSet;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.Persist;
import fr.hd3d.exception.Hd3dAuthorizationException;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.ListResultQuery;
import fr.hd3d.model.persistence.impl.hibernate.query.Queries;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.services.translator.json.JSonFormSerializer;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ServerResourceUtils;
import fr.hd3d.utils.closure.LoadApprovalNotesClosure;
import fr.hd3d.utils.closure.LoadBoundTasksClosure;
import fr.hd3d.utils.closure.LoadDynMetaDataValuesClosure;
import fr.hd3d.utils.closure.LoadEntityTaskLinksClosure;
import fr.hd3d.utils.closure.LoadTagNamesClosure;
import fr.hd3d.utils.closure.LoadTagsClosure;


/**
 * Abstract base class for all Collections of RESLET Resources
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 * @param <L>
 *            Lightweight persisted object
 * @param <P>
 *            Regular persisted object
 */
public abstract class AbstractHd3dCollection<L extends ILBase, P extends IBase> extends
        AbstractHd3dCollectionResource<L, P>
{
    public AbstractHd3dCollection(IPersist<P> persistor, IBaseTranslator<L, P> translator) throws Hd3dException
    {
        super(persistor, translator);
        allowMethods("POST,PUT");// DELETE is not allowed
    }

    // ============================================================
    //
    // Override READ methods
    //
    // ============================================================
    protected List<L> getLightWeightList(ResultCollection<P> list) throws Hd3dException
    {
        List<L> ret;
        if (list == null || list.isEmpty())
        {
            ret = new ArrayList<L>(0);
        }
        else
        {
            ret = getResourceContext().getTranslator().toLightweightCollection(list.getResult(), getResourceContext());

            loadLightWeightTags(ret);

            /* read bound dynMetaDataValues */
            loadLightWeightDynMetaDataValues(ret);

            /* read bound approvalNotes */
            loadLightWeightApprovalNotes(ret);

            /* read bound entityTaskLinks */
            List<IEntityTaskLink> entityTasklinks = loadLightWeightEntityTaskLinks(ret);

            /* read bound boundTasks */
            /* Note: the bound tasks list is only present for a GET */
            loadLightWeightBoundTasks(ret, entityTasklinks);

            /* clear */
            if (entityTasklinks != null)
            {
                entityTasklinks.clear();
                entityTasklinks = null;
            }
        }

        return ret;
    }

    private void loadLightWeightTags(List<L> lObjects) throws Hd3dTranslationException
    {
        List<TagParent> tagParents = null;
        if (Const.TRUE.equals(getResourceContext().getQueryMap().get(Const.TAGS)))
        {
            tagParents = loadTags(lObjects);
            loadTagNames(lObjects, tagParents);
        }
        if (tagParents != null)
        {
            tagParents.clear();
            tagParents = null;
        }
    }

    private void loadLightWeightDynMetaDataValues(List<L> lObjects) throws Hd3dTranslationException
    {
        if (Const.TRUE.equals(getResourceContext().getQueryMap().get(Const.DYN)))
        {
            loadDynMetaDataValues(lObjects);
        }
    }

    private void loadLightWeightApprovalNotes(List<L> lObjects) throws Hd3dTranslationException
    {
        if (Const.TRUE.equals(getResourceContext().getQueryMap().get(Const.APPROVALS)))
        {
            loadApprovalNotes(lObjects);
        }
    }

    private List<IEntityTaskLink> loadLightWeightEntityTaskLinks(List<L> lObjects) throws Hd3dTranslationException
    {
        List<IEntityTaskLink> entityTasklinks = null;
        if (Const.TRUE.equals(getResourceContext().getQueryMap().get(Const.ENTITYTASKLINKS)))
        {
            entityTasklinks = loadEntityTaskLinks(lObjects);
        }
        return entityTasklinks;
    }

    private void loadLightWeightBoundTasks(List<L> lObjects, List<IEntityTaskLink> entityTasklinks)
            throws Hd3dTranslationException
    {
        if (Const.TRUE.equals(getResourceContext().getQueryMap().get(Const.DYN)))
        {
            loadBoundTasks(lObjects, entityTasklinks);
        }
    }

    private List<TagParent> loadTags(List<L> lights) throws Hd3dTranslationException
    {
        final String entityName = getResourceContext().getPersistedClassEntityName();
        final List<TagParent> tagParents = Queries.getTagParents(getLightIds(lights), entityName);
        forAllDo(lights, new LoadTagsClosure(tagParents));

        return tagParents;
    }

    private void loadTagNames(List<L> lights, List<TagParent> formerTagParents) throws Hd3dTranslationException
    {
        final String entityName = getResourceContext().getPersistedClassEntityName();
        List<TagParent> tagParents;
        if (formerTagParents == null)
        {
            tagParents = Queries.getTagParents(getLightIds(lights), entityName);
        }
        else
        {
            tagParents = formerTagParents;
        }

        forAllDo(lights, new LoadTagNamesClosure(tagParents));
    }

    private void loadDynMetaDataValues(List<L> lights) throws Hd3dTranslationException
    {
        /* read bound dynMetaDataValues */
        final String entityName = getResourceContext().getPersistedClassEntityName();
        List<IDynMetaDataValue> dyns = Queries.getDynMetaDataValues(getLightIds(lights), entityName);
        forAllDo(lights, new LoadDynMetaDataValuesClosure(dyns, getResourceContext()));
    }

    private void loadApprovalNotes(List<L> lights) throws Hd3dTranslationException
    {
        // read bound dynMetaDataValues
        final String entityName = getResourceContext().getPersistedClassEntityName();
        List<IApprovalNote> approvalNotes = Queries.getApprovalNotes(getLightIds(lights), entityName);
        forAllDo(lights, new LoadApprovalNotesClosure(approvalNotes, getResourceContext()));
    }

    private List<IEntityTaskLink> loadEntityTaskLinks(List<L> lights) throws Hd3dTranslationException
    {
        final String entityName = getResourceContext().getPersistedClassEntityName();
        List<IEntityTaskLink> entityTaskLinks = Queries.getEntityTasksLinks(getLightIds(lights), entityName);
        forAllDo(lights, new LoadEntityTaskLinksClosure(entityTaskLinks, getResourceContext()));
        return entityTaskLinks;
    }

    private void loadBoundTasks(List<L> lights, List<IEntityTaskLink> formerEntityTaskLinks)
            throws Hd3dTranslationException
    {
        final String entityName = getResourceContext().getPersistedClassEntityName();
        List<IEntityTaskLink> entityTaskLinks;
        if (formerEntityTaskLinks == null)
        {
            entityTaskLinks = Queries.getEntityTasksLinks(getLightIds(lights), entityName);
        }
        else
        {
            entityTaskLinks = formerEntityTaskLinks;
        }

        forAllDo(lights, new LoadBoundTasksClosure(entityTaskLinks, getResourceContext()));
    }

    @SuppressWarnings("unchecked")
    protected Representation getLightWeightRepresentation(Variant variant, List lightweightList, int nbRecords)
    {
        return RessourceSerializer.<L> getRepresentation(variant, lightweightList, nbRecords);
    }

    /**
     * Get collection from a query with filters.
     * 
     * @param filter
     * @return the result of the query
     * @throws Hd3dPersistenceException
     * @throws Hd3dException
     */
    @SuppressWarnings("unchecked")
    protected ResultCollection<P> getCollectionForRepresentation() throws Hd3dException
    {
        ResultCollection<P> resultCollection = (ResultCollection<P>) getResourceContext().getPersistor()
                .getCollectionWithTotal(getResourceContext());
        return resultCollection;
    }

    @SuppressWarnings("unchecked")
    protected ResultCollection<P> reloadCollectionIfAny(final ResultCollection luceneFiltered, boolean onIdsOnly)
            throws Hd3dException
    {
        if (onIdsOnly)
        {
            List _entities = getResourceContext().getPersistor().getByIds(getResourceContext(),
                    luceneFiltered.getResult());
            createDynMetaDataValues(_entities);
            return new ResultCollection<P>(_entities, luceneFiltered.getCount());
        }
        else
        {
            createDynMetaDataValues(luceneFiltered.getResult());
            return luceneFiltered;
        }
    }

    /**
     * Creates DynMetaDataValues for the passed list of business entity instances
     * 
     * @param objects
     * @throws Hd3dPersistenceException
     */
    public synchronized void createDynMetaDataValues(List<P> objects) throws Hd3dException
    {
        ServerResourceUtils.createDynMetaDataValuesFor(objects, getResourceContext().getPersistedClassEntityName());
    }

    @SuppressWarnings("unchecked")
    protected ResultCollection<P> doGetCollection(ListResultQuery<L, P> query) throws Hd3dException
    {
        ResultCollection<P> resultCollection;

        // if (QueryFilterFactory.hibernatePagination(getResourceContext().getQueryMap()))
        // {
        resultCollection = getCollection(query);
        // }
        // else
        // {
        // query.setProjectionList(Projections.projectionList().add(Projections.id()));
        // resultCollection = getCollection(query);
        // }

        // createDynMetaDataValues((List<P>) resultCollection.getResult());
        return resultCollection;
    }

    @Override
    protected void doPostRead(ResultCollection<P> list, List<L> lightweightList)
    {
        /* clear */
        if (list != null && list.isNotEmpty())
        {
            list.getResult().clear();
            list = null;
        }

        CollectUtils.removeNull(lightweightList);
        if (lightweightList != null)
        {
            for (L light : lightweightList)
            {
                light.clear();
            }
            lightweightList.clear();
            lightweightList = null;
        }
    }

    // ============================================================
    //
    // Override CREATION methods
    //
    // ============================================================

    protected void doPrePersist(Representation entity, P newObject) throws Hd3dException
    {
        newObject.doPrePersist();
        executeScripts(newObject, Persist.PREPERSIST, getNewIdentifier(newObject));
    }

    protected void doPersist(P object) throws Hd3dException
    {
        if (object != null)
        {
            getResourceContext().getPersistor().persist(object);
        }
    }

    protected void doPostPersist(P object) throws Hd3dException
    {
        if (object != null)
        {
            object.doPostPersist();
            executeScripts(object, Persist.POSTPERSIST, successfulPost(object));
        }
    }

    /**
     * For a collection
     * 
     * @param lObjects
     * @return
     * @throws Hd3dException
     */
    protected void doPrePersistCollection(Representation entity, List<P> newObjects) throws Hd3dException
    {
        /* PrePersist */
        for (P object : newObjects)
        {
            object.doPrePersist();
        }

        /* Note:objects.get(0) or whatever index does not matter */
        if (!newObjects.isEmpty())
        {
            executeScripts(newObjects.get(0), Persist.PREPERSIST, getNewIdentifier(newObjects));
        }
    }

    protected void doPersist(List<P> objects) throws Hd3dException
    {
        if (CollectUtils.isNotEmpty(objects))
        {
            getResourceContext().getPersistor().persistCollection(objects);
        }
    }

    protected void doPostPersist(List<P> objects) throws Hd3dException
    {
        removeNull(objects);

        if (CollectUtils.isNotEmpty(objects))
        {
            postPersistBulk(objects);

            final String identifier = successfulPost(objects);

            // Note:objects.get(0) or whatever index does not matter
            executeScripts(objects.get(0), Persist.POSTPERSIST, identifier);
        }
    }

    protected void postPersistBulk(List<P> objects) throws Hd3dException
    {
        for (P object : objects)
        {
            object.doPostPersist();
        }
    }

    // ============================================================
    //
    // Override UPDATE methods
    //
    // ============================================================
    @SuppressWarnings("unchecked")
    protected List<P> retrieveObjects(final Representation entity, final List<L> lObjects, final LockMode lockMode)
            throws Hd3dException
    {
        List<P> ret = new ArrayList<P>();

        CollectUtils.removeNull(lObjects);
        if (CollectUtils.isNotEmpty(lObjects))
        {
            List<Long> ids = CollectUtils.getLightIds(lObjects);
            List<P> result = getResourceContext().getPersistor().getByIds(getResourceContext(), ids, lockMode);

            ret.addAll(CollectUtils.reorderById(ids, result));

            /* Do not clear light objects collection because used later */

        }

        return ret;
    }

    protected void doPreUpdate(Representation entity, List<L> lObjects, List<P> objects) throws Hd3dException
    {
        CollectUtils.removeNull(objects);
        CollectUtils.removeNull(lObjects);

        if (CollectUtils.isNotEmpty(objects) && CollectUtils.isNotEmpty(lObjects))
        {
            /* PreUpdate */
            L lObject;
            P object;
            for (int i = 0; i < objects.size(); i++)
            {
                lObject = lObjects.get(i);
                object = objects.get(i);

                // checks user permissions on object
                if (object.canUpdate())
                {
                    object.doPreUpdate(lObject);
                }
                else
                {
                    throw new Hd3dAuthorizationException("user does not have 'update' rights on " + object.entityName()
                            + " with id " + object.getId());
                }
            }

            executeScripts(objects.get(0), Persist.PREUPDATE, getRequest().getResourceRef().toString());
        }
    }

    protected void doUpdate(List<L> lObjects, List<P> objects) throws Hd3dException
    {
        CollectUtils.removeNull(objects);
        CollectUtils.removeNull(lObjects);

        if (CollectUtils.isNotEmpty(objects) && CollectUtils.isNotEmpty(lObjects))
        {
            /* PreUpdate */
            L lObject;
            P object;
            for (int i = 0; i < objects.size(); i++)
            {
                lObject = lObjects.get(i);
                object = objects.get(i);

                // checks user permissions on object
                if (object.canUpdate())
                {
                    getResourceContext().getTranslator().updateFromLightweight(lObject, object, getResourceContext());
                }
                else
                {
                    throw new Hd3dAuthorizationException("user does not have 'update' rights on " + object.entityName()
                            + " with id " + object.getId());
                }
            }

            getResourceContext().getPersistor().mergeCollection(objects);
        }
    }

    protected void doPostUpdate(List<P> objects, Representation entity) throws Hd3dException
    {
        removeNull(objects);
        if (CollectUtils.isNotEmpty(objects))
        {
            postUpdateBulk(objects);

            List<L> updated = getResourceContext().getTranslator().toLightweightCollection(objects,
                    getResourceContext());

            sucessfulPut(updated, entity.getMediaType());

            executeScripts(objects.get(0), Persist.POSTUPDATE, getRequest().getResourceRef().toString());

            updated.clear();
            updated = null;
        }
    }

    protected void postUpdateBulk(final List<P> objects) throws Hd3dException
    {
        for (P o : objects)
        {
            o.doPostUpdate();
        }
    }

    protected void sucessfulPut(Collection<L> object, MediaType type)
    {
        // Set the response's status and entity
        Representation r = RessourceSerializer.getRepresentation(type, object);
        r.setCharacterSet(CharacterSet.UTF_8);
        getResponse().setEntity(ServerResourceUtils.encodeRepresentation(r));
        getResponse().setStatus(Status.SUCCESS_OK);
    }

    /**
     * used by Hd3dResource to create the object upon POST request
     * 
     * @param form
     * @return
     * @throws Hd3dTranslationException
     * @throws Hd3dException
     */
    protected P newObject(Representation entity) throws Hd3dException
    {
        final L lObject = JSonFormSerializer.<L> deserialize(entity);
        return getResourceContext().getTranslator().fromLightweight(lObject, getResourceContext());
    }

    protected List<P> newCollection(Representation entity) throws Hd3dException
    {
        List<P> objects = new ArrayList<P>();

        List<L> lObjects = JSonFormSerializer.<L> deserializeList(entity);
        if (CollectUtils.isNotEmpty(lObjects))
        {
            objects = getResourceContext().getTranslator().fromLightweightCollection(lObjects, getResourceContext());

            /* cleaning */
            for (L light : lObjects)
            {
                light.clear();
            }
            lObjects.clear();
            lObjects = null;
        }

        return objects;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        return object == null ? null : StringUtils.join(new Object[] { getRequest().getResourceRef().getBaseRef(),
                ((P) object).getId() }, '/');
    }

    // ============================================================
    //
    // Override DELETE methods
    //
    // ============================================================
    protected boolean preCheck(final List<P> objects)
    {
        boolean ok;

        if (CollectUtils.isEmpty(objects))
        {
            error(Status.CLIENT_ERROR_BAD_REQUEST, new Hd3dTranslationException("ENTITIES DO NOT EXIST"), ERROR_OCCURED);
            ok = false;
        }
        else if (!CollectUtils.allEnabled(objects))
        {
            error(Status.CLIENT_ERROR_BAD_REQUEST, new Hd3dTranslationException("ENTITY HAS BEEN DISABLED"),
                    ERROR_OCCURED);
            ok = false;
        }
        else
        {
            ok = true;
        }

        return ok;
    }

    protected void doPreDelete(final List<P> objects) throws Hd3dException
    {
        for (P object : objects)
        {
            object.doPreDelete();
            executeScripts(object, Persist.PREDELETE, getRequest().getResourceRef().toString());
        }
    }

    protected void doDelete(final List<P> objects) throws Hd3dException
    {
        getResourceContext().getPersistor().disableCollection(objects);
    }

    protected void doPostDelete(final List<P> objects) throws Hd3dException
    {
        for (P object : objects)
        {
            object.doPostDelete();
            executeScripts(object, Persist.POSTDELETE, getRequest().getResourceRef().toString());
        }
        getResponse().setStatus(Status.SUCCESS_OK);
    }
}
