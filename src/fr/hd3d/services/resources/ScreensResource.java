package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILScreen;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all software objects.<br>
 * URI : /screens/
 * 
 * @author HD3D
 */
public class ScreensResource extends AbstractHd3dCollection<ILScreen, IScreen>
{
    public ScreensResource() throws Hd3dException
    {
        super(Persistors.screen, Translators.screen);
    }
}
