package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTaskType;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectTaskTypesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * Give access to task types linked to the project.
 * 
 * @author HD3D
 */
@Hd3dComment("GET: Retrieve task types linked to the project.")
public class ProjectTaskTypesResource extends AbstractHd3dCollection<ILTaskType, ITaskType>
{
    public ProjectTaskTypesResource() throws Hd3dException
    {
        super(Persistors.tasktype, Translators.taskType);
    }

    @Override
    protected ResultCollection<ITaskType> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectTaskTypesCmd(getResourceContext()).wantTotalSize());
    }
}
