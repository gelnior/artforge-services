package fr.hd3d.services.resources;

import org.hd3d2.sp4.common.search.AssetsSearch;
import org.hd3d2.sp4.common.search.AssetsSearchResult;
import org.restlet.Context;
import org.restlet.data.CharacterSet;
import org.restlet.data.Method;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;
import org.restlet.resource.ClientResource;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.ontology.annotation.BaseProducer;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.services.translator.json.JSonFormSerializer;


public class AssetsSearchResource extends AbstractHd3dBaseResource
{
    public AssetsSearchResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("POST");
    }

    @Override
    public Representation post(Representation entity, Variant variant)
    {
        try
        {
            final ClientResource client = new ClientResource(new Context(), Method.POST,
                    BaseProducer.ANNOTATION_SERVER_URI);

            org.hd3d2.sp4.common.search.AssetsSearchResource searchResource = client.getChild("/search/assets/",
                    org.hd3d2.sp4.common.search.AssetsSearchResource.class);

            AssetsSearch searchQuery = JSonFormSerializer.<AssetsSearch> deserialize(entity);

            AssetsSearchResult result = searchResource.search(searchQuery);

            // build the representation
            Representation r = RessourceSerializer.getRepresentation(variant, result);
            r.setCharacterSet(CharacterSet.UTF_8);
            return r;
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
