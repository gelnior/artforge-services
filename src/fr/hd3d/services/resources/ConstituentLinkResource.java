package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ASSETREVISIONLINK_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILConstituentLink;
import fr.hd3d.model.persistence.IConstituentLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class ConstituentLinkResource extends AbstractHd3dResource<ILConstituentLink, IConstituentLink>
{

    public ConstituentLinkResource() throws Hd3dException
    {
        super(Persistors.constituentlink, Translators.constituentlink);
    }

    @Override
    protected IConstituentLink initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ASSETREVISIONLINK_ID_ATTRIBUTE, lockMode);
    }
}
