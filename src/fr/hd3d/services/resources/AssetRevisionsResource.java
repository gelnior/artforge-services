package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class AssetRevisionsResource extends AbstractHd3dCollection<ILAssetRevision, IAssetRevision>
{

    public AssetRevisionsResource() throws Hd3dException
    {
        super(Persistors.assetrevision, Translators.assetrevision);
    }
}
