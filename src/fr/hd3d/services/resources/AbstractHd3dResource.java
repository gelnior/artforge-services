package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.utils.CollectUtils.getIds;
import static org.apache.commons.collections15.CollectionUtils.collect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.sojo.interchange.json.JsonSerializer;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ChainedTransformer;
import org.apache.commons.lang.StringUtils;
import org.hibernate.LockMode;
import org.restlet.Response;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.Persist;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.lightweight.ILDynMetaDataValue;
import fr.hd3d.model.lightweight.ILEntityTaskLink;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.services.translator.json.JSonFormSerializer;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ServerResourceUtils;
import fr.hd3d.utils.transformer.GetEntityTaskLinkTaskTransformer;
import fr.hd3d.utils.transformer.GetTagNameTransformer;
import fr.hd3d.utils.transformer.GetTagTransformer;


/**
 * Abstract base class for all RESLET Resource
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 * @param <L>
 *            Lightweight persisted object
 * @param <P>
 *            Regular persisted object
 */
public abstract class AbstractHd3dResource<L extends ILBase, P extends IBase> extends AbstractHd3dSingleResource<L, P>
{
    public AbstractHd3dResource(IPersist<P> persistor, IBaseTranslator<L, P> translator) throws Hd3dException
    {
        super(persistor, translator);
        allowMethods("POST,PUT,DELETE");
    }

    /**
     * used by Hd3dResource to initialize the object and provide default behaviors
     * 
     * @return
     * @throws Hd3dPersistenceException
     */
    protected abstract P initializeObject(LockMode lockMode) throws Hd3dException;

    protected P initializeObject(final String idString, LockMode lockMode) throws Hd3dException
    {
        final P obj = (P) getResourceContext().getPersistor().getById(getResourceContext(),
                getResourceContext().getIdFromUrl(idString), lockMode);
        createDynMetaDataValue(obj);
        return obj;
    }

    protected synchronized void createDynMetaDataValue(final P object) throws Hd3dException
    {
        ServerResourceUtils.createDynMetaDataValueFor(object, getResourceContext().getPersistedClassEntityName());
    }

    /**
     * @param errorCode
     * @param errorMessage
     * @param response
     */
    static void generateErrorRepresentation(String errorCode, String errorMessage, Response response)
    {
        response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
        final ResourceError resourceError = new ResourceError(errorCode, errorMessage);
        response.setEntity(new StringRepresentation(new JsonSerializer().serialize(resourceError).toString()));
    }

    // ============================================================
    //
    // Override READ methods
    //
    // ============================================================
    protected Representation getLightWeightRepresentation(Variant variant, L lightweight)
            throws Hd3dTranslationException
    {
        return RessourceSerializer.getRepresentation(variant, lightweight);
    }

    protected Representation getLightWeightRepresentation(Variant variant, List<L> lightweightList, int nbRecords)
    {
        return RessourceSerializer.<L> getRepresentation(variant, lightweightList, nbRecords);
    }

    protected L getLightweight(P object) throws Hd3dException
    {
        L light = getResourceContext().getTranslator().toLightweight(object, getResourceContext());

        final Map<String, String> map = getResourceContext().getQueryMap();

        /* tags */
        List<TagParent> tagParents = null;
        if (Const.TRUE.equals(map.get(Const.TAGS)))
        {
            tagParents = loadTags(light);
            loadTagNames(light, tagParents);
        }
        if (tagParents != null)
        {
            tagParents.clear();
        }

        /* read bound dynMetaDataValues */
        if (Const.TRUE.equals(map.get(Const.DYN)))
        {
            loadDynMetaDataValues(light);
        }

        /* read bound approvalNotes */
        if (Const.TRUE.equals(map.get(Const.APPROVALS)))
        {
            loadApprovalNotes(light);
        }

        /* read bound entityTaskLinks */
        List<IEntityTaskLink> entityTaskLinks = null;
        if (Const.TRUE.equals(map.get(Const.ENTITYTASKLINKS)))
        {
            entityTaskLinks = loadEntityTaskLinks(light);
        }

        /* read bound boundTasks */
        /* Note: the bound tasks list is only present for a GET */
        if (Const.TRUE.equals(map.get(Const.BOUNDTASKS)))
        {
            loadBoundTasks(light, entityTaskLinks);
        }

        if (entityTaskLinks != null)
        {
            entityTaskLinks.clear();
        }

        return light;
    }

    private List<TagParent> loadTags(final L light) throws Hd3dTranslationException
    {
        Set<TagParent> tagParents = object.getTagParents();

        List<Long> tags;
        if (CollectUtils.isNotEmpty(tagParents))
        {
            tags = getIds(collect(tagParents, new GetTagTransformer()));
        }
        else
        {
            tags = new ArrayList<Long>(0);
        }
        light.setTags(tags);

        return new ArrayList<TagParent>(tagParents);
    }

    private void loadTagNames(final L light, final List<TagParent> formerTagParents) throws Hd3dTranslationException
    {
        Set<TagParent> tagParents;
        if (formerTagParents == null)
        {
            tagParents = object.getTagParents();
        }
        else
        {
            tagParents = new HashSet<TagParent>(formerTagParents);
        }

        List<String> tagNames;
        if (CollectUtils.isNotEmpty(tagParents))
        {
            tagNames = new ArrayList<String>(collect(tagParents, new ChainedTransformer<TagParent, String>(
                    new Transformer[] { new GetTagTransformer(), new GetTagNameTransformer() })));
        }
        else
        {
            tagNames = new ArrayList<String>(0);
        }

        light.setTagNames(tagNames);
    }

    private void loadDynMetaDataValues(final L light) throws Hd3dException
    {
        List<ILDynMetaDataValue> lDynValues = new ArrayList<ILDynMetaDataValue>();

        CollectUtils.addAllIfNotEmpty(lDynValues, Translators.dynmetadatavalue.toLightweightCollection(object
                .getDynMetaDataValues(), getResourceContext()));

        light.setDynMetaDataValues(lDynValues);
    }

    private List<IEntityTaskLink> loadEntityTaskLinks(final L light) throws Hd3dException
    {
        Collection<ILEntityTaskLink> lEntityTaskLinks = new HashSet<ILEntityTaskLink>();

        List<IEntityTaskLink> entityTaskLinks = object.getEntityTaskLinks();
        if (CollectUtils.isNotEmpty(entityTaskLinks))
        {
            lEntityTaskLinks = Translators.entitytasklink
                    .toLightweightCollection(entityTaskLinks, getResourceContext());
        }

        light.setEntityTaskLinks(new HashSet<ILEntityTaskLink>(lEntityTaskLinks));

        return entityTaskLinks;
    }

    private void loadBoundTasks(final L light, final List<IEntityTaskLink> formerEntityTaskLinks) throws Hd3dException
    {
        List<IEntityTaskLink> entityTaskLinks;
        if (formerEntityTaskLinks == null)
        {
            entityTaskLinks = object.getEntityTaskLinks();
        }
        else
        {
            entityTaskLinks = formerEntityTaskLinks;// reuse previously retrieved list
        }

        Collection<ILTask> lTasks = new HashSet<ILTask>();
        if (CollectUtils.isNotEmpty(entityTaskLinks))
        {
            Collection<ITask> tasks = collect(entityTaskLinks, new GetEntityTaskLinkTaskTransformer());
            lTasks = Translators.task.toLightweightCollection(tasks, getResourceContext());
        }

        light.setBoundTasks(new HashSet<ILTask>(lTasks));
    }

    private void loadApprovalNotes(L light) throws Hd3dException
    {
        Set<ILApprovalNote> lApprovals = new HashSet<ILApprovalNote>();

        CollectUtils.addAllIfNotEmpty(lApprovals, Translators.approvalnote.toLightweightCollection(object
                .getApprovalNotes(), getResourceContext()));

        light.setApprovalNotes(lApprovals);
    }

    @Override
    protected void doPostRead(L lObject)
    {
        /* clear */
        if (lObject != null)
        {
            lObject.clear();
            lObject = null;
        }
    }

    protected boolean preCheck(P object)
    {
        boolean ok;

        if (object == null)
        {
            error(Status.CLIENT_ERROR_BAD_REQUEST, new Hd3dTranslationException("ENTITY DOES NOT EXIST"), ERROR_OCCURED);
            ok = false;
        }
        else if (!object.enabled())
        {
            error(Status.CLIENT_ERROR_BAD_REQUEST, new Hd3dTranslationException("ENTITY HAS BEEN DISABLED"),
                    ERROR_OCCURED);
            ok = false;
        }
        else
        {
            ok = true;
        }

        return ok;
    }

    // ============================================================
    //
    // Override DELETE methods
    //
    // ============================================================
    protected void doPreDelete() throws Hd3dException
    {
        // PreDelete
        object.doPreDelete();
        executeScripts(object, Persist.PREDELETE, getRequest().getResourceRef().toString());
    }

    protected void doDelete() throws Hd3dException
    {
        getResourceContext().getPersistor().disable(object);
    }

    protected void doPostDelete() throws Hd3dException
    {
        object.doPostDelete();
        getResponse().setStatus(Status.SUCCESS_OK);
        executeScripts(object, Persist.POSTDELETE, getRequest().getResourceRef().toString());
    }

    // ============================================================
    //
    // Override CREATION methods
    //
    // ============================================================
    protected void doPrePersist(Representation entity, P newObject) throws Hd3dException
    {
        newObject.doPrePersist();
        executeScripts(newObject, Persist.PREPERSIST, getNewIdentifier(newObject));
    }

    protected void doPersist(P object) throws Hd3dException
    {
        getResourceContext().getPersistor().persist(object);
    }

    protected void doPostPersist(P object) throws Hd3dException
    {
        if (object != null)
        {
            object.doPostPersist();
            final String identifier = successfulPost(object);
            executeScripts(object, Persist.POSTPERSIST, identifier);
        }
    }

    protected void doPreUpdate(Representation entity, L lObject) throws Hd3dException
    {
        // PreUpdate
        object.doPreUpdate(lObject);
        executeScripts(object, Persist.PREUPDATE, getRequest().getResourceRef().toString());
    }

    /**
     * For a collection
     * 
     * @param lObjects
     * @return
     * @throws Hd3dException
     */
    protected void doPrePersistCollection(Representation entity, List<P> objects) throws Hd3dException
    {
        // PrePersist
        for (P object : objects)
        {
            object.doPrePersist();
        }
        executeScripts(objects.get(0), Persist.PREPERSIST, getNewIdentifier(objects));
    }

    protected void doPersist(List<P> objects) throws Hd3dException
    {
        getResourceContext().getPersistor().persistCollection(objects);
    }

    protected void doPostPersist(List<P> objects) throws Hd3dException
    {
        CollectUtils.removeNull(objects);

        if (CollectUtils.isNotEmpty(objects))
        {
            for (P object : objects)
            {
                object.doPostPersist();
            }
            final String identifier = successfulPost(objects);
            executeScripts(objects.get(0), Persist.POSTPERSIST, identifier);
        }
    }

    protected final P getObject()
    {
        return object;
    }

    /**
     * used by Hd3dResource to create the object upon POST request
     * 
     * @param form
     * @return
     * @throws Hd3dException
     */
    protected P newObject(Representation entity) throws Hd3dException
    {
        P object = null;

        L lObject = JSonFormSerializer.<L> deserialize(entity);
        if (lObject != null)
        {
            object = getResourceContext().getTranslator().fromLightweight(lObject, getResourceContext());
            lObject = null;
        }

        return object;
    }

    protected List<P> newCollection(Representation entity) throws Hd3dException
    {
        List<P> objects = new ArrayList<P>();

        List<L> lObjects = JSonFormSerializer.<L> deserializeList(entity);
        if (CollectUtils.isNotEmpty(lObjects))
        {
            objects = getResourceContext().getTranslator().fromLightweightCollection(lObjects, getResourceContext());

            /* cleaning */
            for (L light : lObjects)
            {
                light.clear();
            }
            lObjects.clear();
            lObjects = null;
        }

        return objects;
    }

    @Override
    protected P copy(Representation entity)
    {
        P copy = super.copy(entity);
        copy.setId(null);
        copy.setInternalUUID(BaseH.getUUID());
        return copy;
    }

    // ============================================================
    //
    // Override MODIFICATION methods
    //
    // ============================================================
    @SuppressWarnings("unchecked")
    protected void doUpdate(L lObject) throws Hd3dException
    {
        getResourceContext().getTranslator().updateFromLightweight(lObject, object, getResourceContext());
        getResourceContext().getPersistor().merge(object);

    }

    protected void doPostUpdate(Representation entity) throws Hd3dException
    {
        object.doPostUpdate();
        L lObject = getLightweight(object);
        sucessfulPut(lObject, entity.getMediaType());
        executeScripts(object, Persist.POSTUPDATE, getRequest().getResourceRef().toString());

        if (lObject != null)
        {
            lObject.clear();
            lObject = null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        String baseRef = getRequest().getResourceRef().getBaseRef().toString();

        String lastSegment = getRequest().getOriginalRef().getLastSegment();
        if (fr.hd3d.utils.Const.COPY.equals(lastSegment))
        {
            baseRef = StringUtils.substringBeforeLast(baseRef, "/");
        }
        return StringUtils.join(new Object[] { baseRef, (object == null) ? null : ((P) object).getId() }, '/');
    }
}
