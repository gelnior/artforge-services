package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


@Hd3dComment("Abstract base webservice for all resources related to file upload")
public abstract class AbstractHd3dUploadBaseResource<L, P> extends AbstractHd3dBaseResource
{
    public AbstractHd3dUploadBaseResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("POST");
    }

    public Representation post(Representation entity, Variant variant)
    {
        /* POST request with no entity or no form */
        if (entity == null || false == MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true))
        {
            Log.LOGGER.error("*** Error: NO ENTITY\n");
            getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            return getResponseEntity();
        }

        /* initialize upload restlet stuffs */
        RestletFileUpload upload = new RestletFileUpload(new DiskFileItemFactory()) {
            {
                setFileSizeMax(150000000);
                setSizeMax(150000000);
            }
        };

        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();

        List<FileItem> items = null;
        Map<String, Object> resultMap = null;
        try
        {
            /* Parsing parameters */
            items = upload.parseRequest(getRequest());

            /* map used to store result from processing */
            resultMap = new HashMap<String, Object>();

            for (final FileItem fi : items)
            {
                /* parse form fields and uploaded files */
                parseUploadedFiles(resultMap, fi);
                parseTextFields(resultMap, fi);
            }

            /* processing results from resultMap */
            process(resultMap);

            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            tx.commit();

            /* build representation */
            Representation rep = buildRepresentation(resultMap);
            getResponse().setEntity(rep);
            getResponse().setStatus(Status.SUCCESS_OK);
        }
        catch (Exception e)
        {
            Log.LOGGER.error("*** Error: EXCEPTION!!\n");
            error(Status.CLIENT_ERROR_BAD_REQUEST, e, ERROR_OCCURED);
        }
        finally
        {
            upload = null;
            if (resultMap != null)
                resultMap.clear();
            if (tx != null)
                tx = null;
            cleanup();
        }

        return getResponseEntity();
    }

    abstract protected void parseUploadedFiles(Map<String, Object> resultMap, FileItem fileItem);

    abstract protected void parseTextFields(Map<String, Object> resultMap, FileItem fileItem);

    abstract protected void process(Map<String, Object> resultMap) throws Hd3dException;

    abstract protected Representation buildRepresentation(Map<String, Object> resultMap);
}
