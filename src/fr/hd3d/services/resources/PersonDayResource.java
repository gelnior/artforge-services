package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.DAY_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author david gauthier
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'une journée et les activités\r\n" + "\r\nPUT:Mise a jour d'une journée\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class PersonDayResource extends AbstractHd3dResource<ILPersonDay, IPersonDay>
{
    public PersonDayResource() throws Hd3dException
    {
        super(Persistors.personDay, Translators.personDay);
    }

    @Override
    protected IPersonDay initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(DAY_ID_ATTRIBUTE, lockMode);
    }
}
