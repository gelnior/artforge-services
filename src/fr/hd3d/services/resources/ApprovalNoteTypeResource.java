package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.APPROVALNOTETYPE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNoteType;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class ApprovalNoteTypeResource extends AbstractHd3dResource<ILApprovalNoteType, IApprovalNoteType>
{

    public ApprovalNoteTypeResource() throws Hd3dException
    {
        super(Persistors.approvalnotetype, Translators.approvalnotetype);
    }

    @Override
    protected IApprovalNoteType initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(APPROVALNOTETYPE_ID_ATTRIBUTE, lockMode);
    }
}
