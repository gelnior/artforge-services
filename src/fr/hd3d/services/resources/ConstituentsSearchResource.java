package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.StringValueTransformer;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ConstituentH;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


/**
 * Return all the available business entities
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:recherche des constituents correspondant aux critères passés en paramètre. "
        + "projs = projet; tags= tags; shots= shots;cats=catégories; seqs=séquences."
        + "On peut passer plusieurs valeurs pour chaque paramètre, séparées par des virgules\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
@Deprecated
public class ConstituentsSearchResource<L, P> extends AbstractHd3dBaseResource
{
    public ConstituentsSearchResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Representation get(Variant variant)
    {
        // list of values with ',' separator
        String tags = getQuery().getValues("tags");
        String shots = getQuery().getValues("shots");
        String seqs = getQuery().getValues("seqs");
        String cats = getQuery().getValues("cats");
        String projs = getQuery().getValues("projs");

        String[] tagsId = null, shotsId, seqsId, catsId, projsId;

        // create native Lucene query
        BooleanQuery bq = new BooleanQuery();

        if (StringUtils.isNotBlank(tags))
        {
            tagsId = StringUtils.split(tags, ',');
        }
        if (StringUtils.isNotBlank(shots))
        {
            shotsId = StringUtils.split(shots, ',');
            for (String shotId : shotsId)
            {
                TermQuery tq = new TermQuery(new Term("shots_id", shotId));
                bq.add(new BooleanClause(tq, BooleanClause.Occur.MUST));
            }
        }
        if (StringUtils.isNotBlank(seqs))
        {
            seqsId = StringUtils.split(seqs, ',');
            for (String seqId : seqsId)
            {
                TermQuery tq = new TermQuery(new Term("sequences_id", seqId));
                bq.add(new BooleanClause(tq, BooleanClause.Occur.MUST));
            }
        }
        if (StringUtils.isNotBlank(cats))
        {
            catsId = StringUtils.split(cats, ',');
            for (String catId : catsId)
            {
                TermQuery tq = new TermQuery(new Term("categories_id", catId));
                bq.add(new BooleanClause(tq, BooleanClause.Occur.MUST));
            }
        }
        if (StringUtils.isNotBlank(projs))
        {
            projsId = StringUtils.split(projs, ',');
            for (String projId : projsId)
            {
                TermQuery tq = new TermQuery(new Term("category_project", projId));
                bq.add(new BooleanClause(tq, BooleanClause.Occur.MUST));
            }
        }

        FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
        org.hibernate.Transaction tx = fullTextSession.beginTransaction();

        // System.out.println(bq);

        // wrap Lucene query in a org.hibernate.Query
        org.hibernate.Query hibQuery = fullTextSession.createFullTextQuery(bq, ConstituentH.class);

        // execute search
        List<IConstituent> result = hibQuery.list();
        // System.out.println(result);

        tx.commit();
        tx = null;

        // filter on tags (Note: tags are NOT indexed)
        if (!ArrayUtils.isEmpty(tagsId))
        {
            // if the results are not empty, apply extra filter by tags
            if (CollectionUtils.isNotEmpty(result))
            {
                List<IConstituent> filteredList = new ArrayList<IConstituent>(result.size());
                for (IConstituent baseObject : result)
                {
                    Collection<String> resultTagsId = CollectionUtils.collect(fr.hd3d.utils.CollectUtils
                            .getIds(baseObject.tags()), StringValueTransformer.getInstance());

                    if (org.apache.commons.collections15.CollectionUtils.containsAny(resultTagsId, Arrays
                            .asList(tagsId)))
                    {
                        filteredList.add(baseObject);
                    }
                }
                ((ArrayList) result).trimToSize();
                result = filteredList;
            }
            else
            {
                // otherwise, search only by tags
                List<IConstituent> consts = new ArrayList<IConstituent>(tagsId.length);
                Collection<Long> tagsIdColl = new ArrayList<Long>(tagsId.length);
                for (String id : tagsId)
                {
                    tagsIdColl.add(NumberUtils.toLong(id));
                }
                try
                {
                    List<ITag> _tags = Persistors.tag.getByIds(null, tagsIdColl);
                    for (ITag tag : _tags)
                    {
                        Collection<IConstituent> col = tag.<IConstituent> parentsByType("Constituent");
                        Collection<IConstituent> colObjs = Persistors.constituent.getByIds(null,
                                fr.hd3d.utils.CollectUtils.getIds(col));
                        consts.addAll(colObjs);
                    }
                    ((ArrayList) consts).trimToSize();
                    result = consts;
                }
                catch (Hd3dException e)
                {
                    Log.LOGGER.error("error occured when searching constituent by tags: {}", e);
                }
            }
        }

        // Convert result objects to lightweight objects.
        final List<ILConstituent> lightweightList = new ArrayList<ILConstituent>(result.size());
        for (IConstituent baseObject : result)
            try
            {
                lightweightList.add(Translators.constituent.toLightweight(baseObject, getResourceContext()));
            }
            catch (Hd3dTranslationException e)
            {
                error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
                return getResponseEntity();
            }
            catch (Hd3dException e)
            {
                error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
                return getResponseEntity();
            }

        Representation r = null;
        r = RessourceSerializer.getRepresentation(variant, lightweightList);
        r.setCharacterSet(CharacterSet.UTF_8);
        return r;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        StringBuilder buf = new StringBuilder(getRequest().getResourceRef().getBaseRef().toString());
        buf.append('/').append(((IConstituent) object).getId());
        buf.trimToSize();
        return buf.toString();
    }
}
