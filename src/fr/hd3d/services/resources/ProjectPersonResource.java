package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectPersonCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.UniqueResultQuery;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


/**
 * A resource to access to a specific person of project.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'une personne\r\n" + "\r\nPUT:Mise a jour d'une personne\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:N/A")
public class ProjectPersonResource extends AbstractHd3dResource<ILPerson, IPerson>
{
    public ProjectPersonResource() throws Hd3dException
    {
        super(Persistors.person, Translators.person);
    }

    @Override
    protected IPerson initializeObject(LockMode lockMode) throws Hd3dException
    {
        final UniqueResultQuery<ILPerson, IPerson> cmd = new GetProjectPersonCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        IPerson obj = (IPerson) cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }
}
