package fr.hd3d.services.resources;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.collections15.MultiMap;
import org.apache.commons.collections15.bag.HashBag;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.ServerResourceUtils;


public class ApprovalNoteBySequenceIdsByDateResource extends AbstractHd3dBaseResource
{

    public ApprovalNoteBySequenceIdsByDateResource() throws Hd3dException
    {
        super();
        allowMethods(null);
        getResourceContext().complete();
    }

    // @SuppressWarnings("unchecked")
    // @Override
    // public Representation get(Variant variant)
    // {
    // Session session = HibernateUtil.currentSession();
    // org.hibernate.Transaction tx = session.beginTransaction();
    // try
    // {
    // /* info from url */
    // // final String projectIdString = (String) getResourceContext().getQueryMap().get(Const.PROJECTID);
    // final String dateString = (String) getResourceContext().getQueryMap().get(Const.DATE);
    // final String sequenceIdsString = (String) getResourceContext().getQueryMap().get(Const.IDS);
    //
    // /* convert in proper types */
    // // final Long projectId = NumberUtils.toLong(projectIdString);
    // List<String> sequenceIdsStringList = Arrays.asList(org.apache.commons.lang.StringUtils.split(
    // sequenceIdsString, ','));
    // final List<Long> sequenceIds = CollectUtils.toLong(sequenceIdsStringList);
    //
    // MultiMap<String, Long> mapSequence = new MultiHashMap<String, Long>();
    // Map<String, IApprovalNote> shotsApprovalNoteType = new HashMap<String, IApprovalNote>();
    //
    // Set<IShot> allShots = new HashSet<IShot>();
    //
    // /* query */
    //
    // /* retrieve all sequences and sub-sequences */
    // // Set<ISequence> allSequences = new HashSet<ISequence>();
    // List<ISequence> sequences = Persistors.sequence.getByIds(getResourceContext(), sequenceIds);
    // for (ISequence sequence : sequences)
    // {
    // Set<IShot> allShotsInSequence = new HashSet<IShot>();
    // for (ISequence subSequence : sequence.allChildren(true))
    // {
    // allShots.addAll(subSequence.getShots());
    // allShotsInSequence.addAll(subSequence.getShots());
    // }
    // for (IShot shot : allShotsInSequence)
    // {
    // mapSequence.put(String.valueOf(sequence.getId()), shot.getId());
    // }
    //
    // }
    //
    // /* retrieve shots of the above sequences */
    // if (CollectUtils.isNotEmpty(allShots))
    // {
    // final Criteria criteria = ((HibernatePersist) Persistors.approvalnote).basicCriteria(session,
    // ApprovalNoteH.class);
    // criteria.add(Restrictions.in("boundEntity", CollectUtils.getIds(allShots)));
    // criteria.add(Restrictions.eq("boundEntityName", "Shot"));
    // criteria.add(Restrictions.le("approvalDate", fr.hd3d.utils.StringUtils.parseDate(dateString)));
    // criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
    //
    // List<IApprovalNote> result = criteria.list();
    //
    // for (IApprovalNote object : result)
    // {
    // if (object == null || object.getBoundEntity() == null || object.getApprovalNoteType() == null)
    // continue;
    //
    // if (shotsApprovalNoteType.get(object.getBoundEntity() + object.getApprovalNoteType().toString()) != null)
    // {
    // if (shotsApprovalNoteType
    // .get(object.getBoundEntity() + object.getApprovalNoteType().toString())
    // .getApprovalDate().before(object.getApprovalDate()))
    // {
    // shotsApprovalNoteType.put(
    // object.getBoundEntity() + object.getApprovalNoteType().toString(), object);
    // }
    // }
    // else
    // {
    // shotsApprovalNoteType.put(object.getBoundEntity() + object.getApprovalNoteType().toString(),
    // object);
    // }
    // }
    //
    // List<IApprovalNote> resultFiltered = new ArrayList<IApprovalNote>();
    // for (String key : shotsApprovalNoteType.keySet())
    // {
    // resultFiltered.add(shotsApprovalNoteType.get(key));
    // }
    //
    // /* properly format response */
    // MultiMap<String, ILApprovalNote> resp = new MultiHashMap<String, ILApprovalNote>(result.size());
    // for (IApprovalNote object : resultFiltered)
    // {
    // for (ISequence sequence : sequences)
    // {
    // if (mapSequence.get(sequence.getId().toString()) != null
    // && mapSequence.get(sequence.getId().toString()).contains(object.getBoundEntity()))
    // {
    // resp.put(String.valueOf(sequence.getId()), Translators.approvalnote.toLightweight(object,
    // getResourceContext()));
    // }
    // }
    // }
    //
    // Representation r = RessourceSerializer.getRepresentation(variant, resp.map());
    // r.setCharacterSet(CharacterSet.UTF_8);
    // return ServerResourceUtils.encodeRepresentation(r);
    // }
    // else
    // {
    // Representation r = RessourceSerializer.getRepresentation(variant, null);
    // r.setCharacterSet(CharacterSet.UTF_8);
    // return ServerResourceUtils.encodeRepresentation(r);
    // }
    // }
    // catch (Hd3dException e)
    // {
    // Log.LOGGER.error(ExceptUtils.format(e));
    // Representation r = new StringRepresentation(ExceptUtils.format(e));
    // r.setCharacterSet(CharacterSet.UTF_8);
    // return ServerResourceUtils.encodeRepresentation(r);
    // }
    // finally
    // {
    // if (tx != null)
    // tx = null;
    // cleanup();
    // }
    // }

    @Override
    public Representation get(Variant variant)
    {
        Session session = HibernateUtil.currentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        try
        {
            /* info from url */
            final String dateString = (String) getResourceContext().getQueryMap().get(Const.DATE);
            final String sequenceIdsString = (String) getResourceContext().getQueryMap().get(Const.IDS);

            /* convert in proper types */
            final List<Long> sequenceIds = CollectUtils.toLong(Arrays.asList(org.apache.commons.lang.StringUtils.split(
                    sequenceIdsString, ',')));

            /* KEY=top Sequence Id, VALUE=top Sequence's shots and children shots */
            MultiMap<String, Long> topSequenceToShotsMap = new MultiHashMap<String, Long>();

            /* KEY=top Sequence Id, VALUE=total numbre of frames of the shots */
            Map<Long, Integer> topSequenceToShotsNbFramesMap = new HashMap<Long, Integer>();

            /* KEY=Top Sequence's shots id and all children's shots id, VALUE=Top Sequence id */
            Map<Long, Long> shotToTopSequenceMap = new HashMap<Long, Long>();

            Set<IShot> allShots = new HashSet<IShot>();

            populate(sequenceIds, topSequenceToShotsMap, topSequenceToShotsNbFramesMap, shotToTopSequenceMap, allShots);

            if (CollectUtils.isNotEmpty(allShots))
            {
                List<Object[]> result = query(dateString, CollectUtils.getIds(allShots), session);

                /* KEY=Top Sequence Id, VALUE=<KEY=approvalNoteTypeId, VALUE=BAG of approvalNoteStatus> */
                Map<Long, Map<Long, HashBag<String>>> ret = process(result, shotToTopSequenceMap);

                /*-
                 * KEY=Top Sequence Id,
                 * VALUE=<KEY=approvalNoteTypeId OR "nbFrame" OR "nbWo" ,
                 * VALUE=<KEY=ApprovalNoteStatus,
                 * VALUE=number of approvalNotes with this status>>
                 * OR total number of frames
                 * OR total number of work objects
                 * "nbFrame" = total number of frames of shots
                 * "nbWo" = total number of shots
                 */
                MultiMap<String, Map<String, ?>> formattedRet = formatResponse(ret, topSequenceToShotsNbFramesMap,
                        topSequenceToShotsMap);

                Representation r = RessourceSerializer.getRepresentation(variant, formattedRet.map());
                r.setCharacterSet(CharacterSet.UTF_8);
                return ServerResourceUtils.encodeRepresentation(r);
            }
            else
            {
                Representation r = RessourceSerializer.getRepresentation(variant, null);
                r.setCharacterSet(CharacterSet.UTF_8);
                return ServerResourceUtils.encodeRepresentation(r);
            }
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            Representation r = new StringRepresentation(ExceptUtils.format(e));
            r.setCharacterSet(CharacterSet.UTF_8);
            return ServerResourceUtils.encodeRepresentation(r);
        }
        finally
        {
            if (tx != null)
                tx = null;
            cleanup();
        }
    }

    private void populate(List<Long> sequenceIds, MultiMap<String, Long> topSequenceToShotsMap,
            Map<Long, Integer> topSequenceToShotsNbFramesMap, Map<Long, Long> shotToTopSequenceMap, Set<IShot> allShots)
            throws Hd3dException
    {
        /* retrieve all sequences and sub-sequences */
        List<ISequence> topSequences = Persistors.sequence.getByIds(getResourceContext(), sequenceIds);
        for (ISequence topSequence : topSequences)
        {
            Set<IShot> allShotsInSequence = new HashSet<IShot>();
            for (ISequence subSequence : topSequence.allChildren(true))
            {
                allShots.addAll(subSequence.getShots());
                allShotsInSequence.addAll(subSequence.getShots());
            }

            topSequenceToShotsMap.putAll(String.valueOf(topSequence.getId()), CollectUtils.getIds(allShotsInSequence));

            Integer totalNbFrame = 0;
            for (IShot shot : allShotsInSequence)
            {
                shotToTopSequenceMap.put(shot.getId(), topSequence.getId());
                /* compute total number of frames of shots */
                totalNbFrame += shot.getNbFrame();
            }
            topSequenceToShotsNbFramesMap.put(topSequence.getId(), totalNbFrame);
        }
    }

    @SuppressWarnings("unchecked")
    private List<Object[]> query(String dateString, List<Long> shotIds, Session session)
    {
        final String sql = "SELECT * FROM   ("
                + " select "
                + " this_.approvalnote_boundentity as y0_, this_.approvalnote_approvalnotetype as y1_, this_.approvalnote_status as y2_, this_.approvalnote_approvaldate as y3_ "
                + " from hd3d_approvalnote this_ where 0 = this_.internalStatus and this_.approvalnote_boundentity in ("
                + StringUtils.join(CollectUtils.toString(shotIds), ',')
                + ") and this_.approvalnote_boundentityname='Shot' and this_.approvalnote_approvaldate<='"
                + dateString
                + "' "
                + " order by this_.approvalnote_boundentity asc, this_.approvalnote_approvalnotetype asc, this_.approvalnote_approvaldate desc, this_.approvalnote_status asc "
                + " ) as s" + " group by y0_,y1_";

        // final Criteria criteria = ((HibernatePersist) Persistors.approvalnote).basicCriteria(session,
        // ApprovalNoteH.class);
        // criteria.setProjection(Projections.projectionList().add(Projections.property("boundEntity")).add(
        // Projections.property("approvalNoteType")).add(Projections.property("status")).add(
        // Projections.property("approvalDate")));
        // criteria.add(Restrictions.in("boundEntity", shotIds));
        // criteria.add(Restrictions.eq("boundEntityName", "Shot"));
        // criteria.add(Restrictions.le("approvalDate", StringUtils.parseDate(dateString)));
        // criteria.addOrder(Order.asc("boundEntity"));
        // criteria.addOrder(Order.asc("approvalNoteType"));
        // criteria.addOrder(Order.desc("approvalDate"));// order the date from most to least recent
        // criteria.addOrder(Order.asc("status"));
        // List<Object[]> result = criteria.list();
        /*-
         * Query result:
         * result[0]=boundEntityId,
         * result[1]=approvalNoteTypeId,
         * result[2]=approvalNoteStatus,
         * result[3]=approvalNoteDate
         */
        return session.createSQLQuery(sql).list();
    }

    private Map<Long, Map<Long, HashBag<String>>> process(List<Object[]> result, Map<Long, Long> shotToTopSequenceMap)
    {
        Map<Long, Map<Long, HashBag<String>>> ret = new HashMap<Long, Map<Long, HashBag<String>>>();

        Object[] row;
        Long boundEntityId, approvalNoteTypeId, topSequenceId;
        String approvalNoteStatus;

        for (int i = 0; i < result.size(); i++)
        {
            row = result.get(i);
            if (row == null || row[0] == null || row[1] == null)
                continue;

            boundEntityId = Long.valueOf(((BigInteger) row[0]).longValue());
            approvalNoteTypeId = Long.valueOf(((BigInteger) row[1]).longValue());
            approvalNoteStatus = (String) row[2];
            // final Date approvalNoteDate = (Date) row[3];

            topSequenceId = shotToTopSequenceMap.get(boundEntityId);

            if (topSequenceId == null)
                continue;

            if (ret.get(topSequenceId) == null)
            {
                ret.put(topSequenceId, new HashMap<Long, HashBag<String>>());
            }
            if (ret.get(topSequenceId).get(approvalNoteTypeId) == null)
            {
                ret.get(topSequenceId).put(approvalNoteTypeId, new HashBag<String>());
            }
            ret.get(topSequenceId).get(approvalNoteTypeId).add(approvalNoteStatus);
        }

        return ret;
    }

    private MultiMap<String, Map<String, ?>> formatResponse(Map<Long, Map<Long, HashBag<String>>> response,
            Map<Long, Integer> topSequenceToShotsNbFramesMap, MultiMap<String, Long> topSequenceToShotsMap)
    {
        /*-
         * KEY=Top Sequence Id, 
         * VALUE=<KEY=approvalNoteTypeId OR "nbFrame" OR "nbWo" , 
         *        VALUE=<KEY=ApprovalNoteStatus,
         *               VALUE=number of approvalNotes with this status>>
         *               OR total number of frames
         *               OR total number of work objects
         * "nbFrame" = total number of frames of shots
         * "nbWo" = total number of shots 
         */
        MultiMap<String, Map<String, ?>> formattedRet = new MultiHashMap<String, Map<String, ?>>();
        Long topSequenceId;
        Map<String, Map<String, String>> approvalNoteTypeKeyMap;
        Map<String, String> statusNbMap;
        HashBag<String> bag;
        Map<String, Integer> nbFrameMap, nbWoMap;
        for (Entry<Long, Map<Long, HashBag<String>>> entry : response.entrySet())
        {
            topSequenceId = entry.getKey();

            for (Entry<Long, HashBag<String>> bagEntry : entry.getValue().entrySet())
            {
                approvalNoteTypeKeyMap = new HashMap<String, Map<String, String>>();
                statusNbMap = new HashMap<String, String>();
                bag = bagEntry.getValue();
                for (String status : bag)
                {
                    statusNbMap.put(status, String.valueOf(bag.getCount(status)));
                }
                approvalNoteTypeKeyMap.put(String.valueOf(bagEntry.getKey()), statusNbMap);

                formattedRet.put(String.valueOf(topSequenceId), approvalNoteTypeKeyMap);
            }
            /* nbFrame */
            nbFrameMap = new HashMap<String, Integer>();
            nbFrameMap.put("nbFrame", topSequenceToShotsNbFramesMap.get(topSequenceId));
            formattedRet.put(String.valueOf(topSequenceId), nbFrameMap);
            /* nbWo */
            nbWoMap = new HashMap<String, Integer>();
            nbWoMap.put("nbWo", topSequenceToShotsMap.get(String.valueOf(topSequenceId)).size());
            formattedRet.put(String.valueOf(topSequenceId), nbWoMap);
        }

        return formattedRet;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
