package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectPersonsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des personnes du projet\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'une personne\r\n" + "\r\nDELETE:N/A")
public class ProjectPersonsResource extends AbstractHd3dCollection<ILPerson, IPerson>
{
    public ProjectPersonsResource() throws Hd3dException
    {
        super(Persistors.person, Translators.person);
    }

    @Override
    protected ResultCollection<IPerson> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectPersonsCmd(getResourceContext()).wantTotalSize());
    }
}
