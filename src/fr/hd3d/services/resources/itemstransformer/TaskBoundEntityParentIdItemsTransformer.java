package fr.hd3d.services.resources.itemstransformer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.math.NumberUtils;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.Pair;
import fr.hd3d.utils.SheetUtils;
import fr.hd3d.utils.transformer.GetParentsIdTransformer;


public class TaskBoundEntityParentIdItemsTransformer extends AbstractItemsTransformerQuery
{
    public TaskBoundEntityParentIdItemsTransformer()
    {
        super();
    }

    public TaskBoundEntityParentIdItemsTransformer(List entities, IItem item, Object[][][] itemResult)
    {
        super(entities, item, itemResult);
    }

    @Override
    public String getInputObject()
    {
        return "List of " + IBase.class.getCanonicalName();
    }

    public void setName()
    {
        name = "Task Bound Entity Parent Id";
    }

    public void setDescription()
    {
        description = "returns the parent id of the entity bound to the task";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>() {
            {
                add(Const.ALL_CLASSES);
            }
        };
    }

    public void setReturnType()
    {
        returnType = fr.hd3d.common.client.Const.JAVA_LANG_LONG;
    }

    @Override
    public List doQuery() throws Hd3dItemFilterException
    {
        List<Long> ret;

        final Long itemId = NumberUtils.toLong(getItem().getTransformerParameters());

        if (BaseH.isNotValidId(itemId) || ArrayUtils.isEmpty(getItemResult()))
        {
            return sameSizeListOf(null);
        }

        try
        {
            Pair<IItem, Integer> tuple = SheetUtils.getColumnItemAndItemIndex(getItemResult(), itemId);
            if (tuple != null)
            {
                final int columnIndex = tuple.getField_2();
                List<IBase> workObjects = new ArrayList<IBase>(getItemResult().length);
                for (int i = 0; i < getItemResult().length; i++)
                {
                    workObjects.add((IBase) getItemResult()[i][columnIndex][0]);
                }
                ret = (List<Long>) org.apache.commons.collections15.CollectionUtils.collect(workObjects,
                        new GetParentsIdTransformer<IBase>());
            }
            else
            {
                ret = new ArrayList<Long>(0);
            }
        }
        catch (Hd3dPersistenceException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            return sameSizeListOf(null);
        }

        postCheck(ret);

        return ret;
    }

}
