package fr.hd3d.services.resources.itemstransformer;

import java.util.List;

import fr.hd3d.model.persistence.IItem;
import fr.hd3d.services.resources.collectionquery.AbstractCollectionQuery;


/**
 * AbstractItemsTransformerQuery is exactly like AbstractCollectionQuery, except that the List of Entities for the input
 * is not the query result, but a list of objects resulting from another item
 */
public abstract class AbstractItemsTransformerQuery extends AbstractCollectionQuery
{
    private Object[][][] itemResult;

    public AbstractItemsTransformerQuery()
    {
        super();
    }

    public AbstractItemsTransformerQuery(List entities, IItem item, Object[][][] itemResult)
    {
        super(entities, item);
        setItemResult(itemResult);
    }

    public Object[][][] getItemResult()
    {
        return itemResult;
    }

    public void setItemResult(Object[][][] itemResult)
    {
        this.itemResult = itemResult;
    }

    /**
     * For information purpose: type of object this ItemsTransformer processes
     * 
     * @return
     */
    public abstract String getInputObject();
}
