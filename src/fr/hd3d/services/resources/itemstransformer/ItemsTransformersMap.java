package fr.hd3d.services.resources.itemstransformer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


public class ItemsTransformersMap
{
    @SuppressWarnings("serial")
    public static Map<String, Class<? extends AbstractItemsTransformerQuery>> map =

    Collections.unmodifiableMap(new HashMap<String, Class<? extends AbstractItemsTransformerQuery>>() {
        {
            put(TaskBoundEntityParentIdItemsTransformer.class.getCanonicalName(),
                    TaskBoundEntityParentIdItemsTransformer.class);
        }
    });

    public static List<Map<String, String>> description = new ArrayList<Map<String, String>>();

    static
    {
        for (final Map.Entry<String, Class<? extends AbstractItemsTransformerQuery>> entry : map.entrySet())
        {
            Map<String, String> map = new HashMap<String, String>();

            AbstractItemsTransformerQuery x;
            try
            {
                x = entry.getValue().newInstance();
                map.put("classname", entry.getKey());
                map.put("name", x.getName());
                map.put("description", x.getDescription());
                map.put("input", x.getInputObject());
                description.add(map);
            }
            catch (InstantiationException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
            catch (IllegalAccessException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }

        }
    }

}
