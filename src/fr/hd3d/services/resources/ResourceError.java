package fr.hd3d.services.resources;

/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ResourceError
{
    private String code;
    private String message;

    public ResourceError()
    {}

    /**
     * @param code
     * @param message
     */
    public ResourceError(String code, String message)
    {
        this.code = code;
        this.message = message;
    }

    public final String getCode()
    {
        return code;
    }

    public final void setCode(String code)
    {
        this.code = code;
    }

    public final String getMessage()
    {
        return message;
    }

    public final void setMessage(String message)
    {
        this.message = message;
    }
}
