package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.COMPUTER_MODEL_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILComputerModel;
import fr.hd3d.model.persistence.IComputerModel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computers/{id}
 * 
 * @author HD3D
 */
public class ComputerModelResource extends AbstractHd3dResource<ILComputerModel, IComputerModel>
{
    @Override
    protected IComputerModel initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(COMPUTER_MODEL_ID_ATTRIBUTE, lockMode);
    }

    public ComputerModelResource() throws Hd3dException
    {
        super(Persistors.computerModel, Translators.computerModel);
    }
}
