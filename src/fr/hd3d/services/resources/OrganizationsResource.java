package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILOrganization;
import fr.hd3d.model.persistence.IOrganization;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer pool objects.<br>
 * URI : /licenses/
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération des fabricants\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:Création d'un fabricant\r\n"
        + "\r\nDELETE:N/A")
public class OrganizationsResource extends AbstractHd3dCollection<ILOrganization, IOrganization>
{
    public OrganizationsResource() throws Hd3dException
    {
        super(Persistors.organization, Translators.organization);
    }
}
