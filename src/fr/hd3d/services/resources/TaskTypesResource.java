/**
 * 
 */
package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTaskType;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author thomas-eskenazi
 * 
 */
@Hd3dComment("GET:Récupération des types de tâches\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Création d'un type de tâches\r\n" + "\r\nDELETE:N/A")
public class TaskTypesResource extends AbstractHd3dCollection<ILTaskType, ITaskType>
{
    public TaskTypesResource() throws Hd3dException
    {
        super(Persistors.tasktype, Translators.taskType);
    }
}
