package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetAssetRevisionUpstream;
import fr.hd3d.model.translator.Translators;


public class AssetRevisionUpstreamResource extends AbstractHd3dCollection<ILAssetRevision, IAssetRevision>
{
    public AssetRevisionUpstreamResource() throws Hd3dException
    {
        super(Persistors.assetrevision, Translators.assetrevision);
        allowMethods("GET");
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetAssetRevisionUpstream(getResourceContext()).wantTotalSize());
    }
}
