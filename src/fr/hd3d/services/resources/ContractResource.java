package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.CONTRACT_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILContract;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single contract object (id for parameter) <br>
 * URI : /contracts/{id}
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'un contrat\r\n" + "\r\nPUT:Modification d'un contrat\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Désactivation d'un contrat")
public class ContractResource extends AbstractHd3dResource<ILContract, IContract>
{
    @Override
    protected IContract initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(CONTRACT_ID_ATTRIBUTE, lockMode);
    }

    public ContractResource() throws Hd3dException
    {
        super(Persistors.contract, Translators.contract);
    }
}
