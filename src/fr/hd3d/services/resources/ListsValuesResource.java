package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILListValues;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Liste de toutes les LISTES de VALEURS des métadonnées.\r\n"
        + "\r\nPUT:modification d'une valeur \r\n" + "\r\nPOST:Création d'une valeur d'un type de métadonnée \r\n"
        + "\r\nDELETE:Effacement de la valeur ")
public class ListsValuesResource extends AbstractHd3dCollection<ILListValues, IListValues>
{
    public ListsValuesResource() throws Hd3dException
    {
        super(Persistors.listvalues, Translators.listvalues);
    }
}
