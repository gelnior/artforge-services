package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.utils.Const.ENTITYNAME_CATEGORY;
import static fr.hd3d.utils.Const.ENTITYNAME_COMPOSITION;
import static fr.hd3d.utils.Const.ENTITYNAME_CONSTITUENT;
import static fr.hd3d.utils.Const.ENTITYNAME_FILEREVISION;
import static fr.hd3d.utils.Const.ENTITYNAME_SEQUENCE;
import static fr.hd3d.utils.Const.ENTITYNAME_SHOT;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.StringValueTransformer;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.CategoryH;
import fr.hd3d.model.persistence.impl.hibernate.SequenceH;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


/**
 * Return all the available business entities
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:recherche des files correspondant aux critères passés en paramètre. "
        + "projs = project ids; tagged= tag ids; compos=compositions ids; seqs=séquences ids; shots= shots ids; cats=catégories ids; consts=constituents ids"
        + "On peut passer plusieurs valeurs pour chaque paramètre, séparées par des virgules\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class FilesSearchResource<L, P> extends AbstractHd3dBaseResource
{
    /* parameters */
    public final static String SEARCHPARAM_PROJ = "projs";
    public final static String SEARCHPARAM_TAG = "tagged";
    public final static String SEARCHPARAM_COMPO = "compos";
    public final static String SEARCHPARAM_SEQ = "seqs";
    public final static String SEARCHPARAM_SHOT = "shots";
    public final static String SEARCHPARAM_CAT = "cats";
    public final static String SEARCHPARAM_CONST = "consts";

    /* the following index fields are defined in entity classes @Field(name = "<index name>" annotation */
    public final static String INDEXFIELD_PARENTS = "parents";
    public final static String INDEXFIELD_BREAKDOWNOBJ_NAME = "breakdownobj_name";
    public final static String INDEXFIELD_BREAKDOWNOBJ_ID = "breakdownobj_id";
    public final static String INDEXFIELD_PROJECT_ID = "project_id";

    /* errors */
    public final static String ERR_0 = "FileSearch: error occured when retrieving Sequences: {}";
    public final static String ERR_1 = "please provide any search criteria...";

    public FilesSearchResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    private List<String> getAllChildren(String id, String entityName)
    {
        if (!ENTITYNAME_SEQUENCE.equals(entityName) && !ENTITYNAME_CATEGORY.equals(entityName))
            return null;

        List<String> children = new ArrayList<String>();
        children.add(id);// include the given id

        /* build the boolean query */
        /* find out all the sequences or categories for which the id is a parent, in the index */
        BooleanQuery bq = new BooleanQuery();
        TermQuery tq = getTermQuery(INDEXFIELD_PARENTS, id);
        bq.add(new BooleanClause(tq, BooleanClause.Occur.MUST));

        /* search */
        FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
        org.hibernate.Query hibQuery = null;

        if (ENTITYNAME_SEQUENCE.equals(entityName))
            hibQuery = fullTextSession.createFullTextQuery(bq, SequenceH.class);
        else if (ENTITYNAME_CATEGORY.equals(entityName))
            hibQuery = fullTextSession.createFullTextQuery(bq, CategoryH.class);

        if (hibQuery != null)
        {
            List<IBase> result = hibQuery.list();
            for (IBase object : result)
                children.addAll(getAllChildren(String.valueOf(object.getId()), entityName));
        }

        return children;
    }

    private List<IAssetRevisionGroup> filter(String entityName, String values)
    {
        if (StringUtils.isBlank(values) || StringUtils.isBlank(entityName))
            return Collections.emptyList();

        BooleanQuery bq = new BooleanQuery();

        if (!ENTITYNAME_SEQUENCE.equals(entityName) && !ENTITYNAME_CATEGORY.equals(entityName))
        {
            // entityName field
            {
                TermQuery tq = getTermQuery(INDEXFIELD_BREAKDOWNOBJ_NAME, entityName);
                bq.add(new BooleanClause(tq, BooleanClause.Occur.MUST));
            }
            // entityId field
            BooleanQuery idsQuery = new BooleanQuery();// collect all the ids
            String[] ids = StringUtils.split(values, ',');
            for (String id : ids)
            {
                TermQuery tq = getTermQuery(INDEXFIELD_BREAKDOWNOBJ_ID, id);
                idsQuery.add(new BooleanClause(tq, BooleanClause.Occur.SHOULD));
            }
            // AND
            bq.add(idsQuery, BooleanClause.Occur.MUST);
        }
        else
        {
            boolean nameTermAdded = false;
            BooleanQuery idsQuery = new BooleanQuery();// collect all the ids
            String[] ids = StringUtils.split(values, ',');
            for (String id : ids)
            {
                if (ENTITYNAME_SEQUENCE.equals(entityName) || ENTITYNAME_CATEGORY.equals(entityName))
                {
                    // find out all the children of sequence or category
                    List<String> allChildren = getAllChildren(id, entityName);
                    if (!allChildren.isEmpty() && !nameTermAdded) // only add once the entityName field
                    {
                        TermQuery tq = getTermQuery(INDEXFIELD_BREAKDOWNOBJ_NAME, entityName);
                        bq.add(new BooleanClause(tq, BooleanClause.Occur.SHOULD));
                        if (ENTITYNAME_CATEGORY.equals(entityName))
                        {
                            tq = getTermQuery(INDEXFIELD_BREAKDOWNOBJ_NAME, ENTITYNAME_CONSTITUENT);
                            bq.add(new BooleanClause(tq, BooleanClause.Occur.SHOULD));
                        }
                        else if (ENTITYNAME_SEQUENCE.equals(entityName))
                        {
                            tq = getTermQuery(INDEXFIELD_BREAKDOWNOBJ_NAME, ENTITYNAME_SHOT);
                            bq.add(new BooleanClause(tq, BooleanClause.Occur.SHOULD));
                        }
                        nameTermAdded = true;
                    }

                    for (String _id : allChildren)
                    {
                        // OR
                        TermQuery tq = getTermQuery(INDEXFIELD_BREAKDOWNOBJ_ID, _id);
                        idsQuery.add(new BooleanClause(tq, BooleanClause.Occur.SHOULD));
                    }

                    // for Category, find out all Constituents
                    if (ENTITYNAME_CATEGORY.equals(entityName))
                    {
                        List<Long> categoryIds = new ArrayList<Long>(allChildren.size());
                        for (String aCategoryId : allChildren)
                            categoryIds.add(NumberUtils.toLong(aCategoryId));

                        try
                        {
                            List<ICategory> categories = Persistors.category.getByIds(null, categoryIds);
                            for (ICategory cat : categories)
                            {
                                Set<IConstituent> consts = cat.getConstituents();
                                for (IConstituent aConst : consts)
                                {
                                    TermQuery tq = getTermQuery(INDEXFIELD_BREAKDOWNOBJ_ID, String.valueOf(aConst
                                            .getId()));
                                    idsQuery.add(new BooleanClause(tq, BooleanClause.Occur.SHOULD));
                                }
                            }
                        }
                        catch (Hd3dException e)
                        {
                            Log.LOGGER.error("FileSearch: error occured when retrieving Categories: {}", e);
                        }
                    }
                    // for Sequence, find out all Shots
                    if (ENTITYNAME_SEQUENCE.equals(entityName))
                    {
                        List<Long> sequenceIds = new ArrayList<Long>(allChildren.size());
                        for (String aSequenceId : allChildren)
                            sequenceIds.add(NumberUtils.toLong(aSequenceId));

                        try
                        {
                            List<ISequence> sequences = Persistors.sequence.getByIds(null, sequenceIds);
                            for (ISequence cat : sequences)
                            {
                                Set<IShot> shots = cat.getShots();
                                for (IShot aShot : shots)
                                {
                                    TermQuery tq = getTermQuery(INDEXFIELD_BREAKDOWNOBJ_ID, String.valueOf(aShot
                                            .getId()));
                                    idsQuery.add(new BooleanClause(tq, BooleanClause.Occur.SHOULD));
                                }
                            }
                        }
                        catch (Hd3dException e)
                        {
                            Log.LOGGER.error(ERR_0, e);
                        }
                    }
                    // AND
                    bq.add(idsQuery, BooleanClause.Occur.MUST);
                }
            }
        }

        // System.out.println(bq);

        FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
        org.hibernate.Transaction tx = fullTextSession.beginTransaction();

        org.hibernate.Query hibQuery = fullTextSession.createFullTextQuery(bq, AssetRevisionGroupH.class);

        // execute search
        List<IAssetRevisionGroup> result = hibQuery.list();

        tx.commit();
        tx = null;

        return result;
    }

    private TermQuery getTermQuery(String fieldName, String value)
    {
        return new TermQuery(new Term(fieldName, StringUtils.lowerCase(value)));
    }

    /**
     * given the list of Sets of T, returns the intersection of all the sets
     * 
     * @param sets
     * @return
     */
    private <T> Set<T> intersect(List<Set<T>> sets)
    {
        if (sets == null || sets.isEmpty())
            return null;

        int size = sets.size();

        if (sets.size() == 1)
            return sets.get(0);

        if (sets.size() == 2)
            return new HashSet(org.apache.commons.collections15.CollectionUtils.intersection(sets.get(0), sets.get(1)));

        return intersect(sets.subList(1, size));// end index is EXCLUSIVE
    }

    private Set<IFileRevision> fileRevisions(List<IAssetRevisionGroup> assetRevisionGroups)
    {
        Set<IFileRevision> files = new HashSet<IFileRevision>();
        for (IAssetRevisionGroup assetGroup : assetRevisionGroups)
            try
            {
                files.addAll(assetGroup.getFileRevisions());
            }
            catch (Hd3dException e)
            {
                // nothing to do
                continue;
            }
        return files;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Representation get(Variant variant)
    {
        try
        {
            // list of values with ',' separator
            String projs = getQuery().getValues(SEARCHPARAM_PROJ);
            String tags = getQuery().getValues(SEARCHPARAM_TAG);
            String compos = getQuery().getValues(SEARCHPARAM_COMPO);
            String seqs = getQuery().getValues(SEARCHPARAM_SEQ);
            String shots = getQuery().getValues(SEARCHPARAM_SHOT);
            String cats = getQuery().getValues(SEARCHPARAM_CAT);
            String consts = getQuery().getValues(SEARCHPARAM_CONST);

            if (StringUtils.isBlank(projs) && StringUtils.isBlank(tags) && StringUtils.isBlank(compos)
                    && StringUtils.isBlank(seqs) && StringUtils.isBlank(shots) && StringUtils.isBlank(cats)
                    && StringUtils.isBlank(consts))
                return new StringRepresentation(ERR_1);

            String[] tagsId = null;

            Set<IAssetRevisionGroup> A_projs = null;// found out AssetRevisionGroups after projects filtering
            Set<IFileRevision> A_projs_files = null;// files related to A_projs
            Set<IAssetRevisionGroup> A_compos = null;// found out AssetRevisionGroups after compositions filtering
            Set<IFileRevision> A_compos_files = null;// files related to A_compos
            Set<IAssetRevisionGroup> A_seqs = null;// found out AssetRevisionGroups after sequences filtering
            Set<IFileRevision> A_seqs_files = null;// files related to A_seqs
            Set<IAssetRevisionGroup> A_shots = null;// found out AssetRevisionGroups after shots filtering
            Set<IFileRevision> A_shots_files = null;// files related to found A_shots
            Set<IAssetRevisionGroup> A_cats = null;// found out AssetRevisionGroups after cats filtering
            Set<IFileRevision> A_cats_files = null;// files related to A_cats
            Set<IAssetRevisionGroup> A_consts = null;// found out AssetRevisionGroups after consts filtering
            Set<IFileRevision> A_consts_files = null;// files related to A_consts

            // filter by projects
            if (StringUtils.isNotBlank(projs))
            {
                A_projs = new HashSet<IAssetRevisionGroup>();
                String[] projsId = StringUtils.split(projs, ',');
                for (String projId : projsId)
                {
                    BooleanQuery bq = new BooleanQuery();
                    TermQuery tq = new TermQuery(new Term(INDEXFIELD_PROJECT_ID, projId));
                    bq.add(new BooleanClause(tq, BooleanClause.Occur.MUST));

                    FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
                    // org.hibernate.Transaction tx = fullTextSession.beginTransaction();

                    org.hibernate.Query hibQuery = fullTextSession.createFullTextQuery(bq, AssetRevisionGroupH.class);

                    // execute search
                    List<IAssetRevisionGroup> result = hibQuery.list();
                    if (CollectionUtils.isNotEmpty(result))
                    {
                        A_projs.addAll(result);
                        A_projs_files = fileRevisions(result);
                    }
                }
            }

            if (StringUtils.isNotBlank(compos))
            {
                A_compos = new HashSet<IAssetRevisionGroup>();
                List<IAssetRevisionGroup> assetRevisionGroups = filter(ENTITYNAME_COMPOSITION, compos);
                A_compos.addAll(assetRevisionGroups);
                A_compos_files = fileRevisions(assetRevisionGroups);
            }

            if (StringUtils.isNotBlank(seqs))
            {
                A_seqs = new HashSet<IAssetRevisionGroup>();
                List<IAssetRevisionGroup> assetRevisionGroups = filter(ENTITYNAME_SEQUENCE, seqs);
                A_seqs.addAll(assetRevisionGroups);
                A_seqs_files = fileRevisions(assetRevisionGroups);
            }

            if (StringUtils.isNotBlank(shots))
            {
                A_shots = new HashSet<IAssetRevisionGroup>();
                List<IAssetRevisionGroup> assetRevisionGroups = filter(ENTITYNAME_SHOT, shots);
                A_shots.addAll(assetRevisionGroups);
                A_shots_files = fileRevisions(assetRevisionGroups);
            }

            if (StringUtils.isNotBlank(cats))
            {
                A_cats = new HashSet<IAssetRevisionGroup>();
                List<IAssetRevisionGroup> assetRevisionGroups = filter(ENTITYNAME_CATEGORY, cats);
                A_cats.addAll(assetRevisionGroups);
                A_cats_files = fileRevisions(assetRevisionGroups);
            }

            if (StringUtils.isNotBlank(consts))
            {
                A_consts = new HashSet<IAssetRevisionGroup>();
                List<IAssetRevisionGroup> assetRevisionGroups = filter(ENTITYNAME_CONSTITUENT, consts);
                A_consts.addAll(assetRevisionGroups);
                A_consts_files = fileRevisions(assetRevisionGroups);
            }

            List<Set<IFileRevision>> A_sets_files = new ArrayList<Set<IFileRevision>>();
            if (A_projs_files != null)
                A_sets_files.add(A_projs_files);
            if (A_compos_files != null)
                A_sets_files.add(A_compos_files);
            if (A_seqs_files != null)
                A_sets_files.add(A_seqs_files);
            if (A_shots_files != null)
                A_sets_files.add(A_shots_files);
            if (A_cats_files != null)
                A_sets_files.add(A_cats_files);
            if (A_consts_files != null)
                A_sets_files.add(A_consts_files);

            Set<IFileRevision> A_result_files = intersect(A_sets_files);

            if (A_result_files == null || A_result_files.isEmpty())
                return toLightWeight(Collections.<IFileRevision> emptyList(), variant);

            // filter on tags (Note: tags are NOT indexed)
            List<IFileRevision> filelist = new ArrayList<IFileRevision>(A_result_files);
            if (StringUtils.isNotBlank(tags))
                tagsId = StringUtils.split(tags, ',');
            if (!ArrayUtils.isEmpty(tagsId))
                filelist = filterByTags(filelist, tagsId);

            // Convert result objects to lightweight objects.
            return toLightWeight(filelist, variant);
        }
        finally
        {
            cleanup();
        }
    }

    private List<IFileRevision> filterByTags(List<IFileRevision> files, String[] tagsId)
    {
        // if the results are not empty, apply extra filter by tags
        if (CollectionUtils.isNotEmpty(files))
        {
            List<IFileRevision> filteredList = new ArrayList<IFileRevision>(files.size());
            for (IFileRevision baseObject : files)
            {
                // collect all the tag ids of the files
                Collection<String> fileTagsId = CollectionUtils.collect(fr.hd3d.utils.CollectUtils.getIds(baseObject
                        .tags()), StringValueTransformer.getInstance());

                if (fileTagsId.containsAll(Arrays.asList(tagsId)))
                    filteredList.add(baseObject);
            }
            ((ArrayList<IFileRevision>) filteredList).trimToSize();
            return filteredList;
        }
        else
        {
            // otherwise, search only by tags
            List<IFileRevision> _files = new ArrayList<IFileRevision>(tagsId.length);
            Collection<Long> tagsIdColl = new ArrayList<Long>(tagsId.length);
            for (String id : tagsId)
                tagsIdColl.add(NumberUtils.toLong(id));

            try
            {
                List<ITag> _tags = Persistors.tag.getByIds(null, tagsIdColl);
                for (ITag tag : _tags)
                {
                    Collection<IFileRevision> col = tag.<IFileRevision> parentsByType(ENTITYNAME_FILEREVISION);
                    Collection<IFileRevision> colObjs = Persistors.filerevision.getByIds(null,
                            fr.hd3d.utils.CollectUtils.getIds(col));
                    _files.addAll(colObjs);
                }
                ((ArrayList) _files).trimToSize();
                return _files;
            }
            catch (Hd3dException e)
            {
                return _files;
            }
        }
    }

    private Representation toLightWeight(List<IFileRevision> files, Variant variant)
    {
        final List<ILFileRevision> lightweightList = new ArrayList<ILFileRevision>(files.size());
        for (IFileRevision baseObject : files)
            try
            {
                lightweightList.add(Translators.filerevision.toLightweight(baseObject, getResourceContext()));
            }
            catch (Hd3dTranslationException e)
            {
                error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
                return getResponseEntity();
            }
            catch (Hd3dException e)
            {
                error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
                return getResponseEntity();
            }

        Representation r = null;
        r = RessourceSerializer.getRepresentation(variant, lightweightList);
        r.setCharacterSet(CharacterSet.UTF_8);
        return r;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
