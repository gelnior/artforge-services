package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.common.client.ServicesURI.FILESYSTEM_ID_ATTRIBUTE;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.EmptyRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.Conf;


public class FileSystemResource<L, P> extends AbstractHd3dBaseResource
{
    Storage storage;

    public FileSystemResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("GET,POST");
        storage = new Storage();
    }

    /***
     * Writes a single file to disc from a multipart/form-data file upload, at the destination given in the last part of
     * the requested URL.
     */
    @Override
    protected Representation post(Representation entity, Variant variant) throws ResourceException
    {
        try
        {
            String destinationPath = "";

            // Where the destination path is parsed and validated
            try
            {
                // Get destination path from url
                destinationPath = getResourceContext().getStringFromUrl(FILESYSTEM_ID_ATTRIBUTE).replace(":", "/");

                // Is path in one of the authorized root paths? (.properties file)
                if (!storage.checkRootPathAuthorization(destinationPath))
                    throw new IOException("Specified destination path is forbidden.");
            }
            catch (NullPointerException e)
            {
                String description = "URL does not specify write destination.";
                error(Status.CLIENT_ERROR_BAD_REQUEST, new NullPointerException(description), description);
                return getResponseEntity();
            }
            catch (IOException e)
            {
                error(Status.CLIENT_ERROR_FORBIDDEN, e, ERROR_OCCURED);
                return getResponseEntity();
            }

            // Where the request is parsed and the file(s) saved.
            // Content-type:multipart/form-data
            if (entity.getMediaType().equals(MediaType.MULTIPART_FORM_DATA, true))
            {
                DiskFileItemFactory factory = new DiskFileItemFactory();
                RestletFileUpload upload = new RestletFileUpload(factory);

                try
                {
                    List<FileItem> items = upload.parseRequest(getRequest());
                    // No item in files
                    if (items.size() < 1)
                        throw new FileUploadException("No file was found for upload.");

                    // Exactly one item in files
                    if (items.size() == 1)
                    {
                        File file = null;
                        FileItem item = items.get(0);
                        if (item.getName() != null && item.getName() != "")
                            file = storage.saveSingleFile(item, destinationPath);
                        if (file == null)
                            throw new FileUploadException("Error occurred while saving file");
                        StringRepresentation rep = new StringRepresentation(file.getAbsolutePath());
                        rep.setLocationRef(destinationPath);
                        getResponse().setStatus(Status.SUCCESS_CREATED);

                        if (getQuery().getValuesArray("filerevisionid").length > 0)
                            updateFileRevisions(getQuery().getValuesArray("filerevisionid"));

                        return rep;
                    }
                    else if (items.size() > 1)
                    {
                        error(Status.SERVER_ERROR_NOT_IMPLEMENTED, null, "Multiple files upload not implemented.");
                        getResponse().setStatus(Status.SERVER_ERROR_NOT_IMPLEMENTED);
                        return getResponseEntity();
                    }
                }
                catch (FileUploadException e)
                {
                    error(Status.CLIENT_ERROR_BAD_REQUEST, e, ERROR_OCCURED);
                    return getResponseEntity();
                }
                catch (Storage.DirectoryNotCreatedException e)
                {
                    error(Status.CLIENT_ERROR_FORBIDDEN, e, ERROR_OCCURED);
                    return getResponseEntity();
                }
                catch (Storage.FileAlreadyExistsException e)
                {
                    error(Status.CLIENT_ERROR_CONFLICT, e, ERROR_OCCURED);
                    return getResponseEntity();
                }
                catch (Storage.FileNotWrittenException e)
                {
                    error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
                    return getResponseEntity();
                }
            }

            return new EmptyRepresentation();
        }
        finally
        {
            cleanup();
        }
    }

    private void updateFileRevisions(String[] values)
    {
        for (String value : values)
        {
            try
            {
                Long id = Long.parseLong(value);
                IFileRevision f = Persistors.filerevision.getById(id);
                f.setValidity("prod");
                f.doPreUpdate();
                Persistors.filerevision.merge(f);
                f.doPostUpdate();
            }
            catch (Hd3dException e)
            {
                continue;
            }
        }

    }

    @Override
    protected Representation get(Variant variant) throws ResourceException
    {
        String path = getResourceContext().getStringFromUrl(FILESYSTEM_ID_ATTRIBUTE).replace(":", "/");
        try
        {
            // Get destination path from url
            path = getResourceContext().getStringFromUrl(FILESYSTEM_ID_ATTRIBUTE).replace(":", "/");

            // Is path in one of the authorized root paths? (.properties file)
            if (!storage.checkRootPathAuthorization(path))
                throw new IOException("Specified destination path is forbidden.");

            // Checks whether file exists
            boolean fileOnDisk = storage.fileExists(path);
            String message = "";
            if (fileOnDisk)
            {
                message = path;
                getResponse().setStatus(Status.SUCCESS_OK);
            }
            else
            {
                message = "File not found";
                getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            }
            return new StringRepresentation(message);
        }
        catch (NullPointerException e)
        {
            String description = "URL does not specify write destination.";
            error(Status.CLIENT_ERROR_BAD_REQUEST, new NullPointerException(description), ERROR_OCCURED);
            return getResponseEntity();
        }
        catch (IOException e)
        {
            error(Status.CLIENT_ERROR_FORBIDDEN, e, ERROR_OCCURED);
            return getResponseEntity();
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        return null;
    }

    private static class Storage
    {
        private List<String> allowedRootPaths;

        public Storage()
        {
            String propString = Conf.getAllowedRootPaths();
            allowedRootPaths = Arrays.asList(propString.split(":"));
        }

        public File saveSingleFile(FileItem fileItem, String destinationPath) throws FileAlreadyExistsException,
                DirectoryNotCreatedException, FileNotWrittenException
        {
            File savedFile = new File(destinationPath);

            if (savedFile.exists())
                throw new FileAlreadyExistsException("File already exists.");

            createParentDirectory(savedFile);

            try
            {
                fileItem.write(savedFile);
                return savedFile;
            }
            catch (Exception e)
            {
                throw new FileNotWrittenException("The file could not be written for some reason. Check stack trace.",
                        e);
            }
        }

        public boolean fileExists(String path)
        {
            File f = new File(path);

            return f.exists() && f.isFile();
        }

        public boolean checkRootPathAuthorization(String path)
        {
            boolean checked = false;

            for (String root : allowedRootPaths)
            {
                if (path.startsWith(root))
                {
                    checked = true;
                    break;
                }
            }

            return checked;
        }

        public void createParentDirectory(File file) throws DirectoryNotCreatedException
        {
            if (!file.getParentFile().exists())
            {
                boolean ok = file.getParentFile().mkdirs();
                if (!ok)
                    throw new DirectoryNotCreatedException("Could not create destination directory.");
            }
        }

        @SuppressWarnings("serial")
        private static class DirectoryNotCreatedException extends Exception
        {
            public DirectoryNotCreatedException(String message)
            {
                super(message);
            }
        }

        @SuppressWarnings("serial")
        private static class FileAlreadyExistsException extends Exception
        {
            public FileAlreadyExistsException(String message)
            {
                super(message);
            }
        }

        @SuppressWarnings("serial")
        private static class FileNotWrittenException extends Exception
        {
            public FileNotWrittenException(String message, Exception cause)
            {
                super(message, cause);
            }
        }
    }
}
