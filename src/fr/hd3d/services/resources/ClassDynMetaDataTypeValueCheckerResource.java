package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.CLASSDYNMETADATATYPE_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.LockMode;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILClassDynMetaDataType;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataValuesToCreate;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.query.Queries;
import fr.hd3d.model.persistence.impl.hibernate.visitor.DynMetaDataValueHandler;
import fr.hd3d.model.translator.Translators;


public class ClassDynMetaDataTypeValueCheckerResource extends
        AbstractHd3dResource<ILClassDynMetaDataType, IClassDynMetaDataType>
{
    public ClassDynMetaDataTypeValueCheckerResource() throws Hd3dException
    {
        super(Persistors.classdynmetadatatype, Translators.classdynmetadata);
    }

    @Override
    protected IClassDynMetaDataType initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(CLASSDYNMETADATATYPE_ID_ATTRIBUTE, lockMode);
    }

    public Representation get(Variant variant)
    {
        String html = "<html>";
        Collection<IBase> objects = DynMetaDataValueHandler.getMatchingEntityInstances(this.object);
        String[] properties = new String[] { "parentType", "parent", "classDynMetaDataType.id" };
        Object[] values = new Object[] { this.object.getClassName(), NumberUtils.LONG_ZERO, this.object.getId() };

        List<DynMetaDataValuesToCreate> toCreates = new ArrayList<DynMetaDataValuesToCreate>();

        for (IBase obj : objects)
        {
            Boolean exist = Boolean.FALSE;

            // check if dynMetaDataValue Exist
            values[1] = obj.getId();
            try
            {
                List<IDynMetaDataValue> value = Persistors.dynmetadatavalue
                        .getByValues(null, properties, values, "AND");
                if (value == null || value.size() == 0)
                {
                    // check if dynMetaDataValueToCreate exist
                    List<DynMetaDataValuesToCreate> dynMetaDataValuesToCreate = Queries.getDynMetaDataValuesToCreate(
                            this.object.getClassName(), obj.getId(), this.object.getId());
                    if (dynMetaDataValuesToCreate == null || dynMetaDataValuesToCreate.size() == 0)
                    {
                        toCreates.add(new DynMetaDataValuesToCreate(object, obj.getId(), this.object.getClassName()));
                        html += "\n<br/> - Missing value for " + this.object.getClassName() + " " + obj.getId();
                    }
                }
                else
                {
                    exist = Boolean.TRUE;
                }
            }
            catch (Hd3dException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        try
        {
            HibernatePersist.persistRawCollection(toCreates);
        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
        }
        html += "</html>";
        StringRepresentation rep = new StringRepresentation(html);
        return rep;
    }
}
