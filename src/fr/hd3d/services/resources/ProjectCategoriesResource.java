package fr.hd3d.services.resources;

import java.util.ArrayList;
import java.util.List;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectCategoriesCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.translator.json.JSonFormSerializer;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des catégories principales (= situées directement sous le racine) dans la bibliothèque des constituants (triées par ordre alphabétique des labels)\r\n"
        + "\r\nPUT:Mise à jour d'une LISTE de catégories (ou sous-catégories)\r\n"
        + "\r\nPOST:Ajout d'une CATEGORIE PRINCIPALE par le nom en ajoutant à l'url: ?mode=bulk, Ajout d'une LISTE de catégories\r\n"
        + "\r\nDELETE:N/A")
public class ProjectCategoriesResource extends AbstractHd3dCollection<ILCategory, ICategory>
{
    public ProjectCategoriesResource() throws Hd3dException
    {
        super(Persistors.category, Translators.category);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectCategoriesCmd(getResourceContext()).wantTotalSize());
    }

    @Override
    protected ICategory newObject(Representation entity) throws Hd3dException
    {
        ILCategory lcategory = JSonFormSerializer.<ILCategory> deserialize(entity);
        ICategory category = getResourceContext().getTranslator().fromLightweight(lcategory, getResourceContext());
        if (category.getParent() == null)
            category.setParent(category.getProject().rootCategory());

        return category;
    }

    @Override
    protected List<ICategory> newCollection(Representation entity) throws Hd3dException
    {
        List<ILCategory> lcategories = JSonFormSerializer.<ILCategory> deserializeList(entity);
        List<ICategory> objects = new ArrayList<ICategory>(lcategories.size());
        for (ILCategory c : lcategories)
        {
            objects.add(createCategory(c));
        }

        return objects;
    }

    private ICategory createCategory(ILCategory lcategory) throws Hd3dException
    {
        ICategory category = getResourceContext().getTranslator().fromLightweight(lcategory, getResourceContext());
        if (category.getParent() == null)
        {
            category.setParent(category.getProject().rootCategory());
        }
        return category;
    }

    // @Override
    // protected List<ICategory> doPrePersistCollection(Representation entity, Map<String, String> map)
    // throws Hd3dTranslationException
    // {
    // List<ILCategory> lcategories = JSonFormSerializer.<ILCategory> deserializeList(entity);
    // List<ICategory> objects = new ArrayList<ICategory>(lcategories.size());
    // for (ILCategory c : lcategories)
    // {
    // ICategory category = translator.fromLightweight(c, map);
    // if (category.getParent() == null)
    // category.setParent(category.getProject().rootCategory());
    // objects.add(category);
    //
    // // PrePersist
    // category.doPrePersist();
    // executeScripts(category, IPersist.PREPERSIST, getNewIdentifier(category));
    // }
    //
    // return objects;
    // }

    // @Override
    // protected ICategory doPrePersist(Representation entity, Map<String, String> map) throws Hd3dTranslationException
    // {
    // ILCategory lcategory = JSonFormSerializer.<ILCategory> deserialize(entity);
    // ICategory category = translator.fromLightweight(lcategory, map);
    // if (category.getParent() == null)
    // category.setParent(category.getProject().rootCategory());
    //
    // // PrePersist
    // category.doPrePersist();
    // executeScripts(category, IPersist.PREPERSIST, getNewIdentifier(category));
    // return category;
    // }
}
