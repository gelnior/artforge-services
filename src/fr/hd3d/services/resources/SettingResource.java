package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.SETTING_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.impl.LSetting;
import fr.hd3d.model.persistence.impl.hibernate.query.AbstractHibernateQuery;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.model.preferences.Setting;
import fr.hd3d.model.translator.impl.SettingTranslator;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.services.translator.json.JSonFormSerializer;
import fr.hd3d.utils.HibernateUtil;


@Hd3dComment("GET:Récupération du setting identifié par l'id. Si \"self\" est fourni à la place d'un id, ce sont les settings de l'utilisateur courant qui sont retournés.\r\n"
        + "\r\nPUT:Mise a jour du setting\r\n" + "\r\nPOST:N/A\r\n" + "\r\nDELETE:Effacement du settingN/A")
public class SettingResource extends AbstractHd3dSingleResource<LSetting, Setting>
{
    public SettingResource() throws Hd3dException
    {
        super(Setting.class, new SettingTranslator());
        allowMethods("PUT,DELETE");
    }

    protected Setting initializeObject() throws Hd3dException
    {
        Long id = getResourceContext().getIdFromUrl(SETTING_ID_ATTRIBUTE);

        final GetObject<Setting> cmd = new GetObject<Setting>(id);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    protected Setting initializeObject(LockMode lockMode) throws Hd3dException
    {
        Long id = getResourceContext().getIdFromUrl(SETTING_ID_ATTRIBUTE);

        final GetObject<Setting> cmd = new GetObject<Setting>(getResourceContext(), id);
        HibernateUtil.safeQuery(cmd);
        return cmd.result;
    }

    class GetObject<A> extends AbstractHibernateQuery<Setting>
    {
        A result;
        private final Long id;

        public GetObject(Long id)
        {
            this.id = id;
        }

        public GetObject(Long id, LockMode lockMode)
        {
            super(lockMode);
            this.id = id;
        }

        public GetObject(final ResourceContext<?, ?> context, Long id)
        {
            super(context);
            this.id = id;
        }

        public GetObject(final ResourceContext<?, ?> context, Long id, LockMode lockMode)
        {
            super(context, lockMode);
            this.id = id;
        }

        @SuppressWarnings("unchecked")
        public void execute(org.hibernate.Session session) throws Hd3dException
        {
            try
            {
                // final Criteria query = basicIdCriteria(session, id);
                // setLockMode(query);
                result = (A) getCriteria(session).uniqueResult();
            }
            catch (HibernateException e)
            {
                A obj = (A) session.get(Setting.class, id);
                if (obj != null)
                {
                    result = obj;
                }
            }
        }

        @Override
        protected Criteria buildCriteriaOrQuery(Session session, boolean forCount) throws Hd3dException
        {
            final Criteria query = basicIdCriteria(session, id);
            return query;
        }
    }

    public Criteria basicIdCriteria(Session session, Long id)
    {
        return basicIdCriteria(session, id, Setting.class);
    }

    public Criteria basicIdCriteria(Session session, Long id, Class<?> clazz)
    {
        Criteria criteria = session.createCriteria(clazz);
        // session.enableFilter("internalStatus").setParameter("internalStatus", Const.INTERNALSTATUS_ENABLED);
        criteria.add(Restrictions.idEq(id));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria;
    }

    protected Representation getLightWeightRepresentation(Variant variant, LSetting lightweight)
            throws Hd3dTranslationException
    {
        return RessourceSerializer.getRepresentation(variant, lightweight);
    }

    protected boolean preCheck(Setting object)
    {
        if (object == null)
            return false;
        return true;
    }

    protected void doPreDelete() throws Hd3dPersistenceException
    {
    /* nothing to do */
    }

    protected void doPostDelete() throws Hd3dPersistenceException
    {
    /* nothing to do */
    }

    protected void doDelete() throws Hd3dPersistenceException
    {
        HibernateUtil.safeTransaction(new IHibernateTransaction() {

            public void execute(org.hibernate.Session session) throws Hd3dException
            {
                session.delete(object);
                // session.flush();
            }
        });
    }

    protected void auditDelete(Setting object)
    {
    /* no audit for Setting */
    }

    protected void doPreUpdate(Representation entity, LSetting lObject) throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    protected void doUpdate(LSetting lObject) throws Hd3dTranslationException
    {
        SettingTranslator translator = new SettingTranslator();
        translator.updateFromLightweight(lObject, object, getResourceContext());
        HibernateUtil.safeTransaction(new IHibernateTransaction() {
            public void execute(org.hibernate.Session session) throws Hd3dException
            {
                session.merge(object);
            }
        });
    }

    protected void doPostUpdate(Representation entity) throws Hd3dTranslationException
    {
        sucessfulPut(getLightweight(object), entity.getMediaType());
    }

    protected void auditPost(Setting object)
    {
    /* nothing to do */
    }

    protected void auditPost(List<Setting> objects)
    {
    /* nothing to do */
    }

    protected void doPersist(Setting object) throws Hd3dPersistenceException
    {
    /* nothing to do */
    }

    protected void doPostPersist(Setting object) throws Hd3dPersistenceException
    {
    /* nothing to do */
    }

    protected void doPersist(List<Setting> objects) throws Hd3dPersistenceException
    {
    /* nothing to do */
    }

    protected void doPostPersist(List<Setting> objects) throws Hd3dPersistenceException
    {
    /* nothing to do */
    }

    protected LSetting getLightweight(Setting object) throws Hd3dTranslationException
    {
        SettingTranslator translator = new SettingTranslator();
        return translator.toLightweight(object, getResourceContext());
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        final Setting setting = (Setting) object;
        StringBuilder buf = new StringBuilder(getRequest().getResourceRef().getBaseRef().toString());
        buf.append('/').append(setting == null ? "null" : setting.getId());
        return buf.toString();
    }

    @Override
    protected void doPrePersist(Representation entity, Setting object) throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    @Override
    protected void doPrePersistCollection(Representation entity, List<Setting> newObjects)
            throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    @Override
    protected Setting newObject(Representation entity) throws Hd3dException
    {
        LSetting lObject = JSonFormSerializer.<LSetting> deserialize(entity);
        if (lObject == null)
            return null;
        Setting object = getResourceContext().getTranslator().fromLightweight(lObject, getResourceContext());
        lObject = null;
        return object;
    }

    @Override
    protected List<Setting> newCollection(Representation entity) throws Hd3dException
    {
        List<LSetting> lObjects = JSonFormSerializer.<LSetting> deserializeList(entity);
        if (lObjects == null)
            return new ArrayList<Setting>();

        List<Setting> objects = new ArrayList<Setting>(lObjects.size());
        for (LSetting light : lObjects)
            CollectionUtils.addIgnoreNull(objects, getResourceContext().getTranslator().fromLightweight(light,
                    getResourceContext()));

        /* clear */
        for (LSetting light : lObjects)
            light.clear();
        lObjects.clear();
        lObjects = null;

        return objects;
    }

    @Override
    protected void doPostRead(LSetting object)
    {
    /* nothing to do */
    }
}
