package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILComputerType;
import fr.hd3d.model.persistence.IComputerType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer pool objects.<br>
 * URI : /pools/
 * 
 * @author HD3D
 */
public class ComputerTypesResource extends AbstractHd3dCollection<ILComputerType, IComputerType>
{
    public ComputerTypesResource() throws Hd3dException
    {
        super(Persistors.computerType, Translators.computerType);
    }
}
