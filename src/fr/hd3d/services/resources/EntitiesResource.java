package fr.hd3d.services.resources;

import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.resources.serializer.RessourceSerializer;


/**
 * Return all the available business entities
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:retourne la liste des interfaces des classes métiers, sans les attributs\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class EntitiesResource<L, P> extends AbstractHd3dBaseResource
{
    public EntitiesResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    public Representation get(Variant variant)
    {
        try
        {
            // build the representation
            Representation r = null;
            r = RessourceSerializer.getRepresentation(variant, EntitiesMaps.allEntityNames());
            r.setCharacterSet(CharacterSet.UTF_8);
            return r;
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
