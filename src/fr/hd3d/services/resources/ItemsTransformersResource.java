package fr.hd3d.services.resources;

import java.util.List;
import java.util.Map;

import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.services.resources.itemstransformer.ItemsTransformersMap;
import fr.hd3d.services.resources.serializer.RessourceSerializer;


/**
 * Return all the available business entities
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:retourne la liste des classes ItemTransformer permettant de faire une requête pour toutes les lignes des sheets en prenant comme entrée la liste des résultats venant d'un autre Item\r\n"
        + "\r\nPUT:N/A\r\n" + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class ItemsTransformersResource<L, P> extends AbstractHd3dBaseResource
{
    public ItemsTransformersResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    public Representation get(Variant variant)
    {
        List<Map<String, String>> description = null;
        try
        {
            // build the representation
            Representation r = null;
            r = RessourceSerializer.getRepresentation(variant, ItemsTransformersMap.map);
            r.setCharacterSet(CharacterSet.UTF_8);
            return r;
        }
        finally
        {
            if (description != null)
                description.clear();
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
