package fr.hd3d.services.resources;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.collections15.MultiMap;
import org.apache.commons.collections15.bag.HashBag;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.hibernate.Session;
import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.ServerResourceUtils;


public class ApprovalNoteByCategoryIdsByDateResource extends AbstractHd3dBaseResource
{

    public ApprovalNoteByCategoryIdsByDateResource() throws Hd3dException
    {
        super();
        allowMethods(null);
        getResourceContext().complete();
    }

    // @SuppressWarnings("unchecked")
    // @Override
    // public Representation get(Variant variant)
    // {
    // Session session = HibernateUtil.currentSession();
    // org.hibernate.Transaction tx = session.beginTransaction();
    // try
    // {
    // /* info from url */
    // // final String projectIdString = (String) getResourceContext().getQueryMap().get(Const.PROJECTID);
    // final String dateString = (String) getResourceContext().getQueryMap().get(Const.DATE);
    // final String categoryIdsString = (String) getResourceContext().getQueryMap().get(Const.IDS);
    //
    // /* convert in proper types */
    // // final Long projectId = NumberUtils.toLong(projectIdString);
    // List<String> categoryIdsStringList = Arrays.asList(org.apache.commons.lang.StringUtils.split(
    // categoryIdsString, ','));
    // final List<Long> categoryIds = CollectUtils.toLong(categoryIdsStringList);
    //
    // MultiMap<String, Long> mapCategory = new MultiHashMap<String, Long>();
    // Map<String, IApprovalNote> constituentsApprovalNoteType = new HashMap<String, IApprovalNote>();
    //
    // Set<IConstituent> allConstituents = new HashSet<IConstituent>();
    //
    // /* query */
    //
    // /* retrieve all categories and sub-categories */
    // // Set<ICategory> allCategorys = new HashSet<ICategory>();
    // List<ICategory> categories = Persistors.category.getByIds(getResourceContext(), categoryIds);
    // for (ICategory category : categories)
    // {
    // Set<IConstituent> allConstituentsInCategory = new HashSet<IConstituent>();
    // for (ICategory subCategory : category.allChildren(true))
    // {
    // allConstituents.addAll(subCategory.getConstituents());
    // allConstituentsInCategory.addAll(subCategory.getConstituents());
    // }
    // for (IConstituent constituent : allConstituentsInCategory)
    // {
    // mapCategory.put(String.valueOf(category.getId()), constituent.getId());
    // }
    //
    // }
    //
    // /* retrieve shots of the above Categorys */
    // // Set<IShot> allShots = new HashSet<IShot>();
    // // for (ICategory Category : allCategorys)
    // // {
    // // allShots.addAll(Category.getShots());
    // // }
    // if (CollectUtils.isNotEmpty(allConstituents))
    // {
    // final Criteria criteria = ((HibernatePersist) Persistors.approvalnote).basicCriteria(session,
    // ApprovalNoteH.class);
    // criteria.add(Restrictions.in("boundEntity", CollectUtils.getIds(allConstituents)));
    // criteria.add(Restrictions.eq("boundEntityName", "Constituent"));
    // criteria.add(Restrictions.le("approvalDate", StringUtils.parseDate(dateString)));
    //
    // List<IApprovalNote> result = criteria.list();
    //
    // // Without Historic
    // for (IApprovalNote object : result)
    // {
    // if (object == null || object.getBoundEntity() == null || object.getApprovalNoteType() == null)
    // continue;
    //
    // if (constituentsApprovalNoteType.get(object.getBoundEntity()
    // + object.getApprovalNoteType().toString()) != null)
    // {
    // if (constituentsApprovalNoteType.get(
    // object.getBoundEntity() + object.getApprovalNoteType().toString()).getApprovalDate()
    // .before(object.getApprovalDate()))
    // {
    // constituentsApprovalNoteType.put(object.getBoundEntity()
    // + object.getApprovalNoteType().toString(), object);
    // }
    // }
    // else
    // {
    // constituentsApprovalNoteType.put(object.getBoundEntity()
    // + object.getApprovalNoteType().toString(), object);
    // }
    // }
    //
    // List<IApprovalNote> resultFiltered = new ArrayList<IApprovalNote>();
    // for (String key : constituentsApprovalNoteType.keySet())
    // {
    // resultFiltered.add(constituentsApprovalNoteType.get(key));
    // }
    //
    // /* properly format response */
    // MultiMap<String, ILApprovalNote> resp = new MultiHashMap<String, ILApprovalNote>(result.size());
    // for (IApprovalNote object : resultFiltered)
    // {
    // for (ICategory category : categories)
    // {
    // if (mapCategory.get(category.getId().toString()) != null
    // && mapCategory.get(category.getId().toString()).contains(object.getBoundEntity()))
    // {
    // resp.put(String.valueOf(category.getId()), Translators.approvalnote.toLightweight(object,
    // getResourceContext()));
    // }
    // }
    // }
    //
    // Representation r = RessourceSerializer.getRepresentation(variant, resp.map());
    // r.setCharacterSet(CharacterSet.UTF_8);
    // return ServerResourceUtils.encodeRepresentation(r);
    // }
    // else
    // {
    // Representation r = RessourceSerializer.getRepresentation(variant, null);
    // r.setCharacterSet(CharacterSet.UTF_8);
    // return ServerResourceUtils.encodeRepresentation(r);
    // }
    // }
    // catch (Hd3dException e)
    // {
    // Log.LOGGER.error(ExceptUtils.format(e));
    // Representation r = new StringRepresentation(ExceptUtils.format(e));
    // r.setCharacterSet(CharacterSet.UTF_8);
    // return ServerResourceUtils.encodeRepresentation(r);
    // }
    // finally
    // {
    // if (tx != null)
    // tx = null;
    // cleanup();
    // }
    //
    // }

    @Override
    public Representation get(Variant variant)
    {
        Session session = HibernateUtil.currentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        try
        {
            /* info from url */
            final String dateString = (String) getResourceContext().getQueryMap().get(Const.DATE);
            final String categoryIdsString = (String) getResourceContext().getQueryMap().get(Const.IDS);

            /* convert in proper types */
            final List<Long> categoryIdsStringList = CollectUtils.toLong(Arrays
                    .asList(org.apache.commons.lang.StringUtils.split(categoryIdsString, ',')));

            /* KEY=top Category Id, VALUE=top Category's constituents and children constituents */
            MultiMap<String, Long> topCategoryToConstituentsMap = new MultiHashMap<String, Long>();

            /* KEY=Top Category's constituents id and all children's constituents id, VALUE=Top Category id */
            Map<Long, Long> constituentsToTopCategoryMap = new HashMap<Long, Long>();

            Set<IConstituent> allConstituents = new HashSet<IConstituent>();

            populate(categoryIdsStringList, topCategoryToConstituentsMap, constituentsToTopCategoryMap, allConstituents);

            if (CollectUtils.isNotEmpty(allConstituents))
            {
                List<Object[]> result = query(dateString, CollectUtils.getIds(allConstituents), session);

                /* KEY=Top Category Id, VALUE=<KEY=approvalNoteTypeId, VALUE=BAG of approvalNoteStatus> */
                Map<Long, Map<Long, HashBag<String>>> ret = process(result, constituentsToTopCategoryMap);

                /*-
                 * KEY=Top Category Id,
                 * VALUE=<KEY=approvalNoteTypeId OR "nbFrame" OR "nbWo" ,
                 * VALUE=<KEY=ApprovalNoteStatus,
                 * VALUE=number of approvalNotes with this status>>
                 * OR total number of frames
                 * OR total number of work objects
                 * "nbWo" = total number of constituents
                 */
                MultiMap<String, Map<String, ?>> formattedRet = formatResponse(ret, topCategoryToConstituentsMap);

                Representation r = RessourceSerializer.getRepresentation(variant, formattedRet.map());
                r.setCharacterSet(CharacterSet.UTF_8);
                return ServerResourceUtils.encodeRepresentation(r);
            }
            else
            {
                Representation r = RessourceSerializer.getRepresentation(variant, null);
                r.setCharacterSet(CharacterSet.UTF_8);
                return ServerResourceUtils.encodeRepresentation(r);
            }
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            Representation r = new StringRepresentation(ExceptUtils.format(e));
            r.setCharacterSet(CharacterSet.UTF_8);
            return ServerResourceUtils.encodeRepresentation(r);
        }
        finally
        {
            if (tx != null)
                tx = null;
            cleanup();
        }
    }

    private void populate(List<Long> categoryIds, MultiMap<String, Long> topCategoryToConstituentsMap,
            Map<Long, Long> constituentsToTopCategoryMap, Set<IConstituent> allConstituents) throws Hd3dException
    {
        /* retrieve all Categories and sub-categories */
        List<ICategory> topCategories = Persistors.category.getByIds(getResourceContext(), categoryIds);
        for (ICategory topCategory : topCategories)
        {
            Set<IConstituent> allConstituentsInCategory = new HashSet<IConstituent>();
            for (ICategory subCategory : topCategory.allChildren(true))
            {
                allConstituents.addAll(subCategory.getConstituents());
                allConstituentsInCategory.addAll(subCategory.getConstituents());
            }

            topCategoryToConstituentsMap.putAll(String.valueOf(topCategory.getId()), CollectUtils
                    .getIds(allConstituentsInCategory));

            for (IConstituent constituent : allConstituentsInCategory)
            {
                constituentsToTopCategoryMap.put(constituent.getId(), topCategory.getId());
            }
        }
    }

    @SuppressWarnings("unchecked")
    private List<Object[]> query(String dateString, List<Long> constituentIds, Session session)
    {
        final String sql = "SELECT * FROM   ("
                + " select "
                + " this_.approvalnote_boundentity as y0_, this_.approvalnote_approvalnotetype as y1_, this_.approvalnote_status as y2_, this_.approvalnote_approvaldate as y3_ "
                + " from hd3d_approvalnote this_ where 0 = this_.internalStatus and this_.approvalnote_boundentity in ("
                + org.apache.commons.lang.StringUtils.join(CollectUtils.toString(constituentIds), ',')
                + ") and this_.approvalnote_boundentityname='Constituent' and this_.approvalnote_approvaldate<='"
                + dateString
                + "' "
                + " order by this_.approvalnote_boundentity asc, this_.approvalnote_approvalnotetype asc, this_.approvalnote_approvaldate desc, this_.approvalnote_status asc "
                + " ) as s" + " group by y0_,y1_";

        /*-
         * Query result:
         * result[0]=boundEntityId,
         * result[1]=approvalNoteTypeId,
         * result[2]=approvalNoteStatus,
         * result[3]=approvalNoteDate
         */
        return session.createSQLQuery(sql).list();
    }

    private Map<Long, Map<Long, HashBag<String>>> process(List<Object[]> result, Map<Long, Long> shotToTopSequenceMap)
    {
        Map<Long, Map<Long, HashBag<String>>> ret = new HashMap<Long, Map<Long, HashBag<String>>>();

        Object[] row;
        Long boundEntityId, approvalNoteTypeId, topSequenceId;
        String approvalNoteStatus;

        for (int i = 0; i < result.size(); i++)
        {
            row = result.get(i);
            if (row == null || row[0] == null || row[1] == null)
                continue;

            boundEntityId = Long.valueOf(((BigInteger) row[0]).longValue());
            approvalNoteTypeId = Long.valueOf(((BigInteger) row[1]).longValue());
            approvalNoteStatus = (String) row[2];
            // final Date approvalNoteDate = (Date) row[3];

            topSequenceId = shotToTopSequenceMap.get(boundEntityId);

            if (topSequenceId == null)
                continue;

            if (ret.get(topSequenceId) == null)
            {
                ret.put(topSequenceId, new HashMap<Long, HashBag<String>>());
            }
            if (ret.get(topSequenceId).get(approvalNoteTypeId) == null)
            {
                ret.get(topSequenceId).put(approvalNoteTypeId, new HashBag<String>());
            }
            ret.get(topSequenceId).get(approvalNoteTypeId).add(approvalNoteStatus);
        }

        return ret;
    }

    private MultiMap<String, Map<String, ?>> formatResponse(Map<Long, Map<Long, HashBag<String>>> response,
            MultiMap<String, Long> topCategoryToConstituentsMap)
    {
        /*-
         * KEY=Top Sequence Id, 
         * VALUE=<KEY=approvalNoteTypeId OR "nbFrame" OR "nbWo" , 
         *        VALUE=<KEY=ApprovalNoteStatus,
         *               VALUE=number of approvalNotes with this status>>
         *               OR total number of frames
         *               OR total number of work objects
         * "nbFrame" = total number of frames of shots
         * "nbWo" = total number of shots 
         */
        MultiMap<String, Map<String, ?>> formattedRet = new MultiHashMap<String, Map<String, ?>>();
        Long topCategoryId;
        Map<String, Map<String, String>> approvalNoteTypeKeyMap;
        Map<String, String> statusNbMap;
        HashBag<String> bag;
        Map<String, Integer> nbWoMap;
        for (Entry<Long, Map<Long, HashBag<String>>> entry : response.entrySet())
        {
            topCategoryId = entry.getKey();

            for (Entry<Long, HashBag<String>> bagEntry : entry.getValue().entrySet())
            {
                approvalNoteTypeKeyMap = new HashMap<String, Map<String, String>>();
                statusNbMap = new HashMap<String, String>();
                bag = bagEntry.getValue();
                for (String status : bag)
                {
                    statusNbMap.put(status, String.valueOf(bag.getCount(status)));
                }
                approvalNoteTypeKeyMap.put(String.valueOf(bagEntry.getKey()), statusNbMap);

                formattedRet.put(String.valueOf(topCategoryId), approvalNoteTypeKeyMap);
            }

            /* nbWo */
            nbWoMap = new HashMap<String, Integer>();
            nbWoMap.put("nbWo", topCategoryToConstituentsMap.get(String.valueOf(topCategoryId)).size());
            formattedRet.put(String.valueOf(topCategoryId), nbWoMap);
        }

        return formattedRet;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
