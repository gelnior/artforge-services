package fr.hd3d.services.resources;

import java.io.File;

import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.Log;


public class ImageResource extends AbstractHd3dBaseResource
{

    public ImageResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("GET");
    }

    @Override
    public Representation get(Variant variant)
    {
        try
        {
            if (getRequest().getOriginalRef().getPath().endsWith("black-logo/"))
            {
                try
                {
                    File logoFile = new File(Conf.getImagePath() + "black-logo.png");
                    FileRepresentation fileRepresentation = new FileRepresentation(logoFile, MediaType.IMAGE_PNG);
                    getResponse().setEntity(fileRepresentation);
                }
                catch (Exception exception)
                {
                    Log.LOGGER.warn("Black logo image not found (black-logo.png).");
                    getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
                }
            }
            if (getRequest().getOriginalRef().getPath().endsWith("white-logo/"))
            {
                try
                {
                    File logoFile = new File(Conf.getImagePath() + "white-logo.png");
                    FileRepresentation fileRepresentation = new FileRepresentation(logoFile, MediaType.IMAGE_PNG);
                    getResponse().setEntity(fileRepresentation);
                }
                catch (Exception exception)
                {
                    Log.LOGGER.warn("White logo image not found (white-logo.png).");
                    getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
                }
            }
            else
            {
                getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            }

            return getResponseEntity();
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        return null;
    }

}
