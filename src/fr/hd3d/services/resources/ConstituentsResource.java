package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetConstituentsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des constituents de la Catégorie\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'un constituent dans une catégorie (ou sous-catégorie) donnée\r\n" + "\r\nDELETE:N/A")
public class ConstituentsResource extends AbstractHd3dCollection<ILConstituent, IConstituent>
{
    public ConstituentsResource() throws Hd3dException
    {
        super(Persistors.constituent, Translators.constituent);
    }

    @Override
    protected ResultCollection<IConstituent> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetConstituentsCmd(getResourceContext()).wantTotalSize());
    }
}
