package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILMountPoint;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * HD3D
 */
@Hd3dComment("GET:Récupération de la liste des mount points\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'un nouveau mount point\r\n" + "\r\nDELETE:N/A")
public class MountPointsResource extends AbstractHd3dCollection<ILMountPoint, IMountPoint>
{
    public MountPointsResource() throws Hd3dException
    {
        super(Persistors.mountpoint, Translators.mountpoint);
    }
}
