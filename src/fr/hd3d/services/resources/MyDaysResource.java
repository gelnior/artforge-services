package fr.hd3d.services.resources;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonDaysCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.utils.AuthenticationUtil;


/**
 * @author david gauthier
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Retrieve days of current user.\r\n" + "\r\nPUT: N/A\r\n"
        + "\r\nPOST:Create a new day for current user.\r\n" + "\r\nDELETE: N/A")
public class MyDaysResource extends AbstractHd3dCollection<ILPersonDay, IPersonDay>
{
    public MyDaysResource() throws Hd3dException
    {
        super(Persistors.personDay, Translators.personDay);
    }

    @Override
    protected ResultCollection<IPersonDay> getCollectionForRepresentation() throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user != null)
        {
            return doGetCollection(new GetPersonDaysCmd(getResourceContext(), user.getId()).wantTotalSize());
        }
        else
        {
            throw new Hd3dException("No user is set for this request, cannot return days for current user.");
        }
    }

    @Override
    protected void doPrePersist(Representation entity, IPersonDay newObject) throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user == null)
            return;
        if (user.getId().longValue() == newObject.getPerson().getId().longValue())
        {
            super.doPrePersist(entity, newObject);
        }
        else
        {
            throw new Hd3dException("You cannot create activity you don't own.");
        }
    }
}
