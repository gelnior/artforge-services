/**
 * 
 */
package fr.hd3d.services.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import net.sf.sojo.interchange.json.JsonParser;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.OutputRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ContractH;
import fr.hd3d.model.persistence.impl.hibernate.PersonH;
import fr.hd3d.model.persistence.impl.hibernate.ProjectH;
import fr.hd3d.model.persistence.impl.hibernate.SimpleActivityH;
import fr.hd3d.model.persistence.impl.hibernate.TaskActivityH;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


public class ExportCsvResource extends ServerResource
{

    private static final String INDENTATION_PATTERN = "   ";
    private static final String WEEK_END_DAY_STYLE = "weekEndDay";
    private static final String WEEK_END_DAY_NAME_STYLE = "weekEndDayName";
    private static final String WEEK_DAY_STYLE = "weekDay";
    private static final String WEEK_DAY_NAME_STYLE = "weekDayName";
    private static final String OVERTIME_STYLE = "overtime";
    private static final String BOLDWEIGHT_BOLD = "bold";
    private static final String BOLDWEIGHT_BOLD_CENTER = "bold_center";
    private static final String TEXT_FIELD = "text_field";
    private static final String MONTH_STYLE = "month";
    private static final String END_MONTH_STYLE = "end_month";

    private String[] days = new String[] { "S", "M", "T", "W", "T", "F", "S" };
    private int currentMonthNumber;
    private Map<String, CellStyle> styleMap;
    private HSSFWorkbook wb;

    private int index = 1;

    private Date startDate = null;
    private Date endDate = null;
    private Long projectId = -1L;
    private Long personId = -1L;
    private String titleTab = "";

    public ExportCsvResource()
    {
        init(Context.getCurrent(), Request.getCurrent(), Response.getCurrent());
        Log.LOGGER.info(info(Request.getCurrent()));
    }

    private DateFormat getDateFormatter()
    {
        return new SimpleDateFormat("yyyy-MM-dd");
    }

    @SuppressWarnings("unchecked")
    @Override
    @Post
    protected Representation post(Representation entity) throws ResourceException
    {

        String json = getRequest().getEntityAsForm().getValues("json");
        // read json file
        String fileName = "File-export";
        OutputRepresentation out = null;
        JsonParser jsonParser = new JsonParser();

        Map<Object, Object> object = (Map<Object, Object>) jsonParser.parse(json);
        if (object.get("timeRecap") != null)
        {
            String activityName = (String) object.get("timeRecap");
            if (activityName != null)
            {

                String str[] = activityName.split("_");
                try
                {
                    startDate = getDateFormatter().parse(str[0]);
                }
                catch (java.text.ParseException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                try
                {
                    endDate = getDateFormatter().parse(str[1]);
                }
                catch (java.text.ParseException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            // Write the output to a file
            fileName = "TimeRecap-" + activityName;
            titleTab = "";

            IProject project = null;
            if (object.get("project-id") != null)
            {
                projectId = (Long) object.get("project-id");
                Criteria criteria = HibernateUtil.currentSession().createCriteria(ProjectH.class);
                criteria.add(Restrictions.eq("id", projectId));
                project = (IProject) criteria.uniqueResult();
                if (project != null)
                {
                    fileName = "TimeRecap-" + activityName + "-" + project.getName();
                    titleTab = activityName + " " + project.getName();
                }
            }

            IPerson person = null;
            if (object.get("person-id") != null)
            {
                personId = (Long) object.get("person-id");
                Criteria criteria = HibernateUtil.currentSession().createCriteria(PersonH.class);
                criteria.add(Restrictions.eq("id", personId));
                person = (IPerson) criteria.uniqueResult();
                if (person != null)
                {
                    fileName = fileName + "-" + person.fullName() + ".ods";
                    titleTab = titleTab + " - " + person.fullName();
                }
            }

            fileName = fileName + ".ods";

            if (object.get("type") != null)
            {
                String type = (String) object.get("type");
                if (type.contains("Month"))
                {
                    Calendar calendar = new GregorianCalendar();
                    calendar.setTime(startDate);

                    fileName = "TimeRecap-" + type + "-"
                            + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH) + "-"
                            + calendar.get(Calendar.YEAR) + ".ods";
                    titleTab = type + " " + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH)
                            + "-" + calendar.get(Calendar.YEAR);
                    if (project != null)
                    {
                        fileName = "TimeRecap-" + type + "-"
                                + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH) + "-"
                                + calendar.get(Calendar.YEAR) + "-" + project.getName() + ".ods";
                        titleTab = type + " " + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH)
                                + "-" + calendar.get(Calendar.YEAR) + "-" + project.getName();
                    }
                }
            }
        }

        out = new OutputRepresentation(MediaType.APPLICATION_OPENOFFICE_ODS) {

            @Override
            public void write(OutputStream stream) throws IOException
            {
                String json = getRequest().getEntityAsForm().getValues("json");
                JsonParser jsonParser = new JsonParser();
                Map<Object, Object> object = (Map<Object, Object>) jsonParser.parse(json);

                String type = (String) object.get("type");
                if (type.compareTo("Detail") == 0)
                {

                    String activityName = (String) object.get("timeRecap");
                    if (activityName != null)
                    {

                        wb = new HSSFWorkbook();
                        Sheet sheet = wb.createSheet(activityName);

                        for (int i = 0; i < 14; i++)
                        {
                            sheet.setColumnWidth(i, 256 * 20);
                        }

                        styleMap = createStyles(wb);

                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(startDate);

                        // title row
                        Row titleRow = sheet.createRow(0);
                        titleRow.setHeightInPoints(45);
                        Cell titleCell = titleRow.createCell(0);
                        titleCell.setCellValue("Timesheets : raw data - " + activityName);
                        titleCell.setCellStyle(styleMap.get("title"));

                        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$N$1"));

                        // Create a row and put some cells in it. Rows are 0 based.
                        Row row = sheet.createRow((short) 1);
                        // Create a cell and put a value in it.

                        // Or do it on one line.
                        row.createCell(0).setCellValue("Login");
                        row.createCell(1).setCellValue("Lastname");
                        row.createCell(2).setCellValue("Firstname");
                        row.createCell(3).setCellValue("Date");
                        row.createCell(4).setCellValue("Project");
                        row.createCell(5).setCellValue("Project Type");
                        row.createCell(6).setCellValue("Task Type");
                        row.createCell(7).setCellValue("Work Object");
                        row.createCell(8).setCellValue("Work Object Type"); // Family
                        row.createCell(9).setCellValue("Work Object Scope"); // Sous DEPT => scope
                        row.createCell(10).setCellValue("Duration (Hours)");
                        row.createCell(11).setCellValue("Cost");
                        row.createCell(12).setCellValue("Task Name");
                        row.createCell(13).setCellValue("Comment");
                        // row.createCell(14).setCellValue("Departement"); TODO

                        for (int i = 0; i < 14; i++)
                        {
                            row.getCell(i).setCellStyle(styleMap.get(TEXT_FIELD));
                        }

                        addObjectTabs(sheet, row, 2);
                    }
                }
                else
                {
                    // MONTHLY
                    Calendar calendar = new GregorianCalendar();

                    // On prend le mois entier correspondant à la startDate
                    calendar.setTime(startDate);
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                    startDate = calendar.getTime();

                    calendar.setTime(startDate);
                    calendar.add(Calendar.MONTH, 1);
                    calendar.add(Calendar.DAY_OF_MONTH, -1);
                    endDate = calendar.getTime();

                    wb = new HSSFWorkbook();
                    Sheet sheet = wb.createSheet(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH)
                            + "-" + calendar.get(Calendar.YEAR));

                    for (int i = 0; i < 4; i++)
                    {
                        sheet.setColumnWidth(i, 256 * 20);
                    }

                    styleMap = createStyles(wb);

                    // title row
                    Row titleRow = sheet.createRow(0);
                    titleRow.setHeightInPoints(45);
                    Cell titleCell = titleRow.createCell(0);
                    titleCell.setCellValue("Timesheets : " + titleTab);
                    titleCell.setCellStyle(styleMap.get("title"));

                    Row row = sheet.createRow((short) 1);
                    // Create a cell and put a value in it.
                    if (type.compareTo("MonthWeekByPerson") == 0)
                    {
                        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$L$1"));
                        // Or do it on one line.
                        row.createCell(0).setCellValue("Lastname");
                        row.createCell(1).setCellValue("Firstname");
                        row.createCell(2).setCellValue("Login");
                        row.createCell(3).setCellValue("Total Hours");

                        for (int i = 0; i < 4; i++)
                        {
                            row.getCell(i).setCellStyle(styleMap.get(TEXT_FIELD));
                        }

                        createTimeLine(sheet, row, 4);

                        addWeekSumPersonTabs(sheet, row);
                    }
                    else if (type.compareTo("MonthDayByPerson") == 0)
                    {
                        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$AI$1"));
                        // Or do it on one line.
                        row.createCell(0).setCellValue("Lastname");
                        row.createCell(1).setCellValue("Firstname");
                        row.createCell(2).setCellValue("Login");
                        row.createCell(3).setCellValue("Total Hours");

                        for (int i = 0; i < 4; i++)
                        {
                            row.getCell(i).setCellStyle(styleMap.get(TEXT_FIELD));
                        }

                        createTimeLineDay(sheet, row, 4);

                        addDayPersonTabs(sheet, row);
                    }
                    else if (type.compareTo("MonthWeekByTaskType") == 0)
                    {
                        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$L$1"));
                        // Or do it on one line.
                        row.createCell(0).setCellValue("Task Type");
                        row.createCell(1).setCellValue("Project");
                        row.createCell(2).setCellValue("Total Hours");

                        for (int i = 0; i < 3; i++)
                        {
                            row.getCell(i).setCellStyle(styleMap.get(TEXT_FIELD));
                        }

                        createTimeLine(sheet, row, 3);

                        addWeekSumTaskTypeTabs(sheet, row);
                    }
                    else if (type.compareTo("MonthDayByTaskType") == 0)
                    {
                        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$AI$1"));
                        // Or do it on one line.
                        row.createCell(0).setCellValue("Task Type");
                        row.createCell(1).setCellValue("Project");
                        row.createCell(2).setCellValue("Total Hours");

                        for (int i = 0; i < 3; i++)
                        {
                            row.getCell(i).setCellStyle(styleMap.get(TEXT_FIELD));
                        }

                        createTimeLineDay(sheet, row, 3);

                        addDayTaskTypeTabs(sheet, row);
                    }
                    if (type.compareTo("MonthWeekByCategory/Sequence") == 0)
                    {
                        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$L$1"));
                        // Or do it on one line.
                        row.createCell(0).setCellValue("Category / Sequence");
                        row.createCell(1).setCellValue("Project");
                        row.createCell(2).setCellValue("Type");
                        row.createCell(3).setCellValue("Total Hours");

                        for (int i = 0; i < 4; i++)
                        {
                            row.getCell(i).setCellStyle(styleMap.get(TEXT_FIELD));
                        }

                        createTimeLine(sheet, row, 4);

                        addWeekSumCategoryTabs(sheet, row);
                    }
                    else if (type.compareTo("MonthDayByCategory/Sequence") == 0)
                    {
                        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$AI$1"));
                        // Or do it on one line.
                        row.createCell(0).setCellValue("Category / Sequence");
                        row.createCell(1).setCellValue("Project");
                        row.createCell(2).setCellValue("Type");
                        row.createCell(3).setCellValue("Total Hours");

                        for (int i = 0; i < 4; i++)
                        {
                            row.getCell(i).setCellStyle(styleMap.get(TEXT_FIELD));
                        }

                        createTimeLineDay(sheet, row, 4);

                        addDayCategoryTabs(sheet, row);
                    }
                    if (type.compareTo("MonthWeekByWorkObject") == 0)
                    {
                        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$L$1"));
                        // Or do it on one line.
                        row.createCell(0).setCellValue("Work Object");
                        row.createCell(1).setCellValue("Path");
                        row.createCell(2).setCellValue("Project");
                        row.createCell(3).setCellValue("Total Hour");

                        for (int i = 0; i < 4; i++)
                        {
                            row.getCell(i).setCellStyle(styleMap.get(TEXT_FIELD));
                        }

                        createTimeLine(sheet, row, 4);

                        addWeekSumWorkObjectTabs(sheet, row);
                    }
                    else if (type.compareTo("MonthDayByWorkObject") == 0)
                    {
                        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$AI$1"));
                        // Or do it on one line.
                        row.createCell(0).setCellValue("Work Object");
                        row.createCell(1).setCellValue("Path");
                        row.createCell(2).setCellValue("Project");
                        row.createCell(3).setCellValue("Total Hours");

                        for (int i = 0; i < 4; i++)
                        {
                            row.getCell(i).setCellStyle(styleMap.get(TEXT_FIELD));
                        }

                        createTimeLineDay(sheet, row, 4);

                        addDayWorkObjectTabs(sheet, row);
                    }
                }

                wb.write(stream);
            }
        };
        out.setDownloadable(true);
        out.setDownloadName(fileName);

        return out;
    }

    private String getIndentation(int level)
    {
        return StringUtils.repeat(INDENTATION_PATTERN, level);

    }

    @SuppressWarnings("unchecked")
    private void addObjectTabs(Sheet sheet, Row row, int index)
    {
        try
        {
            String workObjectType = "";

            // get task
            Criteria criteria = HibernateUtil.currentSession().createCriteria(TaskActivityH.class);
            criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
            criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
            if (projectId != -1)
            {
                criteria.createAlias("task", "task");
                criteria.add(Restrictions.eq("task.project.id", projectId));
            }
            criteria.createAlias("day", "personday");
            criteria.add(Restrictions.between("personday.date", startDate, endDate));

            criteria.createAlias("filledBy", "filledBy");
            if (personId != -1)
            {
                criteria.add(Restrictions.eq("filledBy.id", personId));
            }

            criteria.addOrder(Order.asc("filledBy.lastName"));

            List<ITaskActivity> listTaskActivity = criteria.list();

            for (Iterator<ITaskActivity> iterator = listTaskActivity.iterator(); iterator.hasNext();)

            {
                ITaskActivity activity = (ITaskActivity) iterator.next();
                workObjectType = "";
                StringBuilder workObjectScope = new StringBuilder();

                row = sheet.createRow((short) index);

                row.createCell(4).setCellValue(activity.getProject().getName());
                if (activity.getProject().getProjectType() != null)
                {
                    row.createCell(5).setCellValue(activity.getProject().getProjectType().getName());
                }
                row.createCell(6).setCellValue(activity.getTask().getTaskType().getName());

                if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Constituent") == 0)
                {
                    IConstituent constituent = Persistors.constituent.getById(activity.getTask()
                            .getBoundEntityTaskLink().getBoundEntity());
                    row.createCell(7).setCellValue(constituent.getLabel());

                    Object[] setCat = constituent.getCategory().parents().toArray();
                    if (setCat.length > 1)
                    {
                        for (int k = setCat.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ICategory) setCat[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(constituent.getCategory().getName());
                    workObjectType = constituent.getCategory().getName();
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Shot") == 0)
                {
                    IShot shot = Persistors.shot.getById(activity.getTask().getBoundEntityTaskLink().getBoundEntity());
                    row.createCell(7).setCellValue(shot.getLabel());

                    Object[] setSeq = shot.getSequence().parents().toArray();
                    if (setSeq.length > 1)
                    {
                        for (int k = setSeq.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ISequence) setSeq[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(shot.getSequence().getName());
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("category") == 0)
                {
                    ICategory category = Persistors.category.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());
                    row.createCell(7).setCellValue(category.getName());

                    Object[] setCat = category.parents().toArray();
                    if (setCat.length > 1)
                    {
                        workObjectType = ((ICategory) setCat[0]).getName();
                        for (int k = setCat.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ICategory) setCat[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(workObjectType);
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("sequence") == 0)
                {
                    ISequence sequence = Persistors.sequence.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());
                    row.createCell(7).setCellValue(sequence.getName());

                    Object[] setSeq = sequence.parents().toArray();
                    if (setSeq.length > 1)
                    {
                        workObjectType = ((ISequence) setSeq[0]).getName();
                        for (int k = setSeq.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ISequence) setSeq[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(workObjectType);
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Project") == 0)
                {
                    IProject project = Persistors.project.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());
                    row.createCell(7).setCellValue(project.getName());
                }

                // get salary
                Criteria criteriaSalary = HibernateUtil.currentSession().createCriteria(ContractH.class);
                criteriaSalary.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
                criteriaSalary.add(Restrictions.lt("startDate", activity.getDay().getDate()));
                criteriaSalary.add(Restrictions.gt("endDate", activity.getDay().getDate()));

                criteriaSalary.createAlias("person", "person");
                criteriaSalary.add(Restrictions.eq("person.id", activity.getFilledBy().getId()));

                criteriaSalary.addOrder(Order.desc("startDate"));

                List<IContract> listContract = criteriaSalary.list();

                Float salary = new Float(0);
                if (!listContract.isEmpty())
                {
                    salary = listContract.get(0).getSalary();
                }

                row.createCell(0).setCellValue(activity.getFilledBy().getLogin());
                row.createCell(2).setCellValue(activity.getFilledBy().getFirstName());
                row.createCell(1).setCellValue(activity.getFilledBy().getLastName());
                row.getCell(1).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD));
                row.createCell(3).setCellValue(getDateFormatter().format(activity.getDay().getDate()));
                row.createCell(8).setCellValue(workObjectType);
                row.createCell(9).setCellValue(workObjectScope.toString());
                row.createCell(10).setCellValue((float) (activity.getDuration()) / 3600);
                row.getCell(10).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));
                if (salary != 0)
                {
                    row.createCell(11).setCellValue((float) ((activity.getDuration()) / 3600) * (salary / 8));
                    row.getCell(11).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));
                }
                row.createCell(12).setCellValue(activity.getTask().getName());
                row.createCell(13).setCellValue(activity.getComment());
                // row.createCell(14).setCellValue(group);
                index += 1;
            }

            // get simpleActivity
            criteria = HibernateUtil.currentSession().createCriteria(SimpleActivityH.class);
            criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
            criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
            if (projectId != -1)
            {
                criteria.add(Restrictions.eq("project.id", projectId));
            }
            criteria.createAlias("day", "personday");
            criteria.add(Restrictions.between("personday.date", startDate, endDate));

            criteria.createAlias("filledBy", "filledBy");
            criteria.addOrder(Order.asc("filledBy.lastName"));

            List<ISimpleActivity> listSimpleActivity = criteria.list();

            for (Iterator<ISimpleActivity> iterator = listSimpleActivity.iterator(); iterator.hasNext();)
            {
                ISimpleActivity activity = iterator.next();

                row = sheet.createRow((short) index);

                row.createCell(4).setCellValue(activity.getProject().getName());

                if (activity.getProject().getProjectType() != null)
                {
                    row.createCell(5).setCellValue(activity.getProject().getProjectType().getName());
                }

                // get salary
                Criteria criteriaSalary = HibernateUtil.currentSession().createCriteria(ContractH.class);
                criteriaSalary.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
                criteriaSalary.add(Restrictions.lt("startDate", activity.getDay().getDate()));
                criteriaSalary.add(Restrictions.gt("endDate", activity.getDay().getDate()));

                criteriaSalary.createAlias("person", "person");
                criteriaSalary.add(Restrictions.eq("person.id", activity.getFilledBy().getId()));

                criteriaSalary.addOrder(Order.desc("startDate"));

                List<IContract> listContract = criteriaSalary.list();

                Float salary = new Float(0);
                if (!listContract.isEmpty())
                {
                    salary = listContract.get(0).getSalary();
                }

                row.createCell(6).setCellValue(activity.getType().name());

                row.createCell(0).setCellValue(activity.getFilledBy().getLogin());
                row.createCell(2).setCellValue(activity.getFilledBy().getFirstName());
                row.createCell(1).setCellValue(activity.getFilledBy().getLastName());
                row.getCell(1).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD));
                row.createCell(3).setCellValue(getDateFormatter().format(activity.getDay().getDate()));
                row.createCell(10).setCellValue((float) (activity.getDuration()) / 3600);
                row.getCell(10).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));
                if (salary != 0)
                {
                    row.createCell(11).setCellValue((float) ((activity.getDuration()) / 3600) * (salary / 8));
                    row.getCell(11).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));
                }
                row.createCell(12).setCellValue(activity.getComment());
                row.createCell(13).setCellValue(activity.getComment());
                // row.createCell(14).setCellValue(group);

                index += 1;
            }

        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
        }
    }

    public static int hex2decimal(String s)
    {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16 * val + d;
        }
        return val;
    }

    private void createTimeLine(Sheet sheet, Row monthRow, int lastWeekIndex)
    {
        int index = 1;
        Row weekRow = sheet.createRow(2);

        Calendar startCalendar = new GregorianCalendar();
        startCalendar.setTime(startDate);
        int mounth = startCalendar.get(Calendar.MONTH);

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(endDate);

        // int lastWeekIndex = 4;
        int lastMontIndex = lastWeekIndex;
        currentMonthNumber = -1;
        createMonthRow(sheet, monthRow, endCalendar, startCalendar, index, lastMontIndex);
        String calendarFormatted = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT).format(
                startCalendar.getTime());
        String endDateFormatted = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT).format(endDate.getTime());
        while (!calendarFormatted.equals(endDateFormatted))
        {
            // create week row
            lastWeekIndex = createWeekRow(sheet, weekRow, startCalendar, endCalendar, index, lastWeekIndex);

            startCalendar.add(Calendar.DAY_OF_MONTH, 1);
            index++;
            calendarFormatted = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT)
                    .format(startCalendar.getTime());
            if (startCalendar.get(Calendar.MONTH) != mounth)
            {
                break;
            }
            endDateFormatted = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT).format(endDate.getTime());
        }
        Cell weekCell = monthRow.createCell(lastWeekIndex);
        weekCell.setCellStyle(styleMap.get(END_MONTH_STYLE));
        createWeekRow(sheet, weekRow, startCalendar, endCalendar, index - 1, lastWeekIndex);
    }

    private void createMonthRow(Sheet sheet, Row monthRow, Calendar endCalendar, Calendar calendar, int index,
            int lastMontIndex)
    {
        int monthNumber = calendar.get(Calendar.MONTH);

        if (currentMonthNumber != monthNumber)
        {
            Cell monthCell = monthRow.createCell(lastMontIndex);

            monthCell.setCellValue(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH) + " "
                    + calendar.get(Calendar.YEAR));
            monthCell.setCellStyle(styleMap.get(MONTH_STYLE));

            int colSpan = calendar.getActualMaximum(Calendar.WEEK_OF_MONTH);

            Calendar startCalendar = new GregorianCalendar();
            startCalendar.setTime(startDate);
            if (startCalendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
                colSpan++;

            CellRangeAddress cellRangeAddress = new CellRangeAddress(index, index, lastMontIndex, lastMontIndex
                    + colSpan - 1);
            sheet.addMergedRegion(cellRangeAddress);

            currentMonthNumber = monthNumber;
        }
    }

    private int createWeekRow(Sheet sheet, Row weekRow, Calendar calendar, Calendar endCalendar, int index,
            int lastWeekIndex)
    {
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int weekOfYear = 1;
        if (dayOfWeek == Calendar.SUNDAY || calendar.getTime().toString().equals(endCalendar.getTime().toString()))
        {
            Cell weekCell = weekRow.createCell(lastWeekIndex);
            weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
            if (weekOfYear > 53)
            {
                weekOfYear = weekOfYear % 54;
            }
            int lastDay = calendar.get(Calendar.DAY_OF_MONTH);
            int day = lastDay - 6;
            if (dayOfWeek != 1)
            {
                day = lastDay - dayOfWeek + 2;
            }
            if (day < 1)
            {
                day = 1;
            }
            if (lastDay > endCalendar.get(Calendar.DAY_OF_MONTH))
            {
                lastDay = endCalendar.get(Calendar.DAY_OF_MONTH);
            }
            weekCell.setCellValue("Week " + weekOfYear + " - " + day + " -> " + lastDay);
            weekCell.setCellStyle(styleMap.get(WEEK_DAY_STYLE));
            sheet.setColumnWidth(lastWeekIndex, 256 * 20);

            lastWeekIndex = lastWeekIndex + 1;
        }
        return lastWeekIndex;
    }

    /**
     * create a library of cell styles
     */
    private Map<String, CellStyle> createStyles(Workbook wb)
    {
        Map<String, CellStyle> styles = new HashMap<String, CellStyle>();

        CellStyle style;

        Font titleFont = wb.createFont();
        titleFont.setFontHeightInPoints((short) 14);
        titleFont.setFontName("Trebuchet MS");
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setFont(titleFont);
        styles.put("title", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put(WEEK_DAY_NAME_STYLE, style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styles.put(WEEK_DAY_STYLE, style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put(WEEK_END_DAY_NAME_STYLE, style);

        style = createBorderedStyle(wb);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put(WEEK_END_DAY_STYLE, style);

        style = createAllBorderedStyle(wb);
        style.setWrapText(true);
        style.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put(OVERTIME_STYLE, style);

        style = wb.createCellStyle();
        style = createAllBorderedStyle(wb);
        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);
        styles.put(TEXT_FIELD, style);

        style = wb.createCellStyle();
        font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);
        styles.put(BOLDWEIGHT_BOLD, style);

        style = wb.createCellStyle();
        style = createAllBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setIndention((short) 1);
        style.setWrapText(true);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);
        styles.put(BOLDWEIGHT_BOLD_CENTER, style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);
        styles.put(MONTH_STYLE, style);

        style = wb.createCellStyle();
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);
        styles.put(END_MONTH_STYLE, style);

        return styles;
    }

    private static CellStyle createBorderedStyle(Workbook wb)
    {
        CellStyle style = wb.createCellStyle();
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }

    private static CellStyle createAllBorderedStyle(Workbook wb)
    {
        CellStyle style = wb.createCellStyle();
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }

    protected String info(Request request)
    {
        StringBuilder info = new StringBuilder(300);
        info.append(request.getClientInfo().getAddress()).append(' ').append(request.getClientInfo().getAgent())
                .append(' ').append(request.getMethod()).append(' ').append(request.getOriginalRef());
        return info.toString();
    }

    public Long getIdFromUrl(String idString)
    {
        return NumberUtils.toLong((String) getRequest().getAttributes().get(idString));
    }

    protected void error(Status status, Exception e)
    {
        String error = ExceptUtils.format(e);
        Log.LOGGER.info(info(getResponse()));
        Log.LOGGER.error(error);
        getResponse().setStatus(status);
        getResponse().setEntity(new StringRepresentation(Hd3dJsonSerializer.serialize(error)));
    }

    protected String info(Response response)
    {
        StringBuilder info = new StringBuilder(300);
        info.append(info(response.getRequest())).append(' ').append(response.getStatus());
        return info.toString();
    }

    @SuppressWarnings("unchecked")
    private void addWeekSumPersonTabs(Sheet sheet, Row row)
    {

        ArrayList<Object[]> recapWeeks = new ArrayList<Object[]>();

        Calendar calendar = new GregorianCalendar();
        calendar.setMinimalDaysInFirstWeek(1);
        calendar.setTime(startDate);
        int numberOfWeeks = calendar.getActualMaximum(Calendar.WEEK_OF_MONTH);
        int mounth = calendar.get(Calendar.MONTH);

        // get task
        Criteria criteria = HibernateUtil.currentSession().createCriteria(TaskActivityH.class);
        criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
        criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
        if (projectId != -1)
        {
            criteria.createAlias("task", "task");
            criteria.add(Restrictions.eq("task.project.id", projectId));
        }
        criteria.createAlias("day", "personday");
        criteria.add(Restrictions.between("personday.date", startDate, endDate));

        criteria.createAlias("filledBy", "filledBy");
        criteria.addOrder(Order.asc("filledBy.lastName"));

        List<ITaskActivity> listTaskActivity = criteria.list();

        for (Iterator<ITaskActivity> iterator = listTaskActivity.iterator(); iterator.hasNext();)
        {
            ITaskActivity activity = (ITaskActivity) iterator.next();

            boolean find = false;

            calendar = new GregorianCalendar();
            calendar.setMinimalDaysInFirstWeek(1);
            calendar.setTime(activity.getDay().getDate());
            int numberWeek = calendar.get(Calendar.WEEK_OF_MONTH);

            if (mounth != calendar.get(Calendar.MONTH))
            {
                continue;
            }

            if (!recapWeeks.isEmpty())
            {
                for (Object[] recapWeek : recapWeeks)
                {
                    if (((String) recapWeek[2]).compareTo(activity.getFilledBy().getLogin()) == 0)
                    {
                        ((Object[]) recapWeek[3])[numberWeek - 1] = (Long) ((Object[]) recapWeek[3])[numberWeek - 1]
                                + activity.getDuration();
                        recapWeek[4] = (Long) recapWeek[4] + activity.getDuration();
                        find = true;
                        break;
                    }
                }
            }

            if (!find)
            {
                Object recapWeek[] = new Object[5];
                recapWeek[0] = activity.getFilledBy().getFirstName();
                recapWeek[1] = activity.getFilledBy().getLastName();
                recapWeek[2] = activity.getFilledBy().getLogin();

                recapWeek[3] = new Object[6];
                ((Object[]) recapWeek[3])[0] = new Long(0L);
                ((Object[]) recapWeek[3])[1] = new Long(0L);
                ((Object[]) recapWeek[3])[2] = new Long(0L);
                ((Object[]) recapWeek[3])[3] = new Long(0L);
                ((Object[]) recapWeek[3])[4] = new Long(0L);
                ((Object[]) recapWeek[3])[5] = new Long(0L);
                ((Object[]) recapWeek[3])[numberWeek - 1] = activity.getDuration();

                // Total
                recapWeek[4] = activity.getDuration();

                recapWeeks.add(recapWeek);
            }

            index += 1;
        }

        // get simpleActivity
        criteria = HibernateUtil.currentSession().createCriteria(SimpleActivityH.class);
        criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
        criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
        if (projectId != -1)
        {
            criteria.add(Restrictions.eq("project.id", projectId));
        }
        criteria.createAlias("day", "personday");
        criteria.add(Restrictions.between("personday.date", startDate, endDate));

        List<ISimpleActivity> listSimpleActivity = criteria.list();

        for (Iterator<ISimpleActivity> iterator = listSimpleActivity.iterator(); iterator.hasNext();)
        {
            ISimpleActivity activity = iterator.next();

            boolean find = false;

            calendar = new GregorianCalendar();
            calendar.setMinimalDaysInFirstWeek(1);
            calendar.setTime(activity.getDay().getDate());
            int numberWeek = calendar.get(Calendar.WEEK_OF_MONTH);

            if (mounth != calendar.get(Calendar.MONTH))
            {
                continue;
            }

            if (!recapWeeks.isEmpty())
            {
                for (Object[] recapWeek : recapWeeks)
                {
                    if (((String) recapWeek[2]).compareTo(activity.getFilledBy().getLogin()) == 0)
                    {
                        ((Object[]) recapWeek[3])[numberWeek - 1] = (Long) ((Object[]) recapWeek[3])[numberWeek - 1]
                                + activity.getDuration();
                        recapWeek[4] = (Long) recapWeek[4] + activity.getDuration();
                        find = true;
                        break;
                    }
                }
            }

            if (!find)
            {
                Object recapWeek[] = new Object[5];
                recapWeek[0] = activity.getFilledBy().getFirstName();
                recapWeek[1] = activity.getFilledBy().getLastName();
                recapWeek[2] = activity.getFilledBy().getLogin();

                recapWeek[3] = new Object[6];
                ((Object[]) recapWeek[3])[0] = new Long(0L);
                ((Object[]) recapWeek[3])[1] = new Long(0L);
                ((Object[]) recapWeek[3])[2] = new Long(0L);
                ((Object[]) recapWeek[3])[3] = new Long(0L);
                ((Object[]) recapWeek[3])[4] = new Long(0L);
                ((Object[]) recapWeek[3])[5] = new Long(0L);

                ((Object[]) recapWeek[3])[numberWeek - 1] = activity.getDuration();

                // Total
                recapWeek[4] = activity.getDuration();

                recapWeeks.add(recapWeek);
            }
        }

        int index = 3;

        for (Object[] recapWeek : recapWeeks)
        {
            row = sheet.createRow((short) index);

            row.createCell(1).setCellValue((String) recapWeek[0]);
            row.createCell(0).setCellValue((String) recapWeek[1]);
            row.getCell(0).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD));
            row.createCell(2).setCellValue((String) recapWeek[2]);

            row.createCell(3).setCellValue((float) (Long) recapWeek[4] / 3600);
            row.getCell(3).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));

            for (int i = 0; i < numberOfWeeks; i++)
            {
                Cell dayCell = row.createCell(i + 4);
                dayCell.setCellValue((float) ((Long) ((Object[]) recapWeek[3])[i]) / 3600);
                // 40 heures (à vérifier)
                if (((Long) ((Object[]) recapWeek[3])[i]) > 144000)
                {
                    dayCell.setCellStyle(styleMap.get(OVERTIME_STYLE));
                }
            }

            index += 1;
        }
    }

    private void createTimeLineDay(Sheet sheet, Row monthRow, int lastWeekIndex)
    {
        Row weekRow = sheet.createRow(2);
        Row dayNameRow = sheet.createRow(3);
        Row dayNumberRow = sheet.createRow(4);
        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(endDate);
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startDate);
        int index = lastWeekIndex;
        // int lastWeekIndex = 4;
        int lastMontIndex = lastWeekIndex;
        currentMonthNumber = -1;
        createMonthRowDay(sheet, monthRow, endCalendar, calendar, 1, lastMontIndex);
        String calendarFormatted = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT).format(calendar.getTime());
        Calendar endCalendar2 = new GregorianCalendar();
        endCalendar2.setTime(endDate);
        endCalendar2.add(Calendar.DAY_OF_MONTH, 1);
        String endDateFormatted = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT).format(
                endCalendar2.getTime());
        while (!calendarFormatted.equals(endDateFormatted))
        {
            Cell dayNumberCell = dayNumberRow.createCell(index);
            dayNumberCell.setCellValue(calendar.get(Calendar.DAY_OF_MONTH));
            Cell dayNameCell = dayNameRow.createCell(index);
            dayNameCell.setCellValue(days[calendar.get(Calendar.DAY_OF_WEEK) - 1]);

            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
                    || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
            {
                dayNumberCell.setCellStyle(styleMap.get(WEEK_END_DAY_STYLE));
                dayNameCell.setCellStyle(styleMap.get(WEEK_END_DAY_NAME_STYLE));
            }
            else
            {
                dayNumberCell.setCellStyle(styleMap.get(WEEK_DAY_STYLE));
                dayNameCell.setCellStyle(styleMap.get(WEEK_DAY_NAME_STYLE));
            }
            // create week row
            lastWeekIndex = createWeekRowDay(sheet, weekRow, calendar, endCalendar, index, lastWeekIndex);
            // create month row
            createMonthRowDay(sheet, monthRow, endCalendar, calendar, 1, lastMontIndex);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            index++;
            calendarFormatted = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT).format(calendar.getTime());
            // endDateFormatted = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT).format(endDate.getTime());
        }
        if (lastWeekIndex < index - 1)
        {
            createWeekRowDay(sheet, weekRow, calendar, endCalendar, index - 1, lastWeekIndex);
        }

        Cell weekCell = monthRow.createCell(lastWeekIndex - 1);
        weekCell.setCellStyle(styleMap.get(END_MONTH_STYLE));
        weekCell = weekRow.createCell(lastWeekIndex - 1);
        weekCell.setCellStyle(styleMap.get(WEEK_END_DAY_NAME_STYLE));
    }

    private int createMonthRowDay(Sheet sheet, Row monthRow, Calendar endCalendar, Calendar calendar, int index,
            int lastMontIndex)
    {
        int monthNumber = calendar.get(Calendar.MONTH);
        int endDateMonthNumber = endCalendar.get(Calendar.MONTH);
        if (currentMonthNumber != monthNumber)
        {
            Cell monthCell = monthRow.createCell(lastMontIndex);
            int lastDayOfMonthNumber = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            int colSpan = lastDayOfMonthNumber - calendar.get(Calendar.DAY_OF_MONTH);
            if (monthNumber == endDateMonthNumber)
            {
                lastDayOfMonthNumber = endCalendar.get(Calendar.DAY_OF_MONTH);
                colSpan = lastDayOfMonthNumber - calendar.get(Calendar.DAY_OF_MONTH);
            }
            monthCell.setCellValue(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH) + " "
                    + calendar.get(Calendar.YEAR));
            monthCell.setCellStyle(styleMap.get(MONTH_STYLE));
            CellRangeAddress cellRangeAddress = new CellRangeAddress(index, index, lastMontIndex, lastMontIndex
                    + colSpan);
            sheet.addMergedRegion(cellRangeAddress);
            currentMonthNumber = monthNumber;
            lastMontIndex += colSpan + 1;
        }
        return lastMontIndex;
    }

    private int createWeekRowDay(Sheet sheet, Row weekRow, Calendar calendar, Calendar endCalendar, int index,
            int lastWeekIndex)
    {
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int weekOfYear = 1;
        if (dayOfWeek == Calendar.SUNDAY || calendar.getTime().toString().equals(endCalendar.getTime().toString()))
        {
            Cell weekCell = weekRow.createCell(lastWeekIndex);
            weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
            if (weekOfYear > 53)
            {
                weekOfYear = weekOfYear % 54;
            }
            weekCell.setCellValue(weekOfYear);
            weekCell.setCellStyle(styleMap.get(WEEK_DAY_STYLE));
            CellRangeAddress cellRangeAddress = new CellRangeAddress(2, 2, lastWeekIndex, index);
            sheet.addMergedRegion(cellRangeAddress);
            lastWeekIndex = index + 1;
        }
        return lastWeekIndex;
    }

    @SuppressWarnings("unchecked")
    private void addDayPersonTabs(Sheet sheet, Row row)
    {

        ArrayList<Object[]> recapDays = new ArrayList<Object[]>();

        Calendar calendar = new GregorianCalendar();
        calendar.setMinimalDaysInFirstWeek(1);
        calendar.setTime(startDate);
        int numberOfDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int mounth = calendar.get(Calendar.MONTH);

        // get task
        Criteria criteria = HibernateUtil.currentSession().createCriteria(TaskActivityH.class);
        criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
        criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
        if (projectId != -1)
        {
            criteria.createAlias("task", "task");
            criteria.add(Restrictions.eq("task.project.id", projectId));
        }
        criteria.createAlias("day", "personday");
        criteria.add(Restrictions.between("personday.date", startDate, endDate));

        criteria.createAlias("filledBy", "filledBy");
        criteria.addOrder(Order.asc("filledBy.lastName"));

        List<ITaskActivity> listTaskActivity = criteria.list();

        for (Iterator<ITaskActivity> iterator = listTaskActivity.iterator(); iterator.hasNext();)
        {
            ITaskActivity activity = (ITaskActivity) iterator.next();

            boolean find = false;

            calendar = new GregorianCalendar();
            calendar.setMinimalDaysInFirstWeek(1);
            calendar.setTime(activity.getDay().getDate());
            int numberDay = calendar.get(Calendar.DAY_OF_MONTH);

            if (mounth != calendar.get(Calendar.MONTH))
            {
                continue;
            }

            if (!recapDays.isEmpty())
            {
                for (Object[] recapDay : recapDays)
                {
                    if (((String) recapDay[2]).compareTo(activity.getFilledBy().getLogin()) == 0)
                    {
                        ((Object[]) recapDay[3])[numberDay - 1] = (Long) ((Object[]) recapDay[3])[numberDay - 1]
                                + activity.getDuration();
                        recapDay[4] = (Long) recapDay[4] + activity.getDuration();
                        find = true;
                        break;
                    }
                }
            }

            if (!find)
            {
                Object recapDay[] = new Object[5];
                recapDay[0] = activity.getFilledBy().getFirstName();
                recapDay[1] = activity.getFilledBy().getLastName();
                recapDay[2] = activity.getFilledBy().getLogin();

                recapDay[3] = new Object[31];

                for (int i = 0; i < 31; i++)
                {
                    ((Object[]) recapDay[3])[i] = new Long(0L);
                }

                ((Object[]) recapDay[3])[numberDay - 1] = activity.getDuration();

                // Total
                recapDay[4] = activity.getDuration();

                recapDays.add(recapDay);
            }

            index += 1;
        }

        // get simpleActivity
        criteria = HibernateUtil.currentSession().createCriteria(SimpleActivityH.class);
        criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
        criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
        if (projectId != -1)
        {
            criteria.add(Restrictions.eq("project.id", projectId));
        }
        criteria.createAlias("day", "personday");
        criteria.add(Restrictions.between("personday.date", startDate, endDate));

        List<ISimpleActivity> listSimpleActivity = criteria.list();

        for (Iterator<ISimpleActivity> iterator = listSimpleActivity.iterator(); iterator.hasNext();)
        {
            ISimpleActivity activity = iterator.next();

            boolean find = false;

            calendar = new GregorianCalendar();
            calendar.setMinimalDaysInFirstWeek(1);
            calendar.setTime(activity.getDay().getDate());
            int numberDay = calendar.get(Calendar.DAY_OF_MONTH);

            if (mounth != calendar.get(Calendar.MONTH))
            {
                continue;
            }

            if (!recapDays.isEmpty())
            {
                for (Object[] recapDay : recapDays)
                {
                    if (((String) recapDay[2]).compareTo(activity.getFilledBy().getLogin()) == 0)
                    {
                        ((Object[]) recapDay[3])[numberDay - 1] = (Long) ((Object[]) recapDay[3])[numberDay - 1]
                                + activity.getDuration();
                        recapDay[4] = (Long) recapDay[4] + activity.getDuration();
                        find = true;
                        break;
                    }
                }
            }

            if (!find)
            {
                Object recapDay[] = new Object[5];
                recapDay[0] = activity.getFilledBy().getFirstName();
                recapDay[1] = activity.getFilledBy().getLastName();
                recapDay[2] = activity.getFilledBy().getLogin();

                recapDay[3] = new Object[31];

                for (int i = 0; i < 31; i++)
                {
                    ((Object[]) recapDay[3])[i] = new Long(0L);
                }

                ((Object[]) recapDay[3])[numberDay - 1] = activity.getDuration();

                // Total
                recapDay[4] = activity.getDuration();

                recapDays.add(recapDay);
            }
        }

        int index = 5;

        for (Object[] recapDay : recapDays)
        {
            row = sheet.createRow((short) index);

            row.createCell(1).setCellValue((String) recapDay[0]);
            row.createCell(0).setCellValue((String) recapDay[1]);
            row.getCell(0).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD));
            row.createCell(2).setCellValue((String) recapDay[2]);

            row.createCell(3).setCellValue((float) (Long) recapDay[4] / 3600);
            row.getCell(3).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));

            for (int i = 0; i < numberOfDays; i++)
            {
                Cell dayCell = row.createCell(i + 4);
                dayCell.setCellValue((float) ((Long) ((Object[]) recapDay[3])[i]) / 3600);
                if (((Long) ((Object[]) recapDay[3])[i]) > 28800)
                {
                    dayCell.setCellStyle(styleMap.get(OVERTIME_STYLE));
                }
            }

            index += 1;
        }
    }

    @SuppressWarnings("unchecked")
    private void addWeekSumTaskTypeTabs(Sheet sheet, Row row)
    {

        ArrayList<Object[]> recapWeeks = new ArrayList<Object[]>();

        Calendar calendar = new GregorianCalendar();
        calendar.setMinimalDaysInFirstWeek(1);
        calendar.setTime(startDate);
        int numberOfWeeks = calendar.getActualMaximum(Calendar.WEEK_OF_MONTH);
        int mounth = calendar.get(Calendar.MONTH);

        // get task
        Criteria criteria = HibernateUtil.currentSession().createCriteria(TaskActivityH.class);
        criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
        criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
        criteria.createAlias("task", "task");
        if (projectId != -1)
        {
            criteria.add(Restrictions.eq("task.project.id", projectId));
        }
        criteria.createAlias("day", "personday");
        criteria.add(Restrictions.between("personday.date", startDate, endDate));

        criteria.createAlias("task.taskType", "tasktype");
        criteria.addOrder(Order.asc("tasktype.name"));

        List<ITaskActivity> listTaskActivity = criteria.list();

        for (Iterator<ITaskActivity> iterator = listTaskActivity.iterator(); iterator.hasNext();)
        {
            ITaskActivity activity = (ITaskActivity) iterator.next();

            boolean find = false;

            calendar = new GregorianCalendar();
            calendar.setMinimalDaysInFirstWeek(1);
            calendar.setTime(activity.getDay().getDate());
            int numberWeek = calendar.get(Calendar.WEEK_OF_MONTH);

            if (mounth != calendar.get(Calendar.MONTH))
            {
                continue;
            }

            if (!recapWeeks.isEmpty())
            {
                for (Object[] recapWeek : recapWeeks)
                {
                    if (((String) recapWeek[0]).compareTo(activity.getTask().getTaskType().getName()) == 0)
                    {
                        ((Object[]) recapWeek[2])[numberWeek - 1] = (Long) ((Object[]) recapWeek[2])[numberWeek - 1]
                                + activity.getDuration();
                        recapWeek[3] = (Long) recapWeek[3] + activity.getDuration();
                        find = true;
                        break;
                    }
                }
            }

            if (!find)
            {
                Object recapWeek[] = new Object[5];
                recapWeek[0] = activity.getTask().getTaskType().getName();
                recapWeek[1] = activity.getProject().getName();

                recapWeek[2] = new Object[6];
                ((Object[]) recapWeek[2])[0] = new Long(0L);
                ((Object[]) recapWeek[2])[1] = new Long(0L);
                ((Object[]) recapWeek[2])[2] = new Long(0L);
                ((Object[]) recapWeek[2])[3] = new Long(0L);
                ((Object[]) recapWeek[2])[4] = new Long(0L);
                ((Object[]) recapWeek[2])[5] = new Long(0L);
                ((Object[]) recapWeek[2])[numberWeek - 1] = activity.getDuration();

                // Total
                recapWeek[3] = activity.getDuration();

                recapWeeks.add(recapWeek);
            }

            index += 1;
        }

        // get simpleActivity
        criteria = HibernateUtil.currentSession().createCriteria(SimpleActivityH.class);
        criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
        criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
        if (projectId != -1)
        {
            criteria.add(Restrictions.eq("project.id", projectId));
        }
        criteria.createAlias("day", "personday");
        criteria.add(Restrictions.between("personday.date", startDate, endDate));

        List<ISimpleActivity> listSimpleActivity = criteria.list();

        for (Iterator<ISimpleActivity> iterator = listSimpleActivity.iterator(); iterator.hasNext();)
        {
            ISimpleActivity activity = iterator.next();

            boolean find = false;

            calendar = new GregorianCalendar();
            calendar.setMinimalDaysInFirstWeek(1);
            calendar.setTime(activity.getDay().getDate());
            int numberWeek = calendar.get(Calendar.WEEK_OF_MONTH);

            if (mounth != calendar.get(Calendar.MONTH))
            {
                continue;
            }

            if (!recapWeeks.isEmpty())
            {
                for (Object[] recapWeek : recapWeeks)
                {
                    if (((String) recapWeek[0]).compareTo(activity.getType().name()) == 0)
                    {
                        ((Object[]) recapWeek[2])[numberWeek - 1] = (Long) ((Object[]) recapWeek[2])[numberWeek - 1]
                                + activity.getDuration();
                        recapWeek[3] = (Long) recapWeek[3] + activity.getDuration();
                        find = true;
                        break;
                    }
                }
            }

            if (!find)
            {
                Object recapWeek[] = new Object[5];
                recapWeek[0] = activity.getType().name();
                recapWeek[1] = activity.getProject().getName();

                recapWeek[2] = new Object[6];
                ((Object[]) recapWeek[2])[0] = new Long(0L);
                ((Object[]) recapWeek[2])[1] = new Long(0L);
                ((Object[]) recapWeek[2])[2] = new Long(0L);
                ((Object[]) recapWeek[2])[3] = new Long(0L);
                ((Object[]) recapWeek[2])[4] = new Long(0L);
                ((Object[]) recapWeek[2])[5] = new Long(0L);

                ((Object[]) recapWeek[2])[numberWeek - 1] = activity.getDuration();

                // Total
                recapWeek[3] = activity.getDuration();

                recapWeeks.add(recapWeek);
            }
        }

        int index = 3;

        for (Object[] recapWeek : recapWeeks)
        {
            row = sheet.createRow((short) index);

            row.createCell(0).setCellValue((String) recapWeek[0]);
            row.createCell(1).setCellValue((String) recapWeek[1]);
            row.getCell(0).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD));

            row.createCell(2).setCellValue((float) (Long) recapWeek[3] / 3600);
            row.getCell(2).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));

            for (int i = 0; i < numberOfWeeks; i++)
            {
                Cell dayCell = row.createCell(i + 3);
                dayCell.setCellValue((float) ((Long) ((Object[]) recapWeek[2])[i]) / 3600);
            }

            index += 1;
        }
    }

    @SuppressWarnings("unchecked")
    private void addDayTaskTypeTabs(Sheet sheet, Row row)
    {

        ArrayList<Object[]> recapDays = new ArrayList<Object[]>();

        Calendar calendar = new GregorianCalendar();
        calendar.setMinimalDaysInFirstWeek(1);
        calendar.setTime(startDate);
        int numberOfDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int mounth = calendar.get(Calendar.MONTH);

        // get task
        Criteria criteria = HibernateUtil.currentSession().createCriteria(TaskActivityH.class);
        criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
        criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
        if (projectId != -1)
        {
            criteria.createAlias("task", "task");
            criteria.add(Restrictions.eq("task.project.id", projectId));
        }
        criteria.createAlias("day", "personday");
        criteria.add(Restrictions.between("personday.date", startDate, endDate));

        criteria.createAlias("task.taskType", "tasktype");
        criteria.addOrder(Order.asc("tasktype.name"));

        List<ITaskActivity> listTaskActivity = criteria.list();

        for (Iterator<ITaskActivity> iterator = listTaskActivity.iterator(); iterator.hasNext();)
        {
            ITaskActivity activity = (ITaskActivity) iterator.next();

            boolean find = false;

            calendar = new GregorianCalendar();
            calendar.setMinimalDaysInFirstWeek(1);
            calendar.setTime(activity.getDay().getDate());
            int numberDay = calendar.get(Calendar.DAY_OF_MONTH);

            if (mounth != calendar.get(Calendar.MONTH))
            {
                continue;
            }

            if (!recapDays.isEmpty())
            {
                for (Object[] recapDay : recapDays)
                {
                    if (((String) recapDay[0]).compareTo(activity.getTask().getTaskType().getName()) == 0)
                    {
                        ((Object[]) recapDay[2])[numberDay - 1] = (Long) ((Object[]) recapDay[2])[numberDay - 1]
                                + activity.getDuration();
                        recapDay[3] = (Long) recapDay[3] + activity.getDuration();
                        find = true;
                        break;
                    }
                }
            }

            if (!find)
            {
                Object recapDay[] = new Object[5];
                recapDay[0] = activity.getTask().getTaskType().getName();
                recapDay[1] = activity.getProject().getName();

                recapDay[2] = new Object[31];

                for (int i = 0; i < 31; i++)
                {
                    ((Object[]) recapDay[2])[i] = new Long(0L);
                }

                ((Object[]) recapDay[2])[numberDay - 1] = activity.getDuration();

                // Total
                recapDay[3] = activity.getDuration();

                recapDays.add(recapDay);
            }

            index += 1;
        }

        // get simpleActivity
        criteria = HibernateUtil.currentSession().createCriteria(SimpleActivityH.class);
        criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
        criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
        if (projectId != -1)
        {
            criteria.add(Restrictions.eq("project.id", projectId));
        }
        criteria.createAlias("day", "personday");
        criteria.add(Restrictions.between("personday.date", startDate, endDate));

        List<ISimpleActivity> listSimpleActivity = criteria.list();

        for (Iterator<ISimpleActivity> iterator = listSimpleActivity.iterator(); iterator.hasNext();)
        {
            ISimpleActivity activity = iterator.next();

            boolean find = false;

            calendar = new GregorianCalendar();
            calendar.setMinimalDaysInFirstWeek(1);
            calendar.setTime(activity.getDay().getDate());
            int numberDay = calendar.get(Calendar.DAY_OF_MONTH);

            if (mounth != calendar.get(Calendar.MONTH))
            {
                continue;
            }

            if (!recapDays.isEmpty())
            {
                for (Object[] recapDay : recapDays)
                {
                    if (((String) recapDay[0]).compareTo(activity.getType().name()) == 0)
                    {
                        ((Object[]) recapDay[2])[numberDay - 1] = (Long) ((Object[]) recapDay[2])[numberDay - 1]
                                + activity.getDuration();
                        recapDay[3] = (Long) recapDay[3] + activity.getDuration();
                        find = true;
                        break;
                    }
                }
            }

            if (!find)
            {
                Object recapDay[] = new Object[5];
                recapDay[0] = activity.getType().name();
                recapDay[1] = activity.getProject().getName();

                recapDay[2] = new Object[31];

                for (int i = 0; i < 31; i++)
                {
                    ((Object[]) recapDay[2])[i] = new Long(0L);
                }

                ((Object[]) recapDay[2])[numberDay - 1] = activity.getDuration();

                // Total
                recapDay[3] = activity.getDuration();

                recapDays.add(recapDay);
            }
        }

        int index = 5;

        for (Object[] recapDay : recapDays)
        {
            row = sheet.createRow((short) index);

            row.createCell(0).setCellValue((String) recapDay[0]);
            row.createCell(1).setCellValue((String) recapDay[1]);
            row.getCell(0).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD));

            row.createCell(2).setCellValue((float) (Long) recapDay[3] / 3600);
            row.getCell(2).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));

            for (int i = 0; i < numberOfDays; i++)
            {
                Cell dayCell = row.createCell(i + 3);
                dayCell.setCellValue((float) ((Long) ((Object[]) recapDay[2])[i]) / 3600);
            }

            index += 1;
        }
    }

    @SuppressWarnings("unchecked")
    private void addWeekSumCategoryTabs(Sheet sheet, Row row)
    {
        try
        {
            String workObjectType = "";
            String objectType = "";

            ArrayList<Object[]> recapWeeks = new ArrayList<Object[]>();

            Calendar calendar = new GregorianCalendar();
            calendar.setMinimalDaysInFirstWeek(1);
            calendar.setTime(startDate);
            int numberOfWeeks = calendar.getActualMaximum(Calendar.WEEK_OF_MONTH);
            int mounth = calendar.get(Calendar.MONTH);

            // get task
            Criteria criteria = HibernateUtil.currentSession().createCriteria(TaskActivityH.class);
            criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
            criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
            criteria.createAlias("task", "task");
            if (projectId != -1)
            {
                criteria.add(Restrictions.eq("task.project.id", projectId));
            }
            criteria.createAlias("day", "personday");
            criteria.add(Restrictions.between("personday.date", startDate, endDate));

            criteria.createAlias("task.taskType", "tasktype");
            criteria.createAlias("task.project", "project");
            criteria.addOrder(Order.asc("project.name"));
            criteria.addOrder(Order.asc("tasktype.name"));

            List<ITaskActivity> listTaskActivity = criteria.list();

            for (Iterator<ITaskActivity> iterator = listTaskActivity.iterator(); iterator.hasNext();)
            {
                workObjectType = "";
                objectType = "";
                StringBuilder workObjectScope = new StringBuilder();

                ITaskActivity activity = (ITaskActivity) iterator.next();

                boolean find = false;

                calendar = new GregorianCalendar();
                calendar.setMinimalDaysInFirstWeek(1);
                calendar.setTime(activity.getDay().getDate());
                int numberWeek = calendar.get(Calendar.WEEK_OF_MONTH);

                if (mounth != calendar.get(Calendar.MONTH))
                {
                    continue;
                }

                if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Constituent") == 0)
                {
                    IConstituent constituent = Persistors.constituent.getById(activity.getTask()
                            .getBoundEntityTaskLink().getBoundEntity());
                    // row.createCell(7).setCellValue(constituent.getLabel());

                    Object[] setCat = constituent.getCategory().parents().toArray();
                    if (setCat.length > 1)
                    {
                        for (int k = setCat.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ICategory) setCat[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(constituent.getCategory().getName());
                    workObjectType = constituent.getCategory().getName();
                    objectType = "Category";
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Shot") == 0)
                {
                    IShot shot = Persistors.shot.getById(activity.getTask().getBoundEntityTaskLink().getBoundEntity());
                    // row.createCell(7).setCellValue(shot.getLabel());

                    Object[] setSeq = shot.getSequence().parents().toArray();
                    if (setSeq.length > 1)
                    {
                        for (int k = setSeq.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ISequence) setSeq[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(shot.getSequence().getName());
                    objectType = "Sequence";
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("category") == 0)
                {
                    ICategory category = Persistors.category.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());
                    // row.createCell(7).setCellValue(category.getName());

                    Object[] setCat = category.parents().toArray();
                    if (setCat.length > 1)
                    {
                        workObjectType = ((ICategory) setCat[0]).getName();
                        for (int k = setCat.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ICategory) setCat[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(workObjectType);
                    objectType = "Category";
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("sequence") == 0)
                {
                    ISequence sequence = Persistors.sequence.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());
                    // row.createCell(7).setCellValue(sequence.getName());

                    Object[] setSeq = sequence.parents().toArray();
                    if (setSeq.length > 1)
                    {
                        workObjectType = ((ISequence) setSeq[0]).getName();
                        for (int k = setSeq.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ISequence) setSeq[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(workObjectType);
                    objectType = "Sequence";
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Project") == 0)
                {
                    IProject project = Persistors.project.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());
                    // row.createCell(7).setCellValue(project.getName());
                    workObjectScope = new StringBuilder("Undefined");
                    objectType = "Undefined";
                }
                else
                {
                    workObjectScope = new StringBuilder("Undefined");
                    objectType = "Undefined";
                }

                if (!recapWeeks.isEmpty())
                {
                    for (Object[] recapWeek : recapWeeks)
                    {
                        if (((String) recapWeek[0]).compareTo(workObjectScope.toString()) == 0
                                && ((String) recapWeek[1]).compareTo(activity.getProject().getName()) == 0)
                        {
                            ((Object[]) recapWeek[3])[numberWeek - 1] = (Long) ((Object[]) recapWeek[3])[numberWeek - 1]
                                    + activity.getDuration();
                            recapWeek[4] = (Long) recapWeek[4] + activity.getDuration();
                            find = true;
                            break;
                        }
                    }
                }

                if (!find)
                {
                    Object recapWeek[] = new Object[5];
                    recapWeek[0] = workObjectScope.toString();
                    recapWeek[1] = activity.getProject().getName();
                    recapWeek[2] = objectType;

                    recapWeek[3] = new Object[6];
                    ((Object[]) recapWeek[3])[0] = new Long(0L);
                    ((Object[]) recapWeek[3])[1] = new Long(0L);
                    ((Object[]) recapWeek[3])[2] = new Long(0L);
                    ((Object[]) recapWeek[3])[3] = new Long(0L);
                    ((Object[]) recapWeek[3])[4] = new Long(0L);
                    ((Object[]) recapWeek[3])[5] = new Long(0L);
                    ((Object[]) recapWeek[3])[numberWeek - 1] = activity.getDuration();

                    // Total
                    recapWeek[4] = activity.getDuration();

                    recapWeeks.add(recapWeek);
                }

                index += 1;
            }

            // get simpleActivity
            criteria = HibernateUtil.currentSession().createCriteria(SimpleActivityH.class);
            criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
            criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
            if (projectId != -1)
            {
                criteria.add(Restrictions.eq("project.id", projectId));
            }
            criteria.createAlias("day", "personday");
            criteria.add(Restrictions.between("personday.date", startDate, endDate));
            criteria.createAlias("project", "project");
            criteria.addOrder(Order.asc("project.name"));

            List<ISimpleActivity> listSimpleActivity = criteria.list();

            for (Iterator<ISimpleActivity> iterator = listSimpleActivity.iterator(); iterator.hasNext();)
            {
                ISimpleActivity activity = iterator.next();

                boolean find = false;

                calendar = new GregorianCalendar();
                calendar.setMinimalDaysInFirstWeek(1);
                calendar.setTime(activity.getDay().getDate());
                int numberWeek = calendar.get(Calendar.WEEK_OF_MONTH);

                if (mounth != calendar.get(Calendar.MONTH))
                {
                    continue;
                }

                if (!recapWeeks.isEmpty())
                {
                    for (Object[] recapWeek : recapWeeks)
                    {
                        if (((String) recapWeek[1]).compareTo(activity.getProject().getName()) == 0
                                && ((String) recapWeek[0]).compareTo("Undefined") == 0)
                        {
                            ((Object[]) recapWeek[3])[numberWeek - 1] = (Long) ((Object[]) recapWeek[3])[numberWeek - 1]
                                    + activity.getDuration();
                            recapWeek[4] = (Long) recapWeek[4] + activity.getDuration();
                            find = true;
                            break;
                        }
                    }
                }

                if (!find)
                {
                    Object recapWeek[] = new Object[5];
                    recapWeek[0] = "Undefined";
                    recapWeek[1] = activity.getProject().getName();
                    recapWeek[2] = "Undefined";

                    recapWeek[3] = new Object[6];
                    ((Object[]) recapWeek[3])[0] = new Long(0L);
                    ((Object[]) recapWeek[3])[1] = new Long(0L);
                    ((Object[]) recapWeek[3])[2] = new Long(0L);
                    ((Object[]) recapWeek[3])[3] = new Long(0L);
                    ((Object[]) recapWeek[3])[4] = new Long(0L);
                    ((Object[]) recapWeek[3])[5] = new Long(0L);

                    ((Object[]) recapWeek[3])[numberWeek - 1] = activity.getDuration();

                    // Total
                    recapWeek[4] = activity.getDuration();

                    recapWeeks.add(recapWeek);
                }
            }

            int index = 3;

            for (Object[] recapWeek : recapWeeks)
            {
                row = sheet.createRow((short) index);

                row.createCell(0).setCellValue((String) recapWeek[0]);
                row.createCell(1).setCellValue((String) recapWeek[1]);
                if (((String) recapWeek[0]).compareTo("Undefined") != 0)
                {
                    row.getCell(0).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD));
                }
                row.createCell(2).setCellValue((String) recapWeek[2]);

                row.createCell(3).setCellValue((float) (Long) recapWeek[4] / 3600);
                row.getCell(3).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));

                for (int i = 0; i < numberOfWeeks; i++)
                {
                    Cell dayCell = row.createCell(i + 4);
                    dayCell.setCellValue((float) ((Long) ((Object[]) recapWeek[3])[i]) / 3600);
                }

                index += 1;
            }

        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private void addDayCategoryTabs(Sheet sheet, Row row)
    {
        try
        {
            String workObjectType = "";
            String objectType = "";

            ArrayList<Object[]> recapDays = new ArrayList<Object[]>();

            Calendar calendar = new GregorianCalendar();
            calendar.setMinimalDaysInFirstWeek(1);
            calendar.setTime(startDate);
            int numberOfDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            int mounth = calendar.get(Calendar.MONTH);

            // get task
            Criteria criteria = HibernateUtil.currentSession().createCriteria(TaskActivityH.class);
            criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
            criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
            criteria.createAlias("task", "task");
            if (projectId != -1)
            {
                criteria.add(Restrictions.eq("task.project.id", projectId));
            }
            criteria.createAlias("day", "personday");
            criteria.add(Restrictions.between("personday.date", startDate, endDate));

            criteria.createAlias("task.taskType", "tasktype");
            criteria.createAlias("task.project", "project");
            criteria.addOrder(Order.asc("project.name"));
            criteria.addOrder(Order.asc("tasktype.name"));

            List<ITaskActivity> listTaskActivity = criteria.list();

            for (Iterator<ITaskActivity> iterator = listTaskActivity.iterator(); iterator.hasNext();)
            {
                ITaskActivity activity = (ITaskActivity) iterator.next();

                StringBuilder workObjectScope = new StringBuilder();

                boolean find = false;

                calendar = new GregorianCalendar();
                calendar.setMinimalDaysInFirstWeek(1);
                calendar.setTime(activity.getDay().getDate());
                int numberDay = calendar.get(Calendar.DAY_OF_MONTH);

                if (mounth != calendar.get(Calendar.MONTH))
                {
                    continue;
                }

                if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Constituent") == 0)
                {
                    IConstituent constituent = Persistors.constituent.getById(activity.getTask()
                            .getBoundEntityTaskLink().getBoundEntity());
                    // row.createCell(7).setCellValue(constituent.getLabel());

                    Object[] setCat = constituent.getCategory().parents().toArray();
                    if (setCat.length > 1)
                    {
                        for (int k = setCat.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ICategory) setCat[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(constituent.getCategory().getName());
                    workObjectType = constituent.getCategory().getName();
                    objectType = "Category";
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Shot") == 0)
                {
                    IShot shot = Persistors.shot.getById(activity.getTask().getBoundEntityTaskLink().getBoundEntity());
                    // row.createCell(7).setCellValue(shot.getLabel());

                    Object[] setSeq = shot.getSequence().parents().toArray();
                    if (setSeq.length > 1)
                    {
                        for (int k = setSeq.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ISequence) setSeq[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(shot.getSequence().getName());
                    objectType = "Sequence";
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("category") == 0)
                {
                    ICategory category = Persistors.category.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());
                    // row.createCell(7).setCellValue(category.getName());

                    Object[] setCat = category.parents().toArray();
                    if (setCat.length > 1)
                    {
                        workObjectType = ((ICategory) setCat[0]).getName();
                        for (int k = setCat.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ICategory) setCat[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(workObjectType);
                    objectType = "Category";
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("sequence") == 0)
                {
                    ISequence sequence = Persistors.sequence.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());
                    // row.createCell(7).setCellValue(sequence.getName());

                    Object[] setSeq = sequence.parents().toArray();
                    if (setSeq.length > 1)
                    {
                        workObjectType = ((ISequence) setSeq[0]).getName();
                        for (int k = setSeq.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ISequence) setSeq[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(workObjectType);
                    objectType = "Sequence";
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Project") == 0)
                {
                    IProject project = Persistors.project.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());
                    // row.createCell(7).setCellValue(project.getName());
                    workObjectScope = new StringBuilder("Undefined");
                    objectType = "Undefined";
                }
                else
                {
                    workObjectScope = new StringBuilder("Undefined");
                    objectType = "Undefined";
                }

                if (!recapDays.isEmpty())
                {
                    for (Object[] recapDay : recapDays)
                    {
                        if (((String) recapDay[0]).compareTo(workObjectScope.toString()) == 0
                                && ((String) recapDay[1]).compareTo(activity.getProject().getName()) == 0)
                        {
                            ((Object[]) recapDay[3])[numberDay - 1] = (Long) ((Object[]) recapDay[3])[numberDay - 1]
                                    + activity.getDuration();
                            recapDay[4] = (Long) recapDay[4] + activity.getDuration();
                            find = true;
                            break;
                        }
                    }
                }

                if (!find)
                {
                    Object recapDay[] = new Object[5];
                    recapDay[0] = workObjectScope.toString();
                    recapDay[1] = activity.getProject().getName();
                    recapDay[2] = objectType;

                    recapDay[3] = new Object[31];

                    for (int i = 0; i < 31; i++)
                    {
                        ((Object[]) recapDay[3])[i] = new Long(0L);
                    }

                    ((Object[]) recapDay[3])[numberDay - 1] = activity.getDuration();

                    // Total
                    recapDay[4] = activity.getDuration();

                    recapDays.add(recapDay);
                }

                index += 1;
            }

            // get simpleActivity
            criteria = HibernateUtil.currentSession().createCriteria(SimpleActivityH.class);
            criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
            criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
            if (projectId != -1)
            {
                criteria.add(Restrictions.eq("project.id", projectId));
            }
            criteria.createAlias("day", "personday");
            criteria.add(Restrictions.between("personday.date", startDate, endDate));
            criteria.createAlias("project", "project");
            criteria.addOrder(Order.asc("project.name"));

            List<ISimpleActivity> listSimpleActivity = criteria.list();

            for (Iterator<ISimpleActivity> iterator = listSimpleActivity.iterator(); iterator.hasNext();)
            {
                ISimpleActivity activity = iterator.next();

                boolean find = false;

                calendar = new GregorianCalendar();
                calendar.setMinimalDaysInFirstWeek(1);
                calendar.setTime(activity.getDay().getDate());
                int numberDay = calendar.get(Calendar.DAY_OF_MONTH);

                if (mounth != calendar.get(Calendar.MONTH))
                {
                    continue;
                }

                if (!recapDays.isEmpty())
                {
                    for (Object[] recapDay : recapDays)
                    {
                        if (((String) recapDay[1]).compareTo(activity.getProject().getName()) == 0
                                && ((String) recapDay[0]).compareTo("Undefined") == 0)
                        {
                            ((Object[]) recapDay[3])[numberDay - 1] = (Long) ((Object[]) recapDay[3])[numberDay - 1]
                                    + activity.getDuration();
                            recapDay[4] = (Long) recapDay[4] + activity.getDuration();
                            find = true;
                            break;
                        }
                    }
                }

                if (!find)
                {
                    Object recapDay[] = new Object[5];
                    recapDay[0] = "Undefined";
                    recapDay[1] = activity.getProject().getName();
                    recapDay[2] = "Undefined";

                    recapDay[3] = new Object[31];

                    for (int i = 0; i < 31; i++)
                    {
                        ((Object[]) recapDay[3])[i] = new Long(0L);
                    }

                    ((Object[]) recapDay[3])[numberDay - 1] = activity.getDuration();

                    // Total
                    recapDay[4] = activity.getDuration();

                    recapDays.add(recapDay);
                }
            }

            int index = 5;

            for (Object[] recapDay : recapDays)
            {
                row = sheet.createRow((short) index);

                row.createCell(0).setCellValue((String) recapDay[0]);
                row.createCell(1).setCellValue((String) recapDay[1]);
                if (((String) recapDay[0]).compareTo("Undefined") != 0)
                {
                    row.getCell(0).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD));
                }

                row.createCell(2).setCellValue((String) recapDay[2]);

                row.createCell(3).setCellValue((float) (Long) recapDay[4] / 3600);
                row.getCell(3).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));

                for (int i = 0; i < numberOfDays; i++)
                {
                    Cell dayCell = row.createCell(i + 4);
                    dayCell.setCellValue((float) ((Long) ((Object[]) recapDay[3])[i]) / 3600);
                }

                index += 1;
            }

        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private void addWeekSumWorkObjectTabs(Sheet sheet, Row row)
    {
        try
        {
            String workObjectType = "";
            String workObjectName = "";

            ArrayList<Object[]> recapWeeks = new ArrayList<Object[]>();

            Calendar calendar = new GregorianCalendar();
            calendar.setMinimalDaysInFirstWeek(1);
            calendar.setTime(startDate);
            int numberOfWeeks = calendar.getActualMaximum(Calendar.WEEK_OF_MONTH);
            int mounth = calendar.get(Calendar.MONTH);

            // get task
            Criteria criteria = HibernateUtil.currentSession().createCriteria(TaskActivityH.class);
            criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
            criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
            criteria.createAlias("task", "task");
            if (projectId != -1)
            {
                criteria.add(Restrictions.eq("task.project.id", projectId));
            }
            criteria.createAlias("day", "personday");
            criteria.add(Restrictions.between("personday.date", startDate, endDate));

            criteria.createAlias("task.taskType", "tasktype");
            criteria.createAlias("task.project", "project");
            criteria.addOrder(Order.asc("project.name"));
            criteria.addOrder(Order.asc("tasktype.name"));

            List<ITaskActivity> listTaskActivity = criteria.list();

            for (Iterator<ITaskActivity> iterator = listTaskActivity.iterator(); iterator.hasNext();)
            {
                workObjectType = "";
                workObjectName = "";
                StringBuilder workObjectScope = new StringBuilder();

                ITaskActivity activity = (ITaskActivity) iterator.next();

                boolean find = false;

                calendar = new GregorianCalendar();
                calendar.setMinimalDaysInFirstWeek(1);
                calendar.setTime(activity.getDay().getDate());
                int numberWeek = calendar.get(Calendar.WEEK_OF_MONTH);

                if (mounth != calendar.get(Calendar.MONTH))
                {
                    continue;
                }

                if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Constituent") == 0)
                {
                    IConstituent constituent = Persistors.constituent.getById(activity.getTask()
                            .getBoundEntityTaskLink().getBoundEntity());

                    Object[] setCat = constituent.getCategory().parents().toArray();
                    if (setCat.length > 1)
                    {
                        for (int k = setCat.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ICategory) setCat[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(constituent.getCategory().getName());
                    workObjectType = constituent.getCategory().getName();
                    workObjectName = constituent.getLabel();
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Shot") == 0)
                {
                    IShot shot = Persistors.shot.getById(activity.getTask().getBoundEntityTaskLink().getBoundEntity());

                    Object[] setSeq = shot.getSequence().parents().toArray();
                    if (setSeq.length > 1)
                    {
                        for (int k = setSeq.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ISequence) setSeq[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(shot.getSequence().getName());
                    workObjectName = shot.getLabel();
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("category") == 0)
                {
                    ICategory category = Persistors.category.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());

                    Object[] setCat = category.parents().toArray();
                    if (setCat.length > 1)
                    {
                        workObjectType = ((ICategory) setCat[0]).getName();
                        for (int k = setCat.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ICategory) setCat[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(workObjectType);
                    workObjectName = category.getName();
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("sequence") == 0)
                {
                    ISequence sequence = Persistors.sequence.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());

                    Object[] setSeq = sequence.parents().toArray();
                    if (setSeq.length > 1)
                    {
                        workObjectType = ((ISequence) setSeq[0]).getName();
                        for (int k = setSeq.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ISequence) setSeq[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(workObjectType);
                    workObjectName = sequence.getName();
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Project") == 0)
                {
                    IProject project = Persistors.project.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());

                    workObjectScope = new StringBuilder("Undefined");
                    workObjectName = project.getName();
                }
                else
                {
                    workObjectScope = new StringBuilder("Undefined");
                    workObjectName = activity.getProject().getName();
                }

                if (!recapWeeks.isEmpty())
                {
                    for (Object[] recapWeek : recapWeeks)
                    {
                        if (((String) recapWeek[0]).compareTo(workObjectName) == 0
                                && ((String) recapWeek[1]).compareTo(workObjectScope.toString()) == 0
                                && ((String) recapWeek[2]).compareTo(activity.getProject().getName()) == 0)
                        {
                            ((Object[]) recapWeek[3])[numberWeek - 1] = (Long) ((Object[]) recapWeek[3])[numberWeek - 1]
                                    + activity.getDuration();
                            recapWeek[4] = (Long) recapWeek[4] + activity.getDuration();
                            find = true;
                            break;
                        }
                    }
                }

                if (!find)
                {
                    Object recapWeek[] = new Object[5];
                    recapWeek[0] = workObjectName;
                    recapWeek[1] = workObjectScope.toString();
                    recapWeek[2] = activity.getProject().getName();

                    recapWeek[3] = new Object[6];
                    ((Object[]) recapWeek[3])[0] = new Long(0L);
                    ((Object[]) recapWeek[3])[1] = new Long(0L);
                    ((Object[]) recapWeek[3])[2] = new Long(0L);
                    ((Object[]) recapWeek[3])[3] = new Long(0L);
                    ((Object[]) recapWeek[3])[4] = new Long(0L);
                    ((Object[]) recapWeek[3])[5] = new Long(0L);
                    ((Object[]) recapWeek[3])[numberWeek - 1] = activity.getDuration();

                    // Total
                    recapWeek[4] = activity.getDuration();

                    recapWeeks.add(recapWeek);
                }

                index += 1;
            }

            // get simpleActivity
            criteria = HibernateUtil.currentSession().createCriteria(SimpleActivityH.class);
            criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
            criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
            if (projectId != -1)
            {
                criteria.add(Restrictions.eq("project.id", projectId));
            }
            criteria.createAlias("day", "personday");
            criteria.add(Restrictions.between("personday.date", startDate, endDate));

            List<ISimpleActivity> listSimpleActivity = criteria.list();

            for (Iterator<ISimpleActivity> iterator = listSimpleActivity.iterator(); iterator.hasNext();)
            {
                ISimpleActivity activity = iterator.next();

                boolean find = false;

                calendar = new GregorianCalendar();
                calendar.setMinimalDaysInFirstWeek(1);
                calendar.setTime(activity.getDay().getDate());
                int numberWeek = calendar.get(Calendar.WEEK_OF_MONTH);

                if (mounth != calendar.get(Calendar.MONTH))
                {
                    continue;
                }

                if (!recapWeeks.isEmpty())
                {
                    for (Object[] recapWeek : recapWeeks)
                    {
                        if (((String) recapWeek[2]).compareTo(activity.getProject().getName()) == 0
                                && ((String) recapWeek[0]).compareTo("Undefined") == 0)
                        {
                            ((Object[]) recapWeek[3])[numberWeek - 1] = (Long) ((Object[]) recapWeek[3])[numberWeek - 1]
                                    + activity.getDuration();
                            recapWeek[4] = (Long) recapWeek[4] + activity.getDuration();
                            find = true;
                            break;
                        }
                    }
                }

                if (!find)
                {
                    Object recapWeek[] = new Object[5];
                    recapWeek[0] = "Undefined";
                    recapWeek[1] = "Undefined";
                    recapWeek[2] = activity.getProject().getName();

                    recapWeek[3] = new Object[6];
                    ((Object[]) recapWeek[3])[0] = new Long(0L);
                    ((Object[]) recapWeek[3])[1] = new Long(0L);
                    ((Object[]) recapWeek[3])[2] = new Long(0L);
                    ((Object[]) recapWeek[3])[3] = new Long(0L);
                    ((Object[]) recapWeek[3])[4] = new Long(0L);
                    ((Object[]) recapWeek[3])[5] = new Long(0L);

                    ((Object[]) recapWeek[3])[numberWeek - 1] = activity.getDuration();

                    // Total
                    recapWeek[4] = activity.getDuration();

                    recapWeeks.add(recapWeek);
                }
            }

            int index = 3;

            for (Object[] recapWeek : recapWeeks)
            {
                row = sheet.createRow((short) index);

                row.createCell(0).setCellValue((String) recapWeek[0]);
                row.createCell(1).setCellValue((String) recapWeek[1]);
                if (((String) recapWeek[0]).compareTo("Undefined") != 0)
                {
                    row.getCell(0).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD));
                }
                row.createCell(2).setCellValue((String) recapWeek[2]);

                row.createCell(3).setCellValue((float) (Long) recapWeek[4] / 3600);
                row.getCell(3).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));

                for (int i = 0; i < numberOfWeeks; i++)
                {
                    Cell dayCell = row.createCell(i + 4);
                    dayCell.setCellValue((float) ((Long) ((Object[]) recapWeek[3])[i]) / 3600);
                }

                index += 1;
            }

        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private void addDayWorkObjectTabs(Sheet sheet, Row row)
    {
        try
        {
            String workObjectType = "";
            String workObjectName = "";

            ArrayList<Object[]> recapDays = new ArrayList<Object[]>();

            Calendar calendar = new GregorianCalendar();
            calendar.setMinimalDaysInFirstWeek(1);
            calendar.setTime(startDate);
            int numberOfDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            int mounth = calendar.get(Calendar.MONTH);

            // get task
            Criteria criteria = HibernateUtil.currentSession().createCriteria(TaskActivityH.class);
            criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
            criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
            criteria.createAlias("task", "task");
            if (projectId != -1)
            {
                criteria.add(Restrictions.eq("task.project.id", projectId));
            }
            criteria.createAlias("day", "personday");
            criteria.add(Restrictions.between("personday.date", startDate, endDate));

            criteria.createAlias("task.taskType", "tasktype");
            criteria.createAlias("task.project", "project");
            criteria.addOrder(Order.asc("project.name"));
            criteria.addOrder(Order.asc("tasktype.name"));

            List<ITaskActivity> listTaskActivity = criteria.list();

            for (Iterator<ITaskActivity> iterator = listTaskActivity.iterator(); iterator.hasNext();)
            {
                ITaskActivity activity = (ITaskActivity) iterator.next();

                boolean find = false;
                StringBuilder workObjectScope = new StringBuilder();

                calendar = new GregorianCalendar();
                calendar.setMinimalDaysInFirstWeek(1);
                calendar.setTime(activity.getDay().getDate());
                int numberDay = calendar.get(Calendar.DAY_OF_MONTH);

                if (mounth != calendar.get(Calendar.MONTH))
                {
                    continue;
                }

                if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Constituent") == 0)
                {
                    IConstituent constituent = Persistors.constituent.getById(activity.getTask()
                            .getBoundEntityTaskLink().getBoundEntity());

                    Object[] setCat = constituent.getCategory().parents().toArray();
                    if (setCat.length > 1)
                    {
                        for (int k = setCat.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ICategory) setCat[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(constituent.getCategory().getName());
                    workObjectType = constituent.getCategory().getName();
                    workObjectName = constituent.getLabel();
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Shot") == 0)
                {
                    IShot shot = Persistors.shot.getById(activity.getTask().getBoundEntityTaskLink().getBoundEntity());

                    Object[] setSeq = shot.getSequence().parents().toArray();
                    if (setSeq.length > 1)
                    {
                        for (int k = setSeq.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ISequence) setSeq[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(shot.getSequence().getName());
                    workObjectName = shot.getLabel();
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("category") == 0)
                {
                    ICategory category = Persistors.category.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());

                    Object[] setCat = category.parents().toArray();
                    if (setCat.length > 1)
                    {
                        workObjectType = ((ICategory) setCat[0]).getName();
                        for (int k = setCat.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ICategory) setCat[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(workObjectType);
                    workObjectName = category.getName();
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("sequence") == 0)
                {
                    ISequence sequence = Persistors.sequence.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());

                    Object[] setSeq = sequence.parents().toArray();
                    if (setSeq.length > 1)
                    {
                        workObjectType = ((ISequence) setSeq[0]).getName();
                        for (int k = setSeq.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ISequence) setSeq[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(workObjectType);
                    workObjectName = sequence.getName();
                }
                else if (activity.getTask().getBoundEntityTaskLink().getBoundEntityName().compareTo("Project") == 0)
                {
                    IProject project = Persistors.project.getById(activity.getTask().getBoundEntityTaskLink()
                            .getBoundEntity());

                    workObjectScope = new StringBuilder("Undefined");
                    workObjectName = project.getName();
                }
                else
                {
                    workObjectScope = new StringBuilder("Undefined");
                    workObjectName = activity.getProject().getName();
                }

                if (!recapDays.isEmpty())
                {
                    for (Object[] recapDay : recapDays)
                    {
                        if (((String) recapDay[0]).compareTo(workObjectName) == 0
                                && ((String) recapDay[1]).compareTo(workObjectScope.toString()) == 0
                                && ((String) recapDay[2]).compareTo(activity.getProject().getName()) == 0)
                        {
                            ((Object[]) recapDay[3])[numberDay - 1] = (Long) ((Object[]) recapDay[3])[numberDay - 1]
                                    + activity.getDuration();
                            recapDay[4] = (Long) recapDay[4] + activity.getDuration();
                            find = true;
                            break;
                        }
                    }
                }

                if (!find)
                {
                    Object recapDay[] = new Object[5];
                    recapDay[0] = workObjectName;
                    recapDay[1] = workObjectScope.toString();
                    recapDay[2] = activity.getProject().getName();

                    recapDay[3] = new Object[31];

                    for (int i = 0; i < 31; i++)
                    {
                        ((Object[]) recapDay[3])[i] = new Long(0L);
                    }

                    ((Object[]) recapDay[3])[numberDay - 1] = activity.getDuration();

                    // Total
                    recapDay[4] = activity.getDuration();

                    recapDays.add(recapDay);
                }

                index += 1;
            }

            // get simpleActivity
            criteria = HibernateUtil.currentSession().createCriteria(SimpleActivityH.class);
            criteria.add(Restrictions.gt("duration", NumberUtils.LONG_ZERO));
            criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
            if (projectId != -1)
            {
                criteria.add(Restrictions.eq("project.id", projectId));
            }
            criteria.createAlias("day", "personday");
            criteria.add(Restrictions.between("personday.date", startDate, endDate));
            criteria.createAlias("project", "project");
            criteria.addOrder(Order.asc("project.name"));

            List<ISimpleActivity> listSimpleActivity = criteria.list();

            for (Iterator<ISimpleActivity> iterator = listSimpleActivity.iterator(); iterator.hasNext();)
            {
                ISimpleActivity activity = iterator.next();

                boolean find = false;

                calendar = new GregorianCalendar();
                calendar.setMinimalDaysInFirstWeek(1);
                calendar.setTime(activity.getDay().getDate());
                int numberDay = calendar.get(Calendar.DAY_OF_MONTH);

                if (mounth != calendar.get(Calendar.MONTH))
                {
                    continue;
                }

                if (!recapDays.isEmpty())
                {
                    for (Object[] recapDay : recapDays)
                    {
                        if (((String) recapDay[2]).compareTo(activity.getProject().getName()) == 0
                                && ((String) recapDay[0]).compareTo("Undefined") == 0)
                        {
                            ((Object[]) recapDay[3])[numberDay - 1] = (Long) ((Object[]) recapDay[3])[numberDay - 1]
                                    + activity.getDuration();
                            recapDay[4] = (Long) recapDay[4] + activity.getDuration();
                            find = true;
                            break;
                        }
                    }
                }

                if (!find)
                {
                    Object recapDay[] = new Object[5];
                    recapDay[0] = "Undefined";
                    recapDay[1] = "Undefined";
                    recapDay[2] = activity.getProject().getName();

                    recapDay[3] = new Object[31];

                    for (int i = 0; i < 31; i++)
                    {
                        ((Object[]) recapDay[3])[i] = new Long(0L);
                    }

                    ((Object[]) recapDay[3])[numberDay - 1] = activity.getDuration();

                    // Total
                    recapDay[4] = activity.getDuration();

                    recapDays.add(recapDay);
                }
            }

            int index = 5;

            for (Object[] recapDay : recapDays)
            {
                row = sheet.createRow((short) index);

                row.createCell(0).setCellValue((String) recapDay[0]);
                if (((String) recapDay[0]).compareTo("Undefined") != 0)
                {
                    row.getCell(0).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD));
                }
                row.createCell(1).setCellValue((String) recapDay[1]);
                row.createCell(2).setCellValue((String) recapDay[2]);

                row.createCell(3).setCellValue((float) (Long) recapDay[4] / 3600);
                row.getCell(3).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));

                for (int i = 0; i < numberOfDays; i++)
                {
                    Cell dayCell = row.createCell(i + 4);
                    dayCell.setCellValue((float) ((Long) ((Object[]) recapDay[3])[i]) / 3600);
                }

                index += 1;
            }

        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
        }
    }

}
