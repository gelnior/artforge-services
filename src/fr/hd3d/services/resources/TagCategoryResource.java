package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.TAGCATEGORY_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTagCategory;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single tag category object<br>
 * URI : /tagcategories/{id}
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'une catégorie de tags\r\n" + "\r\nPUT:Modification d'une catégorie de tags\r\n"
        + "\r\nPOST:Création d'une catégorie de tags\r\n" + "\r\nDELETE:Désactivation d'une catégorie de tags")
public class TagCategoryResource extends AbstractHd3dResource<ILTagCategory, ITagCategory>
{
    @Override
    protected ITagCategory initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(TAGCATEGORY_ID_ATTRIBUTE, lockMode);
    }

    public TagCategoryResource() throws Hd3dException
    {
        super(Persistors.tagcategory, Translators.tagcategory);
    }
}
