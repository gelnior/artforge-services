package fr.hd3d.services.resources.serializer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.collections.MapIterator;
import org.apache.commons.collections.OrderedMap;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.model.lightweight.annotation.ClientField;
import fr.hd3d.model.lightweight.annotation.ServerImmutable;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;


/**
 * Serialize Lightweight objects as HTML
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 * @param <L>
 *            Lightweight object to serialize
 */
// public class HtmlResourceSerializer<L extends ILightweightBaseObject> {
public class HtmlResourceSerializer<L extends Object>
{
    private final Map<Class<L>, List<L>> map = new HashMap<Class<L>, List<L>>();
    // Lightweight object(s) to serialize
    private final Object object;
    // returned value
    private final StringBuilder buffer = new StringBuilder();

    private int nbRecords;

    /**
     * 
     * @param objects
     *            a List of Lightweight objects
     */
    @SuppressWarnings("unchecked")
    public HtmlResourceSerializer(List<L> objects)
    {
        this.object = objects;
        for (L l : objects)
            getSetForClass((Class<L>) l.getClass()).add(l);

        this.nbRecords = -1;
    }

    @SuppressWarnings("unchecked")
    public HtmlResourceSerializer(List<L> objects, int nbRecords)
    {

        this.object = objects;
        for (L l : objects)
            getSetForClass((Class<L>) l.getClass()).add(l);

        this.nbRecords = nbRecords;
    }

    @SuppressWarnings("unchecked")
    public HtmlResourceSerializer(Collection<L> objects, int nbRecords)
    {

        this.object = objects;
        for (L l : objects)
            getSetForClass((Class<L>) l.getClass()).add(l);

        this.nbRecords = nbRecords;
    }

    /**
     * 
     * @param l
     *            a Lightweight object
     */
    @SuppressWarnings("unchecked")
    public HtmlResourceSerializer(L l)
    {

        this.object = l;
        getSetForClass((Class<L>) l.getClass()).add(l);

        this.nbRecords = -1;
    }

    /**
     * return the List bound to <i>objectClass</i>. If the list does not exist, it is created.
     * 
     * @param objectClass
     * @return
     */
    private List<L> getSetForClass(final Class<L> objectClass)
    {

        List<L> set = map.get(objectClass);
        if (set == null)
        {
            set = new ArrayList<L>();
            map.put(objectClass, set);
        }
        return set;
    }

    public String process()
    {
        buffer.append("<html>");
        buffer.append("<style>");
        // buffer.append("h1 { width: 100%; background-color: black; color: white; } ");
        buffer.append("table { border: 1px solid black; } ");
        buffer.append("tr { border: 1px solid black; } ");
        buffer.append("td { border: 1px solid black; font-size: 12px; } ");
        buffer
                .append(".header { background-color: black; color: white; text-align:center; font-size: 13px; padding: 5px; } ");
        buffer.append("</style>");
        buffer.append("<body style=\"font-family: sans-serif; font-size: 14px;\">");
        for (Class<L> objectClass : map.keySet())
            appendTable(objectClass);
        appendJson();
        buffer.append("</body>");
        buffer.append("</html>");
        return buffer.toString();
    }

    private void appendJson()
    {

        buffer.append("<br />");
        buffer.append("<fieldset style='font-family:Courier'>");
        buffer.append("<legend align=top>JSON code</legend>");
        if (this.nbRecords >= 0)
        {
            buffer.append(Hd3dJsonSerializer.serialize(object, this.nbRecords));
        }
        else
        {
            buffer.append(Hd3dJsonSerializer.serialize(object));
        }
        buffer.append("</fieldset>");
    }

    private void appendTable(Class<L> objectClass)
    {
        boolean headerDone = false;// true if column headers already processed.

        buffer.append("<h1>").append(objectClass.getName()).append("</h1>");
        buffer.append("<table cellspacing=0 cellpadding=5>");
        List superInterfaces = ClassUtils.getAllInterfaces(objectClass);

        // dynabean
        // Note: unused but do not remove for the moment
        if (objectClass == org.apache.commons.beanutils.DynaBean.class
                || superInterfaces.contains(org.apache.commons.beanutils.DynaBean.class))
        {
            for (L l : map.get(objectClass))
            {
                DynaBean bean = ((DynaBean) l);
                DynaProperty[] props = bean.getDynaClass().getDynaProperties();

                if (props.length > 0)
                {
                    // build table column header
                    if (!headerDone)
                    {
                        buffer.append("<tr>");
                        for (DynaProperty p : props)
                        {
                            buffer.append("<td>").append(p.getName()).append("</td>");
                        }
                        buffer.append("</tr>");
                        headerDone = true;
                    }
                    // build the lines
                    buffer.append("<tr>");
                    for (DynaProperty p : props)
                    {
                        buffer.append("<td>").append(bean.get(p.getName())).append("</td>");
                    }
                    buffer.append("</tr>");
                }
            }
        }// usual business object class
        else if (superInterfaces.contains(fr.hd3d.model.lightweight.ILBase.class))
        {
            final List<Method> methods = fetchMethod(objectClass);
            appendRow(methods, null);
            for (L l : map.get(objectClass))
            {
                appendRow(methods, l);
            }
        }// response from a free query
        else if (objectClass == org.apache.commons.collections.map.ListOrderedMap.class)
        {
            for (L l : map.get(objectClass))
            {
                OrderedMap m = (OrderedMap) l;
                // build the column headers
                if (!headerDone)
                {
                    buffer.append("<tr>");
                    Iterator<String> it = m.keySet().iterator();
                    while (it.hasNext())
                    {
                        buffer.append("<td>").append((String) it.next()).append("</td>");
                        headerDone = true;
                    }
                    buffer.append("</tr>");
                }
                // build the lines
                buffer.append("<tr>");
                MapIterator mapIt = m.orderedMapIterator();
                while (mapIt.hasNext())
                {
                    mapIt.next();// mandatory
                    buffer.append("<td>").append(mapIt.getValue()).append("</td>");
                }
                buffer.append("</tr>");
            }
        }
        else
        {
            buffer.append(object);
        }
        buffer.append("</table>");
    }

    /**
     * returns all the getters of <i>objectClass</i>
     * 
     * @param objectClass
     * @return
     */
    private List<Method> fetchMethod(Class<L> objectClass)
    {

        final List<Method> methods = new ArrayList<Method>(objectClass.getInterfaces().length);
        for (Class<?> i : objectClass.getInterfaces())
        {
            for (Method method : i.getMethods())
            {
                if (method.getName().startsWith("get") || method.getName().startsWith("is"))
                {
                    methods.add(method);
                }
            }
        }
        return methods;
    }

    private void appendRow(List<Method> methods, L object)
    {

        buffer.append("<tr>");
        Method idMethod = null;
        Method nameMethod = null;

        for (Method method : methods)
        {
            String name = StringUtils.uncapitalize(method.getName().substring(3));

            if ("id".equals(name))
                idMethod = method;

            if ("name".equals(name))
                nameMethod = method;
        }

        if (object == null)
        {
            if (idMethod != null)
                appendHeaderCell(idMethod);
            if (nameMethod != null)
                appendHeaderCell(nameMethod);
        }
        else
        {
            if (idMethod != null)
                appendValueCell(idMethod, object);
            if (nameMethod != null)
                appendValueCell(nameMethod, object);
        }

        for (Method method : methods)
        {
            String name = StringUtils.uncapitalize(method.getName().substring(3));
            // table header
            if (!"id".equals(name) && !"name".equals(name))
            {
                if (object == null)
                {
                    appendHeaderCell(method);
                }
                // other rows
                else
                {
                    appendValueCell(method, object);
                }
            }
        }
        buffer.append("</tr>");
    }

    private void appendValueCell(Method method, L object)
    {
        try
        {
            Object invoke = method.invoke(object);
            if (invoke != null)
            {
                if (method.getName().equals("getId"))
                    buffer.append("<td bgcolor='#FFCCCC'>" + invoke.toString());
                else
                    buffer.append("<td>" + invoke.toString());
            }
            else
                buffer.append("<td>null");

            buffer.append("</td>");
        }
        catch (InvocationTargetException e)
        {
            buffer.append("<td>null");
        }
        catch (IllegalAccessException e)
        {
            buffer.append("<td>null");
        }
        catch (RuntimeException e)
        {
            buffer.append("<td>null");
        }
    }

    private void appendHeaderCell(Method method)
    {
        String name = StringUtils.uncapitalize(method.getName().substring(3));
        if ("id".equals(name))
            name = "<td class=\"header\" style=\"font-weight: bold;\">ID";
        else if (method.getName().startsWith("is"))
        {
            name = "<td class=\"header\">"
                    + annotationRender(method, StringUtils.uncapitalize(method.getName().substring(2)));
        }
        else
        {
            name = "<td class=\"header\">" + annotationRender(method, name);
        }

        // name = name.substring(0, 1).toLowerCase() + name.substring(1);
        // if (method.getAnnotation(ServerImmutable.class) != null)
        // name = "<i>" + name + "</i>";
        // if (method.getAnnotation(ClientField.class) != null)
        // name = "<b><i>" + name + "</i></b>";

        buffer.append(name);

        buffer.append("</td>");
    }

    private String annotationRender(Method method, String name)
    {
        String s = name;
        if (method.getAnnotation(ServerImmutable.class) != null)
            s = "<i>" + name + "</i>";
        if (method.getAnnotation(ClientField.class) != null)
            s = "<b><i>" + name + "</i></b>";
        return s;
    }

    @Override
    public String toString()
    {

        return process();
    }
}
