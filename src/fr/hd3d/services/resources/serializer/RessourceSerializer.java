package fr.hd3d.services.resources.serializer;

import java.util.Collection;
import java.util.List;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.utils.Log;


/**
 * Serialize Resources depending on their Variant
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public final class RessourceSerializer
{
    private RessourceSerializer()
    {}

    public static Representation getRepresentation(final Variant variant, final Object object)
    {
        return getRepresentation(variant.getMediaType(), object);
    }

    public static Representation getRepresentation(final MediaType type, final Object object)
    {
        Representation ret;

        if (MediaType.APPLICATION_JSON.equals(type))
        {
            ret = new StringRepresentation(Hd3dJsonSerializer.serialize(object), MediaType.APPLICATION_JSON);
        }
        else if (MediaType.TEXT_HTML.equals(type))
        {
            ret = new StringRepresentation(new HtmlResourceSerializer(object).toString(), MediaType.TEXT_HTML);
        }
        else
        {
            ret = new StringRepresentation(null);
        }

        return ret;
    }

    /**
     * Return the JSON or HTML representation from an object and record count without pagination.
     * 
     * @param <L>
     * @param variant
     * @param object
     *            Object to serialize.
     * @param nbRecords
     *            Number of total records without pagination
     * @return the JSON or HTML representation from object
     */
    public static <L> Representation getRepresentation(final Variant variant, final List<L> object, final int nbRecords)
    {
        return getRepresentation(variant.getMediaType(), object, nbRecords);
    }

    public static <L> Representation getRepresentation(final MediaType type, final List<L> object, final int nbRecords)
    {
        Representation ret;

        /* Chose media type. */
        if (MediaType.APPLICATION_JSON.equals(type))
        {
            /* JSON */
            ret = new StringRepresentation(Hd3dJsonSerializer.serialize(object, nbRecords), MediaType.APPLICATION_JSON);
        }
        else if (MediaType.TEXT_HTML.equals(type))
        {
            /* HTML */
            ret = new StringRepresentation(new HtmlResourceSerializer<L>(object, nbRecords).toString(),
                    MediaType.TEXT_HTML);
        }
        else
        {
            ret = new StringRepresentation(null);
        }

        return ret;
    }

    public static Representation getRepresentation(final MediaType type, final Collection<?> object)
    {
        Representation ret;

        if (MediaType.APPLICATION_JSON.equals(type))
        {
            ret = new StringRepresentation(Hd3dJsonSerializer.serialize(object), MediaType.APPLICATION_JSON);
        }
        else if (MediaType.TEXT_HTML.equals(type))
        {
            ret = new StringRepresentation(new HtmlResourceSerializer(object).toString(), MediaType.TEXT_HTML);
        }
        else
        {
            ret = new StringRepresentation(null);
        }

        return ret;
    }

    public static <L> Representation getRepresentation(final Variant variant, final Collection<L> object,
            final int nbRecords)
    {
        return getRepresentation(variant.getMediaType(), object, nbRecords);
    }

    public static Representation getRepresentation(final MediaType type, final Collection<?> object, final int nbRecords)
    {
        Representation ret;

        if (MediaType.APPLICATION_JSON.equals(type))
        {
            ret = new StringRepresentation(Hd3dJsonSerializer.serialize(object, nbRecords), MediaType.APPLICATION_JSON);
        }
        else if (MediaType.TEXT_HTML.equals(type))
        {
            ret = new StringRepresentation(new HtmlResourceSerializer(object, nbRecords).toString(),
                    MediaType.TEXT_HTML);
        }
        else
        {
            ret = new StringRepresentation(null);
        }

        return ret;
    }

    public static void main(final String[] args)
    {
        Log.LOGGER.info(MediaType.APPLICATION_JSON.toString());
    }
}
