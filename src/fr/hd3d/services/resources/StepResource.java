package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.STEP_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILStep;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class StepResource extends AbstractHd3dResource<ILStep, IStep>
{

    public StepResource() throws Hd3dException
    {
        super(Persistors.step, Translators.step);
    }

    @Override
    protected IStep initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(STEP_ID_ATTRIBUTE, lockMode);
    }
}
