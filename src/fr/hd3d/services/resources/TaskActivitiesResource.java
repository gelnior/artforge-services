package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.persistence.ITaskActivity;


/**
 * A resource to access to specific person activities.
 * 
 * @author HD3D
 */
@Hd3dComment("GET: Retrieve task activities list. <br />\r\n" + "POST: Create a task activity .<br />\r\n")
public class TaskActivitiesResource extends BaseTaskActivitiesResource
{

    public TaskActivitiesResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected void doPostPersist(ITaskActivity object) throws Hd3dException
    {
        if (object != null)
        {
            super.doPostPersist(object);

            this.updateTaskActualStartDate(object);
        }
    }

}
