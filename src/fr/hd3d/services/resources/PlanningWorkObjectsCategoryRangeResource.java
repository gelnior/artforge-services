/**
 * 
 */
package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.common.client.ServicesURI.CATEGORY_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;
import static fr.hd3d.utils.Const.CANONICALNAME_CATEGORY;
import static fr.hd3d.utils.Const.CANONICALNAME_CONSTITUENT;
import static fr.hd3d.utils.Const.JSON_CATEGORYID;
import static fr.hd3d.utils.Const.JSON_CATEGORYID_1;
import static fr.hd3d.utils.Const.JSON_CATEGORYNAME;
import static fr.hd3d.utils.Const.JSON_CHILDS;
import static fr.hd3d.utils.Const.JSON_CONSTITUENTID;
import static fr.hd3d.utils.Const.JSON_CONSTITUENTLABEL;
import static fr.hd3d.utils.Const.JSON_CONSTITUENTS;
import static fr.hd3d.utils.Const.JSON_ENDBRACECURL;
import static fr.hd3d.utils.Const.JSON_ENDBRACECURL_1;
import static fr.hd3d.utils.Const.JSON_ENDBRACECURL_2;
import static fr.hd3d.utils.Const.JSON_ENDDATE;
import static fr.hd3d.utils.Const.JSON_STARTDATE;
import static fr.hd3d.utils.Const.JSON_STARTDATE_1;
import static fr.hd3d.utils.Const.JSON_WORKOBJECTPLANNING;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.CategoryH;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.PlanningUtils;
import fr.hd3d.utils.ServerResourceUtils;


/**
 * @author thomas-eskenazi
 * 
 */
public class PlanningWorkObjectsCategoryRangeResource<L, P> extends AbstractHd3dBaseResource
{
    static final CharSequence FROM = "from ";
    static final String CATEGORYQUERY = FROM + (String) CANONICALNAME_CATEGORY;
    static final String CHUNK_1 = " where parent is null and project.id = ";
    static final String CHUNK_2 = " where parent.id =";
    static final String CHUNK_3 = " order by name";

    static final String CONSTITUENQUERY = FROM + (String) CANONICALNAME_CONSTITUENT;
    static final String CHUNK_4 = " where category.project.id =";
    static final String CHUNK_5 = " and category.id = ";
    static final String CHUNK_6 = " order by label";

    /**
     * @throws Hd3dException
     * 
     */
    public PlanningWorkObjectsCategoryRangeResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Representation get(Variant variant) throws ResourceException
    {
        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();

        List<ICategory> plannedCategories = null;
        List<Long> categoryIDs = null;
        List<IConstituent> plannedConstituents = null;
        try
        {
            Long projectID = getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE);
            Long categoryID = getResourceContext().getIdFromUrl(CATEGORY_ID_ATTRIBUTE);

            StringBuilder queryStr = new StringBuilder(120);
            queryStr.append(CATEGORYQUERY);
            if (categoryID == 0)
            {
                queryStr.append(CHUNK_1).append(projectID);
                // queryCategories = session.createQuery("from " + CANONICALNAME_CATEGORY
                // + " where parent is null and project.id = " + projectID);
            }
            else
            {
                queryStr.append(CHUNK_2).append(categoryID).append(CHUNK_3);
            }
            plannedCategories = session.createQuery(queryStr.toString()).list();
            // ObjectProviderCustoms.filter(plannedCategories);
            categoryIDs = new ArrayList<Long>();
            categoryIDs.add(categoryID);

            ICategory rootCategory = Persistors.category.getById(categoryID);

            StringBuilder childs = new StringBuilder();
            for (ICategory category : plannedCategories)
            {
                Date[] taskLimits = PlanningUtils.getStartandEndDateForCategory(category);
                String startDate = "";
                String endDate = "";
                if (taskLimits[0] != null && taskLimits[1] != null)
                {
                    startDate = taskLimits[0].toString();
                    endDate = taskLimits[1].toString();
                }
                childs.append(JSON_CATEGORYID).append(category.getId()).append(JSON_CATEGORYNAME).append(
                        category.getName()).append(JSON_STARTDATE).append(startDate).append(JSON_ENDDATE).append(
                        endDate).append(JSON_ENDBRACECURL);

                categoryIDs.add(category.getId());
            }
            StringUtils.chomp(childs.toString(), ",");

            StringBuilder plannedConstituentsQueryStr = new StringBuilder(300);
            plannedConstituentsQueryStr.append(CONSTITUENQUERY).append(CHUNK_4).append(projectID).append(CHUNK_5)
                    .append(categoryID).append(CHUNK_6);

            plannedConstituents = session.createQuery(plannedConstituentsQueryStr.toString()).list();
            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            tx.commit();
            // ObjectProviderCustoms.filter(plannedConstituents);
            StringBuilder constituents = new StringBuilder();
            for (IConstituent constituent : plannedConstituents)
            {
                Date[] taskLimits = PlanningUtils.getStartandEndDateForConstituent(constituent);
                String startDate = "";
                String endDate = "";
                if (taskLimits[0] != null && taskLimits[1] != null)
                {
                    startDate = taskLimits[0].toString();
                    endDate = taskLimits[1].toString();
                }
                // constituents += "{\"constituentID\":" + constituent.getId() + ",\"constituentLabel\":\""
                // + constituent.getLabel() + "\",\"categoryID\":" + constituent.getCategory().getId()
                // + ",\"startDate\":\"" + startDate + "\",\"endDate\":\"" + endDate + "\"},";

                constituents.append(JSON_CONSTITUENTID).append(constituent.getId()).append(JSON_CONSTITUENTLABEL)
                        .append(constituent.getLabel()).append(JSON_CATEGORYID_1).append(
                                constituent.getCategory().getId()).append(JSON_STARTDATE_1).append(startDate).append(
                                JSON_ENDDATE).append(endDate).append(JSON_ENDBRACECURL);
            }
            StringUtils.chomp(constituents.toString(), ",");

            if (rootCategory == null)
            {
                rootCategory = new CategoryH();
                rootCategory.setId(0L);
                rootCategory.setName("root");
                rootCategory.setChildren(plannedCategories);
            }
            Date[] taskLimits = PlanningUtils.getStartandEndDateForCategory(rootCategory);
            String startDate = "";
            String endDate = "";
            if (taskLimits[0] != null && taskLimits[1] != null)
            {
                startDate = taskLimits[0].toString();
                endDate = taskLimits[1].toString();
            }
            StringBuilder result = new StringBuilder(300);
            result.append(JSON_WORKOBJECTPLANNING).append(JSON_CATEGORYID).append(rootCategory.getId()).append(
                    JSON_CATEGORYNAME).append(rootCategory.getName()).append(JSON_STARTDATE).append(startDate).append(
                    JSON_ENDDATE).append(endDate).append(JSON_CHILDS).append(childs).append(JSON_CONSTITUENTS).append(
                    constituents).append(JSON_ENDBRACECURL_1).append(JSON_ENDBRACECURL_2);

            Representation r = new StringRepresentation(result.toString());
            r.setCharacterSet(CharacterSet.UTF_8);
            getResponse().setStatus(Status.SUCCESS_OK);
            Log.LOGGER.info(responseInfo());
            getResponse().setEntity(ServerResourceUtils.encodeRepresentation(r));
            return getResponseEntity();
        }
        catch (JDBCException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, "Unable to connect to database");
            return getResponseEntity();
        }
        catch (HibernateException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
            return getResponseEntity();
        }
        catch (Exception e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
            return getResponseEntity();
        }
        finally
        {
            if (plannedCategories != null)
            {
                plannedCategories.clear();
                plannedCategories = null;
            }
            if (categoryIDs != null)
            {
                categoryIDs.clear();
                categoryIDs = null;
            }
            if (plannedConstituents != null)
            {
                plannedConstituents.clear();
                plannedConstituents = null;
            }

            if (tx != null)
                tx = null;
            cleanup();
        }
    }

    protected String listToArrayStr(List<?> list)
    {
        if (list == null || list.size() == 0)
        {
            return "()";
        }
        return '(' + StringUtils.join(list, ',') + ')';
        // String result = "(";
        // for (Object object : list)
        // {
        // result += object.toString() + ",";
        // }
        // result = result.substring(0, result.length() - 1) + ")";
        // return result;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
