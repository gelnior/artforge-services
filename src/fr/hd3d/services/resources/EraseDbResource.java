package fr.hd3d.services.resources;

import java.util.List;

import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.impl.SessionImpl;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.audit.Audit;
import fr.hd3d.model.audit.UpdatesStack;
import fr.hd3d.model.persistence.impl.hibernate.AbsenceH;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteH;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteTypeH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionLinkH;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.CategoryH;
import fr.hd3d.model.persistence.impl.hibernate.ClassDynMetaDataTypeH;
import fr.hd3d.model.persistence.impl.hibernate.CompositionH;
import fr.hd3d.model.persistence.impl.hibernate.ComputerH;
import fr.hd3d.model.persistence.impl.hibernate.ComputerModelH;
import fr.hd3d.model.persistence.impl.hibernate.ComputerTypeH;
import fr.hd3d.model.persistence.impl.hibernate.ConstituentH;
import fr.hd3d.model.persistence.impl.hibernate.ConstituentLinkH;
import fr.hd3d.model.persistence.impl.hibernate.ContractH;
import fr.hd3d.model.persistence.impl.hibernate.DeviceH;
import fr.hd3d.model.persistence.impl.hibernate.DeviceModelH;
import fr.hd3d.model.persistence.impl.hibernate.DeviceTypeH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataTypeH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataValueH;
import fr.hd3d.model.persistence.impl.hibernate.DynamicPathH;
import fr.hd3d.model.persistence.impl.hibernate.EntityTaskLinkH;
import fr.hd3d.model.persistence.impl.hibernate.EventH;
import fr.hd3d.model.persistence.impl.hibernate.ExtraLineH;
import fr.hd3d.model.persistence.impl.hibernate.FactH;
import fr.hd3d.model.persistence.impl.hibernate.FactTypeH;
import fr.hd3d.model.persistence.impl.hibernate.FileAttributeH;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionLinkH;
import fr.hd3d.model.persistence.impl.hibernate.GraphicalAnnotationH;
import fr.hd3d.model.persistence.impl.hibernate.ItemGroupH;
import fr.hd3d.model.persistence.impl.hibernate.ItemH;
import fr.hd3d.model.persistence.impl.hibernate.LicenseH;
import fr.hd3d.model.persistence.impl.hibernate.ManufacturerH;
import fr.hd3d.model.persistence.impl.hibernate.MileStoneH;
import fr.hd3d.model.persistence.impl.hibernate.MountPointH;
import fr.hd3d.model.persistence.impl.hibernate.OrganizationH;
import fr.hd3d.model.persistence.impl.hibernate.PersonDayH;
import fr.hd3d.model.persistence.impl.hibernate.PersonH;
import fr.hd3d.model.persistence.impl.hibernate.PlanningH;
import fr.hd3d.model.persistence.impl.hibernate.PlayListRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.PlayListRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.PoolH;
import fr.hd3d.model.persistence.impl.hibernate.ProcessorH;
import fr.hd3d.model.persistence.impl.hibernate.ProjectH;
import fr.hd3d.model.persistence.impl.hibernate.ProxyH;
import fr.hd3d.model.persistence.impl.hibernate.ProxyTypeH;
import fr.hd3d.model.persistence.impl.hibernate.QualificationH;
import fr.hd3d.model.persistence.impl.hibernate.ResourceGroupH;
import fr.hd3d.model.persistence.impl.hibernate.RoleH;
import fr.hd3d.model.persistence.impl.hibernate.RoomH;
import fr.hd3d.model.persistence.impl.hibernate.ScreenH;
import fr.hd3d.model.persistence.impl.hibernate.ScreenModelH;
import fr.hd3d.model.persistence.impl.hibernate.ScriptH;
import fr.hd3d.model.persistence.impl.hibernate.SequenceH;
import fr.hd3d.model.persistence.impl.hibernate.SheetH;
import fr.hd3d.model.persistence.impl.hibernate.ShotH;
import fr.hd3d.model.persistence.impl.hibernate.SimpleActivityH;
import fr.hd3d.model.persistence.impl.hibernate.SkillH;
import fr.hd3d.model.persistence.impl.hibernate.SkillLevelH;
import fr.hd3d.model.persistence.impl.hibernate.SoftwareH;
import fr.hd3d.model.persistence.impl.hibernate.StepH;
import fr.hd3d.model.persistence.impl.hibernate.StudioMapH;
import fr.hd3d.model.persistence.impl.hibernate.TagH;
import fr.hd3d.model.persistence.impl.hibernate.TagParent;
import fr.hd3d.model.persistence.impl.hibernate.TaskActivityH;
import fr.hd3d.model.persistence.impl.hibernate.TaskChangesH;
import fr.hd3d.model.persistence.impl.hibernate.TaskGroupH;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.model.persistence.impl.hibernate.TaskTypeH;
import fr.hd3d.model.persistence.impl.hibernate.WorkingTimeH;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.model.preferences.Setting;
import fr.hd3d.model.preferences.SheetFilter;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.services.security.utils.AuthorizationUtil;
import fr.hd3d.utils.HibernateUtil;


/**
 * Removes anything from DB
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("Efface toutes les données de la base. <br /> "
        + "Dans certains cas, il est plus efficade de dropper les tables directement au niveau de la base de données.")
public class EraseDbResource<L, P> extends AbstractHd3dBaseResource
{
    public EraseDbResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    private static void run(IHibernateTransaction tx) throws Hd3dPersistenceException, ConstraintViolationException
    {
        Session session = HibernateUtil.currentSession();
        Transaction trx = session.beginTransaction();
        try
        {
            HibernateUtil.safeTransaction(tx);
            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            trx.commit();
            trx = null;
        }
        finally
        {
            HibernateUtil.closeSession();
        }
    }

    private static void purge(Class<?> clazz)
    {
        //
        try
        {
            Session session = HibernateUtil.currentSession();
            // final String className = clazz.getCanonicalName();
            // Log.logger.debug("purging {}", className);
            FullTextSession fullTextSession = Search.getFullTextSession(session);
            org.hibernate.Transaction luceneTx = fullTextSession.beginTransaction();
            fullTextSession.purgeAll(clazz);
            luceneTx.commit();
            luceneTx = null;
            fullTextSession.clear();
            ((SessionImpl) session).getActionQueue().executeActions();
            fullTextSession.close();
            fullTextSession = null;
        }
        catch (IllegalArgumentException e)
        {
            // TODO: handle exception
        }
    }

    private static void processBulk(Class<?> clazz) throws ConstraintViolationException, Hd3dPersistenceException
    {
        Query q = HibernateUtil.currentSession().createSQLQuery("SET FOREIGN_KEY_CHECKS = 0");
        q.executeUpdate();
        run(new BulkEraseDbCmd(clazz));
        purge(clazz);
    }

    private static void processBulk(String tableName) throws ConstraintViolationException, Hd3dPersistenceException
    {
        run(new BulkEraseDbCmd(tableName));
    }

    @Override
    public Representation get(Variant arg0)
    {
        try
        {
            // disable constraint check
            Query q = HibernateUtil.currentSession().createSQLQuery("SET FOREIGN_KEY_CHECKS = 0");
            q.executeUpdate();

            q = HibernateUtil.currentSession().createSQLQuery("unlock tables");
            q.executeUpdate();

            // delete in THIS ORDER (due to constraints)
            BaseH.CLASSDYNMETADATATYPES_MAP.clear();
            processBulk(Audit.class);// no purge
            processBulk(Setting.class);
            processBulk(UpdatesStack.class);
            processBulk(SheetFilter.class);
            processBulk(TagParent.class);
            processBulk(TagH.class);
            processBulk(ScriptH.class);
            processBulk(ClassDynMetaDataTypeH.class);
            processBulk(DynMetaDataValueH.class);
            processBulk(DynMetaDataTypeH.class);
            processBulk(EventH.class);
            processBulk(MileStoneH.class);
            processBulk(EntityTaskLinkH.class);
            processBulk(AbsenceH.class);
            processBulk(ItemH.class);
            processBulk(ItemGroupH.class);
            processBulk(SheetH.class);
            processBulk(FactTypeH.class);
            processBulk(FactH.class);
            processBulk(StudioMapH.class);
            processBulk(PoolH.class);
            processBulk(RoomH.class);
            processBulk(ComputerModelH.class);
            processBulk(ComputerTypeH.class);
            processBulk(ProcessorH.class);
            processBulk(ComputerModelH.class);
            processBulk(DeviceTypeH.class);
            processBulk(DeviceModelH.class);
            processBulk(ManufacturerH.class);
            processBulk(ScreenModelH.class);
            processBulk(DeviceH.class);
            processBulk(ScreenH.class);
            processBulk(LicenseH.class);
            processBulk(SoftwareH.class);
            processBulk(ComputerH.class);
            processBulk("hd3d_computer__device");
            processBulk("hd3d_computer__license");
            processBulk("hd3d_computer__pool");
            processBulk("hd3d_computer__screen");
            processBulk("hd3d_computer__software");

            processBulk(OrganizationH.class);
            processBulk(SkillH.class);
            processBulk(SkillLevelH.class);
            processBulk(ContractH.class);
            processBulk(QualificationH.class);
            processBulk(FileRevisionLinkH.class);
            processBulk(AssetRevisionH.class);
            processBulk(FileAttributeH.class);
            processBulk(FileRevisionH.class);
            processBulk(AssetRevisionGroupH.class);
            processBulk(ConstituentLinkH.class);
            processBulk(CompositionH.class);
            processBulk(ConstituentH.class);
            processBulk(ShotH.class);
            processBulk(CategoryH.class);
            processBulk(SequenceH.class);
            processBulk(ExtraLineH.class);
            processBulk(TaskGroupH.class);
            processBulk(TaskChangesH.class);
            processBulk(PlanningH.class);
            processBulk(TaskActivityH.class);
            processBulk(SimpleActivityH.class);
            processBulk(PersonDayH.class);
            processBulk(TaskTypeH.class);
            processBulk(WorkingTimeH.class);
            processBulk(TaskH.class);
            processBulk(ApprovalNoteH.class);
            processBulk(ApprovalNoteTypeH.class);
            processBulk(TaskTypeH.class);
            processBulk(PersonH.class);
            processBulk(PlayListRevisionH.class);
            processBulk(PlayListRevisionGroupH.class);
            processBulk(RoleH.class);
            processBulk("hd3d_resourcegroup__project");
            processBulk("hd3d_resourcegroup__organization");
            processBulk("hd3d_resourcegroup__resource");
            processBulk("hd3d_resourcegroup__role");
            processBulk("hd3d_organization__resource");
            processBulk(DynamicPathH.class);
            processBulk(ResourceGroupH.class);
            processBulk(ProjectH.class);
            processBulk(AssetRevisionLinkH.class);
            // processBulk(AssetRevisionFileRevisionH.class);
            processBulk("hd3d_assetrevisiongroup__assetrevision");
            processBulk(StepH.class);
            processBulk(GraphicalAnnotationH.class);
            processBulk(DynMetaDataTypeH.class);

            processBulk(MountPointH.class);
            processBulk(ProxyTypeH.class);
            processBulk(ProxyH.class);

            run(new EraseDbCmd(Object.class));
            purge(Object.class);

            HibernateUtil.closeSession();
        }
        catch (Hd3dPersistenceException e)
        {
            e.printStackTrace();
        }
        catch (HibernateException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            AuthorizationUtil.clearCache();
            AuthenticationUtil.clearRegisteredTokens();
            AuthorizationUtil.initSecurityTemplates();
            AuthenticationUtil.initSuperUser();
        }

        return new StringRepresentation("database erased...");
    }

    private static class BulkEraseDbCmd implements IHibernateTransaction
    {
        public Class<?> clazz;
        public String tableName;

        public BulkEraseDbCmd(Class<?> clazz)
        {
            this.clazz = clazz;
        }

        public BulkEraseDbCmd(String tableName)
        {
            this.tableName = tableName;
        }

        public void execute(Session session) throws Hd3dException, HibernateException
        {
            delete(session);
        }

        private void delete(Session session)
        {
            // bulk delete
            Query query;
            String hql = "delete from ";
            if (clazz != null)
            {
                hql += clazz.getCanonicalName();
                query = session.createQuery(hql);
                query.executeUpdate();
                query = null;
            }
            else if (tableName != null)
            {
                hql += tableName;
                query = session.createSQLQuery(hql);
                query.executeUpdate();
                query = null;
            }
        }
    }

    private static class EraseDbCmd implements IHibernateTransaction
    {
        Class<?> clazz;

        public EraseDbCmd(Class<?> clazz)
        {
            this.clazz = clazz;
        }

        public void execute(Session session) throws Hd3dException
        {
            delete(session, clazz);
        }

        private void delete(Session session, Class<?> clazz)
        {
            List<?> objects = session.createCriteria(clazz).list();
            for (Object object : objects)
            {
                session.delete(object);
            }

            objects.clear();
            objects = null;
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
