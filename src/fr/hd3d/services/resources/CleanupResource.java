package fr.hd3d.services.resources;

import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.utils.HibernateUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class CleanupResource<L, P> extends AbstractHd3dBaseResource
{

    public CleanupResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    public Representation get(Variant arg0)
    {
        HibernateUtil.closeSession();
        // HibernateUtil.cleanupThreadLocals(null, "cglib", Thread.currentThread().getContextClassLoader());
        return new StringRepresentation("cleanup done...");
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
