package fr.hd3d.services.resources;

import static fr.hd3d.common.client.Const.JAVA_LANG_BOOLEAN;
import static fr.hd3d.common.client.Const.JAVA_LANG_LONG;
import static fr.hd3d.common.client.Const.JAVA_LANG_STRING;
import static fr.hd3d.common.client.Const.JAVA_UTIL_DATE;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.Entity;
import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.client.Sheet;
import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILClassDynMetaDataType;
import fr.hd3d.model.lightweight.ILDynMetaDataType;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.impl.LApprovalNoteType;
import fr.hd3d.model.lightweight.impl.LCategory;
import fr.hd3d.model.lightweight.impl.LClassDynMetaDataType;
import fr.hd3d.model.lightweight.impl.LComposition;
import fr.hd3d.model.lightweight.impl.LConstituent;
import fr.hd3d.model.lightweight.impl.LDynMetaDataType;
import fr.hd3d.model.lightweight.impl.LItem;
import fr.hd3d.model.lightweight.impl.LItemGroup;
import fr.hd3d.model.lightweight.impl.LPerson;
import fr.hd3d.model.lightweight.impl.LPersonDay;
import fr.hd3d.model.lightweight.impl.LProject;
import fr.hd3d.model.lightweight.impl.LResourceGroup;
import fr.hd3d.model.lightweight.impl.LSequence;
import fr.hd3d.model.lightweight.impl.LSheet;
import fr.hd3d.model.lightweight.impl.LShot;
import fr.hd3d.model.lightweight.impl.LSimpleActivity;
import fr.hd3d.model.lightweight.impl.LTaskActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IResource;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.EntityTaskLinkH;
import fr.hd3d.model.persistence.impl.hibernate.PersonDayH;
import fr.hd3d.model.persistence.impl.hibernate.TagH;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.model.persistence.impl.hibernate.TaskTypeH;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.auth.FDTAuthUtils;
import fr.hd3d.services.resources.collectionquery.TaskBoundEntityApprovalNotesCollectionQuery;
import fr.hd3d.services.resources.collectionquery.TaskBoundEntityCollectionQuery;
import fr.hd3d.services.resources.collectionquery.TaskDurationCollectionQuery;
import fr.hd3d.services.resources.collectionquery.TaskWorkersCollectionQuery;
import fr.hd3d.services.security.dao.IUserAccount;
import fr.hd3d.services.security.dao.UserAccountH;
import fr.hd3d.utils.Bench;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


/**
 * Test class to initialize DB with some "Pat et Stanley" production values.
 * 
 * @author Nicolas Provost
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("Initialise la base de données avec le jeu de tests PAT & STAN.")
public class InitializePastaResource<L, P> extends AbstractHd3dBaseResource
{
    static final String PAT_ET_STAN = "Pat et Stanley";

    static final String MODELING = "modeling";
    static final String DESIGN = "design";
    static final String MATTEPAINTING = "MattePainting";
    static final String SETUP = "setup";
    static final String TEXTURING = "texturing";
    static final String SHADING = "shading";
    static final String HAIRS = "hairs";
    static final String LAYOUT = "layout";
    static final String ANIMATION = "animation";
    static final String EXPORT = "export";
    static final String TRACKING = "tracking";
    static final String DRESSING = "dressing";
    static final String LIGHTING = "lighting";
    static final String RENDERING = "rendering";
    static final String COMPOSITING = "compositing";

    private static String cap(String s)
    {
        if (s == null)
            return null;
        return WordUtils.capitalize(s);
    }

    /**
     * Default Constructor
     * 
     * @param context
     *            Context parameters
     * @param request
     *            Request received
     * @param response
     *            Response to send
     * @throws Hd3dException
     */
    public InitializePastaResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    private static void run(IHibernateTransaction tx) throws Hd3dPersistenceException
    {
        Bench b = new Bench();
        b.b();
        Session session = HibernateUtil.currentSession();
        Transaction trx = session.beginTransaction();
        HibernateUtil.safeTransaction(tx);
        if (session.getFlushMode() == FlushMode.MANUAL)
        {
            session.flush();
        }
        trx.commit();
        trx = null;
        HibernateUtil.closeSession();

        Log.LOGGER.debug("{} took: {} ms", tx.getClass().getName(), b.e());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.restlet.resource.Resource#getRepresentation(org.restlet.resource. Variant)
     */
    @Override
    public Representation get(Variant arg0)
    {

        try
        {
            BaseH.init();

            run(new InitDbCmd());

            // Library tree
            run(new InitLibraryStructure());
            run(new InitLibraryContent());

            // temporal tree
            run(new InitTemporalStructure());
            run(new InitTemporalContent());

            // casting data
            run(new InitCasting());

            // task data
            run(new InitPlanning());

            // Suivi data
            run(new InitSuivi());

        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
            return new StringRepresentation(e.getMessage());
        }
        return new StringRepresentation("database pastafied...");
    }

    /**
     * Database Initialization : persons, projects, tasks, activities and days
     * 
     * @author Nicolas Provost
     */
    private class InitDbCmd implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            // Projects
            final IProject projectPasta = getProject(session, PAT_ET_STAN, EProjectStatus.OPEN, "#80FF80", "PASTA");
            // Persons
            final IPerson jgo = getPerson(session, "jgo", FDTAuthUtils.encrypt("jgopass"), "Jerome", "Gonnon");
            final IPerson gch = getPerson(session, "gch", FDTAuthUtils.encrypt("gchpass"), "Guillaume", "Chatelet");
            final IPerson eth = getPerson(session, "eth", FDTAuthUtils.encrypt("ethpass"), "Thomas", "Eskénazi");
            final IPerson acs = getPerson(session, "acs", FDTAuthUtils.encrypt("acspass"), "Arnaud", "Chassagne");
            final IPerson npr = getPerson(session, "npr", FDTAuthUtils.encrypt("nprpass"), "Nicolas", "Provost");
            final IPerson dga = getPerson(session, "dga", FDTAuthUtils.encrypt("dgapass"), "David", "Gauthier");
            final IPerson gdl = getPerson(session, "gdl", FDTAuthUtils.encrypt("gdlpass"), "Georges", "De Luca");
            final IPerson agm = getPerson(session, "agm", FDTAuthUtils.encrypt("agmpass"), "Anne-Laure",
                    "George-Molland");
            final IPerson tri = getPerson(session, "tri", FDTAuthUtils.encrypt("tripass"), "Try", "Lam");
            final IPerson frr = getPerson(session, "frr", FDTAuthUtils.encrypt("frrpass"), "Frank", "Rousseau");
            final IPerson cha = getPerson(session, "cha", FDTAuthUtils.encrypt("chapass"), "Christophe", "Archambault");
            final IPerson vin = getPerson(session, "vin", FDTAuthUtils.encrypt("vinpass"), "Vincent", "Lefort");
            final IPerson bem = getPerson(session, "bem", FDTAuthUtils.encrypt("bempass"), "Benoît", "Maujean");
            final IPerson jpb = getPerson(session, "jpb", FDTAuthUtils.encrypt("jpbpass"), "Jean-Pierre", "Boiget");
            final IPerson mho = getPerson(session, "mho", FDTAuthUtils.encrypt("mhopass"), "Marie", "Holzer");
            final IPerson frc = getPerson(session, "frc", FDTAuthUtils.encrypt("frcpass"), "Franck", "Chopin");
            final IPerson lal = getPerson(session, "lal", FDTAuthUtils.encrypt("lalpass"), "Laurent", "Alt");
            final IPerson bln = getPerson(session, "bln", FDTAuthUtils.encrypt("blnpass"), "Blandine", "Nicolas");
            final IPerson jnp = getPerson(session, "jnp", FDTAuthUtils.encrypt("jnppass"), "Jean-Noël", "Portugal");

            appendStaffToProject(projectPasta, jgo, npr, gch, eth, acs, dga, gdl, agm, tri, frr, cha, vin, bem, jpb,
                    mho, frc, lal, bln, jnp);

            projectPasta.doPreUpdate();
            session.update(projectPasta);
            session.flush();
            projectPasta.doPostUpdate();

            // Groups
            getResourceGroup(session, cap(MODELING));
            getResourceGroup(session, cap(DESIGN));
            getResourceGroup(session, cap(MATTEPAINTING));
            getResourceGroup(session, cap(SETUP));
            getResourceGroup(session, cap(TEXTURING));
            getResourceGroup(session, cap(SHADING));
            getResourceGroup(session, cap(HAIRS));
            getResourceGroup(session, cap(LAYOUT));
            getResourceGroup(session, cap(ANIMATION));
            getResourceGroup(session, cap(EXPORT));
            getResourceGroup(session, cap(TRACKING));
            getResourceGroup(session, cap(DRESSING));
            getResourceGroup(session, cap(LIGHTING));
            getResourceGroup(session, cap(RENDERING));
            getResourceGroup(session, cap(COMPOSITING));

            HashSet<IResource> persons = new HashSet<IResource>();
            persons.add(jgo);
            persons.add(gch);
            persons.add(dga);
            persons.add(frc);
            final IResourceGroup groupA = getResourceGroup(session, cap(MODELING));
            groupA.setResources(persons);
            groupA.doPreUpdate();
            session.update(groupA);
            session.flush();
            groupA.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(jnp);
            persons.add(bem);
            persons.add(bln);
            persons.add(npr);
            final IResourceGroup groupB = getResourceGroup(session, cap(DESIGN));
            groupB.setResources(persons);
            groupB.doPreUpdate();
            session.update(groupB);
            session.flush();
            groupB.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(agm);
            persons.add(frr);
            persons.add(npr);
            persons.add(tri);
            final IResourceGroup groupC = getResourceGroup(session, cap(MATTEPAINTING));
            groupC.setResources(persons);
            groupC.doPreUpdate();
            session.update(groupC);
            session.flush();
            groupC.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(mho);
            persons.add(frc);
            persons.add(lal);
            persons.add(tri);
            final IResourceGroup groupD = getResourceGroup(session, cap(SETUP));
            groupD.setResources(persons);
            groupD.doPreUpdate();
            session.update(groupD);
            session.flush();
            groupD.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(jgo);
            persons.add(gch);
            final IResourceGroup groupE = getResourceGroup(session, cap(TEXTURING));
            groupE.setResources(persons);
            groupE.doPreUpdate();
            session.update(groupE);
            session.flush();
            groupE.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(frc);
            persons.add(lal);
            persons.add(cha);
            persons.add(eth);
            persons.add(vin);
            persons.add(jnp);
            final IResourceGroup groupF = getResourceGroup(session, cap(SHADING));
            groupF.setResources(persons);
            groupF.doPreUpdate();
            session.update(groupF);
            session.flush();
            groupF.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(bln);
            persons.add(npr);
            persons.add(tri);
            final IResourceGroup groupG = getResourceGroup(session, cap(HAIRS));
            groupG.setResources(persons);
            groupG.doPreUpdate();
            session.update(groupG);
            session.flush();
            groupG.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(dga);
            persons.add(gdl);
            persons.add(agm);
            persons.add(npr);
            persons.add(tri);
            final IResourceGroup groupH = getResourceGroup(session, cap(LAYOUT));
            groupH.setResources(persons);
            groupH.doPreUpdate();
            session.update(groupH);
            session.flush();
            groupH.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(gdl);
            persons.add(agm);
            persons.add(frr);
            persons.add(bem);
            persons.add(tri);
            final IResourceGroup groupI = getResourceGroup(session, cap(ANIMATION));
            groupI.setResources(persons);
            groupI.doPreUpdate();
            session.update(groupI);
            session.flush();
            groupI.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(jgo);
            persons.add(gch);
            persons.add(cha);
            persons.add(jnp);
            final IResourceGroup groupJ = getResourceGroup(session, cap(EXPORT));
            groupJ.setResources(persons);
            groupJ.doPreUpdate();
            session.update(groupJ);
            session.flush();
            groupJ.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(frr);
            persons.add(tri);
            final IResourceGroup groupK = getResourceGroup(session, cap(TRACKING));
            groupK.setResources(persons);
            groupK.doPreUpdate();
            session.update(groupK);
            session.flush();
            groupK.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(gch);
            persons.add(dga);
            persons.add(gdl);
            persons.add(agm);
            persons.add(frr);
            final IResourceGroup groupL = getResourceGroup(session, cap(DRESSING));
            groupL.setResources(persons);
            groupL.doPreUpdate();
            session.update(groupL);
            session.flush();
            groupL.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(mho);
            persons.add(frc);
            persons.add(lal);
            persons.add(cha);
            final IResourceGroup groupM = getResourceGroup(session, cap(LIGHTING));
            groupM.setResources(persons);
            groupM.doPreUpdate();
            session.update(groupM);
            session.flush();
            groupM.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(cha);
            persons.add(eth);
            persons.add(vin);
            persons.add(jnp);
            persons.add(bem);
            final IResourceGroup groupN = getResourceGroup(session, cap(RENDERING));
            groupN.setResources(persons);
            groupN.doPreUpdate();
            session.update(groupN);
            session.flush();
            groupN.doPostUpdate();

            persons = new HashSet<IResource>();
            persons.add(acs);
            persons.add(jpb);
            persons.add(bln);
            persons.add(npr);
            persons.add(tri);
            final IResourceGroup groupO = getResourceGroup(session, cap(COMPOSITING));
            groupO.setResources(persons);
            groupO.doPreUpdate();
            session.update(groupO);
            groupO.doPostUpdate();

            // Refresh data
            session.flush();
            session.refresh(jgo);
            session.refresh(npr);
            session.refresh(eth);
            session.refresh(gch);
            session.refresh(acs);
            session.refresh(dga);
            session.refresh(gdl);
            session.refresh(agm);
            session.refresh(tri);
            session.refresh(frr);
            session.refresh(cha);
            session.refresh(vin);
            session.refresh(bem);
            session.refresh(jpb);
            session.refresh(mho);
            session.refresh(frc);
            session.refresh(lal);
            session.refresh(bln);
            session.refresh(jnp);
            session.refresh(projectPasta);

            createInitialTaskTypes(session);

            // Commit
            session.flush();
        }

        // private void save(ITaskType object) throws Hd3dPersistenceException
        // {
        // object.doPrePersist();
        // session.save(object);
        // session.flush();
        // object.doPostPersist();
        // }
    }

    /**
     * LIBRARY : creates categories and sub categories for Pasta
     */
    private class InitLibraryStructure implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            this.session = session;
            final IProject projectPasta = Persistors.project.getObjectByValue_NoPermCheck("name", PAT_ET_STAN);
            final Long projectPastaId = projectPasta.getId();

            ICategory constituent = projectPasta.rootCategory();
            final Long parentId = constituent.getId();

            // Characters categories
            ICategory chars = getCategory(this.session, null, null, "CHARS", parentId, projectPastaId);
            projectPasta.addCategory(chars);
            projectPasta.doPreUpdate();
            session.update(projectPasta);
            session.flush();
            projectPasta.doPostUpdate();
            session.refresh(chars);

            final Long charsId = chars.getId();
            ICategory main = getCategory(this.session, null, null, "MAIN", charsId, projectPastaId);
            projectPasta.addCategory(main);
            projectPasta.doPreUpdate();
            session.update(projectPasta);
            session.flush();
            projectPasta.doPostUpdate();
            session.refresh(main);

            ICategory others = getCategory(this.session, null, null, "OTHERS", charsId, projectPastaId);
            projectPasta.addCategory(others);
            projectPasta.doPreUpdate();
            session.update(projectPasta);
            session.flush();
            projectPasta.doPostUpdate();
            session.refresh(others);

            // Props categories
            ICategory props = getCategory(this.session, null, null, "PROPS", parentId, projectPastaId);
            projectPasta.addCategory(props);
            projectPasta.doPreUpdate();
            session.update(projectPasta);
            session.flush();
            projectPasta.doPostUpdate();
            session.refresh(props);

            // SFX categories
            ICategory sfx = getCategory(this.session, null, null, "SFX", parentId, projectPastaId);
            projectPasta.addCategory(sfx);
            projectPasta.doPreUpdate();
            session.update(projectPasta);
            session.flush();
            projectPasta.doPostUpdate();
            session.refresh(sfx);
            ICategory sets = getCategory(this.session, null, null, "SETS", parentId, projectPastaId);
            projectPasta.addCategory(sets);
            projectPasta.doPreUpdate();
            session.update(projectPasta);
            session.flush();
            projectPasta.doPostUpdate();
            session.refresh(sets);

            final Long setsId = sets.getId();
            ICategory home = getCategory(this.session, null, null, "HOMEPATSTAN", setsId, projectPastaId);
            projectPasta.addCategory(home);
            projectPasta.doPreUpdate();
            session.update(projectPasta);
            session.flush();
            projectPasta.doPostUpdate();
            session.refresh(home);
        }
    }

    /**
     * creates constituents for Pasta
     */
    private class InitLibraryContent implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            final IProject projectPasta = Persistors.project.getObjectByValue_NoPermCheck("hook", "PASTA");
            final Long projectPastaId = projectPasta.getId();
            Long categoryId = null;

            // Chars constituents (main)
            categoryId = retrieveCategoryId(projectPastaId, "MAIN");

            createConstituent(session, categoryId, "PAT", "Patrick, l'hippo", 3);
            createConstituent(session, categoryId, "STAN", "Stanley, le chien", 2);
            createConstituent(session, categoryId, "JEANLUC", "Jean-Luc, le paresseux", 1);
            createConstituent(session, categoryId, "CHICHI", "Le professeur Chichi, savant fou", 1);
            createConstituent(session, categoryId, "STEPHANIE", "Stéphanie, reine de beauté", 1);
            createConstituent(session, categoryId, "EMILIE", "Emilie, la taupe", 1);
            createConstituent(session, categoryId, "MARTHE", "L'inénarable tante Marthe", 1);

            categoryId = retrieveCategoryId(projectPastaId, "PROPS");
            IConstituent mug = getConstituent(session, null, categoryId, null, 0, "Mug à café (avec des os)", 0, "MUG",
                    "MUG", 0, null);
            session.flush();
            session.refresh(mug);
            createTask(session, "Design_MUG", 0, mug.getId());
            createTask(session, "Modeling_MUG", 0, mug.getId());
            createTask(session, "Setup_MUG", 0, mug.getId());

            IConstituent calendrier = getConstituent(session, null, categoryId, null, 0,
                    "Calendrier des activités de Stanley", 1, "CALENDRIER", "CALENDRIER", 0, null);
            session.flush();
            session.refresh(calendrier);
            createTask(session, "Design_CALENDRIER", 1, calendrier.getId());
            createTask(session, "Modeling_CALENDRIER", 1, calendrier.getId());
            createTask(session, "Setup_CALENDRIER", 1, calendrier.getId());

            IConstituent clock = getConstituent(session, null, categoryId, null, 0, "Réveil-matin de Pat", 0, "REVEIL",
                    "REVEIL", 0, null);
            session.flush();
            session.refresh(clock);
            createTask(session, "Design_REVEIL", 0, clock.getId());
            createTask(session, "Modeling_REVEIL", 0, clock.getId());
            createTask(session, "Setup_REVEIL", 0, clock.getId());

            IConstituent carA = getConstituent(session, null, categoryId, null, 0, "Véhicule individuel, rouge", 0,
                    "VOITURE1", "VOITURE1", 0, null);
            session.flush();
            session.refresh(carA);
            createTask(session, "Design_VOITURE1", 0, carA.getId());
            createTask(session, "Modeling_VOITURE1", 0, carA.getId());
            createTask(session, "Setup_VOITURE1", 0, carA.getId());

            IConstituent carB = getConstituent(session, null, categoryId, null, 0, "Véhicule pour transport en commun",
                    0, "VOITURE2", "VOITURE2", 0, null);
            session.flush();
            session.refresh(carB);
            createTask(session, "Design_VOITURE2", 0, carB.getId());
            createTask(session, "Modeling_VOITURE2", 0, carB.getId());
            createTask(session, "Setup_VOITURE2", 0, carB.getId());

            // Sets constituents
            categoryId = retrieveCategoryId(projectPastaId, "HOMEPATSTAN");
            IConstituent cuisine = getConstituent(session, null, categoryId, null, 0, "Cuisine", 3, "CUISINE",
                    "CUISINE", 0, null);
            session.flush();
            session.refresh(cuisine);
            createTask(session, "Design_CUISINE", 3, cuisine.getId());
            createTask(session, "Modeling_CUISINE", 3, cuisine.getId());
            createTask(session, "Setup_CUISINE", 3, cuisine.getId());

            IConstituent entree = getConstituent(session, null, categoryId, null, 0, "Entrée de la maison", 3,
                    "ENTREE", "ENTREE", 0, null);
            session.flush();
            session.refresh(entree);
            createTask(session, "Design_ENTREE", 3, entree.getId());
            createTask(session, "Modeling_ENTREE", 3, entree.getId());
            createTask(session, "Setup_ENTREE", 3, entree.getId());

            IConstituent portesfenetres = getConstituent(session, null, categoryId, null, 0,
                    "Portes-fenêtres du salon", 1, "PORTESFENETRES", "PORTESFENETRES", 0, null);
            session.flush();
            session.refresh(portesfenetres);
            createTask(session, "Design_PORTESFENETRES", 1, portesfenetres.getId());
            createTask(session, "Modeling_PORTESFENETRES", 1, portesfenetres.getId());
            createTask(session, "Setup_PORTESFENETRES", 1, portesfenetres.getId());

            IConstituent salon = getConstituent(session, null, categoryId, null, 0, "Salon", 1, "SALON", "SALON", 2,
                    null);
            session.flush();
            session.refresh(salon);
            createTask(session, "Design_SALON", 1, salon.getId());
            createTask(session, "Modeling_SALON", 1, salon.getId());
            createTask(session, "Setup_SALON", 1, salon.getId());

            IConstituent bedRoom = getConstituent(session, null, categoryId, null, 0, "Chambre à coucher", 1,
                    "CHAMBRE", "CHAMBRE", 2, null);
            session.flush();
            session.refresh(bedRoom);
            createTask(session, "Design_CHAMBRE", 1, bedRoom.getId());
            createTask(session, "Modeling_CHAMBRE", 1, bedRoom.getId());
            createTask(session, "Setup_CHAMBRE", 1, bedRoom.getId());

            // Props constituents (others)
            categoryId = retrieveCategoryId(projectPastaId, "SETS");
            IConstituent home = getConstituent(session, null, categoryId, null, 0, "Maison, vue de l'extérieur", 1,
                    "MAISON", "MAISON", 2, null);
            session.flush();
            session.refresh(home);
            createTask(session, "Design_MAISON", 1, home.getId());
            createTask(session, "Modeling_MAISON", 1, home.getId());
            createTask(session, "Setup_MAISON", 1, home.getId());

            IConstituent rue = getConstituent(session, null, categoryId, null, 0, "Vue de la rue principale", 1,
                    "GRANDRUE", "GRANDRUE", 2, null);
            session.flush();
            session.refresh(rue);
            createTask(session, "Design_GRANDRUE", 1, rue.getId());
            createTask(session, "Modeling_GRANDRUE", 1, rue.getId());
            createTask(session, "Setup_GRANDRUE", 1, rue.getId());

            session.flush();
        }
    }

    /**
     * TEMPORAL : creates sequences and sub subsequences for Pasta
     */
    private class InitTemporalStructure implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            this.session = session;
            final String episodePref = "E";
            final String sequencePref = "S";
            final IProject projectPasta = Persistors.project.getObjectByValue_NoPermCheck("name", PAT_ET_STAN);
            final Long projectPastaId = projectPasta.getId();

            ISequence rootSeq = projectPasta.rootSequence();
            final Long parentId = rootSeq.getId();

            // Episodes / Sequences init parameters
            ISequence currentEp = null;
            ISequence currentSeq = null;
            String epHook = "";
            String epName = "";
            String seqHook = "";
            String seqName = "";
            int numberOfDigitByEp = 4;
            double episodeCard = 5;
            int[] seqInEpisode = { 4, 7, 6, 8, 5, 4, 7, 6, 8, 5, 4, 7, 6, 8, 5, 4, 7, 6, 8, 5 };
            double sequenceCard = 0;
            for (int i = 0; i < episodeCard; i++)
            {
                epHook = headWithZeros(i + 1, numberOfDigitByEp);
                epHook = episodePref + epHook;
                epName = "Episode " + epHook;
                currentEp = getSequence(this.session, null, null, epName, "Titre", epHook, parentId, projectPastaId);
                projectPasta.addSequence(currentEp);
                projectPasta.doPreUpdate();
                session.update(projectPasta);
                session.flush();
                projectPasta.doPostUpdate();
                session.refresh(currentEp);

                int numberOfDigitBySeq = 2;
                sequenceCard = seqInEpisode[i];
                for (int j = 0; j < sequenceCard; j++)
                {
                    seqHook = headWithZeros(j + 1, numberOfDigitBySeq);
                    seqHook = epHook + "_" + sequencePref + seqHook;
                    seqName = "Séquence " + seqHook;
                    currentSeq = getSequence(this.session, null, null, seqName, "Titre", seqHook, currentEp.getId(),
                            projectPastaId);
                    projectPasta.addSequence(currentSeq);
                    projectPasta.doPreUpdate();
                    session.update(projectPasta);
                    session.flush();
                    projectPasta.doPostUpdate();
                    session.refresh(currentSeq);
                }
            }

            session.flush();
        }
    }

    /**
     * creates shots for Pasta
     */
    private class InitTemporalContent implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            final IProject projectPasta = Persistors.project.getObjectByValue_NoPermCheck("name", PAT_ET_STAN);
            Iterator<ISequence> it = projectPasta.getSequences().iterator();

            // ISequence action = null;
            while (it.hasNext())
            {
                ISequence currentSeq = it.next();
                // Episodes are not filled with shots
                if (currentSeq.getParent() != null)
                {
                    ISequence currentParent = currentSeq.getParent();
                    if (currentParent.getParent() != null)
                    {

                        String shotPref = "P";
                        IShot currentShot = null;

                        String shotHook = "";
                        String shotName = "";
                        String seqHook = "";

                        String cuName = "";
                        String cuDescription = "";
                        int numberOfDigitByShot = 3;

                        // TODO : make it tunable (per sequence)
                        final double maxShotCard = 10;
                        double thisShotCard = 1 + Math.floor(Math.random() * (maxShotCard - 1));
                        for (int i = 0; i < thisShotCard; i++)
                        {
                            shotHook = Integer.toString(i + 1);
                            seqHook = currentSeq.getHook();
                            shotName = "Plan " + seqHook;

                            shotHook = headWithZeros(i + 1, numberOfDigitByShot);
                            shotHook = shotPref + shotHook;
                            shotName = "Plan " + shotHook;
                            shotHook = seqHook + "_" + shotHook;

                            currentShot = getShot(session, null, currentSeq.getId(), 0, "shot description ...", 1,
                                    shotHook, shotName, 0, null);

                            // currentShot.doPrePersist();
                            // session.save(currentShot);
                            // session.flush();
                            // currentShot.doPostPersist();
                            session.refresh(currentShot);

                            createTask(session, "MattePainting_" + currentShot.getHook(), currentShot.getId(), "Shot");
                            createTask(session, "Layout_" + currentShot.getHook(), currentShot.getId(), "Shot");
                        }
                    }
                }
            }
            session.flush();
        }
    }

    /**
     * create default sheet for Pasta
     */

    private class InitSuivi implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            final IProject projectPasta = Persistors.project.getObjectByValue_NoPermCheck("name", PAT_ET_STAN);
            final Long PROJECT_PASTA_ID = projectPasta.getId();

            IClassDynMetaDataType stringAttribute = createAttributDynamique(session, "String", Entity.CONSTITUENT,
                    JAVA_LANG_STRING, null);

            // Creation des attributs dynamiques necessaires au suivi
            IClassDynMetaDataType modelingPersonAttribute = createAttributDynamique(session, "Modeling User",
                    Entity.CONSTITUENT, Entity.PERSON, null);
            IClassDynMetaDataType setupPersonAttribute = createAttributDynamique(session, "Setup User",
                    Entity.CONSTITUENT, Entity.PERSON, null);
            IClassDynMetaDataType texturingPersonAttribute = createAttributDynamique(session, "Texturing User",
                    Entity.CONSTITUENT, Entity.PERSON, null);

            IClassDynMetaDataType modelingDateAttribute = createAttributDynamique(session, "Modeling date",
                    Entity.CONSTITUENT, JAVA_UTIL_DATE, "");
            IClassDynMetaDataType setupDateAttribute = createAttributDynamique(session, "Setup date",
                    Entity.CONSTITUENT, JAVA_UTIL_DATE, "");
            IClassDynMetaDataType texturingDateAttribute = createAttributDynamique(session, "Texturing date",
                    Entity.CONSTITUENT, JAVA_UTIL_DATE, "");

            IClassDynMetaDataType modelingReadyAttribute = createAttributDynamique(session, "Modeling ready",
                    Entity.CONSTITUENT, JAVA_LANG_BOOLEAN, "false");
            IClassDynMetaDataType setupReadyAttribute = createAttributDynamique(session, "Setup ready",
                    Entity.CONSTITUENT, JAVA_LANG_BOOLEAN, "false");
            IClassDynMetaDataType texturingReadyAttribute = createAttributDynamique(session, "Texturing ready",
                    Entity.CONSTITUENT, JAVA_LANG_BOOLEAN, "false");

            String animationEstimation = "Estimation";
            IClassDynMetaDataType shotEstimation = createAttributDynamique(session, animationEstimation, Entity.SHOT,
                    JAVA_LANG_LONG, "");
            String shotTechDifficulty = "Difficulté Tech";
            IClassDynMetaDataType shotDifficultyTech = createAttributDynamique(session, shotTechDifficulty,
                    Entity.SHOT, JAVA_LANG_LONG, "");
            String shotStarted = "Started";
            IClassDynMetaDataType shotStartedMetaData = createAttributDynamique(session, shotStarted, Entity.SHOT,
                    JAVA_LANG_BOOLEAN, "false");

            // CREATION DES APPROVALS NOTE TYPE

            // Retrieve task Types
            final Class<?> persistedClass = Persistors.tasktype.getPersistedClass();
            List<?> types = session.createCriteria(persistedClass).add(Restrictions.eq("name", cap(ANIMATION))).list();
            ITaskType typeAnimation = (ITaskType) types.get(0);

            types = session.createCriteria(persistedClass).add(Restrictions.eq("name", cap(MODELING))).list();
            ITaskType typeModeling = (ITaskType) types.get(0);

            types = session.createCriteria(persistedClass).add(Restrictions.eq("name", cap(SETUP))).list();
            ITaskType typeSetup = (ITaskType) types.get(0);

            types = session.createCriteria(persistedClass).add(Restrictions.eq("name", cap(TEXTURING))).list();
            ITaskType typeTexturing = (ITaskType) types.get(0);

            IApprovalNoteType modelingApproval = createApprovalNoteType(session, PROJECT_PASTA_ID,
                    "Validation Modeling", typeModeling.getId());
            IApprovalNoteType setupApproval = createApprovalNoteType(session, PROJECT_PASTA_ID, "Validation Setup",
                    typeSetup.getId());
            IApprovalNoteType texturingApproval = createApprovalNoteType(session, PROJECT_PASTA_ID,
                    "Validation Texturing", typeTexturing.getId());
            IApprovalNoteType animRealApproval = createApprovalNoteType(session, PROJECT_PASTA_ID,
                    "Validation Anim Réal", typeAnimation.getId());
            IApprovalNoteType animTF1Approval = createApprovalNoteType(session, PROJECT_PASTA_ID,
                    "Validation Anim TF1", typeAnimation.getId());

            // /////////////////////////// DEBUT Suivi des constituants /////////////////////////////
            final ISheet sheetConstituantResume = createSheet(session, PROJECT_PASTA_ID, "Validation constituants",
                    "Validation constituants", Entity.CONSTITUENT, ESheetType.PRODUCTION);
            // create group
            final IItemGroup groupInfo = createItemGroup(session, sheetConstituantResume.getId(), "Info");
            final Long GROUPINFO_ID = groupInfo.getId();

            // create Items
            createItem(session, "Label", Sheet.ITEMTYPE_ATTRIBUTE, "label", "", GROUPINFO_ID, Const.JAVA_LANG_STRING,
                    "", Editor.SHORT_TEXT);
            createItem(session, "Thumbnail", Sheet.ITEMTYPE_METHOD, "thumbnail", "", GROUPINFO_ID, JAVA_LANG_STRING,
                    Renderer.THUMBNAIL, "");
            createItem(session, "Workers", Sheet.ITEMTYPE_COLLECTIONQUERY, TaskWorkersCollectionQuery.class
                    .getCanonicalName(), "", GROUPINFO_ID, Const.LIST_OF_PERSONS, Renderer.PERSON, "");

            createTaskStatusItems(session, groupInfo);

            // le group modeling
            final IItemGroup groupModeling = createItemGroup(session, sheetConstituantResume.getId(), cap(MODELING));
            final Long GROUP_MODELING_ID = groupModeling.getId();

            createItem(session, "User", Sheet.ITEMTYPE_METADATA, modelingPersonAttribute.getId().toString(), "",
                    GROUP_MODELING_ID, modelingPersonAttribute.getDynMetaDataType().getType(), "", "");

            // createItem(session, "Activities Duration", Sheet.ITEMTYPE_METADATA, modelingDateAttribute.getId()
            // .toString(), "", groupModeling.getId(), JAVA_UTIL_DATE, "", "");

            createItem(session, "Activities Duration", Sheet.ITEMTYPE_COLLECTIONQUERY,
                    fr.hd3d.services.resources.collectionquery.TaskActivitiesDurationCollectionQuery.class
                            .getCanonicalName(), "", GROUP_MODELING_ID, JAVA_LANG_LONG, Editor.INT, "");

            createItem(session, "Ready For Approval", Sheet.ITEMTYPE_METADATA, modelingReadyAttribute.getId()
                    .toString(), "", GROUP_MODELING_ID, JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Renderer.BOOLEAN);

            IItem item;
            item = createItem(session, modelingApproval.getName(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                    fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery.class.getCanonicalName(),
                    "", GROUP_MODELING_ID, Entity.APPROVALNOTE, Renderer.APPROVAL, "");
            item.setTransformerParameters(modelingApproval.getId().toString());
            item.doPreUpdate();
            session.update(item);
            session.flush();
            item.doPostUpdate();

            // le group setup
            final IItemGroup groupSetup = createItemGroup(session, sheetConstituantResume.getId(), cap(SETUP));
            final Long GROUP_SETUP_ID = groupSetup.getId();

            createItem(session, "User", Sheet.ITEMTYPE_METADATA, setupPersonAttribute.getId().toString(), "",
                    GROUP_SETUP_ID, setupPersonAttribute.getDynMetaDataType().getType(), "", "");
            createItem(session, "Activities Duration", Sheet.ITEMTYPE_METADATA, setupDateAttribute.getId().toString(),
                    "", GROUP_SETUP_ID, JAVA_UTIL_DATE, "", "");
            createItem(session, "Ready For Approval", Sheet.ITEMTYPE_METADATA, setupReadyAttribute.getId().toString(),
                    "", GROUP_SETUP_ID, JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Renderer.BOOLEAN);

            item = createItem(session, setupApproval.getName(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                    fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery.class.getCanonicalName(),
                    "", GROUP_SETUP_ID, Entity.APPROVALNOTE, Renderer.APPROVAL, "");
            item.setTransformerParameters(setupApproval.getId().toString());
            item.doPreUpdate();
            session.update(item);
            session.flush();
            item.doPostUpdate();

            // le group texturing
            final IItemGroup groupTexturing = createItemGroup(session, sheetConstituantResume.getId(), cap(TEXTURING));
            final Long GROUP_TEXTURING_ID = groupTexturing.getId();

            createItem(session, "User", Sheet.ITEMTYPE_METADATA, texturingPersonAttribute.getId().toString(), "",
                    GROUP_TEXTURING_ID, texturingPersonAttribute.getDynMetaDataType().getType(), "", "");
            createItem(session, "Activities Duration", Sheet.ITEMTYPE_METADATA, texturingDateAttribute.getId()
                    .toString(), "", GROUP_TEXTURING_ID, JAVA_UTIL_DATE, "", "");
            createItem(session, "Ready For Approval", Sheet.ITEMTYPE_METADATA, texturingReadyAttribute.getId()
                    .toString(), "", GROUP_TEXTURING_ID, JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Renderer.BOOLEAN);

            item = createItem(session, texturingApproval.getName(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                    fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery.class.getCanonicalName(),
                    "", GROUP_TEXTURING_ID, Entity.APPROVALNOTE, Renderer.APPROVAL, "");
            item.setTransformerParameters(texturingApproval.getId().toString());
            item.doPreUpdate();
            session.update(item);
            session.flush();
            item.doPostUpdate();

            // /////////////////////////// FIN Suivi des constituants /////////////////////////////

            // ///////////////////////// FIN Suivi des constituants Resume /////////////////////////////
            final ISheet sheetSimpleConstituantResume = createSheet(session, PROJECT_PASTA_ID,
                    "Validation constituants simple", "Validation constituants simple", Entity.CONSTITUENT,
                    ESheetType.PRODUCTION);
            // create group
            final IItemGroup groupInfo_0 = createItemGroup(session, sheetSimpleConstituantResume.getId(), "Info");
            final Long GROUPINFO_ID_0 = groupInfo_0.getId();
            // create Items
            createItem(session, "Label", Sheet.ITEMTYPE_ATTRIBUTE, "label", "", GROUPINFO_ID_0, JAVA_LANG_STRING, "",
                    Editor.SHORT_TEXT);
            createItem(session, "Thumbnail", Sheet.ITEMTYPE_METHOD, "thumbnail", "", GROUPINFO_ID_0, JAVA_LANG_STRING,
                    Renderer.THUMBNAIL, "");

            createTaskStatusItems(session, groupInfo_0);

            // le group modeling
            final IItemGroup groupModeling_1 = createItemGroup(session, sheetSimpleConstituantResume.getId(),
                    cap(MODELING));

            item = createItem(session, modelingApproval.getName(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                    fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery.class.getCanonicalName(),
                    "", groupModeling_1.getId(), Entity.APPROVALNOTE, Renderer.APPROVAL, "");
            item.setTransformerParameters(modelingApproval.getId().toString());
            item.doPreUpdate();
            session.update(item);
            session.flush();
            item.doPostUpdate();

            // le group setup
            final IItemGroup groupSetup_1 = createItemGroup(session, sheetSimpleConstituantResume.getId(), cap(SETUP));
            item = createItem(session, setupApproval.getName(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                    fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery.class.getCanonicalName(),
                    "", groupSetup_1.getId(), Entity.APPROVALNOTE, Renderer.APPROVAL, "");
            item.setTransformerParameters(setupApproval.getId().toString());
            item.doPreUpdate();
            session.update(item);
            session.flush();
            item.doPostUpdate();

            // le group texturing
            final IItemGroup groupTexturing_1 = createItemGroup(session, sheetSimpleConstituantResume.getId(),
                    TEXTURING);
            item = createItem(session, texturingApproval.getName(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                    fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery.class.getCanonicalName(),
                    "", groupTexturing_1.getId(), Entity.APPROVALNOTE, Renderer.APPROVAL, "");
            item.setTransformerParameters(texturingApproval.getId().toString());
            item.doPreUpdate();
            session.update(item);
            session.flush();
            item.doPostUpdate();
            // /////////////////////////// FIN Suivi des constituants /////////////////////////////

            // ///////////////////////// FIN Suivi des Shot /////////////////////////////
            final ISheet sheetValidationShot = createSheet(session, PROJECT_PASTA_ID, "Validation shots",
                    "Validation shots", Entity.SHOT, ESheetType.PRODUCTION);

            // create group
            final IItemGroup groupInfo_1 = createItemGroup(session, sheetValidationShot.getId(), "Shot Info");
            final Long GROUPINFO_ID_1 = groupInfo_1.getId();
            // create Items
            createItem(session, "Sequence", Sheet.ITEMTYPE_ATTRIBUTE, "sequence", "", GROUPINFO_ID_1, Entity.SEQUENCE,
                    "", "");
            createItem(session, "Thumbnail", Sheet.ITEMTYPE_ATTRIBUTE, "hook", "", GROUPINFO_ID_1, JAVA_LANG_STRING,
                    Renderer.THUMBNAIL, "");
            createItem(session, "Label", Sheet.ITEMTYPE_ATTRIBUTE, "label", "", GROUPINFO_ID_1, JAVA_LANG_STRING, "",
                    Editor.SHORT_TEXT);
            createItem(session, "Nb Frame", Sheet.ITEMTYPE_ATTRIBUTE, "nbFrame", "", GROUPINFO_ID_1, JAVA_LANG_STRING,
                    "", Editor.SHORT_TEXT);
            createItem(session, "Description", Sheet.ITEMTYPE_ATTRIBUTE, "description", "", GROUPINFO_ID_1,
                    JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

            createTaskStatusItems(session, groupInfo_1);

            /* create group */
            final IItemGroup groupAnimInfo = createItemGroup(session, sheetValidationShot.getId(), "Anim Info");
            final Long GROUP_ANIMINFO_ID = groupAnimInfo.getId();
            createItem(session, "Difficulté Art", Sheet.ITEMTYPE_ATTRIBUTE, "difficulty", "", GROUP_ANIMINFO_ID,
                    JAVA_LANG_LONG, Renderer.RATING, "");
            createItem(session, "Difficulté Tech ", Sheet.ITEMTYPE_METADATA, shotDifficultyTech.getId().toString(), "",
                    GROUP_ANIMINFO_ID, JAVA_LANG_LONG, Renderer.RATING, "");
            createItem(session, "Started", Sheet.ITEMTYPE_METADATA, shotStartedMetaData.getId().toString(), "",
                    GROUP_ANIMINFO_ID, JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Renderer.BOOLEAN);
            createItem(session, "Shot Estimation", Sheet.ITEMTYPE_METADATA, shotEstimation.getId().toString(), "",
                    GROUP_ANIMINFO_ID, JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

            final IItemGroup groupValid = createItemGroup(session, sheetValidationShot.getId(), "Validation");
            final Long GROUP_VALID_ID = groupValid.getId();
            item = createItem(session, animRealApproval.getName(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                    fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery.class.getCanonicalName(),
                    "", GROUP_VALID_ID, Entity.APPROVALNOTE, Renderer.APPROVAL, "");
            item.setTransformerParameters(animRealApproval.getId().toString());
            item.doPreUpdate();
            session.update(item);
            session.flush();
            item.doPostUpdate();

            item = createItem(session, animTF1Approval.getName(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                    fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery.class.getCanonicalName(),
                    "", GROUP_VALID_ID, Entity.APPROVALNOTE, Renderer.APPROVAL, "");
            item.setTransformerParameters(animTF1Approval.getId().toString());
            item.doPreUpdate();
            session.update(item);
            session.flush();
            item.doPostUpdate();

            // /////////////////////////// FIN Suivi des Shot /////////////////////////////

            // create sheet
            final ISheet sheet = createSheet(session, PROJECT_PASTA_ID, "Détail constituants", "Détail constituants",
                    Entity.CONSTITUENT, ESheetType.BREAKDOWN); // create group
            final IItemGroup group = createItemGroup(session, sheet.getId(), "Default");
            final Long GROUP_ID = group.getId();
            // create Items
            createItem(session, "Label", Sheet.ITEMTYPE_ATTRIBUTE, "label", "", GROUP_ID, JAVA_LANG_STRING, "",
                    Editor.SHORT_TEXT);
            createItem(session, "Thumbnail", Sheet.ITEMTYPE_METHOD, "thumbnail", "", GROUP_ID, JAVA_LANG_STRING,
                    Renderer.THUMBNAIL, "");
            createItem(session, "Category", Sheet.ITEMTYPE_ATTRIBUTE, "category", "", GROUP_ID, Entity.CATEGORY,
                    Renderer.CATEGORY, "");
            createItem(session, "Activities Duration", Sheet.ITEMTYPE_COLLECTIONQUERY,
                    TaskDurationCollectionQuery.class.getCanonicalName(), "", GROUP_ID, JAVA_LANG_LONG,
                    Renderer.DURATION, "");

            createTaskStatusItems(session, group);

            createItem(session, "Difficulty", Sheet.ITEMTYPE_ATTRIBUTE, "difficulty", "", GROUP_ID, JAVA_LANG_LONG,
                    Renderer.RATING, Editor.INT);

            // /////////////////////////// DEBUT TASK MANAGER /////////////////////////////
            // create sheet taskManager default
            final ISheet sheetTask = createSheet(session, null, "Détail tâches", "Liste des tâches", Entity.TASK,
                    ESheetType.TASK);
            // create group
            final IItemGroup groupTask = createItemGroup(session, sheetTask.getId(), "Default");
            // create Items
            final Long groupTaskId = groupTask.getId();
            createItem(session, "Name", Sheet.ITEMTYPE_ATTRIBUTE, "name", "", groupTaskId, JAVA_LANG_STRING, "",
                    Editor.SHORT_TEXT);
            createItem(session, "Task Type", Sheet.ITEMTYPE_ATTRIBUTE, "taskType", "", groupTaskId, Entity.TASKTYPE,
                    Renderer.TASK_TYPE, "");
            final IItem workObjectItem = createItem(session, "Work Object", Sheet.ITEMTYPE_COLLECTIONQUERY,
                    TaskBoundEntityCollectionQuery.class.getCanonicalName(), "", groupTaskId, Const.BOUNDENTITY,
                    Renderer.WORK_OBJECT, "");

            final IItem workObjectParentIdItem = createItem(session, "Work Object Parent Ids",
                    Sheet.ITEMTYPE_ITEMSTRANSFORMER,
                    fr.hd3d.services.resources.itemstransformer.TaskBoundEntityParentIdItemsTransformer.class
                            .getCanonicalName(), "", groupTaskId, JAVA_LANG_LONG, Editor.INT, JAVA_LANG_LONG, "",
                    workObjectItem.getId().toString());

            createItem(session, "Project", Sheet.ITEMTYPE_ATTRIBUTE, "project", "", groupTaskId, Entity.PROJECT,
                    Renderer.PROJECT, "");

            createItem(session, "Approval Notes", Sheet.ITEMTYPE_COLLECTIONQUERY,
                    TaskBoundEntityApprovalNotesCollectionQuery.class.getCanonicalName(), "", groupTaskId,
                    Entity.APPROVALNOTE, Renderer.APPROVAL, "");

            createItem(session, "Status", Sheet.ITEMTYPE_ATTRIBUTE, "status", "", groupTaskId, Entity.TASKSTATUS,
                    Renderer.TASK_STATUS, "");
            createItem(session, "Startable", Sheet.ITEMTYPE_ATTRIBUTE, "startable", "", groupTaskId, JAVA_LANG_BOOLEAN,
                    Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, "Worker", Sheet.ITEMTYPE_ATTRIBUTE, "worker", "", groupTaskId, Entity.PERSON,
                    Renderer.PERSON_TASK, "");
            createItem(session, "Estimated Duration", Sheet.ITEMTYPE_ATTRIBUTE, "duration", "", groupTaskId,
                    JAVA_LANG_LONG, Renderer.DURATION, Editor.DURATION);

            createItem(session, "Start date", Sheet.ITEMTYPE_ATTRIBUTE, "startDate", "", groupTaskId, JAVA_UTIL_DATE,
                    Renderer.DATE_AND_DAY, Editor.DATE);
            createItem(session, "End date", Sheet.ITEMTYPE_ATTRIBUTE, "endDate", "", groupTaskId, JAVA_UTIL_DATE,
                    Renderer.DATE_AND_DAY, Editor.DATE);

            // /////////////////////////// DEBUT Directory /////////////////////////////
            // create list Person default
            final ISheet listPerson = createSheet(session, null, "Directory", "List of persons", Entity.PERSON,
                    ESheetType.PRODUCTION);
            // create group
            final IItemGroup groupListPersonProfil = createItemGroup(session, listPerson.getId(), "Profil");
            final Long GROUP_LIST_PERSON_PROFIL_ID = groupListPersonProfil.getId();
            // create Items

            createItem(session, "Last Name", Sheet.ITEMTYPE_ATTRIBUTE, "lastName", "", GROUP_LIST_PERSON_PROFIL_ID,
                    JAVA_LANG_STRING, Renderer.BOLD, Editor.SHORT_TEXT);
            createItem(session, "First Name", Sheet.ITEMTYPE_ATTRIBUTE, "firstName", "", GROUP_LIST_PERSON_PROFIL_ID,
                    JAVA_LANG_STRING, Renderer.BOLD, Editor.SHORT_TEXT);
            createItem(session, "Login", Sheet.ITEMTYPE_ATTRIBUTE, "login", "", GROUP_LIST_PERSON_PROFIL_ID,
                    JAVA_LANG_STRING, "", "");
            createItem(session, "Thumbnail", Sheet.ITEMTYPE_ATTRIBUTE, "photoPath", "", GROUP_LIST_PERSON_PROFIL_ID,
                    JAVA_LANG_STRING, Renderer.THUMBNAIL, "");
            createItem(session, "Phone", Sheet.ITEMTYPE_ATTRIBUTE, "phone", "", GROUP_LIST_PERSON_PROFIL_ID,
                    JAVA_LANG_STRING, "", Editor.SHORT_TEXT);
            createItem(session, "E-mail Address", Sheet.ITEMTYPE_ATTRIBUTE, "email", "", GROUP_LIST_PERSON_PROFIL_ID,
                    JAVA_LANG_STRING, "", Editor.SHORT_TEXT);

            // create group
            final IItemGroup groupListPersonProjects = createItemGroup(session, listPerson.getId(), "Projects");
            final Long GROUP_LIST_PERSON_PROJECTS_ID = groupListPersonProjects.getId();
            // create Items
            createItem(session, "Projects", Sheet.ITEMTYPE_METHOD, "getProjects", "", GROUP_LIST_PERSON_PROJECTS_ID,
                    Const.JAVA_UTIL_SET, Renderer.PROJECT, "");

            // create group
            final IItemGroup groupListPersonGroups = createItemGroup(session, listPerson.getId(), "Groups");
            // create Items
            createItem(session, "Groups", Sheet.ITEMTYPE_ATTRIBUTE, "resourceGroups", "",
                    groupListPersonGroups.getId(), Const.JAVA_UTIL_SET, Renderer.GROUP, "");
            // /////////////////////////// FIN Directory /////////////////////////////

            // /////////////////////////// DEBUT Constituents /////////////////////////////
            // create list Constituent default
            final ISheet listConstituent = createSheet(session, PROJECT_PASTA_ID, "Fabrication constituants",
                    "Liste des Constituants", Entity.CONSTITUENT, ESheetType.BREAKDOWN);
            final Long LIST_CONSTITUENT_ID = listConstituent.getId();

            // create group
            final IItemGroup groupListConstProfil = createItemGroup(session, LIST_CONSTITUENT_ID, "Principal");
            final Long GROUP_LIST_CONST_PROFIL_ID = groupListConstProfil.getId();
            // create Items
            createItem(session, "Label", Sheet.ITEMTYPE_ATTRIBUTE, "label", "", GROUP_LIST_CONST_PROFIL_ID,
                    JAVA_LANG_STRING, "", Editor.SHORT_TEXT);
            createItem(session, "Thumbnail", Sheet.ITEMTYPE_METHOD, "thumbnail", "", GROUP_LIST_CONST_PROFIL_ID,
                    JAVA_LANG_STRING, Renderer.THUMBNAIL, "");
            createItem(session, "Description", Sheet.ITEMTYPE_ATTRIBUTE, "description", "", GROUP_LIST_CONST_PROFIL_ID,
                    JAVA_LANG_STRING, "", Editor.SHORT_TEXT);
            createItem(session, "Category", Sheet.ITEMTYPE_ATTRIBUTE, "category", "", GROUP_LIST_CONST_PROFIL_ID,
                    "Category", Renderer.CATEGORY, "");

            createTaskStatusItems(session, groupListConstProfil);
            // createItem("Hook", IItem.ITEMTYPE_ATTRIBUTE, "hook", "", groupListConstProfil.getId(),
            // JAVA_UTIL_STRING, "", "");

            // create group
            final IItemGroup groupListConstInfo = createItemGroup(session, LIST_CONSTITUENT_ID, "Informations");
            final Long GROUP_LIST_CONST_INFO_ID = groupListConstInfo.getId();
            // create Items
            // createItem("Completion", IItem.ITEMTYPE_ATTRIBUTE, "completion", "", groupListConstInfo.getId(),
            // JAVA_LANG_LONG, "",
            // "");
            // createItem("Activitites duration", IItem.ITEMTYPE_METHOD, "activitiesDuration", "",
            // groupListConstInfo.getId(),
            // JAVA_LANG_LONG, "activitiesDurationRenderer", "");
            // createItem("Nb task Ok", IItem.ITEMTYPE_METHOD, "nbTaskOk", "", groupListConstInfo.getId(),
            // JAVA_UTIL_STRING,
            // "taskOkRenderer", "");
            createItem(session, "Difficulty", Sheet.ITEMTYPE_ATTRIBUTE, "difficulty", "", GROUP_LIST_CONST_INFO_ID,
                    JAVA_LANG_LONG, Renderer.RATING, "");
            createItem(session, "Risk", Sheet.ITEMTYPE_ATTRIBUTE, "trust", "", GROUP_LIST_CONST_INFO_ID,
                    JAVA_LANG_LONG, Renderer.RATING, "");
            createItem(session, "Version", Sheet.ITEMTYPE_ATTRIBUTE, "version", "", GROUP_LIST_CONST_INFO_ID,
                    JAVA_UTIL_DATE, "", "");

            // create group
            final IItemGroup groupListConstEtapes = createItemGroup(session, LIST_CONSTITUENT_ID, "Steps");
            final Long GROUP_LIST_CONST_ETAPES_ID = groupListConstEtapes.getId();
            // create Items
            createItem(session, cap(DESIGN), Sheet.ITEMTYPE_ATTRIBUTE, DESIGN, "", GROUP_LIST_CONST_ETAPES_ID,
                    JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(MODELING), Sheet.ITEMTYPE_ATTRIBUTE, MODELING, "", GROUP_LIST_CONST_ETAPES_ID,
                    JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(SETUP), Sheet.ITEMTYPE_ATTRIBUTE, "setup", "", GROUP_LIST_CONST_ETAPES_ID,
                    JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(TEXTURING), Sheet.ITEMTYPE_ATTRIBUTE, "texturing", "", GROUP_LIST_CONST_ETAPES_ID,
                    JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(SHADING), Sheet.ITEMTYPE_ATTRIBUTE, "shading", "", GROUP_LIST_CONST_ETAPES_ID,
                    JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(HAIRS), Sheet.ITEMTYPE_ATTRIBUTE, "hairs", "", GROUP_LIST_CONST_ETAPES_ID,
                    JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Editor.BOOLEAN);

            // /////////////////////////// FIN Constituent /////////////////////////////

            // /////////////////////////// DEBUT Shot /////////////////////////////
            // create list Shot default
            final ISheet listShot = createSheet(session, PROJECT_PASTA_ID, "Fabrication shots", "Liste des plans",
                    Entity.SHOT, ESheetType.BREAKDOWN);
            // create group
            final IItemGroup groupListShotPrincipal = createItemGroup(session, listShot.getId(), "Principal");
            final Long GROUP_LIST_SHOT_PRINCIPAL_ID = groupListShotPrincipal.getId();
            // create Items
            createItem(session, "Label", Sheet.ITEMTYPE_ATTRIBUTE, "label", "", GROUP_LIST_SHOT_PRINCIPAL_ID,
                    JAVA_LANG_STRING, "", Editor.SHORT_TEXT);
            createItem(session, "Thumbnail", Sheet.ITEMTYPE_ATTRIBUTE, "hook", "", GROUP_LIST_SHOT_PRINCIPAL_ID,
                    JAVA_LANG_STRING, Renderer.THUMBNAIL, "");
            createItem(session, "Description", Sheet.ITEMTYPE_ATTRIBUTE, "description", "",
                    GROUP_LIST_SHOT_PRINCIPAL_ID, JAVA_LANG_STRING, "", Editor.SHORT_TEXT);
            createItem(session, "Nb Frame", Sheet.ITEMTYPE_ATTRIBUTE, "nbFrame", "", GROUP_LIST_SHOT_PRINCIPAL_ID,
                    JAVA_LANG_LONG, "", Editor.INT);
            createItem(session, "Sequence", Sheet.ITEMTYPE_ATTRIBUTE, "sequence", "", GROUP_LIST_SHOT_PRINCIPAL_ID,
                    Entity.SEQUENCE, Renderer.SEQUENCE, "");

            createTaskStatusItems(session, groupListShotPrincipal);

            // create group
            final IItemGroup groupListShotDetails = createItemGroup(session, listShot.getId(), "Informations");
            final Long GROUP_LIST_SHOT_DETAILS_ID = groupListShotDetails.getId();
            // create Items
            // createItem("Completion", IItem.ITEMTYPE_ATTRIBUTE, "completion", "", groupListShotDetails.getId(),
            // JAVA_LANG_LONG,
            // "",
            // "");
            createItem(session, "Difficulty", Sheet.ITEMTYPE_ATTRIBUTE, "difficulty", "", GROUP_LIST_SHOT_DETAILS_ID,
                    JAVA_LANG_LONG, Renderer.RATING, Editor.INT);

            createItem(session, "Risk", Sheet.ITEMTYPE_ATTRIBUTE, "trust", "", GROUP_LIST_SHOT_DETAILS_ID,
                    JAVA_LANG_LONG, Renderer.RATING, Editor.INT);

            createItem(session, "Version", Sheet.ITEMTYPE_ATTRIBUTE, "version", "", GROUP_LIST_SHOT_DETAILS_ID,
                    JAVA_UTIL_DATE, "", "");
            createItem(session, "Hook", Sheet.ITEMTYPE_ATTRIBUTE, "hook", "", GROUP_LIST_SHOT_DETAILS_ID,
                    JAVA_LANG_STRING, "", "");

            // create group
            final IItemGroup groupListShotEtapes = createItemGroup(session, listShot.getId(), "Steps");
            // create Items
            final Long groupId = groupListShotEtapes.getId();
            createItem(session, cap(MATTEPAINTING), Sheet.ITEMTYPE_ATTRIBUTE, "mattePainting", "", groupId,
                    JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(LAYOUT), Sheet.ITEMTYPE_ATTRIBUTE, "layout", "", groupId, JAVA_LANG_BOOLEAN,
                    Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(ANIMATION), Sheet.ITEMTYPE_ATTRIBUTE, "animation", "", groupId, JAVA_LANG_BOOLEAN,
                    Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(EXPORT), Sheet.ITEMTYPE_ATTRIBUTE, "export", "", groupId, JAVA_LANG_BOOLEAN,
                    Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(TRACKING), Sheet.ITEMTYPE_ATTRIBUTE, "tracking", "", groupId, JAVA_LANG_BOOLEAN,
                    Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(DRESSING), Sheet.ITEMTYPE_ATTRIBUTE, "dressing", "", groupId, JAVA_LANG_BOOLEAN,
                    Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(LIGHTING), Sheet.ITEMTYPE_ATTRIBUTE, "lighting", "", groupId, JAVA_LANG_BOOLEAN,
                    Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(RENDERING), Sheet.ITEMTYPE_ATTRIBUTE, "rendering", "", groupId, JAVA_LANG_BOOLEAN,
                    Renderer.BOOLEAN, Editor.BOOLEAN);
            createItem(session, cap(COMPOSITING), Sheet.ITEMTYPE_ATTRIBUTE, "compositing", "", groupId,
                    JAVA_LANG_BOOLEAN, Renderer.BOOLEAN, Editor.BOOLEAN);

            // /////////////////////////// FIN Shot /////////////////////////////

            // /////////////////////////// DEBUT Activities /////////////////////////////
            // create list Person default
            final ISheet listActivity = createSheet(session, PROJECT_PASTA_ID, "Détails activités",
                    "List of activities", Entity.ACTIVITY, ESheetType.TASK);
            // create group
            final IItemGroup groupActivity = createItemGroup(session, listActivity.getId(), "Default");
            // create Items
            final Long groupActivityId = groupActivity.getId();
            createItem(session, "Project", Sheet.ITEMTYPE_ATTRIBUTE, "project", "", groupActivityId, Entity.PROJECT,
                    "", "");

            createItem(session, "Project Type", Sheet.ITEMTYPE_COLLECTIONQUERY,
                    fr.hd3d.services.resources.collectionquery.ActivitiesProjectTypeCollectionQuery.class
                            .getCanonicalName(), "", groupActivityId, JAVA_LANG_STRING, "", "");

            createItem(session, "Task Type", Sheet.ITEMTYPE_COLLECTIONQUERY,
                    fr.hd3d.services.resources.collectionquery.ActivitiesTaskTypeCollectionQuery.class
                            .getCanonicalName(), "", groupActivityId, JAVA_LANG_STRING, "", "");

            // createItem(session, "Work Object", IItem.ITEMTYPE_COLLECTIONQUERY,
            // "fr.hd3d.services.resources.collectionquery.TaskBoundEntityCollectionQuery", "", groupActivity
            // .getId(), "BoundEntity", Renderer.BOUND_ENTITY, "");

            createItem(session, "ActivityDuration", Sheet.ITEMTYPE_ATTRIBUTE, "duration", "", groupActivityId,
                    JAVA_LANG_LONG, Renderer.DURATION, "");

            createItem(session, "Worker", Sheet.ITEMTYPE_ATTRIBUTE, "worker", "", groupActivityId, Entity.PERSON,
                    Renderer.PERSON_TASK, "");

            createItem(session, "Date", Sheet.ITEMTYPE_ATTRIBUTE, "day", "", groupActivityId, Entity.PERSONDAY, "", "");

            // /////////////////////////// FIN Directory /////////////////////////////

        }
    }

    /**
     * creates casting elements for Pasta
     */
    private static class InitCasting implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            final IProject projectPasta = Persistors.project.getObjectByValue_NoPermCheck("name", PAT_ET_STAN);
            Iterator<ISequence> seqIt = projectPasta.getSequences().iterator();

            List<IConstituent> constList;
            final Criteria constituentQuery = ((Session) session).createCriteria(Persistors.constituent
                    .getPersistedClass());
            // constituentQuery.createCriteria(
            // "projecJe viens de commiter une première partie, je n'ai pas fini. Mais tu peux t"
            // ).add(Restrictions.idEq(PROJECT_PASTA_ID));
            constList = constituentQuery.list();
            // Iterator iter = constList.iterator();
            // IConstituent thisConst = (IConstituent) iter.next();
            IConstituent thisConst;

            IComposition newCompo = null;
            while (seqIt.hasNext())
            {
                // Root level
                ISequence currentSeq = seqIt.next();
                if (currentSeq.getParent() != null)
                {
                    // Episode level
                    ISequence currentParent = currentSeq.getParent();
                    if (currentParent.getParent() != null)
                    {
                        // Sequence level

                        List<IShot> shotList;
                        final Criteria shotQuery = ((Session) session).createCriteria(Persistors.shot
                                .getPersistedClass());
                        shotQuery.add(Restrictions.eq("sequence", currentSeq));
                        shotList = shotQuery.list();

                        Iterator<IShot> shotIt = (Iterator<IShot>) shotList.iterator();
                        while (shotIt.hasNext())
                        {
                            IShot currentShot = shotIt.next();
                            // Each shot receives randomly selected constituents

                            final double minCompoCard = 2;
                            final double maxCompoCard = 15;
                            double thisCompoCard = minCompoCard
                                    + Math.floor(Math.random() * (maxCompoCard - minCompoCard));

                            ArrayList<Integer> selectedConsts = new ArrayList<Integer>();

                            Integer newIndex = (int) Math.floor(Math.random() * (constList.size() - 1));
                            selectedConsts.add(0, newIndex);

                            thisConst = constList.get(newIndex);
                            newCompo = getComposition(session, null, currentShot.getId(), thisConst.getId(), thisConst
                                    .getLabel(), thisConst.getCategory().getId());// TODO a
                            // merger

                            session.flush();
                            session.refresh(newCompo);

                            for (Integer i = 1; i < thisCompoCard; i++)
                            {
                                newIndex = (int) Math.floor(Math.random() * (constList.size() - 1));
                                if (!selectedConsts.contains(newIndex))
                                {
                                    selectedConsts.add(newIndex);
                                    thisConst = constList.get(newIndex);
                                    newCompo = getComposition(session, null, currentShot.getId(), thisConst.getId(),
                                            thisConst.getLabel(), thisConst.getCategory().getId());
                                    session.flush();
                                    session.refresh(newCompo);
                                }
                            }
                        }
                    }
                }
            }
            session.flush();
        }
    }

    /**
     * creates casting elements for Pasta
     */
    private class InitPlanning implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            Date today = new Date();
            List<ITask> taskList;
            final Criteria taskQuery = ((Session) session).createCriteria(Persistors.task.getPersistedClass());
            taskQuery.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            taskList = taskQuery.list();

            Iterator iter = taskList.iterator();

            final Criteria personQuery = ((Session) session).createCriteria(Persistors.person.getPersistedClass());
            personQuery.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            List<IPerson> personList = personQuery.list();
            ArrayList<Date> disponibility = new ArrayList<Date>();

            for (IPerson person : personList)
            {
                disponibility.add(new Date());
            }
            Date lastStartDate = new Date();

            while (iter.hasNext())
            {
                ITask thisTask = (ITask) iter.next();
                IPerson assignedPerson = null;
                int index = -1;
                String[] list = thisTask.getName().split("_");
                if (list.length != 1)
                {
                    String groupName = (thisTask.getName().split("_"))[0];

                    IResourceGroup group = (IResourceGroup) session.createCriteria(
                            Persistors.resourcegroup.getPersistedClass()).add(Restrictions.eq("name", groupName))
                            .uniqueResult();

                    if (group != null)
                    {
                        int groupCard = group.getResources().size();
                        int assigneeIndex = (int) Math.floor(Math.random() * (groupCard - 1));
                        int cnt = 0;
                        for (IResource person : group.getResources())
                        {
                            assignedPerson = (IPerson) person;
                            index = personList.indexOf(assignedPerson);
                            if (cnt >= assigneeIndex)
                            {
                                break;
                            }
                            cnt = cnt + 1;
                        }
                    }
                }

                Long duration = thisTask.getDuration() * 3L;// in seconds

                if (index != -1)
                {
                    lastStartDate = disponibility.get(index);
                }

                Calendar calendar = new GregorianCalendar();
                Date newEndDate = lastStartDate;

                calendar.setTimeInMillis(lastStartDate.getTime() - (duration * 1000L));
                // Long startDateInMs = calendar.setTimeInMillis(lastStartDate.getTime() - duration);
                lastStartDate = calendar.getTime();

                thisTask.setStartDate(lastStartDate);
                thisTask.setEndDate(newEndDate);

                thisTask.setWorker(assignedPerson);

                // The task is put 'OK' if it starts 2~6 days before today
                int durationRnd = 2 + (int) Math.floor(Math.random() * 4);
                Long thresholdDuration = 24L * 60L * 60L * Long.valueOf(durationRnd);
                calendar.setTimeInMillis(today.getTime() - thresholdDuration);
                Date thresholdDate = calendar.getTime();

                if (lastStartDate.compareTo(thresholdDate) < 0)
                {
                    thisTask.setStatus(ETaskStatus.OK);
                    thisTask.setCompletionDate(newEndDate);
                }

                if (index != -1)
                {
                    disponibility.set(index, lastStartDate);
                }

                createTaskActivities(session, (TaskH) thisTask);

                session.flush();
                session.refresh(thisTask);
            }
            session.flush();
        }

    }

    private class InitTags implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            // create tags
            ITag tag_Interieur = new TagH("Intérieur");
            ITag tag_Exterieur = new TagH("Extérieur");
            ITag tag_Salon = new TagH("Salon");
            ITag tag_Nuit = new TagH("Nuit");
            ITag tag_Jour = new TagH("Jour");
            ITag tag_Nihongo = new TagH("???");

            // bind them to objects
            IConstituent const_Salon = Persistors.constituent.getObjectByValue_NoPermCheck("label", "SALON");
            IConstituent const_Cuisine = Persistors.constituent.getObjectByValue_NoPermCheck("label", "CUISINE");
            IConstituent const_PortesFenetres = Persistors.constituent.getObjectByValue_NoPermCheck("label",
                    "PORTESFENETRES");

            save(session, tag_Nuit);
            save(session, tag_Jour);
            save(session, tag_Nihongo);
            save(session, tag_Exterieur);
            save(session, tag_Interieur);
            save(session, tag_Salon);
            session.flush();

            save(session, const_Salon);
            save(session, const_Cuisine);
            save(session, const_PortesFenetres);
            session.flush();
        }

    }

    /*
     * ================================================
     * 
     * Convenience common methods
     * 
     * ================================================
     */
    /**
     * Create a new user account in User_Account Table. If it already exists, nothing is created.
     * 
     * @param login
     *            a person's login
     * @return User account newly created or user account in table if it already exists.
     * @throws Hd3dTranslationException
     * @throws Hd3dPersistenceException
     *             When database error occurs
     */
    private static IUserAccount getAccount(Session session, String login) throws Hd3dException
    {
        // Retrieve the user account.
        IUserAccount object = (IUserAccount) session.createCriteria(Persistors.useraccount.getPersistedClass()).add(
                Restrictions.eq("principal", login)).uniqueResult();

        // If the user account is in database, return it.
        if (object != null)
        {
            return object;
        }

        // Else create a new user account instance.
        object = new UserAccountH(login);

        // Save user account in database.
        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    private static void createPass(Session session, String login) throws Hd3dException
    {
        IUserAccount account = getAccount(session, login);
        account.setSecret(FDTAuthUtils.encrypt(login + "pass"));
        session.save(account);
    }

    /**
     * Create a new project in Project Table. If project already exists, no project is created.
     * 
     * @param name
     *            Project name
     * @param status
     *            Project status
     * @param color
     *            Project color
     * @return Project newly created or project in table if it already exists.
     * @throws Hd3dTranslationException
     * 
     * @throws Hd3dPersistenceException
     *             When database error occurs
     */
    private static IProject getProject(Session session, String name, EProjectStatus status, String color, String hook)
            throws Hd3dException
    {

        // Retrieve project.
        IProject object = (IProject) session.createCriteria(Persistors.project.getPersistedClass()).add(
                Restrictions.eq("name", name)).uniqueResult();

        // If project is in database, return project.
        if (object != null)
        {
            return object;
        }

        // Else create a new project instance.
        object = Translators.project.fromLightweight(LProject.getNewInstance(name, status.toString(), color, hook),
                null);

        // Save project in database.
        object.doPrePersist();
        session.save(object);
        object.doPostPersist();
        return object;
    }

    /**
     * Create a new person in Person Table. If task already exists, no person is created.
     * 
     * @param login
     *            Person login
     * @param password
     *            Person Password
     * @param first
     *            Person first name
     * @param last
     *            Person last name
     * @return Person newly created or person in table if it already exists.
     * @throws Hd3dTranslationException
     * @throws Hd3dPersistenceException
     *             When database error occurs
     */
    private static IPerson getPerson(Session session, String login, String password, String first, String last)
            throws Hd3dException
    {

        // Retrieve Person.
        IPerson object = (IPerson) session.createCriteria(Persistors.person.getPersistedClass()).add(
                Restrictions.eq("login", login)).uniqueResult();

        // If person is in database, return person.
        if (object != null)
        {
            return object;
        }

        // Else create a new person instance.
        object = Translators.person.fromLightweight(LPerson.getNewInstance(login, first, last, null, null, null,
                NumberUtils.LONG_ZERO), null);

        // Save person in database.
        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();

        createPass(session, login);
        return object;
    }

    private static void createInitialTaskTypes(Session session) throws Hd3dPersistenceException
    {
        // ITaskType design =
        getTaskType(session, cap(DESIGN), "#99FE80", "The Design Task Type", Entity.CONSTITUENT, true);

        // ITaskType modeling =
        getTaskType(session, cap(MODELING), "#009999", "The Modeling Task Type", Entity.CONSTITUENT, true);

        // ITaskType setup =
        getTaskType(session, cap(SETUP), "#00B366", "The Setup Task Type", Entity.CONSTITUENT, true);

        // ITaskType texturing =
        getTaskType(session, cap(TEXTURING), "#FE8080", "The Texturing Task Type", Entity.CONSTITUENT, true);

        // ITaskType shading =
        getTaskType(session, cap(SHADING), "#FEE680", "The Shading Task Type", Entity.CONSTITUENT, true);

        // ITaskType hairs =
        getTaskType(session, cap(HAIRS), "#FE80DF", "The Hairs Task Type", Entity.CONSTITUENT, true);

        // ITaskType mattePainting =
        getTaskType(session, cap(MATTEPAINTING), "#FE9980", "The MattePainting Task Type", Entity.SHOT, true);

        // ITaskType layout =
        getTaskType(session, cap(LAYOUT), "#FECC80", "The Layout Task Type", Entity.SHOT, true);

        // ITaskType export =
        getTaskType(session, cap(EXPORT), "#990099", "The Export Task Type", Entity.CONSTITUENT, true);

        // ITaskType animation =
        getTaskType(session, cap(ANIMATION), "#B27D00", "The Animation Task Type", Entity.SHOT, true);

        // ITaskType tracking =
        getTaskType(session, cap(TRACKING), "#00477D", "The Tracking Task Type", Entity.SHOT, true);

        // ITaskType rendering =
        getTaskType(session, cap(RENDERING), "#FF9900", "The Rendering Task Type", Entity.SHOT, true);

        // ITaskType dressing =
        getTaskType(session, cap(DRESSING), "#6BB200", "The Dressing Task Type", Entity.CONSTITUENT, true);

        // ITaskType lighting =
        getTaskType(session, cap(LIGHTING), "#FF3300", "The Lighting Task Type", Entity.SHOT, true);

        // ITaskType compositing =
        getTaskType(session, cap(COMPOSITING), "#80FEC8", "The Compositing Task Type", Entity.SHOT, true);
    }

    /**
     * Associate a Persons to a project (Many-to-many relationship).
     * 
     * @param project
     *            Project to be associated with persons
     * @param persons
     *            List of person to associate to a project
     * @throws Hd3dPersistenceException
     */
    private void appendStaffToProject(IProject project, IPerson... persons) throws Hd3dPersistenceException
    {

    // Build a new hash set if not initialize
    // if (project.getResourceGroups() == null)
    // project.setStaff(new HashSet<IPerson>());
    //
    // project.getResourceGroups().addAll(Arrays.asList(persons));
    }

    /**
     * Create a new group in Group Table. If group already exists, no group is created.
     * 
     * @param name
     *            Group name
     * @return Group newly created or project in table if it already exists.
     * @throws Hd3dTranslationException
     * 
     * @throws Hd3dPersistenceException
     *             When database error occurs
     */
    private static IResourceGroup getResourceGroup(Session session, String name) throws Hd3dException
    {

        // Retrieve project.
        IResourceGroup object = (IResourceGroup) session.createCriteria(Persistors.resourcegroup.getPersistedClass())
                .add(Restrictions.eq("name", name)).uniqueResult();

        // If project is in database, return project.
        if (object != null)
        {
            return object;
        }

        // Else create a new project instance.
        object = Translators.resourcegroup.fromLightweight(LResourceGroup.getNewInstance(name, null), null);

        // Save project in database.
        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();
        return object;
    }

    private static ICategory getCategory(Session session, Long id, Long version, String name, Long parent, Long project)
            throws Hd3dException
    {

        ICategory category = (ICategory) session.createCriteria(Persistors.category.getPersistedClass()).add(
                Restrictions.eq("id", id)).uniqueResult();// "id" must be
        // property and
        // not column name
        if (category != null)
            return category;
        category = Translators.category.fromLightweight(LCategory.getNewInstance(name, parent, project), null);
        category.doPrePersist();
        session.save(category);
        session.flush();
        category.doPostPersist();
        return category;
    }

    private static IConstituent getConstituent(Session session, Long id, Long categoryID, String categoryName,
            Integer completion, String description, Integer difficulty, String hook, String label, Integer trust,
            String uuid) throws Hd3dException
    {

        IConstituent object = (IConstituent) session.createCriteria(Persistors.constituent.getPersistedClass()).add(
                Restrictions.eq("id", id)).uniqueResult();
        if (object != null)
            return object;
        object = Translators.constituent.fromLightweight(LConstituent.getNewInstance(categoryID, categoryName,
                completion, description, difficulty, hook, label, trust, true, true, true, false, false, false, null),
                null);

        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();

        return object;
    }

    /**
     * Create a new task in Person Table. If task already exists, no task is created.
     * 
     * @param project
     *            Project associated to task
     * @param name
     *            Task name
     * @param status
     *            Task status
     * @param creator
     *            Task creator
     * @param worker
     *            Worker associated to task
     * @return Task newly created or task in table if it already exists.
     * @throws Hd3dPersistenceException
     * @throws Hd3dTranslationException
     */
    private static ITask getTask(Session session, IProject project, String name, ETaskStatus status, IPerson creator,
            IPerson worker, Date startDate, Date endDate, Long duration, Date deadLine, Date actualStartDate,
            Date actualEndDate, Boolean startable, Boolean confirmed, Boolean startup, ITaskType taskType)
            throws Hd3dPersistenceException, Hd3dTranslationException
    {

        // Retrieve task
        final Class<?> persistedClass = Persistors.task.getPersistedClass();
        ITask object = (ITask) session.createCriteria(persistedClass).add(Restrictions.eq("name", name)).uniqueResult();

        // If task is in database, return task.
        if (object != null)
            return object;

        // Else Save task in database.
        try
        {
            object = new TaskH(name, NumberUtils.BYTE_ZERO, null, status, project, creator, worker, startDate, endDate,
                    duration, deadLine, actualStartDate, actualEndDate, startable, confirmed, startup, null, taskType);

            object.doPrePersist();
            session.save(object);
            session.flush();
            object.doPostPersist();
            return object;

        }
        catch (Exception e)
        {
            // e.printStackTrace();
            throw new Hd3dPersistenceException(e);
        }
    }

    public static ITaskType getGlobalTaskType(Session session, String name, String entityName) throws Hd3dException
    {
        return getTaskType(session, name, "default color", "default description", entityName, true);
    }

    public static ITaskType getPastaTaskType(Session session, String name, String entityName) throws Hd3dException
    {
        return getTaskType(session, name, "default color", "default description", entityName, false);
    }

    @SuppressWarnings("unchecked")
    public static ITaskType getTaskType(Session session, String name, String color, String description,
            String entityName, boolean isGlobal) throws Hd3dPersistenceException
    {

        // Retrieve taskType
        final Class<?> persistedClass = Persistors.tasktype.getPersistedClass();
        List<ITaskType> taskTypes = session.createCriteria(persistedClass).add(Restrictions.eq("name", name)).list();// there
        // must
        // be
        // only
        // one,
        // but
        // you
        // may
        // have
        // a
        // few
        // when
        // you
        // mix
        // up initDB and initPasta

        // If taskType is in database, return task.
        if (taskTypes != null && !taskTypes.isEmpty())
        {
            return taskTypes.get(0);
        }
        // Else Save taskType in database.
        try
        {
            ITaskType newTaskType = new TaskTypeH(null, color, name, description, entityName);
            if (!isGlobal)
            {
                final IProject projectPasta = Persistors.project.getObjectByValue_NoPermCheck("name", PAT_ET_STAN);
                newTaskType.setProject(projectPasta);

            }
            newTaskType.doPrePersist();
            session.save(newTaskType);
            session.flush();
            newTaskType.doPostPersist();

            return newTaskType;
        }
        catch (Exception e)
        {
            // e.printStackTrace();
            throw new Hd3dPersistenceException(e);
        }
    }

    public static void createTask(Session session, String name, int difficulty, Long idWo) throws Hd3dException,
            NoSuchAlgorithmException
    {

        final IProject projectPasta = Persistors.project.getObjectByValue_NoPermCheck("name", PAT_ET_STAN);
        // Persons
        final IPerson eth = Persistors.person.getObjectByValue_NoPermCheck("login", "eth");
        // TODO make consistent taskTypes
        // final ITaskType defaultTaskType = getPastaTaskType("default Task Type");

        String nameTask = name.split("_")[0];
        final ITaskType TaskType = getTaskType(session, nameTask, null, null, null, true);

        /* Tasks */
        Date startDate = new Date();
        Date endDate = null;
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(startDate.getTime() + (3 * 24 * 60 * 60 * 1000));// 3 days later
        endDate = calendar.getTime();
        // Long duration = endDate.getTime() - startDate.getTime();
        // Long duration = new Long(8 * 60 * 60 * 1000 * (difficulty + 1));
        Long duration = 8L * 60L * 60L * Long.valueOf(difficulty + 1);
        // Projects
        ITask task = getTask(session, projectPasta, name, ETaskStatus.STAND_BY, eth, null, startDate, endDate,
                duration, null, startDate, endDate, true, null, null, TaskType);
        session.flush();

        IEntityTaskLink link = new EntityTaskLinkH(idWo, Entity.CONSTITUENT, task);
        link.doPrePersist();
        session.save(link);
        session.flush();
        link.doPostPersist();
    }

    public static void createTask(Session session, String name, Long idWo, String type) throws Hd3dException,
            NoSuchAlgorithmException
    {

        final IProject projectPasta = Persistors.project.getObjectByValue_NoPermCheck("name", PAT_ET_STAN);
        // Persons
        final IPerson eth = Persistors.person.getObjectByValue_NoPermCheck("login", "eth");
        // TaskType
        // TODO make consistent taskTypes
        // final ITaskType defaultTaskType = getPastaTaskType("default Task Type");
        String nameTask = name.split("_")[0];
        final ITaskType taskType = getTaskType(session, nameTask, null, null, null, true);

        // Tasks
        Date startDate = new Date();
        Date endDate = null;
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(startDate.getTime() + (3 * 24 * 60 * 60 * 1000));// 3 days later
        endDate = calendar.getTime();

        Long duration = new Long(28800);
        // Projects
        ITask task = getTask(session, projectPasta, name, ETaskStatus.STAND_BY, eth, null, startDate, endDate,
                duration, null, startDate, endDate, true, null, null, taskType);
        session.flush();

        IEntityTaskLink link = new EntityTaskLinkH(idWo, type, task);

        link.doPrePersist();
        session.save(link);
        session.flush();
        link.doPostPersist();
    }

    private static Long retrieveCategoryId(Long projectId, String name) throws Hd3dException
    {
        // TODO : filter retrieved categories with project id to enable a simple db update with Pasta data.
        ICategory category = Persistors.category.getObjectByValue_NoPermCheck("name", name);
        final Long categoryId = category.getId();
        return categoryId;
    }

    protected static void createConstituent(Session session, Long categoryId, String hook, String description,
            int taskDifficulty)
    {
        final String FILE_DIR = Conf.getTmpDir();

        try
        {
            IConstituent c = getConstituent(session, null, categoryId, null, 0, description, taskDifficulty, hook,
                    hook, 0, null);

            session.flush();
            session.refresh(c);

            String filePath = FILE_DIR + SystemUtils.FILE_SEPARATOR + c.getHook() + ".jpg";
            String thumbnailPath = FILE_DIR + SystemUtils.FILE_SEPARATOR + c.getHook() + "_thumb.jpg";
            if (DBInitializer.createFileRevisionFromTempFile(filePath, thumbnailPath, c, c.getCategory().getProject()) == null)
            {
                System.out.println("*** Error: could not create fileRevision for '" + hook + "'");
            }

            Long id = c.getId();
            createTask(session, "Design_" + hook, taskDifficulty, id);
            createTask(session, "Modeling_" + hook, taskDifficulty, id);
            createTask(session, "Setup_" + hook, taskDifficulty, id);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static ISequence getSequence(Session session, Long id, Long version, String name, String title,
            String hook, Long parent, Long project) throws Hd3dException
    {

        ISequence sequence = (ISequence) session.createCriteria(Persistors.sequence.getPersistedClass()).add(
                Restrictions.eq("id", id)).uniqueResult();// "id" must be property and not column name
        if (sequence != null)
            return sequence;

        ILSequence ls = LSequence.getNewInstance(name, title, parent, project, null);
        ls.setHook(hook);
        sequence = Translators.sequence.fromLightweight(ls, null);
        sequence.doPrePersist();
        session.save(sequence);
        session.flush();
        sequence.doPostPersist();
        return sequence;
    }

    private static IShot getShot(Session session, Long id, Long sequenceID, Integer completion, String description,
            Integer difficulty, String hook, String label, Integer trust, String uuid) throws Hd3dException
    {
        IShot object = (IShot) session.createCriteria(Persistors.shot.getPersistedClass()).add(
                Restrictions.eq("id", id)).uniqueResult();// "id" must be property and not column name
        if (object != null)
            return object;
        object = Translators.shot.fromLightweight(LShot.getNewInstance(sequenceID, completion, description, difficulty,
                hook, label, trust, true, true, false, false, false, false, false, false, false, trust, null, 0, 0),
                null);
        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();
        return object;
    }

    private static IApprovalNoteType createApprovalNoteType(Session session, Long projectId, String name,
            Long taskTypeId) throws Hd3dException
    {
        IApprovalNoteType object = Translators.approvalnotetype.fromLightweight(LApprovalNoteType.getNewInstance(name,
                taskTypeId, projectId), null);
        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();
        return object;
    }

    private static ISheet createSheet(Session session, Long projectId, String name, String description,
            String boundClassName, ESheetType type) throws Hd3dException
    {
        ISheet object = Translators.sheet.fromLightweight(LSheet.getNewInstance(name, description, projectId,
                boundClassName, null), null);
        object.setType(type);
        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();
        return object;
    }

    private static IItemGroup createItemGroup(Session session, Long sheetId, String name) throws Hd3dException
    {
        // IItemGroup object = Translators.itemGroup.fromLightweight(LItemGroup.getNewInstance(name,
        // new ArrayList<Long>(), sheetId, name.substring(0, 4)), null);
        IItemGroup object = Translators.itemGroup.fromLightweight(LItemGroup.getNewInstance(name, null, sheetId, name
                .substring(0, 4)), null);
        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();
        return object;
    }

    private static IItem createItem(Session session, String name, String type, String query, String controlContent,
            Long itemGroup, String metaType, String renderer, String editor) throws Hd3dException
    {
        IItem object = Translators.item.fromLightweight(LItem.getNewInstance(itemGroup, null, null, name, type, query,
                controlContent, itemGroup, metaType, null, null), null);
        object.setRenderer(renderer);
        object.setEditor(editor);
        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();
        return object;
    }

    private static IItem createItem(Session session, String name, String type, String query, String controlContent,
            Long itemGroup, String metaType, String renderer, String editor, String transformer,
            String transformerParams) throws Hd3dException
    {
        IItem object = Translators.item.fromLightweight(LItem.getNewInstance(itemGroup, null, null, name, type, query,
                controlContent, itemGroup, metaType, transformer, transformerParams), null);
        object.setRenderer(renderer);
        object.setEditor(editor);
        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();
        return object;
    }

    private static void createTaskStatusItems(Session session, IItemGroup group) throws Hd3dException
    {
        createItem(session, "Nb task", Sheet.ITEMTYPE_COLLECTIONQUERY,
                "fr.hd3d.services.resources.collectionquery.TotalNbOfTasksCollectionQuery", "", group.getId(),
                JAVA_LANG_LONG, Editor.INT, "");

        // STAND_BY, WORK_IN_PROGRESS, WAIT_APP, OK, CANCELLED, CLOSE;
        createItem(session, "Nb task " + ETaskStatus.OK.toString(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                "fr.hd3d.services.resources.collectionquery.NbOfTaskOKCollectionQuery", "", group.getId(),
                JAVA_LANG_LONG, Editor.INT, "", null, null);

        createItem(session, "Nb task " + ETaskStatus.STAND_BY.toString(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                "fr.hd3d.services.resources.collectionquery.NbOfTaskSTANDBYCollectionQuery", "", group.getId(),
                JAVA_LANG_LONG, Editor.INT, "", null, null);

        createItem(session, "Nb task " + ETaskStatus.WORK_IN_PROGRESS.toString(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                "fr.hd3d.services.resources.collectionquery.NbOfTaskWIPCollectionQuery", "", group.getId(),
                JAVA_LANG_LONG, Editor.INT, "", null, null);

        createItem(session, "Nb task " + ETaskStatus.WAIT_APP.toString(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                "fr.hd3d.services.resources.collectionquery.NbOfTaskWAITAPPCollectionQuery", "", group.getId(),
                JAVA_LANG_LONG, Editor.INT, "", null, null);

        createItem(session, "Nb task " + ETaskStatus.CANCELLED.toString(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                "fr.hd3d.services.resources.collectionquery.NbOfTaskCANCELLEDCollectionQuery", "", group.getId(),
                JAVA_LANG_LONG, Editor.INT, "", null, null);

        createItem(session, "Nb task " + ETaskStatus.CLOSE.toString(), Sheet.ITEMTYPE_COLLECTIONQUERY,
                "fr.hd3d.services.resources.collectionquery.NbOfTaskCLOSEDCollectionQuery", "", group.getId(),
                JAVA_LANG_LONG, Editor.INT, "", null, null);
    }

    private static IClassDynMetaDataType createAttributDynamique(Session session, String name, String entity,
            String type, String defaultValue) throws Hd3dException
    {

        /* found out the DynMetaDataType */

        String[] properties = new String[] { "name", "type" };
        Object[] values = new Object[] { name, type };
        List<IDynMetaDataType> dynMetaDataTypes = Persistors.dynmetadatatype.getByValues(null, properties, values,
                "AND");

        IDynMetaDataType dataType = null;
        if (CollectUtils.isNotEmpty(dynMetaDataTypes))
        {
            dataType = dynMetaDataTypes.get(0);
        }

        /* if not exist, create it */
        if (dataType == null)
        {
            // String typeName = name;
            // if (type.startsWith("java.util."))
            // {
            // typeName = type.substring("java.util.".length());
            // }
            // else if (type.startsWith("java.lang."))
            // {
            // typeName = type.substring("java.lang.".length());
            // }

            ILDynMetaDataType lightDataType = LDynMetaDataType.getNewInstance(defaultValue, name,
                    Const.DYNMETADATATYPE_SIMPLE, type);
            dataType = Translators.dynmetadatatype.fromLightweight(lightDataType, null);
            dataType.doPrePersist();
            session.save(dataType);
            session.flush();
            dataType.doPostPersist();
        }

        /* Create the ClassDynMetaDataType with predicate = "*"(all) */
        ILClassDynMetaDataType lightClassMeta = LClassDynMetaDataType.getNewInstance(name, dataType.getId(), "*",
                entity, "");
        IClassDynMetaDataType classMeta = Translators.classdynmetadata.fromLightweight(lightClassMeta, null);
        classMeta.doPrePersist();
        session.save(classMeta);
        session.flush();
        classMeta.doPostPersist();

        return classMeta;
    }

    private static IComposition getComposition(Session session, Long id, Long shotId, Long inputConstituentId,
            String inputConstituentName, Long inputCategoryId) throws Hd3dException
    {
        IComposition object = (IComposition) session.createCriteria(Persistors.composition.getPersistedClass()).add(
                Restrictions.eq("id", id)).uniqueResult();
        if (object != null)
            return object;
        String name = "CompoName_" + shotId + "_" + inputConstituentId;
        String description = "No Description";
        object = Translators.composition.fromLightweight(LComposition.getNewInstance(name, description,
                inputConstituentId, inputConstituentName, shotId, null, inputCategoryId), null);// TODO a merger

        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();
        return object;
    }

    private static String headWithZeros(Integer numberToConvert, Integer numberOfDigits)
    {

        String result = Integer.toString(numberToConvert);
        int threshold = 0;
        for (int i = numberOfDigits; i > 1; i--)
        {
            threshold = (int) Math.pow(10, i - 1);
            if (numberToConvert < threshold && numberOfDigits >= 2)
            {
                result = "0" + result;
            }
        }
        return result;
    }

    /**
     * creation d'un série d'activité sur la tache les activités vont seront daté en fonction de la startDate de la
     * tache et sur le nombre de jour de la durée rien n'est rempli les weekend
     * 
     * @param task
     * @return
     * @throws Hd3dTranslationException
     */
    private static List<IActivity> createTaskActivities(Session session, TaskH task) throws Hd3dException
    {
        List<IActivity> activities = new ArrayList<IActivity>();
        Date startDate = task.getStartDate();
        if (startDate == null)
        {
            return null;
        }
        Long nbSecIn7Hour = new Long(25200);
        Long nbSecInHalfAnHour = new Long(1800);
        Long duration = task.getDuration();
        int nbDay = (int) (duration / (nbSecIn7Hour * 1000));
        for (int i = 0; i < nbDay; i++)
        {
            // Date dayDate = new Date();
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(startDate);
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + i);

            if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
                    && calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
            {
                // on met heure de la date a 0
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                Date dayDate = calendar.getTime();

                // on verifie qu'il y a bien un worker sur la tache
                if (task.getWorker() != null)
                {

                    Double rand = new Double(Math.random() * 100);
                    IActivity activity = createActivity(session, dayDate, nbSecIn7Hour, task, task.getWorker().getId(),
                            task.getWorker().getId(), task.getProject().getId());
                    if (activity != null)
                    {
                        activities.add(activity);
                    }

                    // petit random pour creer un reunion de temps en temps
                    if (rand < 10)
                    {
                        IActivity activity2 = createActivity(session, dayDate, nbSecInHalfAnHour, null, task
                                .getWorker().getId(), task.getWorker().getId(), task.getProject().getId());
                        if (activity2 != null)
                        {
                            activities.add(activity2);
                        }
                    }
                }
            }
        }

        return activities;
    }

    private static IActivity createActivity(Session session, Date dayDate, Long duration, TaskH task, Long workerId,
            Long filledByID, Long projectId) throws Hd3dException
    {
        IPersonDay day = getDay(session, dayDate, workerId);
        if (day != null)
        {
            Date filledDate = new Date();
            if (task == null)
            {
                ISimpleActivity object = Translators.simpleActivity.fromLightweight(LSimpleActivity.getNewInstance(
                        ESimpleActivityType.MEETING, day.getId(), duration, "", workerId, projectId, filledByID,
                        filledDate), null);
                object.doPrePersist();
                session.save(object);
                session.flush();
                object.doPostPersist();
                return object;
            }
            else
            {
                ILTask ltask = task.defaultTranslator().toLightweight(task, null);

                ITaskActivity object = Translators.taskActivity.fromLightweight(LTaskActivity.getNewInstance(day
                        .getId(), dayDate, duration, "", ltask, workerId, task.getProject().getId(), filledByID,
                        filledDate), null);
                object.doPrePersist();
                session.save(object);
                session.flush();
                object.doPostPersist();
                return object;
            }
        }
        return null;
    }

    private static IPersonDay getDay(Session session, Date date, Long personId) throws Hd3dException
    {
        String query = "select day from " + PersonDayH.class.getCanonicalName() + " day where day.person.id = '"
                + personId + "'";
        List<IPersonDay> result = HibernateUtil.currentSession().createQuery(query).list();

        for (Iterator<IPersonDay> iterator = result.iterator(); iterator.hasNext();)
        {
            IPersonDay personDay = (IPersonDay) iterator.next();
            if (date.equals(personDay.getDate()))
            {
                return personDay;
            }
        }
        return createPersonDay(session, date, personId);
    }

    private static IPersonDay createPersonDay(Session session, Date dayDate, Long personId) throws Hd3dException
    {
        IPersonDay day = Translators.personDay.fromLightweight(LPersonDay.getNewInstance(dayDate, personId), null);
        day.doPrePersist();
        session.save(day);
        session.flush();
        day.doPostPersist();
        return day;
    }

    private static void save(Session session, ITag object) throws Hd3dException
    {
        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();
    }

    private static void save(Session session, IConstituent object) throws Hd3dException
    {
        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();
    }

    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
