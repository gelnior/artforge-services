package fr.hd3d.services.resources;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.Constraint;
import fr.hd3d.common.client.ItemConstraint;
import fr.hd3d.common.client.LuceneConstraint;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.impl.ILPersistent;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.persistence.impl.hibernate.ComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.IComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.search.LuceneFilterFactory;
import fr.hd3d.model.persistence.impl.hibernate.search.LuceneFilterFactory.LuceneFilters;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.CollectUtils;


public class ResourceContext<L extends ILPersistent, P extends Persistent>
{
    private boolean bulk = false;// bulk mode: process a collection of objects at once
    private boolean paginated = false;// pagination requested
    private Class<?> persistedClass;
    private IPersist<? extends IBase> persistor;
    private IBaseTranslator<L, P> translator;
    private IComplexTypeProvider complexTypeProvider;
    private CompositeQueryModifier filter;
    private Map<String, Object> attributes = new HashMap<String, Object>();
    private Map<String, String> queryMap = new HashMap<String, String>();
    private AbstractHd3dBaseResource resource;
    private LuceneFilters luceneFilters;

    /* Note: itemSorters can not be determined here, it can be determined when sorting */

    public ResourceContext()
    {}

    public ResourceContext(final AbstractHd3dBaseResource resource) throws Hd3dException
    {
        setResource(resource);
    }

    public void complete() throws Hd3dException
    {
        if (resource != null)
        {
            setBulk(Const.BULK.equals(resource.getQuery().getValues(Const.MODE)));
            setPaginated(resource.getQuery().getValuesMap().containsKey(Constraint.PAGINATION));
            setAttributes(resource.getRequest().getAttributes());
            setQueryMap(resource.getQuery().getValuesMap());
            luceneFilters = LuceneFilterFactory.getFilter(getQueryMap());

            buildFilter();
        }
    }

    private void buildFilter() throws Hd3dException
    {
        if (getResource() != null && getPersistedClass() != null)
        {
            setFilter(QueryFilterFactory.getFilter(getPersistedClass(), getResource().getQuery().getValuesMap(),
                    getComplexTypeProvider()));
        }
    }

    public boolean isBulk()
    {
        return bulk;
    }

    public void setBulk(boolean bulk)
    {
        this.bulk = bulk;
    }

    public boolean isPaginated()
    {
        return paginated;
    }

    public void setPaginated(boolean paginated)
    {
        this.paginated = paginated;
    }

    public Class<?> getPersistedClass()
    {
        return persistedClass;
    }

    public void setPersistedClass(Class<?> persistedClass) throws Hd3dException
    {
        this.persistedClass = persistedClass;
    }

    public String getPersistedClassEntityName()
    {
        return EntitiesMaps.withClassName(getPersistedClass().getCanonicalName()).getEntityName();
    }

    public IPersist getPersistor()
    {
        return persistor;
    }

    public void setPersistor(IPersist<? extends IBase> persistor) throws Hd3dException
    {
        this.persistor = persistor;
        if (persistor != null)
        {
            setPersistedClass(persistor.getPersistedClass());
            setComplexTypeProvider(ComplexTypeProvider.INSTANCE.getProvider(persistor.getPersistedClass()));
        }
    }

    public IBaseTranslator<L, P> getTranslator()
    {
        return translator;
    }

    public void setTranslator(IBaseTranslator<L, P> translator)
    {
        this.translator = translator;
    }

    public IComplexTypeProvider getComplexTypeProvider()
    {
        return complexTypeProvider;
    }

    public void setComplexTypeProvider(IComplexTypeProvider complexTypeProvider)
    {
        this.complexTypeProvider = complexTypeProvider;
    }

    public CompositeQueryModifier getFilter()
    {
        return filter;
    }

    public void setFilter(CompositeQueryModifier filter)
    {
        this.filter = filter;
    }

    public Map<String, Object> getAttributes()
    {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes)
    {
        this.attributes.clear();
        if (CollectUtils.isNotEmpty(attributes))
        {
            this.attributes.putAll(attributes);
        }
    }

    public Map<String, String> getQueryMap()
    {
        return queryMap;
    }

    public void setQueryMap(Map<String, String> queryMap)
    {
        this.queryMap.clear();
        if (CollectUtils.isNotEmpty(queryMap))
        {
            this.queryMap.putAll(queryMap);
        }
    }

    public AbstractHd3dBaseResource getResource()
    {
        return resource;
    }

    public void setResource(AbstractHd3dBaseResource resource)
    {
        this.resource = resource;
    }

    public LuceneFilters getLuceneFilters()
    {
        return luceneFilters;
    }

    public void setLuceneFilters(LuceneFilters luceneFilters)
    {
        this.luceneFilters = luceneFilters;
    }

    public boolean hibernatePagination()
    {
        return !getQueryMap().containsKey(LuceneConstraint.LUCENECONSTRAINT)
                && !getQueryMap().containsKey(ItemConstraint.ITEMSORT);
    }

    /**
     * retrieve id from url. If idString is "" or null or if conversion fails, 0 is returned.
     * 
     * @param idString
     * @return
     */
    public Long getIdFromUrl(final String idString)
    {
        return NumberUtils.toLong((String) getAttributes().get(idString));
    }

    public String getStringFromUrl(final String idString)
    {
        return getAttributes().get(idString).toString();
    }

}
