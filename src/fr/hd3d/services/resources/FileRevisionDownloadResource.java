package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.FILEREVISION_ID_ATTRIBUTE;

import java.io.File;

import org.hibernate.LockMode;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resolver.MountPointResolver;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.FilenameMaker;
import fr.hd3d.utils.Log;


/**
 * @author HD3D
 */
@Hd3dComment("Download file revision service. <br />"
        + "GET: Download file corresponding to file revision object described in URI. ")
public class FileRevisionDownloadResource extends AbstractHd3dResource<ILFileRevision, IFileRevision>
{
    protected final static String UPLOAD_DIR = Conf.getUploadDir();

    public FileRevisionDownloadResource() throws Hd3dException
    {
        super(Persistors.filerevision, Translators.filerevision);
        allowMethods("GET");
    }

    @Override
    public Representation get(Variant variant)
    {
        try
        {
            Representation rep = doTreatIt();
            getResponse().setEntity(rep);
            return rep;
        }
        finally
        {
            cleanup();
        }
    }

    private Representation doTreatIt()
    {
        Representation rep = null;

        try
        {
            IFileRevision file = initializeObject();

            if (file != null)
            {
                IAssetRevision assetRevision = file.getAssetRevision();
                if (assetRevision != null)
                {
                    String mountPoint = MountPointResolver.getResolvedMountPoint(getResourceContext().getResource()
                            .getClientInfo().getAgent(), assetRevision.getMountPointByState(file.getState()));
                    if (!mountPoint.equals(""))
                    {
                        String path = mountPoint + file.getLocation() + '/' + file.getRelativePath();
                        // rep = new
                        // StringRepresentation("<html><head><meta http-equiv='refresh' content='0; url=file://"
                        // + path + "'></head></html>", MediaType.TEXT_ALL);
                        // TODO faire une redirection vers le path

                    }
                    else
                    {
                        String fullPath = FilenameMaker.getFullPath(file);

                        File f = new File(fullPath);
                        String filename = file.getKey();

                        if (f.exists())
                        {
                            rep = new FileRepresentation(f, MediaType.ALL);
                            Disposition disposition = new Disposition();
                            disposition.setFilename(filename);
                            disposition.setType(file.getFormat());
                            disposition.setModificationDate(file.getVersion());
                            rep.setDisposition(disposition);

                            // rep.setDownloadName(filename);
                        }
                        else
                        {
                            Log.LOGGER.error("[ERROR] No file");
                            return new StringRepresentation("Could not open the specified file");
                        }
                    }
                }
            }
            else
            {
                Log.LOGGER.error("[ERROR] No file");
                return new StringRepresentation("Could not open the specified file");
            }
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error("[ERROR] No file");
            return new StringRepresentation("Could not open the specified file");
        }
        return rep;
    }

    @Override
    protected IFileRevision initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(FILEREVISION_ID_ATTRIBUTE, lockMode);
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
