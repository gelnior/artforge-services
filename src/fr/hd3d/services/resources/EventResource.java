package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.EVENT_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILEvent;
import fr.hd3d.model.persistence.IEvent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'une event\r\n" + "\r\nPUT:Changement d'une event\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Suppression d'une event")
public class EventResource extends AbstractHd3dResource<ILEvent, IEvent>
{
    public EventResource() throws Hd3dException
    {
        super(Persistors.event, Translators.event);
    }

    @Override
    protected IEvent initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(EVENT_ID_ATTRIBUTE, lockMode);
    }
}
