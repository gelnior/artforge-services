package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class FilesRevisionResource extends AbstractHd3dCollection<ILFileRevision, IFileRevision>
{

    public FilesRevisionResource() throws Hd3dException
    {
        super(Persistors.filerevision, Translators.filerevision);
    }
}
