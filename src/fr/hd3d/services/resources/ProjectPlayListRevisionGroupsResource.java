package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectPlayListRevisionGroupsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public final class ProjectPlayListRevisionGroupsResource extends
        AbstractHd3dCollection<ILPlayListRevisionGroup, IPlayListRevisionGroup>
{

    public ProjectPlayListRevisionGroupsResource() throws Hd3dException
    {
        super(Persistors.playlistrevisiongroup, Translators.playlistrevisiongroup);
        allowMethods(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectPlayListRevisionGroupsCmd(getResourceContext()).wantTotalSize());
    }
}
