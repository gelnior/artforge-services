package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.DEVICE_TYPE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILDeviceType;
import fr.hd3d.model.persistence.IDeviceType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single device object (id ford parameter) <br>
 * URI : /devicetypes/{id}
 * 
 * @author HD3D
 */
public class DeviceTypeResource extends AbstractHd3dResource<ILDeviceType, IDeviceType>
{
    @Override
    protected IDeviceType initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(DEVICE_TYPE_ID_ATTRIBUTE, lockMode);
    }

    public DeviceTypeResource() throws Hd3dException
    {
        super(Persistors.deviceType, Translators.deviceType);
    }
}
