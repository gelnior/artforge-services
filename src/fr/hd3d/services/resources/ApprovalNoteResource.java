package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.APPROVALNOTE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class ApprovalNoteResource extends AbstractHd3dResource<ILApprovalNote, IApprovalNote>
{

    public ApprovalNoteResource() throws Hd3dException
    {
        super(Persistors.approvalnote, Translators.approvalnote);
    }

    @Override
    protected IApprovalNote initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(APPROVALNOTE_ID_ATTRIBUTE, lockMode);
    }

}
