package fr.hd3d.services.resources;

import java.util.Map;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetTasksCmd;
import fr.hd3d.model.translator.Translators;


public class TasksResource extends AbstractHd3dCollection<ILTask, ITask>
{
    public TasksResource() throws Hd3dException
    {
        super(Persistors.task, Translators.task);
    }

    @Override
    protected ResultCollection<ITask> getCollectionForRepresentation() throws Hd3dException
    {
        Map<String, String> map = getQuery().getValuesMap();
        String customValues = map.get(Const.CUSTOM);
        if (customValues != null)
        {
            return doGetCollection(new GetTasksCmd(getResourceContext()).wantTotalSize());
        }
        return super.getCollectionForRepresentation();
    }
}
