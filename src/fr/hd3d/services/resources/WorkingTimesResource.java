package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILWorkingTime;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des working times\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'un working time\r\n" + "\r\nDELETE:N/A")
public class WorkingTimesResource extends AbstractHd3dCollection<ILWorkingTime, IWorkingTime>
{
    public WorkingTimesResource() throws Hd3dException
    {
        super(Persistors.workingTime, Translators.workingTime);
    }
}
