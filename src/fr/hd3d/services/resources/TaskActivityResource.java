/**
 * 
 */
package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;


/**
 * Access for modification and delete of an identified task activity. When a task activity is deleted, the linked task
 * actual start date is update with the oldest activity date for this task.
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'une activité de tâche {taskActivityID}\r\n"
        + "\r\nPUT:Modification d'une activité de tâche\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Désactivation d'une activité de tâche")
public class TaskActivityResource extends BaseTaskActivityResource
{
    public TaskActivityResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected void doPostDelete() throws Hd3dException
    {
        super.doPostDelete();
        this.updateTaskActualStartDate();
    }

}
