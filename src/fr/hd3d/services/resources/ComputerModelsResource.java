package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILComputerModel;
import fr.hd3d.model.persistence.IComputerModel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer pool objects.<br>
 * URI : /pools/
 * 
 * @author HD3D
 */
public class ComputerModelsResource extends AbstractHd3dCollection<ILComputerModel, IComputerModel>
{
    public ComputerModelsResource() throws Hd3dException
    {
        super(Persistors.computerModel, Translators.computerModel);
    }
}
