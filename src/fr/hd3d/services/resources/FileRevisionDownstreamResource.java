package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetFileRevisionDownstream;
import fr.hd3d.model.translator.Translators;


public class FileRevisionDownstreamResource extends AbstractHd3dCollection<ILFileRevision, IFileRevision>
{

    public FileRevisionDownstreamResource() throws Hd3dException
    {
        super(Persistors.filerevision, Translators.filerevision);
        allowMethods("GET");
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetFileRevisionDownstream(getResourceContext()).wantTotalSize());
    }
}
