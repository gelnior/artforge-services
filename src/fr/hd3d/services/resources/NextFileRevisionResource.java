package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.FILEREVISION_ID_ATTRIBUTE;

import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;

import fr.hd3d.common.client.enums.EFileStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.translator.json.JSonFormSerializer;
import fr.hd3d.utils.HibernateUtil;


public class NextFileRevisionResource extends AbstractHd3dBaseResource
{
    public NextFileRevisionResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    protected Representation post(Representation entity, Variant variant) throws ResourceException
    {

        Long fileRevisionId = getResourceContext().getIdFromUrl(FILEREVISION_ID_ATTRIBUTE);
        if (BaseH.isNotValidId(fileRevisionId))
            return getResponseEntity();

        List<ILFileRevision> lFileRevisions = null;
        final Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();
        try
        {
            final IFileRevision fileRevision = Persistors.filerevision.getById(fileRevisionId, LockMode.OPTIMISTIC);
            if (fileRevision == null)
                throw new Hd3dException("No file revision id");

            IFileRevision newfileRevision = null;
            lFileRevisions = JSonFormSerializer.<ILFileRevision> deserializeList(entity);
            if (lFileRevisions == null || lFileRevisions.isEmpty())
            {
                throw new Hd3dException("No file revision json in post data");
            }

            newfileRevision = Translators.filerevision.fromLightweight(lFileRevisions.get(0), getResourceContext());

            String maxRevisionQuery = "(select max(revision) from FileRevisionH where key='" + fileRevision.getKey()
                    + "' and variation='" + fileRevision.getVariation() + "')";
            String query = "from FileRevisionH where key='" + fileRevision.getKey() + "' and variation='"
                    + fileRevision.getVariation() + "' and revision=" + maxRevisionQuery;
            IFileRevision maxRevisionFileRevision = (IFileRevision) HibernateUtil.currentSession().createQuery(query)
                    .uniqueResult();

            if (maxRevisionFileRevision.getStatus() == EFileStatus.LOCKED)
                throw new Hd3dPersistenceException("File with key=" + fileRevision.getKey() + " and variation="
                        + fileRevision.getVariation() + " is locked!");

            newfileRevision.setRevision(maxRevisionFileRevision.getRevision() + 1);
            newfileRevision.setAssetRevision(maxRevisionFileRevision.getAssetRevision());
            Persistors.filerevision.persist(newfileRevision);
            maxRevisionFileRevision.setAssetRevision(null);
            Persistors.filerevision.merge(maxRevisionFileRevision);

            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            tx.commit();
            successfulPost(newfileRevision);
        }
        catch (Hd3dException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally
        {
            /* clear */
            if (lFileRevisions != null)
            {
                lFileRevisions.clear();
                lFileRevisions = null;
            }
            if (tx != null)
            {
                tx = null;
            }
            cleanup();
        }

        Representation rep = getResponseEntity();
        return rep;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        String baseRef = getRequest().getResourceRef().getBaseRef().toString();
        String[] tokens = StringUtils.splitPreserveAllTokens(baseRef, '/');
        Object[] subTokens = ArrayUtils.subarray(tokens, 0, tokens.length - 2);
        String newIdentifier = StringUtils.join(subTokens, '/');
        return object == null ? null : '[' + StringUtils.join(new Object[] { newIdentifier,
                ((IFileRevision) object).getId() }, '/') + ']';
    }
}
