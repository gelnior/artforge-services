package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PLAYLISTREVISION_ID_ATTRIBUTE;

import java.util.List;

import org.hibernate.LockMode;
import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


/**
 * @author Try LAM
 */
public class PlayListRevisionResource extends AbstractHd3dResource<ILPlayListRevision, IPlayListRevision>
{

    public PlayListRevisionResource() throws Hd3dException
    {
        super(Persistors.playlistrevision, Translators.playlistrevision);
    }

    @Override
    protected IPlayListRevision initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(PLAYLISTREVISION_ID_ATTRIBUTE, lockMode);
    }

    @Override
    protected IPlayListRevision copy(Representation entity)
    {
        try
        {
            IPlayListRevision newRevision = object.copy();
            List<IPlayListRevision> pl = Persistors.playlistrevision.getByValue("name", object.getName(),
                    LockMode.OPTIMISTIC);
            newRevision.setRevision(getLastRevisionNumber(pl, object.getProject().getId()) + 1);
            return newRevision;
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            return null;
        }
    }

    private int getLastRevisionNumber(List<IPlayListRevision> pl, Long projectId)
    {
        int maxRevNum = -1;

        if (projectId != null && CollectUtils.isNotEmpty(pl))
        {
            CollectUtils.removeNull(pl);

            for (IPlayListRevision rev : pl)
            {
                if (!projectId.equals(CollectUtils.nullSafeId(rev.getProject())))
                {
                    continue;
                }

                Integer revNum = rev.getRevision();
                if (revNum > maxRevNum)
                {
                    maxRevNum = revNum;
                }
            }
        }

        return maxRevNum;
    }
}
