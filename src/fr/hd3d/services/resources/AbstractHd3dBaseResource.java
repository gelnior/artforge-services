package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.CREATION_OK;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.shiro.util.ThreadContext;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.CharacterSet;
import org.restlet.data.Language;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.resource.ServerResource;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.utils.Bench;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.ServerResourceUtils;


@SuppressWarnings("serial")
public abstract class AbstractHd3dBaseResource extends ServerResource
{
    // Bench b;// DEBUG

    private ResourceContext<?, ?> resourceContext;

    protected final static Long RETRY_SLEEP_TIME = 1000L;// ms

    /* keep trace of errors */
    protected List<String> errorList = new ArrayList<String>();

    final static List<Variant> VARIANTS = new ArrayList<Variant>() {
        {
            add(new Variant(MediaType.APPLICATION_JSON));
            add(new Variant(MediaType.TEXT_HTML));
            add(new Variant(MediaType.TEXT_PLAIN));
            for (Variant v : this)
            {
                v.setCharacterSet(CharacterSet.UTF_8);
            }
        }
    };

    @SuppressWarnings("unchecked")
    public AbstractHd3dBaseResource() throws Hd3dException
    {
        init(Context.getCurrent(), Request.getCurrent(), Response.getCurrent());

        resourceContext = new ResourceContext(this);

        Log.LOGGER.info(requestInfo());

        /* workaround: store this resource in ThreadContext. Must be cleared after use */
        ThreadContext.put("resource", this);
    }

    public AbstractHd3dBaseResource(ResourceContext<?, ?> context) throws Hd3dException
    {
        this();
        if (context != null)
        {
            this.resourceContext = context;
        }
    }

    @Override
    protected void doRelease()
    {
        /* workaround: clear Shiro ThreadContext */
        ThreadContext.remove();
    }

    protected ResourceContext<?, ?> getResourceContext()
    {
        return resourceContext;
    }

    protected void setResourceContext(final ResourceContext<?, ?> context)
    {
        this.resourceContext = context;
    }

    @Override
    public Representation handle()
    {
        return super.handle();

        // if (LicenseManager.isLicenseOK()){
        // return super.handle();
        // }
        //
        // Hd3dException e = new Hd3dException("License health is not OK");
        // error(Status.CLIENT_ERROR_FORBIDDEN, e, "License health is not OK");
        // return new StringRepresentation(ExceptUtils.format(e));
    }

    @Override
    public List<Variant> getVariants()
    {
        return VARIANTS;
    }

    /**
     * set allowed HTTP methods for this resource. READ is always allowed
     * 
     * @param methods
     *            Possible values: POST,PUT,DELETE with "," separator
     */
    protected void allowMethods(final String methods)
    {
        getAllowedMethods().clear();

        final Set<Method> allowed = new CopyOnWriteArraySet<Method>();
        allowed.add(Method.GET);

        if (StringUtils.isNotEmpty(methods))
        {
            String[] mArray = StringUtils.split(StringUtils.upperCase(methods), ',');
            if (ArrayUtils.contains(mArray, "POST"))
            {
                allowed.add(Method.POST);
            }
            if (ArrayUtils.contains(mArray, "PUT"))
            {
                allowed.add(Method.PUT);
            }
            if (ArrayUtils.contains(mArray, "DELETE"))
            {
                allowed.add(Method.DELETE);
            }

            /* clean */
            for (int i = 0; i < mArray.length; i++)
            {
                mArray[i] = null;
            }
            mArray = null;
        }

        setAllowedMethods(allowed);
    }

    protected final String requestInfo()
    {
        return ServerResourceUtils.info(Request.getCurrent());
    }

    protected String responseInfo()
    {
        return ServerResourceUtils.info(getResponse());
    }

    protected String responseInfo(Bench b)
    {
        return ServerResourceUtils.info(getResponse(), b);
    }

    protected void cleanup()
    {
        errorList.clear();
        HibernateUtil.closeSession();
        release();
    }

    public void error(final Status status, final Throwable excep, final String message)
    {
        String[] errorMessage = ServerResourceUtils.buildErrorMessage(excep, message);
        Log.LOGGER.info(responseInfo());
        Log.LOGGER.error(StringUtils.join(errorMessage));
        getResponse().setStatus(status);
        getResponse().setEntity(new StringRepresentation(Hd3dJsonSerializer.serialize(errorMessage)));
    }

    @SuppressWarnings("unchecked")
    protected String getNewIdentifier(final Object object)
    {
        String ret;

        /* bulk creation */
        if (Collection.class.isInstance(object))
        {
            final Collection<String> resourceIdentifiers = (Collection<String>) org.apache.commons.collections15.CollectionUtils
                    .collect((Collection) object, new GetNewResourceIdentifierTransformer());

            ret = '[' + StringUtils.join(resourceIdentifiers, ',') + ']';
            resourceIdentifiers.clear();
        }
        else
        {
            /* Indicates where is located the new resource. */
            ret = getNewResourceIdentifier(object);
        }

        return ret;
    }

    public class GetNewResourceIdentifierTransformer implements
            org.apache.commons.collections15.Transformer<Object, String>
    {
        public String transform(final Object object)
        {
            return getNewResourceIdentifier(object);
        }
    }

    protected abstract String getNewResourceIdentifier(final Object object);

    /**
     * 
     * @param object
     * @return resource identifier (url)
     */
    protected String successfulPost(final Object object)
    {
        getResponse().setStatus(Status.SUCCESS_CREATED);
        final Representation rep = new StringRepresentation(CREATION_OK, MediaType.TEXT_PLAIN, Language.ALL,
                CharacterSet.UTF_8);
        rep.setLocationRef(getNewIdentifier(object));
        getResponse().setEntity(ServerResourceUtils.encodeRepresentation(rep));

        return rep.getLocationRef().toString();
    }

    protected void sucessfulPut(final Object object, final MediaType type)
    {
        // Set the response's status and entity
        if (CollectUtils.isNotEmpty(errorList))
        {
            final StringBuilder buf = new StringBuilder("OK but some errors occurred:");
            for (String err : errorList)
            {
                buf.append(err).append(SystemUtils.LINE_SEPARATOR);
            }
            getResponse().setStatus(Status.SUCCESS_OK, buf.toString());
        }
        else
        {
            getResponse().setStatus(Status.SUCCESS_OK);
        }

        final Representation rep = RessourceSerializer.getRepresentation(type, object);
        rep.setCharacterSet(CharacterSet.UTF_8);
        getResponse().setEntity(ServerResourceUtils.encodeRepresentation(rep));
    }

    /**
     * Run the scripts triggered by the event. Note: must be called AFTER the the response identifier (url of the
     * resource) has been set, since it is passed to the scripts to retrieve the object
     * 
     * @param object
     * @param event
     *            that triggered the script. Possible values are: "prePersist", "postPersist", "preUpdate",
     *            "postUpdate", "preDelete", "postDelete"
     * @param identifier
     *            is the uri identifying the resource passed to the script as parameter
     * @return a message about script execution
     * @throws Hd3dPersistenceException
     */
    protected String executeScripts(final IBase object, final String event, final String identifier)
            throws Hd3dPersistenceException
    {
        return ServerResourceUtils.executeScripts(object, event, identifier);
    }

    protected void doOnError(final Throwable excep, final Session session, final Transaction tx, final Status status,
            final String message)
    {
        errorList.add(ExceptUtils.format(excep));
        /* Do not do: session.cancelQuery() since session is shared, a query in another thread may be canceled */
        if (tx != null && tx.isActive())
        {
            tx.rollback();
        }
        error(status, excep, message);
    }

    protected void sleep()
    {
        final long sleepTime = (long) (RETRY_SLEEP_TIME * RandomUtils.nextDouble());
        Log.LOGGER.info("Retry...sleep for:" + sleepTime);
        try
        {
            Thread.sleep(sleepTime);
        }
        catch (InterruptedException e1)
        {
            Log.LOGGER.warn(ExceptUtils.format(e1));
        }
    }
}
