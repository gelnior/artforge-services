package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetShotsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des shots présents dans la séquence {sequenceID}\r\n"
        + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'un shot dans une séquence (ou sous-séquence) donnée en ajoutant à l'url: ?mode=bulk, Ajout d'une LISTE de shots\r\n"
        + "\r\nDELETE:N/A")
public class SequenceShotsResource extends AbstractHd3dCollection<ILShot, IShot>
{
    public SequenceShotsResource() throws Hd3dException
    {
        super(Persistors.shot, Translators.shot);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetShotsCmd(getResourceContext()).wantTotalSize());
    }
}
