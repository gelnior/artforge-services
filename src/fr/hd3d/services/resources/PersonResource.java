package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * A resource to access to a specific person.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération des informations d'un utilisateur\r\n" + "\r\nPUT:Modification d'un utilisateur\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class PersonResource extends AbstractHd3dResource<ILPerson, IPerson>
{
    public PersonResource() throws Hd3dException
    {
        super(Persistors.person, Translators.person);
    }

    @Override
    protected IPerson initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(PERSON_ID_ATTRIBUTE, lockMode);
    }
}
