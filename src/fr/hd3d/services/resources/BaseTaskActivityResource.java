package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.TASK_ACTIVITY_ID_ATTRIBUTE;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.Const;


public abstract class BaseTaskActivityResource extends AbstractHd3dResource<ILTaskActivity, ITaskActivity>
{

    public BaseTaskActivityResource() throws Hd3dException
    {
        super(Persistors.taskActivity, Translators.taskActivity);
    }

    @Override
    protected ITaskActivity initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(TASK_ACTIVITY_ID_ATTRIBUTE, lockMode);
    }

    /**
     * Sets the actual start date of tasks with the date of oldest activity linked to the task of deleted activity.
     * 
     * @throws Hd3dException
     */
    protected void updateTaskActualStartDate() throws Hd3dException
    {
        ITask task = object.getTask();
        List<ITaskActivity> activities = Persistors.taskActivity.getByValue(Const.TASK + Const._ID, object.getTask()
                .getId());

        if (CollectionUtils.isNotEmpty(activities))
        {
            IActivity firstActivity = activities.get(0);
            for (IActivity activity : activities)
            {
                if (firstActivity.getDay() != null && activity.getDay() != null
                        && activity.getDay().getDate().before(firstActivity.getDay().getDate()))
                    firstActivity = activity;
            }
            task.setActualStartDate(firstActivity.getDay().getDate());
        }
        else
        {
            task.setActualStartDate(null);
        }
        task.doPreUpdate();
        Persistors.task.persist(task);
        task.doPostUpdate();
    }
}
