package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.DYNMETADATATYPE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class DynMetaDataTypeResource extends AbstractHd3dResource<ILDynMetaDataType, IDynMetaDataType>
{
    public DynMetaDataTypeResource() throws Hd3dException
    {
        super(Persistors.dynmetadatatype, Translators.dynmetadatatype);
    }

    @Override
    protected IDynMetaDataType initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(DYNMETADATATYPE_ID_ATTRIBUTE, lockMode);
    }
}
