/**
 * 
 */
package fr.hd3d.services.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.representation.OutputRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.Log;


public class ExportCastingOdsResource extends ServerResource
{

    private Collection<ICategory> categories;
    private List<IShot> shots;
    private int indexRow = 0;

    // private List<IComposition> compositions;
    private HashMap<Long, IConstituent> constituentsById = new HashMap<Long, IConstituent>();
    private HashMap<Long, Integer> indexCellByCategoryId = new HashMap<Long, Integer>();

    public ExportCastingOdsResource()
    {
        init(Context.getCurrent(), Request.getCurrent(), Response.getCurrent());

    }

    @Override
    protected Representation get() throws ResourceException
    {
        try
        {
            final Long projectID = getIdFromUrl(ServicesURI.PROJECT_ID_ATTRIBUTE);
            IProject project = Persistors.project.getById(projectID);
            if (project == null)
            {
                return getResponseEntity();
            }

            categories = project.rootCategory().getChildren();
            shots = Persistors.shot.getByValue("sequence.project.id", projectID);

            // compositions = Persistors.composition.getByValueIn("shot", CollectUtils.getIds(shots));

            OutputRepresentation out = new OutputRepresentation(MediaType.APPLICATION_OPENOFFICE_ODS) {

                @Override
                public void write(OutputStream stream) throws IOException
                {
                    HSSFWorkbook wb = new HSSFWorkbook();
                    Sheet sheet = wb.createSheet("Casting");

                    setHeaders(sheet);
                    setShots(sheet);
                    wb.write(stream);
                }
            };
            Disposition disposition = new Disposition();
            disposition.setFilename("Casting_" + project.getName() + ".ods");
            disposition.setType(Disposition.TYPE_INLINE);
            out.setDisposition(disposition);
            return out;
        }
        catch (Exception e)
        {
            return getResponseEntity();
        }
    }

    protected void setShots(Sheet sheet)
    {
        for (IShot shot : shots)
        {
            try
            {
                List<IComposition> compositions = Persistors.composition.getByValue("shot.id", shot.getId());
                setRows(sheet, shot, compositions);
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error("Error during get composition from shot : id=" + shot.getId());
            }
        }
    }

    private void setRows(Sheet sheet, IShot shot, List<IComposition> compositions) throws Hd3dException
    {
        HashMap<Long, StringBuilder> valueByCategoryId = new HashMap<Long, StringBuilder>();
        Integer cellIndex = 0;
        Row row = sheet.createRow(indexRow++);
        row.createCell(cellIndex++).setCellValue(shot.getPath());
        row.createCell(cellIndex++).setCellValue(shot.getLabel());
        row.createCell(cellIndex++).setCellValue(shot.getNbFrame());

        for (IComposition composition : compositions)
        {
            IConstituent constituent = constituentsById.get(composition.getConstituent().getId());
            if (constituent == null)
            {
                constituent = Persistors.constituent.getById(composition.getConstituent().getId());
            }
            ICategory[] constituentCategory = constituent.parents();
            if (constituentCategory.length > 1)
            {
                Long categoryId = constituentCategory[1].getId();
                StringBuilder value = valueByCategoryId.get(categoryId);
                if (value == null)
                {
                    value = new StringBuilder();
                    valueByCategoryId.put(categoryId, value);
                }
                value.append(constituent.getLabel());
                if (composition.getNbOccurence() > 0)
                {
                    value.append('X').append(composition.getNbOccurence());
                }
                value.append(", ");
            }

        }

        for (Long id : valueByCategoryId.keySet())
        {
            cellIndex = indexCellByCategoryId.get(id);
            if (cellIndex == null)
            {
                Log.LOGGER.error("ExportCastingODSResource -> setRows : indexCell not find for category id= " + id);
                continue;
            }
            StringBuilder value = valueByCategoryId.get(id);
            if (value == null)
            {
                Log.LOGGER.error("ExportCastingODSResource -> setRows : value is null for category id= " + id);
                continue;
            }
            value.setLength(value.length() - 2);
            row.createCell(cellIndex).setCellValue(value.toString());
        }

    }

    protected void setHeaders(Sheet sheet)
    {
        int indexCell = 0;
        Row row = sheet.createRow(indexRow++);
        row.createCell(indexCell++).setCellValue("Sequence");
        row.createCell(indexCell++).setCellValue("Shot");
        row.createCell(indexCell++).setCellValue("Nb Frames");
        for (ICategory category : categories)
        {
            indexCellByCategoryId.put(category.getId(), indexCell);
            row.createCell(indexCell++).setCellValue(category.getName());
        }

    }

    /**
     * Return the id long from the id string of the url.
     * 
     * @param idString
     * @return the id long
     */
    public Long getIdFromUrl(String idString)
    {
        return NumberUtils.toLong((String) getRequest().getAttributes().get(idString));
    }

}
