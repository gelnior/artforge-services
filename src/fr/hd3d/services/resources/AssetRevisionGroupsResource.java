package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class AssetRevisionGroupsResource extends AbstractHd3dCollection<ILAssetRevisionGroup, IAssetRevisionGroup>
{

    public AssetRevisionGroupsResource() throws Hd3dException
    {
        super(Persistors.assetrevisiongroup, Translators.assetrevisiongroup);
    }

}
