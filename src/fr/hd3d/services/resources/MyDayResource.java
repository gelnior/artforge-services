package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.DAY_ID_ATTRIBUTE;

import org.hibernate.LockMode;
import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.utils.AuthenticationUtil;


/**
 * @author david gauthier
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'une journée et les activités\r\n" + "\r\nPUT:Mise a jour d'une journée\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class MyDayResource extends AbstractHd3dResource<ILPersonDay, IPersonDay>
{
    public MyDayResource() throws Hd3dException
    {
        super(Persistors.personDay, Translators.personDay);
    }

    @Override
    protected IPersonDay initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(DAY_ID_ATTRIBUTE, lockMode);
    }

    @Override
    protected void doPreUpdate(Representation entity, ILPersonDay personDay) throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user == null)
            return;
        if (user.getId().longValue() == personDay.getPersonID().longValue())
        {
            super.doPreUpdate(entity, personDay);
        }
        else
        {
            throw new Hd3dException("You cannot modify day you don't own.");
        }
    }

    @Override
    protected void doPreDelete() throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user == null)
            return;
        if (user.getId().longValue() == object.getPerson().getId().longValue())
        {
            super.doPreDelete();
        }
        else
        {
            throw new Hd3dException("You cannot delete day you don't own.");
        }
    }
}
