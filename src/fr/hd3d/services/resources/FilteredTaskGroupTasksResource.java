package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTaskBase;
import fr.hd3d.model.persistence.ITaskBase;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetFilteredTaskGroupTasksCmd;
import fr.hd3d.model.translator.Translators;


public class FilteredTaskGroupTasksResource extends AbstractHd3dCollection<ILTaskBase, ITaskBase>
{
    public FilteredTaskGroupTasksResource() throws Hd3dException
    {
        super(Persistors.taskbase, Translators.taskBase);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetFilteredTaskGroupTasksCmd(getResourceContext()).wantTotalSize());
    }
}
