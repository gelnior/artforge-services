package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.common.client.ServicesURI.FILEREVISIONS;
import static fr.hd3d.common.client.ServicesURI.UPLOAD;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.shiro.util.StringUtils;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.FileRevisionUtils;
import fr.hd3d.utils.FilenameMaker;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.TagUtils;


@Hd3dComment("Webservice d'upload de fichiers. <br>\n"
        + "POST/Multipart: [file]= fichier à uploader; [thumbnail]= imagette correspondant au fichier; [preview]= vidéo de preview correspondant au fichier; <br>\n"
        + "[catId]/[constId]/[seqId]/[shotId]= id du parent à qui lier le fichier; [tagLabel]= label du tag à créer/associer au fichier; [tagId]= id du tag à associer au fichier; <br>\n"
        + "[tagIdArray]/[tagLabelArray]= la même chose mais séparé par des ',' pour en passer plusieurs"
        + "<br>\r\nPUT:N/A\r\n" + "<br>\r\nDELETE:N/A")
public class UploadResource extends AbstractHd3dUploadBaseResource<ILFileRevision, IFileRevision>
{
    private final static String NEW_FILE_REVISION = "newFileRevision";
    private final static String FILE = "file";
    private final static String THUMB_FILE = "thumbFile";
    private final static String PREVIEW_FILE = "previewFile";
    private final static String TAG_ID = "tagId";
    private final static String TAG_IDS = "tagIds";
    private final static String TAG_LABEL = "tagLabel";
    private final static String TAG_ID_ARRAY = "tagIdArray";
    private final static String TAG_LABEL_ARRAY = "tagLabelArray";
    private final static String CAT_ID = "catId";
    private final static String CONST_ID = "constId";
    private final static String SEQ_ID = "seqId";
    private final static String SHOT_ID = "shotId";
    private final static String REVISION = "revision";
    private final static String VARIATION = "variation";
    private final static String ASSET_ID = "assetId";

    /**
     * @param context
     * @param request
     * @param response
     * @return
     * @throws Hd3dException
     */
    public UploadResource() throws Hd3dException
    {
        super();
        allowMethods("POST");
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void parseTextFields(Map<String, Object> resultMap, FileItem fileItem)
    {
        if (resultMap == null || fileItem == null || !fileItem.isFormField())
            return;

        final String fieldName = fileItem.getFieldName();
        final String fieldValue = fileItem.getString();

        /* list to store tag ids */
        List<Long> tagIds = (List<Long>) resultMap.get(TAG_IDS);
        if (tagIds == null)
            tagIds = new ArrayList<Long>();

        /* parsing text fields */
        if (fieldName.equals(TAG_ID))
        {
            final Long tagId = NumberUtils.toLong(fieldValue);
            if (TagUtils.tagExists(tagId))
                tagIds.add(tagId);
        }
        else if (fieldName.equals(TAG_LABEL))
        {
            tagIds.add(TagUtils.getOrCreateTagIdFromName(fieldValue));
        }
        else if (fieldName.equals(TAG_ID_ARRAY))
        {
            for (String id : StringUtils.split(fieldValue, ','))
            {
                final Long tagId = NumberUtils.toLong(id);
                if (TagUtils.tagExists(tagId))
                    tagIds.add(tagId);
            }
        }
        else if (fieldName.equals(TAG_LABEL_ARRAY))
        {
            for (String id : StringUtils.split(fieldValue, ','))
            {
                Long newid = TagUtils.getOrCreateTagIdFromName(id);
                tagIds.add(newid);
            }
        }
        else if (fieldName.equals(CAT_ID))
        {
            resultMap.put(CAT_ID, NumberUtils.toLong(fieldValue));
        }
        else if (fieldName.equals(CONST_ID))
        {
            resultMap.put(CONST_ID, NumberUtils.toLong(fieldValue));
        }
        else if (fieldName.equals(SEQ_ID))
        {
            resultMap.put(SEQ_ID, NumberUtils.toLong(fieldValue));
        }
        else if (fieldName.equals(SHOT_ID))
        {
            resultMap.put(SHOT_ID, NumberUtils.toLong(fieldValue));
        }
        else if (fieldName.equals(REVISION))
        {
            resultMap.put(REVISION, NumberUtils.toInt(fieldValue));
        }
        else if (fieldName.equals(VARIATION))
        {
            resultMap.put(VARIATION, fieldValue);
        }
        else if (fieldName.equals(ASSET_ID))
        {
            resultMap.put(ASSET_ID, NumberUtils.toLong(fieldValue));
        }

        /* do not forget to put back in the map */
        resultMap.put(TAG_IDS, tagIds);
    }

    @Override
    protected void parseUploadedFiles(Map<String, Object> resultMap, FileItem fileItem)
    {
        if (resultMap == null || fileItem == null || fileItem.isFormField())
            return;

        final String fileName = fileItem.getFieldName();

        /* parsing uploaded files */
        if (FILE.equals(fileName))
        {
            resultMap.put(FILE, fileItem);
        }
        else if ("thumbnail".equals(fileName))
        {
            // TODO check jpg extension/format/resolution else nconvert
            // Also, if not given... trying to generate it
            String tmpFilename = FilenameMaker.getTemporaryFileFullPath() + "_thumb";
            File thumbFile = new File(tmpFilename);
            try
            {
                /* persist the uploaded file in temporary directory */
                fileItem.write(thumbFile);
                if (thumbFile.length() == 0)
                {
                    thumbFile.delete();
                    thumbFile = null;
                }
            }
            catch (Exception e)
            {
                thumbFile = null;
            }

            resultMap.put(THUMB_FILE, thumbFile);
        }
        else if ("preview".equals(fileName))
        {
            // TODO check flv extension/format else ffmpeg
            String tmpFilename = FilenameMaker.getTemporaryFileFullPath() + "_preview";
            File previewFile = new File(tmpFilename);
            try
            {
                /* persist the uploaded file in temporary directory */
                fileItem.write(previewFile);
                if (previewFile.length() == 0)
                {
                    previewFile.delete();
                    previewFile = null;
                }
            }
            catch (Exception e)
            {
                previewFile = null;
            }

            resultMap.put(PREVIEW_FILE, previewFile);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void process(Map<String, Object> resultMap) throws Hd3dException
    {
        FileItem file = (FileItem) resultMap.get(FILE);
        Integer revision = (Integer) resultMap.get(REVISION);
        String variation = (String) resultMap.get(VARIATION);
        File thumbFile = (File) resultMap.get(THUMB_FILE);
        File previewFile = (File) resultMap.get(PREVIEW_FILE);
        List<Long> tagIds = (List<Long>) resultMap.get(TAG_IDS);
        Long assetId = (Long) resultMap.get(ASSET_ID);

        String destFolder = "";
        String mountPoint = null;
        IAssetRevision assetRevision = null;
        if (assetId != null)
        {

            assetRevision = Persistors.assetrevision.getById(assetId);
            if (assetRevision == null)
            {
                throw new Hd3dException("There are no asset to upload file !");
            }
            destFolder = assetRevision.getWipPath();
            if (destFolder == null)
            {
                throw new Hd3dException("The WIP path of this asset is empty. It couldn't upload the file.");
            }
            mountPoint = assetRevision.getMountPointWipPath();

        }
        IFileRevision fileRevision = FileRevisionUtils.persistUploadedFile(mountPoint, destFolder, file,
                file.getName(), variation, revision, EFileState.getDefault(), assetRevision);

        resultMap.put(NEW_FILE_REVISION, fileRevision);

        /* Renaming properly uploaded files */
        if (previewFile != null)
        {
            File destFile = new File(FilenameMaker.getMoviePreviewFromPath(fileRevision));

            if (previewFile.renameTo(destFile))
            {
                Log.LOGGER.info("*** Preview file: '{}'", destFile.getAbsolutePath());
            }
            else
            {
                Log.LOGGER.error("*** Error: could not rename preview from '{}' to '{}'\n", previewFile
                        .getAbsolutePath(), destFile.getAbsolutePath());
                previewFile.delete();
            }
        }
        if (thumbFile != null)
        {
            File destFile = new File(FilenameMaker.getImagePreviewFromPath(fileRevision));

            if (thumbFile.renameTo(destFile))
            {
                Log.LOGGER.info("*** Thumbnail file: '{}'", destFile.getAbsolutePath());
            }
            else
            {
                Log.LOGGER.error("*** Error: could not rename thumb from '{}' to '{}'\n", thumbFile.getAbsolutePath(),
                        destFile.getAbsolutePath());
                thumbFile.delete();
            }
        }

        try
        {
            /* Adding the tags */
            if (CollectUtils.isNotEmpty(tagIds))
            {
                for (final Long tagId : tagIds)
                    fileRevision.addTag(tagId);
            }

        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error("*** Error while accessing database. \n");
            error(Status.CLIENT_ERROR_BAD_REQUEST, e, ERROR_OCCURED);
        }
    }

    @Override
    protected Representation buildRepresentation(Map<String, Object> resultMap)
    {
        IFileRevision fileRevision = (IFileRevision) resultMap.get(NEW_FILE_REVISION);

        Representation rep = null;
        if (fileRevision == null)
        {
            rep = new StringRepresentation("0", MediaType.TEXT_PLAIN);
        }
        else
        {
            /* file successfully uploaded */
            rep = new StringRepresentation(String.valueOf(fileRevision.getId()));
            rep.setLocationRef(this.getNewResourceIdentifier(fileRevision));
        }

        return rep;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        StringBuilder buf = new StringBuilder(getRequest().getResourceRef().getBaseRef().toString());
        buf.delete(buf.lastIndexOf('/' + UPLOAD), buf.length());
        buf.append('/' + FILEREVISIONS).append('/').append(((IBase) object).getId());
        return buf.toString();
    }
}
