package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.QUALIFICATION_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILQualification;
import fr.hd3d.model.persistence.IQualification;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computers/{id}
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'un qualification de machines\r\n"
        + "\r\nPUT:Modification d'un qualification de machines\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Désactivation d'un qualifications de machines")
public class QualificationResource extends AbstractHd3dResource<ILQualification, IQualification>
{
    @Override
    protected IQualification initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(QUALIFICATION_ID_ATTRIBUTE, lockMode);
    }

    public QualificationResource() throws Hd3dException
    {
        super(Persistors.qualification, Translators.qualification);
    }
}
