package fr.hd3d.services.resources;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILAssetRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetAssetAbstractRevisionsCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.model.translator.impl.BaseTranslator;
import fr.hd3d.services.translator.json.JSonFormSerializer;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


@Hd3dComment("<ul>"
        + "<li>GET : list of revisions for an asset</li>"
        + "<li>POST : Creates a revision for this asset.<br/>Needed fields : lastUser, status, validity, comment. Other fields deducted from previous versions.</li>"
        + "</ul>")
public class AssetAbstractRevisionsResource extends AbstractHd3dCollection<ILAssetRevision, IAssetRevision>
{
    public AssetAbstractRevisionsResource() throws Hd3dException
    {
        super(Persistors.assetrevision, Translators.assetrevision);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetAssetAbstractRevisionsCmd(getResourceContext()).wantTotalSize());
    }

    @Override
    protected IAssetRevision newObject(Representation entity) throws Hd3dException
    {
        final ILAssetRevision lObject = JSonFormSerializer.<ILAssetRevision> deserialize(entity);
        {
            Date now = new Date();

            // Get the asset revisions for the current context
            IAssetRevision last = getLastAssetRevision();

            // Set all the IAsset fields correctly
            lObject.setProject(last.getProject() != null ? last.getProject().getId() : null);
            lObject.setKey(last.getKey());
            lObject.setVariation(last.getVariation());
            lObject.setRevision(last.getRevision() + 1);
            lObject.setTaskType(BaseTranslator.nullSafeId(last.getTaskType()));
            lObject.setName(last.getName());
            lObject.setCreator(lObject.getLastUser());
            lObject.setCreationDate(now);
            lObject.setLastOperationDate(now);
            lObject.setStatus(Const.UNLOCKED);
            lObject.setVersion(new Timestamp(now.getTime()));
        }

        return getResourceContext().getTranslator().fromLightweight(lObject, getResourceContext());
    }

    private IAssetRevision getLastAssetRevision()
    {
        final GetAssetAbstractRevisionsCmd cmd = new GetAssetAbstractRevisionsCmd(getResourceContext());
        try
        {
            HibernateUtil.safeQuery(cmd);
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            return null;
        }

        List<IAssetRevision> currentRevisions = cmd.result;
        return currentRevisions.get(Math.max(0, currentRevisions.size() - 1));
    }
}
