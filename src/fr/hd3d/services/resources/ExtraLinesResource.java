package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILExtraLine;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetFilteredExtraLinesCmd;
import fr.hd3d.model.translator.Translators;


public class ExtraLinesResource extends AbstractHd3dCollection<ILExtraLine, IExtraLine>
{
    public ExtraLinesResource() throws Hd3dException
    {
        super(Persistors.extraline, Translators.extraLine);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetFilteredExtraLinesCmd(getResourceContext()).wantTotalSize());
    }
}
