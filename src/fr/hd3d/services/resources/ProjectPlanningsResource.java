package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectPlanningsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author michael.guiral
 * 
 */
public class ProjectPlanningsResource extends AbstractHd3dCollection<ILPlanning, IPlanning>
{
    public ProjectPlanningsResource() throws Hd3dException
    {
        super(Persistors.planning, Translators.planning);
    }

    @Override
    protected void doPostPersist(IPlanning object) throws Hd3dException
    {
        super.doPostPersist(object);
        if (object.getProject() != null)
        {
            // IProject project = object.getProject();
            // TaskGroupH taskGroupHibernateImpl = new TaskGroupH();
            // taskGroupHibernateImpl.setName(project.getName() + " - " + object.getName());
            // taskGroupHibernateImpl.setColor(project.getColor());
            // taskGroupHibernateImpl.setPlanning(object);
            // taskGroupHibernateImpl.setStartDate(object.getStartDate());
            // taskGroupHibernateImpl.setEndDate(object.getEndDate());
            // Persistors.taskGroup.persist(taskGroupHibernateImpl);
        }
    }

    @Override
    protected ResultCollection<IPlanning> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectPlanningsCmd(getResourceContext()).wantTotalSize());
    }
}
