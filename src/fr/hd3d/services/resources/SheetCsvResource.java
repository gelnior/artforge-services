package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.data.CharacterSet;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.util.Triple;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.impl.LApprovalNote;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.SheetUtils;


/**
 * This resource build a CSV file based on sheet data (data usually returned at JSON format). All cells printed in CSV
 * file are built with the toString() method of the value object.
 * 
 */
@Hd3dComment("GET: Return sheet data at CSV format.<br />PUT: N/A<br /> POST: N/A<br />DELETE : N/A")
public class SheetCsvResource extends SheetResource
{
    List<IItem> headers = new ArrayList<IItem>();

    public SheetCsvResource() throws Hd3dException
    {
        super();
        allowMethods("");
    }

    /**
     * Build a CSV file based on sheet data then return it with media type set to "application/excel".
     * 
     * @see fr.hd3d.services.resources.AbstractHd3dSingleResource#get(org.restlet.representation.Variant)
     */
    @Override
    public Representation get(Variant variant)
    {
        if (!initObject())
        {
            return getResponseEntity();
        }
        if (object == null)
        {
            /*
             * NOTE: do not set Status to Status.SUCCESS_NO_CONTENT If so, there will be no empty list returned in the
             * HTML screen, which may be confusing
             */
            return getResponseEntity();
        }

        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();

        try
        {
            // get ALL the instances of the bound class
            // Set the query filter.
            Collection<Map<String, Object>> rows = this.getData(session, tx);
            StringBuilder buffer = new StringBuilder();

            ISheet sheet = object;
            this.setHeaders(sheet, buffer);
            this.setRows(rows, buffer);
            this.headers.clear();

            return this.getCSVRrepesentation(sheet, buffer);
        }
        catch (Hd3dPersistenceException e)
        {
            headers.clear();
            error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
            return getResponseEntity();
        }
        catch (Hd3dException e)
        {
            // HD3D Error
            headers.clear();
            error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
            return getResponseEntity();
        }
        catch (Exception e)
        {
            // HD3D Error
            headers.clear();
            error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
            return getResponseEntity();
        }
        finally
        {
            headers.clear();
            tx = null;
            cleanup();
        }
    }

    /**
     * @param sheet
     *            The sheet to print in the CSV file.
     * @param buffer
     *            The string buffer containing the sheet data.
     * @return The representation needed by the resource to send its response.
     */
    private Representation getCSVRrepesentation(ISheet sheet, StringBuilder buffer)
    {
        StringRepresentation representation = new StringRepresentation(buffer, MediaType.APPLICATION_EXCEL);
        representation.setCharacterSet(CharacterSet.UTF_8);
        representation.setDownloadable(true);
        representation.setDownloadName(sheet.getName() + ".csv");

        return representation;
    }

    /**
     * @return Sheet data depending on its items and its filters.
     * @throws Exception
     */
    private Collection<Map<String, Object>> getData(Session session, Transaction tx) throws Exception
    {
        final int nbColumns = SheetUtils.nbColumns(object);

        Triple<Object[][][], Integer, Long[]> triple = process(session, tx, getResourceContext(), nbColumns);

        return buildResponse(safeConvert(triple.getFirst(), nbColumns, getResourceContext()), nbColumns, null);
    }

    /**
     * Print sheet rows in the CSV file.
     * 
     * @param rows
     *            The sheet rows to print.
     * @param buffer
     *            The buffer that contains string data.
     */
    @SuppressWarnings("unchecked")
    private void setRows(Collection<Map<String, Object>> rows, StringBuilder buffer)
    {
        Map<Long, Object> items = new HashMap<Long, Object>();

        for (Map<String, Object> row : rows)
        {
            List<Map<String, Object>> groups = (List<Map<String, Object>>) row.get(Const.GROUPS);
            for (Map<String, Object> group : groups)
            {
                List<Map<String, Object>> itemList = (List<Map<String, Object>>) group.get(Const.ITEMS);
                for (Map<String, Object> item : itemList)
                {
                    items.put((Long) item.get(Const.ID), item.get(ItemGroupResource.VALUE));
                }
            }

            for (IItem header : headers)
            {
                Object value = items.get(header.getId());
                if (value != null
                        || "fr.hd3d.services.resources.collectionquery.StepsByTaskTypeCollectionQuery".equals(header
                                .getQuery()))
                {
                    buffer.append('"');
                    if ("fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery".equals(header
                            .getQuery()))
                    {
                        appendApprovalText(buffer, value);
                    }
                    else if ("fr.hd3d.services.resources.collectionquery.StepsByTaskTypeCollectionQuery".equals(header
                            .getQuery()))
                    {
                        if (value == null)
                            buffer.append("0");
                        else
                            buffer.append(value.toString().replace(",", ","));
                    }
                    else
                    {
                        buffer.append(value.toString().replace(",", ","));
                    }
                    buffer.append('"');

                }
                buffer.append(',');
            }
            buffer.append("\n");
            items.clear();
        }
    }

    /**
     * Convert approval tuple into a string :
     * <ul>
     * <li>If isTask is set to false, it appends "No task" to the buffer.</li>
     * <li>If approval list is empty, it appends "No notes" to the buffer.</li>
     * <li>If approval list is not empty, it appends last approval status and date to the buffer.</li>
     * </ul>
     * 
     * @param buffer
     *            The buffer to extends.
     * @param value
     *            The value to convert into a string.
     */
    @SuppressWarnings("unchecked")
    private void appendApprovalText(StringBuilder buffer, Object value)
    {
        if (value instanceof List<?> && ((List<?>) value).size() > 1)
        {
            List<Object> values = ((List<Object>) value);
            Object tmpIsTasks = values.get(0);
            Object tmpApproval = values.get(1);

            if (tmpIsTasks instanceof Boolean && tmpApproval instanceof List<?>)
            {
                Boolean isTasks = (Boolean) values.get(0);
                List<LApprovalNote> approvals = (List<LApprovalNote>) tmpApproval;
                if (isTasks)
                {
                    if (approvals.size() > 0)
                    {
                        buffer.append(approvals.get(0).toString());
                    }
                    else
                    {
                        buffer.append("No notes");
                    }
                }
            }
            else
            {
                buffer.append("No tasks");
            }
        }
    }

    /**
     * Print sheet headers and store them locally.
     * 
     * @param sheet
     *            The sheet to print.
     * @param buffer
     *            The buffer that contains string data.
     */
    private void setHeaders(ISheet sheet, StringBuilder buffer)
    {
        if (sheet != null)
        {
            for (IItemGroup group : CollectUtils.selectNotNull(sheet.getItemGroups()))
            {
                if (group.getItems() != null)
                {
                    for (IItem item : group.getItems())
                    {
                        headers.add(item);
                        buffer.append(item.getName()).append(',');
                    }
                }
            }

            buffer.append("\n");
        }
    }
}
