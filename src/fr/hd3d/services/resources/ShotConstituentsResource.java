package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetShotConstituentsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * ConstituentShotsResource returns constituents linked to shot via compositions.
 * 
 * @author HD3D
 */
public class ShotConstituentsResource extends AbstractHd3dCollection<ILConstituent, IConstituent>
{
    public ShotConstituentsResource() throws Hd3dException
    {
        super(Persistors.constituent, Translators.constituent);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetShotConstituentsCmd(getResourceContext()).wantTotalSize());
    }
}
