package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPlanningPlannedTasks;
import fr.hd3d.model.translator.Translators;


public class PlanningPlannedTasksResource extends AbstractHd3dCollection<ILTask, ITask>
{

    public PlanningPlannedTasksResource() throws Hd3dException
    {
        super(Persistors.task, Translators.task);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetPlanningPlannedTasks(getResourceContext()).wantTotalSize());
    }
}
