package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetGroupRoleCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.UniqueResultQuery;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


/**
 * 
 * @author florent-della-valle
 * 
 */
public class GroupRoleResource extends AbstractHd3dResource<ILRole, IRole>
{
    public GroupRoleResource() throws Hd3dException
    {
        super(Persistors.role, Translators.role);
    }

    @Override
    protected IRole initializeObject(LockMode lockMode) throws Hd3dException
    {
        final UniqueResultQuery<ILRole, IRole> cmd = new GetGroupRoleCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        IRole obj = (IRole) cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }

}
