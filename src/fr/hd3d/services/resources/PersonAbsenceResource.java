package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILAbsence;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonAbsenceCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.UniqueResultQuery;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


/**
 * A resource to access to a specific person's activity.
 * 
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET: Absence Retrieval <br />" + "PUT: Absence Update<br />" + "\r\nPOST:N/A<br />" + "DELETE:N/A<br />")
public class PersonAbsenceResource extends AbstractHd3dResource<ILAbsence, IAbsence>
{
    public PersonAbsenceResource() throws Hd3dException
    {
        super(Persistors.absence, Translators.absence);
    }

    @Override
    protected IAbsence initializeObject(LockMode lockMode) throws Hd3dException
    {
        final UniqueResultQuery<ILAbsence, IAbsence> cmd = new GetPersonAbsenceCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        IAbsence obj = (IAbsence) cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }
}
