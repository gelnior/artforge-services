package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.FILEREVISION_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevisionLink;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class FileRevisionLinkResource extends AbstractHd3dResource<ILFileRevisionLink, IFileRevisionLink>
{

    public FileRevisionLinkResource() throws Hd3dException
    {
        super(Persistors.filerevisionlink, Translators.filerevisionlink);
    }

    @Override
    protected IFileRevisionLink initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(FILEREVISION_ID_ATTRIBUTE, lockMode);
    }
}
