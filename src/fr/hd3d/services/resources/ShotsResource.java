package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ShotsResource extends AbstractHd3dCollection<ILShot, IShot>
{
    public ShotsResource() throws Hd3dException
    {
        super(Persistors.shot, Translators.shot);
    }
}
