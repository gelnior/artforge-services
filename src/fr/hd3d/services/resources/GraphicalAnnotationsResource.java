package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILGraphicalAnnotation;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class GraphicalAnnotationsResource extends AbstractHd3dCollection<ILGraphicalAnnotation, IGraphicalAnnotation>
{

    public GraphicalAnnotationsResource() throws Hd3dException
    {
        super(Persistors.graphicalannotation, Translators.graphicalannotation);
    }
}
