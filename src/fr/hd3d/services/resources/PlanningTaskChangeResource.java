package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTaskChanges;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPlanningTaskChangeCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.UniqueResultQuery;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


public class PlanningTaskChangeResource extends AbstractHd3dResource<ILTaskChanges, ITaskChanges>
{
    public PlanningTaskChangeResource() throws Hd3dException
    {
        super(Persistors.taskChanges, Translators.taskChanges);
    }

    @Override
    protected ITaskChanges initializeObject(LockMode lockMode) throws Hd3dException
    {
        final UniqueResultQuery<ILTaskChanges, ITaskChanges> cmd = new GetPlanningTaskChangeCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        ITaskChanges obj = (ITaskChanges) cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }
}
