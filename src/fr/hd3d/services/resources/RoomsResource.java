package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILRoom;
import fr.hd3d.model.persistence.IRoom;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer pool objects.<br>
 * URI : /pools/
 * 
 * @author HD3D
 */
public class RoomsResource extends AbstractHd3dCollection<ILRoom, IRoom>
{
    public RoomsResource() throws Hd3dException
    {
        super(Persistors.room, Translators.room);
    }
}
