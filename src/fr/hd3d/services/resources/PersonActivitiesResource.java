package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonActivitiesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * A resource to access to specific person activities.
 * 
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET: Retrieve person activity list. <br />\r\n" + "POST: Create an activity for person.<br />\r\n")
public class PersonActivitiesResource extends AbstractHd3dCollection<ILActivity, IActivity>
{
    public PersonActivitiesResource() throws Hd3dException
    {
        super(Persistors.activity, Translators.activity);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetPersonActivitiesCmd(getResourceContext()).wantTotalSize());
    }
}
