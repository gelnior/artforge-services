package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.CATEGORY_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetCategoriesCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.translator.json.JSonFormSerializer;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des sous-catégories la catégorie indiquée\r\n"
        + "\r\nPUT:Changement de nom d'une catégorie (ou sous-catégorie)\r\n"
        + "\r\nPOST:Ajout d'une SOUS-CATEGORIE par le nom\r\n"
        + "\r\nDELETE:Suppression d'une catégorie (ou sous-catégorie) et de tous ses fils.")
public class CategoriesResource extends AbstractHd3dCollection<ILCategory, ICategory>
{
    public CategoriesResource() throws Hd3dException
    {
        super(Persistors.category, Translators.category);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetCategoriesCmd(getResourceContext()).wantTotalSize());
    }

    protected ICategory newObject(Representation entity) throws Hd3dException
    {
        ILCategory lcategory = JSonFormSerializer.<ILCategory> deserialize(entity);
        final Long categoryId = getResourceContext().getIdFromUrl(CATEGORY_ID_ATTRIBUTE);
        lcategory.setParent(categoryId);
        ICategory category = getResourceContext().getTranslator().fromLightweight(lcategory, getResourceContext());
        return category;
    }

    protected List<ICategory> newCollection(Representation entity) throws Hd3dException
    {
        List<ILCategory> lObjects = JSonFormSerializer.<ILCategory> deserializeList(entity);
        final Long categoryId = getResourceContext().getIdFromUrl(CATEGORY_ID_ATTRIBUTE);

        List<ICategory> objects = new ArrayList<ICategory>(lObjects.size());
        for (ILCategory light : lObjects)
        {
            light.setParent(categoryId);
            objects.add(getResourceContext().getTranslator().fromLightweight(light, getResourceContext()));
        }
        return objects;
    }

    // @Override
    // protected void doPrePersist(Representation entity, Map<String, String> map, ICategory category)
    // throws Hd3dTranslationException
    // {
    // category.doPrePersist();
    // executeScripts(category, IPersist.PREPERSIST, getNewIdentifier(category));
    // }

    // @Override
    // protected void doPrePersistCollection(Representation entity, Map<String, String> map, List<ICategory> objects)
    // throws Hd3dTranslationException
    // {
    // // PrePersist
    // for (ICategory object : objects)
    // object.doPrePersist();
    // executeScripts(objects.get(0), IPersist.PREPERSIST, getNewIdentifier(objects));
    // }
}
