package fr.hd3d.services.resources;

import java.util.List;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILScript;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ScriptH;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class ScriptsResource extends AbstractHd3dCollection<ILScript, IScript>
{

    public ScriptsResource() throws Hd3dException
    {
        super(Persistors.script, Translators.script);
    }

    @Override
    protected void doPostPersist(IScript object) throws Hd3dException
    {
        super.doPostPersist(object);
        /* refresh in-memory scripts table */
        ScriptH.refreshScriptsMap = true;
    }

    @Override
    protected void doPostPersist(List<IScript> objects) throws Hd3dException
    {
        super.doPostPersist(objects);
        /* refresh in-memory scripts table */
        ScriptH.refreshScriptsMap = true;
    }

    @Override
    protected void doPostUpdate(List<IScript> objects, Representation entity) throws Hd3dException
    {
        super.doPostUpdate(objects, entity);
        /* refresh in-memory scripts table */
        ScriptH.refreshScriptsMap = true;
    }
}
