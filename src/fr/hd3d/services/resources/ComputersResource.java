package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILComputer;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer objects.<br>
 * URI : /computers/
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération de toutes les machines\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:Création d'une machine\r\n"
        + "\r\nDELETE:N/A")
public class ComputersResource extends AbstractHd3dCollection<ILComputer, IComputer>
{

    public ComputersResource() throws Hd3dException
    {
        super(Persistors.computer, Translators.computer);
    }
}
