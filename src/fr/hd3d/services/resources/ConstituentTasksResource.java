package fr.hd3d.services.resources;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetWorkObjectTasksCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.Const;


/**
 * ShotTasksResource returns tasks linked to given shot via entity task links.
 * 
 * @author HD3D
 */
public class ConstituentTasksResource extends AbstractHd3dCollection<ILTask, ITask>
{
    public ConstituentTasksResource() throws Hd3dException
    {
        super(Persistors.task, Translators.task);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetWorkObjectTasksCmd(getResourceContext(), ServicesURI.CONSTITUENT_ID_ATTRIBUTE,
                Const.CONSTITUENT).wantTotalSize());
    }
}
