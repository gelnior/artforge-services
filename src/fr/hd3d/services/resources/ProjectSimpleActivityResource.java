/**
 * 
 */
package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.SIMPLE_ACTIVITY_ID_ATTRIBUTE;

import org.hibernate.LockMode;
import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Access for modification and delete of an identified simple activity.
 * 
 * @author HD3D
 */
@Hd3dComment("GET: Retrieve simple activity where ID is equal to {simpleActivityID}\r\n"
        + "\r\nPUT: Modify simple activity where ID is equal to {simpleActivityID} \r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE: Delete simple activity where ID is equal to {simpleActivityID}")
public class ProjectSimpleActivityResource extends AbstractHd3dResource<ILSimpleActivity, ISimpleActivity>
{
    public ProjectSimpleActivityResource() throws Hd3dException
    {
        super(Persistors.simpleActivity, Translators.simpleActivity);
    }

    @Override
    protected void doPreUpdate(Representation entity, ILSimpleActivity simpleActivity) throws Hd3dException
    {
        IProject project = Persistors.project.getById(getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE));

        if (project.getId().longValue() == simpleActivity.getProject().getId().longValue())
        {
            super.doPreUpdate(entity, simpleActivity);
        }
        else
        {
            throw new Hd3dException("You cannot modify activity from another project.");
        }
    }

    @Override
    protected void doPreDelete() throws Hd3dException
    {
        IProject project = Persistors.project.getById(getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE));

        if (project.getId().longValue() == object.getProject().getId().longValue())
        {
            super.doPreDelete();
        }
        else
        {
            throw new Hd3dException("You cannot delete activity from another project.");
        }
    }

    @Override
    protected ISimpleActivity initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(SIMPLE_ACTIVITY_ID_ATTRIBUTE, lockMode);
    }

}
