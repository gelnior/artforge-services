package fr.hd3d.services.resources;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.impl.LConstituent;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ConstituentH;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.Bench;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


/**
 * Test class to initialize DB with lot of Constituents in "MAIN" category. It must be run AFTER InitPasta.
 * 
 * @author Nicolas Provost
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("Initialise la base de données avec le jeu de tests PAT & STAN.")
public class InitializeBunchConstituentResource<L, P> extends AbstractHd3dBaseResource
{
    /**
     * Default Constructor
     * 
     * @param context
     *            Context parameters
     * @param request
     *            Request received
     * @param response
     *            Response to send
     * @throws Hd3dException
     */
    public InitializeBunchConstituentResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    private static void run(IHibernateTransaction tx) throws Hd3dPersistenceException
    {
        Bench b = new Bench();
        b.b();
        Session session = HibernateUtil.currentSession();
        Transaction trx = session.beginTransaction();
        HibernateUtil.safeTransaction(tx);
        if (session.getFlushMode() == FlushMode.MANUAL)
        {
            session.flush();
        }
        trx.commit();
        trx = null;
        HibernateUtil.closeSession();

        Log.LOGGER.debug("{} took: {} ms", tx.getClass().getName(), b.e());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.restlet.resource.Resource#getRepresentation(org.restlet.resource. Variant)
     */
    @Override
    public Representation get(Variant arg0)
    {

        try
        {
            BaseH.init();

            // Library tree
            // run(new InitLibraryStructure());
            run(new InitLibraryContent());

            // temporal tree
            // run(new InitTemporalStructure());
            // run(new InitTemporalContent());
            //
            // // casting data
            // run(new InitCasting());
            //
            // // task data
            // run(new InitPlanning());
            //
            // // Suivi data
            // run(new InitSuivi());

        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
            return new StringRepresentation(e.getMessage());
        }
        return new StringRepresentation("database pastafied...");
    }

    /**
     * creates constituents for Pasta
     */
    private class InitLibraryContent implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            // final IProject projectPasta = Persistors.project.getByValue("hook", "PASTA");
            // final Long projectPastaId = projectPasta.getId();
            ICategory category = null;

            // Chars constituents (main)
            category = retrieveCategory("MAIN");

            List<IConstituent> constituents = new ArrayList<IConstituent>(250);
            for (int i = 0; i < 293; i++)
            {
                IConstituent c = new ConstituentH();
                c.setCategory(category);
                c.setHook("CONST_" + i);
                c.setDescription("Constituent " + i);
                c.setDifficulty(1);
                c.setLabel("CONST_" + i);
                c.setCompletion(0);
                constituents.add(c);
            }

            for (IConstituent constituent : constituents)
            {
                constituent.doPrePersist();
            }
            Persistors.constituent.persistCollection(constituents);
            for (IConstituent constituent : constituents)
            {
                constituent.doPostPersist();
            }

            // categoryId = retrieveCategoryId(projectPastaId, "PROPS");
            // IConstituent mug = getConstituent(session, null, categoryId, null, 0, "Mug à café (avec des os)", 0,
            // "MUG",
            // "MUG", 0, null);
            // session.flush();
            // session.refresh(mug);
            // createTask(session, "Design_MUG", 0, mug.getId());
            // createTask(session, "Modeling_MUG", 0, mug.getId());
            // createTask(session, "Setup_MUG", 0, mug.getId());
            //
            // IConstituent calendrier = getConstituent(session, null, categoryId, null, 0,
            // "Calendrier des activités de Stanley", 1, "CALENDRIER", "CALENDRIER", 0, null);
            // session.flush();
            // session.refresh(calendrier);
            // createTask(session, "Design_CALENDRIER", 1, calendrier.getId());
            // createTask(session, "Modeling_CALENDRIER", 1, calendrier.getId());
            // createTask(session, "Setup_CALENDRIER", 1, calendrier.getId());
            //
            // IConstituent clock = getConstituent(session, null, categoryId, null, 0, "Réveil-matin de Pat", 0,
            // "REVEIL",
            // "REVEIL", 0, null);
            // session.flush();
            // session.refresh(clock);
            // createTask(session, "Design_REVEIL", 0, clock.getId());
            // createTask(session, "Modeling_REVEIL", 0, clock.getId());
            // createTask(session, "Setup_REVEIL", 0, clock.getId());
            //
            // IConstituent carA = getConstituent(session, null, categoryId, null, 0, "Véhicule individuel, rouge", 0,
            // "VOITURE1", "VOITURE1", 0, null);
            // session.flush();
            // session.refresh(carA);
            // createTask(session, "Design_VOITURE1", 0, carA.getId());
            // createTask(session, "Modeling_VOITURE1", 0, carA.getId());
            // createTask(session, "Setup_VOITURE1", 0, carA.getId());
            //
            // IConstituent carB = getConstituent(session, null, categoryId, null, 0,
            // "Véhicule pour transport en commun",
            // 0, "VOITURE2", "VOITURE2", 0, null);
            // session.flush();
            // session.refresh(carB);
            // createTask(session, "Design_VOITURE2", 0, carB.getId());
            // createTask(session, "Modeling_VOITURE2", 0, carB.getId());
            // createTask(session, "Setup_VOITURE2", 0, carB.getId());
            //
            // // Sets constituents
            // categoryId = retrieveCategoryId(projectPastaId, "HOMEPATSTAN");
            // IConstituent cuisine = getConstituent(session, null, categoryId, null, 0, "Cuisine", 3, "CUISINE",
            // "CUISINE", 0, null);
            // session.flush();
            // session.refresh(cuisine);
            // createTask(session, "Design_CUISINE", 3, cuisine.getId());
            // createTask(session, "Modeling_CUISINE", 3, cuisine.getId());
            // createTask(session, "Setup_CUISINE", 3, cuisine.getId());
            //
            // IConstituent entree = getConstituent(session, null, categoryId, null, 0, "Entrée de la maison", 3,
            // "ENTREE", "ENTREE", 0, null);
            // session.flush();
            // session.refresh(entree);
            // createTask(session, "Design_ENTREE", 3, entree.getId());
            // createTask(session, "Modeling_ENTREE", 3, entree.getId());
            // createTask(session, "Setup_ENTREE", 3, entree.getId());
            //
            // IConstituent portesfenetres = getConstituent(session, null, categoryId, null, 0,
            // "Portes-fenêtres du salon", 1, "PORTESFENETRES", "PORTESFENETRES", 0, null);
            // session.flush();
            // session.refresh(portesfenetres);
            // createTask(session, "Design_PORTESFENETRES", 1, portesfenetres.getId());
            // createTask(session, "Modeling_PORTESFENETRES", 1, portesfenetres.getId());
            // createTask(session, "Setup_PORTESFENETRES", 1, portesfenetres.getId());
            //
            // IConstituent salon = getConstituent(session, null, categoryId, null, 0, "Salon", 1, "SALON", "SALON", 2,
            // null);
            // session.flush();
            // session.refresh(salon);
            // createTask(session, "Design_SALON", 1, salon.getId());
            // createTask(session, "Modeling_SALON", 1, salon.getId());
            // createTask(session, "Setup_SALON", 1, salon.getId());
            //
            // IConstituent bedRoom = getConstituent(session, null, categoryId, null, 0, "Chambre à coucher", 1,
            // "CHAMBRE", "CHAMBRE", 2, null);
            // session.flush();
            // session.refresh(bedRoom);
            // createTask(session, "Design_CHAMBRE", 1, bedRoom.getId());
            // createTask(session, "Modeling_CHAMBRE", 1, bedRoom.getId());
            // createTask(session, "Setup_CHAMBRE", 1, bedRoom.getId());
            //
            // // Props constituents (others)
            // categoryId = retrieveCategoryId(projectPastaId, "SETS");
            // IConstituent home = getConstituent(session, null, categoryId, null, 0, "Maison, vue de l'extérieur", 1,
            // "MAISON", "MAISON", 2, null);
            // session.flush();
            // session.refresh(home);
            // createTask(session, "Design_MAISON", 1, home.getId());
            // createTask(session, "Modeling_MAISON", 1, home.getId());
            // createTask(session, "Setup_MAISON", 1, home.getId());
            //
            // IConstituent rue = getConstituent(session, null, categoryId, null, 0, "Vue de la rue principale", 1,
            // "GRANDRUE", "GRANDRUE", 2, null);
            // session.flush();
            // session.refresh(rue);
            // createTask(session, "Design_GRANDRUE", 1, rue.getId());
            // createTask(session, "Modeling_GRANDRUE", 1, rue.getId());
            // createTask(session, "Setup_GRANDRUE", 1, rue.getId());
            //
            // session.flush();
        }
    }

    // /*
    // * ================================================
    // *
    // * Convenience common methods
    // *
    // * ================================================
    // */
    // /**
    // * Create a new user account in User_Account Table. If it already exists, nothing is created.
    // *
    // * @param login
    // * a person's login
    // * @return User account newly created or user account in table if it already exists.
    // * @throws Hd3dTranslationException
    // * @throws Hd3dPersistenceException
    // * When database error occurs
    // */
    // private static IUserAccount getAccount(Session session, String login) throws Hd3dTranslationException,
    // Hd3dPersistenceException
    // {
    // // Retrieve the user account.
    // IUserAccount object = (IUserAccount) session.createCriteria(Persistors.useraccount.getPersistedClass()).add(
    // Restrictions.eq("principal", login)).uniqueResult();
    //
    // // If the user account is in database, return it.
    // if (object != null)
    // {
    // return object;
    // }
    //
    // // Else create a new user account instance.
    // object = new UserAccountH(login);
    //
    // // Save user account in database.
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // return object;
    // }
    //
    // private static void createPass(Session session, String login) throws Hd3dPersistenceException,
    // Hd3dTranslationException
    // {
    // IUserAccount account = getAccount(session, login);
    // account.setSecret(FDTAuthUtils.encrypt(login + "pass"));
    // session.save(account);
    // }
    //
    // /**
    // * Create a new project in Project Table. If project already exists, no project is created.
    // *
    // * @param name
    // * Project name
    // * @param status
    // * Project status
    // * @param color
    // * Project color
    // * @return Project newly created or project in table if it already exists.
    // * @throws Hd3dTranslationException
    // *
    // * @throws Hd3dPersistenceException
    // * When database error occurs
    // */
    // private static IProject getProject(Session session, String name, EProjectStatus status, String color, String
    // hook)
    // throws Hd3dTranslationException, Hd3dPersistenceException
    // {
    //
    // // Retrieve project.
    // IProject object = (IProject) session.createCriteria(Persistors.project.getPersistedClass()).add(
    // Restrictions.eq("name", name)).uniqueResult();
    //
    // // If project is in database, return project.
    // if (object != null)
    // {
    // return object;
    // }
    //
    // // Else create a new project instance.
    // object = Translators.project.fromLightweight(LProject.getNewInstance(name, status.toString(), color, hook),
    // null);
    //
    // // Save project in database.
    // object.doPrePersist();
    // session.save(object);
    // object.doPostPersist();
    // return object;
    // }
    //
    // /**
    // * Create a new person in Person Table. If task already exists, no person is created.
    // *
    // * @param login
    // * Person login
    // * @param password
    // * Person Password
    // * @param first
    // * Person first name
    // * @param last
    // * Person last name
    // * @return Person newly created or person in table if it already exists.
    // * @throws Hd3dTranslationException
    // * @throws Hd3dPersistenceException
    // * When database error occurs
    // */
    // private static IPerson getPerson(Session session, String login, String password, String first, String last)
    // throws Hd3dTranslationException, Hd3dPersistenceException
    // {
    //
    // // Retrieve Person.
    // IPerson object = (IPerson) session.createCriteria(Persistors.person.getPersistedClass()).add(
    // Restrictions.eq("login", login)).uniqueResult();
    //
    // // If person is in database, return person.
    // if (object != null)
    // {
    // return object;
    // }
    //
    // // Else create a new person instance.
    // object = Translators.person.fromLightweight(LPerson.getNewInstance(login, first, last, null, null, null,
    // NumberUtils.LONG_ZERO), null);
    //
    // // Save person in database.
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // object.doPostPersist();
    //
    // createPass(session, login);
    // return object;
    // }

    // private static void createInitialTaskTypes(Session session) throws Hd3dPersistenceException
    // {
    // // ITaskType design =
    // getTaskType(session, "Design", "#99FE80", "The Design Task Type", true);
    //
    // // ITaskType modeling =
    // getTaskType(session, "Modeling", "#009999", "The Modeling Task Type", true);
    //
    // // ITaskType setup =
    // getTaskType(session, "Setup", "#00B366", "The Setup Task Type", true);
    //
    // // ITaskType texturing =
    // getTaskType(session, "Texturing", "#FE8080", "The Texturing Task Type", true);
    //
    // // ITaskType shading =
    // getTaskType(session, "Shading", "#FEE680", "The Shading Task Type", true);
    //
    // // ITaskType hairs =
    // getTaskType(session, "Hairs", "#FE80DF", "The Hairs Task Type", true);
    //
    // // ITaskType mattePainting =
    // getTaskType(session, "MattePainting", "#FE9980", "The MattePainting Task Type", true);
    //
    // // ITaskType layout =
    // getTaskType(session, "Layout", "#FECC80", "The Layout Task Type", true);
    //
    // // ITaskType export =
    // getTaskType(session, "Export", "#990099", "The Export Task Type", true);
    //
    // // ITaskType animation =
    // getTaskType(session, "Animation", "#B27D00", "The Animation Task Type", true);
    //
    // // ITaskType tracking =
    // getTaskType(session, "Tracking", "#00477D", "The Tracking Task Type", true);
    //
    // // ITaskType rendering =
    // getTaskType(session, "Rendering", "#FF9900", "The Rendering Task Type", true);
    //
    // // ITaskType dressing =
    // getTaskType(session, "Dressing", "#6BB200", "The Dressing Task Type", true);
    //
    // // ITaskType lighting =
    // getTaskType(session, "Lighting", "#FF3300", "The Lighting Task Type", true);
    //
    // // ITaskType compositing =
    // getTaskType(session, "Compositing", "#80FEC8", "The Compositing Task Type", true);
    // }
    //
    // /**
    // * Associate a Persons to a project (Many-to-many relationship).
    // *
    // * @param project
    // * Project to be associated with persons
    // * @param persons
    // * List of person to associate to a project
    // * @throws Hd3dPersistenceException
    // */
    // private void appendStaffToProject(IProject project, IPerson... persons) throws Hd3dPersistenceException
    // {
    //
    // // Build a new hash set if not initialize
    // // if (project.getResourceGroups() == null)
    // // project.setStaff(new HashSet<IPerson>());
    // //
    // // project.getResourceGroups().addAll(Arrays.asList(persons));
    // }
    //
    // /**
    // * Create a new group in Group Table. If group already exists, no group is created.
    // *
    // * @param name
    // * Group name
    // * @return Group newly created or project in table if it already exists.
    // * @throws Hd3dTranslationException
    // *
    // * @throws Hd3dPersistenceException
    // * When database error occurs
    // */
    // private static IResourceGroup getResourceGroup(Session session, String name) throws Hd3dTranslationException,
    // Hd3dPersistenceException
    // {
    //
    // // Retrieve project.
    // IResourceGroup object = (IResourceGroup) session.createCriteria(Persistors.resourcegroup.getPersistedClass())
    // .add(Restrictions.eq("name", name)).uniqueResult();
    //
    // // If project is in database, return project.
    // if (object != null)
    // {
    // return object;
    // }
    //
    // // Else create a new project instance.
    // object = Translators.resourcegroup.fromLightweight(LResourceGroup.getNewInstance(name, null), null);
    //
    // // Save project in database.
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // object.doPostPersist();
    // return object;
    // }

    // private static ICategory getCategory(Session session, Long id, Long version, String name, Long parent, Long
    // project)
    // throws Hd3dTranslationException, Hd3dPersistenceException
    // {
    //
    // ICategory category = (ICategory) session.createCriteria(Persistors.category.getPersistedClass()).add(
    // Restrictions.eq("id", id)).uniqueResult();// "id" must be
    // // property and
    // // not column name
    // if (category != null)
    // return category;
    // category = Translators.category.fromLightweight(LCategory.getNewInstance(name, parent, project), null);
    // category.doPrePersist();
    // session.save(category);
    // session.flush();
    // category.doPostPersist();
    // return category;
    // }

    private static IConstituent getConstituent(Session session, Long id, Long categoryID, String categoryName,
            Integer completion, String description, Integer difficulty, String hook, String label, Integer trust,
            String uuid) throws Hd3dException
    {

        IConstituent object = (IConstituent) session.createCriteria(Persistors.constituent.getPersistedClass()).add(
                Restrictions.eq("id", id)).uniqueResult();
        if (object != null)
            return object;
        object = Translators.constituent.fromLightweight(LConstituent.getNewInstance(categoryID, categoryName,
                completion, description, difficulty, hook, label, trust, true, true, true, false, false, false, null),
                null);

        object.doPrePersist();
        session.save(object);
        session.flush();
        object.doPostPersist();

        return object;
    }

    /**
     * Create a new task in Person Table. If task already exists, no task is created.
     * 
     * @param project
     *            Project associated to task
     * @param name
     *            Task name
     * @param status
     *            Task status
     * @param creator
     *            Task creator
     * @param worker
     *            Worker associated to task
     * @return Task newly created or task in table if it already exists.
     * @throws Hd3dPersistenceException
     * @throws Hd3dTranslationException
     */
    // private static ITask getTask(Session session, IProject project, String name, ETaskStatus status, IPerson creator,
    // IPerson worker, Date startDate, Date endDate, Long duration, Date deadLine, Date actualStartDate,
    // Date actualEndDate, Boolean startable, Boolean confirmed, Boolean startup, ITaskType taskType)
    // throws Hd3dPersistenceException, Hd3dTranslationException
    // {
    //
    // // Retrieve task
    // final Class<?> persistedClass = Persistors.task.getPersistedClass();
    // ITask object = (ITask) session.createCriteria(persistedClass).add(Restrictions.eq("name", name)).uniqueResult();
    //
    // // If task is in database, return task.
    // if (object != null)
    // return object;
    //
    // // Else Save task in database.
    // try
    // {
    // object = new TaskH(name, NumberUtils.BYTE_ZERO, null, status, project, creator, worker, startDate, endDate,
    // duration, deadLine, actualStartDate, actualEndDate, startable, confirmed, startup, null, taskType);
    //
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // object.doPostPersist();
    // return object;
    //
    // }
    // catch (Exception e)
    // {
    // // e.printStackTrace();
    // throw new Hd3dPersistenceException(e);
    // }
    // }
    // public static ITaskType getGlobalTaskType(Session session, String name) throws Hd3dException
    // {
    // return getTaskType(session, name, "default color", "default description", true);
    // }
    //
    // public static ITaskType getPastaTaskType(Session session, String name) throws Hd3dException
    // {
    // return getTaskType(session, name, "default color", "default description", false);
    // }
    // @SuppressWarnings("unchecked")
    // public static ITaskType getTaskType(Session session, String name, String color, String description, boolean
    // isGlobal)
    // throws Hd3dPersistenceException
    // {
    //
    // // Retrieve taskType
    // final Class<?> persistedClass = Persistors.taskType.getPersistedClass();
    // List<ITaskType> taskTypes = session.createCriteria(persistedClass).add(Restrictions.eq("name", name)).list();//
    // there
    // // must
    // // be
    // // only
    // // one,
    // // but
    // // you
    // // may
    // // have
    // // a
    // // few
    // // when
    // // you
    // // mix
    // // up initDB and initPasta
    //
    // // If taskType is in database, return task.
    // if (taskTypes != null && !taskTypes.isEmpty())
    // {
    // return taskTypes.get(0);
    // }
    // // Else Save taskType in database.
    // try
    // {
    // ITaskType newTaskType = new TaskTypeH(null, null, null, color, name, description);
    // if (!isGlobal)
    // {
    // final IProject projectPasta = Persistors.project.getByValue("name", "Pat et Stanley");
    // newTaskType.setProject(projectPasta);
    //
    // }
    // newTaskType.doPrePersist();
    // session.save(newTaskType);
    // session.flush();
    // newTaskType.doPostPersist();
    //
    // return newTaskType;
    // }
    // catch (Exception e)
    // {
    // // e.printStackTrace();
    // throw new Hd3dPersistenceException(e);
    // }
    // }
    // public static void createTask(Session session, String name, int difficulty, Long idWo) throws Hd3dException,
    // NoSuchAlgorithmException
    // {
    //
    // final IProject projectPasta = Persistors.project.getByValue("name", "Pat et Stanley");
    // // Persons
    // final IPerson eth = Persistors.person.getByValue("login", "eth");
    // // TODO make consistent taskTypes
    // // final ITaskType defaultTaskType = getPastaTaskType("default Task Type");
    //
    // String nameTask = name.split("_")[0];
    // final ITaskType TaskType = getTaskType(session, nameTask, null, null, true);
    //
    // // Tasks
    // Date startDate = new Date();
    // Date endDate = null;
    // Calendar calendar = new GregorianCalendar();
    // calendar.setTimeInMillis(startDate.getTime() + (3 * 24 * 60 * 60 * 1000));
    // endDate = calendar.getTime();
    // // Long duration = endDate.getTime() - startDate.getTime();
    // // Long duration = new Long(8 * 60 * 60 * 1000 * (difficulty + 1));
    // Long duration = 8L * 60L * 60L * 1000L * Long.valueOf(difficulty + 1);
    // // Projects
    // ITask task = getTask(session, projectPasta, name, ETaskStatus.stand_by, eth, null, startDate, endDate,
    // duration, null, startDate, endDate, true, null, null, TaskType);
    // session.flush();
    //
    // IEntityTaskLink link = new EntityTaskLinkH(idWo, "Constituent", task);
    // link.doPrePersist();
    // session.save(link);
    // session.flush();
    // link.doPostPersist();
    // }
    // public static void createTask(Session session, String name, Long idWo, String type) throws Hd3dException,
    // NoSuchAlgorithmException
    // {
    //
    // final IProject projectPasta = Persistors.project.getByValue("name", "Pat et Stanley");
    // // Persons
    // final IPerson eth = Persistors.person.getByValue("login", "eth");
    // // TaskType
    // // TODO make consistent taskTypes
    // // final ITaskType defaultTaskType = getPastaTaskType("default Task Type");
    // String nameTask = name.split("_")[0];
    // final ITaskType taskType = getTaskType(session, nameTask, null, null, true);
    //
    // // Tasks
    // Date startDate = new Date();
    // Date endDate = null;
    // Calendar calendar = new GregorianCalendar();
    // calendar.setTimeInMillis(startDate.getTime() + (3 * 24 * 60 * 60 * 1000));
    // endDate = calendar.getTime();
    //
    // Long duration = new Long(28800000);
    // // Projects
    // ITask task = getTask(session, projectPasta, name, ETaskStatus.stand_by, eth, null, startDate, endDate,
    // duration, null, startDate, endDate, true, null, null, taskType);
    // session.flush();
    //
    // IEntityTaskLink link = new EntityTaskLinkH(idWo, type, task);
    //
    // link.doPrePersist();
    // session.save(link);
    // session.flush();
    // link.doPostPersist();
    // }
    private static Long retrieveCategoryId(Long projectId, String name) throws Hd3dException
    {
        // TODO : filter retrieved categories with project id to enable a simple db update with Pasta data.
        ICategory category = Persistors.category.getObjectByValue_NoPermCheck("name", name);
        final Long categoryId = category.getId();
        return categoryId;
    }

    private static ICategory retrieveCategory(String name) throws Hd3dException
    {
        return Persistors.category.getObjectByValue_NoPermCheck("name", name);
    }

    protected static void createConstituent(Session session, Long categoryId, String hook, String description,
            int taskDifficulty)
    {
        // final String FILE_DIR = Conf.getTmpDir();

        try
        {
            IConstituent c = getConstituent(session, null, categoryId, null, 0, description, taskDifficulty, hook,
                    hook, 0, null);

            session.flush();
            session.refresh(c);

            // String filePath = FILE_DIR + SystemUtils.FILE_SEPARATOR + c.getHook() + ".jpg";
            // String thumbnailPath = FILE_DIR + SystemUtils.FILE_SEPARATOR + c.getHook() + "_thumb.jpg";
            // if (DBInitializer.createFileRevisionFromTempFile(filePath, thumbnailPath, c,
            // c.getCategory().getProject()) == null)
            // {
            // System.out.println("*** Error: could not create fileRevision for '" + hook + "'");
            // }
            //
            // Long id = c.getId();
            // createTask(session, "Design_" + hook, taskDifficulty, id);
            // createTask(session, "Modeling_" + hook, taskDifficulty, id);
            // createTask(session, "Setup_" + hook, taskDifficulty, id);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    // private static IApprovalNoteType createApprovalNoteType(Session session, Long projectId, String name,
    // Long taskTypeId) throws Hd3dException
    // {
    // IApprovalNoteType object = Translators.approvalnotetype.fromLightweight(LApprovalNoteType.getNewInstance(name,
    // taskTypeId, projectId), null);
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // object.doPostPersist();
    // return object;
    // }
    //
    // private static ISheet createSheet(Session session, Long projectId, String name, String description,
    // String boundClassName) throws Hd3dException
    // {
    // ISheet object = Translators.sheet.fromLightweight(LSheet.getNewInstance(name, description, projectId,
    // boundClassName, null), null);
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // object.doPostPersist();
    // return object;
    // }
    //
    // private static IItemGroup createItemGroup(Session session, Long sheetId, String name) throws Hd3dException
    // {
    // // IItemGroup object = Translators.itemGroup.fromLightweight(LItemGroup.getNewInstance(name,
    // // new ArrayList<Long>(), sheetId, name.substring(0, 4)), null);
    // IItemGroup object = Translators.itemGroup.fromLightweight(LItemGroup.getNewInstance(name, null, sheetId, name
    // .substring(0, 4)), null);
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // object.doPostPersist();
    // return object;
    // }
    //
    // private static IItem createItem(Session session, String name, String type, String query, String controlContent,
    // Long itemGroup, String metaType, String renderer, String editor) throws Hd3dTranslationException
    // {
    // IItem object = Translators.item.fromLightweight(LItem.getNewInstance(itemGroup, null, null, name, type, query,
    // controlContent, itemGroup, metaType, null, null), null);
    // object.setRenderer(renderer);
    // object.setEditor(editor);
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // object.doPostPersist();
    // return object;
    // }
    //
    // private static IClassDynMetaDataType createAttributDynamique(Session session, String name, String entity,
    // String type, String defaultValue) throws Hd3dTranslationException
    // {
    //
    // IDynMetaDataType dataType = (IDynMetaDataType) session.createCriteria(
    // Persistors.dynmetadatatype.getPersistedClass()).add(Restrictions.eq("type", type)).uniqueResult();
    //
    // if (dataType == null)
    // {
    // String typeName = type;
    // if (type.startsWith("java.util."))
    // {
    // typeName = typeName.substring("java.util.".length());
    // }
    // else if (type.startsWith("java.lang."))
    // {
    // typeName = typeName.substring("java.lang.".length());
    // }
    //
    // ILDynMetaDataType lightDataType = LDynMetaDataType.getNewInstance(defaultValue, typeName, "simple", type);
    // dataType = Translators.dynmetadatatype.fromLightweight(lightDataType, null);
    // dataType.doPrePersist();
    // session.save(dataType);
    // session.flush();
    // dataType.doPostPersist();
    // }
    //
    // ILClassDynMetaDataType lightClassMeta = LClassDynMetaDataType.getNewInstance(name, dataType.getId(), "*",
    // entity, "");
    // IClassDynMetaDataType classMeta = Translators.classdynmetadata.fromLightweight(lightClassMeta, null);
    // classMeta.doPrePersist();
    // session.save(classMeta);
    // session.flush();
    // classMeta.doPostPersist();
    //
    // return classMeta;
    // }
    //
    // private static IComposition getComposition(Session session, Long id, Long shotId, Long inputConstituentId)
    // throws Hd3dTranslationException, Hd3dPersistenceException
    // {
    // IComposition object = (IComposition) session.createCriteria(Persistors.composition.getPersistedClass()).add(
    // Restrictions.eq("id", id)).uniqueResult();
    // if (object != null)
    // return object;
    // String name = "CompoName_" + shotId + "_" + inputConstituentId;
    // String description = "No Description";
    // object = Translators.composition.fromLightweight(LComposition.getNewInstance(name, description,
    // inputConstituentId, shotId, null), null);// TODO a merger
    //
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // object.doPostPersist();
    // return object;
    // }
    //
    // private static String headWithZeros(Integer numberToConvert, Integer numberOfDigits)
    // {
    //
    // String result = Integer.toString(numberToConvert);
    // int threshold = 0;
    // for (int i = numberOfDigits; i > 1; i--)
    // {
    // threshold = (int) Math.pow(10, i - 1);
    // if (numberToConvert < threshold && numberOfDigits >= 2)
    // {
    // result = "0" + result;
    // }
    // }
    // return result;
    // }

    /**
     * creation d'un série d'activité sur la tache les activités vont seront daté en fonction de la startDate de la
     * tache et sur le nombre de jour de la durée rien n'est rempli les weekend
     * 
     * @param task
     * @return
     * @throws Hd3dTranslationException
     */
    // private static List<IActivity> createTaskActivities(Session session, TaskH task) throws Hd3dTranslationException
    // {
    // List<IActivity> activities = new ArrayList<IActivity>();
    // Date startDate = task.getStartDate();
    // if (startDate == null)
    // {
    // return null;
    // }
    // Long nbSecIn7Hour = new Long(25200);
    // Long nbSecInHalfAnHour = new Long(1800);
    // Long duration = task.getDuration();
    // int nbDay = (int) (duration / (nbSecIn7Hour * 1000));
    // for (int i = 0; i < nbDay; i++)
    // {
    // // Date dayDate = new Date();
    // Calendar calendar = new GregorianCalendar();
    // calendar.setTime(startDate);
    // calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + i);
    //
    // if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
    // && calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
    // {
    // // on met heure de la date a 0
    // calendar.set(Calendar.HOUR_OF_DAY, 0);
    // calendar.set(Calendar.MINUTE, 0);
    // calendar.set(Calendar.SECOND, 0);
    // calendar.set(Calendar.MILLISECOND, 0);
    // Date dayDate = calendar.getTime();
    //
    // // on verifie qu'il y a bien un worker sur la tache
    // if (task.getWorker() != null)
    // {
    //
    // Double rand = new Double(Math.random() * 100);
    // IActivity activity = createActivity(session, dayDate, nbSecIn7Hour, task, task.getWorker().getId(),
    // task.getWorker().getId(), task.getProject().getId());
    // if (activity != null)
    // {
    // activities.add(activity);
    // }
    //
    // // petit random pour creer un reunion de temps en temps
    // if (rand < 10)
    // {
    // IActivity activity2 = createActivity(session, dayDate, nbSecInHalfAnHour, null, task
    // .getWorker().getId(), task.getWorker().getId(), task.getProject().getId());
    // if (activity2 != null)
    // {
    // activities.add(activity2);
    // }
    // }
    // }
    // }
    // }
    //
    // return activities;
    // }
    //
    // private static IActivity createActivity(Session session, Date dayDate, Long duration, TaskH task, Long workerId,
    // Long filledByID, Long projectId) throws Hd3dTranslationException
    // {
    // IPersonDay day = getDay(session, dayDate, workerId);
    // if (day != null)
    // {
    // Date filledDate = new Date();
    // if (task == null)
    // {
    // ISimpleActivity object = Translators.simpleActivity.fromLightweight(LSimpleActivity.getNewInstance(
    // ESimpleActivityType.meeting, day.getId(), duration, "", workerId, projectId, filledByID,
    // filledDate), null);
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // object.doPostPersist();
    // return object;
    // }
    // else
    // {
    // ILTask ltask = task.defaultTranslator().toLightweight(task, null);
    //
    // ITaskActivity object = Translators.taskActivity.fromLightweight(LTaskActivity.getNewInstance(day
    // .getId(), dayDate, duration, "", ltask, workerId, task.getProject().getId(), filledByID,
    // filledDate), null);
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // object.doPostPersist();
    // return object;
    // }
    // }
    // return null;
    // }
    //
    // private static IPersonDay getDay(Session session, Date date, Long personId) throws Hd3dTranslationException
    // {
    // String query = "select day from " + PersonDayH.class.getCanonicalName() + " day where day.person.id = '"
    // + personId + "'";
    // List<IPersonDay> result = HibernateUtil.currentSession().createQuery(query).list();
    //
    // for (Iterator<IPersonDay> iterator = result.iterator(); iterator.hasNext();)
    // {
    // IPersonDay personDay = (IPersonDay) iterator.next();
    // if (date.equals(personDay.getDate()))
    // {
    // return personDay;
    // }
    // }
    // return createPersonDay(session, date, personId);
    // }
    //
    // private static IPersonDay createPersonDay(Session session, Date dayDate, Long personId)
    // throws Hd3dTranslationException
    // {
    // IPersonDay day = Translators.personDay.fromLightweight(LPersonDay.getNewInstance(dayDate, personId), null);
    // day.doPrePersist();
    // session.save(day);
    // session.flush();
    // day.doPostPersist();
    // return day;
    // }
    //
    // private static void save(Session session, ITag object) throws Hd3dPersistenceException
    // {
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // object.doPostPersist();
    // }
    //
    // private static void save(Session session, IConstituent object) throws Hd3dPersistenceException
    // {
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // object.doPostPersist();
    // }
    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
