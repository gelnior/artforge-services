package fr.hd3d.services.resources;

import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.services.resources.collectionquery.CollectionQueriesMap;
import fr.hd3d.services.resources.serializer.RessourceSerializer;


/**
 * Return all the available business entities
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:retourne la liste des classes CollectionQuery permettant de faire une requête pour toutes les lignes des sheets\r\n"
        + "\r\nPUT:N/A\r\n" + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class CollectionQueriesResource<L, P> extends AbstractHd3dBaseResource
{
    public CollectionQueriesResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    public Representation get(Variant variant)
    {
        try
        {
            Representation r = null;
            r = RessourceSerializer.getRepresentation(variant, CollectionQueriesMap.description);
            r.setCharacterSet(CharacterSet.UTF_8);
            return r;
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
