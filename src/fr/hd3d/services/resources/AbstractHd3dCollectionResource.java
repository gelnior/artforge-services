package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import net.sf.sojo.interchange.json.JsonParserException;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.WrapDynaBean;
import org.apache.commons.lang.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.JDBCException;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;
import org.hibernate.Transaction;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.impl.ILPersistent;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.model.persistence.impl.hibernate.query.ListResultQuery;
import fr.hd3d.model.persistence.impl.hibernate.search.LuceneFilterFactory;
import fr.hd3d.model.persistence.impl.hibernate.search.LuceneFilterFactory.LuceneFilters;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.resources.collectionquery.AbstractCollectionQuery;
import fr.hd3d.services.resources.collectionquery.CollectionQueriesMap;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.services.translator.json.JSonFormSerializer;
import fr.hd3d.utils.Bench;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.Pair;
import fr.hd3d.utils.ServerResourceUtils;


@SuppressWarnings("unchecked")
public abstract class AbstractHd3dCollectionResource<L extends ILPersistent, P extends Persistent> extends
        AbstractHd3dBaseResource
{
    public AbstractHd3dCollectionResource(final IPersist<? extends P> persistor, final IBaseTranslator<L, P> translator)
            throws Hd3dException
    {
        super();
        final ResourceContext context = new ResourceContext<L, P>(this);
        context.setPersistor(persistor);
        context.setTranslator(translator);
        context.complete();
        setResourceContext(context);
    }

    public AbstractHd3dCollectionResource(final Class<?> persistedClass, final IBaseTranslator<L, P> translator)
            throws Hd3dException
    {
        super();
        final ResourceContext context = new ResourceContext<L, P>(this);
        context.setPersistedClass(persistedClass);
        context.setTranslator(translator);
        context.complete();
        setResourceContext(context);
    }

    public AbstractHd3dCollectionResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected ResourceContext<L, P> getResourceContext()
    {
        return (ResourceContext<L, P>) super.getResourceContext();
    }

    // ============================================================
    //
    // READ
    //
    // ============================================================
    @Override
    public Representation get(final Variant variant)
    {
        Bench b = new Bench();

        final Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();

        ResultCollection list = null;
        ResultCollection luceneFiltered = null;
        ResultCollection<P> paginatedResult = null;
        List<L> lightweightList = null;
        try
        {
            list = getCollectionForRepresentation();

            /* filter by lucene constraint */
            final LuceneFilters luceneFilter = LuceneFilterFactory.getFilter(getResourceContext().getQueryMap());
            luceneFiltered = luceneFilter.filter(getResourceContext().getPersistedClass(), list, false);

            /* need to load objects based on their ids */
            list = reloadCollectionIfAny(luceneFiltered, false);

            /* post process: complete objects' transient fields */
            processExtraFields(list);

            /* pagination */
            paginatedResult = paginate(list);

            /* Convert result objects to lightweight objects. */
            lightweightList = getLightWeightList(paginatedResult);

            Representation r = getLightWeightRepresentation(variant, lightweightList, paginatedResult.getCount());
            r.setCharacterSet(CharacterSet.UTF_8);

            doPostRead(list, lightweightList);

            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            tx.commit();

            /*
             * NOTE: do not set Status to Status.SUCCESS_NO_CONTENT If so, there will be no empty list returned in the
             * HTML screen, which may be confusing
             */
            getResponse().setStatus(Status.SUCCESS_OK);
            getResponse().setEntity(ServerResourceUtils.encodeRepresentation(r));

            Log.LOGGER.info(responseInfo(b));
        }
        catch (JDBCException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, "Unable to connect to database");
        }
        catch (ClassCastException e)
        {
            if (tx != null && tx.isActive())
            {
                tx.rollback();
            }
            Log.LOGGER.error(ExceptUtils.format(e));
            getResponse().setStatus(Status.REDIRECTION_FOUND);
            /* Note: do not return any representation, it's up to client to resubmit the request. */
        }
        catch (Hd3dException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
        }
        catch (HibernateException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
        }
        catch (Exception e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
        }
        finally
        {
            if (list != null)
            {
                list.clear();
            }
            if (luceneFiltered != null)
            {
                luceneFiltered.clear();
            }
            if (lightweightList != null)
            {
                lightweightList.clear();
            }
            if (tx != null)
            {
                tx = null;
            }
            cleanup();
        }

        return getResponseEntity();
    }

    protected void processExtraFields(ResultCollection list) throws Hd3dException
    {
        /* requested extra fields */
        List<String> extraFields;
        try
        {
            extraFields = Hd3dJsonSerializer.<List<String>> unserialize(getResourceContext().getQueryMap().get(
                    Const.EXTRAFIELDS));
            CollectUtils.removeNull(extraFields);
        }
        catch (JsonParserException e)
        {
            Log.LOGGER.error("a List<Map<String, Object>> is expected in JSON");
            Log.LOGGER.error(ExceptUtils.format(e));
            extraFields = Collections.emptyList();
        }

        /* process them */
        if (CollectUtils.isNotEmpty(extraFields))
        {
            Map<String, String> hd3dCollectionQueries = EntitiesMaps
                    .classAttributeHd3dCollectionQueryAnnotation(getResourceContext().getPersistedClass());
            if (CollectUtils.isNotEmpty(hd3dCollectionQueries))
            {
                for (String extraField : extraFields)
                {
                    if (StringUtils.isNotBlank(extraField))
                    {
                        String colQueryName = hd3dCollectionQueries.get(extraField);
                        if (colQueryName != null)
                        {
                            /* instantiate the collection query */
                            Class<? extends AbstractCollectionQuery> cqClass = CollectionQueriesMap.map
                                    .get(colQueryName);

                            /* item query field must contain the CollectionQuery full name name */
                            try
                            {
                                AbstractCollectionQuery collectionQuery = cqClass.newInstance();
                                collectionQuery.setEntities(list.getResult());
                                List collectionQueryResult = collectionQuery.doQuery();
                                /* reorder */
                                for (int i = 0; i < list.getResult().size(); i++)
                                {
                                    final DynaBean dynabean = new WrapDynaBean(list.getResult().get(i));
                                    if (i < collectionQueryResult.size())
                                    {
                                        dynabean.set(extraField, collectionQueryResult.get(i));
                                    }
                                }
                            }
                            catch (InstantiationException e1)
                            {
                                throw new Hd3dException("Cannot instanciate collection query of class:" + cqClass);
                            }
                            catch (IllegalAccessException e1)
                            {
                                throw new Hd3dException("Cannot instanciate collection query of class:" + cqClass);
                            }
                        }
                    }
                }
            }
        }
    }

    protected void cleanup()
    {
        super.cleanup();
    }

    protected ResultCollection getCollection(final ListResultQuery query) throws Hd3dException
    {
        HibernateUtil.safeQuery((IHibernateQuery) query);
        return new ResultCollection(query.result, query.totalCount);
    }

    protected void doPostRead(final ResultCollection<P> list, List<L> lightweightList)
    {}

    protected abstract ResultCollection getCollectionForRepresentation() throws Hd3dException;

    protected abstract ResultCollection<P> reloadCollectionIfAny(final ResultCollection luceneFiltered,
            boolean onIdsOnly) throws Hd3dException;

    protected abstract List<L> getLightWeightList(final ResultCollection<P> list) throws Hd3dException;

    protected abstract Representation getLightWeightRepresentation(final Variant variant,
            final List<L> lightweightList, final int nbRecords);

    // ============================================================
    //
    // CREATE
    //
    // ============================================================
    /**
     * handle object CREATION (single and multiple)
     */
    @Override
    public Representation post(final Representation entity, final Variant variant)
    {
        Bench b = new Bench();

        /*
         * if deserialized light object lObject is used several times, it must be deserialized ONCE because
         * entity.getText() can only be called ONCE
         */
        P newObject = null;
        List<P> newObjects = null;
        int count = 3;
        boolean stop = false;
        while (!stop && count > 0)
        {
            count--;

            final Session session = HibernateUtil.currentSession();
            Transaction tx = session.beginTransaction();

            try
            {
                if (getResourceContext().isBulk())
                {/* MULTIPLE objects creation */

                    if (CollectUtils.isEmpty(newObjects))
                    {
                        newObjects = newCollection(entity);
                    }

                    doPrePersistCollection(entity, newObjects);

                    doPersist(newObjects);

                    if (session.getFlushMode() == FlushMode.MANUAL)
                    {
                        session.flush();
                    }
                    tx.commit();

                    tx = session.beginTransaction();

                    doPostPersist(newObjects);

                    if (session.getFlushMode() == FlushMode.MANUAL)
                    {
                        session.flush();
                    }
                    tx.commit();

                    stop = true;
                }
                else
                {/* SINGLE object creation */

                    if (newObject == null)
                    {
                        newObject = newObject(entity);
                    }

                    doPrePersist(entity, newObject);

                    doPersist(newObject);

                    if (session.getFlushMode() == FlushMode.MANUAL)
                    {
                        session.flush();
                    }
                    tx.commit();

                    tx = session.beginTransaction();

                    doPostPersist(newObject);

                    if (session.getFlushMode() == FlushMode.MANUAL)
                    {
                        session.flush();
                    }
                    tx.commit();

                    stop = true;
                }

                Log.LOGGER.info(responseInfo(b));
            }
            catch (StaleObjectStateException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                sleep();
            }
            catch (Hd3dPersistenceException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                stop = true; // no point to continue
            }
            catch (Hd3dTranslationException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                stop = true;// may be a validation exception, so no point to continue
            }
            catch (Hd3dException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                stop = true;
            }
            catch (HibernateException e)
            {
                doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                stop = true; // no point to continue
            }
            catch (Exception e)
            {
                doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                stop = true; // no point to continue
            }
            finally
            {
                if (tx != null)
                {
                    tx = null;
                }
                cleanup();
            }
        }

        /* clear */
        if (newObjects != null)
        {
            newObjects.clear();
        }
        newObjects = null;
        cleanup();

        return getResponseEntity();
    }

    protected abstract P newObject(Representation entity) throws Hd3dException;

    protected abstract List<P> newCollection(Representation entity) throws Hd3dException;

    protected abstract void doPrePersistCollection(Representation entity, List<P> newObjects) throws Hd3dException;

    protected abstract void doPersist(List<P> objects) throws Hd3dException;

    protected abstract void doPostPersist(List<P> objects) throws Hd3dException;

    protected abstract void doPrePersist(Representation entity, P object) throws Hd3dException;

    protected abstract void doPersist(P newObject) throws Hd3dException;

    protected abstract void doPostPersist(P newObject) throws Hd3dException;

    // ============================================================
    //
    // UPDATE
    //
    // ============================================================
    /**
     * handle MULTIPLE objects MODIFICATION. For single modification, see AbstractHd3dResource.
     * 
     * @see AbstractHd3dResource
     */
    @Override
    public Representation put(Representation entity, Variant variant)
    {
        Bench b = new Bench();
        /*
         * if deserialized light object lObject is used several times, it must be deserialized ONCE because
         * entity.getText() can only be called ONCE
         */
        List<L> lObjects = null;
        List<P> objects = null;
        int count = 3;
        boolean stop = false;
        while (!stop && count > 0)
        {
            count--;

            final Session session = HibernateUtil.currentSession();
            Transaction tx = session.beginTransaction();

            try
            {
                if (CollectUtils.isEmpty(lObjects))
                {
                    lObjects = JSonFormSerializer.<L> deserializeList(entity);
                }

                objects = retrieveObjects(entity, lObjects, LockMode.UPGRADE_NOWAIT);

                doPreUpdate(entity, lObjects, objects);

                doUpdate(lObjects, objects);

                if (session.getFlushMode() == FlushMode.MANUAL)
                {
                    session.flush();
                }
                tx.commit();

                tx = session.beginTransaction();

                doPostUpdate(objects, entity);

                if (session.getFlushMode() == FlushMode.MANUAL)
                {
                    session.flush();
                }
                tx.commit();

                stop = true;

                Log.LOGGER.info(responseInfo(b));
            }
            catch (StaleObjectStateException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                sleep();
            }
            catch (Hd3dPersistenceException e)
            {
                doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                if (e.isRetry())
                {
                    sleep();
                }
                else
                {
                    stop = true;
                }
            }
            catch (Hd3dTranslationException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                stop = true;// may be a validation exception, so no point to continue
            }
            catch (Hd3dException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                stop = true;// may be a validation exception, so no point to continue
            }
            catch (HibernateException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                sleep();
            }
            catch (Exception e)
            {
                doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                stop = true; // no point to continue
            }
            finally
            {
                if (tx != null)
                {
                    tx = null;
                }
                cleanup();
            }
        }

        /* clear */
        if (lObjects != null)
        {
            lObjects.clear();
            lObjects = null;
        }
        if (objects != null)
        {
            objects.clear();
            objects = null;
        }
        cleanup();

        return getResponseEntity();
    }

    private ResultCollection paginate(final ResultCollection values) throws JsonParserException,
            Hd3dQueryFilterException
    {
        /*
         * if already paginated by hibernate do not paginate here.
         */
        if (QueryFilterFactory.hibernatePagination(getResourceContext().getQueryMap()))
        {
            return values;
        }

        ResultCollection ret;

        if (values != null && values.isNotEmpty())
        {
            Pair<Integer, Integer> indexes = ServerResourceUtils.getStartAndEndIndex(
                    getResourceContext().getQueryMap(), values.size());
            ret = new ResultCollection(values.getResult().subList(indexes.getField_1(), indexes.getField_2()), values
                    .getCount());
        }
        else
        {
            ret = values;
        }

        return ret;
    }

    protected abstract List<P> retrieveObjects(Representation entity, List<L> lObjects, LockMode lockMode)
            throws Hd3dException;

    protected abstract void doPreUpdate(Representation entity, List<L> lObjects, List<P> objects) throws Hd3dException;

    protected abstract void doUpdate(List<L> lObjects, List<P> objects) throws Hd3dException;

    protected abstract void doPostUpdate(List<P> objects, Representation entity) throws Hd3dException;

    // ============================================================
    //
    // DELETE
    //
    // ============================================================
    /**
     * Handle DELETE requests.
     */
    @Override
    public Representation delete(Variant variant)
    {
        Bench b = new Bench();

        List<P> objects = null;
        int count = 3;
        boolean stop = false;
        final ResourceContext<L, P> context = getResourceContext();
        while (!stop && count > 0)
        {
            count--;

            Session session = HibernateUtil.currentSession();
            Transaction tx = session.beginTransaction();

            /* Remove the item from the list. */
            try
            {
                /* for deletion, the ids of the entities to delete must be: id1,id2,... */
                // String lastSegment = getRequest().getOriginalRef().getLastSegment();
                // List<String> idStrings = Arrays.asList(StringUtils.split(lastSegment, ','));
                List<Long> ids = Hd3dJsonSerializer.unserialize(getResourceContext().getQueryMap().get("ids"));

                objects = context.getPersistor().getByIds(context, ids, LockMode.PESSIMISTIC_WRITE);

                if (CollectUtils.isEmpty(objects))
                {
                    /* request successful because the data does not exist */
                    getResponse().setStatus(Status.SUCCESS_NO_CONTENT);
                    break;
                }

                if (preCheck(objects))
                {
                    doPreDelete(objects);

                    doDelete(objects);

                    if (session.getFlushMode() == FlushMode.MANUAL)
                    {
                        session.flush();
                    }
                    tx.commit();

                    tx = session.beginTransaction();

                    doPostDelete(objects);

                    if (session.getFlushMode() == FlushMode.MANUAL)
                    {
                        session.flush();
                    }
                    tx.commit();

                    Log.LOGGER.info(responseInfo(b));

                    stop = true;
                }
            }
            catch (StaleObjectStateException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                sleep();
            }
            catch (Hd3dPersistenceException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                if (e.isRetry())
                {
                    sleep();
                }
                else
                {
                    stop = true;
                }
            }
            catch (Hd3dException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                if (e.isRetry())
                {
                    sleep();
                }
                else
                {
                    stop = true;
                }
            }
            catch (HibernateException e)
            {
                doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                sleep();
            }
            catch (Exception e)
            {
                doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                stop = true;// no point to continue
            }
            finally
            {
                if (tx != null)
                {
                    tx = null;
                }
                cleanup();
            }
        }

        if (objects != null)
        {
            objects.clear();
            objects = null;
        }
        cleanup();

        return getResponseEntity();
    }

    protected abstract boolean preCheck(final List<P> objects);

    protected abstract void doPreDelete(final List<P> objects) throws Hd3dException;

    protected abstract void doDelete(final List<P> objects) throws Hd3dException;

    protected abstract void doPostDelete(final List<P> objects) throws Hd3dException;
}
