package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.FILEREVISION_ID_ATTRIBUTE;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.fileupload.FileItem;
import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILProxy;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ProxyH;
import fr.hd3d.services.resolver.DynamicPathResolver;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.FileRevisionUtils;


public class ProxiesUploadResource extends AbstractHd3dUploadBaseResource<ILProxy, IProxy>
{

    public ProxiesUploadResource() throws Hd3dException
    {
        super();
    }

    private final static String FILE = "file";
    private final static String DESCRIPTION = "description";
    private final static String PROXYTYPE = "proxyType";

    @Override
    protected Representation buildRepresentation(Map<String, Object> resultMap)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void parseTextFields(Map<String, Object> resultMap, FileItem fileItem)
    {
        if (resultMap == null || fileItem == null || !fileItem.isFormField())
            return;

        final String fieldName = fileItem.getFieldName();
        final String fieldValue = fileItem.getString();

        if (fieldName.equals(DESCRIPTION))
        {
            resultMap.put(DESCRIPTION, fieldValue);
        }
        else if (fieldName.equals(PROXYTYPE))
        {
            resultMap.put(PROXYTYPE, fieldValue);
        }
    }

    @Override
    protected void parseUploadedFiles(Map<String, Object> resultMap, FileItem fileItem)
    {
        if (resultMap == null || fileItem == null || fileItem.isFormField())
            return;

        if (fileItem.getFieldName().equals(FILE))
        {
            resultMap.put(FILE, fileItem);
        }
    }

    @Override
    protected void process(Map<String, Object> resultMap) throws Hd3dException
    {
        /* collect processed results */
        FileItem file = (FileItem) resultMap.get(FILE);

        IFileRevision currentFile = initializeObject();

        List<IProxyType> proxyTypes = Persistors.proxytype.getByValue("name", resultMap.get(PROXYTYPE));
        if (CollectionUtils.isEmpty(proxyTypes))
        {
            throw new Hd3dException("Unknown proxy type!!");
        }
        IProxyType proxyType = proxyTypes.get(0);

        String directory = DynamicPathResolver.getDynamicPathResolved(currentFile, true, proxyType.getName());
        String proxyFileName = currentFile.getRelativePath() + "." + proxyType.getName() + ".proxy";
        String dir = Conf.getFileTreeDir();
        directory = dir + directory;
        FileRevisionUtils.copyFile(file, directory, proxyFileName);

        String description = resultMap.get(DESCRIPTION) == null ? "" : (String) resultMap.get(DESCRIPTION);
        IProxy proxy = new ProxyH(currentFile, proxyType, "", directory, proxyFileName, description, 25);
        Persistors.proxy.persist(proxy);

    }

    protected IFileRevision initializeObject() throws Hd3dException
    {
        return Persistors.filerevision.getById(getResourceContext().getIdFromUrl(FILEREVISION_ID_ATTRIBUTE));
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
