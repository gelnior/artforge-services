package fr.hd3d.services.resources;

import java.io.File;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;

import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.impl.LComputer;
import fr.hd3d.model.lightweight.impl.LComputerModel;
import fr.hd3d.model.lightweight.impl.LComputerType;
import fr.hd3d.model.lightweight.impl.LDevice;
import fr.hd3d.model.lightweight.impl.LDeviceModel;
import fr.hd3d.model.lightweight.impl.LDeviceType;
import fr.hd3d.model.lightweight.impl.LItem;
import fr.hd3d.model.lightweight.impl.LItemGroup;
import fr.hd3d.model.lightweight.impl.LLicense;
import fr.hd3d.model.lightweight.impl.LPool;
import fr.hd3d.model.lightweight.impl.LScreen;
import fr.hd3d.model.lightweight.impl.LScreenModel;
import fr.hd3d.model.lightweight.impl.LSheet;
import fr.hd3d.model.lightweight.impl.LSoftware;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IComputerModel;
import fr.hd3d.model.persistence.IComputerType;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.IDeviceModel;
import fr.hd3d.model.persistence.IDeviceType;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.IScreenModel;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.FileRevisionUtils;
import fr.hd3d.utils.FilenameMaker;
import fr.hd3d.utils.Log;


public class DBInitializer
{

    public static IComputer createComputer(Session session, String name, String serial, String billingReference,
            Date purchaseDate, Date warrantyEnd, String inventoryStatus, Long inventoryId, String dnsName,
            String ipAdress, String macAdress, Integer procFrequency, Integer numberOfProcs, Integer numberOfCores,
            Integer ramQuantity) throws Hd3dException
    {
        IComputer object = Translators.computer.fromLightweight(LComputer.getNewInstance(name, serial,
                billingReference, purchaseDate, warrantyEnd, inventoryStatus, inventoryId, dnsName, ipAdress,
                macAdress, procFrequency, numberOfProcs, numberOfCores, ramQuantity, "NO_WORKER", 0, 0), null);

        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    public static ISoftware createSoftware(Session session, String name) throws Hd3dException
    {

        ISoftware object = Translators.software.fromLightweight(LSoftware.getNewInstance(name), null);

        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    public static ILicense createLicense(Session session, String name, String serial, String billingReference,
            Date purchaseDate, Date warrantyEnd, String inventoryStatus, Integer number, String type)
            throws Hd3dException
    {

        ILicense object = Translators.license.fromLightweight(LLicense.getNewInstance(name, serial, billingReference,
                purchaseDate, warrantyEnd, inventoryStatus, number, type), null);

        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    public static IDevice createDevice(Session session, String name, String serial, String billingReference,
            Date purchaseDate, Date warrantyEnd, String inventoryStatus, float size, String type) throws Hd3dException
    {

        IDevice object = Translators.device.fromLightweight(LDevice.getNewInstance(name, serial, billingReference,
                purchaseDate, warrantyEnd, inventoryStatus, size), null);

        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    public static IScreen createScreen(Session session, String name, String serial, String billingReference,
            Date purchaseDate, Date warrantyEnd, String inventoryStatus, float size, String type, String qualification)
            throws Hd3dException
    {

        IScreen object = Translators.screen.fromLightweight(LScreen.getNewInstance(name, serial, billingReference,
                purchaseDate, warrantyEnd, inventoryStatus, size, type, qualification), null);

        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    public static IPool createPool(Session session, String name, Boolean isDispatcherAllowed) throws Hd3dException
    {

        IPool object = Translators.pool.fromLightweight(LPool.getNewInstance(name, isDispatcherAllowed), null);

        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    public static ISheet createSheet(Session session, Long projectId, String name, String description,
            String boundClassName, ESheetType sheetType) throws Hd3dException
    {
        ISheet object = Translators.sheet.fromLightweight(LSheet.getNewInstance(name, description, projectId,
                boundClassName, null), null);
        object.setType(sheetType);
        session.save(object);
        session.flush();
        return object;
    }

    public static IItemGroup createItemGroup(Session session, Long sheetId, String name) throws Hd3dException
    {
        IItemGroup object = Translators.itemGroup.fromLightweight(LItemGroup.getNewInstance(name, null, sheetId, name
                .substring(0, 4)), null);

        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    public static IItem createItem(Session session, String name, String type, String query, String controlContent,
            Long itemGroup, String metaType, String renderer, String editor) throws Hd3dException
    {
        IItem object = Translators.item.fromLightweight(LItem.getNewInstance(name, type, query, controlContent,
                itemGroup, metaType, null, null), null);
        object.setRenderer(renderer);
        object.setEditor(editor);

        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    public static IDeviceType createDeviceType(Session session, String name) throws Hd3dException
    {
        IDeviceType object = Translators.deviceType.fromLightweight(LDeviceType.getNewInstance(name), null);

        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    public static IDeviceModel createDeviceModel(Session session, String name) throws Hd3dException
    {
        IDeviceModel object = Translators.deviceModel.fromLightweight(LDeviceModel.getNewInstance(name), null);

        object.doPrePersist();
        session.save(object);
        session.flush();

        return object;
    }

    public static IScreenModel createScreenModel(Session session, String name) throws Hd3dException
    {
        IScreenModel object = Translators.screenModel.fromLightweight(LScreenModel.getNewInstance(name), null);

        object.doPrePersist();
        session.save(object);
        session.flush();

        return object;
    }

    public static IComputerType createComputerType(Session session, String name) throws Hd3dException
    {
        IComputerType object = Translators.computerType.fromLightweight(LComputerType.getNewInstance(name), null);

        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    public static IComputerModel createComputerModel(Session session, String name) throws Hd3dException
    {
        IComputerModel object = Translators.computerModel.fromLightweight(LComputerModel.getNewInstance(name), null);

        object.doPrePersist();
        session.save(object);
        session.flush();

        return object;
    }

    public static IFileRevision createFileRevisionFromTempFile(String file_path, String thumbnail_path, IBase parent,
            IProject project)
    {
        IFileRevision ret = null;
        File f = new File(file_path);
        try
        {
            ret = FileRevisionUtils.createFileRevision(null, f, f.getName(), "standard", 1, EFileState.getDefault(),
                    true, null);

            if (ret == null)
            {
                return null;
            }
            if (parent != null)
            {
                FileRevisionUtils.attachFileToParent(ret, parent, project);
            }
            File dest = new File(FilenameMaker.getFullPath(ret));

            dest.getParentFile().mkdirs();

            if (FileRevisionUtils.copyFile(f, dest) == false)
            {
                return null;
            }
            if (StringUtils.isNotEmpty(thumbnail_path))
            {
                File thumb = new File(thumbnail_path);
                dest = new File(FilenameMaker.getImagePreviewFromPath(ret));
                if (FileRevisionUtils.copyFile(thumb, dest) == false)
                {
                    return null;
                }
            }
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }

        return ret;
    }
}
