package fr.hd3d.services.resources;

import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.impl.LProject;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.auth.FDTAuthUtils;
import fr.hd3d.services.security.dao.IUserAccount;
import fr.hd3d.services.security.dao.UserAccountH;
import fr.hd3d.utils.Bench;
import fr.hd3d.utils.HibernateUtil;


/**
 * Class to create the Bank project
 */
@Hd3dComment("Initialise la base de données avec le projet Bank nécessaire à la réut'.")
public class InitializeBankResource<L, P> extends AbstractHd3dBaseResource
{
    public static final String REUSEBANK_PROJECT_STRING = "[Reuse Bank]";

    protected static final Logger logger = Logger.getLogger("hd3d");

    /**
     * Default Constructor
     * 
     * @param context
     *            Context parameters
     * @param request
     *            Request received
     * @param response
     *            Response to send
     * @throws Hd3dException
     */
    public InitializeBankResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        getVariants().add(new Variant(MediaType.TEXT_PLAIN));
    }

    private static void run(IHibernateTransaction tx) throws Hd3dPersistenceException
    {
        Bench b = new Bench();
        b.b();
        Session session = HibernateUtil.currentSession();
        Transaction trx = session.beginTransaction();
        HibernateUtil.safeTransaction(tx);
        if (session.getFlushMode() == FlushMode.MANUAL)
        {
            session.flush();
        }
        trx.commit();
        trx = null;
        HibernateUtil.closeSession();

        logger.log(org.apache.log4j.Level.DEBUG, tx.getClass().getName() + " took:" + b.e() + " ms");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.restlet.resource.Resource#getRepresentation(org.restlet.resource. Variant)
     */
    @Override
    public Representation get(Variant arg0)
    {
        try
        {
            run(new InitDbCmd());
        }
        catch (Hd3dTranslationException e)
        {
            e.printStackTrace();
            return new StringRepresentation(e.getMessage());
        }
        return new StringRepresentation("Bank successfully added...");
    }

    /**
     * Database Initialization : persons, projects, tasks, activities and days
     * 
     * @author Nicolas Provost
     */
    private class InitDbCmd implements IHibernateTransaction
    {
        private Session session;

        /**
         * Create a new user account in User_Account Table. If it already exists, nothing is created.
         * 
         * @param login
         *            a person's login
         * @return User account newly created or user account in table if it already exists.
         * @throws Hd3dException
         */
        private IUserAccount getAccount(String login) throws Hd3dException
        {
            // Retrieve the user account.
            IUserAccount object = (IUserAccount) session.createCriteria(Persistors.useraccount.getPersistedClass())
                    .add(Restrictions.eq("principal", login)).uniqueResult();

            // If the user account is in database, return it.
            if (object != null)
            {
                return object;
            }

            // Else create a new user account instance.
            object = new UserAccountH(login);

            // Save user account in database.
            object.doPrePersist();
            session.save(object);
            session.flush();
            return object;
        }

        private void createPass(String login) throws Hd3dException
        {
            IUserAccount account = getAccount(login);
            account.setSecret(FDTAuthUtils.encrypt(login + "pass"));
            session.save(account);
        }

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {

            this.session = session;

            // Projects
            final IProject bankProject = getProject(REUSEBANK_PROJECT_STRING, EProjectStatus.OPEN, "#9e1fc8",
                    "REUSEBANK");

            bankProject.doPreUpdate();
            session.update(bankProject);
            session.flush();
            bankProject.doPostUpdate();

            // Commit
            session.flush();
        }

        private void save(ITaskType object) throws Hd3dException
        {
            object.doPrePersist();
            session.save(object);
            session.flush();
            object.doPostPersist();
        }

        /**
         * Create a new project in Project Table. If project already exists, no project is created.
         * 
         * @param name
         *            Project name
         * @param status
         *            Project status
         * @param color
         *            Project color
         * @return Project newly created or project in table if it already exists.
         * @throws Hd3dException
         */
        private IProject getProject(String name, EProjectStatus status, String color, String hook) throws Hd3dException
        {

            // Retrieve project.
            IProject object = (IProject) session.createCriteria(Persistors.project.getPersistedClass()).add(
                    Restrictions.eq("name", name)).uniqueResult();

            // If project is in database, return project.
            if (object != null)
            {
                return object;
            }

            // Else create a new project instance.
            object = Translators.project.fromLightweight(LProject.getNewInstance(name, status.toString(), color, hook,
                    null, null), null);

            // Save project in database.
            object.doPrePersist();
            session.save(object);
            object.doPostPersist();
            return object;
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
