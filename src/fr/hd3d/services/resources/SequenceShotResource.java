package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.SHOT_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération du shot {shotID}\r\n" + "\r\nPUT:Changement de nom d'un shot\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Suppression d'un shot")
public class SequenceShotResource extends AbstractHd3dResource<ILShot, IShot>
{
    public SequenceShotResource() throws Hd3dException
    {
        super(Persistors.shot, Translators.shot);
    }

    @Override
    protected IShot initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(SHOT_ID_ATTRIBUTE, lockMode);
    }
}
