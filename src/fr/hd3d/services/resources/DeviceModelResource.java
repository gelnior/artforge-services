package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.DEVICE_MODEL_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILDeviceModel;
import fr.hd3d.model.persistence.IDeviceModel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computers/{id}
 * 
 * @author HD3D
 */
public class DeviceModelResource extends AbstractHd3dResource<ILDeviceModel, IDeviceModel>
{
    @Override
    protected IDeviceModel initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(DEVICE_MODEL_ID_ATTRIBUTE, lockMode);
    }

    public DeviceModelResource() throws Hd3dException
    {
        super(Persistors.deviceModel, Translators.deviceModel);
    }
}
