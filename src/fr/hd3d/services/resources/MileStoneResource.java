package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.MILESTONE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILMileStone;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'un milestone\r\n" + "\r\nPUT:Changement d'un milestone\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Suppression d'un milestone")
public class MileStoneResource extends AbstractHd3dResource<ILMileStone, IMileStone>
{
    public MileStoneResource() throws Hd3dException
    {
        super(Persistors.mileStone, Translators.mileStone);
    }

    @Override
    protected IMileStone initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(MILESTONE_ID_ATTRIBUTE, lockMode);
    }
}
