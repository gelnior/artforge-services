package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILDynMetaDataValue;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Liste de toutes les VALEURS des métadonnées définies pour TOUS les instances d'entités.\r\n"
        + "\r\nPUT:modification d'une valeur \r\n" + "\r\nPOST:Création d'une valeur d'un type de métadonnée \r\n"
        + "\r\nDELETE:Effacement de la valeur ")
public class DynMetaDataValuesResource extends AbstractHd3dCollection<ILDynMetaDataValue, IDynMetaDataValue>
{
    public DynMetaDataValuesResource() throws Hd3dException
    {
        super(Persistors.dynmetadatavalue, Translators.dynmetadatavalue);
    }
}
