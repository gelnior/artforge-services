package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ACTIVITY_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération des informations d'un utilisateur\r\n" + "\r\nPUT:Modification d'une activité\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:Suppression d'une activité")
public class ActivityResource extends AbstractHd3dResource<ILActivity, IActivity>
{

    public ActivityResource() throws Hd3dException
    {
        super(Persistors.activity, Translators.activity);
    }

    @Override
    protected IActivity initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ACTIVITY_ID_ATTRIBUTE, lockMode);
    }
}
