package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author david gauthier
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class DaysResource extends AbstractHd3dCollection<ILPersonDay, IPersonDay>
{

    public DaysResource() throws Hd3dException
    {
        super(Persistors.personDay, Translators.personDay);
    }
}
