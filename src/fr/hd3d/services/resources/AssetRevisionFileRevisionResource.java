// package fr.hd3d.services.resources;
//
// import static fr.hd3d.common.client.ServicesURI.ASSETREVISIONFILEREVISION_ID_ATTRIBUTE;
//
// import java.util.List;
//
// import org.apache.commons.lang.math.NumberUtils;
// import org.apache.shiro.util.StringUtils;
// import org.hibernate.LockMode;
//
// import fr.hd3d.exception.Hd3dException;
// import fr.hd3d.model.lightweight.ILAssetRevisionFileRevision;
// import fr.hd3d.model.persistence.IAssetRevision;
// import fr.hd3d.model.persistence.IAssetRevisionFileRevision;
// import fr.hd3d.model.persistence.IFileRevision;
// import fr.hd3d.model.persistence.Persistors;
// import fr.hd3d.model.translator.Translators;
// import fr.hd3d.utils.CollectUtils;
// import fr.hd3d.utils.Const;
//
//
// /**
// * @author Try LAM
// */
// public class AssetRevisionFileRevisionResource extends
// AbstractHd3dResource<ILAssetRevisionFileRevision, IAssetRevisionFileRevision>
// {
//
// public AssetRevisionFileRevisionResource() throws Hd3dException
// {
// super(Persistors.assetrevisionfilerevision, Translators.assetrevisionfilerevision);
// }
//
// @Override
// protected IAssetRevisionFileRevision initializeObject(
// final ResourceContext<ILAssetRevisionFileRevision, IAssetRevisionFileRevision> context, LockMode lockMode)
// throws Hd3dException
// {
// String lastSegment = getRequest().getOriginalRef().getLastSegment();
//
// /* if no "-" in parameters: standard case */
// if (!lastSegment.contains("-"))
// return initializeObject(context, ASSETREVISIONFILEREVISION_ID_ATTRIBUTE, lockMode);
//
// /* if "-" in parameters: <AssetRevisionLinkInputId>-<AssetRevisionLinkOutputId> */
// String[] inputOutputIds = StringUtils.split((String) getRequest().getAttributes().get(
// ASSETREVISIONFILEREVISION_ID_ATTRIBUTE), '-');
//
// if (inputOutputIds != null && inputOutputIds.length != 2)
// return null;
//
// Long assetId = NumberUtils.toLong(inputOutputIds[0]);
// Long fileId = NumberUtils.toLong(inputOutputIds[1]);
//
// /* find out the AssetRevisions which matching ids */
// IAssetRevision asset = Persistors.assetrevision.getById(assetId);
// IFileRevision file = Persistors.filerevision.getById(fileId);
//
// String[] properties = new String[] { Const.ASSETKEY, Const.ASSETREVISION, Const.FILEKEY, Const.FILEREVISION };
//
// Object[] values = new Object[] { asset.getKey(), asset.getRevision(), file.getKey(), file.getRevision() };
//
// /* find out the AssetRevisionLink matching the previous AssetRevisions' field values */
// List<IAssetRevisionFileRevision> links = Persistors.assetrevisionfilerevision.getByValues(null, properties,
// values, "AND");
//
// if (CollectUtils.isEmpty(links))
// return null;
//
// IAssetRevisionFileRevision link = links.get(0);// there must be only one
// createDynMetaDataValue(link);
// return link;
// }
// }
