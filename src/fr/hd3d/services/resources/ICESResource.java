package fr.hd3d.services.resources;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.utils.Conf;


/**
 * @author Try LAM
 */
public class ICESResource<L, P> extends AbstractHd3dBaseResource
{
    protected final static String ICES_PATH = Conf.getICESPath();
    protected final static String ICES_UPLOAD_DIR = Conf.getICESUploadDir();
    protected final static String ICES_OUTPUT_DIR = Conf.getICESOutputDir();

    public ICESResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("POST");
    }

    @Override
    public Representation post(Representation entity, Variant variant)
    {
        if (entity != null)
        {
            if (MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true))
            {
                // Form form = new Form(entity);

                // 1/ Create a factory for disk-based file items
                DiskFileItemFactory factory = new DiskFileItemFactory();
                factory.setSizeThreshold(1000240);

                // 2/ Create a new file upload handler based on the Restlet
                // FileUpload extension that will parse Restlet requests and
                // generates FileItems.
                RestletFileUpload upload = new RestletFileUpload(factory);
                List<FileItem> items;
                try
                {
                    // 3/ Request is parsed by the handler which generates a
                    // list of FileItems
                    items = upload.parseRequest(getRequest());
                    // Process only the uploaded item called "fileToUpload" and
                    // save it on disk
                    boolean namefound = false, ipfound = false, opfound = false, bifound = false, bofound = false, tfound = false;

                    String nameField = null, ipfileName = null, opfileName = null, biField = null, boField = null, tField = null;
                    for (final Iterator<FileItem> it = items.iterator(); it.hasNext();)
                    {
                        FileItem fi = it.next();
                        if (fi.getFieldName().equals("ip"))
                        {
                            ipfound = true;
                            ipfileName = ICES_UPLOAD_DIR + SystemUtils.FILE_SEPARATOR + fi.getName();
                            File file = new File(ipfileName);
                            fi.write(file);
                            // persistFile(fi, file);
                        }
                        if (fi.getFieldName().equals("op"))
                        {
                            opfound = true;
                            opfileName = ICES_UPLOAD_DIR + SystemUtils.FILE_SEPARATOR + fi.getName();
                            File file = new File(opfileName);
                            fi.write(file);
                            // persistFile(fi, file);
                        }
                        if (fi.isFormField())
                        {
                            if ("name".equals(fi.getFieldName()))
                            {
                                nameField = fi.getString();
                                namefound = true;
                            }
                            if ("bo".equals(fi.getFieldName()))
                            {
                                boField = fi.getString();
                                bofound = true;
                            }
                            if ("bi".equals(fi.getFieldName()))
                            {
                                biField = fi.getString();
                                bifound = true;
                            }
                            if ("t".equals(fi.getFieldName()))
                            {
                                tField = fi.getString();
                                tfound = true;
                            }
                        }
                    }

                    // Once handled, the content of the uploaded file is sent
                    // back to the client.
                    Representation rep = null;
                    if (ipfound && opfound && bofound && bifound)
                    {
                        DefaultExecutor executor = new DefaultExecutor();
                        ExecuteWatchdog watchdog = new ExecuteWatchdog(60000);
                        executor.setWatchdog(watchdog);

                        String outputFileName = null;

                        try
                        {
                            CommandLine commandLine = CommandLine.parse(ICES_PATH);
                            commandLine.addArgument("-ip").addArgument(ipfileName);
                            commandLine.addArgument("-op").addArgument(opfileName);
                            commandLine.addArgument("-bi").addArgument(biField);
                            commandLine.addArgument("-bo").addArgument(boField);
                            if (StringUtils.isNotBlank(tField))
                                commandLine.addArgument("-t").addArgument(tField);
                            SimpleDateFormat format = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
                            outputFileName = nameField + '_' + format.format(new Date()) + ".3dl";
                            commandLine.addArgument("-o").addArgument(
                                    ICES_OUTPUT_DIR + SystemUtils.FILE_SEPARATOR + outputFileName);
                            executor.setExitValue(0);
                            executor.execute(commandLine);
                        }
                        catch (ExecuteException e)
                        {
                            // If CommandLine is called Asynchroniously, the exception is redirected to the handler
                            // exitValue = e.getExitValue();
                            // exitValues += s.getName() + " run with Error, exit code is " + exitValue +
                            // ", exeption is "
                            // + e.getMessage() + "\r\n";

                            rep = new StringRepresentation("Error occured: " + e, MediaType.TEXT_PLAIN);
                        }
                        catch (IOException e)
                        {
                            // If CommandLine is called Asynchroniously, the exception is redirected to the handler
                            // exitValue = handler.exitCode;
                            // exitValues += s.getName() + " run with Error, exit code is " + exitValue +
                            // ", exeption is "
                            // + e.getMessage() + "\r\n";
                            rep = new StringRepresentation("Error occured: " + e, MediaType.TEXT_PLAIN);
                        }

                        // Create a new representation based on disk file.
                        // The content is arbitrarily sent as plain text.
                        // rep = new FileRepresentation(new File(fileName), MediaType.TEXT_PLAIN, 0);

                        final String referrerUrl = getRequest().getReferrerRef().toString();
                        String url = referrerUrl.substring(0, referrerUrl.lastIndexOf('/'));

                        // String rootUrl = getRequest().getRootRef().toString();
                        // String originalUrl = getRequest().getOriginalRef().toString();

                        String page = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">"
                                + "<html>"
                                + "<head>"
                                + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
                                + "<title>Hd3d</title>"
                                + "</head>"
                                + "<body>"
                                + "ICES RESULT<br>"
                                + "Your result is <a href='"
                                + url
                                + "/icesdownload?id="
                                + outputFileName
                                + "'>here</a><br>"
                                + "Do not forget to delete it after download: <a href='"
                                + url
                                + "/icesdownload?delete=" + outputFileName + "'>DELETE</a>" + "</body>" + "</html>";
                        rep = new StringRepresentation(page, MediaType.TEXT_HTML);
                    }
                    else
                    {
                        // Some problem occurs, sent back a simple line of text.
                        rep = new StringRepresentation("no file uploaded", MediaType.TEXT_PLAIN);
                    }
                    // Set the representation of the resource once the POST
                    // request has been handled.
                    getResponse().setEntity(rep);
                    // Set the status of the response.
                    getResponse().setStatus(Status.SUCCESS_OK);
                    return getResponseEntity();
                }
                catch (Exception e)
                {
                    // The message of all thrown exception is sent back to
                    // client as simple plain text
                    getResponse().setEntity(new StringRepresentation(e.getMessage(), MediaType.TEXT_PLAIN));
                    getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
                    e.printStackTrace();
                    return getResponseEntity();
                }
            }
        }
        else
        {
            // POST request with no entity.
            getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            return getResponseEntity();
        }
        return getResponseEntity();
    }

    // @Override
    // public Representation represent(Variant variant)
    // {
    // // build the representation
    // Representation r = null;
    // r = RessourceSerializer.getRepresentation(variant, EntitiesMaps.entityNameToClassMap.keySet());
    // r.setCharacterSet(CharacterSet.UTF_8);
    // return r;
    // }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
