/**
 * 
 */
package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.TASK_TYPE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTaskType;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author thomas-eskenazi
 * 
 */
@Hd3dComment("GET:Récupération dun type de tâches {taskTypeID}\r\n" + "\r\nPUT:Modification d'un type de tâche\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:Désactivation d'un type de tâche")
public class TaskTypeResource extends AbstractHd3dResource<ILTaskType, ITaskType>
{
    public TaskTypeResource() throws Hd3dException
    {
        super(Persistors.tasktype, Translators.taskType);
    }

    @Override
    protected ITaskType initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(TASK_TYPE_ID_ATTRIBUTE, lockMode);
    }
}
