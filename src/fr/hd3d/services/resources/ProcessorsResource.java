package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILProcessor;
import fr.hd3d.model.persistence.IProcessor;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer pool objects.<br>
 * URI : /pools/
 * 
 * @author HD3D
 */
public class ProcessorsResource extends AbstractHd3dCollection<ILProcessor, IProcessor>
{
    public ProcessorsResource() throws Hd3dException
    {
        super(Persistors.processor, Translators.processor);
    }
}
