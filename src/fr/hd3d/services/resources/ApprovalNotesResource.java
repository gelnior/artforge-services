package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class ApprovalNotesResource extends AbstractHd3dCollection<ILApprovalNote, IApprovalNote>
{

    public ApprovalNotesResource() throws Hd3dException
    {
        super(Persistors.approvalnote, Translators.approvalnote);
    }

}
