package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ROOM_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILRoom;
import fr.hd3d.model.persistence.IRoom;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computers/{id}
 * 
 * @author HD3D
 */
public class RoomResource extends AbstractHd3dResource<ILRoom, IRoom>
{
    @Override
    protected IRoom initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ROOM_ID_ATTRIBUTE, lockMode);
    }

    public RoomResource() throws Hd3dException
    {
        super(Persistors.room, Translators.room);
    }
}
