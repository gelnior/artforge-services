package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.FILEREVISION_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class FileRevisionResource extends AbstractHd3dResource<ILFileRevision, IFileRevision>
{

    public FileRevisionResource() throws Hd3dException
    {
        super(Persistors.filerevision, Translators.filerevision);
    }

    @Override
    protected IFileRevision initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(FILEREVISION_ID_ATTRIBUTE, lockMode);
    }
}
