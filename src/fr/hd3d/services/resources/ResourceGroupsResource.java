package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Handle Group Resource
 * 
 * @author David Gauthier
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des groupes d'utilisateur\r\n"
        + "\r\nPUT:Mise à jour d'un groupe d'utilisateur\r\n" + "\r\nPOST:Ajout d'un nouveau groupe d'utilisateur\r\n"
        + "\r\nDELETE:N/A")
public final class ResourceGroupsResource extends AbstractHd3dCollection<ILResourceGroup, IResourceGroup>
{

    public ResourceGroupsResource() throws Hd3dException
    {
        super(Persistors.resourcegroup, Translators.resourcegroup);
    }
}
