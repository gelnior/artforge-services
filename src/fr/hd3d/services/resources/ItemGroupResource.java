package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.common.client.ServicesURI.ITEMGROUP_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;
import org.hibernate.Transaction;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.Persist;
import fr.hd3d.exception.Hd3dAuthorizationException;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILItemGroup;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.ItemTypeHandler;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.services.translator.json.JSonFormSerializer;
import fr.hd3d.utils.Bench;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.transformer.GetItemTransformer;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des 'items' (type de query + query(nom de l'attribut pour une query de type "
        + "'attribute') + metatype + control content) présents dans un 'itemGroup' (avec à terme le 'profil' "
        + "de l'utilisateur en paramètre)."
        + "Avec le paramètre ?objectId=XXXX, récupère les VALEURS des attributs référencés par les Items du ItemGroup"
        + "sous la forme d'une liste de:"
        + "'itemName':nom de l'attribut"
        + "'itemValue':valeur de l'attribut"
        + "'controlContent':valeur du controlContent"
        + "'metaType':metaType de l'attribut\r\n"
        + "\r\nPUT:Mise à jour des 'items' contenus dans l'itemGroup, et dont les valeurs ont été modifiées."
        + "Ce qu'il faut PUTer:"
        + "Liste de Map:"
        + "'itemName' = updated Item name"
        + "'itemValue' = updated item value."
        + "Ex: 'itemName' = 'label', 'itemValue' = 'Mon label' modifie l'attribut 'label' à 'Mon label' de "
        + "l'instance de la classe liée, l'instance étant identifiée par objectId=XXXX passé en paramètre.\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class ItemGroupResource extends AbstractHd3dResource<ILItemGroup, IItemGroup>
{
    public final static String ID = "id";
    public final static String OBJECTID = "objectId";
    public final static String NAME = "name";
    public final static String VALUE = "value";
    public final static String EXTRA = "extra";
    public final static String ITEMGROUPID = "itemGroupId";
    public final static String CONTROLCONTENT = "controlContent";
    public final static String METATYPE = "metaType";

    public ItemGroupResource() throws Hd3dException
    {
        super(Persistors.itemgroup, Translators.itemGroup);
    }

    @Override
    protected IItemGroup initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ITEMGROUP_ID_ATTRIBUTE, lockMode);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Representation get(Variant variant)
    {
        Bench b = new Bench();

        /* get the parameter ?objectId=XXXX */
        String objectIdString = getQuery().getValues(OBJECTID);

        if (StringUtils.isBlank(objectIdString))
        {
            return super.get(variant);
        }

        if (!initObject())
        {
            return getResponseEntity();
        }

        if (object == null)
        {
            /*
             * NOTE: do not set Status to Status.SUCCESS_NO_CONTENT If so, there will be no empty list returned in the
             * HTML screen, which may be confusing
             */
            return getResponseEntity();
        }

        Long objectId = NumberUtils.toLong(objectIdString);
        // apply the itemGroup's items query to the class bound to the sheet
        IPersist persistor = Persistors.getPersistor(object.getSheet().getBoundClassName());

        if (persistor == null)
            return getResponseEntity();

        try
        {
            final IBase persistedObject = persistor.getById(objectId);
            Representation r = null;

            if (persistedObject.canRead())
            {
                Collection<Map<String, Object>> response = org.apache.commons.collections15.CollectionUtils.collect(
                        object.getItems(), new GetItemTransformer(persistedObject, false, getResourceContext(),
                                new ArrayList() {
                                    {
                                        add(persistedObject);
                                    }
                                }));

                r = RessourceSerializer.getRepresentation(variant, response);
            }
            else
            {
                // FIXME do proper return
                r = new StringRepresentation("client does not have the right to read " + persistedObject.entityName()
                        + " with id " + persistedObject.getId());
                getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
            }

            r.setCharacterSet(CharacterSet.UTF_8);

            Log.LOGGER.info(responseInfo(b));

            return r;
        }
        catch (Hd3dException e)
        {
            error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
        }
        finally
        {
            cleanup();
        }
        return super.get(variant);
    }

    /**
     * partial PUT: update of some attributes The expected input is a list of maps: <br>
     * "queryName" = updated Item name <br>
     * "queryValue" = updated item value <br>
     * Note: the remaining item fields ... <br>
     * "controlContent", "metaType" <br>
     * ...are not updated, since the purpose is to update the bound class
     */
    @SuppressWarnings("unchecked")
    @Override
    public Representation put(Representation entity, Variant variant)
    {
        Bench b = new Bench();

        List<Map<String, Object>> lObjects = null;
        int count = 3;
        boolean stop = false;
        while (!stop && count > 0)
        {
            count--;

            Session session = HibernateUtil.currentSession();
            Transaction tx = session.beginTransaction();
            try
            {
                if (!initObject(LockMode.OPTIMISTIC))
                {
                    return getResponseEntity();
                }

                if (object == null)
                {
                    /*
                     * NOTE: do not set Status to Status.SUCCESS_NO_CONTENT If so, there will be no empty list returned
                     * in the HTML screen, which may be confusing
                     */
                    return getResponseEntity();
                }

                final Long objectId = getObjectId();

                if (lObjects == null)
                {
                    lObjects = getLightObjects(entity);
                }

                final String boundClassName = getBoundEntityClassName();

                final IPersist persistor = Persistors.getPersistor(boundClassName);

                final IBase persistedObject = getPersistedObject(persistor, objectId);

                /* keep trace of the object state before update */
                // final ILBase objectLightCopy = Translators.getTranslator(persistedObject.entityName()).toLightweight(
                // persistedObject, null);
                /* get the updated values from request */
                for (Map<String, Object> fieldValueMap : CollectUtils.selectNotNull(lObjects))
                {
                    final IItem item = object.itemByName((String) fieldValueMap.get(NAME));
                    if (item == null)
                    {
                        Log.LOGGER.warn("No item found for " + (String) fieldValueMap.get(NAME));
                        continue;
                    }

                    /* PreUpdate */
                    persistedObject.doPreUpdate();

                    ItemTypeHandler itemHandler = new ItemTypeHandler(persistedObject, item, null);
                    itemHandler.setValue(fieldValueMap.get(VALUE));
                    itemHandler.complete(fieldValueMap.get(EXTRA));
                }
                executeScripts(persistedObject, Persist.PREUPDATE, getRequest().getResourceRef().toString());

                /* Update */
                persistor.merge(persistedObject);

                if (session.getFlushMode() == FlushMode.MANUAL)
                {
                    session.flush();
                }
                tx.commit();

                /* PostUpdate */
                tx = session.beginTransaction();

                persistedObject.doPostUpdate();
                executeScripts(persistedObject, Persist.POSTUPDATE, getRequest().getResourceRef().toString());

                if (session.getFlushMode() == FlushMode.MANUAL)
                {
                    session.flush();
                }
                tx.commit();

                stop = true;

                getResponse().setStatus(Status.SUCCESS_OK);

                Log.LOGGER.info(responseInfo(b));
            }
            catch (IllegalArgumentException e)
            {
                /*
                 * BeanUtils API DynaBean.set method may raise an exception with embedded exception, so get rid of it,
                 * just keep the message
                 */
                Log.LOGGER.error(ExceptUtils.format(e));
                if (tx != null)
                    tx.rollback();
                error(Status.SERVER_ERROR_INTERNAL, new Hd3dRuntimeException(StringUtils.substringAfter(e.getMessage(),
                        ":")), ERROR_OCCURED);
                // return getResponseEntity();
                stop = true;
            }
            catch (ConversionException e)
            {
                error(Status.CLIENT_ERROR_BAD_REQUEST, e, ERROR_OCCURED);
                // return getResponseEntity();
                stop = true;
            }
            catch (StaleObjectStateException e)
            {
                // doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                sleep();
            }
            catch (Hd3dPersistenceException e)
            {
                doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                if (e.isRetry())
                {
                    sleep();
                }
                else
                {
                    stop = true;
                }
            }
            catch (HibernateException e)
            {
                if (tx != null)
                    tx.rollback();
                error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
                // return getResponseEntity();
                stop = true;
            }
            catch (Hd3dAuthorizationException e)
            {
                if (tx != null)
                    tx.rollback();
                error(Status.CLIENT_ERROR_FORBIDDEN, e, ERROR_OCCURED);
                // return getResponseEntity();
                stop = true;
            }
            catch (Hd3dException e)
            {
                if (tx != null)
                    tx.rollback();
                error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
                // return getResponseEntity();
                stop = true;
            }
            catch (Exception e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
                if (tx != null)
                {
                    tx.rollback();
                }
                error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
                // return getResponseEntity();
                stop = true;
            }
            finally
            {
                if (tx != null)
                    tx = null;
                cleanup();
            }
        }

        return getResponseEntity();
    }

    private Long getObjectId() throws Hd3dException
    {
        Long objectId = NumberUtils.toLong(getQuery().getValues(OBJECTID));
        if (BaseH.isNotValidId(objectId))
        {
            throw new Hd3dException("objectId not found in query.");
        }
        return objectId;
    }

    private List<Map<String, Object>> getLightObjects(Representation entity) throws Hd3dException
    {
        final List<Map<String, Object>> lObjects = JSonFormSerializer.<List<Map<String, Object>>> deserialize(entity);
        if (CollectUtils.isEmpty(lObjects))
        {
            throw new Hd3dException("empty light objects after deserialization.");
        }

        return lObjects;
    }

    private String getBoundEntityClassName() throws Hd3dException
    {
        final String boundClassName = object.getSheet().getBoundClassName();
        if (EntitiesMaps.isNotEntity(boundClassName))
        {
            throw new Hd3dException(boundClassName + " is not an entity.");
        }
        return boundClassName;
    }

    private IBase getPersistedObject(final IPersist<?> persistor, final Long objectId) throws Hd3dException
    {
        final IBase persistedObject = persistor.getById(objectId, LockMode.UPGRADE_NOWAIT);
        if (persistedObject == null)
        {
            throw new Hd3dException("Cannot retrieve entity with id=" + objectId + " and type="
                    + persistor.getPersistedClass());
        }

        /* if not allowed to update, just return */
        if (!persistedObject.canUpdate())
        {
            throw new Hd3dAuthorizationException("Not allowed to update.");
        }
        return persistedObject;
    }
}
