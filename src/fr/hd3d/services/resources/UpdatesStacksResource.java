package fr.hd3d.services.resources;

import java.util.List;

import org.hibernate.LockMode;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.audit.UpdatesStack;
import fr.hd3d.model.lightweight.impl.LUpdatesStack;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetUpdatesStacksCollectionCmd;
import fr.hd3d.model.translator.impl.UpdatesStackTranslator;
import fr.hd3d.services.resources.serializer.RessourceSerializer;


public class UpdatesStacksResource extends AbstractHd3dCollectionResource<LUpdatesStack, UpdatesStack>
{
    public UpdatesStacksResource() throws Hd3dException
    {
        super(UpdatesStack.class, new UpdatesStackTranslator());
        allowMethods("GET");
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection<UpdatesStack> getCollectionForRepresentation() throws Hd3dException
    {
        return getCollection(new GetUpdatesStacksCollectionCmd(getResourceContext()).wantTotalSize());
    }

    @SuppressWarnings("unchecked")
    protected ResultCollection reloadCollectionIfAny(final ResultCollection luceneFiltered, boolean onIdsOnly)
            throws Hd3dException
    {
        return luceneFiltered;
    }

    @Override
    protected List<LUpdatesStack> getLightWeightList(ResultCollection<UpdatesStack> list) throws Hd3dException
    {
        return getResourceContext().getTranslator().toLightweightCollection(list.getResult(), getResourceContext());
    }

    @Override
    protected Representation getLightWeightRepresentation(Variant variant, List<LUpdatesStack> lightweightList,
            int nbRecords)
    {
        return RessourceSerializer.<LUpdatesStack> getRepresentation(variant, lightweightList, nbRecords);
    }

    protected void doPrePersistCollection(Representation entity, List<UpdatesStack> newObjects)
            throws Hd3dTranslationException
    {
    /* not allowed */
    }

    protected void doPersist(final List<UpdatesStack> objects) throws Hd3dException
    {
    /* not allowed */
    }

    protected void doPostPersist(List<UpdatesStack> objects) throws Hd3dPersistenceException
    {
        successfulPost(objects);
    }

    protected void doPrePersist(Representation entity, UpdatesStack object) throws Hd3dTranslationException
    {
    /* not allowed */
    }

    protected UpdatesStack newObject(Representation entity) throws Hd3dTranslationException
    {
        /* not allowed */
        return null;
    }

    protected List<UpdatesStack> newCollection(Representation entity) throws Hd3dTranslationException
    {
        /* not allowed */
        return null;
    }

    protected List<UpdatesStack> retrieveObjects(Representation entity, List<LUpdatesStack> lObjects, LockMode lockMode)
            throws Hd3dTranslationException
    {
        return null;
    }

    protected void doPersist(final UpdatesStack object) throws Hd3dPersistenceException
    {
    /* not allowed */
    }

    protected void doPostPersist(UpdatesStack object) throws Hd3dPersistenceException
    {
        successfulPost(object);
    }

    protected String getNewResourceIdentifier(Object newObject)
    {
        /* not allowed */
        return null;
    }

    @Override
    protected void doPostUpdate(List<UpdatesStack> objects, Representation entity) throws Hd3dTranslationException
    {}

    @Override
    protected void doPreUpdate(Representation entity, List<LUpdatesStack> lObjects, List<UpdatesStack> objects)
            throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    @Override
    protected void doUpdate(List<LUpdatesStack> lObjects, List<UpdatesStack> objects) throws Hd3dPersistenceException
    {/* nothing to do */}

    // ============================================================
    //
    // Override DELETE methods
    //
    // ============================================================
    protected boolean preCheck(final List<UpdatesStack> objects)
    {
        return true;
    }

    protected void doPreDelete(final List<UpdatesStack> objects) throws Hd3dException
    {
    /* nothing to do */
    }

    protected void doDelete(final List<UpdatesStack> objects) throws Hd3dException
    {
        ((HibernatePersist) getResourceContext().getPersistor()).deleteRawCollection(objects);
    }

    protected void doPostDelete(final List<UpdatesStack> objects) throws Hd3dException
    {
        /* nothing to do */
        getResponse().setStatus(Status.SUCCESS_OK);
    }
}
