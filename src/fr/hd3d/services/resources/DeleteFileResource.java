package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.io.File;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.FilenameMaker;
import fr.hd3d.utils.HibernateUtil;


/**
 * @author Nicolas Jauffret
 */
@Hd3dComment("Webservice de suppression des fichiers. <br>\n" + "GET/POST: id= id du fichier à supprimer"
        + "<br>\r\nPUT:N/A\r\n" + "<br>\r\nDELETE:N/A")
public class DeleteFileResource<L, P> extends AbstractHd3dBaseResource
{
    private static final CharSequence ERR_0 = "Please specify an Id";
    private static final CharSequence ERR_1 = "Could not delete the specified file";
    private static final CharSequence ERR_2 = "The file was successfully deleted";
    private static final CharSequence ERR_3 = "Could not delete the specified file";

    protected final static String UPLOAD_DIR = Conf.getUploadDir();

    public DeleteFileResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("POST");
    }

    @Override
    public Representation get(Variant variant)
    {
        try
        {
            return doTreatIt();
        }
        finally
        {
            cleanup();
        }
    }

    private Representation doTreatIt()
    {
        String id = getQuery().getValues(Const.ID);

        if (id == null)
        {
            return new StringRepresentation(ERR_0);
        }
        else
        {
            Session session = HibernateUtil.currentSession();
            org.hibernate.Transaction tx = session.beginTransaction();
            try
            {
                IFileRevision file = Persistors.filerevision.getById(Long.parseLong(id));

                if (file != null)
                {
                    // deleteFileAssets(file);

                    IAssetRevision assetRevision = file.getAssetRevision();
                    if (assetRevision != null && assetRevision.getFileRevisions() != null)
                        assetRevision.getFileRevisions().remove(file);
                    Persistors.filerevision.delete(file);
                    if (session.getFlushMode() == FlushMode.MANUAL)
                    {
                        session.flush();
                    }
                    tx.commit();

                    String fullPath = FilenameMaker.getFullPath(file);

                    File f = new File(fullPath);
                    if (!f.delete())
                    {
                        return new StringRepresentation(ERR_1);
                    }

                    f = new File(FilenameMaker.getImagePreviewFromPath(fullPath));
                    f.delete();
                    f = new File(FilenameMaker.getMoviePreviewFromPath(fullPath));
                    f.delete();

                    return new StringRepresentation(ERR_2);
                }
            }
            catch (Hd3dException e)
            {
                return new StringRepresentation(ERR_3);
            }
            finally
            {
                if (tx != null)
                    tx = null;
                cleanup();
            }
        }
        return null;
    }

    private void deleteFileAssets(IFileRevision file)
    {
    // String[] properties = { Const.KEY, Const.VARIATION, Const.REVISION };
    // Object[] values = { file.getKey(), file.getVariation(), file.getRevision() };
    //
    // try
    // {
    // List<IAssetRevision> assetRevisions = Persistors.assetrevision.getByValues(null, properties, values, "AND");
    // if (assetRevisions != null)
    // {
    // for (IAssetRevision asset : assetRevisions)
    // {
    // System.out.println(asset.getAssetRevisionGroups());
    // // On ne devrait passer qu'une seule fois ici
    // for (IAssetRevisionFileRevision af : asset.getAssetRevisionFileRevisions())
    // Persistors.assetrevisionfilerevision.delete(af);
    //
    // // asset.getAssetRevisionGroups().clear();
    //
    // for (IAssetRevisionGroup group : asset.getAssetRevisionGroups())
    // {
    // group.removeAssetRevision(asset);
    // // asset.removeAssetRevisionGroup(group);
    // }
    // asset.getAssetRevisionGroups().clear();
    // // PersistedObjectProviders.assetrevision.deleteObject(asset);
    // }
    // // remove the assets from their groups
    // HibernatePersist.deleteRawCollection(assetRevisions);
    // }
    // }
    // catch (Hd3dException e)
    // {
    // error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
    // }

    }

    @Override
    public Representation post(Representation entity, Variant variant)
    {
        try
        {
            if (entity != null)
            {
                try
                {
                    Representation rep = doTreatIt();
                    getResponse().setEntity(rep);
                    // Set the status of the response.
                    getResponse().setStatus(Status.SUCCESS_OK);
                    return rep;
                }
                catch (Exception e)
                {
                    error(Status.CLIENT_ERROR_BAD_REQUEST, e, ERROR_OCCURED);
                    return new StringRepresentation(ExceptUtils.format(e));

                    // The message of all thrown exception is sent back to
                    // client as simple plain text
                    // getResponse().setEntity(new StringRepresentation(e.getMessage(), MediaType.TEXT_PLAIN));
                    //
                    // getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
                    // e.printStackTrace();
                    // return getResponseEntity();
                }
            }
            else
            {
                // POST request with no entity.
                getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
                return getResponseEntity();
            }
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
