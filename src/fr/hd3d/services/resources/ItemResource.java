package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ITEM_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILItem;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de l'item\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Création d'un content unit (avec éventuellement un lien vers un shot)\r\n" + "\r\nDELETE:N/A")
public class ItemResource extends AbstractHd3dResource<ILItem, IItem>
{
    public ItemResource() throws Hd3dException
    {
        super(Persistors.item, Translators.item);
    }

    @Override
    protected IItem initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ITEM_ID_ATTRIBUTE, lockMode);
    }
}
