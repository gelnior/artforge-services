package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectTasksCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des taches d'un projet\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'une tache\r\n" + "\r\nDELETE:N/A")
public class ProjectTasksResource extends AbstractHd3dCollection<ILTask, ITask>
{
    public ProjectTasksResource() throws Hd3dException
    {
        super(Persistors.task, Translators.task);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectTasksCmd(getResourceContext()).wantTotalSize());
    }

}
