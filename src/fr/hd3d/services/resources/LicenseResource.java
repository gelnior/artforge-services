package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.LICENSE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILLicense;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /licenses/{id}
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'une licence\r\n" + "\r\nPUT:Modification d'une licence\r\n"
        + "\r\nPOST:Désactivation d'une licence\r\n" + "\r\nDELETE:Désactivation d'une licence")
public class LicenseResource extends AbstractHd3dResource<ILLicense, ILicense>
{
    @Override
    protected ILicense initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(LICENSE_ID_ATTRIBUTE, lockMode);
    }

    public LicenseResource() throws Hd3dException
    {
        super(Persistors.license, Translators.license);
    }
}
