package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.CONSTITUENT_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération du constituent\r\n" + "\r\nPUT:Changement de nom d'un constituent\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:Suppression d'un constituent")
public class ConstituentResource extends AbstractHd3dResource<ILConstituent, IConstituent>
{
    public ConstituentResource() throws Hd3dException
    {
        super(Persistors.constituent, Translators.constituent);
    }

    @Override
    protected IConstituent initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(CONSTITUENT_ID_ATTRIBUTE, lockMode);
    }
}
