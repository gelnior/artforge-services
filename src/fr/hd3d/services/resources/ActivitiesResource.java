package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET: Retrieve activity list. <br />\r\n" + "POST:Create an activity.<br />\r\n")
public class ActivitiesResource extends AbstractHd3dCollection<ILActivity, IActivity>
{
    public ActivitiesResource() throws Hd3dException
    {
        super(Persistors.activity, Translators.activity);
    }
}
