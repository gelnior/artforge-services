package fr.hd3d.services.resources;

import org.hibernate.LockMode;
import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonSimpleActivityCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.utils.HibernateUtil;


/**
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'une activité\r\n" + "\r\nPUT:Modification d'une activité\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Suppression d'une activité")
public class MySimpleActivityResource extends AbstractHd3dResource<ILSimpleActivity, ISimpleActivity>
{
    public MySimpleActivityResource() throws Hd3dException
    {
        super(Persistors.simpleActivity, Translators.simpleActivity);
    }

    @Override
    protected ISimpleActivity initializeObject(LockMode lockMode) throws Hd3dException
    {
        final GetPersonSimpleActivityCmd cmd = new GetPersonSimpleActivityCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        ISimpleActivity obj = cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }

    @Override
    protected void doPreUpdate(Representation entity, ILSimpleActivity personDay) throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user == null)
            return;
        if (user.getId().longValue() == object.getWorker().getId().longValue())
        {
            super.doPreUpdate(entity, personDay);
        }
        else
        {
            throw new Hd3dException("You cannot modify activity you don't own.");
        }
    }

    @Override
    protected void doPreDelete() throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user == null)
            return;
        if (user.getId().longValue() == object.getWorker().getId().longValue())
        {
            super.doPreDelete();
        }
        else
        {
            throw new Hd3dException("You cannot delete activity you don't own.");
        }
    }
}
