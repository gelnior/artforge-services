package fr.hd3d.services.resources;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetWorkObjectNotesCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.Const;


/**
 * ConstituentNotesResource returns notes linked to given constituent.
 * 
 * @author HD3D
 */
public class ConstituentNotesResource extends AbstractHd3dCollection<ILApprovalNote, IApprovalNote>
{
    public ConstituentNotesResource() throws Hd3dException
    {
        super(Persistors.approvalnote, Translators.approvalnote);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetWorkObjectNotesCmd(getResourceContext(), ServicesURI.CONSTITUENT_ID_ATTRIBUTE,
                Const.CONSTITUENT).wantTotalSize());
    }
}
