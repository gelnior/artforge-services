package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.FILEREVISION_ANNOTATION_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevisionAnnotation;
import fr.hd3d.model.persistence.IFileRevisionAnnotation;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class FileRevisionAnnotationResource extends
        AbstractHd3dResource<ILFileRevisionAnnotation, IFileRevisionAnnotation>
{

    public FileRevisionAnnotationResource() throws Hd3dException
    {
        super(Persistors.filerevisionannotation, Translators.filerevisionannotation);
    }

    @Override
    protected IFileRevisionAnnotation initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(FILEREVISION_ANNOTATION_ID_ATTRIBUTE, lockMode);
    }
}
