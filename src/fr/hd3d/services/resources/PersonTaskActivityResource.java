package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonTaskActivityCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


/**
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'une activité\r\n" + "\r\nPUT:Modification d'une activité\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Suppression d'une activité")
public class PersonTaskActivityResource extends AbstractHd3dResource<ILTaskActivity, ITaskActivity>
{
    public PersonTaskActivityResource() throws Hd3dException
    {
        super(Persistors.taskActivity, Translators.taskActivity);
    }

    @Override
    protected ITaskActivity initializeObject(LockMode lockMode) throws Hd3dException
    {
        final GetPersonTaskActivityCmd cmd = new GetPersonTaskActivityCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        ITaskActivity obj = cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }
}
