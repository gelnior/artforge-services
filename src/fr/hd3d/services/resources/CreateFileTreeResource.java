package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.util.Map;

import org.hibernate.LockMode;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.exec.ScriptExec;
import fr.hd3d.model.lightweight.ILScript;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.ServerResourceUtils;


public class CreateFileTreeResource extends AbstractHd3dResource<ILScript, IScript>
{

    public CreateFileTreeResource() throws Hd3dException
    {
        super(Persistors.script, Translators.script);
    }

    @Override
    protected IScript initializeObject(LockMode lockMode) throws Hd3dException
    {
        return (IScript) getResourceContext().getPersistor().getObjectByValue("name", "createFileTree");
    }

    @Override
    public Representation get(Variant variant)
    {

        if (!initObject())
        {
            return getResponseEntity();
        }
        if (object == null)
        {
            /*
             * NOTE: do not set Status to Status.SUCCESS_NO_CONTENT If so, there will be no empty list returned in the
             * HTML screen, which may be confusing
             */
            return getResponseEntity();
        }

        // if requested by name, execute the script identified by its name, passing it parameters
        // ex: http://..../scripts/{script_name}?params=...
        Map<String, String> maps = getQuery().getValuesMap();
        String params = maps.get("params");
        params = params.replace("assetDir", "'assetDir':'" + Conf.getFileTreeDir() + "'");
        maps.put("params", params);
        ScriptExec scriptExec = new ScriptExec(maps);

        try
        {
            String exitValues = scriptExec.exec(object);
            Representation r = new StringRepresentation(exitValues);
            r.setCharacterSet(CharacterSet.UTF_8);
            Log.LOGGER.info(responseInfo());
            return ServerResourceUtils.encodeRepresentation(r);
        }
        catch (Hd3dPersistenceException e)
        {
            error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
            return getResponseEntity();
        }
    }

}
