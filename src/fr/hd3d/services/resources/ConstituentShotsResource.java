package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetConstituentShotsCmd;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;


/**
 * ConstituentShotsResource returns shots linked to constituent via compositions.
 * 
 * @author HD3D
 */
public class ConstituentShotsResource extends AbstractHd3dCollection<ILShot, IShot>
{
    public ConstituentShotsResource(IPersist<IShot> persistor, IBaseTranslator<ILShot, IShot> translator)
            throws Hd3dException
    {
        super(persistor, translator);
        allowMethods("GET");
    }

    public ConstituentShotsResource() throws Hd3dException
    {
        super(Persistors.shot, Translators.shot);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetConstituentShotsCmd(getResourceContext()).wantTotalSize());
    }
}
