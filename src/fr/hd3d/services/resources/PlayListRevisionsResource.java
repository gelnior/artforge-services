package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class PlayListRevisionsResource extends AbstractHd3dCollection<ILPlayListRevision, IPlayListRevision>
{

    public PlayListRevisionsResource() throws Hd3dException
    {
        super(Persistors.playlistrevision, Translators.playlistrevision);
    }

}
