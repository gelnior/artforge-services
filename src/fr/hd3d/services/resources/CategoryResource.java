package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.CATEGORY_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des sous-catégories situées juste en dessous de la catégorie indiquée\r\n"
        + "\r\nPUT:Changement de nom d'une catégorie (ou sous-catégorie)\r\n"
        + "\r\nPOST:Ajout d'une SOUS-CATEGORIE par le nom\r\n"
        + "\r\nDELETE:Suppression d'une catégorie (ou sous-catégorie) et de tous ses fils.")
public class CategoryResource extends AbstractHd3dResource<ILCategory, ICategory>
{
    public CategoryResource() throws Hd3dException
    {
        super(Persistors.category, Translators.category);
    }

    @Override
    protected ICategory initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(CATEGORY_ID_ATTRIBUTE, lockMode);
    }
}
