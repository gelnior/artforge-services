package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ASSETREVISION_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


public class AssetAbstractRevisionResource extends AbstractHd3dResource<ILAssetRevision, IAssetRevision>
{
    public AssetAbstractRevisionResource() throws Hd3dException
    {
        super(Persistors.assetrevision, Translators.assetrevision);
    }

    @Override
    protected IAssetRevision initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ASSETREVISION_ID_ATTRIBUTE, lockMode);
    }

}
