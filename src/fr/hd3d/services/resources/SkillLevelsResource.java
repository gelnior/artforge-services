package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSkillLevel;
import fr.hd3d.model.persistence.ISkillLevel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer skillLevel objects.<br>
 * URI : /skillLevels/
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération des skillLevel de machines\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Création d'un skillLevel de machines\r\n" + "\r\nDELETE:N/A")
public class SkillLevelsResource extends AbstractHd3dCollection<ILSkillLevel, ISkillLevel>
{

    public SkillLevelsResource() throws Hd3dException
    {
        super(Persistors.skillLevel, Translators.skillLevel);
    }
}
