package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.SKILLLEVEL_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSkillLevel;
import fr.hd3d.model.persistence.ISkillLevel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computers/{id}
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'un skillLevel de machines\r\n"
        + "\r\nPUT:Modification d'un skillLevel de machines\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Désactivation d'un skillLevels de machines")
public class SkillLevelResource extends AbstractHd3dResource<ILSkillLevel, ISkillLevel>
{
    @Override
    protected ISkillLevel initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(SKILLLEVEL_ID_ATTRIBUTE, lockMode);
    }

    public SkillLevelResource() throws Hd3dException
    {
        super(Persistors.skillLevel, Translators.skillLevel);
    }
}
