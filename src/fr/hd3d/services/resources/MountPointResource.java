package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.MOUNTPOINT_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILMountPoint;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * 
 * @author HD3D
 * 
 */
public class MountPointResource extends AbstractHd3dResource<ILMountPoint, IMountPoint>
{

    public MountPointResource() throws Hd3dException
    {
        super(Persistors.mountpoint, Translators.mountpoint);
    }

    @Override
    protected IMountPoint initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(MOUNTPOINT_ID_ATTRIBUTE, lockMode);
    }

}
