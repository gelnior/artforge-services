package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILEvent;
import fr.hd3d.model.persistence.IEvent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des events\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:Ajout d'une event\r\n"
        + "\r\nDELETE:N/A")
public class EventsResource extends AbstractHd3dCollection<ILEvent, IEvent>
{
    public EventsResource() throws Hd3dException
    {
        super(Persistors.event, Translators.event);
    }
}
