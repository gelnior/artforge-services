package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonProjectCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.UniqueResultQuery;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


/**
 * A resource to access to a specific project of a specific person.
 * 
 * @author Frank Rousseau
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération des informations d'une projets\r\n" + "\r\nPUT:Mise a jour d'un projet\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class PersonProjectResource extends AbstractHd3dResource<ILProject, IProject>
{
    public PersonProjectResource() throws Hd3dException
    {
        super(Persistors.project, Translators.project);
    }

    @Override
    protected IProject initializeObject(LockMode lockMode) throws Hd3dException
    {
        final UniqueResultQuery<ILProject, IProject> cmd = new GetPersonProjectCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        IProject obj = (IProject) cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }
}
