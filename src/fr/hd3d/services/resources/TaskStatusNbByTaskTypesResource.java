package fr.hd3d.services.resources;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.MultiMap;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.ServerResourceUtils;


public class TaskStatusNbByTaskTypesResource extends AbstractHd3dBaseResource
{

    public TaskStatusNbByTaskTypesResource() throws Hd3dException
    {
        super();
        allowMethods(null);
        getResourceContext().complete();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Representation get(Variant variant)
    {
        Session session = HibernateUtil.currentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        try
        {
            /* info from url */
            final String projectIdString = (String) getResourceContext().getQueryMap().get(Const.PROJECTID);
            final String entityName = (String) getResourceContext().getQueryMap().get(Const.ENTITYNAME);
            final String taskTypeIdsString = (String) getResourceContext().getQueryMap().get(Const.TASKTYPE_IDS);

            /* convert in proper types */
            final Long projectId = NumberUtils.toLong(projectIdString);
            List<String> taskTypeIdsStringList = Arrays.asList(org.apache.commons.lang.StringUtils.split(
                    taskTypeIdsString, ','));
            final List<Long> taskTypeIds = CollectUtils.toLong(taskTypeIdsStringList);

            /* query */
            final Criteria criteria = ((HibernatePersist) Persistors.task).basicCriteria(session, TaskH.class);
            criteria.createAlias(fr.hd3d.utils.Const.PROJECT, "project");
            criteria.add(Restrictions.eq("project.id", projectId));
            criteria.createAlias(fr.hd3d.utils.Const.TASKTYPE, "tasktype");
            criteria.add(Restrictions.in("tasktype.id", taskTypeIds));
            criteria.createAlias("boundEntityTaskLinks", "links");
            // criteria.createAlias(fr.hd3d.utils.Const.BOUNDENTITYTASKLINK, "boundEntityTaskLink");
            criteria.add(Restrictions.eq("links.boundEntityName", entityName));
            criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));
            ProjectionList projList = Projections.projectionList();
            projList.add(Projections.groupProperty("taskType"));
            projList.add(Projections.groupProperty("status"));
            projList.add(Projections.count("status"));
            projList.add(Projections.rowCount());
            criteria.setProjection(projList);

            List<Object[]> result = criteria.list();

            /* properly format response */
            MultiMap<String, Map<String, String>> resp = new MultiHashMap<String, Map<String, String>>(result.size());
            for (Object[] object : result)
            {
                Map<String, String> status_Nb_Map = new HashMap<String, String>(2);
                status_Nb_Map.put(object[1].toString(), object[2].toString());
                resp.put(object[0].toString(), status_Nb_Map);
            }

            Representation r = RessourceSerializer.getRepresentation(variant, resp.map());
            r.setCharacterSet(CharacterSet.UTF_8);
            return ServerResourceUtils.encodeRepresentation(r);
        }
        finally
        {
            if (tx != null)
                tx = null;
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
