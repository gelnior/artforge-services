package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ASSETREVISIONLINK_ID_ATTRIBUTE;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.shiro.util.StringUtils;
import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevisionLink;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.CollectUtils;


/**
 * @author Try LAM
 */
public class AssetRevisionLinkResource extends AbstractHd3dResource<ILAssetRevisionLink, IAssetRevisionLink>
{

    public AssetRevisionLinkResource() throws Hd3dException
    {
        super(Persistors.assetrevisionlink, Translators.assetrevisionlink);
    }

    @Override
    protected IAssetRevisionLink initializeObject(LockMode lockMode) throws Hd3dException
    {
        String lastSegment = getRequest().getOriginalRef().getLastSegment();

        /* if no "-" in parameters: standard case */
        if (!lastSegment.contains("-"))
            return initializeObject(ASSETREVISIONLINK_ID_ATTRIBUTE, lockMode);

        /* if "-" in parameters: <AssetRevisionLinkInputId>-<AssetRevisionLinkOutputId> */
        String[] inputOutputIds = StringUtils.split((String) getRequest().getAttributes().get(
                ASSETREVISIONLINK_ID_ATTRIBUTE), '-');

        if (inputOutputIds.length != 2)
            return null;

        Long inputId = NumberUtils.toLong(inputOutputIds[0]);
        Long outputId = NumberUtils.toLong(inputOutputIds[1]);

        /* find out the AssetRevisions which matching ids */
        IAssetRevision input = Persistors.assetrevision.getById(inputId);
        IAssetRevision output = Persistors.assetrevision.getById(outputId);

        String[] properties = new String[] { "inputAsset.id", "outputAsset.id" };

        Object[] values = new Object[] { input.getId(), output.getId() };

        /* find out the AssetRevisionLink matching the previous AssetRevisions' field values */
        List<IAssetRevisionLink> links = Persistors.assetrevisionlink.getByValues(null, properties, values, "AND");

        if (CollectUtils.isEmpty(links))
            return null;

        IAssetRevisionLink assetRevisionLink = links.get(0);// there must be only one
        createDynMetaDataValue(assetRevisionLink);
        return assetRevisionLink;
    }
}
