package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.TAGCATEGORY_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.Collection;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTag;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
@Hd3dComment("GET:Liste des tags appartenant à la catégorie de tag identifiée par id\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:\r\n" + "\r\nDELETE:N/A")
public class TagCategoryTagsResource extends AbstractHd3dCollection<ILTag, ITag>
{
    public TagCategoryTagsResource() throws Hd3dException
    {
        super(Persistors.tag, Translators.tag);
        allowMethods(null);
    }

    protected ResultCollection<ITag> getCollectionForRepresentation(final CompositeQueryModifier filter)
            throws Hd3dException
    {
        final Long tagCategoryId = getResourceContext().getIdFromUrl(TAGCATEGORY_ID_ATTRIBUTE);
        ITagCategory tagcategory = Persistors.tagcategory.getById(tagCategoryId);
        Collection<ITag> tags = tagcategory.getBoundTags();
        ResultCollection<ITag> resultCollection = new ResultCollection<ITag>(new ArrayList<ITag>(tags), tags.size());
        createDynMetaDataValues(resultCollection.getResult());
        return resultCollection;
    }
}
