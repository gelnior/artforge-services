package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILScreenModel;
import fr.hd3d.model.persistence.IScreenModel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer pool objects.<br>
 * URI : /pools/
 * 
 * @author HD3D
 */
public class ScreenModelsResource extends AbstractHd3dCollection<ILScreenModel, IScreenModel>
{
    public ScreenModelsResource() throws Hd3dException
    {
        super(Persistors.screenModel, Translators.screenModel);
    }
}
