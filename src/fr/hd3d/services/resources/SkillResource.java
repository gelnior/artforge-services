package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.SKILL_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSkill;
import fr.hd3d.model.persistence.ISkill;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computers/{id}
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'un skill de machines\r\n" + "\r\nPUT:Modification d'un skill de machines\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:Désactivation d'un skills de machines")
public class SkillResource extends AbstractHd3dResource<ILSkill, ISkill>
{
    @Override
    protected ISkill initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(SKILL_ID_ATTRIBUTE, lockMode);
    }

    public SkillResource() throws Hd3dException
    {
        super(Persistors.skill, Translators.skill);
    }
}
