/**
 * 
 */
package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.SEQUENCE_ID_ATTRIBUTE;
import static fr.hd3d.utils.Const.CANONICALNAME_SEQUENCE;
import static fr.hd3d.utils.Const.CANONICALNAME_SHOT;
import static fr.hd3d.utils.Const.JSON_CHILDS;
import static fr.hd3d.utils.Const.JSON_ENDBRACECURL;
import static fr.hd3d.utils.Const.JSON_ENDBRACECURL_1;
import static fr.hd3d.utils.Const.JSON_ENDBRACECURL_2;
import static fr.hd3d.utils.Const.JSON_ENDDATE;
import static fr.hd3d.utils.Const.JSON_SEQUENCEID;
import static fr.hd3d.utils.Const.JSON_SEQUENCEID_1;
import static fr.hd3d.utils.Const.JSON_SEQUENCENAME;
import static fr.hd3d.utils.Const.JSON_SHOTID;
import static fr.hd3d.utils.Const.JSON_SHOTLABEL;
import static fr.hd3d.utils.Const.JSON_SHOTS;
import static fr.hd3d.utils.Const.JSON_STARTDATE;
import static fr.hd3d.utils.Const.JSON_STARTDATE_1;
import static fr.hd3d.utils.Const.JSON_WORKOBJECTPLANNING;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.JDBCException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.PlanningUtils;
import fr.hd3d.utils.ServerResourceUtils;


/**
 * @author thomas-eskenazi
 * 
 */
public class PlanningWorkObjectsTemporalRangeResource<L, P> extends AbstractHd3dBaseResource
{
    static final CharSequence FROM = "from ";
    static final CharSequence CHUNK_1 = " where parent is null and project.id = ";
    static final CharSequence CHUNK_2 = " where parent.id =";
    static final CharSequence CHUNK_3 = " order by name";

    static final String SHOTQUERY = FROM + (String) CANONICALNAME_SHOT + " where sequence.id = {0} order by label";
    static final String SEQUENCESQUERY = FROM + (String) CANONICALNAME_SEQUENCE;

    /**
     * @throws Hd3dException
     * 
     */
    public PlanningWorkObjectsTemporalRangeResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Representation get(Variant variant) throws ResourceException
    {
        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();

        List<ISequence> plannedSequences = null;
        List<Long> sequenceIDs = null;
        List<IShot> plannedShots = null;
        try
        {
            Long projectID = getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE);
            Long sequenceID = getResourceContext().getIdFromUrl(SEQUENCE_ID_ATTRIBUTE);
            StringBuilder queryStr = new StringBuilder(120);
            queryStr.append(SEQUENCESQUERY);

            if (sequenceID == 0)
            {
                queryStr.append(CHUNK_1).append(projectID);
            }
            else
            {
                queryStr.append(CHUNK_2).append(sequenceID).append(CHUNK_3);
            }
            Query querySequences = session.createQuery(queryStr.toString());
            plannedSequences = querySequences.list();
            // ObjectProviderCustoms.filter(plannedCategories);
            sequenceIDs = new ArrayList<Long>();
            sequenceIDs.add(sequenceID);
            ISequence rootSequence = Persistors.sequence.getById(sequenceID);
            StringBuilder childs = new StringBuilder(300);
            for (ISequence sequence : plannedSequences)
            {
                Date[] taskLimits = PlanningUtils.getStartandEndDateForSequence(sequence);
                String startDate = "";
                String endDate = "";
                if (taskLimits[0] != null && taskLimits[1] != null)
                {
                    startDate = taskLimits[0].toString();
                    endDate = taskLimits[1].toString();
                }
                // childs += "{\"sequenceID\":" + sequence.getId() + ",\"sequenceName\":\"" + sequence.getName()
                // + "\",\"startDate\":\"" + startDate + "\",\"endDate\":\"" + endDate + "\"},";
                childs.append(JSON_SEQUENCEID).append(sequence.getId()).append(JSON_SEQUENCENAME).append(
                        sequence.getName()).append(JSON_STARTDATE).append(startDate).append(JSON_ENDDATE).append(
                        endDate).append(JSON_ENDBRACECURL);

                sequenceIDs.add(sequence.getId());
            }
            StringUtils.chomp(childs.toString(), ",");

            String queryShotStr = MessageFormat.format(SHOTQUERY, String.valueOf(sequenceID));
            Query queryShots = session.createQuery(queryShotStr);

            plannedShots = queryShots.list();
            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            tx.commit();
            // ObjectProviderCustoms.filter(plannedConstituents);
            StringBuilder constituents = new StringBuilder(300);
            for (IShot shot : plannedShots)
            {
                Date[] taskLimits = PlanningUtils.getStartandEndDateForShot(shot);
                String startDate = "";
                String endDate = "";
                if (taskLimits[0] != null && taskLimits[1] != null)
                {
                    startDate = taskLimits[0].toString();
                    endDate = taskLimits[1].toString();
                }
                constituents.append(JSON_SHOTID).append(shot.getId()).append(JSON_SHOTLABEL).append(shot.getLabel())
                        .append(JSON_SEQUENCEID_1).append(shot.getSequence().getId()).append(JSON_STARTDATE_1).append(
                                startDate).append(JSON_ENDDATE).append(endDate).append(JSON_ENDBRACECURL);

                // constituents.append("{\"shotID\":").append(shot.getId()).append(",\"shotLabel\":\"").append(
                // shot.getLabel()).append("\",\"sequenceID\":").append(shot.getSequence().getId()).append(
                // ",\"startDate\":\"").append(startDate).append("\",\"endDate\":\"").append(endDate).append(
                // "\"},");

            }
            StringUtils.chomp(constituents.toString(), ",");
            Date[] taskLimits = PlanningUtils.getStartandEndDateForSequence(rootSequence);
            String startDate = "";
            String endDate = "";
            if (taskLimits[0] != null && taskLimits[1] != null)
            {
                startDate = taskLimits[0].toString();
                endDate = taskLimits[1].toString();
            }
            // result = "{\"sequenceID\":" + rootSequence.getId() + ",\"sequenceName\":\"" + rootSequence.getName()
            // + "\",\"startDate\":\"" + startDate + "\",\"endDate\":\"" + endDate + "\",\"childs\":[" + childs
            // + "],\"shots\":[" + constituents + "]}";

            StringBuilder result = new StringBuilder(300);
            result.append(JSON_WORKOBJECTPLANNING).append(JSON_SEQUENCEID).append(rootSequence.getId()).append(
                    JSON_SEQUENCENAME).append(rootSequence.getName()).append(JSON_STARTDATE).append(startDate).append(
                    JSON_ENDDATE).append(endDate).append(JSON_CHILDS).append(childs).append(JSON_SHOTS).append(
                    constituents).append(JSON_ENDBRACECURL_1).append(JSON_ENDBRACECURL_2);

            Representation r = new StringRepresentation(result.toString());
            r.setCharacterSet(CharacterSet.UTF_8);
            getResponse().setStatus(Status.SUCCESS_OK);
            Log.LOGGER.info(responseInfo());
            getResponse().setEntity(ServerResourceUtils.encodeRepresentation(r));
            return getResponseEntity();
        }
        catch (JDBCException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, "Unable to connect to database");
            return getResponseEntity();
        }
        catch (HibernateException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
            return getResponseEntity();
        }
        catch (Exception e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
            return getResponseEntity();
        }

        finally
        {
            if (plannedSequences != null)
            {
                plannedSequences.clear();
                plannedSequences = null;
            }
            if (sequenceIDs != null)
            {
                sequenceIDs.clear();
                sequenceIDs = null;
            }
            if (plannedShots != null)
            {
                plannedShots.clear();
                plannedShots = null;
            }

            if (tx != null)
                tx = null;
            cleanup();
        }
    }

    protected String listToArrayStr(List<?> list)
    {
        if (list == null || list.size() == 0)
        {
            return "()";
        }
        return '(' + StringUtils.join(list, ',') + ')';
        // String result = "(";
        // for (Object object : list)
        // {
        // result += object.toString() + ",";
        // }
        // result = result.substring(0, result.length() - 1) + ")";
        // return result;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
