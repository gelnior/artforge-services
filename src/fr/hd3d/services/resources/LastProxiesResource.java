package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILProxy;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetLastProxiesCmd;
import fr.hd3d.model.translator.Translators;


public class LastProxiesResource extends AbstractHd3dCollection<ILProxy, IProxy>
{
    public LastProxiesResource() throws Hd3dException
    {
        super(Persistors.proxy, Translators.proxy);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetLastProxiesCmd(getResourceContext()));
    }
}
