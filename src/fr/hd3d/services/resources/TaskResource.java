package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.TASK_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computers/{id}
 * 
 * @author HD3D
 */
public class TaskResource extends AbstractHd3dResource<ILTask, ITask>
{
    @Override
    protected ITask initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(TASK_ID_ATTRIBUTE, lockMode);
    }

    public TaskResource() throws Hd3dException
    {
        super(Persistors.task, Translators.task);
    }
}
