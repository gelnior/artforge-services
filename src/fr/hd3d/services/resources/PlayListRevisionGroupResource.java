package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


@Hd3dComment("")
public class PlayListRevisionGroupResource extends
        AbstractHd3dResource<ILPlayListRevisionGroup, IPlayListRevisionGroup>
{
    public PlayListRevisionGroupResource() throws Hd3dException
    {
        super(Persistors.playlistrevisiongroup, Translators.playlistrevisiongroup);
    }

    @Override
    protected IPlayListRevisionGroup initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ServicesURI.PLAYLISTREVISIONGROUP_ID_ATTRIBUTE, lockMode);
    }
}
