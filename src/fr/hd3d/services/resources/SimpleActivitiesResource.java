package fr.hd3d.services.resources;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * A resource to access to specific person activities.
 * 
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET: Retrieve simple activities<br />\r\n" + "POST: Create an simple activity.<br />\r\n")
public class SimpleActivitiesResource extends AbstractHd3dCollection<ILSimpleActivity, ISimpleActivity>
{
    public SimpleActivitiesResource() throws Hd3dException
    {
        super(Persistors.simpleActivity, Translators.simpleActivity);
    }

    // TODO: Optimize for bulk requests
    /**
     * Checks that no activity has been set for current day.
     */
    @Override
    protected void doPrePersist(Representation entity, ISimpleActivity newObject) throws Hd3dException
    {
        String[] fields = { "filledBy.id", "day.id", "type" };
        Object[] values = { newObject.getFilledBy().getId(), newObject.getDay().getId(), newObject.getType() };

        List<ISimpleActivity> activities = Persistors.simpleActivity.getByValues(fields, values, "AND");

        if (CollectionUtils.isEmpty(activities))
        {
            super.doPrePersist(entity, newObject);
        }
        else
        {
            throw new Hd3dException(
                    "An activity already exists for this task, this user and this day. Please modify existing one rather than creating a new one.");
        }
    }
}
