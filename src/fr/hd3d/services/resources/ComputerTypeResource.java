package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.COMPUTER_TYPE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILComputerType;
import fr.hd3d.model.persistence.IComputerType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computertypes/{id}
 * 
 * @author HD3D
 */
public class ComputerTypeResource extends AbstractHd3dResource<ILComputerType, IComputerType>
{
    @Override
    protected IComputerType initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(COMPUTER_TYPE_ID_ATTRIBUTE, lockMode);
    }

    public ComputerTypeResource() throws Hd3dException
    {
        super(Persistors.computerType, Translators.computerType);
    }
}
