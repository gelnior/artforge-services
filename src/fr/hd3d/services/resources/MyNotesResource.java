package fr.hd3d.services.resources;

import java.util.List;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetMyNotesCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;


/**
 * @author Try LAM
 */
public class MyNotesResource extends AbstractHd3dCollection<ILApprovalNote, IApprovalNote>
{

    public MyNotesResource() throws Hd3dException
    {
        super(Persistors.approvalnote, Translators.approvalnote);
    }

    @Override
    protected ResultCollection<IApprovalNote> getCollectionForRepresentation() throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user != null)
        {
            String ids = getResourceContext().getQueryMap().get(ServicesURI.TASK_IDS);
            List<Long> taskIds = null;
            if (ids != null && ids.length() > 0)
            {
                taskIds = (List<Long>) Hd3dJsonSerializer.getSerializer().deserialize(ids);
            }
            return doGetCollection(new GetMyNotesCmd(getResourceContext(), user.getId(), taskIds).wantTotalSize());
        }
        else
        {
            throw new Hd3dException("No user is set for this request, cannot returns tasks for current user.");
        }
    }
}
