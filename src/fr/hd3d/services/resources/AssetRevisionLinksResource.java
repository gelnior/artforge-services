package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevisionLink;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class AssetRevisionLinksResource extends AbstractHd3dCollection<ILAssetRevisionLink, IAssetRevisionLink>
{

    public AssetRevisionLinksResource() throws Hd3dException
    {
        super(Persistors.assetrevisionlink, Translators.assetrevisionlink);
    }
}
