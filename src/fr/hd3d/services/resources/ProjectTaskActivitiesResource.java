package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectTaskActivitiesCmd;


/**
 * A resource to access to project task activities. It allows task activity creation only if activity is linked to a
 * task of this project.
 * 
 * @author HD3D
 */
@Hd3dComment("GET: Retrieve task activities list for given project. <br />\r\n"
        + "POST: Create a task activity for given project.<br />\r\n")
public class ProjectTaskActivitiesResource extends BaseTaskActivitiesResource
{

    public ProjectTaskActivitiesResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected ResultCollection<ITaskActivity> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectTaskActivitiesCmd(getResourceContext()).wantTotalSize());
    }

    @Override
    protected void doPostPersist(ITaskActivity object) throws Hd3dException
    {
        if (object != null)
        {
            super.doPostPersist(object);

            this.updateTaskActualStartDate(object);
        }
    }

    @Override
    protected void doPrePersist(Representation entity, ITaskActivity newObject) throws Hd3dException
    {
        IProject project = Persistors.project.getById(getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE));

        if (project.getId().equals(newObject.getTask().getProject().getId()))
        {
            super.doPrePersist(entity, newObject);
        }
        else
        {
            throw new Hd3dException("You cannot create activity for another project.");
        }
    }

}
