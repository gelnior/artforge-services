package fr.hd3d.services.resources;

import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;


/**
 * Returns the version, build of the current Hd3dServices
 * 
 * @author Try LAM
 */
public class ServerTimeResource<L, P> extends AbstractHd3dBaseResource
{
    public ServerTimeResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    public Representation get(Variant variant)
    {
        try
        {
            Representation r = new StringRepresentation(String.valueOf(System.currentTimeMillis()));
            r.setCharacterSet(CharacterSet.UTF_8);
            return r;
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
