package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.ontology.annotation.AnnotationHandler;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.ServerResourceUtils;


public class AnnotationIndexationResource extends AbstractHd3dBaseResource
{

    public AnnotationIndexationResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Representation get(Variant variant)
    {
        if (!Conf.isAnnotationOn() || AuthenticationUtil.isCurrentNotNullNormalUser())
        {
            Representation r = new StringRepresentation("NOT ALLOWED...");
            r.setCharacterSet(CharacterSet.UTF_8);
            getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
            getResponse().setEntity(ServerResourceUtils.encodeRepresentation(r));
            return getResponseEntity();
        }

        /* read all FileRevisions and send annotations to annotation server */
        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();
        try
        {
            indexFileRevisions(session);

            Representation r = new StringRepresentation("ANNOTATION PROCESSED...");
            r.setCharacterSet(CharacterSet.UTF_8);
            getResponse().setStatus(Status.SUCCESS_OK);
            getResponse().setEntity(ServerResourceUtils.encodeRepresentation(r));

            return getResponseEntity();
        }
        catch (IOException e)
        {
            error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
            return getResponseEntity();
        }
        finally
        {
            if (tx != null)
                tx = null;
            cleanup();
        }
    }

    private void indexFileRevisions(Session session) throws IOException
    {
        /* number of fileRevisions */
        Criteria nbCriteria = HibernatePersist.basicCriteria(session, FileRevisionH.class);
        nbCriteria.setProjection(Projections.rowCount());
        Integer totalFileRevisions = ((Long) nbCriteria.uniqueResult()).intValue();

        for (int i = 0; i < totalFileRevisions; i += HibernateUtil.JDBC_BATCHSIZE)
        {
            Criteria criteria = HibernatePersist.basicCriteria(session, FileRevisionH.class);
            criteria.setMaxResults(HibernateUtil.JDBC_BATCHSIZE);
            criteria.setFirstResult(i);
            criteria.setReadOnly(true);
            List<IFileRevision> fileRevisions = criteria.list();
            AnnotationHandler handler = new AnnotationHandler((Set<Persistent>) new HashSet<Persistent>(fileRevisions));
            handler.buildAnnotations();
            handler.sendAnnotations();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
