package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ACL_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILHd3dACL;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.dao.IHd3dACL;


/**
 * @author Try LAM
 */
public class AclResource extends AbstractHd3dResource<ILHd3dACL, IHd3dACL>
{
    public AclResource() throws Hd3dException
    {
        super(Persistors.acl, Translators.acl);
    }

    @Override
    protected IHd3dACL initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ACL_ID_ATTRIBUTE, lockMode);
    }
}
