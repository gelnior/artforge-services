package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectPersonTasksCmd;


/**
 * A resource to access to specific person's tasks in a project.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des taches d'une personne sur un projet\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'une tache\r\n" + "\r\nDELETE:N/A")
public class ProjectPersonTasksResource extends TasksResource
{
    public ProjectPersonTasksResource() throws Hd3dException
    {
        super();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection<ITask> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectPersonTasksCmd(getResourceContext()).wantTotalSize());
    }
}
