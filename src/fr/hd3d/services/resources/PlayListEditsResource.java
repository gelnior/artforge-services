package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlayListEdit;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class PlayListEditsResource extends AbstractHd3dCollection<ILPlayListEdit, IPlayListEdit>
{

    public PlayListEditsResource() throws Hd3dException
    {
        super(Persistors.playlistedit, Translators.playlistedit);
    }

}
