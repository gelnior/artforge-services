/**
 * 
 */
package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILProjectType;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectTypesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author thomas-eskenazi
 * 
 */
@Hd3dComment("GET:Récupération des types de projet\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Création d'un type de projet\r\n" + "\r\nDELETE:N/A")
public class ProjectTypesResource extends AbstractHd3dCollection<ILProjectType, IProjectType>
{
    public ProjectTypesResource() throws Hd3dException
    {
        super(Persistors.projecttype, Translators.projecttype);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectTypesCmd(getResourceContext()).wantTotalSize());
    }
}
