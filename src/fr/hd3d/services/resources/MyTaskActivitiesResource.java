package fr.hd3d.services.resources;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonTaskActivitiesCmd;
import fr.hd3d.services.security.utils.AuthenticationUtil;


/**
 * A resource to access to specific person activities.
 * 
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET: Retrieve current prerson task activity list. <br />\r\n"
        + "POST: Create a task activity for current person.<br />\r\n")
public class MyTaskActivitiesResource extends BaseTaskActivitiesResource
{
    public MyTaskActivitiesResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected ResultCollection<ITaskActivity> getCollectionForRepresentation() throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user != null)
        {
            return doGetCollection(new GetPersonTaskActivitiesCmd<ILTaskActivity, ITaskActivity>(getResourceContext(),
                    user.getId()).wantTotalSize());
        }
        else
        {
            throw new Hd3dException("No user is set for this request, cannot return activities for current user.");
        }
    }

    @Override
    protected void doPostPersist(ITaskActivity object) throws Hd3dException
    {
        if (object != null)
        {
            super.doPostPersist(object);

            this.updateTaskActualStartDate(object);
            this.checkAssignation(object);
        }
    }

    private void checkAssignation(ITaskActivity object) throws Hd3dException
    {
        ITask task = object.getTask();
        if (task == null)
            return;
        if (task.getWorker() == null)
        {
            IPerson user = AuthenticationUtil.getCurrentUser();
            task.setWorker(user);
            Persistors.task.persist(task);
        }
    }

    @Override
    protected void doPrePersist(Representation entity, ITaskActivity newObject) throws Hd3dException
    {
        if (newObject == null)
        {
            return;
        }

        IPerson user = AuthenticationUtil.getCurrentUser();
        if (newObject.getFilledBy() != null && user.getId().equals(newObject.getFilledBy().getId()))
        {
            super.doPrePersist(entity, newObject);
        }
        else
        {
            throw new Hd3dException("You cannot create activity you don't own.");
        }
    }

}
