package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.TAG_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTag;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
@Hd3dComment("GET:Récupéraration d'un Tag\r\n" + "\r\nPUT:Modifiation d'un tag\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:N/A")
public class TagResource extends AbstractHd3dResource<ILTag, ITag>
{
    public TagResource() throws Hd3dException
    {
        super(Persistors.tag, Translators.tag);
    }

    @Override
    protected ITag initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(TAG_ID_ATTRIBUTE, lockMode);
    }

    // @Override
    // protected void doDelete() throws Hd3dException
    // {
    // ITag tag = (ITag) object;
    // for (TagParent tgp : tag.getTagParents())
    // {
    // if (tgp.getParent() == null)
    // continue;
    // ((IBase) Persistors.getPersistor(tgp.getParentType()).getById(tgp.getParent())).getTagParents().remove(tgp);
    // }
    // getResourceContext().getPersistor().disable(tag);
    // }
}
