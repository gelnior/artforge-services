package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonAssetsCmd;
import fr.hd3d.services.security.utils.AuthenticationUtil;


public class MyAssetRevisionsResource extends AssetRevisionsResource
{

    public MyAssetRevisionsResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected ResultCollection<IAssetRevision> getCollectionForRepresentation() throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user != null)
        {
            return doGetCollection(new GetPersonAssetsCmd(getResourceContext(), user.getId()).wantTotalSize());
        }
        else
        {
            throw new Hd3dException("No user is set for this request, cannot returns asset for current user.");
        }
    }

}
