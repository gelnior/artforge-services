package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetSequenceAllSequencesCmd;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;


/**
 * 
 * Given a project id, returns all the sequences of the project
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class SequenceAllSequencesResource extends AbstractHd3dCollection<ILSequence, ISequence>
{

    public SequenceAllSequencesResource(IPersist<ISequence> persistor, IBaseTranslator<ILSequence, ISequence> translator)
            throws Hd3dException
    {
        super(persistor, translator);
    }

    public SequenceAllSequencesResource() throws Hd3dException
    {
        super(Persistors.sequence, Translators.sequence);
        allowMethods(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetSequenceAllSequencesCmd(getResourceContext()).wantTotalSize());
    }
}
