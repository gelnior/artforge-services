package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonTaskActivitiesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * A resource to access to specific person activities.
 * 
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET: Retrieve person task activity list. <br />\r\n"
        + "POST: Create a task activity for person.<br />\r\n")
public class PersonTaskActivitiesResource extends AbstractHd3dCollection<ILTaskActivity, ITaskActivity>
{
    public PersonTaskActivitiesResource() throws Hd3dException
    {
        super(Persistors.taskActivity, Translators.taskActivity);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetPersonTaskActivitiesCmd(getResourceContext()).wantTotalSize());
    }
}
