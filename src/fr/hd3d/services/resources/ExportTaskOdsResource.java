/**
 * 
 */
package fr.hd3d.services.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.sojo.interchange.json.JsonParser;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.OutputRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ProjectH;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


public class ExportTaskOdsResource extends ServerResource
{

    private static final String INDENTATION_PATTERN = "   ";
    private static final String WEEK_END_DAY_STYLE = "weekEndDay";
    private static final String WEEK_END_DAY_NAME_STYLE = "weekEndDayName";
    private static final String WEEK_DAY_STYLE = "weekDay";
    private static final String WEEK_DAY_NAME_STYLE = "weekDayName";
    private static final String OVERTIME_STYLE = "overtime";
    private static final String BOLDWEIGHT_BOLD = "bold";
    private static final String BOLDWEIGHT_BOLD_CENTER = "bold_center";
    private static final String TEXT_FIELD = "text_field";
    private static final String MONTH_STYLE = "month";
    private static final String END_MONTH_STYLE = "end_month";
    private String[] days = new String[] { "S", "M", "T", "W", "T", "F", "S" };
    private int currentMonthNumber;
    private Map<String, CellStyle> styleMap;
    private HSSFWorkbook wb;

    private int index = 1;

    private Date startDate = null;
    private Date endDate = null;
    private Long projectId = -1L;
    private Long personId = -1L;
    private String titleTab = "";

    public ExportTaskOdsResource()
    {
        init(Context.getCurrent(), Request.getCurrent(), Response.getCurrent());
        Log.LOGGER.info(info(Request.getCurrent()));
    }

    private DateFormat getDateFormatter()
    {
        return new SimpleDateFormat("yyyy-MM-dd");
    }

    @SuppressWarnings("unchecked")
    @Override
    @Post
    protected Representation post(Representation entity) throws ResourceException
    {

        String json = getRequest().getEntityAsForm().getValues("json");
        // read json file
        String fileName = "File-export";
        OutputRepresentation out = null;
        JsonParser jsonParser = new JsonParser();

        Map<Object, Object> object = (Map<Object, Object>) jsonParser.parse(json);
        if (object.get("TaskRawData") != null)
        {
            String activityName = (String) object.get("TaskRawData");
            if (activityName != null)
            {

                String str[] = activityName.split("_");
                try
                {
                    startDate = getDateFormatter().parse(str[0]);
                }
                catch (java.text.ParseException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            // Write the output to a file
            fileName = "Tasks-" + activityName;
            titleTab = "Tasks-" + activityName;

            IProject project = null;
            if (object.get("project-id") != null)
            {
                projectId = (Long) object.get("project-id");
                Criteria criteria = HibernateUtil.currentSession().createCriteria(ProjectH.class);
                criteria.add(Restrictions.eq("id", projectId));
                project = (IProject) criteria.uniqueResult();
                if (project != null)
                {
                    fileName = "Tasks-" + activityName + "-" + project.getName();
                    titleTab = "Tasks - " + project.getName() + " : Raw Data - " + activityName;
                    titleTab = fileName;
                }
            }

            fileName = fileName + ".ods";
        }

        out = new OutputRepresentation(MediaType.APPLICATION_OPENOFFICE_ODS) {

            @Override
            public void write(OutputStream stream) throws IOException
            {
                String json = getRequest().getEntityAsForm().getValues("json");
                JsonParser jsonParser = new JsonParser();
                Map<Object, Object> object = (Map<Object, Object>) jsonParser.parse(json);

                String type = (String) object.get("type");
                if (type.compareTo("Detail") == 0)
                {

                    String activityName = (String) object.get("TaskRawData");
                    if (activityName != null)
                    {

                        wb = new HSSFWorkbook();
                        Sheet sheet = wb.createSheet(titleTab);

                        for (int i = 0; i < 9; i++)
                        {
                            sheet.setColumnWidth(i, 256 * 20);
                        }

                        styleMap = createStyles(wb);

                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(startDate);

                        // title row
                        // Row titleRow = sheet.createRow(0);
                        // titleRow.setHeightInPoints(45);
                        // Cell titleCell = titleRow.createCell(0);
                        // titleCell.setCellValue(titleTab);
                        // titleCell.setCellStyle(styleMap.get("title"));

                        // sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$I$1"));

                        // Create a row and put some cells in it. Rows are 0 based.
                        Row row = sheet.createRow((short) 0);
                        // Create a cell and put a value in it.

                        // Or do it on one line.

                        row.createCell(0).setCellValue("Work Object Scope");
                        row.createCell(1).setCellValue("Work Object");
                        row.createCell(2).setCellValue("Task Type");
                        row.createCell(3).setCellValue("Status");
                        row.createCell(4).setCellValue("Worker");
                        row.createCell(5).setCellValue("Date");
                        row.createCell(6).setCellValue("Duration (Hours)");
                        row.createCell(7).setCellValue("Activity Duration (Hours)");
                        row.createCell(8).setCellValue("Work Object TYpe");

                        for (int i = 0; i < 9; i++)
                        {
                            row.getCell(i).setCellStyle(styleMap.get(TEXT_FIELD));
                        }

                        addObjectTabs(sheet, row, 1);
                    }
                }

                wb.write(stream);
            }
        };
        out.setDownloadable(true);
        out.setDownloadName(fileName);

        return out;
    }

    private String getIndentation(int level)
    {
        return StringUtils.repeat(INDENTATION_PATTERN, level);

    }

    @SuppressWarnings("unchecked")
    private void addObjectTabs(Sheet sheet, Row row, int index)
    {
        try
        {
            String workObjectType = "";
            List<IApprovalNote> approvalNotes = null;
            Long woId = 0L;

            // get task
            Criteria criteria = HibernateUtil.currentSession().createCriteria(TaskH.class);
            criteria.add(Restrictions.eq("project.id", projectId));
            criteria.add(Restrictions.eq("internalStatus", NumberUtils.INTEGER_ZERO));

            List<ITask> listTask = criteria.list();

            for (Iterator<ITask> iterator = listTask.iterator(); iterator.hasNext();)

            {
                ITask task = (ITask) iterator.next();
                workObjectType = "";
                StringBuilder workObjectScope = new StringBuilder();

                if (task.getBoundEntityTaskLink().getBoundEntityName().compareTo("Constituent") == 0)
                {
                    IConstituent constituent = Persistors.constituent.getById(task.getBoundEntityTaskLink()
                            .getBoundEntity());
                    if (constituent == null)
                        continue;
                    row = sheet.createRow((short) index);
                    row.createCell(1).setCellValue(constituent.getLabel());

                    Object[] setCat = constituent.getCategory().parents().toArray();
                    if (setCat.length > 1)
                    {
                        for (int k = setCat.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ICategory) setCat[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(constituent.getCategory().getName());
                    workObjectType = constituent.getCategory().getName();
                    woId = constituent.getId();
                }
                else if (task.getBoundEntityTaskLink().getBoundEntityName().compareTo("Shot") == 0)
                {
                    IShot shot = Persistors.shot.getById(task.getBoundEntityTaskLink().getBoundEntity());

                    if (shot == null)
                        continue;
                    row = sheet.createRow((short) index);
                    row.createCell(1).setCellValue(shot.getLabel());

                    Object[] setSeq = shot.getSequence().parents().toArray();
                    if (setSeq.length > 1)
                    {
                        for (int k = setSeq.length - 2; k >= 0; k--)
                        {
                            workObjectScope.append(((ISequence) setSeq[k]).getName()).append(" > ");
                        }
                    }
                    workObjectScope.append(shot.getSequence().getName());
                    woId = shot.getId();
                }

                row.createCell(0).setCellValue(workObjectScope.toString());
                row.getCell(1).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD));
                row.createCell(2).setCellValue(task.getTaskType().getName());
                row.getCell(2).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD));
                row.createCell(3).setCellValue(task.getStatus().toString());
                row.getCell(3).setCellStyle(styleMap.get(BOLDWEIGHT_BOLD_CENTER));

                String worker = "Unassigned";
                if (task.getWorker() != null)
                {
                    worker = task.getWorker().getFirstName() + " " + task.getWorker().getLastName() + " - "
                            + task.getWorker().getLogin();
                }
                row.createCell(4).setCellValue(worker);
                Date date = null;

                List<IApprovalNote> result = new ArrayList<IApprovalNote>();

                String queryApp = "from " + Const.CANONICALNAME_APPROVALNOTE + " note where "
                        + "note.boundEntityName = '" + task.getBoundEntityName() + "' and note.boundEntity = " + woId
                        + " and note.approvalNoteType is not null " + " and note.approvalNoteType.taskType.id = "
                        + task.getTaskType().getId()
                        + " and note.internalStatus=0 order by note.boundEntity, note.approvalNoteType.taskType.id";

                result.addAll(HibernateUtil.currentSession().createQuery(queryApp).list());

                for (IApprovalNote approvalNote : result)
                {
                    if (approvalNote.getApprovalNoteType().getTaskType() == task.getTaskType())
                    {
                        if (date == null || approvalNote.getApprovalDate().after(date))
                        {
                            date = approvalNote.getApprovalDate();
                        }
                    }
                }

                if (date != null)
                {
                    row.createCell(5).setCellValue(getDateFormatter().format(date));
                }
                row.createCell(6).setCellValue((float) (task.getDuration()) / 3600);
                float totalDuration = 0L;

                String query = "select sum(ta.duration) from " + Const.CANONICALNAME_TASKACTIVITY
                        + " ta where ta.task.id =  " + task.getId() + " and ta.internalStatus=0 group by ta.task.id";

                Object obj = HibernateUtil.currentSession().createQuery(query).uniqueResult();
                if (obj != null)
                {

                    totalDuration = (float) Float.parseFloat(obj.toString()) / 3600;
                }

                row.createCell(7).setCellValue(totalDuration);
                row.createCell(8).setCellValue(task.getBoundEntityTaskLink().getBoundEntityName());

                index += 1;
            }

        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
        }
    }

    public static int hex2decimal(String s)
    {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16 * val + d;
        }
        return val;
    }

    /**
     * create a library of cell styles
     */
    private Map<String, CellStyle> createStyles(Workbook wb)
    {
        Map<String, CellStyle> styles = new HashMap<String, CellStyle>();

        CellStyle style;

        Font titleFont = wb.createFont();
        titleFont.setFontHeightInPoints((short) 14);
        titleFont.setFontName("Trebuchet MS");
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setFont(titleFont);
        styles.put("title", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put(WEEK_DAY_NAME_STYLE, style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styles.put(WEEK_DAY_STYLE, style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put(WEEK_END_DAY_NAME_STYLE, style);

        style = createBorderedStyle(wb);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put(WEEK_END_DAY_STYLE, style);

        style = createAllBorderedStyle(wb);
        style.setWrapText(true);
        style.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put(OVERTIME_STYLE, style);

        style = wb.createCellStyle();
        style = createAllBorderedStyle(wb);
        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);
        styles.put(TEXT_FIELD, style);

        style = wb.createCellStyle();
        font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);
        styles.put(BOLDWEIGHT_BOLD, style);

        style = wb.createCellStyle();
        style = createAllBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setIndention((short) 1);
        style.setWrapText(true);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);
        styles.put(BOLDWEIGHT_BOLD_CENTER, style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);
        styles.put(MONTH_STYLE, style);

        style = wb.createCellStyle();
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);
        styles.put(END_MONTH_STYLE, style);

        return styles;
    }

    private static CellStyle createBorderedStyle(Workbook wb)
    {
        CellStyle style = wb.createCellStyle();
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }

    private static CellStyle createAllBorderedStyle(Workbook wb)
    {
        CellStyle style = wb.createCellStyle();
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }

    protected String info(Request request)
    {
        StringBuilder info = new StringBuilder(300);
        info.append(request.getClientInfo().getAddress()).append(' ').append(request.getClientInfo().getAgent())
                .append(' ').append(request.getMethod()).append(' ').append(request.getOriginalRef());
        return info.toString();
    }

    public Long getIdFromUrl(String idString)
    {
        return NumberUtils.toLong((String) getRequest().getAttributes().get(idString));
    }

    protected void error(Status status, Exception e)
    {
        String error = ExceptUtils.format(e);
        Log.LOGGER.info(info(getResponse()));
        Log.LOGGER.error(error);
        getResponse().setStatus(status);
        getResponse().setEntity(new StringRepresentation(Hd3dJsonSerializer.serialize(error)));
    }

    protected String info(Response response)
    {
        StringBuilder info = new StringBuilder(300);
        info.append(info(response.getRequest())).append(' ').append(response.getStatus());
        return info.toString();
    }

}
