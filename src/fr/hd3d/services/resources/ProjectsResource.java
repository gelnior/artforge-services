package fr.hd3d.services.resources;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.Log;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des projets\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'un nouveau projet\r\n" + "\r\nDELETE:N/A")
public class ProjectsResource extends AbstractHd3dCollection<ILProject, IProject>
{

    public ProjectsResource() throws Hd3dException
    {
        super(Persistors.project, Translators.project);
    }

    // FOR TEST PURPOSE: DO NOT REMOVE
    // TODO remove test
    private void test(Session session)
    {
        String[] attributes = new String[] { "category", "description" };
        String className = "ConstituentH";

        Query q = session.createQuery("select " + appendHead(attributes) + " from " + className
                + " as C where C.description like '%1%' ");
        // display class attributes
        try
        {
            printProps(Class.forName("fr.hd3d.model.persistence.impl.hibernate." + className));
        }
        catch (ClassNotFoundException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        // result

        // Build dynamically a new class with returned types as properties
        List<DynaProperty> props = new ArrayList<DynaProperty>(attributes.length);

        org.hibernate.type.Type[] beanTypes = q.getReturnTypes();
        for (int i = 0; i < attributes.length; i++)
        {
            props.add(new DynaProperty(attributes[i], beanTypes[i].getReturnedClass()));
        }

        DynaProperty[] propsArray = new DynaProperty[attributes.length];
        BasicDynaClass dynaClass = new BasicDynaClass("Result", null, props.toArray(propsArray));
        // test: display the dynamically created class' properties
        DynaProperty[] dynaProps = dynaClass.getDynaProperties();
        for (DynaProperty dp : dynaProps)
        {
            Log.LOGGER.debug(dp.getName());
        }

        // update test
        q = session
                .createQuery("update versioned ConstituentH  set description = 'bobo', difficulty=3 where id = 3662");
        q.executeUpdate();
        q = session
                .createQuery("update versioned ConstituentH  set description = 'bubu', difficulty=4, hook='took' where id = 3662");
        q.executeUpdate();

    }

    private String appendHead(String[] attributes)
    {
        StringBuffer buf = new StringBuffer();
        for (String attr : attributes)
        {
            buf.append(" C.").append(attr).append(',');
        }
        return StringUtils.chop(buf.toString());
    }

    private void printProps(Class<?> c)
    {
        PropertyDescriptor[] propsDesc = PropertyUtils.getPropertyDescriptors(c);
        for (PropertyDescriptor p : propsDesc)
        {
            System.out.println(p.getName() + "/" + p.getPropertyType());
        }
    }
}
