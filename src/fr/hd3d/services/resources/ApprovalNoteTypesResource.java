package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import org.restlet.representation.Representation;

import fr.hd3d.common.client.Persist;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNoteType;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetApprovalNoteType;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.translator.json.JSonFormSerializer;


/**
 * @author Try LAM
 */
public class ApprovalNoteTypesResource extends AbstractHd3dCollection<ILApprovalNoteType, IApprovalNoteType>
{

    public ApprovalNoteTypesResource() throws Hd3dException
    {
        super(Persistors.approvalnotetype, Translators.approvalnotetype);
    }

    @Override
    protected ResultCollection<IApprovalNoteType> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetApprovalNoteType(getResourceContext()).wantTotalSize());
    }

    @Override
    protected IApprovalNoteType newObject(Representation entity) throws Hd3dException
    {
        final ILApprovalNoteType lObject = JSonFormSerializer.<ILApprovalNoteType> deserialize(entity);
        final IApprovalNoteType newObject = getResourceContext().getTranslator().fromLightweight(lObject,
                getResourceContext());

        final Long id = getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        if (BaseH.isValidId(id))
            newObject.setProject(Persistors.project.getById(id));

        newObject.doPrePersist();
        executeScripts(newObject, Persist.PREPERSIST, getNewIdentifier(newObject));
        return newObject;
    }

}
