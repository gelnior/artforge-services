package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetGroupPersonFullListCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Retrieve person list of people inside a given group. People in subgroups are also displayed. \r\n"
        + "\r\nPUT:N/A\r\n" + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class ResouceGroupPersonsFullResource extends AbstractHd3dCollection<ILPerson, IPerson>
{
    public ResouceGroupPersonsFullResource() throws Hd3dException
    {
        super(Persistors.person, Translators.person);
        allowMethods("GET");
    }

    @Override
    protected ResultCollection<IPerson> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetGroupPersonFullListCmd(getResourceContext()).wantTotalSize());
    }
}
