package fr.hd3d.services.resources;

import org.hibernate.LockMode;
import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectPlanningCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.UniqueResultQuery;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


/**
 * @author michael.guiral
 * 
 */
public class ProjectPlanningResource extends AbstractHd3dResource<ILPlanning, IPlanning>
{
    public ProjectPlanningResource() throws Hd3dException
    {
        super(Persistors.planning, Translators.planning);
    }

    @Override
    protected void doPostUpdate(Representation entity) throws Hd3dException
    {
        if (object.getProject() != null)
        {
            // Criteria projectCriteria = HibernateUtil.currentSession().createCriteria(ProjectH.class);
            // projectCriteria.add(Restrictions.idEq(object.getProject().getId()));
            // Criteria criteria = HibernateUtil.currentSession().createCriteria(TaskGroupH.class);
            // criteria.add(Restrictions.eq("planning.id", object.getId()));
            // criteria.add(Restrictions.isNull("taskGroup"));
            // ProjectH projectH = (ProjectH) projectCriteria.uniqueResult();
            // TaskGroupH taskGroupH = (TaskGroupH) criteria.uniqueResult();
            // taskGroupH.setName(projectH.getName() + " - " + object.getName());
            // taskGroupH.setStartDate(object.getStartDate());
            // taskGroupH.setEndDate(object.getEndDate());
            // Persistors.taskGroup.persist(taskGroupH);
        }
        super.doPostUpdate(entity);
    }

    @Override
    protected IPlanning initializeObject(LockMode lockMode) throws Hd3dException
    {
        final UniqueResultQuery<ILPlanning, IPlanning> cmd = new GetProjectPlanningCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        IPlanning obj = (IPlanning) cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }
}
