/**
 * 
 */
package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PROJECT_TYPE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILProjectType;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author thomas-eskenazi
 * 
 */
@Hd3dComment("GET:Récupération d'un type de projet {projectTypeID}\r\n"
        + "\r\nPUT:Modification d'un type de projet\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Désactivation d'un type de projet")
public class ProjectTypeResource extends AbstractHd3dResource<ILProjectType, IProjectType>
{
    public ProjectTypeResource() throws Hd3dException
    {
        super(Persistors.projecttype, Translators.projecttype);
    }

    @Override
    protected IProjectType initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(PROJECT_TYPE_ID_ATTRIBUTE, lockMode);
    }
}
