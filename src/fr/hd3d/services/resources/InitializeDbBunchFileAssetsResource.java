package fr.hd3d.services.resources;

import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.enums.EAssetStatus;
import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.common.client.enums.EFileStatus;
import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.impl.LPerson;
import fr.hd3d.model.lightweight.impl.LProject;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionLinkH;
import fr.hd3d.model.persistence.impl.hibernate.FileAttributeH;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.MountPointH;
import fr.hd3d.model.persistence.impl.hibernate.TaskTypeH;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.auth.FDTAuthUtils;
import fr.hd3d.utils.Bench;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


/**
 * Test class to initialize DB with some test values.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("Crée un jeu de tests pour l'asset manager. <br />"
        + "ATTENTION : cette resource génère beaucoup de fichiers et a donc un temps "
        + "d'éxécution <u>très long</u>.")
public class InitializeDbBunchFileAssetsResource<L, P> extends AbstractHd3dBaseResource
{
    /**
     * Default Constructor
     * 
     * @param context
     *            Context parameters
     * @param request
     *            Request received
     * @param response
     *            Response to send
     * @throws Hd3dException
     */
    public InitializeDbBunchFileAssetsResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    /**
     * (non-Javadoc)
     * 
     * @see org.restlet.resource.Resource#getRepresentation(org.restlet.resource. Variant)
     */
    @Override
    public Representation get(Variant arg0)
    {
        final String cmdToRun = getQuery().getValues("cmd");

        try
        {
            init(cmdToRun);
        }
        catch (Hd3dTranslationException e)
        {
            e.printStackTrace();
            return new StringRepresentation(e.getMessage());
        }
        return new StringRepresentation("database initialized...");
    }

    public static void init(String cmd) throws Hd3dPersistenceException
    {
        if (StringUtils.isBlank(cmd) || "Assets".equalsIgnoreCase(cmd))
        {
            populateAssets();
        }
    }

    public static void populateAssets() throws Hd3dPersistenceException
    {
        run(new InitDbCmd());
        run(new InitAssets());

        final int filesToCreate = 100;
        final int step = 50;
        Bench b = new Bench();
        b.b();
        for (int i = 0; i < filesToCreate; i = i + step)
        {
            run(new InitFiles(i, step));
        }
        Log.LOGGER.debug("Files creation time={} ms, {} files created", b.e(), filesToCreate);
    }

    private static void run(IHibernateTransaction tx) throws Hd3dPersistenceException
    {
        Bench b = new Bench();
        b.b();
        Session session = HibernateUtil.currentSession();
        Transaction trx = session.beginTransaction();
        HibernateUtil.safeTransaction(tx);
        if (session.getFlushMode() == FlushMode.MANUAL)
        {
            session.flush();
        }
        trx.commit();
        trx = null;
        HibernateUtil.closeSession();

        Log.LOGGER.debug("{} took: {} ms", tx.getClass().getName(), b.e());
    }

    private static class InitDbCmd implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            this.session = session;

            // Persons
            getPerson("gch", FDTAuthUtils.encrypt("gchpass"), "Guillaume", "Chatelet");
            getPerson("eth", FDTAuthUtils.encrypt("ethpass"), "Thomas", "Eskénazi");
            getPerson("acs", FDTAuthUtils.encrypt("acspass"), "Arnaud", "Chassagne");

            // Commit
            session.flush();
        }

        /**
         * Create a new person in Person Table. If task already exists, no person is created.
         * 
         * @param login
         *            Person login
         * @param password
         *            Person Password
         * @param first
         *            Person first name
         * @param last
         *            Person last name
         * @return Person newly created or person in table if it already exists.
         * @throws Hd3dException
         */
        private IPerson getPerson(String login, String password, String first, String last) throws Hd3dException
        {
            // Retrieve Person.
            IPerson object = (IPerson) session.createCriteria(Persistors.person.getPersistedClass()).add(
                    Restrictions.eq("login", login)).uniqueResult();

            // If person is in database, return person.
            if (object != null)
            {
                return object;
            }

            // Else create a new person instance.
            object = Translators.person.fromLightweight(LPerson.getNewInstance(login, first, last, null, null, null,
                    NumberUtils.LONG_ZERO), null);

            // Save person in database.
            session.save(object);
            session.flush();
            return object;
        }
    }

    private static class InitAssets implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            final IPerson gch = getPerson(session, "gch", FDTAuthUtils.encrypt("gchpass"), "Guillaume", "Chatelet");
            final IPerson eth = getPerson(session, "eth", FDTAuthUtils.encrypt("ethpass"), "Thomas", "Eskénazi");

            final IProject projectA = getProject(session, "ProjectA", EProjectStatus.OPEN, "#FF0000", null);

            ITaskType yourTaskType = getTaskType(session, "yourTaskType", "#99FE80", "The yourTaskType");

            // final IProject projectA = PersistedObjectProviders.project.getObjectWithValue("name", "ProjectA");
            AssetRevisionH revision;

            // Assets
            final AssetRevisionH asset = new AssetRevisionH();
            asset.setProject(projectA);
            asset.setCreationDate(new Date());
            asset.setKey("BSN1");
            asset.setVariation("high");
            asset.setRevision(1);
            asset.setCreator(gch);
            asset.setTaskType(yourTaskType);
            asset.setName("S001_BSN");
            asset.setStatus(EAssetStatus.UNLOCKED);
            asset.setLastOperationDate(new Date());
            asset.setLastUser(eth);
            asset.setComment("no comment");
            asset.setValidity("prod");
            session.save(asset);
            session.flush();

            revision = new AssetRevisionH();
            revision.setProject(projectA);
            revision.setCreationDate(new Date());
            revision.setKey("BSN1");
            revision.setVariation("high");
            revision.setRevision(2);
            revision.setCreator(gch);
            revision.setTaskType(yourTaskType);
            revision.setName("S001_BSN");
            revision.setStatus(EAssetStatus.UNLOCKED);
            revision.setLastOperationDate(new Date());
            revision.setLastUser(eth);
            revision.setComment("new revision");
            revision.setValidity("prod");
            session.save(revision);
            session.flush();

            revision = new AssetRevisionH();
            revision.setProject(projectA);
            revision.setCreationDate(new Date());
            revision.setKey("BSN1");
            revision.setVariation("high");
            revision.setRevision(3);
            revision.setCreator(gch);
            revision.setTaskType(yourTaskType);
            revision.setName("S001_BSN");
            revision.setStatus(EAssetStatus.UNLOCKED);
            revision.setLastOperationDate(new Date());
            revision.setLastUser(eth);
            revision.setComment("new revision");
            revision.setValidity("prod");
            session.save(revision);
            session.flush();

            final AssetRevisionH asset_low = new AssetRevisionH();
            asset_low.setProject(projectA);
            asset_low.setCreationDate(new Date());
            asset_low.setKey("BSN1");
            asset_low.setVariation("low");
            asset_low.setRevision(1);
            asset_low.setCreator(gch);
            asset_low.setTaskType(yourTaskType);
            asset_low.setName("S001_BSN");
            asset_low.setStatus(EAssetStatus.UNLOCKED);
            asset_low.setLastOperationDate(new Date());
            asset_low.setLastUser(eth);
            asset_low.setComment("no comment");
            asset_low.setValidity("prod");
            session.save(asset_low);
            session.flush();

            final AssetRevisionH asset_1 = new AssetRevisionH();
            asset_1.setProject(projectA);
            asset_1.setCreationDate(new Date());
            asset_1.setKey("RND1_chars");
            asset_1.setVariation("low");
            asset_1.setRevision(1);
            asset_1.setCreator(eth);
            asset_1.setTaskType(yourTaskType);
            asset_1.setName("S001_RND_CHARS");
            asset_1.setStatus(EAssetStatus.UNLOCKED);
            asset_1.setLastOperationDate(new Date());
            asset_1.setLastUser(gch);
            asset_1.setComment("no comment");
            asset_1.setValidity("prod");
            session.save(asset_1);
            session.flush();

            final AssetRevisionH asset_2 = new AssetRevisionH();
            asset_2.setProject(projectA);
            asset_2.setCreationDate(new Date());
            asset_2.setKey("RND1_sets");
            asset_2.setVariation("medium");
            asset_2.setRevision(1);
            asset_2.setCreator(eth);
            asset_2.setTaskType(yourTaskType);
            asset_2.setName("S001_RND_SETS");
            asset_2.setStatus(EAssetStatus.UNLOCKED);
            asset_2.setLastOperationDate(new Date());
            asset_2.setLastUser(eth);
            asset_2.setComment("no comment");
            asset_2.setValidity("prod");
            session.save(asset_2);
            session.flush();

            final AssetRevisionH asset_2_high = new AssetRevisionH();
            asset_2_high.setProject(projectA);
            asset_2_high.setCreationDate(new Date());
            asset_2_high.setKey("RND1_sets");
            asset_2_high.setVariation("high");
            asset_2_high.setRevision(1);
            asset_2_high.setCreator(eth);
            asset_2_high.setTaskType(yourTaskType);
            asset_2_high.setName("S001_RND_SETS");
            asset_2_high.setStatus(EAssetStatus.UNLOCKED);
            asset_2_high.setLastOperationDate(new Date());
            asset_2_high.setLastUser(eth);
            asset_2_high.setComment("no comment");
            asset_2_high.setValidity("prod");
            session.save(asset_2);
            session.flush();

            final AssetRevisionH asset_3 = new AssetRevisionH();
            asset_3.setProject(projectA);
            asset_3.setCreationDate(new Date());
            asset_3.setKey("TMOD");
            asset_3.setVariation(null);
            asset_3.setRevision(1);
            asset_3.setCreator(eth);
            asset_3.setTaskType(yourTaskType);
            asset_3.setName("S001_t_model");
            asset_3.setStatus(EAssetStatus.UNLOCKED);
            asset_3.setLastOperationDate(new Date());
            asset_3.setLastUser(eth);
            asset_3.setComment("no comment");
            asset_3.setValidity("prod");
            session.save(asset_3);
            session.flush();

            revision = new AssetRevisionH();
            revision.setProject(projectA);
            revision.setCreationDate(new Date());
            revision.setKey("TMOD");
            revision.setVariation(null);
            revision.setRevision(2);
            revision.setCreator(eth);
            revision.setTaskType(yourTaskType);
            revision.setName("S001_t_model");
            revision.setStatus(EAssetStatus.UNLOCKED);
            revision.setLastOperationDate(new Date());
            revision.setLastUser(eth);
            revision.setComment("no comment");
            revision.setValidity("prod");
            session.save(revision);
            session.flush();

            // AssetLinks
            final AssetRevisionLinkH assetlink = new AssetRevisionLinkH();
            // assetlink.setInputKey(asset.getKey());
            // assetlink.setOutputKey(asset_1.getKey());
            // assetlink.setInputVariation(asset.getVariation());
            // assetlink.setOutputVariation(asset_1.getVariation());
            // assetlink.setInputRevision(1);
            // assetlink.setOutputRevision(2);
            assetlink.setInputAsset(asset);
            assetlink.setOutputAsset(asset_1);
            assetlink.setType("dependancy");
            session.save(assetlink);
            session.flush();

            final AssetRevisionLinkH assetlink_1 = new AssetRevisionLinkH();
            // assetlink_1.setInputKey(asset.getKey());
            // assetlink_1.setOutputKey(asset_2.getKey());
            // assetlink_1.setInputVariation(asset.getVariation());
            // assetlink_1.setOutputVariation(asset_2.getVariation());
            // assetlink_1.setInputRevision(1);
            // assetlink_1.setOutputRevision(2);
            assetlink_1.setInputAsset(asset);
            assetlink_1.setOutputAsset(asset_2);
            assetlink_1.setType("dependancy");
            session.save(assetlink_1);
            session.flush();

            // AssetGroups
            final AssetRevisionGroupH group = new AssetRevisionGroupH();
            group.setName("1st asset group");
            group.addAssetRevision(asset);
            group.addAssetRevision(asset_1);
            session.save(group);
            session.flush();

            final AssetRevisionGroupH group_1 = new AssetRevisionGroupH();
            group_1.setName("2nd asset group");
            group_1.addAssetRevision(asset);
            group_1.addAssetRevision(asset_2);
            group_1.addAssetRevision(asset_3);
            session.save(group_1);
            session.flush();

            final AssetRevisionGroupH group_3 = new AssetRevisionGroupH();
            group_3.setName("GRP008");
            group_3.addAssetRevision(asset_low);
            group_3.addAssetRevision(asset_2_high);
            session.save(group_3);
            session.flush();

        }

        private static ITaskType getTaskType(Session session, String name, String color, String description)
                throws Hd3dPersistenceException
        {
            return getTaskType(session, name, color, description, null);
        }

        private static ITaskType getTaskType(Session session, String name, String color, String description,
                IProject project) throws Hd3dPersistenceException
        {
            final Class<?> persistedClass = Persistors.tasktype.getPersistedClass();
            ITaskType taskType = null;
            Criteria query = session.createCriteria(persistedClass).add(Restrictions.eq("name", name));
            if (StringUtils.isNotBlank(color))
                query.add(Restrictions.eq("color", color));
            if (project != null)
                query.add(Restrictions.eq("project", project));
            if (StringUtils.isNotBlank(description))
                query.add(Restrictions.eq("description", description));

            taskType = (ITaskType) query.uniqueResult();

            if (taskType != null)
            {
                return taskType;
            }
            try
            {
                taskType = new TaskTypeH(null, null, project, color, name, description);

                taskType.doPrePersist();
                session.save(taskType);
                session.flush();
                return taskType;
            }
            catch (Exception e)
            {
                throw new Hd3dPersistenceException(e);
            }
        }

        // private IPerson getPerson(Session session, String login, String password, String first, String last)
        // throws Hd3dTranslationException, Hd3dPersistenceException
        // {
        // // Retrieve Person.
        // IPerson object = (IPerson) session.createCriteria(PersistedObjectProviders.person.getPersistedClass()).add(
        // Restrictions.eq("login", login)).uniqueResult();
        //
        // // If person is in database, return person.
        // if (object != null)
        // {
        // return object;
        // }
        //
        // // Else create a new person instance.
        // object = Translators.person.fromLightweight(LPerson.getNewInstance(login, password, first, last));
        //
        // // Save person in database.
        // session.save(object);
        // session.flush();
        // return object;
        // }

    }

    private static class InitFiles implements IHibernateTransaction
    {
        int index;
        int step = 100;

        public InitFiles(int index, int step)
        {
            this.index = index;
            this.step = step;
        }

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            final IPerson gch = getPerson(session, "gch", FDTAuthUtils.encrypt("gchpass"), "Guillaume", "Chatelet");
            final IPerson eth = getPerson(session, "eth", FDTAuthUtils.encrypt("ethpass"), "Thomas", "Eskénazi");
            final List<IAssetRevision> assetRevisions = Persistors.assetrevision.getByValue("name", "S001_BSN");

            // final IProject projectA = getProject(session, "ProjectA", EProjectStatus.open, "#FF0000", null);

            String storageName = "V";
            MountPointH mountPoint = new MountPointH(storageName, "Linux", "/mountPoint/");
            session.save(mountPoint);
            session.flush();

            // create bunch of files
            FileRevisionH oneFile;
            // FileAttributeHibernateImpl oneFileAttribute;
            for (int i = index; i < (index + step); i++)
            {
                String format = "txt";
                if (0 < i && i < 50)
                {
                    format = "mov";
                }
                if (50 <= i && i < 100)
                {
                    format = "jpg";
                }

                oneFile = new FileRevisionH(new Date(), "SCE_table_" + i, "low", 1, gch, format, 200L,
                        "d41d8cd98f00b204e9800998ecf8427e", "//server_a/repo/", "file1_001.ma", EFileStatus.COMPRESSED,
                        new Date(), eth, "no comment", "prod", null, EFileState.getDefault(), mountPoint.getName());
                oneFile.setAssetRevision(assetRevisions.get(0));
                session.save(oneFile);
                session.flush();
                if (i % 500 == 0)
                    Log.LOGGER.debug("{} files created...", i);
                // file attributes
                if (i % 5 == 0)
                {
                    FileAttributeH oneFileAttribute_0 = new FileAttributeH(oneFile, "colorspace", "0");
                    FileAttributeH oneFileAttribute_1 = new FileAttributeH(oneFile, "definition", "crap");
                    FileAttributeH oneFileAttribute_2 = new FileAttributeH(oneFile, "toto", "toi-meme");
                    FileAttributeH oneFileAttribute_3 = new FileAttributeH(oneFile, "tutu", "etAlors");
                    FileAttributeH oneFileAttribute_4 = new FileAttributeH(oneFile, "tata", "AstaLuego");
                    session.save(oneFileAttribute_0);
                    session.save(oneFileAttribute_1);
                    session.save(oneFileAttribute_2);
                    session.save(oneFileAttribute_3);
                    session.save(oneFileAttribute_4);
                    session.flush();
                }
                if (i % 2 == 0)
                {
                    FileAttributeH oneFileAttribute_0 = new FileAttributeH(oneFile, "colorspace", "1");
                    FileAttributeH oneFileAttribute_1 = new FileAttributeH(oneFile, "definition", "better");
                    FileAttributeH oneFileAttribute_4 = new FileAttributeH(oneFile, "bouh", "blabla");
                    session.save(oneFileAttribute_0);
                    session.save(oneFileAttribute_1);
                    session.save(oneFileAttribute_4);
                    session.flush();
                }
                if (i % 20 == 0)
                {
                    FileAttributeH oneFileAttribute_0 = new FileAttributeH(oneFile, "colorspace", "2");
                    FileAttributeH oneFileAttribute_1 = new FileAttributeH(oneFile, "adresse", "inconnue");
                    FileAttributeH oneFileAttribute_4 = new FileAttributeH(oneFile, "bouh", "blabla");
                    session.save(oneFileAttribute_0);
                    session.save(oneFileAttribute_1);
                    session.save(oneFileAttribute_4);
                    session.flush();
                }
            }
        }
    }

    private static IPerson getPerson(Session session, String login, String password, String first, String last)
            throws Hd3dException
    {
        // Retrieve Person.
        IPerson object = (IPerson) session.createCriteria(Persistors.person.getPersistedClass()).add(
                Restrictions.eq("login", login)).uniqueResult();

        // If person is in database, return person.
        if (object != null)
        {
            return object;
        }

        // Else create a new person instance.
        object = Translators.person.fromLightweight(LPerson.getNewInstance(login, first, last, null, null, null,
                NumberUtils.LONG_ZERO), null);

        // Save person in database.
        session.save(object);
        session.flush();
        return object;
    }

    private static IProject getProject(Session session, String name, EProjectStatus status, String color, String hook)
            throws Hd3dException
    {

        // Retrieve project.
        IProject object = (IProject) session.createCriteria(Persistors.project.getPersistedClass()).add(
                Restrictions.eq("name", name)).uniqueResult();

        // If project is in database, return project.
        if (object != null)
        {
            return object;
        }

        // Else create a new project instance.
        object = Translators.project.fromLightweight(LProject.getNewInstance(name, status.toString(), color, hook,
                null, null), null);

        // Save project in database.
        session.save(object);
        session.flush();
        return object;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
