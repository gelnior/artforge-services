package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ORGANIZATION_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILOrganization;
import fr.hd3d.model.persistence.IOrganization;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single organization object (id ford parameter) <br>
 * URI : /organizations/{id}
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'une organization\r\n" + "\r\nPUT:Modification d'une organization\r\n"
        + "\r\nPOST:Création d'une organization\r\n" + "\r\nDELETE:Désactivation d'une organization")
public class OrganizationResource extends AbstractHd3dResource<ILOrganization, IOrganization>
{
    @Override
    protected IOrganization initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ORGANIZATION_ID_ATTRIBUTE, lockMode);
    }

    public OrganizationResource() throws Hd3dException
    {
        super(Persistors.organization, Translators.organization);
    }
}
