package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectPlayListRevisionGroupPlayListRevisionsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public final class ProjectPlayListRevisionGroupPlayListRevisionsResource extends
        AbstractHd3dCollection<ILPlayListRevision, IPlayListRevision>
{

    public ProjectPlayListRevisionGroupPlayListRevisionsResource() throws Hd3dException
    {
        super(Persistors.playlistrevision, Translators.playlistrevision);
        allowMethods(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectPlayListRevisionGroupPlayListRevisionsCmd(getResourceContext())
                .wantTotalSize());
    }
}
