package fr.hd3d.services.resources;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonSimpleActivitiesCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.utils.AuthenticationUtil;


/**
 * A resource to access to specific person activities.
 * 
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET: Retrieve current prerson task activity list. <br />\r\n"
        + "POST: Create a task activity for current person.<br />\r\n")
public class MySimpleActivitiesResource extends AbstractHd3dCollection<ILSimpleActivity, ISimpleActivity>
{
    public MySimpleActivitiesResource() throws Hd3dException
    {
        super(Persistors.simpleActivity, Translators.simpleActivity);
    }

    // TODO: Optimize for bulk requests
    /**
     * Checks that no activity has been set for current day.
     */
    @Override
    protected void doPrePersist(Representation entity, ISimpleActivity newObject) throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user == null)
            return;

        if (user.getId().longValue() == newObject.getWorker().getId().longValue())
        {
            String[] fields = { "filledBy.id", "day.id", "type" };
            Object[] values = { newObject.getFilledBy().getId(), newObject.getDay().getId(), newObject.getType() };

            List<ISimpleActivity> activities = Persistors.simpleActivity.getByValues(fields, values, "AND");

            if (CollectionUtils.isEmpty(activities))
            {
                super.doPrePersist(entity, newObject);
            }
            else
            {
                throw new Hd3dException(
                        "An activity already exists for this task, this user and this day. Please modify existing one rather than creating a new one.");
            }
        }
        else
        {
            throw new Hd3dException("You cannot create activity you don't own.");
        }
    }

    @Override
    protected ResultCollection<ISimpleActivity> getCollectionForRepresentation() throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user != null)
        {
            return doGetCollection(new GetPersonSimpleActivitiesCmd(getResourceContext(), user.getId()).wantTotalSize());
        }
        else
        {
            throw new Hd3dException("No user is set for this request, cannot returns activities for current user.");
        }
    }

}
