package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.COMPUTER_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILComputer;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computers/{id}
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'une machine\r\n" + "\r\nPUT:Modification d'une machine\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Désactivation d'une machine")
public class ComputerResource extends AbstractHd3dResource<ILComputer, IComputer>
{
    @Override
    protected IComputer initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(COMPUTER_ID_ATTRIBUTE, lockMode);
    }

    public ComputerResource() throws Hd3dException
    {
        super(Persistors.computer, Translators.computer);
    }
}
