package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILContract;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all contract objects.<br>
 * URI : /contracts/
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération de toutes les contrats\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:Création d'un contrat\r\n"
        + "\r\nDELETE:N/A")
public class ContractsResource extends AbstractHd3dCollection<ILContract, IContract>
{

    public ContractsResource() throws Hd3dException
    {
        super(Persistors.contract, Translators.contract);
    }
}
