package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILItem;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetItemsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des 'item' qui composent l'itemGroup\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Création d'un nouveau item\r\n" + "\r\nDELETE:N/A")
public class ItemsResource extends AbstractHd3dCollection<ILItem, IItem>
{
    public ItemsResource() throws Hd3dException
    {
        super(Persistors.item, Translators.item);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetItemsCmd(getResourceContext()).wantTotalSize());
    }
}
