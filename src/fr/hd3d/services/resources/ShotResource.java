package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.SHOT_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ShotResource extends AbstractHd3dResource<ILShot, IShot>
{
    public ShotResource() throws Hd3dException
    {
        super(Persistors.shot, Translators.shot);
    }

    @Override
    protected IShot initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(SHOT_ID_ATTRIBUTE, lockMode);
    }
}
