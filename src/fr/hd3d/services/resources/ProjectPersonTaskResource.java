package fr.hd3d.services.resources;

import org.hibernate.LockMode;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectPersonTaskCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.UniqueResultQuery;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


/**
 * A resource to access to a specific person's task in a project.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'une tache\r\n" + "\r\nPUT:Mise a jour d'une tache\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Suppression d'une tache")
public class ProjectPersonTaskResource extends AbstractHd3dResource<ILTask, ITask>
{
    public ProjectPersonTaskResource() throws Hd3dException
    {
        super(Persistors.task, Translators.task);
    }

    @Override
    protected ITask initializeObject(LockMode lockMode) throws Hd3dException
    {
        final UniqueResultQuery<ILTask, ITask> cmd = new GetProjectPersonTaskCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        ITask obj = (ITask) cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }

    @Override
    public Representation delete(Variant variant)
    {
        if (!initObject())
        {
            return getResponseEntity();
        }
        if (object == null)
        {
            /*
             * NOTE: do not set Status to Status.SUCCESS_NO_CONTENT If so, there will be no empty list returned in the
             * HTML screen, which may be confusing
             */
            return getResponseEntity();
        }
        if (!object.getTaskActivities().isEmpty())
        {
            // TODO improve logging
            getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        }
        else
        {
            super.delete(variant);
        }
        return getResponseEntity();
    }
}
