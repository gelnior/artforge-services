package fr.hd3d.services.resources.sheet;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.beanutils.WrapDynaBean;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TypeUtils;


public class Lt extends AbstractTypeHandler
{

    protected IOneOperandTypeHandler handler;

    public Lt()
    {}

    public Lt(Class<?> clazz) throws Hd3dException
    {
        super(clazz);
        this.handler = instance();
    }

    @Override
    void setHandler() throws Hd3dException
    {
        this.handler = instance();
    }

    public IOneOperandTypeHandler instance() throws Hd3dException
    {
        if (Collection.class.isAssignableFrom(clazz))
            return new Lt_Collection();
        if (IBase.class.isAssignableFrom(clazz))
            return new Lt_Entity();
        if (Long.class.isAssignableFrom(clazz))
            return new Lt_Long();
        if (Integer.class.isAssignableFrom(clazz))
            return new Lt_Integer();
        if (Date.class.isAssignableFrom(clazz))
            return new Lt_Date();
        throw new Hd3dException("Type of value not handled:" + clazz == null ? null : clazz.getCanonicalName());
    }

    public boolean match(Object columnValue, Object value, Object start, Object end, String field) throws Hd3dException
    {
        return handler.match(columnValue, value, field);
    }

    public static class Lt_Collection implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return lt((Collection<?>) columnValue, value, field);
        }

        protected boolean lt(Collection<?> columnValue, Object value, String field) throws Hd3dException
        {
            if (CollectUtils.isEmpty(columnValue))
                return false;

            Object first = columnValue.iterator().next();
            if (first == null)
                return false;

            IOneOperandTypeHandler handler = (IOneOperandTypeHandler) new Lt(first.getClass()).instance();
            for (Object o : columnValue)
            {
                if (handler.match(o, value, field))
                    return true;
            }
            return false;
        }
    }

    public static class Lt_Entity implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return lt((IBase) columnValue, value, field);
        }

        protected boolean lt(IBase columnValue, Object value, String field) throws Hd3dException
        {
            if (columnValue == null)
            {
                return value != null;
            }
            else
            {
                Object colVal = new WrapDynaBean(columnValue).get(field);
                IOneOperandTypeHandler handler = (IOneOperandTypeHandler) new Lt(colVal.getClass()).instance();
                return handler.match(colVal, value, field);
            }
        }
    }

    public static class Lt_Long implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return lt((Long) columnValue, (Long) value, field);
        }

        protected boolean lt(Long columnValue, Long value, String field)
        {
            if (columnValue == null && value == null)
                return true;
            if (columnValue != null)
                return columnValue.compareTo(value) < 0;
            else
                return false;
        }
    }

    public static class Lt_Integer implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return lt((Integer) columnValue, TypeUtils.toInt(value), field);
        }

        protected boolean lt(Integer columnValue, Integer value, String field)
        {
            if (columnValue == null && value == null)
                return true;
            if (columnValue != null)
                return columnValue.compareTo(value) < 0;
            else
                return false;
        }
    }

    public static class Lt_Date implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return lt((Date) columnValue, (Date) value, field);
        }

        protected boolean lt(Date columnValue, Date value, String field)
        {
            if (columnValue == null && value == null)
                return true;
            if (columnValue != null)
                return columnValue.compareTo(value) < 0;
            else
                return false;
        }
    }
}
