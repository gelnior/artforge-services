package fr.hd3d.services.resources.sheet;

import fr.hd3d.exception.Hd3dException;


public interface IOneOperandTypeHandler
{
    boolean match(Object columnValue, Object value, String field) throws Hd3dException;
}
