package fr.hd3d.services.resources.sheet;

import fr.hd3d.exception.Hd3dException;


public class Neq extends AbstractTypeHandler
{
    protected ITypeHandler eqHandler;

    public Neq()
    {}

    public Neq(Class<?> clazz) throws Hd3dException
    {
        super(clazz);
        eqHandler = new Eq(clazz);
    }

    @Override
    void setHandler() throws Hd3dException
    {
        eqHandler = new Eq(clazz);
    }

    public boolean match(Object columnValue, Object value, Object start, Object end, String field) throws Hd3dException
    {
        return !eqHandler.match(columnValue, value, start, end, field);
    }
}
