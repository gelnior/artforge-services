package fr.hd3d.services.resources.sheet;

import fr.hd3d.exception.Hd3dException;


public interface ITypeHandler
{
    boolean match(Object columnValue, Object value, Object start, Object end, String field) throws Hd3dException;
}
