package fr.hd3d.services.resources.sheet;

import fr.hd3d.exception.Hd3dException;


public class IsNotNull extends AbstractTypeHandler
{
    protected ITypeHandler isNullHandler;

    public IsNotNull()
    {}

    public IsNotNull(Class<?> clazz) throws Hd3dException
    {
        super(clazz);
        isNullHandler = new IsNull(clazz);
    }

    @Override
    void setHandler() throws Hd3dException
    {
        isNullHandler = new IsNull(clazz);
    }

    public boolean match(Object columnValue, Object value, Object start, Object end, String field) throws Hd3dException
    {
        return !isNullHandler.match(columnValue, value, start, end, field);
    }
}
