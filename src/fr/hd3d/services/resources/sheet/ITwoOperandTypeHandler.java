package fr.hd3d.services.resources.sheet;

import fr.hd3d.exception.Hd3dException;


public interface ITwoOperandTypeHandler
{
    boolean match(Object columnValue, Object start, Object end, String field) throws Hd3dException;
}
