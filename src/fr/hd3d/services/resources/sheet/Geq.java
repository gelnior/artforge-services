package fr.hd3d.services.resources.sheet;

import fr.hd3d.exception.Hd3dException;


public class Geq extends AbstractTypeHandler
{
    protected ITypeHandler gtHandler;
    protected ITypeHandler eqHandler;

    public Geq()
    {}

    public Geq(Class<?> clazz) throws Hd3dException
    {
        super(clazz);
        gtHandler = new Gt(clazz);
        eqHandler = new Eq(clazz);
    }

    @Override
    void setHandler() throws Hd3dException
    {
        gtHandler = new Gt(clazz);
        eqHandler = new Eq(clazz);
    }

    public boolean match(Object columnValue, Object value, Object start, Object end, String field) throws Hd3dException
    {
        return gtHandler.match(columnValue, value, start, end, field)
                || eqHandler.match(columnValue, value, start, end, field);
    }
}
