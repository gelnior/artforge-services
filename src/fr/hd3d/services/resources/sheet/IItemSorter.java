package fr.hd3d.services.resources.sheet;

import java.util.Map;

import org.apache.commons.collections15.comparators.ComparatorChain;

import fr.hd3d.exception.Hd3dException;


public interface IItemSorter
{
    Map<String, String> getQueryMap();

    void setQueryMap(Map<String, String> map);

    Object[][][] sort(Object[][][] values) throws Hd3dException;
    
    ComparatorChain getComparators();
}
