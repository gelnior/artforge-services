package fr.hd3d.services.resources.sheet;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import fr.hd3d.common.client.ItemConstraint;
import fr.hd3d.common.client.LogicConstraint;
import fr.hd3d.common.client.enums.EItemFilterLogicalOperator;
import fr.hd3d.common.client.enums.EItemFilterOperator;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.Pair;
import fr.hd3d.utils.SheetUtils;


public class ItemFilterTypeHandler implements IItemFilter
{
    static private Map<EItemFilterOperator, ITypeHandler> itemFilterOperators = new EnumMap<EItemFilterOperator, ITypeHandler>(
            EItemFilterOperator.class);

    static
    {
        itemFilterOperators.put(EItemFilterOperator.gt, new Gt());
        itemFilterOperators.put(EItemFilterOperator.lt, new Lt());
        itemFilterOperators.put(EItemFilterOperator.eq, new Eq());
        itemFilterOperators.put(EItemFilterOperator.geq, new Geq());
        itemFilterOperators.put(EItemFilterOperator.leq, new Leq());
        itemFilterOperators.put(EItemFilterOperator.neq, new Neq());
        itemFilterOperators.put(EItemFilterOperator.btw, new Btw());
        itemFilterOperators.put(EItemFilterOperator.in, new In());
        itemFilterOperators.put(EItemFilterOperator.notin, new NotIn());
        itemFilterOperators.put(EItemFilterOperator.like, new Like());
        itemFilterOperators.put(EItemFilterOperator.isnull, new IsNull());
        itemFilterOperators.put(EItemFilterOperator.isnotnull, new IsNotNull());
    }

    protected Map<String, String> queryMap;
    protected Class<?> clazz;
    protected String type;

    public ItemFilterTypeHandler()
    {}

    public ItemFilterTypeHandler(Class<?> clazz, String type)
    {
        this.clazz = clazz;
        this.type = type;
    }

    public Class<?> getClazz()
    {
        return clazz;
    }

    public String getType()
    {
        return type;
    }

    public void setClazz(Class<?> clazz)
    {
        this.clazz = clazz;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Map<String, String> getQueryMap()
    {
        return this.queryMap;
    }

    public void setQueryMap(Map<String, String> map)
    {
        this.queryMap = map;

    }

    public ITypeHandler instance() throws Hd3dException
    {
        EItemFilterOperator filterType = EItemFilterOperator.valueOf(type);
        ITypeHandler operator = itemFilterOperators.get(filterType);
        if (operator == null)
        {
            throw new Hd3dException("Command not implemented " + type);
        }
        ((AbstractTypeHandler) operator).setClazz(clazz);

        return operator;
    }

    public Object[][][] filter(Object[][][] values) throws Hd3dException
    {
        List<Integer> idx = filterAndReturnIndexes(values);
        return collectFilteredValues(values, idx);
    }

    private Object[][][] collectFilteredValues(Object[][][] values, List<Integer> idx)
    {
        Object[][][] ret = null;
        if (CollectUtils.isNotEmpty(idx))
        {
            List<Object[][]> filtered = new ArrayList<Object[][]>();
            for (Integer i : idx)
            {
                filtered.add(values[i]);
            }

            ret = (Object[][][]) filtered.toArray(new Object[filtered.size()][][]);
        }
        else
        {
            ret = new Object[0][0][0];
        }
        return ret;
    }

    protected List<Map<String, Object>> getItemConstraints() throws Hd3dQueryFilterException
    {
        return Hd3dJsonSerializer.<List<Map<String, Object>>> unserialize((String) getQueryMap().get(
                ItemConstraint.ITEMCONSTRAINT));
    }

    @SuppressWarnings("unchecked")
    protected List<Map<String, Object>> getFilter(final Map<String, Object> constraintMap)
            throws Hd3dQueryFilterException
    {
        return (List<Map<String, Object>>) constraintMap.get(LogicConstraint.FILTER);
    }

    public List<Integer> filterAndReturnIndexes(Object[][][] values) throws Hd3dException
    {
        List<Integer> ret = new ArrayList<Integer>();

        for (Map<String, Object> map : getItemConstraints())
        {
            ret.addAll(filter(values, map));
        }

        return ret;
    }

    public List<Integer> filter(Object[][][] values, Map<String, Object> map) throws Hd3dException
    {
        List<Integer> ret = null;

        if (isLogicalMap(map))
        {
            ret = filterLogicalConstraint(values, map);
        }
        else if (isSimpleConstraint(map))
        {
            ret = filterSimpleConstraint(values, map);
        }

        return ret;
    }

    protected boolean isLogicalMap(Map<String, Object> map)
    {
        return map.get(LogicConstraint.LOGIC) != null;
    }

    protected boolean isSimpleConstraint(Map<String, Object> map)
    {
        return map.get(LogicConstraint.LOGIC) == null;
    }

    protected String getLogicOperator(final Map<String, Object> constraintMap) throws Hd3dItemFilterException
    {
        String logicOperator = (String) constraintMap.get(LogicConstraint.LOGIC);
        if (logicOperator == null)
        {
            throw new Hd3dItemFilterException("Not a logical expression");
        }
        return logicOperator;
    }

    private List<Integer> getIndexes(Object[][][] values)
    {
        List<Integer> ret = new ArrayList<Integer>(values.length);
        for (int i = 0; i < values.length; i++)
        {
            ret.add(i);
        }
        return ret;
    }

    @SuppressWarnings("unchecked")
    private List<Integer> filterLogicalConstraint(Object[][][] values, Map<String, Object> map) throws Hd3dException
    {
        if (values == null || values.length == 0)
            return new ArrayList<Integer>(0);

        /*----------------------------
         * Collect parameters
         -----------------------------*/
        final String logicOperator = getLogicOperator(map);

        List<Map<String, Object>> maps = getFilter(map);

        List<Integer> firstTermResult;
        List<Integer> secondTermResult;
        switch (EItemFilterLogicalOperator.valueOf(logicOperator))
        {
            case AND:
            {
                firstTermResult = filter(values, maps.get(0));
                secondTermResult = filter(values, maps.get(1));

                return (List<Integer>) CollectionUtils.intersection(firstTermResult, secondTermResult);
            }
            case OR:
            {
                firstTermResult = filter(values, maps.get(0));
                secondTermResult = filter(values, maps.get(1));

                return (List<Integer>) CollectionUtils.union(firstTermResult, secondTermResult);
            }
            case NOT:
            {
                firstTermResult = filter(values, maps.get(0));

                return (List<Integer>) CollectionUtils.subtract(getIndexes(values), firstTermResult);
            }
            default:
                throw new Hd3dItemFilterException("Command not implemented " + logicOperator);
        }
    }

    private List<Integer> filterSimpleConstraint(Object[][][] values, Map<String, Object> map) throws Hd3dException
    {
        List<Integer> ret = new ArrayList<Integer>();

        if (values != null && values.length > 0)
        {
            /*----------------------------
             * Collect parameters
            -----------------------------*/
            final Long itemId = (Long) map.get(ItemConstraint.ITEMID_MAP_FIELD);
            if (itemId == null)
            {
                return ret;
            }

            final Pair<IItem, Integer> tuple = SheetUtils.getColumnItemAndItemIndex(values, itemId);
            if (tuple != null)
            {
                final int columnIndex = tuple.getField_2();

                final String type = (String) map.get(ItemConstraint.TYPE_MAP_FIELD);
                /* check mandatory fields */
                if (type == null)
                {
                    throw new Hd3dItemFilterException("type cannot be null ");
                }
                final Object value = map.get(ItemConstraint.VALUE_MAP_FIELD);
                final String field = (String) map.get(ItemConstraint.FIELD_FIELD);
                final Object start = map.get(ItemConstraint.START_MAP_FIELD);
                final Object end = map.get(ItemConstraint.END_MAP_FIELD);

                /*-----------------------------
                 * Filter
                ------------------------------*/
                /* Instantiate the proper ItemFilterTypeHandler */
                final ITypeHandler handler = getTypeHandler(values, columnIndex, type);

                if (handler != null)
                {
                    /* collect indexes of the matching lines */
                    Object columnValue;
                    for (int i = 0; i < values.length; i++)
                    {
                        columnValue = values[i][columnIndex][0];
                        try
                        {
                            if (handler.match(columnValue, value, start, end, field))
                            {
                                ret.add(i);
                            }
                        }
                        catch (Hd3dException e)
                        {
                            Log.LOGGER.error(ExceptUtils.format(e));
                        }
                    }
                }
            }
        }

        return ret;
    }

    private ITypeHandler getTypeHandler(Object[][][] values, final int columnIndex, final String columntype)
            throws Hd3dException
    {
        return new ItemFilterTypeHandler(getColumnClass(values, columnIndex), columntype).instance();
    }

    private Class<?> getColumnClass(Object[][][] values, final int columnIndex)
    {
        Class<?> ret = null;

        for (int i = 0; i < values.length; i++)
        {
            Object columnValue = values[i][columnIndex][0];
            if (columnValue != null)
            {
                ret = columnValue.getClass();
                break;
            }
        }

        return ret;
    }
}
