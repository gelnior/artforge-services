package fr.hd3d.services.resources.sheet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.hd3d.common.client.ItemConstraint;
import fr.hd3d.exception.Hd3dException;


public class ItemFilterFactory
{
    public static class ItemFilters
    {

        private List<IItemFilter> filters = new ArrayList<IItemFilter>();

        public List<IItemFilter> getFilters()
        {
            return filters;
        }

        public void addFilter(IItemFilter filter)
        {
            getFilters().add(filter);
        }

        public void removeFilter(IItemFilter filter)
        {
            getFilters().remove(filter);
        }

        public Object[][][] filter(Object[][][] values) throws Hd3dException
        {
            Object[][][] tmp = values;
            for (IItemFilter filter : getFilters())
            {
                tmp = filter.filter(tmp);
            }
            return tmp;
        }

    }

    @SuppressWarnings("serial")
    private static final Map<String, Class<? extends IItemFilter>> QUERY_KEYWORDS = Collections
            .unmodifiableMap(new HashMap<String, Class<? extends IItemFilter>>() {
                {
                    put(ItemConstraint.ITEMCONSTRAINT, ItemFilterTypeHandler.class);
                }
            });

    public static ItemFilters getFilter(final Map<String, String> map) throws Hd3dException
    {
        /*
         * ex: itemConstraint=...&itemSort=... Le resultat de itemConstraint est passé a itemSort.
         */
        ItemFilters filters = new ItemFilters();
        for (Map.Entry<String, String> entry : map.entrySet())
        {
            /* find out the proper handler for the passed parameter */
            Class<? extends IItemFilter> parserClass = QUERY_KEYWORDS.get(entry.getKey());
            if (parserClass != null)
            {
                try
                {
                    IItemFilter queryParser = parserClass.newInstance();
                    queryParser.setQueryMap(map);
                    filters.addFilter(queryParser);
                }
                catch (InstantiationException e)
                {
                    throw new Hd3dException(e);
                }
                catch (IllegalAccessException e)
                {
                    throw new Hd3dException(e);
                }
            }
        }
        return filters;
    }
}
