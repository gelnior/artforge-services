package fr.hd3d.services.resources.sheet;

import fr.hd3d.exception.Hd3dException;


public interface INoOperandTypeHandler
{
    boolean match(Object columnValue, String field) throws Hd3dException;
}
