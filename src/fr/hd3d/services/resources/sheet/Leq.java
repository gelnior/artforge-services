package fr.hd3d.services.resources.sheet;

import fr.hd3d.exception.Hd3dException;


public class Leq extends AbstractTypeHandler
{

    protected ITypeHandler ltHandler;
    protected ITypeHandler eqHandler;

    public Leq()
    {}

    public Leq(Class<?> clazz) throws Hd3dException
    {
        super(clazz);
        ltHandler = new Lt(clazz);
        eqHandler = new Eq(clazz);
    }

    @Override
    void setHandler() throws Hd3dException
    {
        ltHandler = new Lt(clazz);
        eqHandler = new Eq(clazz);
    }

    public boolean match(Object columnValue, Object value, Object start, Object end, String field) throws Hd3dException
    {
        return ltHandler.match(columnValue, value, start, end, field)
                || eqHandler.match(columnValue, value, start, end, field);
    }
}
