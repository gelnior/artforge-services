package fr.hd3d.services.resources.sheet;

import java.util.Map;

import fr.hd3d.exception.Hd3dException;


public interface IItemFilter
{
    public Map<String, String> getQueryMap();

    public void setQueryMap(Map<String, String> map);

    public Object[][][] filter(Object[][][] values) throws Hd3dException;
}
