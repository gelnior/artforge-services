package fr.hd3d.services.resources.sheet;

import java.util.Collection;

import org.apache.commons.beanutils.WrapDynaBean;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.utils.CollectUtils;


public class IsNull extends AbstractTypeHandler
{

    protected INoOperandTypeHandler handler;

    public IsNull()
    {}

    public IsNull(Class<?> clazz) throws Hd3dException
    {
        super(clazz);
        this.handler = instance();
    }

    @Override
    void setHandler() throws Hd3dException
    {
        this.handler = instance();
    }

    public INoOperandTypeHandler instance() throws Hd3dException
    {
        if (Collection.class.isAssignableFrom(clazz))
            return new IsNull_Collection();
        if (IBase.class.isAssignableFrom(clazz))
            return new IsNull_Entity();
        return new IsNull_Object();
    }

    public boolean match(Object columnValue, Object value, Object start, Object end, String field) throws Hd3dException
    {
        return handler.match(columnValue, field);
    }

    public static class IsNull_Collection implements INoOperandTypeHandler
    {
        public boolean match(Object columnValue, String field) throws Hd3dException
        {
            return isNull((Collection<?>) columnValue, field);
        }

        protected boolean isNull(Collection<?> columnValue, String field) throws Hd3dException
        {
            if (CollectUtils.isEmpty(columnValue))
                return true;

            Object first = columnValue.iterator().next();
            if (first == null)
                return true;

            INoOperandTypeHandler handler = (INoOperandTypeHandler) new IsNull(first.getClass()).instance();
            for (Object o : columnValue)
            {
                if (handler.match(o, field))
                    return true;
            }
            return false;
        }
    }

    public static class IsNull_Entity implements INoOperandTypeHandler
    {
        public boolean match(Object columnValue, String field) throws Hd3dException
        {
            return isNull((IBase) columnValue, field);
        }

        protected boolean isNull(IBase columnValue, String field) throws Hd3dException
        {
            Object colVal = new WrapDynaBean(columnValue).get(field);
            INoOperandTypeHandler handler = (INoOperandTypeHandler) new IsNull(colVal.getClass()).instance();
            return handler.match(colVal, field);
        }
    }

    public static class IsNull_Object implements INoOperandTypeHandler
    {
        public boolean match(Object columnValue, String field) throws Hd3dException
        {
            return columnValue == null;
        }
    }
}
