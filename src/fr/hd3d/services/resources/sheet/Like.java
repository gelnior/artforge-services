package fr.hd3d.services.resources.sheet;

import java.util.Collection;

import org.apache.commons.beanutils.WrapDynaBean;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.utils.CollectUtils;


public class Like extends AbstractTypeHandler
{
    protected IOneOperandTypeHandler handler;

    public Like()
    {}

    public Like(Class<?> clazz) throws Hd3dException
    {
        super(clazz);
        this.handler = instance();
    }

    @Override
    void setHandler() throws Hd3dException
    {
        this.handler = instance();
    }

    public IOneOperandTypeHandler instance() throws Hd3dException
    {
        if (Collection.class.isAssignableFrom(clazz))
            return new Like_Collection();
        if (IBase.class.isAssignableFrom(clazz))
            return new Like_Entity();
        if (String.class.isAssignableFrom(clazz))
            return new Like_String();
        throw new Hd3dException("Type of value not handled:" + clazz == null ? null : clazz.getCanonicalName());
    }

    public boolean match(Object columnValue, Object value, Object start, Object end, String field) throws Hd3dException
    {
        return handler.match(columnValue, value, field);
    }

    public static class Like_Collection implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return like((Collection<?>) columnValue, value, field);
        }

        protected boolean like(Collection<?> columnValue, Object value, String field) throws Hd3dException
        {
            if (CollectUtils.isEmpty(columnValue))
                return false;

            Object first = columnValue.iterator().next();
            if (first == null)
                return false;

            IOneOperandTypeHandler handler = (IOneOperandTypeHandler) new Like(first.getClass()).instance();
            for (Object o : columnValue)
            {
                if (handler.match(o, value, field))
                    return true;
            }
            return false;
        }
    }

    public static class Like_Entity implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return like((IBase) columnValue, value, field);
        }

        protected boolean like(IBase columnValue, Object value, String field) throws Hd3dException
        {
            if (columnValue == null)
            {
                return value == null;
            }
            else
            {
                Object colVal = new WrapDynaBean(columnValue).get(field);
                IOneOperandTypeHandler handler = (IOneOperandTypeHandler) new Like(colVal.getClass()).instance();
                return handler.match(colVal, value, field);
            }
        }
    }

    public static class Like_String implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return like((String) columnValue, (String) value, field);
        }

        protected boolean like(String columnValue, String value, String field)
        {
            return columnValue.contains(value);
        }
    }
}
