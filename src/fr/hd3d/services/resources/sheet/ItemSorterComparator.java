package fr.hd3d.services.resources.sheet;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.ComparatorUtils;

import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.persistence.impl.hibernate.NameOrLabelOrTitleHandler;
import fr.hd3d.model.persistence.impl.hibernate.serialization.SheetDataBinaryFileBuffer;
import fr.hd3d.utils.CollectUtils;


public class ItemSorterComparator implements Comparator<Object>
{
    private int columnIndex;

    public ItemSorterComparator(int columnIndex)
    {
        super();
        this.columnIndex = columnIndex;
    }

    @SuppressWarnings("unchecked")
    public int compare(Object o1, Object o2)
    {
        final int ret;

        if (o1 == null && o2 == null)
        {
            ret = 0;
        }
        else if (Object[][].class.isInstance(o1) && Object[][].class.isInstance(o2))
        {
            Object[][] first = (Object[][]) o1;
            Object[][] second = (Object[][]) o2;
            if (first == null && second == null)
            {
                ret = 0;
            }
            else
            {
                Object firstObj = first == null ? null : (first[columnIndex] == null ? null : first[columnIndex][0]);

                Object secondObj = second == null ? null
                        : (second[columnIndex] == null ? null : second[columnIndex][0]);

                ret = ComparatorUtils.nullHighComparator(this).compare(firstObj, secondObj);
            }
        }
        else if (List.class.isInstance(o1) && List.class.isInstance(o2))
        {
            /* case of ApprovalNotes */
            final java.util.List list1 = (java.util.List) o1;
            final boolean isApprovalNotes = list1.size() == 2 && Boolean.class.isInstance(list1.get(0))
                    && java.util.List.class.isInstance(list1.get(1));

            if (isApprovalNotes)
            {
                final java.util.List list2 = (java.util.List) o2;
                ret = ComparatorUtils.nullHighComparator(this).compare(
                        CollectUtils.getFirstOrNull((List<?>) list1.get(1)),
                        CollectUtils.getFirstOrNull((List<?>) list2.get(1)));
            }
            else
            {
                ret = ComparatorUtils.nullHighComparator(this).compare(CollectUtils.getLastOrNull((List<?>) o1),
                        CollectUtils.getLastOrNull((List<?>) o2));
            }
        }
        else if (Persistent.class.isInstance(o1) && Persistent.class.isInstance(o2))
        {
            if (NameOrLabelOrTitleHandler.hasNameOrLabelOrTitle((Persistent) o1))
            {
                ret = ComparatorUtils.nullHighComparator(this).compare(
                        NameOrLabelOrTitleHandler.getNameOrLabelOrTitle((Persistent) o1),
                        NameOrLabelOrTitleHandler.getNameOrLabelOrTitle((Persistent) o2));
            }
            else if (IApprovalNote.class.isInstance(o1) && IApprovalNote.class.isInstance(o2))
            {
                IApprovalNote obj1 = (IApprovalNote) o1;
                IApprovalNote obj2 = (IApprovalNote) o2;
                String status1 = obj1 != null ? obj1.getStatus() : null;
                String status2 = obj2 != null ? obj2.getStatus() : null;
                Date date1 = obj1 != null ? obj1.getApprovalDate() : null;
                Date date2 = obj2 != null ? obj2.getApprovalDate() : null;

                int statusComp = ComparatorUtils.nullHighComparator(this).compare(status1, status2);
                if (statusComp != 0)
                    ret = statusComp;
                else
                {
                    ret = ComparatorUtils.reversedComparator(ComparatorUtils.nullHighComparator(this)).compare(date1,
                            date2);
                }
                // ret = ComparatorUtils.nullHighComparator(this).compare(status1, status2)
                // + ComparatorUtils.reversedComparator(ComparatorUtils.nullHighComparator(this)).compare(date1,
                // date2);
            }
            else if (IPerson.class.isInstance(o1) && IPerson.class.isInstance(o2))
            {
                IPerson obj1 = (IPerson) o1;
                IPerson obj2 = (IPerson) o2;
                String lastName1 = obj1 != null ? obj1.getLastName() : null;
                String lastName2 = obj2 != null ? obj2.getLastName() : null;
                ret = ComparatorUtils.nullHighComparator(this).compare(lastName1, lastName2);
            }
            else if (IStep.class.isInstance(o1) && IStep.class.isInstance(o2))
            {
                IStep obj1 = (IStep) o1;
                IStep obj2 = (IStep) o2;
                String taskTypeName1 = obj1 != null ? obj1.getTaskType().getName() : null;
                String taskTypeName2 = obj2 != null ? obj2.getTaskType().getName() : null;
                ret = ComparatorUtils.nullHighComparator(this).compare(taskTypeName1, taskTypeName2);
            }
            else
            {
                ret = ComparatorUtils.nullHighComparator(this).compare(o1, o2);
            }
        }
        else if (SheetDataBinaryFileBuffer.class.isInstance(o1) && SheetDataBinaryFileBuffer.class.isInstance(o2))
        {
            ret = ((SheetDataBinaryFileBuffer) o1).compareTo((SheetDataBinaryFileBuffer) o2);
        }
        else
        {
            ret = ComparatorUtils.nullHighComparator(ComparatorUtils.naturalComparator()).compare(o1, o2);
        }

        return ret;
    }
}
