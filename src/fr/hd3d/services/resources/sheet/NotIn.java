package fr.hd3d.services.resources.sheet;

import fr.hd3d.exception.Hd3dException;


public class NotIn extends AbstractTypeHandler
{
    ITypeHandler inHandler;

    public NotIn()
    {}

    public NotIn(Class<?> clazz) throws Hd3dException
    {
        super(clazz);
        inHandler = new In(clazz);
    }

    @Override
    void setHandler() throws Hd3dException
    {
        inHandler = new In(clazz);
    }

    public boolean match(Object columnValue, Object value, Object start, Object end, String field) throws Hd3dException
    {
        return !inHandler.match(columnValue, value, start, end, field);
    }
}
