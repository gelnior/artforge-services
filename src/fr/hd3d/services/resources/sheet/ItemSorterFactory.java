package fr.hd3d.services.resources.sheet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.hd3d.common.client.ItemConstraint;
import fr.hd3d.exception.Hd3dException;


public class ItemSorterFactory
{
    public static class ItemSorters
    {
        private List<IItemSorter> sorters = new ArrayList<IItemSorter>();

        public List<IItemSorter> getSorters()
        {
            return sorters;
        }

        public IItemSorter getSorter(int i)
        {
            if (i < sorters.size())
            {
                return sorters.get(i);
            }
            else
            {
                return null;
            }
        }

        public void addSorter(IItemSorter sorter)
        {
            getSorters().add(sorter);
        }

        public void removeSorter(IItemSorter sorter)
        {
            getSorters().remove(sorter);
        }

        public Object[][][] sort(Object[][][] values) throws Hd3dException
        {
            Object[][][] tmp = values;
            for (IItemSorter sorter : getSorters())
            {
                tmp = sorter.sort(tmp);
            }
            return tmp;
        }

        public boolean isEmpty()
        {
            return sorters.isEmpty();
        }

    }

    @SuppressWarnings("serial")
    private static final Map<String, Class<? extends IItemSorter>> QUERY_KEYWORDS = Collections
            .unmodifiableMap(new HashMap<String, Class<? extends IItemSorter>>() {
                {
                    put(ItemConstraint.ITEMSORT, ItemSorterTypeHandler.class);
                }
            });

    /*
     * parse "itemSort" columns
     */
    public static ItemSorters getSorters(final Map<String, String> map) throws Hd3dException
    {
        /*
         * ex: itemConstraint=...&itemSort=... Le resultat de itemConstraint est passé a itemSort.
         */
        ItemSorters sorters = new ItemSorters();
        Class<? extends IItemSorter> sorterClass;
        IItemSorter sorter;
        for (Map.Entry<String, String> entry : map.entrySet())
        {
            /* find out the proper handler for the passed parameter */
            sorterClass = QUERY_KEYWORDS.get(entry.getKey());
            if (sorterClass != null)
            {
                try
                {
                    sorter = sorterClass.newInstance();
                    sorter.setQueryMap(map);
                    sorters.addSorter(sorter);
                }
                catch (InstantiationException e)
                {
                    throw new Hd3dException(e);
                }
                catch (IllegalAccessException e)
                {
                    throw new Hd3dException(e);
                }
            }
        }
        return sorters;
    }
}
