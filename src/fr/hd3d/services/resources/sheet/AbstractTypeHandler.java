package fr.hd3d.services.resources.sheet;

import fr.hd3d.exception.Hd3dException;


public abstract class AbstractTypeHandler implements ITypeHandler
{
    protected Class<?> clazz;

    public AbstractTypeHandler()
    {}

    public AbstractTypeHandler(Class<?> clazz)
    {
        this.clazz = clazz;
    }

    public Class<?> getClazz()
    {
        return clazz;
    }

    public void setClazz(Class<?> clazz) throws Hd3dException
    {
        this.clazz = clazz;
        setHandler();
    }

    abstract void setHandler() throws Hd3dException;
}
