package fr.hd3d.services.resources.sheet;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.beanutils.WrapDynaBean;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TypeUtils;


public class Eq extends AbstractTypeHandler
{
    protected IOneOperandTypeHandler handler;

    public Eq()
    {}

    public Eq(Class<?> clazz) throws Hd3dException
    {
        super(clazz);
        this.handler = instance();
    }

    @Override
    void setHandler() throws Hd3dException
    {
        this.handler = instance();
    }

    public boolean match(Object columnValue, Object value, Object start, Object end, String field) throws Hd3dException
    {
        return handler.match(columnValue, value, field);
    }

    public IOneOperandTypeHandler instance() throws Hd3dException
    {
        if (Collection.class.isAssignableFrom(clazz))
            return new Eq_Collection();
        if (IBase.class.isAssignableFrom(clazz))
            return new Eq_Entity();
        if (Boolean.class.isAssignableFrom(clazz))
            return new Eq_Boolean();
        if (String.class.isAssignableFrom(clazz))
            return new Eq_String();
        if (Integer.class.isAssignableFrom(clazz))
            return new Eq_Integer();
        if (Long.class.isAssignableFrom(clazz))
            return new Eq_Long();
        if (Date.class.isAssignableFrom(clazz))
            return new Eq_Date();
        if (Enum.class.isAssignableFrom(clazz))
            return new Eq_Enum();
        throw new Hd3dException("Type of value not handled:" + clazz == null ? null : clazz.getCanonicalName());
    }

    public static class Eq_Collection implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return eq((Collection<?>) columnValue, value, field);
        }

        // @SuppressWarnings("unchecked")
        protected boolean eq(Collection<?> columnValue, Object value, String field) throws Hd3dException
        {
            if (CollectUtils.isEmpty(columnValue))
                return false;

            Object first = columnValue.iterator().next();
            if (first == null)
                return false;

            IOneOperandTypeHandler handler = (IOneOperandTypeHandler) new Eq(first.getClass()).instance();
            if (java.util.List.class.isInstance(columnValue))
            {
                /* case of ApprovalNotes */
                final java.util.List<?> list = (java.util.List<?>) columnValue;
                final boolean isApprovalNotes = list.size() == 2 && Boolean.class.isInstance(list.get(0))
                        && java.util.List.class.isInstance(list.get(1));
                if (isApprovalNotes)
                {
                    final java.util.List<?> approvalNotes = (java.util.List<?>) list.get(1);
                    if (CollectUtils.isNotEmpty(approvalNotes))
                    {
                        IApprovalNote firstApprovalNote = (IApprovalNote) approvalNotes.get(0);
                        handler = (IOneOperandTypeHandler) new Eq(firstApprovalNote.getClass()).instance();
                        return handler.match(firstApprovalNote, value, field);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    /* only test the first element */
                    return handler.match(((java.util.List<?>) columnValue).get(0), value, field);
                }
            }
            else
            {
                for (Object o : columnValue)
                {
                    if (handler.match(o, value, field))
                        return true;
                }
            }
            return false;
        }
    }

    public static class Eq_Entity implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return eq((IBase) columnValue, value, field);
        }

        protected boolean eq(IBase columnValue, Object value, String field) throws Hd3dException
        {
            if (columnValue == null)
            {
                return value == null;
            }
            else
            {
                Object colVal = new WrapDynaBean(columnValue).get(field);
                IOneOperandTypeHandler handler = (IOneOperandTypeHandler) new Eq(colVal.getClass()).instance();
                return handler.match(colVal, value, field);
            }
        }
    }

    public static class Eq_Boolean implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return eq((Boolean) columnValue, (Boolean) value, field);
        }

        protected boolean eq(Boolean columnValue, Boolean value, String field)
        {
            return columnValue == null ? value == null : columnValue.equals(value);
        }
    }

    public static class Eq_String implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return eq((String) columnValue, (String) value, field);
        }

        protected boolean eq(String columnValue, String value, String field)
        {
            return columnValue == null ? value == null : columnValue.equals(value);
        }
    }

    public static class Eq_Long implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return eq((Long) columnValue, (Long) value, field);
        }

        protected boolean eq(Long columnValue, Long value, String field)
        {
            return columnValue == null ? value == null : columnValue.equals(value);
        }
    }

    public static class Eq_Integer implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return eq((Integer) columnValue, TypeUtils.toInt(value), field);
        }

        protected boolean eq(Integer columnValue, Integer value, String field)
        {
            return columnValue == null ? value == null : columnValue.equals(value);
        }
    }

    public static class Eq_Date implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return eq((Date) columnValue, (Date) value, field);
        }

        protected boolean eq(Date columnValue, Date value, String field)
        {
            return columnValue == null ? value == null : columnValue.compareTo(value) == 0;
        }
    }

    public static class Eq_Enum implements IOneOperandTypeHandler
    {
        public boolean match(Object columnValue, Object value, String field) throws Hd3dException
        {
            return eq((Enum<?>) columnValue, (String) value, field);
        }

        protected boolean eq(Enum columnValue, String value, String field)
        {
            final Enum colVal = columnValue;
            if (colVal != null)
            {
                final Enum val = Enum.valueOf(colVal.getClass(), value);
                return colVal == val;
            }
            else
            {
                return value == null;
            }
        }
    }

}
