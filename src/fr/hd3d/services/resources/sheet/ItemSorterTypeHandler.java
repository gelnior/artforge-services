package fr.hd3d.services.resources.sheet;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.comparators.ComparatorChain;

import fr.hd3d.common.client.ItemConstraint;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Pair;
import fr.hd3d.utils.SheetUtils;


public class ItemSorterTypeHandler implements IItemSorter
{
    protected Map<String, String> queryMap;
    protected Class<?> clazz;
    protected String type;

    private ComparatorChain comparators = null;

    public ItemSorterTypeHandler()
    {}

    public ItemSorterTypeHandler(Class<?> clazz, String type)
    {
        this.clazz = clazz;
        this.type = type;
    }

    public Class<?> getClazz()
    {
        return clazz;
    }

    public String getType()
    {
        return type;
    }

    public void setClazz(Class<?> clazz)
    {
        this.clazz = clazz;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Map<String, String> getQueryMap()
    {
        return this.queryMap;
    }

    public void setQueryMap(Map<String, String> map)
    {
        this.queryMap = map;

    }

    public ComparatorChain getComparators()
    {
        return comparators;
    }

    public Object[][][] sort(Object[][][] values) throws Hd3dException
    {
        sort(values, getItemSorts());

        return values;
    }

    protected List<Long> getItemSorts() throws Hd3dQueryFilterException
    {
        /* Note: only the first itemSort is handled */
        return Hd3dJsonSerializer.<List<Long>> unserialize((String) getQueryMap().get(ItemConstraint.ITEMSORT));
    }

    @SuppressWarnings("unchecked")
    public void sort(Object[][][] values, List<Long> itemIds) throws Hd3dException
    {
        if (values != null && values.length > 0 && CollectUtils.isNotEmpty(itemIds))
        {
            /*----------------------------
             * Collect parameters
            -----------------------------*/
            comparators = new ComparatorChain();

            for (Long aColumn : itemIds)
            {
                final Long itemId = getItemIdFrom(aColumn);
                /* Note: the sort order is not in the comparator!!! */
                comparators.addComparator(getComparator(values, itemId), !isOrderedByAscending(aColumn));
            }

            if (comparators.size() > 0)
            {
                Arrays.sort(values, comparators);
            }
        }
    }

    private Comparator getComparator(Object[][][] values, Long itemId) throws Hd3dPersistenceException
    {
        Comparator ret = null;

        final Pair<IItem, Integer> tuple = SheetUtils.getColumnItemAndItemIndex(values, itemId);
        if (tuple != null)
        {
            final int columnIndex = tuple.getField_2();

            ret = new ItemSorterComparator(columnIndex);
        }
        return ret;
    }

    private Long getItemIdFrom(final Long itemId)
    {
        return Math.abs(itemId);
    }

    private boolean isOrderedByAscending(final Long itemId)
    {
        return itemId > 0;
    }

    private Class<?> getColumnClass(Object[][][] values, final int columnIndex)
    {
        Class<?> ret = null;

        for (int i = 0; i < values.length; i++)
        {
            Object columnValue = values[i][columnIndex][0];
            if (columnValue != null)
            {
                ret = columnValue.getClass();
                break;
            }
        }

        return ret;
    }
}
