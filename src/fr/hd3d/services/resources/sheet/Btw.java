package fr.hd3d.services.resources.sheet;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.beanutils.WrapDynaBean;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.TypeUtils;


public class Btw extends AbstractTypeHandler
{
    protected ITwoOperandTypeHandler handler;

    public Btw()
    {}

    public Btw(Class<?> clazz) throws Hd3dException
    {
        super(clazz);
        this.handler = instance();
    }

    @Override
    void setHandler() throws Hd3dException
    {
        this.handler = instance();
    }

    public ITwoOperandTypeHandler instance() throws Hd3dException
    {
        if (Collection.class.isAssignableFrom(clazz))
            return new Btw_Collection();
        if (IBase.class.isAssignableFrom(clazz))
            return new Btw_Entity();
        if (Long.class.isAssignableFrom(clazz))
            return new Btw_Long();
        if (Integer.class.isAssignableFrom(clazz))
            return new Btw_Integer();
        if (Date.class.isAssignableFrom(clazz))
            return new Btw_Date();
        throw new Hd3dException("Type of value not handled:" + clazz == null ? null : clazz.getCanonicalName());
    }

    public boolean match(Object columnValue, Object value, Object start, Object end, String field) throws Hd3dException
    {
        return handler.match(columnValue, start, end, field);
    }

    public static class Btw_Collection implements ITwoOperandTypeHandler
    {
        public boolean match(Object columnValue, Object start, Object end, String field) throws Hd3dException
        {
            return btw((Collection<?>) columnValue, start, end, field);
        }

        protected boolean btw(Collection<?> columnValue, Object start, Object end, String field) throws Hd3dException
        {
            if (CollectUtils.isEmpty(columnValue))
                return false;

            Object first = columnValue.iterator().next();
            if (first == null)
                return false;

            ITwoOperandTypeHandler handler = (ITwoOperandTypeHandler) new Btw(first.getClass()).instance();
            for (Object o : columnValue)
            {
                if (handler.match(o, start, end, field))
                    return true;
            }
            return false;
        }
    }

    public static class Btw_Entity implements ITwoOperandTypeHandler
    {
        public boolean match(Object columnValue, Object start, Object end, String field) throws Hd3dException
        {
            return btw((IBase) columnValue, start, end, field);
        }

        protected boolean btw(IBase columnValue, Object start, Object end, String field) throws Hd3dException
        {
            if (columnValue == null)
            {
                return start == null && end == null;
            }
            else
            {
                Object colVal = new WrapDynaBean(columnValue).get(field);
                ITwoOperandTypeHandler handler = (ITwoOperandTypeHandler) new Btw(colVal.getClass()).instance();
                return handler.match(colVal, start, end, field);
            }
        }
    }

    public static class Btw_Long implements ITwoOperandTypeHandler
    {
        public boolean match(Object columnValue, Object start, Object end, String field) throws Hd3dException
        {
            return btw((Long) columnValue, (Long) start, (Long) end, field);
        }

        protected boolean btw(Long columnValue, Long start, Long end, String field) throws Hd3dException
        {
            if (columnValue == null && start == null && end == null)
                return true;

            if (start == null || end == null)
                throw new Hd3dException("start and end must not be null!");

            return columnValue.compareTo(start) > 0 && columnValue.compareTo(end) < 0;
        }
    }

    public static class Btw_Integer implements ITwoOperandTypeHandler
    {
        public boolean match(Object columnValue, Object start, Object end, String field) throws Hd3dException
        {
            return btw((Integer) columnValue, TypeUtils.toInt(start), TypeUtils.toInt(end), field);
        }

        protected boolean btw(Integer columnValue, Integer start, Integer end, String field) throws Hd3dException
        {
            if (columnValue == null && start == null && end == null)
                return true;

            if (start == null || end == null)
                throw new Hd3dException("start and end must not be null!");

            return columnValue.compareTo(start) > 0 && columnValue.compareTo(end) < 0;
        }
    }

    public static class Btw_Date implements ITwoOperandTypeHandler
    {
        public boolean match(Object columnValue, Object start, Object end, String field) throws Hd3dException
        {
            return btw((Date) columnValue, (Date) start, (Date) end, field);
        }

        protected boolean btw(Date columnValue, Date start, Date end, String field) throws Hd3dException
        {
            if (columnValue == null && start == null && end == null)
                return true;

            if (start == null || end == null)
                throw new Hd3dException("start and end must not be null!");

            return columnValue.after(start) && columnValue.before(end);
        }
    }

}
