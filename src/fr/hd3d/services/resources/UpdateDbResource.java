package fr.hd3d.services.resources;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.EntityTaskLinkH;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.HibernateUtil;


/**
 * Class to update DB when needed (migration for instance), and when pure SQL can't do
 * 
 * @author Try LAM
 */
@Hd3dComment("Class to update DB when needed (migration for instance), and when pure SQL can't do")
public class UpdateDbResource<L, P> extends AbstractHd3dBaseResource
{
    public UpdateDbResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    /**
     * (non-Javadoc)
     * 
     * @see org.restlet.resource.Resource#getRepresentation(org.restlet.resource. Variant)
     */
    @Override
    public Representation get(Variant arg0)
    {
        final String cmdToRun = getQuery().getValues("cmd");

        try
        {
            init(cmdToRun);
        }
        catch (Hd3dTranslationException e)
        {
            e.printStackTrace();
            return new StringRepresentation(e.getMessage());
        }
        return new StringRepresentation("database updated...");
    }

    public static void init(String cmd) throws Hd3dPersistenceException
    {
        if ("EntityTaskLink".equalsIgnoreCase(cmd))
        {
            updateEntityTaskLinkH();
        }
    }

    public static void updateEntityTaskLinkH() throws Hd3dPersistenceException
    {
        run(new UpdateEntityTaskLink());
    }

    private static void run(IHibernateTransaction tx) throws Hd3dPersistenceException
    {
        Session session = HibernateUtil.currentSession();
        Transaction trx = session.beginTransaction();
        HibernateUtil.safeTransaction(tx);
        if (session.getFlushMode() == FlushMode.MANUAL)
        {
            session.flush();
        }
        trx.commit();
        trx = null;

        tx = null;
        HibernateUtil.closeSession();

    }

    /**
     * Database Initialization : persons, projects, tasks, activities and days
     * 
     * @author Thomas Eskénazi
     */
    private static class UpdateEntityTaskLink implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            Criteria criteria = HibernateUtil.currentSession().createCriteria(EntityTaskLinkH.class);
            criteria.setProjection(Projections.id());

            List<Long> entityTaskLinkIds = criteria.list();

            for (List<Long> partition : CollectUtils.partition(entityTaskLinkIds, Conf.getMaxRecords()))
            {
                List<IEntityTaskLink> links = Persistors.entitytasklink.getByIds(partition);
                for (IEntityTaskLink link : links)
                {
                    link.completePrePersistOrUpdate();
                }
                HibernateUtil.currentSession().flush();
            }
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
