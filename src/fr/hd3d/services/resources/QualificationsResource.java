package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILQualification;
import fr.hd3d.model.persistence.IQualification;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer qualification objects.<br>
 * URI : /qualifications/
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération des qualification de machines\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Création d'un qualification de machines\r\n" + "\r\nDELETE:N/A")
public class QualificationsResource extends AbstractHd3dCollection<ILQualification, IQualification>
{

    public QualificationsResource() throws Hd3dException
    {
        super(Persistors.qualification, Translators.qualification);
    }
}
