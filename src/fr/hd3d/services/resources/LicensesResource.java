package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILLicense;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer pool objects.<br>
 * URI : /licenses/
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération des fabricants\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:Création d'un fabricant\r\n"
        + "\r\nDELETE:N/A")
public class LicensesResource extends AbstractHd3dCollection<ILLicense, ILicense>
{
    public LicensesResource() throws Hd3dException
    {
        super(Persistors.license, Translators.license);
    }
}
