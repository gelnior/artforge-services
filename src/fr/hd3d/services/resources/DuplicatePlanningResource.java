package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PLANNING_MASTER_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ExtraLineH;
import fr.hd3d.model.persistence.impl.hibernate.PlanningH;
import fr.hd3d.model.persistence.impl.hibernate.TaskChangesH;
import fr.hd3d.model.persistence.impl.hibernate.TaskGroupH;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class DuplicatePlanningResource extends AbstractHd3dResource<ILPlanning, IPlanning>
{

    public DuplicatePlanningResource() throws Hd3dException
    {
        super(Persistors.planning, Translators.planning);
        allowMethods("POST");
    }

    private Map<Long, TaskGroupH> taskGroupsMap;
    private Map<Long, ExtraLineH> extraLinesMap;

    protected IPlanning newObject(Representation entity, Map<String, String> map) throws Hd3dException
    {
        if (false == initObject())
        {
            return null;
        }

        Long projectID = getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        String newPlanningName = getQuery().getValues("planningName");
        IProject project = Persistors.project.getById(projectID);
        // get master task group
        Criteria getMasterTaskGroupCriteria = HibernateUtil.currentSession().createCriteria(TaskGroupH.class);
        getMasterTaskGroupCriteria.add(Restrictions.eq("planning.id", object.getId()));
        getMasterTaskGroupCriteria.addOrder(Order.asc("taskGroup"));
        List<TaskGroupH> masterTaskGroupList = (List<TaskGroupH>) getMasterTaskGroupCriteria.list();
        // create new planning
        IPlanning newPlanning = new PlanningH(null, null, newPlanningName, object.getProject(), object.getStartDate(),
                object.getEndDate());
        newPlanning.setMaster(Boolean.FALSE);
        newPlanning.doPrePersist();
        Persistors.planning.persist(newPlanning);
        newPlanning.doPostPersist();
        this.taskGroupsMap = new HashMap<Long, TaskGroupH>();
        this.extraLinesMap = new HashMap<Long, ExtraLineH>();
        List<TaskGroupH> taskGroups = new ArrayList<TaskGroupH>();
        List<ExtraLineH> extraLines = new ArrayList<ExtraLineH>();
        // duplicate task groups
        for (Iterator<TaskGroupH> iterator = masterTaskGroupList.iterator(); iterator.hasNext();)
        {
            TaskGroupH taskGroupHibernateImpl = (TaskGroupH) iterator.next();
            TaskGroupH parentTaskGroupHibernateImpl = null;
            String taskGroupName = null;
            if (taskGroupHibernateImpl.getTaskGroup() != null)
            {
                parentTaskGroupHibernateImpl = (TaskGroupH) this.taskGroupsMap.get(taskGroupHibernateImpl
                        .getTaskGroup().getId());
                taskGroupName = taskGroupHibernateImpl.getName();
            }
            else
            {
                taskGroupName = project.getName() + " - " + newPlanningName;
            }
            TaskGroupH newTaskGroupHibernateImpl = new TaskGroupH(null, null, taskGroupHibernateImpl.getColor(),
                    taskGroupHibernateImpl.getEndDate(), taskGroupHibernateImpl.getStartDate(), taskGroupName,
                    parentTaskGroupHibernateImpl, newPlanning, taskGroupHibernateImpl.getDuration(),
                    taskGroupHibernateImpl.getFilter());
            Persistors.taskGroup.persist(newTaskGroupHibernateImpl);
            taskGroups.add(taskGroupHibernateImpl);
            this.taskGroupsMap.put(taskGroupHibernateImpl.getId(), newTaskGroupHibernateImpl);
        }
        if (taskGroups.size() > 0)
        {
            Criteria extraLineMasterCriteria = HibernateUtil.currentSession().createCriteria(ExtraLineH.class);
            extraLineMasterCriteria.add(Restrictions.in("taskGroup", taskGroups));
            List<ExtraLineH> extraLineList = extraLineMasterCriteria.list();
            for (Iterator<ExtraLineH> iterator = extraLineList.iterator(); iterator.hasNext();)
            {
                ExtraLineH extraLineHibernateImpl = (ExtraLineH) iterator.next();
                ExtraLineH implToSave = new ExtraLineH();
                TaskGroupH groupHibernateImpl = this.taskGroupsMap.get(CollectUtils.nullSafeId(extraLineHibernateImpl
                        .getTaskGroup()));
                implToSave.setHiddenPerson(extraLineHibernateImpl.getHiddenPerson());
                implToSave.setName(extraLineHibernateImpl.getName());
                implToSave.setTaskGroup(groupHibernateImpl);
                Persistors.extraline.persist(implToSave);
                extraLineList.add(implToSave);
                this.extraLinesMap.put(extraLineHibernateImpl.getId(), implToSave);
            }
        }
        if (extraLines.size() > 0)
        {
            // duplicate master tasks
            Criteria criteria = HibernateUtil.currentSession().createCriteria(TaskH.class);
            criteria.add(Restrictions.eq("project.id", projectID));
            criteria.add(Restrictions.isNotNull("worker"));
            criteria.add(Restrictions.isNotNull("startDate"));
            criteria.add(Restrictions.isNotNull("endDate"));
            List<TaskH> masterTaskList = criteria.list();
            for (Iterator<TaskH> iterator = masterTaskList.iterator(); iterator.hasNext();)
            {
                TaskH taskHibernateImpl = (TaskH) iterator.next();
                IExtraLine extraLine = null;
                if (taskHibernateImpl.getExtraLine() != null)
                {
                    extraLine = this.extraLinesMap.get(taskHibernateImpl.getExtraLine().getId());
                }
                TaskChangesH changesHibernateImpl = new TaskChangesH(taskHibernateImpl.getWorker(), taskHibernateImpl
                        .getStartDate(), taskHibernateImpl.getEndDate(), taskHibernateImpl.getDuration(), null,
                        newPlanning, taskHibernateImpl);
                changesHibernateImpl.setExtraLine(extraLine);
                Persistors.taskChanges.persist(changesHibernateImpl);
            }
        }
        return newPlanning;
    }

    @Override
    protected void doPrePersist(Representation entity, IPlanning planning) throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    @Override
    protected void doPersist(IPlanning object) throws Hd3dPersistenceException
    {}

    @Override
    protected IPlanning initializeObject(LockMode lockMode) throws Hd3dException
    {
        Long masterID = getResourceContext().getIdFromUrl(PLANNING_MASTER_ID_ATTRIBUTE);
        IPlanning obj = Persistors.planning.getById(getResourceContext(), masterID, lockMode);
        createDynMetaDataValue(obj);
        return obj;
    }
}
