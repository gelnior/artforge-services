package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.DYNAMIC_PATH_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILDynamicPath;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * 
 * @author HD3D
 * 
 */
public class DynamicPathResource extends AbstractHd3dResource<ILDynamicPath, IDynamicPath>
{

    public DynamicPathResource() throws Hd3dException
    {
        super(Persistors.dynamicpath, Translators.dynamicpath);
    }

    @Override
    protected IDynamicPath initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(DYNAMIC_PATH_ID_ATTRIBUTE, lockMode);
    }
}
