package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetCastingSequencesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Retourne la liste des constituants qui interviennent dans une sequence {sequenceID}\r\n"
        + "\r\nPUT:N/A\r\n" + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class CastingSequenceResource extends AbstractHd3dCollection<ILConstituent, IConstituent>
{
    public CastingSequenceResource() throws Hd3dException
    {
        super(Persistors.constituent, Translators.constituent);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetCastingSequencesCmd(getResourceContext()).wantTotalSize());

    }
}
