package fr.hd3d.services.resources;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.ResourceConst;
import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.persistence.impl.hibernate.StepH;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.ServerResourceUtils;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des steps liés à un projet\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:N/A")
public class ProjectStepsResource<ILStep, IStep> extends StepsResource
{
    public ProjectStepsResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Representation get(Variant variant)
    {
        final Long projectId = getResourceContext().getIdFromUrl(ServicesURI.PROJECT_ID_ATTRIBUTE);

        BooleanQuery bq = new BooleanQuery();
        TermQuery tq = new TermQuery(new Term("wo_project_id", String.valueOf(projectId)));
        bq.add(new BooleanClause(tq, BooleanClause.Occur.MUST));

        FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
        org.hibernate.Transaction tx = fullTextSession.beginTransaction();
        org.hibernate.Query luceneQuery = fullTextSession.createFullTextQuery(bq, StepH.class);

        List<IStep> result = luceneQuery.list();

        tx.commit();
        tx = null;

        final List<ILStep> lightweightList = new ArrayList<ILStep>(result.size());
        try
        {
            lightweightList.addAll((List<ILStep>) ServerResourceUtils.dynaBeanPropSafeConvert(result,
                    getResourceContext()));
        }
        catch (Hd3dTranslationException e)
        {
            error(Status.SERVER_ERROR_INTERNAL, e, ResourceConst.ERROR_OCCURED);
        }
        catch (Hd3dException e)
        {
            error(Status.SERVER_ERROR_INTERNAL, e, ResourceConst.ERROR_OCCURED);
        }

        Representation r = null;
        r = RessourceSerializer.getRepresentation(variant, lightweightList);
        r.setCharacterSet(CharacterSet.UTF_8);
        return r;

    }

    // @Override
    // protected String getNewResourceIdentifier(Object object)
    // {
    // return null;
    // }
}
