/**
 * 
 */
package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;


/**
 * Access for modification and delete of an identified task activity. When a task activity is deleted, the linked task
 * actual start date is update with the oldest activity date for this task.
 * 
 * @author HD3D
 */
@Hd3dComment("GET: Retrieve task activity where ID is equal to {taskActivityID}\r\n"
        + "\r\nPUT: Modify task activity where ID is equal to {taskActivityID} \r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE: Delete task activity where ID is equal to {taskActivityID}")
public class ProjectTaskActivityResource extends BaseTaskActivityResource
{

    public ProjectTaskActivityResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected void doPreUpdate(Representation entity, ILTaskActivity taskActivity) throws Hd3dException
    {
        IProject project = Persistors.project.getById(getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE));

        if (project.getId().longValue() == taskActivity.getTask().getProject().getId().longValue())
        {
            super.doPreUpdate(entity, taskActivity);
        }
        else
        {
            throw new Hd3dException("You cannot modify activity from another project.");
        }
    }

    @Override
    protected void doPreDelete() throws Hd3dException
    {
        IProject project = Persistors.project.getById(getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE));

        if (project.getId().longValue() == object.getTask().getProject().getId().longValue())
        {
            super.doPreDelete();
        }
        else
        {
            throw new Hd3dException("You cannot delete activity from another project.");
        }
    }

    @Override
    protected void doPostDelete() throws Hd3dException
    {
        super.doPostDelete();
        this.updateTaskActualStartDate();
    }

}
