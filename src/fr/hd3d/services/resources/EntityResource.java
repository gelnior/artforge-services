package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ENTITY_ID_ATTRIBUTE;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.Sheet;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.annotation.Hd3dExposed;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.services.resources.collectionquery.AbstractCollectionQuery;
import fr.hd3d.services.resources.collectionquery.CollectionQueriesMap;
import fr.hd3d.services.resources.collectionquery.StepsByTaskTypeCollectionQuery;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


/**
 * Return the attributes of the given entity class
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 * 
 */
@Hd3dComment("GET:retourne les attributs de l'interface et leur type\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:N/A")
public class EntityResource<L, P> extends AbstractHd3dBaseResource
{
    final static String MAP_KEY_TYPE = "type";
    final static String MAP_KEY_NAME = "name";
    final static String MAP_KEY_QUERY = "query";
    final static String MAP_KEY_DATATYPE = "dataType";
    final static String MAP_KEY_TRANSFORMERPARAM = "param";

    HashSet<String> hiddenProperties = new HashSet<String>();

    public EntityResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
        hiddenProperties.add("approvalNotes");
        hiddenProperties.add("entityTaskLinks");
        hiddenProperties.add("internalStatus");
        hiddenProperties.add("tagParents");
        hiddenProperties.add("taskable");
        hiddenProperties.add("dynMetaDataValues");
        hiddenProperties.add("internalUUID");
    }

    @SuppressWarnings( { "unchecked" })
    @Override
    public Representation get(Variant variant)
    {
        try
        {
            final String entityName = (String) getRequest().getAttributes().get(ENTITY_ID_ATTRIBUTE);
            final String projectId = getQuery().getValuesMap().get(Const.PROJECTID);

            Set ret = new HashSet();

            final String className = getClassName(entityName);
            if (className == null)
                return getResponseEntity();

            /* retrieve all the upper classes */
            ArrayList<String> classHierarchy = new ArrayList(EntitiesMaps.classUpHierarchyNames(className));
            classHierarchy.add(className);// include the class itself
            classHierarchy.trimToSize();

            for (final String aClassName : classHierarchy)
            {
                final Class<?> clazz = getClazz(aClassName);

                /* attributes */
                ret.addAll(getAttributesMap(clazz));
                /* methods */
                ret.addAll(getMethodsMap(clazz));
                // TODO class level metadata
                ret.addAll(getMetadataMap(clazz, entityName));
            }

            /* Collection Queries */
            ret.addAll(getCollectionQueriesMap(entityName));

            if (!StringUtils.isBlank(projectId))
                ret.addAll(getTasktypesMap(projectId, entityName));

            /* build the representation */
            Representation r = RessourceSerializer.getRepresentation(variant, new ArrayList(ret));
            r.setCharacterSet(CharacterSet.UTF_8);
            return r;
        }
        finally
        {
            cleanup();
        }
    }

    /**
     * given an Entity class, retrieves attributes info
     * 
     * @param clazz
     * @return
     */
    private Set<Map<String, String>> getAttributesMap(Class<?> clazz)
    {

        Set<Map<String, String>> ret = new HashSet<Map<String, String>>();
        if (clazz != null)
        {
            PropertyDescriptor[] props = PropertyUtils.getPropertyDescriptors(clazz);
            for (final PropertyDescriptor p : props)
            {
                final String attributeName = p.getName();
                if (hiddenProperties.contains(attributeName))
                    continue;

                Map<String, String> map = new HashMap<String, String>(5);
                map.put(MAP_KEY_TYPE, Sheet.ITEMTYPE_ATTRIBUTE);// attribute
                map.put(MAP_KEY_NAME, unCamelize(attributeName));
                map.put(MAP_KEY_QUERY, attributeName);// attribute name
                map.put(MAP_KEY_DATATYPE, entityName(p.getPropertyType()));// attribute type

                ret.add(map);
            }
        }

        return ret;
    }

    /**
     * given an Entity class, retrieves methods info
     * 
     * @param clazz
     * @return
     */
    private Set<Map<String, String>> getMethodsMap(Class<?> clazz)
    {
        Set<Map<String, String>> ret = new HashSet<Map<String, String>>();
        if (clazz != null)
        {
            for (final Method method : clazz.getMethods())
            {
                if (method.getName().startsWith("get") || method.getAnnotation(Hd3dExposed.class) == null)
                    continue;

                Map<String, String> map = new HashMap<String, String>(5);
                map.put(MAP_KEY_TYPE, Sheet.ITEMTYPE_METHOD);
                map.put(MAP_KEY_NAME, unCamelize(method.getName()));
                map.put(MAP_KEY_QUERY, method.getName());
                map.put(MAP_KEY_DATATYPE, method.getGenericReturnType().toString().replaceAll("class ", ""));
                if (method.getGenericParameterTypes().length == 1)
                {
                    map.put("parameter", method.getGenericParameterTypes()[0].toString().replaceAll("class ", ""));
                }

                ret.add(map);
            }
        }

        return ret;
    }

    /**
     * given an Entity class, retrieves metadata info
     * 
     * @param clazz
     * @return
     */
    private Set<Map<String, String>> getMetadataMap(Class<?> clazz, String entityName)
    {
        Set<Map<String, String>> ret = new HashSet<Map<String, String>>();
        List<IClassDynMetaDataType> metaData = getMetaData(entityName);
        for (final IClassDynMetaDataType classDyn : metaData)
        {
            Map<String, String> map = new HashMap<String, String>(5);
            map.put(MAP_KEY_TYPE, Sheet.ITEMTYPE_METADATA);
            // le nom de l'attribut dynamique a été rentré par l'utilisateur donc on suppose qu'il est
            // déjà bien formaté donc pas besoin d'appliqué la méthode unCamelize
            map.put(MAP_KEY_NAME, classDyn.getName());
            final IDynMetaDataType dynMetaDataType = classDyn.getDynMetaDataType();
            if (dynMetaDataType != null)
            {
                if (fr.hd3d.common.client.Const.DYNMETADATATYPE_SIMPLE.equals(dynMetaDataType.getNature()))
                {
                    map.put(MAP_KEY_DATATYPE, dynMetaDataType.getType());
                }
                else
                {
                    map.put(MAP_KEY_DATATYPE, fr.hd3d.common.client.Const.JAVA_UTIL_LIST);
                }
            }
            else
            {
                map.put(MAP_KEY_DATATYPE, null);
            }
            map.put(MAP_KEY_QUERY, classDyn.getId().toString());

            ret.add(map);
        }

        return ret;
    }

    private Set<Map<String, String>> getCollectionQueriesMap(String entityName)
    {
        Set<Map<String, String>> ret = new HashSet<Map<String, String>>();

        /* case of collection queries that apply on all type of entity */
        for (final AbstractCollectionQuery collectionQuery : CollectionQueriesMap.appliedEntitiesMap.values())
        {
            if (collectionQuery.getAppliedEntities().contains(Const.ALL_CLASSES))
            {
                /* excluded */
                if (collectionQuery.getClass().equals(StepsByTaskTypeCollectionQuery.class))
                    continue;// will be displayed by getTasktypesMap() because more parameters are needed

                /* ok to display */
                Map<String, String> map = new HashMap<String, String>(4);
                map.put(MAP_KEY_TYPE, Sheet.ITEMTYPE_COLLECTIONQUERY);
                map.put(MAP_KEY_NAME, collectionQuery.getName());
                map.put(MAP_KEY_QUERY, collectionQuery.getClass().getCanonicalName());
                map.put(MAP_KEY_DATATYPE, collectionQuery.getReturnType());

                ret.add(map);
            }
        }

        /* for each collection query applying on specific entities */
        Collection<AbstractCollectionQuery> collectionQueries = CollectionQueriesMap.appliedEntitiesMap.get(entityName);
        if (CollectionUtils.isNotEmpty(collectionQueries))
            for (final AbstractCollectionQuery collectionQuery : collectionQueries)
            {
                Map<String, String> map = new HashMap<String, String>(4);
                map.put(MAP_KEY_TYPE, Sheet.ITEMTYPE_COLLECTIONQUERY);
                map.put(MAP_KEY_NAME, collectionQuery.getName());
                map.put(MAP_KEY_QUERY, collectionQuery.getClass().getCanonicalName());
                map.put(MAP_KEY_DATATYPE, collectionQuery.getReturnType());

                ret.add(map);
            }

        return ret;
    }

    /**
     * For each TaskType bound to the given project, returns map containing
     * 
     * @param projectIdStr
     * @param entityName
     * @return
     */
    private Set<Map<String, String>> getTasktypesMap(String projectIdStr, String entityName)
    {
        Set<Map<String, String>> ret = new HashSet<Map<String, String>>();

        if (StringUtils.isBlank(projectIdStr))
            return ret;

        final Long projectId = NumberUtils.toLong(projectIdStr);
        if (BaseH.isNotValidId(projectId))
            return ret;
        IProject project;

        Set<ITaskType> taskTypes;
        try
        {
            project = Persistors.project.getById(projectId);
            taskTypes = new HashSet<ITaskType>();
            for (ITaskType taskType : project.getTaskTypes())
            {
                if (entityName.equals(taskType.getEntityName()))
                    taskTypes.add(taskType);
            }

        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            return ret;
        }

        for (final ITaskType t : taskTypes)
        {
            Map<String, String> map = new HashMap<String, String>(5);
            map.put(MAP_KEY_TYPE, Sheet.ITEMTYPE_COLLECTIONQUERY);// attribute
            map.put(MAP_KEY_NAME, t.getName());
            map.put(MAP_KEY_QUERY, StepsByTaskTypeCollectionQuery.class.getCanonicalName());// attribute name
            map.put(MAP_KEY_DATATYPE, fr.hd3d.common.client.Const.JAVA_LANG_LONG);// attribute type
            map.put(MAP_KEY_TRANSFORMERPARAM, String.valueOf(t.getId()));// attribute type

            ret.add(map);
        }

        return ret;
    }

    private String getClassName(String entityName)
    {
        String className = EntitiesMaps.withEntityName(entityName).getFullClassName();
        if (className == null)
        {
            /* if not found, try with capitalized entityName (entityName may be provided uncapitalized) */
            className = EntitiesMaps.withEntityName(WordUtils.capitalize(entityName)).getFullClassName();
        }
        return className;
    }

    @SuppressWarnings("unchecked")
    private Class<?> getClazz(String className)
    {
        Class<IBase> clazz;
        if (className.equals("*"))
        {
            clazz = IBase.class;
        }
        else
        {
            clazz = (Class<IBase>) EntitiesMaps.withEntityName(EntitiesMaps.withClassName(className).getEntityName())
                    .getInterface();
            if (clazz == null)
            {
                try
                {
                    clazz = ClassUtils.getClass(className);
                }
                catch (ClassNotFoundException e)
                {
                    Log.LOGGER.error("Cannot find entity identified by class name=" + className);
                    clazz = null;
                }
            }
        }
        return clazz;
    }

    /**
     * method simple pour récupérer la liste des attributs dynamique disponible elle est simple pour le moment mais elle
     * se complexifiera lorsque l'on voudra par exemple récuperer les metadata pour les constituents d'un projet précis
     * ( bientot :p )
     * 
     * @param className
     * @return
     */
    private List<IClassDynMetaDataType> getMetaData(String entityName)
    {
        List<IClassDynMetaDataType> ret = new ArrayList<IClassDynMetaDataType>();
        try
        {
            ret.addAll(Persistors.classdynmetadatatype.getByValue("className", entityName));
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
        return ret;
    }

    private String unCamelize(final String keyName)
    {
        Pattern p = Pattern.compile("\\p{Lu}");
        Matcher m = p.matcher(keyName);
        StringBuffer sb = new StringBuffer();
        int last = 0;
        while (m.find())
        {
            if (m.start() != last + 1)
            {
                m.appendReplacement(sb, " " + m.group());
            }
            last = m.start();
        }
        m.appendTail(sb);
        sb.trimToSize();
        String result = sb.toString().toLowerCase();
        result = result.substring(0, 1).toUpperCase() + result.substring(1);
        return result;
    }

    private String entityName(Class<?> propertyType)
    {
        final String className = propertyType.getCanonicalName();
        if (!IBase.class.isAssignableFrom(propertyType))
            return className;

        String entityName = EntitiesMaps.withInterfaceName(className).getEntityName();
        if (entityName == null)
        {
            entityName = EntitiesMaps.withClassName(className).getEntityName();
        }

        return entityName;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
