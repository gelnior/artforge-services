package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ENTITYTASKLINK_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILEntityTaskLink;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class EntityTaskLinkResource extends AbstractHd3dResource<ILEntityTaskLink, IEntityTaskLink>
{

    public EntityTaskLinkResource() throws Hd3dException
    {
        super(Persistors.entitytasklink, Translators.entitytasklink);
    }

    @Override
    protected IEntityTaskLink initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ENTITYTASKLINK_ID_ATTRIBUTE, lockMode);
    }
}
