package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectPersonActivityCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.UniqueResultQuery;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


/**
 * A resource to access to specific person's actvities in a project.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'une activite\r\n" + "\r\nPUT:Mise a jour d'une activité\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Suppression d'une tactivité")
public class ProjectPersonActivityResource extends AbstractHd3dResource<ILActivity, IActivity>
{
    public ProjectPersonActivityResource() throws Hd3dException
    {
        super(Persistors.activity, Translators.activity);
    }

    @Override
    protected IActivity initializeObject(LockMode lockMode) throws Hd3dException
    {
        final UniqueResultQuery<ILActivity, IActivity> cmd = new GetProjectPersonActivityCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        IActivity obj = (IActivity) cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }
}
