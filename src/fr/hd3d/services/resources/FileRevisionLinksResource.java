package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevisionLink;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class FileRevisionLinksResource extends AbstractHd3dCollection<ILFileRevisionLink, IFileRevisionLink>
{

    public FileRevisionLinksResource() throws Hd3dException
    {
        super(Persistors.filerevisionlink, Translators.filerevisionlink);
    }
}
