// package fr.hd3d.services.resources;
//
// import static fr.hd3d.common.client.ServicesURI.ASSETREVISION_ID_ATTRIBUTE;
//
// import java.util.Map;
//
// import org.restlet.representation.Representation;
//
// import fr.hd3d.exception.Hd3dException;
// import fr.hd3d.model.lightweight.ILFileRevision;
// import fr.hd3d.model.persistence.IAssetRevision;
// import fr.hd3d.model.persistence.IAssetRevisionFileRevision;
// import fr.hd3d.model.persistence.IFileRevision;
// import fr.hd3d.model.persistence.Persistors;
// import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
// import fr.hd3d.model.persistence.impl.hibernate.query.GetFileRevisionsByAssetRevision;
// import fr.hd3d.model.translator.Translators;
//
//
// public class AssetAbstractRevisionFileRevisionsResource extends AbstractHd3dCollection<ILFileRevision, IFileRevision>
// {
//
// public AssetAbstractRevisionFileRevisionsResource() throws Hd3dException
// {
// super(Persistors.filerevision, Translators.filerevision);
// }
//
// @SuppressWarnings("unchecked")
// @Override
// protected ResultCollection getCollectionForRepresentation() throws Hd3dException
// {
// return doGetCollection(new GetFileRevisionsByAssetRevision(getResourceContext()));
// }
//
// @Override
// protected IFileRevision newObject(Representation entity, Map<String, String> map) throws Hd3dException
// {
// IFileRevision f = super.newObject(entity, map);
//
// IAssetRevision asset = Persistors.assetrevision.getById(getResourceContext().getIdFromUrl(
// ASSETREVISION_ID_ATTRIBUTE));
// IAssetRevisionFileRevision relation = Persistors.assetrevisionfilerevision.newInstance();
//
// relation.setAssetKey(asset.getKey());
// relation.setAssetVariation(asset.getVariation());
// relation.setAssetRevision(asset.getRevision());
//
// relation.setFileKey(f.getKey());
// relation.setFileVariation(f.getVariation());
// relation.setFileRevision(f.getRevision());
//
// relation.doPrePersist();
// Persistors.assetrevisionfilerevision.persist(relation);
// relation.doPostPersist();
//
// return f;
// }
// }
