package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.DEVICE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILDevice;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single software object (id ford parameter) <br>
 * URI : /softwares/{id}
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'un périphérique\r\n" + "\r\nPUT:Modification d'un périphérique\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:Désactivation d'un périphérique")
public class DeviceResource extends AbstractHd3dResource<ILDevice, IDevice>
{
    @Override
    protected IDevice initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(DEVICE_ID_ATTRIBUTE, lockMode);
    }

    public DeviceResource() throws Hd3dException
    {
        super(Persistors.device, Translators.device);
    }
}
