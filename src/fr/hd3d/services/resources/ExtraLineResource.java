package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.EXTRA_LINES_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILExtraLine;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author michael.guiral
 * 
 */
@Hd3dComment("GET:Récupération d'une extra line\r\n" + "\r\nPUT:Changement d'une extra linet\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Suppression d'une extra line")
public class ExtraLineResource extends AbstractHd3dResource<ILExtraLine, IExtraLine>
{
    public ExtraLineResource() throws Hd3dException
    {
        super(Persistors.extraline, Translators.extraLine);
    }

    @Override
    protected IExtraLine initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(EXTRA_LINES_ID_ATTRIBUTE, lockMode);
    }
}
