package fr.hd3d.services.resources;

import java.util.ArrayList;
import java.util.List;

import org.restlet.representation.Representation;

import fr.hd3d.common.client.Persist;
import fr.hd3d.exception.Hd3dAuthorizationException;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.translator.json.JSonFormSerializer;
import fr.hd3d.utils.Log;


/**
 * 
 * @author florent-della-valle
 * 
 */
public class RolesResource extends AbstractHd3dCollection<ILRole, IRole>
{

    public RolesResource() throws Hd3dException
    {
        super(Persistors.role, Translators.role);
    }

    protected List<IRole> doPreUpdate(Representation entity, ResourceContext<?, ?> context) throws Hd3dException
    {
        List<ILRole> lObjects = JSonFormSerializer.<ILRole> deserializeList(entity);
        List<IRole> objects = new ArrayList<IRole>(lObjects.size());

        // PreUpdate
        for (ILRole light : lObjects)
        {
            if (light == null)
            {
                Log.LOGGER.warn("*** LRole is null!");
                continue;
            }

            IRole object = (IRole) getResourceContext().getPersistor().getById(light.getId());

            if (object == null)
            {
                Log.LOGGER.warn("*** no Role found with id=" + light.getId());
                continue;
            }

            // checks user permissions on object
            if (object.canUpdate() && object.canBeUpdatedFrom(light))
            {

                getResourceContext().getTranslator().updateFromLightweight(light, object, context);

                objects.add(object);
                object.doPreUpdate(light);
            }
            else
            {
                throw new Hd3dAuthorizationException("user may not update " + object.entityName() + " with id "
                        + object.getId());
            }
        }
        executeScripts(objects.get(0), Persist.PREUPDATE, getRequest().getResourceRef().toString());

        // clear
        for (ILRole light : lObjects)
            light.clear();
        lObjects.clear();
        lObjects = null;

        return objects;
    }

}
