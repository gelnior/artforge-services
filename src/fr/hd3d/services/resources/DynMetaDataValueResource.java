package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.DYNMETADATAVALUE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILDynMetaDataValue;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class DynMetaDataValueResource extends AbstractHd3dResource<ILDynMetaDataValue, IDynMetaDataValue>
{
    public DynMetaDataValueResource() throws Hd3dException
    {
        super(Persistors.dynmetadatavalue, Translators.dynmetadatavalue);
    }

    @Override
    protected IDynMetaDataValue initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(DYNMETADATAVALUE_ID_ATTRIBUTE, lockMode);
    }
}
