package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.common.client.ServicesURI.FILEREVISIONS;
import static fr.hd3d.common.client.ServicesURI.FILEREVISION_ID_ATTRIBUTE;

import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.FileRevisionUtils;
import fr.hd3d.utils.Log;


@Hd3dComment("Webservice d'upload de fichiers. <br>\n" + "POST/Multipart: file= fichier à uploader; ")
public class FileRevisionUploadResource extends AbstractHd3dUploadBaseResource<ILFileRevision, IFileRevision>
{
    private final static String FILE = "file";
    private final static String VARIATION = "variation";
    private final static String NEW_FILE_REVISION = "newFileRevision";

    /**
     * 
     * @param context
     * @param request
     * @param response
     * @return
     * @throws Hd3dException
     */
    public FileRevisionUploadResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected void parseUploadedFiles(Map<String, Object> resultMap, FileItem fileItem)
    {
        if (resultMap == null || fileItem == null || fileItem.isFormField())
            return;

        if (fileItem.getFieldName().equals(FILE))
        {
            resultMap.put(FILE, fileItem);
        }
    }

    @Override
    protected void parseTextFields(Map<String, Object> resultMap, FileItem fileItem)
    {
        if (resultMap == null || fileItem == null || !fileItem.isFormField())
            return;

        final String fieldName = fileItem.getFieldName();
        final String fieldValue = fileItem.getString();

        if (fieldName.equals(VARIATION))
        {
            resultMap.put(VARIATION, fieldValue);
        }
    }

    @Override
    protected void process(Map<String, Object> resultMap)
    {
        /* collect processed results */
        FileItem file = (FileItem) resultMap.get(FILE);
        String variation = (String) resultMap.get(VARIATION);

        try
        {
            IFileRevision currentFile = initializeObject();
            IFileRevision fileRevision = FileRevisionUtils.persistUploadedFile(file, currentFile.getKey(), variation,
                    1, EFileState.getDefault());

            resultMap.put(NEW_FILE_REVISION, fileRevision);
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error("*** Error: EXCEPTION!!\n");
            error(Status.CLIENT_ERROR_BAD_REQUEST, e, ERROR_OCCURED);
        }
    }

    @Override
    protected Representation buildRepresentation(Map<String, Object> resultMap)
    {
        IFileRevision newFileRevision = (IFileRevision) resultMap.get(NEW_FILE_REVISION);

        Representation rep = null;
        if (newFileRevision == null)
        {
            rep = new StringRepresentation("0", MediaType.TEXT_PLAIN);
        }
        else
        {
            rep = new StringRepresentation(String.valueOf(newFileRevision.getId()));
            rep.setLocationRef(this.getNewResourceIdentifier(newFileRevision));
        }

        return rep;
    }

    protected IFileRevision initializeObject() throws Hd3dException
    {
        return Persistors.filerevision.getById(getResourceContext().getIdFromUrl(FILEREVISION_ID_ATTRIBUTE));
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        StringBuilder buf = new StringBuilder(getRequest().getResourceRef().getBaseRef().toString());
        buf.delete(buf.lastIndexOf('/' + FILEREVISIONS), buf.length());
        buf.append('/').append(((IBase) object).getId());
        return buf.toString();
    }
}
