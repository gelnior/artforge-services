package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILEntityTaskLink;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectEntityTaskLinksCmd;
import fr.hd3d.model.translator.Translators;


public class ProjectEntityTaskLinksResource extends AbstractHd3dCollection<ILEntityTaskLink, IEntityTaskLink>
{

    public ProjectEntityTaskLinksResource() throws Hd3dException
    {
        super(Persistors.entitytasklink, Translators.entitytasklink);
    }

    protected ResultCollection<IEntityTaskLink> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectEntityTaskLinksCmd(getResourceContext()).wantTotalSize());
    }
}
