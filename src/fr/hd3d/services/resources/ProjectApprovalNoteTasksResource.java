package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.APPROVALNOTE_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectApprovalNoteTasksCmd;
import fr.hd3d.model.translator.Translators;


/**
 * Return tasks linked to a given approval note.
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération de la liste des taches d'un projet\r\n")
public class ProjectApprovalNoteTasksResource extends AbstractHd3dCollection<ILTask, ITask>
{
    public ProjectApprovalNoteTasksResource() throws Hd3dException
    {
        super(Persistors.task, Translators.task);
        allowMethods("GET");
    }

    @Override
    protected ResultCollection<ITask> getCollectionForRepresentation() throws Hd3dException
    {
        Long projectId = getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        IApprovalNote note = Persistors.approvalnote.getById(getResourceContext().getIdFromUrl(
                APPROVALNOTE_ID_ATTRIBUTE));
        IApprovalNoteType noteType = note.getApprovalNoteType();

        if (projectId != null && projectId.longValue() == noteType.getProject().getId().longValue())
        {
            return doGetCollection(new GetProjectApprovalNoteTasksCmd(getResourceContext()).wantTotalSize());
        }
        else
        {
            throw new Hd3dException(
                    "You cannot retrieve task from an approval note that does not correspond to current project.");
        }
    }
}
