package fr.hd3d.services.resources;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ConstituentH;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.utils.HibernateUtil;


/**
 * Return all the available business entities
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:retourne la liste des interfaces des classes métiers, sans les attributs\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class LuceneTestResource<L, P> extends AbstractHd3dBaseResource
{
    public LuceneTestResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    public Representation get(Variant variant)
    {
        try
        {
            // build the representation
            Representation r = null;
            /* extract from indexes some field values */
            try
            {
                IConstituent pat = Persistors.constituent.getObjectByValue_NoPermCheck("hook", "PAT");

                IConstituent stan = Persistors.constituent.getObjectByValue_NoPermCheck("hook", "STAN");
                IConstituent jeanluc = Persistors.constituent.getObjectByValue_NoPermCheck("hook", "JEANLUC");
                IConstituent chichi = Persistors.constituent.getObjectByValue_NoPermCheck("hook", "CHICHI");
                IConstituent stef = Persistors.constituent.getObjectByValue_NoPermCheck("hook", "STEPHANIE");
                IConstituent emilie = Persistors.constituent.getObjectByValue_NoPermCheck("hook", "EMILIE");
                IConstituent marthe = Persistors.constituent.getObjectByValue_NoPermCheck("hook", "MARTHE");

                if (pat == null)
                    return null;

                List<Long> ids = new ArrayList<Long>();
                ids.add(pat.getId());
                ids.add(stan.getId());
                ids.add(jeanluc.getId());
                ids.add(chichi.getId());
                ids.add(stef.getId());
                ids.add(emilie.getId());
                ids.add(marthe.getId());

                // query
                BooleanQuery bq = new BooleanQuery();
                for (final Long id : ids)
                {
                    TermQuery tq = new TermQuery(new Term("id", String.valueOf(id)));
                    bq.add(new BooleanClause(tq, BooleanClause.Occur.SHOULD));
                }

                FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
                FullTextQuery query = fullTextSession.createFullTextQuery(bq, ConstituentH.class);

                // query.setProjection(FullTextQuery.THIS, FullTextQuery.DOCUMENT_ID, FullTextQuery.DOCUMENT);
                query.setProjection(FullTextQuery.DOCUMENT);
                Collection<Object[]> lista = query.list();

                for (Object[] zlozony : lista)
                {
                    Document document = (Document) zlozony[0];
                    org.apache.lucene.document.Field hook = document.getField("hook");
                    org.apache.lucene.document.Field categories_id = document.getField("categories_id");
                    org.apache.lucene.document.Field desc = document.getField("description");
                    System.out.println(hook.stringValue() + "'s categories_id is " + categories_id.stringValue()
                            + ", desc is:" + desc.stringValue());
                }

                // final SearchFactory searchFactory = fullTextSession.getSearchFactory();
                // DirectoryProvider constituentProvider = searchFactory.getDirectoryProviders(ConstituentH.class)[0];
                //
                // ReaderProvider readerProvider = searchFactory.getReaderProvider();
                // IndexReader reader = readerProvider.openReader(constituentProvider);
                //
                // try
                // {
                // // do read-only operations on the reader
                // // reader.
                // }
                // finally
                // {
                // readerProvider.closeReader(reader);
                // }
                r = RessourceSerializer.getRepresentation(variant, lista);
                r.setCharacterSet(CharacterSet.UTF_8);
                return r;
            }
            catch (Hd3dException e)
            {
                return null;
            }
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
