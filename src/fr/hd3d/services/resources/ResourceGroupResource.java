package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.RESOURCEGROUP_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


@Hd3dComment("")
public class ResourceGroupResource extends AbstractHd3dResource<ILResourceGroup, IResourceGroup>
{
    public ResourceGroupResource() throws Hd3dException
    {
        super(Persistors.resourcegroup, Translators.resourcegroup);
    }

    @Override
    protected IResourceGroup initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(RESOURCEGROUP_ID_ATTRIBUTE, lockMode);
    }
}
