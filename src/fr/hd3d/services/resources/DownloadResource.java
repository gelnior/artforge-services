package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.io.File;

import org.apache.commons.lang.SystemUtils;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.FilenameMaker;
import fr.hd3d.utils.Log;


/**
 * @author Nicolas Jauffret
 */
@Hd3dComment("Webservice de téléchargement des fichiers. <br>\n"
        + "GET/POST: id= id du fichier à télécharger; preview= boolean, true si on veut télécharger l'imagette de preview; <br>\n"
        + "movie= boolean, true si on veut télécharger la vidéo correspondant au fichier; <br>\n online= boolean, false si on veut une boite de dialogue de téléchargement"
        + "<br>\r\nPUT:N/A\r\n" + "<br>\r\nDELETE:N/A")
public class DownloadResource<L, P> extends AbstractHd3dBaseResource
{
    protected final static String UPLOAD_DIR = Conf.getUploadDir();
    protected final static String NO_IMAGE_PATH = UPLOAD_DIR + SystemUtils.FILE_SEPARATOR + "noimage.jpeg";

    private final static String PARAM_ID = "id";
    private final static String PARAM_PREVIEW = "preview";
    private final static String PARAM_MOVIE = "movie";
    private final static String PARAM_ONLINE = "online";
    private final static String TRUE = "true";
    private final static String FALSE = "false";

    public static void init() throws Hd3dException
    {
        // make sure needed resources are present
        if (!new File(NO_IMAGE_PATH).exists())
            throw new Hd3dException(NO_IMAGE_PATH + " cannot be found!");
    }

    public DownloadResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("POST");
    }

    @Override
    public Representation get(Variant variant)
    {
        try
        {
            // if (true)
            // return doTreatIt();
            Representation rep = doTreatIt();
            getResponse().setEntity(rep);
            return rep;
        }
        finally
        {
            cleanup();
        }
    }

    private Representation doTreatIt()
    {
        String id = getQuery().getValues(PARAM_ID);
        String preview = getQuery().getValues(PARAM_PREVIEW);
        String movie = getQuery().getValues(PARAM_MOVIE);
        String online = getQuery().getValues(PARAM_ONLINE);

        if (id == null)
        {
            return new StringRepresentation("Please specify an Id ( ?id=<id> )");
        }
        if (preview == null || !preview.equals(TRUE))
        {
            preview = FALSE;
        }
        if (movie == null || !movie.equals(TRUE))
        {
            movie = FALSE;
        }
        if (online == null || !online.equals(TRUE))
        {
            online = FALSE;
        }
        FileRepresentation rep;

        try
        {
            IFileRevision file = Persistors.filerevision.getById(Long.parseLong(id));

            if (file != null)
            {
                String fullPath = FilenameMaker.getFullPath(file);

                File f;
                String filename;
                if (preview.equals(FALSE))
                {
                    f = new File(fullPath);
                    filename = file.getKey();
                }
                else
                {
                    if (movie.equals(TRUE))
                    {
                        f = new File(FilenameMaker.getMoviePreviewFromPath(fullPath));
                        filename = FilenameMaker.getMoviePreviewFromPath(file.getKey());
                    }
                    else
                    {
                        f = new File(FilenameMaker.getImagePreviewFromPath(fullPath));
                        filename = FilenameMaker.getImagePreviewFromPath(file.getKey());
                    }
                }
                // System.out.println("FILENAME=" + filename + "  local=" + f.getPath());
                if (f.exists())
                {
                    rep = new FileRepresentation(f, MediaType.ALL);
                    rep.setDownloadName(filename);
                }
                else
                {
                    if (preview.equals(FALSE) && movie.equals(FALSE))
                    {
                        Log.LOGGER.error("*** Error: file not found '{}'", f.getAbsolutePath());
                    }
                    rep = new FileRepresentation(new File(NO_IMAGE_PATH), MediaType.IMAGE_JPEG);
                    rep.setDownloadName("notfound.jpg");
                }
            }
            else
            {
                Log.LOGGER.error("*** Error: file does not exist in database (id={})", id);
                rep = new FileRepresentation(new File(NO_IMAGE_PATH), MediaType.IMAGE_JPEG);
                rep.setDownloadName("notfound.jpg");
                return rep;
            }
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error("*** Error: Exception found!");
            error(Status.CLIENT_ERROR_BAD_REQUEST, e, ERROR_OCCURED);
            String error = ExceptUtils.format(e);
            Log.LOGGER.error(error);
            rep = new FileRepresentation(new File(NO_IMAGE_PATH), MediaType.IMAGE_JPEG);
            rep.setDownloadName("notfound.jpg");
            return rep;
            // return new StringRepresentation("Could not open the specified file");
        }

        if (online.equals(FALSE))
        {
            rep.setDownloadable(true);
        }
        else
        {
            rep.setDownloadable(false);
        }
        return rep;
    }

    public Representation post(Representation entity, Variant variant)
    {
        try
        {
            if (entity != null)
            {
                try
                {
                    Representation rep = doTreatIt();
                    getResponse().setEntity(rep);
                    // Set the status of the response.
                    getResponse().setStatus(Status.SUCCESS_OK);
                    return getResponseEntity();
                }
                catch (Exception e)
                {
                    error(Status.CLIENT_ERROR_BAD_REQUEST, e, ERROR_OCCURED);
                    // The message of all thrown exception is sent back to
                    // client as simple plain text
                    // getResponse().setEntity(new StringRepresentation(e.getMessage(), MediaType.TEXT_PLAIN));
                    // getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
                    // e.printStackTrace();
                    return getResponseEntity();
                }
            }
            else
            {
                // POST request with no entity.
                getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
                return getResponseEntity();
            }
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
