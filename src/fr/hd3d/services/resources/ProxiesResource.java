package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILProxy;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * HD3D
 */
@Hd3dComment("GET:Récupération de la liste des proxies\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'un nouveau proxy\r\n" + "\r\nDELETE:N/A")
public class ProxiesResource extends AbstractHd3dCollection<ILProxy, IProxy>
{
    public ProxiesResource() throws Hd3dException
    {
        super(Persistors.proxy, Translators.proxy);
    }
}
