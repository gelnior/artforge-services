package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ALLCHILDREN;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTagCategory;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetTagCategoriesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer pool objects.<br>
 * URI : /licenses/
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération des catégories de tag\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Création d'une catégorie de tag\r\n" + "\r\nDELETE:N/A")
public class TagCategoriesResource extends AbstractHd3dCollection<ILTagCategory, ITagCategory>
{
    public TagCategoriesResource() throws Hd3dException
    {
        super(Persistors.tagcategory, Translators.tagcategory);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        final String lastSegment = getRequest().getOriginalRef().getLastSegment();
        // Ex: http://../tagcategories
        if (!('/' + ALLCHILDREN).equals(lastSegment))
        {
            return doGetCollection(new GetTagCategoriesCmd(getResourceContext()));
        }
        else
            // Ex: http://../tagcategories/allchildren
            return super.getCollectionForRepresentation();

    }

    // @Override
    // protected ResultCollection<ITagCategory> getCollectionForRepresentation(CompositeQueryModifier filter)
    // throws Hd3dPersistenceException
    // {
    // final Long tagCategoryId = getIdFromUrl(Hd3dApplication.TAGCATEGORY_ID_ATTRIBUTE);
    // final String lastSegment = getRequest().getOriginalRef().getLastSegment();// children
    // // Ex: http://../tagcategories/{valid TAGCATEGORY_ID_ATTRIBUTE}/children
    // if (tagCategoryId > 0)
    // {
    // final CollectionQueries<ILTagCategory, ITagCategory> cmd = new CollectionQueries<ILTagCategory, ITagCategory>(
    // this);
    // HibernateUtil.SafeQuery(cmd.new GetTagCategoriesCmd(), filter);
    // return new ResultCollection<ITagCategory>(cmd.result, cmd.totalCount);
    // }
    // // Ex: http://../tagcategories.
    // // Note that http://../tagcategories/{INVALID TAGCATEGORY_ID_ATTRIBUTE} is redirected to TagCatetoryResource and
    // // is not handled in this resource
    // else if (tagCategoryId == 0 && !"children".equals(lastSegment))// when TAGCATEGORY_ID_ATTRIBUTE is not provided
    // // or not convertible to Long (ex:XX)
    // return super.getCollectionForRepresentation(filter);
    // // Ex: http://../tagcategories/{INVALID TAGCATEGORY_ID_ATTRIBUTE}/children
    // else
    // throw new Hd3dPersistenceException("Invalid tag category id");
    // }

    // @Override
    // protected ITagCategory doPrePersist(Representation entity, Map<String, String> map) throws
    // Hd3dTranslationException
    // {
    // ILTagCategory lTagCategory = JSonFormSerializer.<ILTagCategory> deserialize(new Form(entity));
    // final Long tagCategoryId = getIdFromUrl(Hd3dApplication.TAGCATEGORY_ID_ATTRIBUTE);
    // if (tagCategoryId == 0)
    // throw new Hd3dTranslationException("Tag category id is invalid");
    //
    // lTagCategory.setParent(tagCategoryId);
    // ITagCategory tagCategory = objectTranslator.fromLightweight(lTagCategory, map);
    //
    // tagCategory.doPrePersist();
    // executeScripts(tagCategory, IObjectProvider.PREPERSIST, getNewIdentifier(tagCategory));
    //
    // return tagCategory;
    // }

    // @Override
    // protected List<ITagCategory> doPrePersistCollection(Representation entity, Map<String, String> map)
    // throws Hd3dTranslationException
    // {
    // List<ILTagCategory> lObjects = JSonFormSerializer.<ILTagCategory> deserializeList(new Form(entity));
    // final Long tagCategoryId = getIdFromUrl(Hd3dApplication.TAGCATEGORY_ID_ATTRIBUTE);
    //
    // if (tagCategoryId == 0)
    // throw new Hd3dTranslationException("Tag category id is invalid");
    //
    // List<ITagCategory> objects = new ArrayList<ITagCategory>(lObjects.size());
    // for (ILTagCategory light : lObjects)
    // {
    // light.setParent(tagCategoryId);
    // objects.add(objectTranslator.fromLightweight(light, map));
    // }
    //
    // // PrePersist
    // for (ITagCategory object : objects)
    // object.doPrePersist();
    // executeScripts(objects.get(0), IObjectProvider.PREPERSIST, getNewIdentifier(objects));
    // return objects;
    // }
}
