package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPool;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer pool objects.<br>
 * URI : /pools/
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération des pool de machines\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Création d'un pool de machines\r\n" + "\r\nDELETE:N/A")
public class PoolsResource extends AbstractHd3dCollection<ILPool, IPool>
{

    public PoolsResource() throws Hd3dException
    {
        super(Persistors.pool, Translators.pool);
    }
}
