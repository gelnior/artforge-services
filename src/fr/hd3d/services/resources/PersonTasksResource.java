package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonTasksCmd;


/**
 * A resource to access to specific person's tasks.
 * 
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des taches d'un utilisateur\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'une nouvelle tache pour un utilisateur\r\n" + "\r\nDELETE:N/A")
public class PersonTasksResource extends TasksResource
{
    public PersonTasksResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected ResultCollection<ITask> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetPersonTasksCmd(getResourceContext()).wantTotalSize());
    }
}
