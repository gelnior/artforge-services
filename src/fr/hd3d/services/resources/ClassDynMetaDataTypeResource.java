package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.CLASSDYNMETADATATYPE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILClassDynMetaDataType;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Liste de toutes les associations entre un type de métadonnées définie au préalable "
        + "et une classe d'entité métier. Le prédicat est un fragment de HQL définissant "
        + "quelles sont les instances de la classe liée qui sont éligibles à l'ajout de la métadonnée. "
        + "Après la création de l'association, les instances éligibles se voit créées des VALEURS de "
        + "métadonnées du TYPE de métadonnée associée.\r\n"
        + "\r\nPUT:Modification de l'association. Le prédicat est réévalué pour les instances des classes liées\r\n"
        + "\r\nPOST:Création de l'association\r\n" + "\r\nDELETE:Effacement de l'association")
public class ClassDynMetaDataTypeResource extends AbstractHd3dResource<ILClassDynMetaDataType, IClassDynMetaDataType>
{
    public ClassDynMetaDataTypeResource() throws Hd3dException
    {
        super(Persistors.classdynmetadatatype, Translators.classdynmetadata);
    }

    @Override
    protected IClassDynMetaDataType initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(CLASSDYNMETADATATYPE_ID_ATTRIBUTE, lockMode);
    }
}
