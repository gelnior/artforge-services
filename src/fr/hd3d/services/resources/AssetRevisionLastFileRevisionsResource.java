package fr.hd3d.services.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.translator.Translators;


public class AssetRevisionLastFileRevisionsResource extends AbstractHd3dCollection<ILFileRevision, IFileRevision>
{

    public AssetRevisionLastFileRevisionsResource() throws Hd3dException
    {
        super(Persistors.filerevision, Translators.filerevision);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        final Long id = getResourceContext().getIdFromUrl(ServicesURI.ASSETREVISION_ID_ATTRIBUTE);
        final IAssetRevision assetRevision = Persistors.assetrevision.getById(id);
        if (assetRevision == null)
        {
            return new ResultCollection(new ArrayList<IFileRevision>(), 0);
        }
        final List<IFileRevision> filesRevisions = assetRevision.getFileRevisions();
        if (filesRevisions == null)
        {
            return new ResultCollection(new ArrayList<IFileRevision>(), 0);
        }

        HashMap<String, IFileRevision> fileMap = new HashMap<String, IFileRevision>();
        for (IFileRevision fileRevision : filesRevisions)
        {
            IFileRevision fileMaxRevision = fileMap.get(fileRevision.getKey() + "_" + fileRevision.getVariation());
            if (fileMaxRevision == null || fileMaxRevision.getRevision() < fileRevision.getRevision())
            {
                fileMaxRevision = fileRevision;
            }
            fileMap.put(fileMaxRevision.getKey() + "_" + fileRevision.getVariation(), fileMaxRevision);
        }

        return new ResultCollection(new ArrayList<IFileRevision>(fileMap.values()), fileMap.values().size());
    }
}
