package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Liste de tous les types de métadonnées définies\r\n"
        + "\r\nPUT:Non autorisé car cela entraîne beaucoup de complication pour le traitement des entités liés (ClassDynMetaDataTypes et DynMetaDataValues). Il faut créer un nouveau type.\r\n"
        + "\r\nPOST:Création d'un type de métadonnée\r\n" + "\r\nDELETE:Effacement d'un type de métadonnée")
public class DynMetaDataTypesResource extends AbstractHd3dCollection<ILDynMetaDataType, IDynMetaDataType>
{

    public DynMetaDataTypesResource() throws Hd3dException
    {
        super(Persistors.dynmetadatatype, Translators.dynmetadatatype);
    }
}
