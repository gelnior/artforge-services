package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILListValues;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ListValuesResource extends AbstractHd3dResource<ILListValues, IListValues>
{
    public ListValuesResource() throws Hd3dException
    {
        super(Persistors.listvalues, Translators.listvalues);
    }

    @Override
    protected IListValues initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ServicesURI.LISTS_VALUES_ID_ATTRIBUTE, lockMode);
    }
}
