package fr.hd3d.services.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.client.enums.EAssetStatus;
import fr.hd3d.common.client.enums.EContractType;
import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.common.client.enums.EFileStatus;
import fr.hd3d.common.client.enums.EFrameRate;
import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.common.client.enums.ELicenseType;
import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.common.client.enums.EScreenQualification;
import fr.hd3d.common.client.enums.EScreenType;
import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.common.client.enums.ESkillLevel;
import fr.hd3d.common.client.enums.ESkillMotivation;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.client.enums.EWorkerStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.services.resources.serializer.RessourceSerializer;


/**
 * Return all the available business entities
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:retourne la liste des enums ainsi que leurs valeurs\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:N/A")
public class EnumsResource<L, P> extends AbstractHd3dBaseResource
{
    static Map<String, String> enumsMap = new HashMap<String, String>();

    static
    {
        List<Class<?>> enums = new ArrayList<Class<?>>();
        enums.add(EApprovalNoteStatus.class);
        enums.add(EAssetStatus.class);
        enums.add(EContractType.class);
        enums.add(EFileState.class);
        enums.add(EFileStatus.class);
        enums.add(EFrameRate.class);
        enums.add(EInventoryStatus.class);
        enums.add(ELicenseType.class);
        enums.add(EProjectStatus.class);
        enums.add(EScreenQualification.class);
        enums.add(EScreenType.class);
        enums.add(ESimpleActivityType.class);
        enums.add(ESkillLevel.class);
        enums.add(ESkillMotivation.class);
        enums.add(ETaskStatus.class);
        enums.add(EWorkerStatus.class);

        for (Class<?> anEnum : enums)
        {
            enumsMap.put(anEnum.getName(), StringUtils.join(anEnum.getEnumConstants(), ','));
        }
    }

    public EnumsResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    public Representation get(Variant variant)
    {
        try
        {
            // build the representation
            Representation r = RessourceSerializer.getRepresentation(variant, enumsMap);
            r.setCharacterSet(CharacterSet.UTF_8);
            return r;
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
