package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILHd3dACL;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.dao.IHd3dACL;


/**
 * @author Try LAM
 */
@Hd3dComment("")
public class AclsResource extends AbstractHd3dCollection<ILHd3dACL, IHd3dACL>
{
    public AclsResource() throws Hd3dException
    {
        super(Persistors.acl, Translators.acl);
    }
}
