package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PLAYLISTEDIT_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlayListEdit;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class PlayListEditResource extends AbstractHd3dResource<ILPlayListEdit, IPlayListEdit>
{

    public PlayListEditResource() throws Hd3dException
    {
        super(Persistors.playlistedit, Translators.playlistedit);
    }

    @Override
    protected IPlayListEdit initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(PLAYLISTEDIT_ID_ATTRIBUTE, lockMode);
    }
}
