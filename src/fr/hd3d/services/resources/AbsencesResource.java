package fr.hd3d.services.resources;

import java.util.Map;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILAbsence;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetAbsenceCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des absences\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:Ajout d'une absence\r\n"
        + "\r\nDELETE:N/A")
public class AbsencesResource extends AbstractHd3dCollection<ILAbsence, IAbsence>
{
    public AbsencesResource() throws Hd3dException
    {
        super(Persistors.absence, Translators.absence);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        Map<String, String> map = getQuery().getValuesMap();
        String customValues = map.get(Const.CUSTOM);
        if (customValues != null)
        {
            return doGetCollection(new GetAbsenceCmd(getResourceContext()).wantTotalSize());
        }
        return super.getCollectionForRepresentation();
    }
}
