package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.POOL_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPool;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computers/{id}
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'un pool de machines\r\n" + "\r\nPUT:Modification d'un pool de machines\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:Désactivation d'un pools de machines")
public class PoolResource extends AbstractHd3dResource<ILPool, IPool>
{
    @Override
    protected IPool initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(POOL_ID_ATTRIBUTE, lockMode);
    }

    public PoolResource() throws Hd3dException
    {
        super(Persistors.pool, Translators.pool);
    }
}
