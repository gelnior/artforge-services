package fr.hd3d.services.resources;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class GreetingResource<L, P> extends AbstractHd3dBaseResource
{
    private static final List<ResourceDescriptor> SET = new ArrayList<ResourceDescriptor>();

    private static final HashMap<String, List<ResourceDescriptor>> map = new HashMap<String, List<ResourceDescriptor>>();

    private static void addMap(ResourceDescriptor descriptor)
    {
        List<ResourceDescriptor> descriptors = map.get(descriptor.getSection());
        if (descriptors == null)
        {
            descriptors = new ArrayList<ResourceDescriptor>();
            map.put(descriptor.getSection(), descriptors);
        }

        descriptors.add(descriptor);
    }

    public static void appendResource(final String url, final Class<? extends ServerResource> resourceClass,
            String section)
    {
        ResourceDescriptor descriptor = new ResourceDescriptor('/' + url, null, resourceClass, section);

        if (descriptor.getSection() != null && descriptor.getSection().length() > 0)
        {
            addMap(descriptor);
        }
        SET.add(descriptor);
    }

    /**
     * @param context
     * @param request
     * @param response
     * @throws Hd3dException
     */
    public GreetingResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    @Get("html")
    public Representation get(Variant arg0)
    {
        if (!MediaType.TEXT_HTML.equals(arg0.getMediaType()))
            return null;
        final StringBuffer buffer = new StringBuffer();

        Collections.sort(SET, new Comparator<ResourceDescriptor>() {
            public int compare(ResourceDescriptor o1, ResourceDescriptor o2)
            {
                return o1.getUrl().compareTo(o2.getUrl());
            }
        });

        buffer.append("<html>\n");
        buffer.append("<style>");
        buffer.append("a { text-decoration: none; color: black; font-weight: bold; }");
        buffer.append("a:hover { color: #088; }");
        buffer.append("ul { list-style-type: none; padding-left:10px; }");
        buffer.append("h1 { margin:0; padding : 15px 0 0 10px; }");
        buffer.append("h2 { margin:0; margin : 25px 0 10px 10px; }");
        buffer.append("#logo { float: left; margin: 10px; }");
        buffer.append("#title { width: 100%; background-color: black; color: white; margin:0; }");
        buffer.append("#menu { background-color: black;  width: 100%; margin: 0; border-top: 1px solid white; }");
        buffer.append("#menu { padding: 5px 0 3px 0 ;}");
        buffer.append("#menu li { display: inline; padding: 0 10px 5px 10px; }");
        buffer.append("#menu a { color: white; }");
        buffer.append("#menu li:hover { padding-top: 5px;  background: white; color:  black;}");
        buffer.append("#menu li:hover a { color:  black;}");
        buffer.append("</style>");
        buffer.append("<body style=\"font-family: sans-serif; margin: 0;\">\n");
        buffer.append("<div id=\"title\">");
        buffer.append("<img id=\"logo\" src=\"images/white-logo/\" alt=\"HD3D logo\">");
        buffer.append("<h1 style=\"margin-bottom: 0px;\">HD3D Web Services</h1><br />");
        buffer.append("</div>");

        buffer.append("<ul id=\"menu\">");

        List<String> sections = new ArrayList<String>(map.keySet());
        Collections.sort(sections);

        for (String section : sections)
        {
            buffer.append("<a href=\"#" + section.toLowerCase() + "\"><li>" + section + "</li></a>");
        }
        buffer.append("</ul>");

        int i = 0;
        for (String section : sections)
        {
            List<ResourceDescriptor> descriptors = map.get(section);

            Collections.sort(descriptors, new Comparator<ResourceDescriptor>() {
                public int compare(ResourceDescriptor o1, ResourceDescriptor o2)
                {
                    return o1.getUrl().compareTo(o2.getUrl());
                }
            });

            buffer.append("<h2 id=\"" + section.toLowerCase() + "\">");
            buffer.append(section);
            buffer.append("</h2>");
            buffer.append("<ul>");
            for (ResourceDescriptor resource : descriptors)
            {
                buffer.append("<li style=\"margin-bottom: 3px;  font-size: 14px;\">\n");
                buffer.append("<a id=\"plus-" + i + "\" href=\"\" style=\"font-weight: bold; text-decoration: none;\""
                        + " onClick=\"javascript:document.getElementById('" + i
                        + "').style.display='inherit'; javascript:document.getElementById('plus-" + i
                        + "').style.display='none'; javascript:document.getElementById('minus-" + i
                        + "').style.display='inline';  return false;\">+</a> \n");
                buffer
                        .append("<a id=\"minus-"
                                + i
                                + "\" href=\"\" style=\"font-weight: bold; text-decoration: none; display: none; margin : 0 4px 0 2px;\""
                                + " onClick=\"javascript:document.getElementById('" + i
                                + "').style.display='none'; javascript:document.getElementById('plus-" + i
                                + "').style.display='inline'; javascript:document.getElementById('minus-" + i
                                + "').style.display='none'; return false;\">-</a> \n");
                if (resource.url.contains("{"))
                {
                    buffer.append("" + resource.url + "/\n");
                }
                else
                {
                    buffer.append("<strong><a href=\"" + resource.url.substring(1)
                            + "\" style=\"text-decoration: none;  \">" + resource.url);
                    if (resource.url.length() > 1)
                        buffer.append("/");
                    buffer.append("</strong>\n</a>\n");
                }
                buffer.append(resource.comment.toString());

                Annotation ann = resource.resourceClass.getAnnotation(Hd3dComment.class);
                buffer.append("<br />\n<div id=\"" + i + "\" style=\"display:none; margin: 7px 0 10px 30px;\">\n");
                buffer.append(" <span style=\"font-family: sans-serif; font-size: 12px; \">");
                if (resource.url.length() == 1)
                    buffer.append("Liste des ressources disponibles.");
                if (ann != null)
                {
                    String comment = ((Hd3dComment) ann).value();
                    buffer.append(comment);
                }
                buffer.append("&nbsp;</span></div>\n");
                buffer.append("</li>\n ");
                i++;
            }
            buffer.append("\n</ul>");
        }

        buffer
                .append("<div style=\"width: 100%; background-color: black; color: white; padding: 10px; margin-top: 10px; \">");
        buffer.append("<span style=\"font-weight: bold; font-size: 16px;\">Contacts : </span>");
        buffer
                .append("<a href=\"mailto:tl@hd3d.fr\" style=\"font-weight: normal; font-decoration:underline; color: white; font-size: 14px;\">Try LAM (tl@hd3d.fr)</a>, ");
        buffer
                .append("<a href=\"mailto:fr@hd3d.fr\" style=\"font-weight: normal; font-decoration:underline; color: white; font-size: 14px;\">Frank ROUSSEAU  (fr@hd3d.fr)</a>");
        buffer.append("</div>");

        buffer.append("</body>\n");
        buffer.append("</html>\n");

        return new StringRepresentation(buffer.toString(), MediaType.TEXT_HTML);
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
