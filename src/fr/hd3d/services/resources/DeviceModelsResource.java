package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILDeviceModel;
import fr.hd3d.model.persistence.IDeviceModel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all computer pool objects.<br>
 * URI : /pools/
 * 
 * @author HD3D
 */
public class DeviceModelsResource extends AbstractHd3dCollection<ILDeviceModel, IDeviceModel>
{
    public DeviceModelsResource() throws Hd3dException
    {
        super(Persistors.deviceModel, Translators.deviceModel);
    }
}
