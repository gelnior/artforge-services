package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileAttribute;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class FileAttributesResource extends AbstractHd3dCollection<ILFileAttribute, IFileAttribute>
{

    public FileAttributesResource() throws Hd3dException
    {
        super(Persistors.fileattribute, Translators.fileattribute);
    }
}
