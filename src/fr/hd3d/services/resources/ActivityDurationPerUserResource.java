package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonDayFromToCollectionCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.ListResultQuery;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.ServerResourceUtils;
import fr.hd3d.utils.StringUtils;


public class ActivityDurationPerUserResource<ILPersonDay, IPersonDay> extends AbstractHd3dBaseResource
{

    public ActivityDurationPerUserResource() throws Hd3dException
    {
        super();
        allowMethods(null);
        getResourceContext().complete();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Representation get(Variant variant)
    {
        Session session = HibernateUtil.currentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        try
        {
            final Long personId = getPersonId();

            final String startString = (String) getResourceContext().getQueryMap().get(Const.STARTDATE);
            final String endString = (String) getResourceContext().getQueryMap().get(Const.ENDDATE);
            final Date start = StringUtils.parseDate(startString);
            final Date end = StringUtils.parseDate(endString);

            /* retrieve PersonDays with date > start and date < end group by user */
            final ListResultQuery<?, ?> cmd = new GetPersonDayFromToCollectionCmd(
                    (ResourceContext<fr.hd3d.model.lightweight.ILPersonDay, fr.hd3d.model.persistence.IPersonDay>) getResourceContext(),
                    start, end, personId).wantTotalSize();
            HibernateUtil.safeQuery(cmd);
            ResultCollection<IPersonDay> personDays = new ResultCollection(cmd.result, cmd.totalCount);

            /* retrieve users */
            // final List<IPerson> persons = Persistors.person.getAllCollection();
            String q = "select id from PersonH where internalStatus=0";
            final List<Long> personIds = HibernateUtil.currentSession().createQuery(q).list();
            /* Reason why the map key is String is that JSON only support String keys in map */
            Map<String, Long> result = new HashMap<String, Long>(1);
            // for (final IPerson person : persons)
            for (final Long id : personIds)
            {
                if (id.equals(personId))
                {
                    result.put(String.valueOf(personId), 0L);
                }
            }

            for (final IPersonDay personDay : personDays.getResult())
            {
                fr.hd3d.model.persistence.IPersonDay day = (fr.hd3d.model.persistence.IPersonDay) personDay;
                final Long id = day.getPerson().getId();
                if (personId.equals(id))
                {
                    Long previousDuration = result.get(String.valueOf(personId)) != null ? result.get(String
                            .valueOf(personId)) : 0L;
                    result.put(String.valueOf(personId), previousDuration + totalDuration(day));
                }
            }

            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            tx.commit();

            Representation r = RessourceSerializer.getRepresentation(variant, (Map<String, Long>) result);
            r.setCharacterSet(CharacterSet.UTF_8);
            return ServerResourceUtils.encodeRepresentation(r);
        }
        catch (Hd3dException e)
        {
            error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
            return getResponseEntity();
        }
        finally
        {
            if (tx != null)
                tx = null;
            cleanup();
        }
    }

    protected Long getPersonId()
    {
        return getResourceContext().getIdFromUrl(ServicesURI.PERSON_ID_ATTRIBUTE);
    }

    private Long totalDuration(fr.hd3d.model.persistence.IPersonDay personDay)
    {
        if (personDay == null)
            return 0L;

        Long duration = 0L;
        for (final IActivity activity : personDay.getActivities())
        {
            duration += activity.getDuration();
        }
        return duration;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
