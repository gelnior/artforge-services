package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.transformer.GetStepBoundEntityTransformer;


public class StepsByTaskTypeCollectionQuery extends AbstractCollectionQuery
{
    public StepsByTaskTypeCollectionQuery()
    {
        super();
    }

    public StepsByTaskTypeCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        /* item.getTransformerParameters() is supposed to contain the id of the task type */
        if (item == null)
            return;
        ITaskType taskType = null;
        try
        {
            taskType = Persistors.tasktype.getById(NumberUtils.toLong(item.getTransformerParameters()));
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
        name = "Step for" + taskType == null ? "(task type not found)" : taskType.getName();
    }

    public void setDescription()
    {
        description = "returns the number of tasks with given status";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>(1);
        appliedEntities.add(Const.ALL_CLASSES);
    }

    public void setReturnType()
    {
        returnType = fr.hd3d.common.client.Const.STEP;
    }

    @Override
    public List<IStep> doQuery() throws Hd3dItemFilterException
    {
        List<IStep> orderedSteps;

        /* nothing to query */
        if (CollectionUtils.isEmpty(entities))
        {
            orderedSteps = sameSizeListOf(NumberUtils.LONG_ZERO);
        }
        else
        {
            List<IStep> result = new ArrayList<IStep>(ids.size());

            /* total number of tasks bound to the entities */
            for (List<Long> partition : idPartitions)
            {
                if (CollectUtils.isNotEmpty(partition))
                {
                    String query = "from " + Const.CANONICALNAME_STEP + " step where step.boundEntityName = '"
                            + entityName + "' and step.taskType.id = " + item.getTransformerParameters()
                            + " and step.boundEntity in(" + StringUtils.join(partition, ',')
                            + ") and step.internalStatus=0 order by step.boundEntity ";
                    result.addAll(HibernateUtil.currentSession().createQuery(query).list());
                }
            }

            /* reorder */

            if (CollectUtils.isEmpty(result))
            {
                orderedSteps = sameSizeListOf(null);
            }
            else
            {
                orderedSteps = new ArrayList<IStep>(CollectUtils.reorder(ids, result,
                        new GetStepBoundEntityTransformer()));
                // for (Long id : ids)
                // {
                // boolean found = false;
                // for (IStep step : result)
                // {
                // if (step.getBoundEntity().equals(id))
                // {
                // orderedSteps.add(step);
                // found = true;
                // break;
                // }
                // }
                // if (!found)
                // {
                // orderedSteps.add(null);
                // }
                // }
            }
            result.clear();
        }

        postCheck(orderedSteps);

        return orderedSteps;
    }
}
