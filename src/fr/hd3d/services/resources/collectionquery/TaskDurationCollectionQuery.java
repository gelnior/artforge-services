package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;


public class TaskDurationCollectionQuery extends AbstractCollectionQuery
{

    public TaskDurationCollectionQuery()
    {
        super();
    }

    public TaskDurationCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Task Duration";
    }

    public void setDescription()
    {
        description = "For an entity, returns the duration of the tasks bound to it";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>(1);
        appliedEntities.add(Const.ALL_CLASSES);
    }

    public void setReturnType()
    {
        returnType = fr.hd3d.common.client.Const.JAVA_LANG_LONG;
    }

    @Override
    public List<Long> doQuery() throws Hd3dItemFilterException
    {
        /* nothing to query */
        if (CollectionUtils.isEmpty(entities))
        {
            return sameSizeListOf(null);
        }

        /* total duration of tasks bound to the entities */
        List<Object[]> result = new ArrayList<Object[]>(ids.size());
        for (List<Long> partition : idPartitions)
        {
            if (CollectUtils.isNotEmpty(partition))
            {
                String query = "select sum(task.duration), links.boundEntity from " + Const.CANONICALNAME_TASK
                        + " task inner join task.boundEntityTaskLinks as links where links.boundEntityName = '"
                        + entityName + "' and links.boundEntity in(" + StringUtils.join(partition, ',')
                        + ") and task.internalStatus=0 group by links.boundEntity order by links.boundEntity";
                result.addAll(HibernateUtil.currentSession().createQuery(query).list());
            }
        }

        /* reorder */
        List<Long> totalDuration = new ArrayList<Long>();

        if (CollectUtils.isEmpty(result))
        {
            return sameSizeListOf(NumberUtils.LONG_ZERO);
        }
        else
        {
            MultiHashMap<Long, Long> multiMap = new MultiHashMap<Long, Long>();
            for (Object[] tuple : result)
            {
                multiMap.put((Long) tuple[0], (Long) tuple[1]);
            }
            for (Long id : ids)
            {
                boolean found = false;
                for (Object[] tuple : result)
                {
                    if (((Long) tuple[1]).equals(id))
                    {
                        totalDuration.add((Long) tuple[0]);
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    totalDuration.add(NumberUtils.LONG_ZERO);
                }
            }
            multiMap.clear();
        }

        result.clear();

        postCheck(totalDuration);

        return totalDuration;
    }
}
