package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;


public class TaskBoundEntityLinksCollectionQuery extends AbstractCollectionQuery
{
    public TaskBoundEntityLinksCollectionQuery()
    {
        super();
    }

    public TaskBoundEntityLinksCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Work Object";
    }

    public void setDescription()
    {
        description = "returns the entity bound (work object) to the task";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>();
        appliedEntities.add(EntitiesMaps.withClass(TaskH.class).getEntityName());
    }

    public void setReturnType()
    {
        returnType = "IBase";
    }

    @Override
    public List<List<IEntityTaskLink>> doQuery() throws Hd3dItemFilterException
    {
        /* nothing to query */
        if (CollectionUtils.isEmpty(entities))
        {
            return sameSizeListOf(null);
        }

        /* retrieve the entityTaskLinks bound to the tasks */
        List<IEntityTaskLink> result = new ArrayList<IEntityTaskLink>(ids.size());
        for (List<Long> partition : idPartitions)
        {
            if (CollectUtils.isNotEmpty(partition))
            {
                String query = "from " + Const.CANONICALNAME_ENTITYTASKLINK + " etl where etl.task.id in("
                        + StringUtils.join(ids, ',') + ") and etl.internalStatus=0 order by etl.task.id";
                result.addAll(HibernateUtil.currentSession().createQuery(query).list());
            }
        }

        /* nothing found */
        if (CollectUtils.isEmpty(result))
        {
            return sameSizeListOf(null);
        }

        /* reorder according to tasks order */
        // final ChainedTransformer<EntityTaskLinkH, Long> transformers = new ChainedTransformer<EntityTaskLinkH, Long>(
        // new Transformer[] { new GetEntityTaskLinkTaskTransformer(), new GetIdTransformer<ITask>() });
        // List<EntityTaskLinkH> orderedEntityTaskLinkH = CollectUtils.reorder(ids, result, transformers);
        List<List<IEntityTaskLink>> orderedEntityTaskLinkH = reorderEntityTaskLinksByTaskId(result);

        postCheck(orderedEntityTaskLinkH);

        return orderedEntityTaskLinkH;

    }

    private List<List<IEntityTaskLink>> reorderEntityTaskLinksByTaskId(List<IEntityTaskLink> entityTaskLinks)
    {
        List<List<IEntityTaskLink>> orderedEntityTaskLinks = new ArrayList<List<IEntityTaskLink>>();
        for (Long id : ids)
        {
            List<IEntityTaskLink> anEntityTaskLinks = new ArrayList<IEntityTaskLink>();
            for (IEntityTaskLink etl : entityTaskLinks)
            {
                if (etl.getTask() != null)
                {
                    if (id.equals(etl.getTask().getId()))
                    {
                        anEntityTaskLinks.add(etl);
                    }
                }
            }
            orderedEntityTaskLinks.add(anEntityTaskLinks);
        }

        return orderedEntityTaskLinks;
    }
}
