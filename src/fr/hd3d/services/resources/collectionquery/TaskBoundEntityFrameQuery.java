package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.GetFrameTransformer;


public class TaskBoundEntityFrameQuery extends AbstractCollectionQuery
{
    public TaskBoundEntityFrameQuery()
    {
        super();
    }

    public TaskBoundEntityFrameQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Shot frames";
    }

    public void setDescription()
    {
        description = "returns the the number of frames  of the shot linked to the task. If the work object is not a task null is returned.";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>() {
            private static final long serialVersionUID = -7948196206479572957L;

            {
                add(EntitiesMaps.withClass(TaskH.class).getEntityName());
            }
        };
    }

    public void setReturnType()
    {
        returnType = fr.hd3d.common.client.Const.JAVA_LANG_STRING;
    }

    @Override
    public List<Long> doQuery() throws Hd3dItemFilterException
    {
        List<Long> ret;

        List<? extends IBase> taskBoundEntities = new TaskBoundEntityCollectionQuery(entities, item).doQuery();

        if (CollectionUtils.isEmpty(taskBoundEntities))
        {
            ret = sameSizeListOf(null);
        }
        else
        {
            ret = new ArrayList<Long>(org.apache.commons.collections15.CollectionUtils.collect(taskBoundEntities,
                    new GetFrameTransformer<IBase>()));
        }

        postCheck(ret);

        return ret;
    }
}
