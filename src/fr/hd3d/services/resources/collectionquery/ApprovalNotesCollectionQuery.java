package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.math.NumberUtils;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.query.Queries;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.transformer.GetApprovalNoteTaskTypeTransformer;


public class ApprovalNotesCollectionQuery extends AbstractCollectionQuery
{
    public static final String ApprovalNotesCollectionQuery_ERR = "Approval notes size({0}) is different than task size({1}))!";

    public ApprovalNotesCollectionQuery()
    {
        super();
    }

    public ApprovalNotesCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Approval Notes";
    }

    public void setDescription()
    {
        description = "returns approval notes";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>(1);
        appliedEntities.add(Const.ALL_CLASSES);
    }

    public void setReturnType()
    {
        returnType = Const.ENTITYNAME_APPROVALNOTE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List doQuery() throws Hd3dException
    {
        List<List> ret = new ArrayList<List>();

        final Long approvalNoteTypeId = NumberUtils.toLong(item.getTransformerParameters());

        if (CollectionUtils.isEmpty(entities) || BaseH.isNotValidId(approvalNoteTypeId))
        {
            ret = sameSizeListOf(new ArrayList<IApprovalNote>(0));
        }
        else
        {
            final IApprovalNoteType approvalNoteType = Persistors.approvalnotetype.getById(approvalNoteTypeId);

            /* query */
            List<List<IApprovalNote>> orderedApprovalNotes = getOrderedApprovalNotes(approvalNoteTypeId);

            List<List<ITask>> orderedTasks = getOrderedTasks(approvalNoteType);

            /*-
             * match entities tasks with approval notes. 
             * Response: 
             * Object[0]=the approval note do have tasks of same tasktype (true/false)
             * Object[1]=List<ApprovalNote>
             */
            for (int i = 0; i < ids.size(); i++)
            {
                List enabledAndApprovalnotes = new ArrayList(2);
                enabledAndApprovalnotes.add(getEnabled(orderedTasks, i, approvalNoteType));
                enabledAndApprovalnotes.add(orderedApprovalNotes.get(i));

                ret.add(enabledAndApprovalnotes);
            }
        }

        return ret;
    }

    private List<List<IApprovalNote>> getOrderedApprovalNotes(Long approvalNoteTypeId) throws Hd3dItemFilterException
    {
        List<IApprovalNote> approvalNotes = new ArrayList<IApprovalNote>();
        for (List<Long> partition : idPartitions)
        {
            approvalNotes.addAll(Queries.getApprovalNotesByType(partition, approvalNoteTypeId, entityName));
        }

        List<List<IApprovalNote>> orderedApprovalNotes = reorder(approvalNotes);

        postCheck(orderedApprovalNotes);

        return orderedApprovalNotes;
    }

    private List<List<ITask>> getOrderedTasks(final IApprovalNoteType approvalNoteType) throws Hd3dException
    {
        List<List<ITask>> orderedTasks = null;

        /* look for tasks with same work object and same taskType */
        if (approvalNoteType != null)
        {
            ITaskType taskType = approvalNoteType.getTaskType();

            if (taskType != null)
            {
                List<ITaskType> approvalNotesTaskTypes = new ArrayList<ITaskType>();
                approvalNotesTaskTypes.add(taskType);

                List<ITask> tasks = new ArrayList<ITask>();
                for (List<Long> partition : idPartitions)
                {
                    tasks.addAll(Queries.getEntitiesTasksByTaskTypes(partition, entityName, approvalNotesTaskTypes));
                }

                orderedTasks = reorderTasks(tasks);
                postCheck(orderedTasks);
            }
        }

        return orderedTasks;
    }

    private boolean getEnabled(List<List<ITask>> orderedTasks, int i, final IApprovalNoteType approvalNoteType)
    {
        boolean ret;
        if (orderedTasks == null || (approvalNoteType != null && approvalNoteType.getTaskType() == null))
        {
            ret = true;
        }
        else
        {
            ret = CollectUtils.isNotEmpty(orderedTasks.get(i));
        }

        return ret;
    }

    private List<ITaskType> getApprovalNoteTaskTypes(List<List<IApprovalNote>> approvalNotes)
    {
        Transformer<IApprovalNote, ITaskType> transformer = new GetApprovalNoteTaskTypeTransformer();
        List<ITaskType> approvalNotesTaskTypes = new ArrayList<ITaskType>();
        for (List<IApprovalNote> approvalNotePart : approvalNotes)
        {
            approvalNotesTaskTypes.addAll(org.apache.commons.collections15.CollectionUtils.collect(approvalNotePart,
                    transformer));
        }
        return approvalNotesTaskTypes;
    }

    private List<List<IApprovalNote>> reorder(List<IApprovalNote> approvalNotes)
    {
        CollectUtils.removeNull(approvalNotes);

        List<List<IApprovalNote>> orderedApprovalNotes = new ArrayList<List<IApprovalNote>>();
        for (Long id : ids)
        {
            List<IApprovalNote> app = new ArrayList<IApprovalNote>();
            for (IApprovalNote note : approvalNotes)
            {
                if (id.equals(note.getBoundEntity()))
                {
                    app.add(note);
                }
            }
            orderedApprovalNotes.add(app);
        }

        return orderedApprovalNotes;
    }

    private List<List<ITask>> reorderTasks(List<ITask> tasks) throws Hd3dItemFilterException
    {
        /* bulk populate tasks's boundEntityTaskLinks */
        TaskBoundEntityLinksCollectionQuery cq = new TaskBoundEntityLinksCollectionQuery(tasks, null);
        List<List<IEntityTaskLink>> orderedEntityTaskLinks = cq.doQuery();

        List<List<ITask>> orderedTasks = new ArrayList<List<ITask>>();
        for (Long id : ids)
        {
            List<ITask> anEntityTasks = new ArrayList<ITask>();
            // for (ITask task : tasks)
            // {
            // if (task.getBoundEntityTaskLink() != null && id.equals(task.getBoundEntityTaskLink().getBoundEntity()))
            // {
            // anEntityTasks.add(task);
            // }
            // }
            for (int i = 0; i < tasks.size(); i++)
            {
                if (CollectUtils.isNotEmpty(orderedEntityTaskLinks.get(i))
                        && id.equals(orderedEntityTaskLinks.get(i).get(0).getBoundEntity()))
                {
                    anEntityTasks.add(tasks.get(i));
                }
            }
            orderedTasks.add(anEntityTasks);
        }

        return orderedTasks;
    }
}
