package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.transformer.GetParentsIdTransformer;


public class TaskBoundEntityParentIdCollectionQuery extends AbstractCollectionQuery
{
    public TaskBoundEntityParentIdCollectionQuery()
    {
        super();
    }

    public TaskBoundEntityParentIdCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Task Bound Entity Parent Id";
    }

    public void setDescription()
    {
        description = "returns the parent id of the entity bound to the task";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>();
        appliedEntities.add(EntitiesMaps.withClass(TaskH.class).getEntityName());
    }

    public void setReturnType()
    {
        returnType = fr.hd3d.common.client.Const.JAVA_LANG_INTEGER;
    }

    @Override
    public List<Long> doQuery() throws Hd3dItemFilterException
    {
        List<? extends IBase> taskBoundEntities = new TaskBoundEntityCollectionQuery(entities, item).doQuery();

        List<Long> parentIds;

        if (CollectionUtils.isEmpty(taskBoundEntities))
        {
            parentIds = sameSizeListOf(null);
        }
        else
        {
            parentIds = new ArrayList<Long>(org.apache.commons.collections15.CollectionUtils.collect(taskBoundEntities,
                    new GetParentsIdTransformer<IBase>()));
        }

        taskBoundEntities.clear();
        taskBoundEntities = null;

        postCheck(parentIds);

        return parentIds;
    }
}
