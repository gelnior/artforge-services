package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.impl.hibernate.SimpleActivityH;
import fr.hd3d.services.EntitiesMaps;


public class ActivitiesProjectTypeCollectionQuery extends AbstractCollectionQuery
{
    public ActivitiesProjectTypeCollectionQuery()
    {
        super();
    }

    public ActivitiesProjectTypeCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Activities Project Type";
    }

    public void setDescription()
    {
        description = "returns activities' project type";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>(1);
        appliedEntities.add(EntitiesMaps.withClass(SimpleActivityH.class).getEntityName());
    }

    public void setReturnType()
    {
        returnType = fr.hd3d.common.client.Const.JAVA_LANG_STRING;
    }

    @Override
    public List<String> doQuery() throws Hd3dItemFilterException
    {
        List<String> projectTypes;

        /* nothing to query */
        if (CollectionUtils.isEmpty(entities))
        {
            projectTypes = sameSizeListOf(null);
        }
        else
        {
            /* process */
            projectTypes = new ArrayList<String>(ids.size());

            String projectType = null;
            IProject project;
            for (Object o : entities)
            {
                if (SimpleActivityH.class.isInstance(o))
                {
                    project = ((SimpleActivityH) o).getProject();

                    if (project != null && project.getProjectType() != null)
                    {
                        projectType = project.getProjectType().getName();
                    }
                }

                projectTypes.add(projectType);
            }

            postCheck(projectTypes);
        }

        return projectTypes;
    }
}
