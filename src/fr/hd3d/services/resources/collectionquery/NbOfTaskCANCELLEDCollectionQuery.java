package fr.hd3d.services.resources.collectionquery;

import java.util.List;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IItem;


public class NbOfTaskCANCELLEDCollectionQuery extends NbOfTaskwGivenStatusCollectionQuery
{
    public NbOfTaskCANCELLEDCollectionQuery()
    {
        super();
    }

    public NbOfTaskCANCELLEDCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    @Override
    public void setName()
    {
        name = "Number of tasks " + ETaskStatus.CANCELLED.toString();
    }

    @Override
    public void setDescription()
    {
        description = "returns the number of tasks with status " + ETaskStatus.CANCELLED.toString();
    }

    @Override
    public List<Long> doQuery() throws Hd3dItemFilterException
    {
        setParam(ETaskStatus.CANCELLED.toString());
        return super.doQuery();
    }
}
