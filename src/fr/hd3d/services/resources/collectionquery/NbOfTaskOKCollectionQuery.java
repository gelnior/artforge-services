package fr.hd3d.services.resources.collectionquery;

import java.util.List;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IItem;


public class NbOfTaskOKCollectionQuery extends NbOfTaskwGivenStatusCollectionQuery
{
    public NbOfTaskOKCollectionQuery()
    {
        super();
    }

    public NbOfTaskOKCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    @Override
    public void setName()
    {
        name = "Number of tasks " + ETaskStatus.OK.toString();
    }

    @Override
    public void setDescription()
    {
        description = "returns the number of tasks with status " + ETaskStatus.OK.toString();
    }

    @Override
    public List<Long> doQuery() throws Hd3dItemFilterException
    {
        setParam(ETaskStatus.OK.toString());
        return super.doQuery();
    }
}
