package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;


public class TaskWorkersCollectionQuery extends AbstractCollectionQuery
{
    public TaskWorkersCollectionQuery()
    {
        super();
    }

    public TaskWorkersCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Task Workers";
    }

    public void setDescription()
    {
        description = "returns the task workers";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>(1);
        appliedEntities.add(Const.ALL_CLASSES);
    }

    public void setReturnType()
    {
        returnType = "java.util.List<fr.hd3d.model.persistence.IPerson>";
    }

    @Override
    public List<List<IPerson>> doQuery() throws Hd3dItemFilterException
    {
        /* nothing to query */
        if (CollectionUtils.isEmpty(entities))
        {
            return sameSizeListOf(null);
        }

        /* query */
        /* total number of tasks bound to the entities */
        /* Note: order of selected attributes DOES MATTER, a wrong order may make query fail */
        List<Object[]> result = new ArrayList<Object[]>();
        for (List<Long> partition : idPartitions)
        {
            // System.out.println("Partition=" + partition);

            if (CollectUtils.isEmpty(partition))
                continue;

            String query = "select task.worker, links.boundEntity  from " + Const.CANONICALNAME_TASK
                    + " task inner join task.boundEntityTaskLinks as links where links.boundEntityName = '"
                    + entityName + "' and links.boundEntity in(" + StringUtils.join(partition, ',')
                    + ") and task.internalStatus=0";

            String taskTypeIdString = item.getTransformerParameters();
            if (StringUtils.isNotEmpty(taskTypeIdString))
            {
                query += " and task.taskType.id = " + taskTypeIdString;
            }
            query += " order by links.boundEntity, task.worker.firstName";// order may be changed

            result.addAll(HibernateUtil.currentSession().createQuery(query).list());
        }

        /* reorder */
        List<List<IPerson>> workers = new ArrayList<List<IPerson>>();

        if (CollectUtils.isEmpty(result))
        {
            return sameSizeListOf(null);
        }
        else
        {
            MultiHashMap<Long, IPerson> multiMap = new MultiHashMap<Long, IPerson>();
            for (Object[] tuple : result)
            {
                /* tuple[0] = worker; tuple[1] = task.boundEntityTaskLink.boundEntity */
                multiMap.put((Long) tuple[1], (IPerson) tuple[0]);
            }

            for (Long id : ids)
            {
                workers.add((List<IPerson>) multiMap.getCollection(id));
            }
        }

        result.clear();

        postCheck(workers);

        return workers;
    }
}
