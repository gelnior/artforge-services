package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.transformer.GetPathTransformer;


public class TaskBoundEntityPathCollectionQuery extends AbstractCollectionQuery
{
    public TaskBoundEntityPathCollectionQuery()
    {
        super();
    }

    public TaskBoundEntityPathCollectionQuery(List entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Work Object Path";
    }

    public void setDescription()
    {
        description = "returns the path of the entity bound (work object) to the task";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>();
        appliedEntities.add(EntitiesMaps.withClass(TaskH.class).getEntityName());
    }

    public void setReturnType()
    {
        returnType = fr.hd3d.common.client.Const.JAVA_LANG_STRING;
    }

    @Override
    public List<String> doQuery() throws Hd3dItemFilterException
    {
        List<? extends IBase> taskBoundEntities = new TaskBoundEntityCollectionQuery(entities, item).doQuery();

        List<String> ret;

        if (CollectionUtils.isEmpty(taskBoundEntities))
        {
            ret = sameSizeListOf(null);
        }
        else
        {
            ret = new ArrayList<String>(org.apache.commons.collections15.CollectionUtils.collect(taskBoundEntities,
                    new GetPathTransformer<IBase>()));
        }

        postCheck(ret);

        return ret;
    }
}
