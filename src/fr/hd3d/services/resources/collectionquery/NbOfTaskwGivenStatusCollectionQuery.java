package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;


public abstract class NbOfTaskwGivenStatusCollectionQuery extends AbstractCollectionQuery
{
    public NbOfTaskwGivenStatusCollectionQuery()
    {
        super();
    }

    public NbOfTaskwGivenStatusCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Number of tasks with given status";
    }

    public void setDescription()
    {
        description = "returns the number of tasks with given status";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>(1);
        appliedEntities.add(Const.ALL_CLASSES);
    }

    public void setReturnType()
    {
        returnType = fr.hd3d.common.client.Const.JAVA_LANG_INTEGER;
    }

    @Override
    public List<Long> doQuery() throws Hd3dItemFilterException
    {
        /* nothing to query */
        if (CollectionUtils.isEmpty(entities))
        {
            return sameSizeListOf(NumberUtils.LONG_ZERO);
        }

        List<Object[]> result = new ArrayList<Object[]>(ids.size());

        /* total number of tasks bound to the entities */
        for (List<Long> partition : idPartitions)
        {
            if (CollectUtils.isNotEmpty(partition))
            {
                String query = "select links.boundEntity, task.id from " + Const.CANONICALNAME_TASK
                        + " task inner join task.boundEntityTaskLinks as links where links.boundEntityName = '"
                        + entityName + "' and links.boundEntity in(" + StringUtils.join(partition, ',')
                        + ") and task.internalStatus=0 order by links.boundEntity";

                result.addAll(HibernateUtil.currentSession().createQuery(query).list());
            }
        }

        /* reorder */
        List<Long> total = new ArrayList<Long>();
        if (CollectUtils.isEmpty(result))
        {
            return sameSizeListOf(NumberUtils.LONG_ZERO);
        }
        else
        {
            MultiHashMap<Long, Long> multiMap = new MultiHashMap<Long, Long>();
            for (Object[] tuple : result)
            {
                multiMap.put((Long) tuple[0], (Long) tuple[1]);
            }

            for (Long id : ids)
            {
                Collection totalNbCol = multiMap.getCollection(id);
                if (totalNbCol == null)
                {
                    total.add(NumberUtils.LONG_ZERO);
                }
                else
                {
                    total.add((long) (totalNbCol.size()));
                    totalNbCol.clear();
                }
            }
            multiMap.clear();
        }
        result.clear();
        /* number of tasks with given status, bound to the entities */
        if (param != null && ETaskStatus.valueOf(param) != null)
        {
            for (List<Long> partition : idPartitions)
            {
                if (CollectUtils.isNotEmpty(partition))
                {
                    String query = "select links.boundEntity, task.id from TaskH task inner join task.boundEntityTaskLinks as links where links.boundEntityName = '"
                            + entityName
                            + "' and links.boundEntity in("
                            + StringUtils.join(partition, ',')
                            + ") and task.status ='"
                            + ETaskStatus.valueOf(param).toString()
                            + "' and task.internalStatus=0 order by links.boundEntity";
                    result.addAll(HibernateUtil.currentSession().createQuery(query).list());
                }
            }
        }
        /* reorder */
        List<Long> tasks = new ArrayList<Long>();
        if (CollectUtils.isEmpty(result))
        {
            return sameSizeListOf(NumberUtils.LONG_ZERO);
        }
        else
        {
            MultiHashMap<Long, Long> multiMap = new MultiHashMap<Long, Long>();
            for (Object[] tuple : result)
            {
                multiMap.put((Long) tuple[0], (Long) tuple[1]);
            }

            for (Long id : ids)
            {
                Collection nbCol = multiMap.getCollection(id);
                if (nbCol == null)
                {
                    tasks.add(NumberUtils.LONG_ZERO);
                }
                else
                {
                    tasks.add((long) (nbCol.size()));
                    nbCol.clear();
                }
            }
            multiMap.clear();
        }

        List<Long> ret = new ArrayList<Long>(ids.size());
        for (int i = 0; i < ids.size(); i++)
        {
            ret.add(tasks.get(i));
        }

        postCheck(ret);

        return ret;

    }
}
