package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;


public class TaskBoundEntityCollectionQuery extends AbstractCollectionQuery
{
    public TaskBoundEntityCollectionQuery()
    {
        super();
    }

    public TaskBoundEntityCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Work Object";
    }

    public void setDescription()
    {
        description = "returns the entity bound (work object) to the task";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>();
        appliedEntities.add(EntitiesMaps.withClass(TaskH.class).getEntityName());
    }

    public void setReturnType()
    {
        returnType = "IBase";
    }

    @Override
    public List<? extends IBase> doQuery() throws Hd3dItemFilterException
    {
        /* nothing to query */
        if (CollectionUtils.isEmpty(entities))
        {
            return sameSizeListOf(null);
        }

        /* retrieve the entityTaskLinks bound to the tasks */
        TaskBoundEntityLinksCollectionQuery cq = new TaskBoundEntityLinksCollectionQuery(entities, item);
        List<List<IEntityTaskLink>> orderedEntityTaskLinkH = cq.doQuery();

        /* retrieve the boundEntities */
        MultiHashMap<String, Long> multiMap = new MultiHashMap<String, Long>();
        for (List<IEntityTaskLink> etls : orderedEntityTaskLinkH)
        {
            if (CollectUtils.isNotEmpty(etls))
            {
                for (final IEntityTaskLink etl : etls)
                {
                    /* key= EntityName, value = list of entities */
                    multiMap.put(etl.getBoundEntityName(), etl.getBoundEntity());
                }
            }
        }

        List<IBase> entities = new ArrayList<IBase>();
        for (Map.Entry<String, Collection<Long>> entry : multiMap.entrySet())
        {
            if (CollectUtils.isNotEmpty(entry.getValue()))
            {
                String entityClassName = EntitiesMaps.withEntityName(entry.getKey()).getFullClassName();
                String q = "from " + entityClassName + " entity where id in ("
                        + StringUtils.join(new HashSet<Long>(entry.getValue()), ',') + ") and entity.internalStatus=0";
                entities.addAll(HibernateUtil.currentSession().createQuery(q).list());
            }
        }

        /* reorder retrieved entities according to tasks order */
        CollectUtils.removeNull(entities);
        List<IBase> orderedEntities_old = new ArrayList<IBase>();
        for (final List<IEntityTaskLink> etls : orderedEntityTaskLinkH)
        {
            if (CollectUtils.isEmpty(etls))
            {
                orderedEntities_old.add(null);
                continue;
            }

            /* Take the first bound entity but actually there may be more than one by task */
            for (IEntityTaskLink etl : etls)
            {
                boolean found = false;
                for (final IBase entity : entities)
                {
                    if (entity.getId().equals(etl.getBoundEntity())
                            && entity.entityName().equals(etl.getBoundEntityName()))
                    {
                        orderedEntities_old.add(entity);
                        found = true;
                        // Note: Do not remove from this collection, to be used later
                        break;
                    }
                }
                if (!found)
                {
                    orderedEntities_old.add(null);
                }
                break;
            }
        }

        postCheck(orderedEntities_old);

        return orderedEntities_old;
    }
}
