package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.list.SetUniqueList;
import org.apache.commons.lang.StringUtils;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Pair;


public class TaskBoundEntityApprovalNotesCollectionQuery extends AbstractCollectionQuery
{
    public TaskBoundEntityApprovalNotesCollectionQuery()
    {
        super();
    }

    public TaskBoundEntityApprovalNotesCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Status notes";
    }

    public void setDescription()
    {
        description = "returns the approval notes of the entity bound to the task";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>();
        appliedEntities.add(EntitiesMaps.withClass(TaskH.class).getEntityName());
    }

    public void setReturnType()
    {
        returnType = fr.hd3d.common.client.Const.LIST_OF_APPROVALS;
    }

    @Override
    public List<List<IApprovalNote>> doQuery() throws Hd3dItemFilterException
    {
        TaskBoundEntityCollectionQuery taskBoundEntitiesCQ = new TaskBoundEntityCollectionQuery(entities, item);

        List<? extends IBase> taskBoundEntities = taskBoundEntitiesCQ.doQuery();

        /* nothing to do */
        if (CollectionUtils.isEmpty(taskBoundEntities))
        {
            return sameSizeListOf(new ArrayList<IApprovalNote>(0));
        }

        /*---------
         * process
         ----------*/
        Set<Long> tasksTaskTypeIds = collectTasksTaskTypeIds();
        List<String> woEntityNames = collectWoEntityNames(taskBoundEntities);

        /* select all Approval notes with ApprovalNoteType.taskType = tasks.taskTypes */
        List<IApprovalNote> result = new ArrayList<IApprovalNote>(ids.size());

        /* total number of tasks bound to the entities */
        for (List<Long> partition : CollectUtils.partition(CollectUtils.getIds(taskBoundEntities),
                AbstractCollectionQuery.SLICE_SIZE))
        {
            if (CollectUtils.isNotEmpty(partition) && CollectUtils.isNotEmpty(woEntityNames)
                    && CollectUtils.isNotEmpty(tasksTaskTypeIds))
            {
                String query = "from " + Const.CANONICALNAME_APPROVALNOTE + " note where "
                        + "note.boundEntityName in ('" + StringUtils.join(new HashSet<String>(woEntityNames), "','")
                        + "') and note.boundEntity in(" + StringUtils.join(new LinkedHashSet<Long>(partition), ',')
                        + ")" + " and note.approvalNoteType is not null "
                        + " and note.approvalNoteType.taskType.id in (" + StringUtils.join(tasksTaskTypeIds, ',')
                        + ") and note.internalStatus=0 order by note.boundEntity, note.approvalNoteType.taskType.id";
                result.addAll(HibernateUtil.currentSession().createQuery(query).list());
            }
        }
        tasksTaskTypeIds.clear();
        woEntityNames.clear();

        /* reorder by bound entities ids and task type ids */
        List<Pair<Long, Long>> boundEntitiesIdsAndTaskTypeIds = new ArrayList<Pair<Long, Long>>();
        IBase wo;
        ITask task;
        ITaskType taskType;
        for (int i = 0; i < entities.size(); i++)
        {
            wo = taskBoundEntities.get(i);
            task = (ITask) entities.get(i);
            taskType = task == null ? null : task.getTaskType();
            /* Pair<WorkObject Id, Bound task's taskType id> */
            Pair<Long, Long> pair = wo == null ? null : new Pair<Long, Long>(wo.getId(),
                    CollectUtils.nullSafeId(taskType));

            boundEntitiesIdsAndTaskTypeIds.add(pair);
        }

        List<List<IApprovalNote>> orderedApprovalNotes = reorder(boundEntitiesIdsAndTaskTypeIds, result);

        taskBoundEntities.clear();
        taskBoundEntities = null;

        postCheck(orderedApprovalNotes);

        return orderedApprovalNotes;
    }

    private Set<Long> collectTasksTaskTypeIds()
    {
        Set<Long> tasksTaskTypeIds = new LinkedHashSet<Long>();

        ITask task;
        for (Object object : entities)
        {
            task = (ITask) object;
            if (task != null && task.getTaskType() != null)
            {
                tasksTaskTypeIds.add(task.getTaskType().getId());
            }
        }

        return tasksTaskTypeIds;
    }

    private List<String> collectWoEntityNames(List<? extends IBase> taskBoundEntities)
    {
        List<String> names = new ArrayList<String>();

        String aTaskBoundEntityName;
        for (IBase object : taskBoundEntities)
        {
            aTaskBoundEntityName = null;
            if (object != null)
            {
                aTaskBoundEntityName = object.entityName();
            }
            names.add(aTaskBoundEntityName);
        }

        return names;
    }

    private List<List<IApprovalNote>> reorder(List<Pair<Long, Long>> boundEntitiesIdsAndTaskTypeIds,
            List<IApprovalNote> approvalnotes)
    {
        /* remove null instances */
        // final Collection<Pair<Long, Long>> notNullBoundEntitiesIdsAndTaskTypeIds = CollectUtils
        // .selectNotNull(boundEntitiesIdsAndTaskTypeIds);
        final Collection<IApprovalNote> notNullApprovalnotes = CollectUtils.selectNotNull(approvalnotes);

        List<List<IApprovalNote>> orderedApprovalNotes = new ArrayList<List<IApprovalNote>>(ids.size());

        Long woId, taskTypeId;
        boolean matchWo, matchTaskType;
        for (Pair<Long, Long> ids : boundEntitiesIdsAndTaskTypeIds)
        {

            List<IApprovalNote> woAppNotes = SetUniqueList.decorate(new ArrayList<IApprovalNote>());

            if (ids != null)
            {
                woId = ids.getField_1();
                taskTypeId = ids.getField_2();

                for (IApprovalNote note : notNullApprovalnotes)
                {
                    matchWo = woId != null && woId.equals(note.getBoundEntity());
                    matchTaskType = taskTypeId != null && note.getApprovalNoteType() != null
                            && note.getApprovalNoteType().getTaskType() != null
                            && taskTypeId.equals(note.getApprovalNoteType().getTaskType().getId());

                    if (matchWo && matchTaskType)
                    {
                        woAppNotes.add(note);
                    }
                }
            }
            orderedApprovalNotes.add(woAppNotes);
        }

        return orderedApprovalNotes;
    }
}
