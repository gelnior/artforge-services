package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.impl.hibernate.ActivityH;
import fr.hd3d.model.persistence.impl.hibernate.SimpleActivityH;
import fr.hd3d.model.persistence.impl.hibernate.TaskActivityH;
import fr.hd3d.services.EntitiesMaps;


public class ActivitiesTaskTypeCollectionQuery extends AbstractCollectionQuery
{
    public ActivitiesTaskTypeCollectionQuery()
    {
        super();
    }

    public ActivitiesTaskTypeCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Activities Task Type";
    }

    public void setDescription()
    {
        description = "returns activities' task type";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>(1);
        appliedEntities.add(EntitiesMaps.withClass(ActivityH.class).getEntityName());
    }

    public void setReturnType()
    {
        returnType = fr.hd3d.common.client.Const.JAVA_LANG_STRING;
    }

    @Override
    public List<String> doQuery() throws Hd3dItemFilterException
    {
        List<String> taskTypes;

        /* nothing to query */
        if (CollectionUtils.isEmpty(entities))
        {
            taskTypes = sameSizeListOf(null);
        }
        else
        {
            taskTypes = new ArrayList<String>(ids.size());

            String taskType = null;
            ITask task;
            for (Object object : entities)
            {
                if (TaskActivityH.class.isInstance(object))
                {
                    task = ((TaskActivityH) object).getTask();

                    if (task != null && task.getTaskType() != null)
                    {
                        taskType = task.getTaskType().getName();
                    }
                }
                else if (SimpleActivityH.class.isInstance(object))
                {
                    taskType = ((SimpleActivityH) object).getType().toString();
                }

                taskTypes.add(taskType);
            }
        }

        postCheck(taskTypes);

        return taskTypes;
    }
}
