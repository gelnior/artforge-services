package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;


public class TaskActivitiesDurationCollectionQuery extends AbstractCollectionQuery
{

    public TaskActivitiesDurationCollectionQuery()
    {
        super();
    }

    public TaskActivitiesDurationCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Task Activities Duration";
    }

    public void setDescription()
    {
        description = "For a task, returns the total duration of all the task activities bound to it";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>(1);
        appliedEntities.add(EntitiesMaps.withClass(TaskH.class).getEntityName());
    }

    public void setReturnType()
    {
        returnType = fr.hd3d.common.client.Const.JAVA_LANG_LONG;
    }

    @Override
    public List<Long> doQuery() throws Hd3dItemFilterException
    {
        /* nothing to query */
        if (CollectionUtils.isEmpty(entities))
        {
            return sameSizeListOf(null);
        }

        /* total duration of tasks bound to the entities */
        List<Object[]> result = new ArrayList<Object[]>(ids.size());
        for (List<Long> partition : idPartitions)
        {
            if (CollectUtils.isNotEmpty(partition))
            {
                String query = "select ta.task.id, sum(ta.duration) from " + Const.CANONICALNAME_TASKACTIVITY
                        + " ta where ta.task.id in(" + StringUtils.join(partition, ',')
                        + ") and ta.internalStatus=0 group by ta.task.id";
                result.addAll(HibernateUtil.currentSession().createQuery(query).list());
            }
        }

        /* reorder */
        List<Long> totalDuration = new ArrayList<Long>();

        if (CollectUtils.isEmpty(result))
        {
            return sameSizeListOf(NumberUtils.LONG_ZERO);
        }
        else
        {
            Collection<Object[]> tmp = result;
            for (Long id : ids)
            {
                boolean found = false;
                Object[] toRemove = null;
                for (Object[] tuple : tmp)
                {
                    if (((Long) tuple[0]).equals(id))
                    {
                        totalDuration.add((Long) tuple[1]);
                        found = true;
                        toRemove = tuple;
                        break;
                    }
                }
                if (!found)
                {
                    totalDuration.add(NumberUtils.LONG_ZERO);
                }

                if (toRemove != null)
                {
                    tmp.remove(toRemove);
                }
            }
        }

        result.clear();

        postCheck(totalDuration);

        return totalDuration;
    }
}
