package fr.hd3d.services.resources.collectionquery;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


public abstract class AbstractCollectionQuery
{
    public static final String ERR = "CollectionQuery returns a collection with different size(size={0}) than entities(size={1})!";

    protected List<?> entities;
    protected IItem item;
    protected List<Long> ids;
    protected String entityName;
    protected List<List<Long>> idPartitions;
    public final static int SLICE_SIZE = HibernateUtil.JDBC_BATCHSIZE;
    protected String name;
    protected String description;
    protected Set<String> appliedEntities;
    protected String returnType;
    public String param;

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public Set<String> getAppliedEntities()
    {
        return appliedEntities;
    }

    public String getReturnType()
    {
        return returnType;
    }

    public abstract void setName();

    public abstract void setDescription();

    public abstract void setAppliedEntities();

    /* provide the return type to the client to select the proper renderer */
    public abstract void setReturnType();

    public AbstractCollectionQuery()
    {
        super();
        setName();
        setDescription();
        setAppliedEntities();
        setReturnType();
    }

    public AbstractCollectionQuery(List<?> entities, IItem item)
    {
        super();
        setName();
        setDescription();
        setAppliedEntities();
        setReturnType();

        setEntities(entities);
        setItem(item);
    }

    public List<?> getEntities()
    {
        return entities;
    }

    public IItem getItem()
    {
        return item;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

    public void setEntities(List<?> entities)
    {
        /* Note: it's better not to check the nulliness or emptiness */
        /* each collection query may return different kind of return */
        /* but they MUST handle null or empty collection in doQuery method */
        this.entities = entities;

        if (CollectUtils.isNotEmpty(entities))
        {
            collectEntityIds(entities);

            splitIdsPartitions();

            /* get entity name */
            entityName = ((IBase) entities.get(0)).entityName();
        }
    }

    private void collectEntityIds(List<?> entities)
    {
        ids = new ArrayList<Long>(entities.size());
        for (Object object : entities)
        {
            ids.add(((IBase) object).getId());
        }
    }

    private void splitIdsPartitions()
    {
        if (CollectUtils.isNotEmpty(ids))
        {
            idPartitions = CollectUtils.partition(ids, SLICE_SIZE);
        }
    }

    public void setItem(IItem item)
    {
        this.item = item;
    }

    protected void postCheck(List<?> list) throws Hd3dItemFilterException
    {
        if (list != null && list.size() != ids.size())
        {
            Log.LOGGER.error(MessageFormat.format(ERR, list.size(), ids.size()));
        }
    }

    @SuppressWarnings("unchecked")
    protected <P> List<P> sameSizeListOf(Object object)
    {
        return (List<P>) (CollectUtils.isEmpty(ids) ? Collections.emptyList() : CollectUtils.asList(object, ids.size()));
    }

    /* Each collection query may return different kind of return. */
    /* They MUST handle null or empty collection in doQuery method */
    public abstract List<?> doQuery() throws Hd3dException;

}
