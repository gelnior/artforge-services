package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;

import fr.hd3d.exception.Hd3dItemFilterException;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IPath;
import fr.hd3d.model.persistence.impl.hibernate.ConstituentH;
import fr.hd3d.model.persistence.impl.hibernate.ShotH;
import fr.hd3d.services.EntitiesMaps;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;


/**
 * Returns the path of the entity (if applicable) from the index. Note that the entire collection must be of the same
 * type.
 * 
 * @author try.lam
 * 
 */
public class PathCollectionQuery extends AbstractCollectionQuery
{
    public PathCollectionQuery()
    {
        super();
    }

    public PathCollectionQuery(List<?> entities, IItem item)
    {
        super(entities, item);
    }

    public void setName()
    {
        name = "Path";
    }

    public void setDescription()
    {
        description = "returns the path of the entity";
    }

    public void setAppliedEntities()
    {
        appliedEntities = new HashSet<String>(2);
        appliedEntities.add(EntitiesMaps.withClass(ConstituentH.class).getEntityName());
        appliedEntities.add(EntitiesMaps.withClass(ShotH.class).getEntityName());
    }

    public void setReturnType()
    {
        returnType = fr.hd3d.common.client.Const.JAVA_LANG_STRING;
    }

    @Override
    public List<String> doQuery() throws Hd3dItemFilterException
    {
        /* nothing to query */
        if (CollectionUtils.isEmpty(entities))
        {
            return sameSizeListOf(null);
        }

        /* make sure the entities do have path */
        if (!IPath.class.isInstance(entities.get(0)))
        {
            return sameSizeListOf(null);
        }

        List<String> ret = new ArrayList<String>(entities.size());

        /* retrieve path from lucene index */
        Collection<Object[]> result = new ArrayList<Object[]>();
        BooleanQuery bq = new BooleanQuery();
        for (List<Long> partition : idPartitions)
        {
            for (final Long id : partition)
            {
                TermQuery tq = new TermQuery(new Term(Const.ID, String.valueOf(id)));
                bq.add(new BooleanClause(tq, BooleanClause.Occur.SHOULD));
            }

            FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
            FullTextQuery query = fullTextSession.createFullTextQuery(bq, entities.get(0).getClass());

            // query.setProjection(FullTextQuery.THIS, FullTextQuery.DOCUMENT_ID, FullTextQuery.DOCUMENT);
            query.setProjection(FullTextQuery.DOCUMENT);
            result.addAll(query.list());
        }

        /* reorder */
        Document document = null;
        Collection<Object[]> tmp = result;
        for (final Long id : ids)
        {
            Object[] toRemove = null;
            for (Object[] tuple : tmp)
            {
                document = (Document) tuple[0];
                org.apache.lucene.document.Field docId = document.getField(Const.ID);
                if (id.equals(NumberUtils.toLong(docId.stringValue())))
                {
                    org.apache.lucene.document.Field path = document.getField("path");
                    if (path != null)
                    {
                        ret.add(document.getField("path").stringValue());
                    }
                    else
                    {
                        ret.add(null);
                    }
                    // found = true;
                    toRemove = tuple;
                    break;
                }
            }
            // if (!found)
            // {
            // ret.add(null);
            // }

            if (toRemove != null)
            {
                tmp.remove(toRemove);
            }
        }

        postCheck(ret);

        return ret;
    }
}
