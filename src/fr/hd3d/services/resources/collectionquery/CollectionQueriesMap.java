package fr.hd3d.services.resources.collectionquery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.multimap.MultiHashMap;

import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


public class CollectionQueriesMap
{
    @SuppressWarnings("serial")
    public static Map<String, Class<? extends AbstractCollectionQuery>> map =

    Collections.unmodifiableMap(new HashMap<String, Class<? extends AbstractCollectionQuery>>() {
        {
            put(key(ApprovalNotesCollectionQuery.class), ApprovalNotesCollectionQuery.class);
            put(key(NbOfTaskOKCollectionQuery.class), NbOfTaskOKCollectionQuery.class);
            put(key(NbOfTaskSTANDBYCollectionQuery.class), NbOfTaskSTANDBYCollectionQuery.class);
            put(key(NbOfTaskWIPCollectionQuery.class), NbOfTaskWIPCollectionQuery.class);
            put(key(NbOfTaskWAITAPPCollectionQuery.class), NbOfTaskWAITAPPCollectionQuery.class);
            put(key(NbOfTaskCANCELLEDCollectionQuery.class), NbOfTaskCANCELLEDCollectionQuery.class);
            put(key(NbOfTaskCLOSEDCollectionQuery.class), NbOfTaskCLOSEDCollectionQuery.class);

            put(key(TaskBoundEntityCollectionQuery.class), TaskBoundEntityCollectionQuery.class);
            put(key(TaskBoundEntityParentIdCollectionQuery.class), TaskBoundEntityParentIdCollectionQuery.class);
            put(key(TaskDurationCollectionQuery.class), TaskDurationCollectionQuery.class);
            put(key(TaskWorkersCollectionQuery.class), TaskWorkersCollectionQuery.class);
            put(key(ActivitiesProjectTypeCollectionQuery.class), ActivitiesProjectTypeCollectionQuery.class);
            put(key(ActivitiesTaskTypeCollectionQuery.class), ActivitiesTaskTypeCollectionQuery.class);
            put(key(TotalNbOfTasksCollectionQuery.class), TotalNbOfTasksCollectionQuery.class);
            put(key(PathCollectionQuery.class), PathCollectionQuery.class);
            put(key(TaskActivitiesDurationCollectionQuery.class), TaskActivitiesDurationCollectionQuery.class);
            put(key(TaskBoundEntityThumbnailCollectionQuery.class), TaskBoundEntityThumbnailCollectionQuery.class);
            put(key(TaskBoundEntityFrameQuery.class), TaskBoundEntityFrameQuery.class);
            put(key(TaskBoundEntityPathCollectionQuery.class), TaskBoundEntityPathCollectionQuery.class);
            put(key(StepsByTaskTypeCollectionQuery.class), StepsByTaskTypeCollectionQuery.class);
            put(key(TaskBoundEntityApprovalNotesCollectionQuery.class),
                    TaskBoundEntityApprovalNotesCollectionQuery.class);
        }
    });

    /**
     * Key=EntityName, Value=List of CollectionQueries that apply on the entity
     */
    public static MultiHashMap<String, AbstractCollectionQuery> appliedEntitiesMap = new MultiHashMap<String, AbstractCollectionQuery>();
    public static List<Map<String, String>> description = new ArrayList<Map<String, String>>();
    static
    {
        for (final Map.Entry<String, Class<? extends AbstractCollectionQuery>> entry : map.entrySet())
        {
            AbstractCollectionQuery cq = null;
            try
            {
                cq = entry.getValue().newInstance();
            }
            catch (InstantiationException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
            catch (IllegalAccessException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }

            if (cq == null)
            {
                continue;
            }

            for (final String appliedEntity : cq.getAppliedEntities())
            {
                appliedEntitiesMap.put(appliedEntity, cq);

            }
            Map<String, String> map = new HashMap<String, String>();
            map.put("classname", entry.getKey());
            map.put("name", cq.getName());
            map.put("description", cq.getDescription());
            description.add(map);
        }
    }

    private CollectionQueriesMap()
    {}

    public static String key(Class<?> c)
    {
        return c.getCanonicalName();
    }
}
