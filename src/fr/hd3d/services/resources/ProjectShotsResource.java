package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetShotsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET: Returns all shots for given project.\r\n" + "\r\nPUT: N/A\r\n"
        + "\r\nPOST: Creates a shot only if this shot is linked to a sequence" + "of the project\r\n"
        + "\r\nDELETE: N/A")
public class ProjectShotsResource extends AbstractHd3dCollection<ILShot, IShot>
{
    public ProjectShotsResource() throws Hd3dException
    {
        super(Persistors.shot, Translators.shot);
    }

    @Override
    protected ResultCollection<IShot> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetShotsCmd(getResourceContext()).wantTotalSize());
    }
}
