package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.STUDIOMAP_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILStudioMap;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single studio map object (id ford parameter) <br>
 * URI : /studiomaps/{id}
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'une carte de studio\r\n" + "\r\nPUT:Modification d'une carte de studio\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:Désactivation d'une carte de studio")
public class StudioMapResource extends AbstractHd3dResource<ILStudioMap, IStudioMap>
{
    @Override
    protected IStudioMap initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(STUDIOMAP_ID_ATTRIBUTE, lockMode);
    }

    public StudioMapResource() throws Hd3dException
    {
        super(Persistors.studiomap, Translators.studiomap);
    }
}
