package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ASSETREVISIONGROUP_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class AssetRevisionGroupResource extends AbstractHd3dResource<ILAssetRevisionGroup, IAssetRevisionGroup>
{

    public AssetRevisionGroupResource() throws Hd3dException
    {
        super(Persistors.assetrevisiongroup, Translators.assetrevisiongroup);
    }

    @Override
    protected IAssetRevisionGroup initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ASSETREVISIONGROUP_ID_ATTRIBUTE, lockMode);
    }
}
