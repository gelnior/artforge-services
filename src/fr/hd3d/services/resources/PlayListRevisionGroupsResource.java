package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public final class PlayListRevisionGroupsResource extends
        AbstractHd3dCollection<ILPlayListRevisionGroup, IPlayListRevisionGroup>
{

    public PlayListRevisionGroupsResource() throws Hd3dException
    {
        super(Persistors.playlistrevisiongroup, Translators.playlistrevisiongroup);
    }
}
