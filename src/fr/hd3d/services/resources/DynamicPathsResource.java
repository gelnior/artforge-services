package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILDynamicPath;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * HD3D
 */
@Hd3dComment("GET:Récupération de la liste des dynamic paths\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'un nouveau dynamic path\r\n" + "\r\nDELETE:N/A")
public class DynamicPathsResource extends AbstractHd3dCollection<ILDynamicPath, IDynamicPath>
{
    public DynamicPathsResource() throws Hd3dException
    {
        super(Persistors.dynamicpath, Translators.dynamicpath);
    }
}
