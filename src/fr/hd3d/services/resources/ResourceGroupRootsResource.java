package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetGroupRootsCmd;
import fr.hd3d.model.translator.Translators;


public class ResourceGroupRootsResource extends AbstractHd3dCollection<ILResourceGroup, IResourceGroup>
{
    public ResourceGroupRootsResource() throws Hd3dException
    {
        super(Persistors.resourcegroup, Translators.resourcegroup);
    }

    @Override
    protected ResultCollection<IResourceGroup> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetGroupRootsCmd(getResourceContext()).wantTotalSize());
    }
}
