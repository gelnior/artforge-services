package fr.hd3d.services.resources;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.utils.BeanUtils;
import fr.hd3d.utils.HibernateUtil;


/**
 * return response from a free HQL query. Ex: select C.id, C.version from ... as C where ...
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class FreeQueryResource<L, P> extends AbstractHd3dBaseResource
{
    final static String QUERY = "query";

    public FreeQueryResource() throws Hd3dException
    {
        super();// no object provider, no translator for free query
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    public Representation get(Variant variant)
    {
        String query = getQuery().getValues(QUERY);

        if (query == null || "".equals(query))
        {
            return new StringRepresentation("Please provide query: /freequery?query=(HQL query)");
        }

        Session session = HibernateUtil.currentSession();
        Query q = session.createQuery(query);

        // aliases of the result of the query
        // ex: select description as mydesc, id as myid from .... will return
        // [0]="mysdesc", [1]="myid"
        boolean hasAliases = q.getReturnAliases() != null;

        // the result of the query can be a list of any kind of objects
        List result = q.list();

        // wrap the result, since raw response from hibernate just has values and not attribute name
        // Note: if the result contains many fields, a Object[] is returned for each row
        List wrapped = new ArrayList();
        if (result != null)
            for (Object o : result)
            {
                if (o == null)
                    continue;

                // ex: select ... as <alias> from ...
                if (hasAliases)
                {
                    // many fields
                    if (o instanceof Object[])
                    {
                        // beans.add(toBean(q.getReturnAliases(), q.getReturnTypes(), (Object[]) o));
                        wrapped.add(BeanUtils.toMap(q.getReturnAliases(), (Object[]) o));
                    }
                    else
                    {
                        // single field
                        // beans.add(toBean(q.getReturnAliases()[0], q.getReturnTypes()[0], o));
                        wrapped.add(BeanUtils.toMap(q.getReturnAliases()[0], o));
                    }
                }// ex: from <returnedClass> ...
                else if (q.getReturnTypes()[0] != null)
                {
                    Class<ILBase> returnedClass = q.getReturnTypes()[0].getReturnedClass();
                    // TODO: redirect to the business object getRepresentation ?
                }
            }

        Representation r = null;
        if (result != null && !result.isEmpty())
        {
            r = RessourceSerializer.getRepresentation(variant, wrapped);
            r.setCharacterSet(CharacterSet.UTF_8);
        }
        return r;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
