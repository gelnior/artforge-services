package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectPersonActivitiesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * A resource to access to specific person's activities in a project.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des activities d'une personne sur un projet\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'une activité\r\n" + "\r\nDELETE:N/A")
public class ProjectPersonActivitiesResource extends AbstractHd3dCollection<ILActivity, IActivity>
{
    public ProjectPersonActivitiesResource() throws Hd3dException
    {
        super(Persistors.activity, Translators.activity);
    }

    @Override
    protected ResultCollection<IActivity> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectPersonActivitiesCmd(getResourceContext()).wantTotalSize());
    }
}
