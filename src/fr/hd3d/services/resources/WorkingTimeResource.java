package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.WORKING_TIME_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILWorkingTime;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * A resource to access to a specific working time.
 * 
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération des informations d'un working time\r\n" + "\r\nPUT:Modification d'un working time\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class WorkingTimeResource extends AbstractHd3dResource<ILWorkingTime, IWorkingTime>
{
    public WorkingTimeResource() throws Hd3dException
    {
        super(Persistors.workingTime, Translators.workingTime);
    }

    @Override
    protected IWorkingTime initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(WORKING_TIME_ID_ATTRIBUTE, lockMode);
    }
}
