package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILComposition;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class CompositionsResource extends AbstractHd3dCollection<ILComposition, IComposition>
{
    public CompositionsResource() throws Hd3dException
    {
        super(Persistors.composition, Translators.composition);
    }
}
