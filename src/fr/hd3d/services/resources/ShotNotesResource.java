package fr.hd3d.services.resources;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetWorkObjectNotesCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.Const;


/**
 * ShotNotesResource returns notes linked to given shot.
 * 
 * @author HD3D
 */
public class ShotNotesResource extends AbstractHd3dCollection<ILApprovalNote, IApprovalNote>
{
    public ShotNotesResource() throws Hd3dException
    {
        super(Persistors.approvalnote, Translators.approvalnote);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetWorkObjectNotesCmd(getResourceContext(), ServicesURI.SHOT_ID_ATTRIBUTE,
                Const.SHOT).wantTotalSize());
    }
}
