package fr.hd3d.services.resources;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.services.security.utils.AuthenticationUtil;


/**
 * Class to allow user to access for writing to all his approval notes. TODO: Ensure that current user is the owner of
 * edited approval note.
 * 
 * @author HD3D
 * 
 */
public class MyNoteResource extends ApprovalNoteResource
{

    public MyNoteResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected void doPrePersist(Representation entity, IApprovalNote newObject) throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user == null)
            return;
        super.doPrePersist(entity, newObject);
    }
}
