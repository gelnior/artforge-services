package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILDevice;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all software objects.<br>
 * URI : /softwares/
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération de tous les périphériques\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Création d'un périphérique\r\n" + "\r\nDELETE:N/A")
public class DevicesResource extends AbstractHd3dCollection<ILDevice, IDevice>
{

    public DevicesResource() throws Hd3dException
    {
        super(Persistors.device, Translators.device);
    }
}
