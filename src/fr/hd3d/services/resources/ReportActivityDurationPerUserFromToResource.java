package fr.hd3d.services.resources;

import java.util.HashMap;
import java.util.Map;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;


public class ReportActivityDurationPerUserFromToResource extends PersonDaysResource
{
    public ReportActivityDurationPerUserFromToResource() throws Hd3dException
    {
        super();
        allowMethods(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection<IPersonDay> getCollectionForRepresentation() throws Hd3dException
    {
        ResultCollection<IPersonDay> personDays = super.getCollectionForRepresentation();

        Map<String, Long> result = new HashMap<String, Long>(personDays.getCount());
        for (final IPersonDay personDay : personDays.getResult())
        {
            result.put(personDay.getPerson().getLogin(), totalDuration(personDay));
        }

        return null;
    }

    private Long totalDuration(IPersonDay personDay)
    {
        return 0L;
    }
}
