package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PERSON_ID_ATTRIBUTE;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILAbsence;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonAbsencesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * A resource to access to specific person's tasks.
 * 
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET: Full absence  list for the specified person. <br />" + "PUT: N/A<br />"
        + "POST: Create a new absence for the specified person.<br />" + "\r\nDELETE: N/A")
public class PersonAbsencesResource extends AbstractHd3dCollection<ILAbsence, IAbsence>
{
    public PersonAbsencesResource() throws Hd3dException
    {
        super(Persistors.absence, Translators.absence);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetPersonAbsencesCmd(getResourceContext()).wantTotalSize());
    }

    @Override
    protected void doPersist(IAbsence object) throws Hd3dException
    {
        final Long personId = getResourceContext().getIdFromUrl(PERSON_ID_ATTRIBUTE);
        IPerson person = Persistors.person.getById(personId);
        object.setWorker(person);

        super.doPersist(object);
    }
}
