package fr.hd3d.services.resources;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.LockMode;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.impl.LSheetFilter;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetSheetFiltersBySheetCollectionCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.model.preferences.SheetFilter;
import fr.hd3d.model.translator.impl.SheetFilterTranslator;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.services.translator.json.JSonFormSerializer;
import fr.hd3d.utils.HibernateUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la listes des filtres d'une sheet." + "\r\nPUT:N/A\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:N/A")
public class SheetSheetFiltersResource extends AbstractHd3dCollectionResource<LSheetFilter, SheetFilter>
{
    final boolean self = "self".equals(getRequest().getOriginalRef().getLastSegment());
    IPerson person = AuthenticationUtil.getCurrentUser();

    public SheetSheetFiltersResource() throws Hd3dException
    {
        super(SheetFilter.class, new SheetFilterTranslator());
        allowMethods("GET");
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection<SheetFilter> getCollectionForRepresentation() throws Hd3dException
    {
        return getCollection(new GetSheetFiltersBySheetCollectionCmd(getResourceContext()).wantTotalSize());
    }

    @SuppressWarnings("unchecked")
    protected ResultCollection reloadCollectionIfAny(final ResultCollection luceneFiltered, boolean onIdsOnly)
            throws Hd3dException
    {
        return luceneFiltered;
    }

    @Override
    protected List<LSheetFilter> getLightWeightList(ResultCollection<SheetFilter> list) throws Hd3dException
    {
        return getResourceContext().getTranslator().toLightweightCollection(list.getResult(), getResourceContext());
    }

    @Override
    protected Representation getLightWeightRepresentation(Variant variant, List<LSheetFilter> lightweightList,
            int nbRecords)
    {
        return RessourceSerializer.<LSheetFilter> getRepresentation(variant, lightweightList, nbRecords);
    }

    protected void doPrePersistCollection(Representation entity, List<SheetFilter> newObjects)
            throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    protected void doPersist(final List<SheetFilter> objects) throws Hd3dPersistenceException
    {
        if (objects == null || objects.isEmpty())
            return;

        HibernateUtil.safeTransaction(new IHibernateTransaction() {

            public void execute(org.hibernate.Session session) throws Hd3dException
            {
                for (SheetFilter obj : objects)
                {
                    session.save(obj);
                    // session.flush();
                }
            }
        });
    }

    protected void doPostPersist(List<SheetFilter> objects) throws Hd3dPersistenceException
    {
        successfulPost(objects);
    }

    protected void doPrePersist(Representation entity, SheetFilter object) throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    protected SheetFilter newObject(Representation entity) throws Hd3dException
    {
        final LSheetFilter lObject = JSonFormSerializer.<LSheetFilter> deserialize(entity);
        SheetFilterTranslator translator = new SheetFilterTranslator();
        final SheetFilter newObject = translator.fromLightweight(lObject, getResourceContext());

        return newObject;
    }

    protected List<SheetFilter> newCollection(Representation entity) throws Hd3dException
    {
        List<LSheetFilter> lObjects = JSonFormSerializer.<LSheetFilter> deserializeList(entity);

        List<SheetFilter> objects = new ArrayList<SheetFilter>(lObjects.size());
        for (LSheetFilter light : lObjects)
        {
            SheetFilterTranslator translator = new SheetFilterTranslator();
            SheetFilter setting = translator.fromLightweight(light, getResourceContext());
            objects.add(setting);
        }

        /* cleaning */
        for (LSheetFilter light : lObjects)
            light.clear();
        lObjects.clear();
        lObjects = null;

        return objects;
    }

    protected List<SheetFilter> retrieveObjects(Representation entity, List<LSheetFilter> lObjects, LockMode lockMode)
            throws Hd3dTranslationException
    {
        return null;
    }

    protected void doPersist(final SheetFilter object) throws Hd3dPersistenceException
    {
        HibernateUtil.safeTransaction(new IHibernateTransaction() {

            public void execute(org.hibernate.Session session) throws Hd3dException
            {
                session.save(object);
                // session.flush();
            }
        });
    }

    protected void doPostPersist(SheetFilter object) throws Hd3dPersistenceException
    {
        successfulPost(object);
    }

    protected String getNewResourceIdentifier(Object newObject)
    {
        StringBuilder buf = new StringBuilder(getRequest().getResourceRef().getBaseRef().toString());
        buf.append('/').append(((SheetFilter) newObject).getId());
        return buf.toString();
    }

    @Override
    protected void doPostUpdate(List<SheetFilter> objects, Representation entity) throws Hd3dTranslationException
    {}

    @Override
    protected void doPreUpdate(Representation entity, List<LSheetFilter> lObjects, List<SheetFilter> objects)
            throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    @Override
    protected void doUpdate(List<LSheetFilter> lObjects, List<SheetFilter> objects) throws Hd3dPersistenceException
    {/* nothing to do */}

    // ============================================================
    //
    // Override DELETE methods
    //
    // ============================================================
    protected boolean preCheck(final List<SheetFilter> objects)
    {
        return true;
    }

    protected void doPreDelete(final List<SheetFilter> objects) throws Hd3dException
    {
    /* nothing to do */
    }

    protected void doDelete(final List<SheetFilter> objects) throws Hd3dException
    {
        ((HibernatePersist) getResourceContext().getPersistor()).deleteRawCollection(objects);
    }

    protected void doPostDelete(final List<SheetFilter> objects) throws Hd3dException
    {
        /* nothing to do */
        getResponse().setStatus(Status.SUCCESS_OK);
    }
}
