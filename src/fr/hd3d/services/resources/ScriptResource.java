package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.common.client.ServicesURI.SCRIPT_ID_ATTRIBUTE;

import java.util.List;

import org.hibernate.LockMode;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.exec.ScriptExec;
import fr.hd3d.model.lightweight.ILScript;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ScriptH;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.ServerResourceUtils;


/**
 * @author Try LAM
 */
public class ScriptResource extends AbstractHd3dResource<ILScript, IScript>
{
    public ScriptResource() throws Hd3dException
    {
        super(Persistors.script, Translators.script);
    }

    @Override
    protected IScript initializeObject(LockMode lockMode) throws Hd3dException
    {
        String scriptId = (String) getRequest().getAttributes().get(SCRIPT_ID_ATTRIBUTE);
        // if id is passed, return the requested script
        if (isRequestedById())
        {
            return super.initializeObject(SCRIPT_ID_ATTRIBUTE, lockMode);
        }
        else
        { // otherwise, find it out by name
            return (IScript) getResourceContext().getPersistor().getObjectByValue("name", scriptId);
        }
    }

    private boolean isRequestedById()
    {
        // scriptId may be the ScriptHibernateImpl id or its name
        String scriptId = (String) getRequest().getAttributes().get(SCRIPT_ID_ATTRIBUTE);
        return org.apache.commons.lang.math.NumberUtils.isNumber(scriptId);
    }

    @Override
    public Representation get(Variant variant)
    {
        if (isRequestedById())
            return super.get(variant);

        if (!initObject())
        {
            return getResponseEntity();
        }
        if (object == null)
        {
            /*
             * NOTE: do not set Status to Status.SUCCESS_NO_CONTENT If so, there will be no empty list returned in the
             * HTML screen, which may be confusing
             */
            return getResponseEntity();
        }

        // if requested by name, execute the script identified by its name, passing it parameters
        // ex: http://..../scripts/{script_name}?params=...
        ScriptExec scriptExec = new ScriptExec(getQuery().getValuesMap());
        try
        {
            String exitValues = scriptExec.exec(object);
            Representation r = new StringRepresentation(exitValues);
            r.setCharacterSet(CharacterSet.UTF_8);
            Log.LOGGER.info(responseInfo());
            return ServerResourceUtils.encodeRepresentation(r);
        }
        catch (Hd3dPersistenceException e)
        {
            error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
            return getResponseEntity();
        }
    }

    @Override
    protected void doPostPersist(IScript object) throws Hd3dException
    {
        super.doPostPersist(object);
        /* refresh in-memory scripts table */
        ScriptH.refreshScriptsMap = true;
    }

    @Override
    protected void doPostPersist(List<IScript> objects) throws Hd3dException
    {
        super.doPostPersist(objects);
        /* refresh in-memory scripts table */
        ScriptH.refreshScriptsMap = true;
    }

    @Override
    protected void doPostUpdate(Representation entity) throws Hd3dException
    {
        super.doPostUpdate(entity);
        /* refresh in-memory scripts table */
        ScriptH.refreshScriptsMap = true;
    }

    @Override
    protected void doPostDelete() throws Hd3dException
    {
        super.doPostDelete();
        /* refresh in-memory scripts table */
        ScriptH.refreshScriptsMap = true;
    }
}
