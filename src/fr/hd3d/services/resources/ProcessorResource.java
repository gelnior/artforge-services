package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PROCESSOR_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILProcessor;
import fr.hd3d.model.persistence.IProcessor;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computers/{id}
 * 
 * @author HD3D
 */
public class ProcessorResource extends AbstractHd3dResource<ILProcessor, IProcessor>
{
    @Override
    protected IProcessor initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(PROCESSOR_ID_ATTRIBUTE, lockMode);
    }

    public ProcessorResource() throws Hd3dException
    {
        super(Persistors.processor, Translators.processor);
    }
}
