package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.GRAPHICAL_ANNOTATION_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILGraphicalAnnotation;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class GraphicalAnnotationResource extends AbstractHd3dResource<ILGraphicalAnnotation, IGraphicalAnnotation>
{

    public GraphicalAnnotationResource() throws Hd3dException
    {
        super(Persistors.graphicalannotation, Translators.graphicalannotation);
    }

    @Override
    protected IGraphicalAnnotation initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(GRAPHICAL_ANNOTATION_ID_ATTRIBUTE, lockMode);
    }
}
