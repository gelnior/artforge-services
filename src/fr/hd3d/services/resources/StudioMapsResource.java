package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILStudioMap;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all software objects.<br>
 * URI : /studiomaps/
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération de tous les cartes de studio\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Création d'une carte de studio\r\n" + "\r\nDELETE:N/A")
public class StudioMapsResource extends AbstractHd3dCollection<ILStudioMap, IStudioMap>
{

    public StudioMapsResource() throws Hd3dException
    {
        super(Persistors.studiomap, Translators.studiomap);
    }
}
