package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.MANUFACTURER_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILManufacturer;
import fr.hd3d.model.persistence.IManufacturer;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computers/{id}
 * 
 * @author HD3D
 */
public class ManufacturerResource extends AbstractHd3dResource<ILManufacturer, IManufacturer>
{
    @Override
    protected IManufacturer initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(MANUFACTURER_ID_ATTRIBUTE, lockMode);
    }

    public ManufacturerResource() throws Hd3dException
    {
        super(Persistors.manufacturer, Translators.manufacturer);
    }
}
