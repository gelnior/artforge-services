package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.io.File;
import java.util.Date;

import org.apache.commons.lang.SystemUtils;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


/**
 * @author HD3D
 */

public class ProxiesDownloadResource extends AbstractHd3dBaseResource
{
    protected final static String UPLOAD_DIR = Conf.getUploadDir();
    protected final static String NO_IMAGE_PATH = UPLOAD_DIR + SystemUtils.FILE_SEPARATOR + "noimage.jpeg";

    public static void init() throws Hd3dException
    {
        // make sure needed resources are present
        if (!new File(NO_IMAGE_PATH).exists())
            throw new Hd3dException(NO_IMAGE_PATH + " cannot be found!");
    }

    public ProxiesDownloadResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
    }

    @Override
    public Representation get(Variant variant)
    {
        try
        {
            FileRepresentation rep;
            try
            {
                IProxy proxy = Persistors.proxy.getById(getResourceContext().getIdFromUrl(
                        ServicesURI.PROXY_ID_ATTRIBUTE));

                if (proxy != null)
                {
                    String fullPath = proxy.getMountPoint() + '/' + proxy.getLocation() + '/' + proxy.getFilename();

                    File f = new File(fullPath);

                    if (f.exists())
                    {
                        rep = new FileRepresentation(f, MediaType.valueOf(proxy.getProxyType().getOpenType()));
                        Disposition disposition = new Disposition();
                        disposition.setFilename(proxy.getFilename());
                        disposition.setType(proxy.getProxyType().getOpenType());
                        disposition.setModificationDate(proxy.getVersion());
                        rep.setDisposition(disposition);
                    }
                    else
                    {

                        rep = new FileRepresentation(new File(NO_IMAGE_PATH), MediaType.IMAGE_JPEG);
                        Disposition disposition = new Disposition();
                        disposition.setFilename("notfound.jpg");
                        disposition.setType(MediaType.IMAGE_JPEG.getName());
                        disposition.setModificationDate(proxy.getVersion());
                        rep.setDisposition(disposition);
                    }
                }
                else
                {
                    Log.LOGGER.error("*** Error: Proxy is null");
                    rep = new FileRepresentation(new File(NO_IMAGE_PATH), MediaType.IMAGE_JPEG);
                    Disposition disposition = new Disposition();
                    disposition.setFilename("notfound.jpg");
                    disposition.setType(MediaType.IMAGE_JPEG.getName());
                    disposition.setModificationDate(new Date());
                    rep.setDisposition(disposition);
                    return rep;
                }
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error("*** Error: Exception found!");
                error(Status.CLIENT_ERROR_BAD_REQUEST, e, ERROR_OCCURED);
                String error = ExceptUtils.format(e);
                Log.LOGGER.error(error);
                rep = new FileRepresentation(new File(NO_IMAGE_PATH), MediaType.IMAGE_JPEG);
                Disposition disposition = new Disposition();
                disposition.setFilename("notfound.jpg");
                disposition.setType(MediaType.IMAGE_JPEG.getName());
                disposition.setModificationDate(new Date());
                rep.setDisposition(disposition);
                return rep;
                // return new StringRepresentation("Could not open the specified file");
            }

            getResponse().setEntity(rep);
            return rep;
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
