package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSoftware;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to all software objects.<br>
 * URI : /softwares/
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération de tous les logiciels\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:Création d'un logiciel\r\n"
        + "\r\nDELETE:N/A")
public class SoftwaresResource extends AbstractHd3dCollection<ILSoftware, ISoftware>
{

    public SoftwaresResource() throws Hd3dException
    {
        super(Persistors.software, Translators.software);
    }
}
