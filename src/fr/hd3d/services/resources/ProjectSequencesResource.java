package fr.hd3d.services.resources;

import java.util.ArrayList;
import java.util.List;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectSequencesCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.translator.json.JSonFormSerializer;


/**
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des séquences princinpales (= premier niveau sous la racine) dans la bibliothèque des éléments temporels (triées par ordre alphabétique des labels)\r\n"
        + "\r\nPUT:Mise à jour d'une LISTE de sequences (ou sous-sequences)\r\n"
        + "\r\nPOST:Ajout d'une SEQUENCE PRINCIPALE par le nom en ajoutant à l'url: ?mode=bulk, Ajout d'une LISTE de séquences\r\n"
        + "\r\nDELETE:N/A")
public class ProjectSequencesResource extends AbstractHd3dCollection<ILSequence, ISequence>
{
    public ProjectSequencesResource() throws Hd3dException
    {
        super(Persistors.sequence, Translators.sequence);
    }

    @Override
    protected ResultCollection<ISequence> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectSequencesCmd(getResourceContext()).wantTotalSize());
    }

    @Override
    protected ISequence newObject(Representation entity) throws Hd3dException
    {
        ILSequence lsequence = JSonFormSerializer.<ILSequence> deserialize(entity);
        ISequence sequence = getResourceContext().getTranslator().fromLightweight(lsequence, getResourceContext());
        if (sequence.getParent() == null)
            sequence.setParent(sequence.getProject().rootSequence());

        return sequence;
    }

    @Override
    protected List<ISequence> newCollection(Representation entity) throws Hd3dException
    {
        List<ILSequence> lsequences = JSonFormSerializer.<ILSequence> deserializeList(entity);
        List<ISequence> objects = new ArrayList<ISequence>(lsequences.size());
        for (ILSequence s : lsequences)
        {
            ISequence sequence = getResourceContext().getTranslator().fromLightweight(s, getResourceContext());
            if (sequence.getParent() == null)
                sequence.setParent(sequence.getProject().rootSequence());
            objects.add(sequence);
        }

        return objects;
    }

    // @Override
    // protected List<ISequence> doPrePersistCollection(Representation entity, Map<String, String> map)
    // throws Hd3dTranslationException
    // {
    // List<ILSequence> lsequences = JSonFormSerializer.<ILSequence> deserializeList(entity);
    // List<ISequence> objects = new ArrayList<ISequence>(lsequences.size());
    // for (ILSequence s : lsequences)
    // {
    // ISequence sequence = translator.fromLightweight(s, map);
    // if (sequence.getParent() == null)
    // sequence.setParent(sequence.getProject().rootSequence());
    // objects.add(sequence);
    //
    // // PrePersist
    // sequence.doPrePersist();
    // executeScripts(sequence, IPersist.PREPERSIST, getNewIdentifier(sequence));
    // }
    //
    // return objects;
    // }

    // @Override
    // protected ISequence doPrePersist(Representation entity, Map<String, String> map) throws Hd3dTranslationException
    // {
    // ILSequence lsequence = JSonFormSerializer.<ILSequence> deserialize(entity);
    // ISequence sequence = translator.fromLightweight(lsequence, map);
    // if (sequence.getParent() == null)
    // sequence.setParent(sequence.getProject().rootSequence());
    //
    // // PrePersist
    // sequence.doPrePersist();
    // executeScripts(sequence, IPersist.PREPERSIST, getNewIdentifier(sequence));
    // return sequence;
    // }
}
