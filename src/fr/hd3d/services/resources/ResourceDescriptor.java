package fr.hd3d.services.resources;

import org.restlet.resource.ServerResource;


public class ResourceDescriptor
{
    public String url;
    public String comment = "";
    public Class<? extends ServerResource> resourceClass;
    public String section;

    public ResourceDescriptor(String url, String comment, Class<? extends ServerResource> resourceClass, String section)
    {
        this.url = url;
        if (comment != null)
            this.comment = comment;
        if (resourceClass != null)
            this.resourceClass = resourceClass;
        this.section = section;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public Class<? extends ServerResource> getResourceClass()
    {
        return resourceClass;
    }

    public void setResourceClass(Class<? extends ServerResource> resourceClass)
    {
        this.resourceClass = resourceClass;
    }

    public String getSection()
    {
        return section;
    }

    public void setSection(String section)
    {
        this.section = section;
    }

}
