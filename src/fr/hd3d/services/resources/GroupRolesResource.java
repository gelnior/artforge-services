package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetGroupRolesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * 
 * @author florent-della-valle
 * 
 */
public class GroupRolesResource extends AbstractHd3dCollection<ILRole, IRole>
{
    public GroupRolesResource() throws Hd3dException
    {
        super(Persistors.role, Translators.role);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetGroupRolesCmd(getResourceContext()).wantTotalSize());
    }
}
