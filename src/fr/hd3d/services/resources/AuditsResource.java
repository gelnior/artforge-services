package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.util.Collection;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.audit.Audit;
import fr.hd3d.model.persistence.impl.hibernate.ComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory;
import fr.hd3d.model.persistence.impl.hibernate.query.GetAuditsCollectionCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateQuery;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.ServerResourceUtils;


public class AuditsResource<L, P> extends AbstractHd3dBaseResource
{

    public AuditsResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Representation get(Variant variant)
    {
        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();
        try
        {
            // Set the query filter.
            getResourceContext().setFilter(
                    QueryFilterFactory.getFilter(Audit.class, getQuery().getValuesMap(), ComplexTypeProvider.INSTANCE
                            .getProvider(Audit.class)));

            final GetAuditsCollectionCmd cmd = new GetAuditsCollectionCmd(getResourceContext());
            HibernateUtil.safeQuery((IHibernateQuery) cmd);
            ResultCollection resultCollection = new ResultCollection(cmd.result, cmd.totalCount);

            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            tx.commit();

            Representation r = RessourceSerializer.getRepresentation(variant, (Collection<Audit>) resultCollection
                    .getResult(), resultCollection.getCount());
            r.setCharacterSet(CharacterSet.UTF_8);
            return ServerResourceUtils.encodeRepresentation(r);
        }
        catch (Hd3dPersistenceException e)
        {
            error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
            return getResponseEntity();
        }
        catch (Hd3dException e)
        {
            error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
            return getResponseEntity();
        }
        finally
        {
            if (tx != null)
                tx = null;
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
