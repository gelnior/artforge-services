package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.SCREEN_MODEL_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILScreenModel;
import fr.hd3d.model.persistence.IScreenModel;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single computer object (id ford parameter) <br>
 * URI : /computers/{id}
 * 
 * @author HD3D
 */
public class ScreenModelResource extends AbstractHd3dResource<ILScreenModel, IScreenModel>
{
    @Override
    protected IScreenModel initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(SCREEN_MODEL_ID_ATTRIBUTE, lockMode);
    }

    public ScreenModelResource() throws Hd3dException
    {
        super(Persistors.screenModel, Translators.screenModel);
    }
}
