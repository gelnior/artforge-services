package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'un projet\r\n" + "\r\nPUT:Mise a jour d'un projet\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Désactivation du projet")
public class ProjectResource extends AbstractHd3dResource<ILProject, IProject>
{
    public ProjectResource() throws Hd3dException
    {
        super(Persistors.project, Translators.project);
    }

    @Override
    protected IProject initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(PROJECT_ID_ATTRIBUTE, lockMode);
    }
}
