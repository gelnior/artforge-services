package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.common.client.ServicesURI.FILEREVISIONS;
import static fr.hd3d.common.client.ServicesURI.REUSEUPLOAD;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.shiro.util.StringUtils;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILFileRevision;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.utils.FileRevisionUtils;
import fr.hd3d.utils.FilenameMaker;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.TagUtils;


/**
 * @author Nicolas Jauffret
 */

@Hd3dComment("Webservice d'upload de fichiers pour la réutilisation (fichiers associés à une prod/banque) <br>\n"
        + "POST/Multipart: file= fichier à uploader; thumbnail= imagette correspondant au fichier; preview= vidéo de preview correspondant au fichier; <br>\n"
        + "catId/constId/seqId/shotId= id du parent à qui lier le fichier; tagLabel= label du tag à créer/associer au fichier; tagId= id du tag à associer au fichier; <br>\n"
        + "tagIdArray/tagLabelArray= la même chose mais séparé par des ',' pour en passer plusieurs"
        + "<br>\r\nPUT:N/A\r\n" + "<br>\r\nDELETE:N/A")
public class ReuseUploadResource extends AbstractHd3dUploadBaseResource<ILFileRevision, IFileRevision>
{
    private Vector<Long> _tagIds = new Vector<Long>();

    private final static String FILE = "file";
    private final static String THUMB_FILE = "thumbFile";
    private final static String PREVIEW_FILE = "previewFile";
    private final static String TAG_IDS = "tagIds";
    private final static String PROJECT_ID = "projId";
    private final static String CATEGORY_ID = "categoryId";
    private final static String CONSTITUENT_ID = "constituentId";
    private final static String SEQ_ID = "seqId";
    private final static String SHOT_ID = "shotId";

    /**
     * @param context
     * @param request
     * @param response
     * @return
     * @throws Hd3dException
     */
    public ReuseUploadResource() throws Hd3dException
    {
        super();
        allowMethods("POST");
    }

    @Override
    protected Representation buildRepresentation(Map<String, Object> resultMap)
    {
        IFileRevision newFileRevision = (IFileRevision) resultMap.get(FILE);

        Representation rep = null;
        if (newFileRevision == null)
        {
            rep = new StringRepresentation("0", MediaType.TEXT_PLAIN);
        }
        else
        {
            Log.LOGGER.info("*** New file: {}", "" + newFileRevision.getId());
            // file successfully uploaded
            rep = new StringRepresentation(String.valueOf(newFileRevision.getId()));
            rep.setLocationRef(this.getNewResourceIdentifier(newFileRevision));
        }
        return rep;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void parseTextFields(Map<String, Object> resultMap, FileItem fileItem)
    {
        if (resultMap == null || fileItem == null || !fileItem.isFormField())
            return;

        final String fieldName = fileItem.getFieldName();
        final String fieldValue = fileItem.getString();

        List<Long> tagIds = (List<Long>) resultMap.get(TAG_IDS);
        if (tagIds == null)
            tagIds = new ArrayList<Long>();

        if (fieldName.equals("tagId"))
        {
            final Long tagId = NumberUtils.toLong(fieldValue);
            if (TagUtils.tagExists(tagId))
                tagIds.add(tagId);
        }
        else if (fieldName.equals("tagLabel"))
        {
            tagIds.add(TagUtils.getOrCreateTagIdFromName(fieldValue));
        }
        else if (fieldName.equals("tagIdArray"))
        {
            for (String id : StringUtils.split(fieldValue, ','))
            {
                final Long tagId = NumberUtils.toLong(id);
                if (TagUtils.tagExists(tagId))
                    tagIds.add(tagId);
            }
        }
        else if (fieldName.equals("tagLabelArray"))
        {
            for (String id : StringUtils.split(fieldValue, ','))
            {
                Long newid = TagUtils.getOrCreateTagIdFromName(id);
                tagIds.add(newid);
            }
        }
        else if (fieldName.equals(PROJECT_ID))
        {
            resultMap.put(PROJECT_ID, NumberUtils.toLong(fieldValue, NumberUtils.LONG_MINUS_ONE));
        }
        else if (fieldName.equals("catId"))
        {
            resultMap.put(CATEGORY_ID, NumberUtils.toLong(fieldValue));
        }
        else if (fieldName.equals("constId"))
        {
            resultMap.put(CONSTITUENT_ID, NumberUtils.toLong(fieldValue));
        }
        else if (fieldName.equals(SEQ_ID))
        {
            resultMap.put(SEQ_ID, NumberUtils.toLong(fieldValue));
        }
        else if (fieldName.equals(SHOT_ID))
        {
            resultMap.put(SHOT_ID, NumberUtils.toLong(fieldValue));
        }

        /* do not forget to put back in the map */
        resultMap.put(TAG_IDS, tagIds);

    }

    @Override
    protected void parseUploadedFiles(Map<String, Object> resultMap, FileItem fileItem)
    {
        if (resultMap == null || fileItem == null || fileItem.isFormField())
            return;

        if (fileItem.getFieldName().equals(FILE))
        {
            resultMap.put(FILE, FileRevisionUtils.persistUploadedFile(fileItem, fileItem.getName(), "standard", 1,
                    EFileState.getDefault()));
        }
        else if (fileItem.getFieldName().equals("thumbnail"))
        {
            // TODO check jpg extension/format/resolution else nconvert
            // Also, if not given... trying to generate it
            String tmpFilename = FilenameMaker.getTemporaryFileFullPath() + "_thumb";
            File thumbFile = new File(tmpFilename);
            try
            {
                fileItem.write(thumbFile);
                if (thumbFile.length() == 0)
                {
                    thumbFile.delete();
                    thumbFile = null;
                }
            }
            catch (Exception e)
            {
                thumbFile = null;
            }

            resultMap.put(THUMB_FILE, thumbFile);
        }
        else if (fileItem.getFieldName().equals("preview"))
        {
            // TODO check flv extension/format else ffmpeg
            String tmpFilename = FilenameMaker.getTemporaryFileFullPath() + "_preview";
            File previewFile = new File(tmpFilename);
            try
            {
                fileItem.write(previewFile);
                if (previewFile.length() == 0)
                {
                    previewFile.delete();
                    previewFile = null;
                }
            }
            catch (Exception e)
            {
                previewFile = null;
            }

            resultMap.put(PREVIEW_FILE, previewFile);
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    protected void process(Map<String, Object> resultMap)
    {
        final IFileRevision newFileRevision = (IFileRevision) resultMap.get(FILE);
        final File previewFile = (File) resultMap.get(PREVIEW_FILE);
        final File thumbnailFile = (File) resultMap.get(THUMB_FILE);
        final List<Long> tagIds = (List<Long>) resultMap.get(TAG_IDS);
        final Long projectId = (Long) resultMap.get(PROJECT_ID);
        Long categoryId = (Long) resultMap.get(CATEGORY_ID);
        final Long constituentId = (Long) resultMap.get(CONSTITUENT_ID);
        Long seqId = (Long) resultMap.get(SEQ_ID);
        final Long shotId = (Long) resultMap.get(SHOT_ID);

        if (previewFile != null)
        {
            File destFile = new File(FilenameMaker.getMoviePreviewFromPath(newFileRevision));

            if (!previewFile.renameTo(destFile))
            {
                Log.LOGGER.error("*** Error: could not rename preview from '{}' to '{}'\n", previewFile
                        .getAbsolutePath(), destFile.getAbsolutePath());
                previewFile.delete();
            }
            else
            {
                Log.LOGGER.info("*** Preview file: '{}'", destFile.getAbsolutePath());
            }
        }
        if (thumbnailFile != null)
        {
            File destFile = new File(FilenameMaker.getImagePreviewFromPath(newFileRevision));

            if (!thumbnailFile.renameTo(destFile))
            {
                Log.LOGGER.error("*** Error: could not rename thumb from '{}' to '{}'\n", thumbnailFile
                        .getAbsolutePath(), destFile.getAbsolutePath());
                thumbnailFile.delete();
            }
            else
            {
                Log.LOGGER.info("*** Thumbnail file: '{}'", destFile.getAbsolutePath());
            }
        }

        try
        {
            /* Adding the tags */
            for (final Long tagId : tagIds)
                newFileRevision.addTag(tagId);

            /* Attaching the file to its parent(s) */
            IBase parent = null;
            IProject project = null;

            if (BaseH.isValidId(projectId))
            {
                categoryId = FileRevisionUtils.getCategoryRootId(projectId);

                if (!NumberUtils.LONG_MINUS_ONE.equals(categoryId) && BaseH.isValidId(categoryId))
                {
                    parent = Persistors.category.getById(categoryId);
                    project = ((ICategory) parent).getProject();
                }
                else if (!NumberUtils.LONG_MINUS_ONE.equals(constituentId) && BaseH.isValidId(constituentId))
                {
                    parent = Persistors.constituent.getById(constituentId);
                    project = ((IConstituent) parent).getCategory().getProject();
                }

                if (FileRevisionUtils.attachFileToParent(newFileRevision, parent, project) == false)
                {
                    Log.LOGGER.error("*** Error: could not attach file to {} to {}", parent.entityName(), parent
                            .getId());
                }

                seqId = FileRevisionUtils.getTemporalRootId(projectId);

                if (!NumberUtils.LONG_MINUS_ONE.equals(seqId) && BaseH.isValidId(seqId))
                {
                    parent = Persistors.sequence.getById(seqId);
                    project = ((ISequence) parent).getProject();
                }
                else if (!NumberUtils.LONG_MINUS_ONE.equals(shotId) && BaseH.isValidId(shotId))
                {
                    parent = Persistors.shot.getById(shotId);
                    ISequence seq = ((IShot) parent).getSequence();
                    if (seq == null)
                    {
                        Log.LOGGER.error("*** Error: parent sequence does not exist shotId: {}", shotId);
                        throw new Hd3dPersistenceException("*** Error: parent sequence does not exist shotId: "
                                + shotId);
                    }
                    project = seq.getProject();
                }
                if (FileRevisionUtils.attachFileToParent(newFileRevision, parent, project) == false)
                {
                    Log.LOGGER.error("*** Error: could not attach file to {} {}", parent.entityName(), parent.getId());
                }
            }
        }
        catch (Hd3dPersistenceException e)
        {
            Log.LOGGER.error("*** Error while accessing database. \n");
            error(Status.CLIENT_ERROR_BAD_REQUEST, e, ERROR_OCCURED);
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error("*** Error while accessing database. \n");
            error(Status.CLIENT_ERROR_BAD_REQUEST, e, ERROR_OCCURED);
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        StringBuilder buf = new StringBuilder(getRequest().getResourceRef().getBaseRef().toString());
        buf.delete(buf.lastIndexOf('/' + REUSEUPLOAD), buf.length());
        buf.append('/' + FILEREVISIONS).append('/').append(((IBase) object).getId());
        return buf.toString();
    }

}
