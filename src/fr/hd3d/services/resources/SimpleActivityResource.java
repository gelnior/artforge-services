/**
 * 
 */
package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.SIMPLE_ACTIVITY_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author thomas-eskenazi
 * 
 */
@Hd3dComment("GET:Récupération d'une activité de tâche {taskActivityID}\r\n"
        + "\r\nPUT:Modification d'une activité de tâche\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Désactivation d'une activité de tâche")
public class SimpleActivityResource extends AbstractHd3dResource<ILSimpleActivity, ISimpleActivity>
{
    public SimpleActivityResource() throws Hd3dException
    {
        super(Persistors.simpleActivity, Translators.simpleActivity);
    }

    @Override
    protected ISimpleActivity initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(SIMPLE_ACTIVITY_ID_ATTRIBUTE, lockMode);
    }
}
