package fr.hd3d.services.resources;

import org.apache.commons.lang.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.services.resources.init.InitComputersInventory;
import fr.hd3d.utils.Bench;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


/**
 * Initializes all data needed for Computer Inventory demonstration.
 * 
 * @author HD3D
 */
@Hd3dComment("Initialise les données de démonstration du gestionnaire de parcs machines.<br />"
        + "Ceci inclut les feuilles de base et des données factices.")
public class InitializeInventoryResource<L, P> extends AbstractHd3dBaseResource
{
    /**
     * Default Constructor
     * 
     * @param context
     *            Context parameters
     * @param request
     *            Request received
     * @param response
     *            Response to send
     * @throws Hd3dException
     */
    public InitializeInventoryResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    public Representation get(Variant arg0)
    {
        final String cmdToRun = getQuery().getValues("cmd");

        try
        {
            init(cmdToRun);
        }
        catch (Hd3dTranslationException e)
        {
            e.printStackTrace();
            return new StringRepresentation(e.getMessage());
        }
        return new StringRepresentation("Computer inventory database initialized...");
    }

    public static void init(String cmd) throws Hd3dPersistenceException
    {
        if (StringUtils.isBlank(cmd) || "Computers".equalsIgnoreCase(cmd))
        {
            populateComputers();
        }
    }

    /**
     * Insert in database the computers test data.
     * 
     * @throws Hd3dPersistenceException
     */
    public static void populateComputers() throws Hd3dPersistenceException
    {
        run(new InitComputersInventory());
    }

    private static void run(IHibernateTransaction tx) throws Hd3dPersistenceException
    {
        Bench b = new Bench();
        b.b();
        Session session = HibernateUtil.currentSession();
        Transaction trx = session.beginTransaction();
        HibernateUtil.safeTransaction(tx);
        if (session.getFlushMode() == FlushMode.MANUAL)
        {
            session.flush();
        }
        trx.commit();
        trx = null;
        HibernateUtil.closeSession();

        Log.LOGGER.debug("{} took: {} ms", tx.getClass().getName(), b.e());
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
