package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILClassDynMetaDataType;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ClassDynMetaDataTypesResource extends
        AbstractHd3dCollection<ILClassDynMetaDataType, IClassDynMetaDataType>
{

    public ClassDynMetaDataTypesResource() throws Hd3dException
    {
        super(Persistors.classdynmetadatatype, Translators.classdynmetadata);
    }
}
