package fr.hd3d.services.resources;

import java.util.Map;

import fr.hd3d.common.client.Const;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTaskChanges;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetCustomPlanningTaskChangesCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPlanningTaskChangesCmd;
import fr.hd3d.model.translator.Translators;


public class PlanningTaskChangesResource extends AbstractHd3dCollection<ILTaskChanges, ITaskChanges>
{
    public PlanningTaskChangesResource() throws Hd3dException
    {
        super(Persistors.taskChanges, Translators.taskChanges);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        Map<String, String> map = getQuery().getValuesMap();
        String customValues = map.get(Const.CUSTOM);
        if (customValues != null)
        {
            return doGetCollection(new GetCustomPlanningTaskChangesCmd(getResourceContext()).wantTotalSize());
        }
        else
        {
            return doGetCollection(new GetPlanningTaskChangesCmd(getResourceContext()).wantTotalSize());
        }
    }
}
