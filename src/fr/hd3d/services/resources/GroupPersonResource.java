package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetGroupPersonPersonCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.UniqueResultQuery;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération des informations d'un utilisateur\r\n" + "\r\nPUT:Mise à jour d'une personne\r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:N/A")
public class GroupPersonResource extends AbstractHd3dResource<ILPerson, IPerson>
{
    public GroupPersonResource() throws Hd3dException
    {
        super(Persistors.person, Translators.person);
    }

    @Override
    protected IPerson initializeObject(LockMode lockMode) throws Hd3dException
    {
        final UniqueResultQuery<ILPerson, IPerson> cmd = new GetGroupPersonPersonCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        IPerson obj = (IPerson) cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }
}
