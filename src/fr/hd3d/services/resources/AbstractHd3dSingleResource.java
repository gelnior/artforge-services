package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;
import org.hibernate.Transaction;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.impl.ILPersistent;
import fr.hd3d.model.persistence.Copy;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.services.translator.json.JSonFormSerializer;
import fr.hd3d.utils.Bench;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.ServerResourceUtils;


public abstract class AbstractHd3dSingleResource<L extends ILPersistent, P extends Persistent> extends
        AbstractHd3dBaseResource
{
    protected P object = null;

    public AbstractHd3dSingleResource(final IPersist<? extends P> persistor, final IBaseTranslator<L, P> translator)
            throws Hd3dException
    {
        super();
        ResourceContext<L, P> context = new ResourceContext<L, P>(this);
        context.setPersistor(persistor);
        context.setTranslator(translator);
        context.complete();
        setResourceContext(context);
    }

    public AbstractHd3dSingleResource(final Class<?> persistedClass, final IBaseTranslator<L, P> translator)
            throws Hd3dException
    {
        super();
        ResourceContext<L, P> context = new ResourceContext<L, P>(this);
        context.setPersistedClass(persistedClass);
        context.setTranslator(translator);
        context.complete();
        setResourceContext(context);
    }

    public AbstractHd3dSingleResource() throws Hd3dException
    {
        super();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResourceContext<L, P> getResourceContext()
    {
        return (ResourceContext<L, P>) super.getResourceContext();
    }

    /**
     * used by Hd3dResource to initialize the object and provide default behaviors
     * 
     * @return
     * @throws Hd3dPersistenceException
     */
    protected abstract P initializeObject(LockMode lockMode) throws Hd3dException;

    protected P initializeObject() throws Hd3dException
    {
        return initializeObject(LockMode.NONE);
    }

    protected boolean initObject(LockMode lockMode)
    {
        boolean ret = false;

        try
        {
            object = initializeObject(lockMode);
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error("Unable to create object previous state as a lightweight object.");
        }
        ret = true;

        return ret;
    }

    protected boolean initObject()
    {
        return initObject(LockMode.NONE);
    }

    // ============================================================
    //
    // READ
    //
    // ============================================================
    @Override
    public Representation get(Variant variant)
    {
        Bench b = new Bench();

        if (initObject())
        {
            if (object != null)
            {
                /*
                 * NOTE: do not set Status to Status.SUCCESS_NO_CONTENT If so, there will be no empty list returned in
                 * the HTML screen, which may be confusing
                 */

                Session session = HibernateUtil.currentSession();
                Transaction tx = session.beginTransaction();

                L lObject = null;
                try
                {
                    lObject = getLightweight(object);
                    Representation r = getLightWeightRepresentation(variant, lObject);

                    if (session.getFlushMode() == FlushMode.MANUAL)
                    {
                        session.flush();
                    }
                    tx.commit();

                    r.setCharacterSet(CharacterSet.UTF_8);
                    getResponse().setStatus(Status.SUCCESS_OK);

                    doPostRead(lObject);

                    Log.LOGGER.info(responseInfo(b));

                    getResponse().setEntity(ServerResourceUtils.encodeRepresentation(r));
                }
                catch (HibernateException e)
                {
                    doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                }
                catch (Hd3dTranslationException e)
                {
                    doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                }
                catch (Hd3dException e)
                {
                    doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                }
                finally
                {
                    object = null;
                    lObject = null;
                    if (tx != null)
                    {
                        tx = null;
                    }
                    cleanup();
                }
            }
            else
            {
                getResponse().setStatus(Status.SUCCESS_OK);
                getResponse().setEntity(null);
            }
        }
        else
        {
            getResponse().setStatus(Status.SUCCESS_OK);
            getResponse().setEntity(null);
        }

        return getResponseEntity();
    }

    protected P copy(Representation entity)
    {
        P copy = null;
        try
        {
            copy = (P) BeanUtils.cloneBean(object);
        }
        catch (IllegalAccessException e)
        {
            Log.LOGGER.error("Error during object copy:" + e);
            throw new HibernateException("Error during object copy");
        }
        catch (InstantiationException e)
        {
            Log.LOGGER.error("Error during object copy:" + e);
            throw new HibernateException("Error during object copy");
        }
        catch (InvocationTargetException e)
        {
            Log.LOGGER.error("Error during object copy:" + e);
            throw new HibernateException("Error during object copy");
        }
        catch (NoSuchMethodException e)
        {
            Log.LOGGER.error("Error during object copy:" + e);
            throw new HibernateException("Error during object copy");
        }

        return copy;
    }

    protected void cleanup()
    {
        super.cleanup();
    }

    protected abstract void doPostRead(L lObject);

    protected abstract L getLightweight(P object) throws Hd3dException;

    protected abstract Representation getLightWeightRepresentation(Variant variant, L lightweight) throws Hd3dException;

    // ============================================================
    //
    // UPDATE
    //
    // ============================================================
    /**
     * Handle PUT requests. Update
     */
    @Override
    public Representation put(Representation entity, Variant variant)
    {
        Bench b = new Bench();

        if (entity == null)
        {
            error(Status.SERVER_ERROR_INTERNAL, new Hd3dTranslationException("EMPTY ENTITY"), ERROR_OCCURED);
        }
        else
        {
            /*
             * if deserialized light object lObject is used several times, it must be deserialized ONCE because
             * entity.getText() can only be called ONCE
             */
            L lObject = null;
            int count = 3;
            boolean stop = false;

            while (!stop && count > 0)
            {
                count--;
                if (initObject(LockMode.UPGRADE_NOWAIT) && preCheck(object))
                {
                    Session session = HibernateUtil.currentSession();
                    Transaction tx = session.beginTransaction();

                    try
                    {
                        if (lObject == null)
                        {
                            lObject = JSonFormSerializer.<L> deserialize(entity);
                        }

                        doPreUpdate(entity, lObject);

                        doUpdate(lObject);

                        if (session.getFlushMode() == FlushMode.MANUAL)
                        {
                            session.flush();
                        }
                        tx.commit();

                        tx = session.beginTransaction();

                        doPostUpdate(entity);

                        if (session.getFlushMode() == FlushMode.MANUAL)
                        {
                            session.flush();
                        }
                        tx.commit();

                        Log.LOGGER.info(responseInfo(b));

                        stop = true;
                    }
                    catch (StaleObjectStateException e)
                    {
                        doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                        sleep();
                    }
                    catch (Hd3dPersistenceException e)
                    {
                        doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                        if (e.isRetry())
                        {
                            sleep();
                        }
                        else
                        {
                            stop = false;
                        }
                    }
                    catch (Hd3dTranslationException e)
                    {
                        doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                        stop = true;// may be a validation exception, so no point to continue
                    }
                    catch (Hd3dException e)
                    {
                        doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                        stop = true;
                    }
                    catch (HibernateException e)
                    {
                        doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                        sleep();
                    }
                    catch (Exception e)
                    {
                        doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                        stop = true;// no point to continue
                    }
                    finally
                    {
                        if (tx != null)
                        {
                            tx = null;
                        }
                        cleanup();
                    }
                }
            }

            object = null;
            lObject = null;
            cleanup();
        }
        return getResponseEntity();
    }

    protected abstract void doPreUpdate(Representation entity, L lObject) throws Hd3dException;

    protected abstract void doUpdate(L object) throws Hd3dException;

    protected abstract void doPostUpdate(Representation entity) throws Hd3dException;

    // ============================================================
    //
    // DELETE
    //
    // ============================================================
    /**
     * Handle DELETE requests.
     */
    @Override
    public Representation delete(Variant variant)
    {
        Bench b = new Bench();

        int count = 3;
        boolean stop = false;
        while (!stop && count > 0)
        {
            count--;

            if (initObject(LockMode.UPGRADE_NOWAIT))
            {
                if (object == null)
                {
                    /* request successful because the data does not exist */
                    getResponse().setStatus(Status.SUCCESS_NO_CONTENT);
                }
                else
                {
                    if (preCheck(object))
                    {
                        Session session = HibernateUtil.currentSession();
                        Transaction tx = session.beginTransaction();

                        /* Remove the item from the list. */
                        try
                        {
                            doPreDelete();

                            doDelete();

                            if (session.getFlushMode() == FlushMode.MANUAL)
                            {
                                session.flush();
                            }
                            tx.commit();

                            tx = session.beginTransaction();

                            doPostDelete();

                            if (session.getFlushMode() == FlushMode.MANUAL)
                            {
                                session.flush();
                            }
                            tx.commit();

                            Log.LOGGER.info(responseInfo(b));

                            stop = true;
                        }
                        catch (StaleObjectStateException e)
                        {
                            doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                            sleep();
                        }
                        catch (Hd3dPersistenceException e)
                        {
                            doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                            if (e.isRetry())
                            {
                                sleep();
                            }
                            else
                            {
                                stop = true;
                            }
                        }
                        catch (Hd3dException e)
                        {
                            doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                            if (e.isRetry())
                            {
                                sleep();
                            }
                            else
                            {
                                stop = true;
                            }
                        }
                        catch (HibernateException e)
                        {
                            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                            sleep();
                        }
                        catch (Exception e)
                        {
                            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                            stop = true;// no point to continue
                        }
                        finally
                        {
                            if (tx != null)
                            {
                                tx = null;
                            }
                            cleanup();
                        }
                    }
                }
            }
        }

        object = null;
        cleanup();

        return getResponseEntity();
    }

    protected abstract boolean preCheck(P object);

    protected abstract void doPreDelete() throws Hd3dException;

    protected abstract void doDelete() throws Hd3dException;

    protected abstract void doPostDelete() throws Hd3dException;

    // ============================================================
    //
    // CREATION
    //
    // ============================================================
    /**
     * handle object CREATION (single and multiple)
     */
    @Override
    public Representation post(Representation entity, Variant variant)
    {
        Bench b = new Bench();
        /*
         * if deserialized light object lObject is used several times, it must be deserialized ONCE because
         * entity.getText() can only be called ONCE
         */
        P newObject = null;
        List<P> newObjects = null;
        int count = 3;
        boolean stop = false;
        while (!stop && count > 0)
        {
            count--;

            Session session = HibernateUtil.currentSession();
            Transaction tx = session.beginTransaction();

            try
            {
                if (getResourceContext().isBulk())
                {/* MULTIPLE objects creation */

                    if (CollectUtils.isEmpty(newObjects))
                    {
                        newObjects = newCollection(entity);
                    }

                    doPrePersistCollection(entity, newObjects);

                    doPersist(newObjects);

                    if (session.getFlushMode() == FlushMode.MANUAL)
                    {
                        session.flush();
                    }
                    tx.commit();

                    tx = session.beginTransaction();

                    doPostPersist(newObjects);

                    if (session.getFlushMode() == FlushMode.MANUAL)
                    {
                        session.flush();
                    }
                    tx.commit();

                    stop = true;
                }
                else
                {/* SINGLE object creation */
                    String lastSegment = getRequest().getOriginalRef().getLastSegment();

                    if (fr.hd3d.utils.Const.COPY.equals(lastSegment))
                    {
                        if (initObject(LockMode.OPTIMISTIC) && preCheck(object))
                        {
                            if (!(object instanceof Copy))
                            {
                                throw new Hd3dException("This entity does not allow copy");
                            }
                            newObject = copy(entity);
                        }
                    }
                    else
                    {
                        if (newObject == null)
                        {
                            newObject = newObject(entity);
                        }
                    }
                    doPrePersist(entity, newObject);

                    doPersist(newObject);

                    if (session.getFlushMode() == FlushMode.MANUAL)
                    {
                        session.flush();
                    }
                    tx.commit();

                    tx = session.beginTransaction();

                    doPostPersist(newObject);

                    if (session.getFlushMode() == FlushMode.MANUAL)
                    {
                        session.flush();
                    }
                    tx.commit();

                    stop = true;
                }

                Log.LOGGER.info(responseInfo(b));
            }
            catch (StaleObjectStateException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                sleep();
            }
            catch (Hd3dPersistenceException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                stop = true;// no point to continue
            }
            catch (Hd3dTranslationException e)
            {
                doOnError(e, session, tx, Status.CLIENT_ERROR_BAD_REQUEST, ERROR_OCCURED);
                stop = true;// may be a validation exception, so no point to continue
            }
            catch (HibernateException e)
            {
                doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                stop = true;// no point to continue
            }
            catch (Exception e)
            {
                doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
                stop = true;// no point to continue
            }
            finally
            {
                /* clear */
                if (tx != null)
                {
                    tx = null;
                }
                cleanup();
            }
        }

        /* clear */
        object = null;
        if (newObjects != null)
        {
            newObjects.clear();
            newObjects = null;
        }
        newObject = null;
        cleanup();

        return getResponseEntity();
    }

    protected abstract P newObject(Representation entity) throws Hd3dException;

    protected abstract List<P> newCollection(Representation entity) throws Hd3dException;

    protected abstract void doPrePersistCollection(Representation entity, List<P> newObjects) throws Hd3dException;

    protected abstract void doPersist(List<P> objects) throws Hd3dException;

    protected abstract void doPostPersist(List<P> objects) throws Hd3dException;

    protected abstract void doPrePersist(Representation entity, P object) throws Hd3dException;

    protected abstract void doPersist(P newObject) throws Hd3dException;

    protected abstract void doPostPersist(P newObject) throws Hd3dException;
}
