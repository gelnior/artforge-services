package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetAssetRevisionGroupsByProject;
import fr.hd3d.model.translator.Translators;


public class ProjectAssetRevisionGroupsResource extends
        AbstractHd3dCollection<ILAssetRevisionGroup, IAssetRevisionGroup>
{
    public ProjectAssetRevisionGroupsResource() throws Hd3dException
    {
        super(Persistors.assetrevisiongroup, Translators.assetrevisiongroup);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetAssetRevisionGroupsByProject(getResourceContext()).wantTotalSize());
    }

    @Override
    protected IAssetRevisionGroup newObject(Representation entity) throws Hd3dException
    {
        IAssetRevisionGroup group = super.newObject(entity);

        long projectId = getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE);
        IProject project = Persistors.project.getById(projectId);
        group.setProject(project);
        return group;
    }
}
