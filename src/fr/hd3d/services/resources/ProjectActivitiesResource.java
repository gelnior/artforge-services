package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectActivitiesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Jerome GONNON
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des activités du projet\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'une activité\r\n" + "\r\nDELETE:N/A")
public class ProjectActivitiesResource extends AbstractHd3dCollection<ILActivity, IActivity>
{
    public ProjectActivitiesResource() throws Hd3dException
    {
        super(Persistors.activity, Translators.activity);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectActivitiesCmd(getResourceContext()).wantTotalSize());
    }
}
