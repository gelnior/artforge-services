package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;
import static fr.hd3d.common.client.ServicesURI.SHEET_ID_ATTRIBUTE;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.sojo.interchange.json.JsonParserException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.comparators.ComparatorChain;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.FlushMode;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.data.CharacterSet;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;
import org.restlet.util.Triple;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSheet;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteH;
import fr.hd3d.model.persistence.impl.hibernate.ComplexTypeProvider;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.ItemTypeHandler;
import fr.hd3d.model.persistence.impl.hibernate.StepH;
import fr.hd3d.model.persistence.impl.hibernate.criteria.Hd3dQueryFilterException;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory;
import fr.hd3d.model.persistence.impl.hibernate.criteria.QueryFilterFactory.CompositeQueryModifier;
import fr.hd3d.model.persistence.impl.hibernate.serialization.SheetExternalSort;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.services.resources.sheet.IItemSorter;
import fr.hd3d.services.resources.sheet.ItemFilterFactory;
import fr.hd3d.services.resources.sheet.ItemSorterFactory;
import fr.hd3d.services.resources.sheet.ItemSorterFactory.ItemSorters;
import fr.hd3d.services.resources.transformer.ItemTransformer;
import fr.hd3d.services.resources.transformer.TransformersMap;
import fr.hd3d.utils.Bench;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.MapUtils;
import fr.hd3d.utils.Pair;
import fr.hd3d.utils.ServerResourceUtils;
import fr.hd3d.utils.SheetUtils;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'une fiche (et du 'portal' associé). \r\n"
        + "\r\nPUT:Mise à jour d'une fiche (et du 'portal' associé)\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Suppression d'une fiche (et du portal associé)")
public class SheetResource extends AbstractHd3dResource<ILSheet, ISheet>
{
    /* TODO: ugly to store this here */
    ComparatorChain<Object[][]> comparators = null;

    public SheetResource() throws Hd3dException
    {
        super(Persistors.sheet, Translators.sheet);
    }

    @Override
    protected ISheet initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(SHEET_ID_ATTRIBUTE, lockMode);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Representation get(Variant variant)
    {
        Bench b = new Bench();

        /*
         * if ...sheets/{id}/data => return the list of values, else super.getRepresentation. NOTE: not very clean to
         * return a collection here since we inherit from AbstractHd3dResource (single element returned)
         */
        String lastSegment = getRequest().getOriginalRef().getLastSegment();

        if (!Const.DATA.equals(lastSegment))
            return super.get(variant);

        final ResourceContext context = getResourceContext();
        if (!initObject())
        {
            return getResponseEntity();
        }
        if (object == null)
        {
            /*
             * NOTE: do not set Status to Status.SUCCESS_NO_CONTENT If so, there will be no empty list returned in the
             * HTML screen, which may be confusing
             */
            return getResponseEntity();
        }

        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();

        try
        {
            final int nbColumns = SheetUtils.nbColumns(object);

            Triple<Object[][][], Integer, Long[]> triple = process(session, tx, context, nbColumns);

            /* TODO: this is to be generalized */
            // Long[] sum = sum(result, nbColumns);
            // Long[] sum = sum(pair.getField_1(), nbColumns);
            // Long[] sum = null;
            /* build response */

            Collection response = buildResponse(safeConvert(triple.getFirst(), nbColumns, getResourceContext()),
                    nbColumns, triple.getThird());

            /* serialize response */
            Representation r = RessourceSerializer.getRepresentation(variant, response, triple.getSecond());
            r.setCharacterSet(CharacterSet.UTF_8);

            CollectUtils.cleanUpArray(triple.getFirst());
            response.clear();

            Log.LOGGER.info(responseInfo(b));

            return r;
        }
        catch (IOException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
        }
        catch (Hd3dPersistenceException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
        }
        catch (Hd3dException e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
        }
        catch (Exception e)
        {
            doOnError(e, session, tx, Status.SERVER_ERROR_INTERNAL, ERROR_OCCURED);
        }
        finally
        {
            if (tx != null)
            {
                tx = null;
            }
            cleanup();
        }

        return getResponseEntity();
    }

    @SuppressWarnings("unchecked")
    public Triple<Object[][][], Integer, Long[]> process(Session session, Transaction tx, ResourceContext context,
            final int nbColumns) throws Exception
    {

        Object[][][] result = null;
        Long[] sum = new Long[nbColumns];
        Arrays.fill(sum, 0L);
        int nbRecords;

        final Map<String, String> map = getResourceContext().getQueryMap();

        /* apply the itemGroup's items query to the class bound to the sheet */
        final String boundClassName = object.getBoundClassName();

        final IPersist boundObjectPersistor = Persistors.getPersistor(boundClassName);

        /* get the instances of the bound class (filtered by constraint) */
        final Class boundClass = boundObjectPersistor.getPersistedClass();
        final CompositeQueryModifier filter = QueryFilterFactory.getFilter(boundClass, map,
                ComplexTypeProvider.INSTANCE.getProvider(boundClass));

        context.setPersistor(boundObjectPersistor);
        context.setFilter(filter);
        context.complete();
        setResourceContext(context);

        /* filter and sort are delegated to hibernate */
        if (QueryFilterFactory.hibernatePagination(map))
        {
            ResultCollection persistedObjects = loadCollection();

            ServerResourceUtils.createDynMetaDataValuesFor(persistedObjects.getResult(), boundClassName);

            result = collectValues(persistedObjects.getResult(), object, map, nbColumns);

            nbRecords = persistedObjects.getCount();

            sum = sum(result, nbColumns);
        }
        else
        /* EXTERNAL/MEMORY filter and sort depending on result size */
        {
            // Bench b = new Bench();
            // b.b();

            ResultCollection persistedObjectsIds = loadCollectionIds();

            // System.out.println("after loadCollectionIds=" + b.e());
            // b.b();

            /* filter by lucene constraint */
            ResultCollection luceneFiltered = context.getLuceneFilters().filter(boundClass, persistedObjectsIds, true);

            nbRecords = luceneFiltered.getCount();

            // System.out.println("after luceneFiltered=" + b.e());
            // b.b();

            List<List<Long>> idPartitions = CollectUtils.partition(luceneFiltered.getAllIds(), Conf.getMaxRecords());
            // System.out.println("after idPartitions=" + b.e());

            if (CollectUtils.isNotEmpty(idPartitions))
            {
                List objects = null;

                /* short result collection: MEMORY sort */
                if (idPartitions.size() < 2)
                {
                    /* need to load objects based on their ids */
                    objects = getResourceContext().getPersistor().getByIds(getResourceContext(), idPartitions.get(0));

                    /* create DynMetaDataValues first */
                    ServerResourceUtils.createDynMetaDataValuesFor(objects, boundClassName);

                    result = collectValues(objects, object, map, nbColumns);

                    sum = sum(result, nbColumns);

                    result = filter(result, map);

                    /*
                     * If the query is paginated return result and number of all records without pagination. If the
                     * result has been filtered ( size after filtering is lesser than before filtering), return number
                     * of filtered list.
                     */
                    nbRecords = result.length;

                    result = sort(result, map);

                    result = paginate(result, map);
                }
                /* big result collection: EXTERNAL sort */
                else
                {
                    /*
                     * Multi thread: CAUSES
                     * "CollectionQuery returns a collection with different size(size=...) than entities(size=...)!"
                     */
                    // ExecutorService threadPool = Executors.newFixedThreadPool(4);
                    // CompletionService<Boolean> pool = new ExecutorCompletionService<Boolean>(threadPool);
                    //
                    // CopyOnWriteArrayList<File> createdFiles = new CopyOnWriteArrayList<File>();
                    // final boolean sort = !ItemSorterFactory.getSorters(map).isEmpty();
                    //
                    // List<List<List<Long>>> partitionsOfIdPartitions = CollectUtils.splitIntoXparts(idPartitions,
                    // 4);
                    //
                    // for (List<List<Long>> partitionOfIds : partitionsOfIdPartitions)
                    // {
                    // pool.submit(new OneSegmentTask(createdFiles, partitionOfIds, boundClassName, sort, map,
                    // nbColumns));
                    // }
                    //
                    // Boolean executionResult = Boolean.FALSE;
                    // for (int i = 0; i < 10; i++)
                    // {
                    // executionResult &= pool.take().get();
                    // }
                    //
                    // threadPool.shutdown();
                    // ///////////////////
                    // b.b();
                    //
                    SheetExternalSort externalSorter = new SheetExternalSort();

                    final boolean sort = !ItemSorterFactory.getSorters(map).isEmpty();
                    List<File> createdFiles = new ArrayList<File>();
                    File file = null;
                    int count = 0;
                    for (int i = 0; i < idPartitions.size(); i++)
                    {
                        /* need to load objects based on their ids */
                        objects = getResourceContext().getPersistor().getByIds(getResourceContext(),
                                idPartitions.get(i));

                        /* create DynMetaDataValues first */
                        ServerResourceUtils.createDynMetaDataValuesFor(objects, boundClassName);

                        result = collectValues(objects, object, map, nbColumns);

                        /* filter collected values */
                        result = filter(result, map);

                        addSum(sum, sum(result, nbColumns));
                        // sum = sum(result, nbColumns);

                        count += result.length;

                        result = sort ? sort(result, map) : result;

                        try
                        {
                            file = externalSorter.serialize(result, nbColumns);
                        }
                        catch (IOException e)
                        {
                            FileUtils.deleteQuietly(file);
                        }

                        CollectUtils.addIfNotNull(createdFiles, file);
                        objects.clear();
                    }

                    nbRecords = count;

                    // System.out.println("after file written=" + b.e());
                    // b.b();

                    /* merge sort the temporary small sorted files */
                    try
                    {
                        if (sort)
                        {
                            result = externalSorter.mergeSort(createdFiles, map, luceneFiltered.getAllIds().size(),
                                    comparators);
                        }
                        else
                        {
                            result = externalSorter.deserializeAndPaginate(createdFiles, map, luceneFiltered
                                    .getAllIds().size());
                        }
                        /*
                         * If the query is paginated return result and number of all records without pagination. If the
                         * result has been filtered ( size after filtering is lesser than before filtering), return
                         * number of filtered list.
                         */
                        // nbRecords = luceneFiltered.getCount();
                    }
                    finally
                    {
                        for (File f : createdFiles)
                        {
                            FileUtils.deleteQuietly(f);
                        }
                    }
                    // System.out.println("after files merge=" + b.e());
                }

                if (objects != null)
                {
                    objects.clear();
                }
            }

            /* DO NOT REMOVE: to commit created metadatavalues */
            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            tx.commit();

            persistedObjectsIds.clear();
            luceneFiltered.clear();
            idPartitions.clear();
        }

        return new Triple<Object[][][], Integer, Long[]>(result, new Integer(nbRecords), sum);
    }

    private void addSum(Long[] toUpdate, Long[] toAdd)
    {
        for (int i = 0; i < toUpdate.length; i++)
        {
            if (toAdd[i] != null)
                toUpdate[i] += toAdd[i];
        }
    }

    @SuppressWarnings("unchecked")
    private ResultCollection loadCollection() throws Hd3dException
    {
        return getResourceContext().getPersistor().getCollectionWithTotal(getResourceContext());
    }

    @SuppressWarnings("unchecked")
    private ResultCollection loadCollectionIds() throws Hd3dException
    {
        return getResourceContext().getPersistor().getCollection(getResourceContext(), true);
    }

    private static Long[] sum(Object[][][] sheetValues, final int nbColumns)
    {
        Long[] ret = new Long[nbColumns];

        boolean[] isNumeric = new boolean[nbColumns];
        /* determine whether each column is numeric */
        for (int j = 0; j < nbColumns; j++)
        {
            for (int i = 0; i < sheetValues.length; i++)
            {
                if (sheetValues[i] != null && sheetValues[i][j] != null)
                {
                    Object itemValue = sheetValues[i][j][0];
                    if (itemValue != null)
                    {
                        if (itemValue instanceof StepH)
                        {
                            isNumeric[j] = true;
                        }
                        else
                        {
                            isNumeric[j] = NumberUtils.isNumber(itemValue.toString());
                        }
                        break;
                    }
                }
            }
        }

        /* compute sum */
        for (int j = 0; j < nbColumns; j++)
        {
            if (isNumeric[j])
            {
                Long sum = 0L;
                for (int i = 0; i < sheetValues.length; i++)
                {
                    Object itemValue = sheetValues[i][j][0];
                    if (itemValue instanceof Integer)
                    {
                        sum += (Integer) itemValue;
                    }
                    else if (itemValue instanceof Long)
                    {
                        sum += (Long) itemValue;
                    }
                    else if (itemValue instanceof StepH)
                    {
                        sum += ((StepH) itemValue).getEstimatedDuration();
                    }

                }
                ret[j] = sum;
            }
            else
            {
                ret[j] = null;
            }
        }

        return ret;
    }

    @SuppressWarnings({ "unchecked" })
    private static Object[][][] collectValues(final List result, final ISheet sheet, final Map<String, String> map,
            final int nbColumns) throws Hd3dException
    {
        if (CollectionUtils.isEmpty(result))
            return new Object[0][0][0];

        /*
         * Collect Values. Array containing: dim 0 = lines of result; dim 1 = columns (items); dim 2 = [0] item
         * value,[1] item group, [2] item, [3] entity id
         */
        final int nbLines = result.size();

        Object[][][] res = new Object[nbLines][nbColumns][4];

        for (int colIndex = 0; colIndex < nbColumns; colIndex++)
        {
            for (final IItemGroup itemGroup : sheet.getItemGroups())
            {
                if (itemGroup == null || CollectUtils.isEmpty(itemGroup.getItems()))
                    continue;

                /* item values for all the results for 1 itemGroup */
                for (final IItem item : itemGroup.getItems())
                {
                    if (item == null)
                        continue;

                    List<Object> itemValues = getValues(result, item, map, res);
                    for (int i = 0; i < nbLines; i++)
                    {
                        res[i][colIndex][0] = i < itemValues.size() ? itemValues.get(i) : null;// value
                        res[i][colIndex][1] = itemGroup;// itemGroup
                        res[i][colIndex][2] = item;// item
                        res[i][colIndex][3] = ((IBase) result.get(i)).getId();
                    }
                    colIndex++;
                }
            }
        }

        return res;
    }

    private static Object[][][] filter(Object[][][] values, final Map<String, String> map) throws Hd3dException
    {
        if (values == null || values.length == 0)
            return values;

        return ItemFilterFactory.getFilter(map).filter(values);
    }

    private Object[][][] sort(Object[][][] values, final Map<String, String> map) throws Hd3dException
    {
        if (values == null || values.length == 0)
            return values;

        ItemSorters sorters = ItemSorterFactory.getSorters(map);
        Object[][][] sorted = sorters.sort(values);

        /* comparators are determined after the first sort */
        IItemSorter itemSorter = sorters.getSorter(0);
        if (itemSorter != null)
        {
            comparators = sorters.getSorter(0).getComparators();
        }

        return sorted;
    }

    private static Object[][][] paginate(final Object[][][] values, final Map<String, String> map)
            throws JsonParserException, Hd3dQueryFilterException
    {
        /*
         * if already paginated by hibernate do not paginate here.
         */
        if (QueryFilterFactory.hibernatePagination(map))
        {
            return values;
        }

        if (ArrayUtils.isNotEmpty(values))
        {
            Pair<Integer, Integer> indexes = ServerResourceUtils.getStartAndEndIndex(map, values.length);
            return Arrays.copyOfRange(values, indexes.getField_1(), indexes.getField_2());
        }

        return values;
    }

    // private final class OneSegmentTask implements Callable<Boolean>
    // {
    // CopyOnWriteArrayList<File> createdFiles;
    // List<List<Long>> idPartitions;
    // String boundClassName;
    // boolean sort;
    // Map<String, String> map;
    // int nbColumns;
    //
    // public OneSegmentTask(CopyOnWriteArrayList<File> createdFiles, List<List<Long>> idPartitions,
    // String boundClassName, boolean sort, Map<String, String> map, int nbColumns)
    // {
    // this.createdFiles = createdFiles;
    // this.idPartitions = idPartitions;
    // this.boundClassName = boundClassName;
    // this.sort = sort;
    // this.map = map;
    // this.nbColumns = nbColumns;
    // }
    //
    // public Boolean call() throws Exception
    // {
    // if (CollectUtils.isEmpty(idPartitions))
    // return Boolean.TRUE;
    //
    // SheetExternalSort externalSorter = new SheetExternalSort();
    //
    // File file = null;
    // List objects;
    // for (int i = 0; i < idPartitions.size(); i++)
    // {
    // /* need to load objects based on their ids */
    // objects = getResourceContext().getPersistor().getByIds(getResourceContext(), idPartitions.get(i));
    //
    // /* create DynMetaDataValues first */
    // createDynMetaDataValues(objects, boundClassName);
    //
    // Object[][][] sheetValues = collectValues(objects, object, map, nbColumns);
    //
    // /* filter collected values */
    // Object[][][] filteredRes = filter(sheetValues, map);
    //
    // Object[][][] toSerialize;
    // /* EXTERNAL merge sort */
    // if (sort)
    // {
    // toSerialize = sort(filteredRes, map);
    // }
    // else
    // {
    // toSerialize = filteredRes;
    // }
    //
    // try
    // {
    // file = externalSorter.serialize(toSerialize, nbColumns);
    // }
    // catch (IOException e)
    // {
    // if (file != null)
    // {
    // file.delete();
    // }
    // }
    // CollectUtils.addIfNotNull(createdFiles, file);
    // objects.clear();
    // }
    //
    // return Boolean.TRUE;
    // }
    // }

    protected static List<Map<String, Object>> buildResponse(Object[][][] values, final int nbColums, Long[] sum)
    {
        List<Map<String, Object>> ret = new ArrayList<Map<String, Object>>();

        for (int i = 0; i < values.length; i++)
        {
            /* build Item HashMaps */
            MultiHashMap<Long, Map> sameItemGroupItemsMap = new MultiHashMap<Long, Map>();
            for (int j = 0; j < nbColums; j++)
            {
                Map<String, Object> itemMap = new HashMap<String, Object>();
                final IItem item = (IItem) values[i][j][2];
                itemMap.put(ItemGroupResource.ID, item.getId());
                itemMap.put(ItemGroupResource.NAME, item.getName());
                itemMap.put(ItemGroupResource.VALUE, values[i][j][0]);

                sameItemGroupItemsMap.put(item.getItemGroup().getId(), itemMap);
            }

            /* build ItemGroup HashMaps */
            List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();
            for (Map.Entry<Long, Collection<Map>> entry : sameItemGroupItemsMap.entrySet())
            {
                Map<String, Object> itemGroupMap = new HashMap<String, Object>();
                itemGroupMap.put(Const.ID, entry.getKey());
                itemGroupMap.put(Const.ITEMS, entry.getValue());
                itemList.add(itemGroupMap);
            }

            /* build Response lines */
            if (!ArrayUtils.isEmpty(values[i]))
            {
                Map<String, Object> line = new HashMap<String, Object>();
                line.put(Const.ID, (Long) values[i][0][3]);
                line.put(Const.GROUPS, itemList);

                ret.add(line);
            }
        }
        if (sum != null)
        {
            /* add the sum line */
            Map<String, Object> line = new HashMap<String, Object>();
            line.put("sum", sum);
            ret.add(line);
        }
        return ret;
    }

    // private List<Object> buildNewResponse(Object[][][] values, final List result, final int nbColums)
    // {
    // if (values.length < 1)
    // return new ArrayList(0);
    //
    // List<String> attributeNames = new ArrayList<String>();
    // List<Class> attributeTypes = new ArrayList<Class>();
    //
    // /* one result line -> one DynaBean */
    // for (int j = 0; j < nbColums; j++)
    // {
    // /* name */
    // final String attributeName = String.valueOf(((IItemGroup) values[0][j][1]).getId()) + '_'
    // + String.valueOf(((IItem) values[0][j][2]).getId());
    // attributeNames.add(attributeName);
    //
    // /* value */
    // final Object value = values[0][j][0];
    //
    // /* type */
    // attributeTypes.add(value == null ? Object.class : value.getClass());
    // }
    //
    // /* build DynaClass */
    // List<DynaProperty> props = new ArrayList<DynaProperty>();
    // for (int k = 0; k < attributeNames.size(); k++)
    // {
    // props.add(new DynaProperty(attributeNames.get(k), attributeTypes.get(k)));
    // }
    // DynaProperty[] propsArray = new DynaProperty[props.size()];
    // final BasicDynaClass dynaClass = new BasicDynaClass("TOTO", null, props.toArray(propsArray));
    //
    // /* instantiate the DynaBeans */
    // List<Object> ret = new ArrayList<Object>();
    //
    // for (int i = 0; i < values.length; i++)
    // {
    // /* one result line -> one DynaBean */
    // List<Object> attributeValues = new ArrayList<Object>();
    // for (int j = 0; j < nbColums; j++)
    // {
    // /* value */
    // final Object value = values[i][j][0];
    // attributeValues.add(value);
    // }
    // ret.add(BeanUtils.toBean(dynaClass, attributeNames.toArray(new String[attributeNames.size()]),
    // attributeValues.toArray()));
    // }
    //
    // return ret;
    // }

    private static List<Map> buildNewResponse(Object[][][] values, final List result, final int nbColums)
            throws Hd3dException
    {
        List<Map> ret = new ArrayList<Map>();

        List<String> attributeNames = new ArrayList<String>();
        List<Object> attributeValues = new ArrayList<Object>();

        for (int i = 0; i < values.length; i++)
        {
            attributeNames.add("id");
            attributeValues.add(((IBase) result.get(i)).getId());

            for (int j = 0; j < nbColums; j++)
            {
                /* name */
                final String attributeName = String.valueOf(((IItemGroup) values[0][j][1]).getId()) + '_'
                        + String.valueOf(((IItem) values[0][j][2]).getId());
                attributeNames.add(attributeName);

                /* value */
                attributeValues.add(values[0][j][0]);

                /* type */
                // attributeTypes.add(value == null ? Object.class : value.getClass());
            }
            ret.add(MapUtils.toMap(attributeNames, attributeValues, HashMap.class));
        }

        return ret;
    }

    private static List<Object> getValues(List<Object> entityInstances, IItem item, Map<String, String> map,
            Object[][][] itemResult) throws Hd3dException
    {
        List<Object> ret = new ArrayList<Object>();

        List<?> values = new ItemTypeHandler(entityInstances, item, itemResult).getValues();

        for (final Object value : values)
        {

            if (value instanceof List<?>)
            {
                List<ApprovalNoteH> val = (List<ApprovalNoteH>) value;
            }
            ret.add(applyTransformer(value, item, value));
        }
        return ret;
    }

    protected static Object[][][] safeConvert(final Object[][][] values, final int nbColumns,
            ResourceContext<?, ?> context) throws Hd3dException
    {
        /* must properly convert values otherwise json serialization fails because of hibernate byte enrichment */
        for (int i = 0; i < values.length; i++)
        {
            for (int colIndex = 0; colIndex < nbColumns; colIndex++)
                values[i][colIndex][0] = safeConvert(values[i][colIndex][0], context);
        }

        return values;
    }

    private static Object safeConvert(Object value, ResourceContext<?, ?> context) throws Hd3dException
    {
        Object val;
        try
        {
            val = ServerResourceUtils.dynaBeanPropSafeConvert(value, context);
        }
        catch (Hd3dTranslationException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            val = e;
        }
        return val;
    }

    private static Object applyTransformer(final Object entityInstance, final IItem item, final Object value)
    {
        Object ret = value;
        /* apply the bound transformer on the the value */
        final String transformerName = item.getTransformer();
        if (StringUtils.isNotBlank(transformerName))
        {
            /* get the transformer from the cache map, if not found try to instantiate it */
            ItemTransformer transformer = getTransformer(transformerName);
            if (transformer != null)
            {
                ret = transformer.transform(entityInstance, value, item.getTransformerParameters());
            }
        }
        return ret;
    }

    @SuppressWarnings("unchecked")
    private static ItemTransformer getTransformer(final String transformerName)
    {
        ItemTransformer trans = TransformersMap.getTransformerInstance(transformerName);
        if (trans == null)
        {
            Class<ItemTransformer> clazz;
            try
            {
                clazz = (Class<ItemTransformer>) Class.forName(transformerName);
                trans = clazz.newInstance();
            }
            catch (ClassNotFoundException e)
            {
                Log.LOGGER.error("unable to instanciate {} class", transformerName);
                trans = null;
            }
            catch (InstantiationException e)
            {
                Log.LOGGER.error("unable to instanciate {} class", transformerName);
                trans = null;
            }
            catch (IllegalAccessException e)
            {
                Log.LOGGER.error("unable to instanciate {} class", transformerName);
                trans = null;
            }
        }
        return trans;
    }

    // @SuppressWarnings("unchecked")
    // protected void createDynMetaDataValues(List<? extends IBase> list, final String entityName) throws Hd3dException
    // {
    // if (CollectUtils.isNotEmpty(list))
    // {
    // /* create DynMetaDataValues for retrieved instances */
    // if (!BaseH.DYNVALUES_TO_CREATE_SET.contains(entityName))
    // return;
    //
    // List<List<Long>> partitions = CollectUtils.partition(CollectUtils.getIds((List<IBase>) list), Conf
    // .getMaxRecords());
    // for (List<Long> part : partitions)
    // {
    // List<DynMetaDataValuesToCreate> toCreate = Queries.getDynMetaDataValuesToCreate(entityName, part);
    //
    // if (CollectUtils.isEmpty(toCreate))
    // continue;
    //
    // ServerResourceUtils.createDynMetaDataValues(toCreate, entityName);
    // }
    // }
    // }
}
