package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetGroupPersonsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste d'utilisateurs d'un groupe\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:(un post va créer un nouvel utilisateur mais il ne sera pas dans le group)\r\n" + "\r\nDELETE:N/A")
public class GroupPersonsResource extends AbstractHd3dCollection<ILPerson, IPerson>
{
    public GroupPersonsResource() throws Hd3dException
    {
        super(Persistors.person, Translators.person);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetGroupPersonsCmd(getResourceContext()).wantTotalSize());
    }
}
