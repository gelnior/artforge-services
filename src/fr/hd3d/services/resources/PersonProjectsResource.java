package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonProjectsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * A resource to access to specific person's projects.
 * 
 * @author David Gauthier
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des projets d'un utilisateur\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:N/A")
public class PersonProjectsResource extends AbstractHd3dCollection<ILProject, IProject>
{
    public PersonProjectsResource() throws Hd3dException
    {
        super(Persistors.project, Translators.project);
    }

    @Override
    protected ResultCollection<IProject> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetPersonProjectsCmd(getResourceContext()).wantTotalSize());
    }
}
