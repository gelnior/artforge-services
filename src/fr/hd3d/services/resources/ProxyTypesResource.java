package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILProxyType;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * HD3D
 */
@Hd3dComment("GET:Récupération de la liste des proxy types\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'un nouveau proxy type\r\n" + "\r\nDELETE:N/A")
public class ProxyTypesResource extends AbstractHd3dCollection<ILProxyType, IProxyType>
{
    public ProxyTypesResource() throws Hd3dException
    {
        super(Persistors.proxytype, Translators.proxytype);
    }
}
