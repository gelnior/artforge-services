package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IPersist;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetAllCategoriesCmd;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;


@Hd3dComment("GET:Récupération de toute les catégories d'un projet\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:N/A")
public class CategoryAllResource extends AbstractHd3dCollection<ILCategory, ICategory>
{
    public CategoryAllResource(IPersist<ICategory> persistor, IBaseTranslator<ILCategory, ICategory> translator)
            throws Hd3dException
    {
        super(persistor, translator);
    }

    public CategoryAllResource() throws Hd3dException
    {
        super(Persistors.category, Translators.category);
        allowMethods(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetAllCategoriesCmd(getResourceContext()).wantTotalSize());
    }
}
