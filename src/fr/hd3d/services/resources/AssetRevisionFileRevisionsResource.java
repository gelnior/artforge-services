// package fr.hd3d.services.resources;
//
// import fr.hd3d.exception.Hd3dException;
// import fr.hd3d.model.lightweight.ILAssetRevisionFileRevision;
// import fr.hd3d.model.persistence.IAssetRevisionFileRevision;
// import fr.hd3d.model.persistence.Persistors;
// import fr.hd3d.model.translator.Translators;
//
//
// /**
// * @author Try LAM
// */
// public class AssetRevisionFileRevisionsResource extends
// AbstractHd3dCollection<ILAssetRevisionFileRevision, IAssetRevisionFileRevision>
// {
//
// public AssetRevisionFileRevisionsResource() throws Hd3dException
// {
// super(Persistors.assetrevisionfilerevision, Translators.assetrevisionfilerevision);
// }
// }
