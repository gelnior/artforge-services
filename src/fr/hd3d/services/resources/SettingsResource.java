package fr.hd3d.services.resources;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.LockMode;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.impl.LSetting;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetSettingsCollectionCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.model.preferences.Setting;
import fr.hd3d.model.translator.impl.SettingTranslator;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.services.translator.json.JSonFormSerializer;
import fr.hd3d.utils.HibernateUtil;


public class SettingsResource extends AbstractHd3dCollectionResource<LSetting, Setting>
{
    final boolean self = "self".equals(getRequest().getOriginalRef().getLastSegment());
    IPerson person = AuthenticationUtil.getCurrentUser();

    public SettingsResource() throws Hd3dException
    {
        super(Setting.class, new SettingTranslator());
        allowMethods("POST,PUT");
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return getCollection(new GetSettingsCollectionCmd(getResourceContext()).wantTotalSize());
    }

    protected ResultCollection<Setting> reloadCollectionIfAny(final ResultCollection luceneFiltered, boolean onIdsOnly)
            throws Hd3dException
    {
        return luceneFiltered;
    }

    @Override
    protected List<LSetting> getLightWeightList(ResultCollection<Setting> list) throws Hd3dException
    {
        final List<LSetting> lightweightList = new ArrayList<LSetting>(list.getResult().size());
        for (Setting baseObject : list.getResult())
            lightweightList.add(getResourceContext().getTranslator().toLightweight(baseObject, getResourceContext()));
        return lightweightList;
    }

    @Override
    protected Representation getLightWeightRepresentation(Variant variant, List<LSetting> lightweightList, int nbRecords)
    {
        return RessourceSerializer.<LSetting> getRepresentation(variant, lightweightList, nbRecords);
    }

    protected void doPrePersistCollection(Representation entity, List<Setting> newObjects)
            throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    protected void doPersist(final List<Setting> objects) throws Hd3dException
    {
        if (objects == null || objects.isEmpty())
            return;

        HibernatePersist.persistRawCollection(objects);
    }

    protected void doPostPersist(List<Setting> objects) throws Hd3dPersistenceException
    {
        successfulPost(objects);
    }

    protected void doPrePersist(Representation entity, Setting object) throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    protected Setting newObject(Representation entity) throws Hd3dException
    {
        final LSetting lObject = JSonFormSerializer.<LSetting> deserialize(entity);
        SettingTranslator translator = new SettingTranslator();
        final Setting newObject = translator.fromLightweight(lObject, getResourceContext());
        IPerson person = AuthenticationUtil.getCurrentUser();
        if (person != null)
        {
            if (newObject.getPerson() == null)
                newObject.setPerson(person.getId());
            if (newObject.getLogin() == null)
                newObject.setLogin(person.getLogin());
        }

        return newObject;
    }

    protected List<Setting> newCollection(Representation entity) throws Hd3dException
    {
        List<LSetting> lObjects = JSonFormSerializer.<LSetting> deserializeList(entity);

        final IPerson person = AuthenticationUtil.getCurrentUser();

        List<Setting> objects = new ArrayList<Setting>(lObjects.size());
        for (LSetting light : lObjects)
        {
            SettingTranslator translator = new SettingTranslator();
            Setting setting = translator.fromLightweight(light, getResourceContext());
            if (person != null)
            {
                if (setting.getPerson() == null)
                    setting.setPerson(person.getId());
                if (setting.getLogin() == null)
                    setting.setLogin(person.getLogin());
            }
            objects.add(setting);
        }

        /* cleaning */
        for (LSetting light : lObjects)
            light.clear();
        lObjects.clear();
        lObjects = null;

        return objects;
    }

    protected List<Setting> retrieveObjects(Representation entity, List<LSetting> lObjects, LockMode lockMode)
            throws Hd3dTranslationException
    {
        return null;
    }

    protected void doPersist(final Setting object) throws Hd3dPersistenceException
    {
        HibernateUtil.safeTransaction(new IHibernateTransaction() {

            public void execute(org.hibernate.Session session) throws Hd3dException
            {
                session.save(object);
                // session.flush();
            }
        });
    }

    protected void doPostPersist(Setting object) throws Hd3dPersistenceException
    {
        successfulPost(object);
    }

    protected String getNewResourceIdentifier(Object newObject)
    {
        StringBuilder buf = new StringBuilder(getRequest().getResourceRef().getBaseRef().toString());
        buf.append('/').append(((Setting) newObject).getId());
        return buf.toString();
    }

    @Override
    protected void doPostUpdate(List<Setting> objects, Representation entity) throws Hd3dTranslationException
    {}

    @Override
    protected void doPreUpdate(Representation entity, List<LSetting> lObjects, List<Setting> objects)
            throws Hd3dTranslationException
    {
    /* nothing to do */
    }

    @Override
    protected void doUpdate(List<LSetting> lObjects, List<Setting> objects) throws Hd3dPersistenceException
    {/* nothing to do */}

    // ============================================================
    //
    // Override DELETE methods
    //
    // ============================================================
    protected boolean preCheck(final List<Setting> objects)
    {
        return true;
    }

    protected void doPreDelete(final List<Setting> objects) throws Hd3dException
    {
    /* nothing to do */
    }

    protected void doDelete(final List<Setting> objects) throws Hd3dException
    {
        ((HibernatePersist) getResourceContext().getPersistor()).deleteRawCollection(objects);
    }

    protected void doPostDelete(final List<Setting> objects) throws Hd3dException
    {
        /* nothing to do */
        getResponse().setStatus(Status.SUCCESS_OK);
    }

}
