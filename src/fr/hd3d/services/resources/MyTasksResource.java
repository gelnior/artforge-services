package fr.hd3d.services.resources;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonTasksCmd;
import fr.hd3d.services.security.utils.AuthenticationUtil;


public class MyTasksResource extends TasksResource
{
    public MyTasksResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected ResultCollection<ITask> getCollectionForRepresentation() throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user != null)
        {
            return doGetCollection(new GetPersonTasksCmd(getResourceContext(), user.getId()).wantTotalSize());
        }
        else
        {
            throw new Hd3dException("No user is set for this request, cannot returns tasks for current user.");
        }
    }

    @Override
    protected void doPrePersist(Representation entity, ITask newObject) throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user == null)
            return;
        if (user.getId().longValue() == newObject.getWorker().getId().longValue())
        {
            super.doPrePersist(entity, newObject);
        }
        else
        {
            throw new Hd3dException("You cannot create task you don't own.");
        }
    }
}
