package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILProxy;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * 
 * @author HD3D
 * 
 */
public class ProxyResource extends AbstractHd3dResource<ILProxy, IProxy>
{

    public ProxyResource() throws Hd3dException
    {
        super(Persistors.proxy, Translators.proxy);
    }

    @Override
    protected IProxy initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ServicesURI.PROXY_ID_ATTRIBUTE, lockMode);
    }

}
