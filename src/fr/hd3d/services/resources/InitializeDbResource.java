package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PERSONS;
import static fr.hd3d.common.client.ServicesURI.PROJECTS;

import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.Entity;
import fr.hd3d.common.client.Sheet;
import fr.hd3d.common.client.enums.EAssetStatus;
import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.common.client.enums.EFileStatus;
import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.audit.Audit;
import fr.hd3d.model.lightweight.impl.LCategory;
import fr.hd3d.model.lightweight.impl.LConstituent;
import fr.hd3d.model.lightweight.impl.LPerson;
import fr.hd3d.model.lightweight.impl.LProject;
import fr.hd3d.model.lightweight.impl.LResourceGroup;
import fr.hd3d.model.lightweight.impl.LRole;
import fr.hd3d.model.lightweight.impl.LSequence;
import fr.hd3d.model.lightweight.impl.LShot;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.ApprovalNoteH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionGroupH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.AssetRevisionLinkH;
import fr.hd3d.model.persistence.impl.hibernate.ClassDynMetaDataTypeH;
import fr.hd3d.model.persistence.impl.hibernate.CompositionH;
import fr.hd3d.model.persistence.impl.hibernate.DynMetaDataTypeH;
import fr.hd3d.model.persistence.impl.hibernate.EntityTaskLinkH;
import fr.hd3d.model.persistence.impl.hibernate.FileAttributeH;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionH;
import fr.hd3d.model.persistence.impl.hibernate.FileRevisionLinkH;
import fr.hd3d.model.persistence.impl.hibernate.ItemGroupH;
import fr.hd3d.model.persistence.impl.hibernate.ItemH;
import fr.hd3d.model.persistence.impl.hibernate.ListValuesH;
import fr.hd3d.model.persistence.impl.hibernate.MountPointH;
import fr.hd3d.model.persistence.impl.hibernate.PersonDayH;
import fr.hd3d.model.persistence.impl.hibernate.PlanningH;
import fr.hd3d.model.persistence.impl.hibernate.SheetH;
import fr.hd3d.model.persistence.impl.hibernate.SimpleActivityH;
import fr.hd3d.model.persistence.impl.hibernate.StepH;
import fr.hd3d.model.persistence.impl.hibernate.TagCategoryH;
import fr.hd3d.model.persistence.impl.hibernate.TagH;
import fr.hd3d.model.persistence.impl.hibernate.TaskActivityH;
import fr.hd3d.model.persistence.impl.hibernate.TaskChangesH;
import fr.hd3d.model.persistence.impl.hibernate.TaskGroupH;
import fr.hd3d.model.persistence.impl.hibernate.TaskH;
import fr.hd3d.model.persistence.impl.hibernate.TaskTypeH;
import fr.hd3d.model.persistence.impl.hibernate.WorkingTimeH;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.model.preferences.Setting;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.auth.FDTAuthUtils;
import fr.hd3d.services.security.dao.IUserAccount;
import fr.hd3d.services.security.dao.UserAccountH;
import fr.hd3d.utils.HibernateUtil;


/**
 * Test class to initialize DB with some test values.
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("Initialise la base de données avec un jeu de tests.")
public class InitializeDbResource<L, P> extends AbstractHd3dBaseResource
{
    public InitializeDbResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    /**
     * (non-Javadoc)
     * 
     * @see org.restlet.resource.Resource#getRepresentation(org.restlet.resource. Variant)
     */
    @Override
    public Representation get(Variant arg0)
    {
        final String cmdToRun = getQuery().getValues("cmd");

        try
        {
            init(cmdToRun);
        }
        catch (Hd3dTranslationException e)
        {
            e.printStackTrace();
            return new StringRepresentation(e.getMessage());
        }
        return new StringRepresentation("database initialized...");
    }

    public static void init(String cmd) throws Hd3dPersistenceException
    {
        if (StringUtils.isBlank(cmd) || "ProjectsTasksActivities".equalsIgnoreCase(cmd))
        {
            populateProjectsTasksActivities();
        }
        // Categories & constituents
        if (StringUtils.isBlank(cmd) || "CategoriesConstituents".equalsIgnoreCase(cmd))
        {
            populateCategoriesConstituents();
        }

        // Sequences & shots
        if (StringUtils.isBlank(cmd) || "SequenceShots".equalsIgnoreCase(cmd))
        {
            populateSequenceShots();
        }
        // metadata
        if (StringUtils.isBlank(cmd) || "DynMetaData".equalsIgnoreCase(cmd))
        {
            populateDynMetaData();
        }
        // Sheets, ItemGroups, Items
        if (StringUtils.isBlank(cmd) || "Sheets".equalsIgnoreCase(cmd))
        {
            populateSheets();
        }
        if (StringUtils.isBlank(cmd) || "Tags".equalsIgnoreCase(cmd))
        {
            populateTags();
        }
        if (StringUtils.isBlank(cmd) || "TagCategories".equalsIgnoreCase(cmd))
        {
            populateTagCategories();
        }
        if (StringUtils.isBlank(cmd) || "ContentUnitCompos".equalsIgnoreCase(cmd))
        {
            populateCompos();
        }

        if (StringUtils.isBlank(cmd) || "ScriptExec".equalsIgnoreCase(cmd))
        {
            populateScriptExec();
        }
        if (StringUtils.isBlank(cmd) || "Assets".equalsIgnoreCase(cmd))
        {
            populateAssets();
        }
        if (StringUtils.isBlank(cmd) || "EntityTaskLink".equalsIgnoreCase(cmd))
        {
            populateEntityTaskLink();
        }
        if (StringUtils.isBlank(cmd) || "ComputersAdmin".equalsIgnoreCase(cmd))
        {
            populateComputersAdmin();
        }
        if (StringUtils.isBlank(cmd) || "Steps".equalsIgnoreCase(cmd))
        {
            populateSteps();
        }
        if (StringUtils.isBlank(cmd) || "Audits".equalsIgnoreCase(cmd))
        {
            populateAudits();
        }
        if (StringUtils.isBlank(cmd) || "Settings".equalsIgnoreCase(cmd))
        {
            populateSettings();
        }
        if ("BasicLoadTest".equalsIgnoreCase(cmd))
        {
            populateBasicLoadTest();
        }

    }

    public static void populateProjectsTasksActivities() throws Hd3dPersistenceException
    {
        run(new InitDbCmd());
    }

    public static void populateComputersAdmin() throws Hd3dPersistenceException
    {
        run(new InitDbCmd_ComputersAdmin());
    }

    public static void populateCategoriesConstituents() throws Hd3dPersistenceException
    {
        run(new InitDbCmd_2());
        run(new InitSubCategory());
        run(new InitCategory());
        run(new InitCategory_1());
        run(new InitConstituents());
    }

    public static void populateSequenceShots() throws Hd3dPersistenceException
    {
        run(new InitSequence());
        run(new InitSubSequence());
        run(new InitSubSequence_0());
        run(new InitSequence_1());
        run(new InitSequence_2());
        run(new InitShots());
    }

    public static void populateSheets() throws Hd3dPersistenceException
    {
        run(new InitSheets());
    }

    public static void populateTags() throws Hd3dPersistenceException
    {
        run(new InitTags());
    }

    public static void populateTagCategories() throws Hd3dPersistenceException
    {
        run(new InitTagCategories());
    }

    public static void populateCompos() throws Hd3dPersistenceException
    {
        run(new InitCompos());
    }

    public static void populateDynMetaData() throws Hd3dPersistenceException
    {
        run(new InitDynMetaData());
    }

    public static void populateScriptExec() throws Hd3dPersistenceException
    {
        run(new InitScriptExec());
    }

    public static void populateAssets() throws Hd3dPersistenceException
    {
        run(new InitAssets());
    }

    public static void populateEntityTaskLink() throws Hd3dPersistenceException
    {
        run(new InitEntityTaskLinks());
    }

    public static void populateAudits() throws Hd3dPersistenceException
    {
        run(new InitAudits());
    }

    public static void populateSteps() throws Hd3dPersistenceException
    {
        run(new InitSteps());
    }

    public static void populateSettings() throws Hd3dPersistenceException
    {
        run(new InitSettings());
    }

    public static void populateBasicLoadTest() throws Hd3dPersistenceException
    {
        run(new InitBasicLoadTest());
    }

    private static void run(IHibernateTransaction tx) throws Hd3dPersistenceException
    {
        // Bench b = new Bench();
        // b.b();
        Session session = HibernateUtil.currentSession();
        Transaction trx = session.beginTransaction();
        HibernateUtil.safeTransaction(tx);
        if (session.getFlushMode() == FlushMode.MANUAL)
        {
            session.flush();
        }
        trx.commit();
        trx = null;

        // Log.logger.debug("{} took: {} ms", tx.getClass().getName(), b.e());
        tx = null;
        HibernateUtil.closeSession();

    }

    /**
     * Database Initialization : persons, projects, tasks, activities and days
     * 
     * @author Thomas Eskénazi
     */
    private static class InitDbCmd implements IHibernateTransaction
    {
        private static final int WORKING_DAY_IN_MILLIS = 24 * 60 * 60 * 1000;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            // Persons
            final IPerson gch = getPerson(session, "gch", "Guillaume", "Chatelet");
            final IPerson eth = getPerson(session, "eth", "Thomas", "Eskénazi");
            final IPerson acs = getPerson(session, "acs", "Arnaud", "Chassagne");

            // Projects
            final IProject projectA = getProject(session, "ProjectA", EProjectStatus.OPEN, "#FF0000", "ProjectAhook");// TODO:
            // ""
            // causes
            // crash !
            final IProject projectB = getProject(session, "ProjectB", EProjectStatus.OPEN, "#0000FF", "ProjectBhook");
            final IProject projectC = getProject(session, "ProjectC", EProjectStatus.CLOSED, "#00FF00", "ProjectChook");

            // ResourceGroups
            for (int i = 0; i < 10; i++)
            {
                getResourceGroup(session, "Test_group_0" + i);
            }
            for (int i = 10; i < 100; i++)
            {
                getResourceGroup(session, "Test_group_" + i);
            }
            final IResourceGroup group = getResourceGroup(session, "Test_group_00");
            group.addPerson(gch);
            group.addPerson(acs);
            group.addPerson(eth);

            Set<IProject> projects = new HashSet<IProject>(1);
            projects.add(projectA);
            group.setProjects(projects);
            group.doPreUpdate();
            session.update(group);
            session.flush();

            final IResourceGroup group_1 = getResourceGroup(session, "Test_group_01");
            group_1.addPerson(gch);

            Set<IProject> projects_1 = new HashSet<IProject>(1);
            projects_1.add(projectB);
            group_1.setProjects(projects_1);
            group_1.doPreUpdate();
            session.update(group_1);
            session.flush();

            // Roles
            for (int i = 0; i < 10; i++)
            {
                getRole(session, "Test_role_0" + i);
            }
            for (int i = 10; i < 100; i++)
            {
                getRole(session, "Test_role_" + i);
            }
            IRole role = getRole(session, "Test_role_00");
            IResourceGroup group2 = getResourceGroup(session, "Test_group_00");
            HashSet<IResourceGroup> groups = new HashSet<IResourceGroup>();
            HashSet<String> perms = new HashSet<String>();
            groups.add(group2);
            role.setResourceGroups(groups);
            perms.add(format("v1:{0}:*:read", '/' + PERSONS));
            perms.add(format("v1:{0}:*:read", '/' + PROJECTS));
            role.setPermissions(perms);

            // HashSet<String> bans = new HashSet<String>();
            // bans.add(format("v1:{0}:*:read", Hd3dApplication.PERSONS));
            // role.setBans(bans);

            role.doPreUpdate();
            session.update(role);
            session.flush();

            /* ACL */
            // final String toAdd = String.valueOf(projectA.getId()) + ' ' + String.valueOf(projectB.getId()) + ' '
            // + String.valueOf(projectC.getId());
            // IHd3dACL newACL = new Hd3dACLH();
            // Set<IRole> roles = new HashSet<IRole>();
            // roles.add(role);
            // newACL.setRoles(roles);
            // newACL.setEntityName("Project");
            // newACL.setReadPermitted(toAdd);
            // // newACL.setReadBans(readBans);
            // newACL.setCreatePermitted("*");
            // newACL.setUpdatePermitted(toAdd);
            // // newACL.setUpdateBans(updateBans);
            // newACL.setDeletePermitted(toAdd);
            // // newACL.setDeleteBans(deleteBans);
            // newACL.doPrePersist();
            // Persistors.acl.persist(newACL);
            // session.flush();
            // newACL.doPostPersist();
            // session.flush();
            // Time
            // int sec = 60000;
            final IPersonDay gchDay0 = getPersonDay(session, new Date(0), gch);
            // final IPersonDay gchDay0 = null;

            // working times
            IWorkingTime workingTime_1 = new WorkingTimeH(gchDay0, fr.hd3d.utils.StringUtils
                    .parseDate("2010-05-24 08:00:00"), fr.hd3d.utils.StringUtils.parseDate("2010-05-24 12:00:00"));
            IWorkingTime workingTime_2 = new WorkingTimeH(gchDay0, fr.hd3d.utils.StringUtils
                    .parseDate("2010-05-24 13:00:00"), fr.hd3d.utils.StringUtils.parseDate("2010-05-24 18:30:00"));
            workingTime_1.doPrePersist();
            session.save(workingTime_1);
            workingTime_1.doPostPersist();
            session.flush();
            workingTime_2.doPrePersist();
            session.save(workingTime_2);
            workingTime_2.doPostPersist();
            session.flush();

            // 
            appendStaffToProject(session, projectA, gch, acs, eth);
            session.flush();
            appendStaffToProject(session, projectB, gch);

            // Refresh data
            session.flush();

            // Activities
            getSimpleActivity(session, ESimpleActivityType.MEETING, gchDay0, 2L, "A simple task", acs, projectB);
            getSimpleActivity(session, ESimpleActivityType.MEETING, gchDay0, 5L, "A simple task for gch", gch, projectA);
            getSimpleActivity(session, ESimpleActivityType.MEETING, gchDay0, 10L, "Another gch", gch, projectA);

            // Planning
            IPlanning planning1A = getPlanning(session, projectA, "Planning1A");
            // to create some more
            getPlanning(session, projectA, "Planning2A");
            getPlanning(session, projectB, "Planning1B");
            getPlanning(session, projectC, "Planning1C");

            // TaskGroup

            // Tasks & TaskGroup & TaskChanges
            Date startDate = new Date();
            Date endDate = null;
            Calendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(startDate.getTime() + (3 * WORKING_DAY_IN_MILLIS));
            endDate = calendar.getTime();
            ITaskGroup parentGroup = getTaskGroup(session, "#dd0000", "Characters", null, planning1A, startDate,
                    endDate);
            // first sub group
            calendar.setTimeInMillis(startDate.getTime() + WORKING_DAY_IN_MILLIS);
            endDate = calendar.getTime();
            getTaskGroup(session, "#dddd00", "Modeling", parentGroup, planning1A, startDate, endDate);
            // second sub group
            startDate = (Date) endDate.clone();
            calendar.setTimeInMillis(calendar.getTimeInMillis() + (2 * WORKING_DAY_IN_MILLIS));
            endDate = calendar.getTime();
            getTaskGroup(session, "#0000dd", "Setup", parentGroup, planning1A, startDate, endDate);
            // add task
            calendar.setTimeInMillis(calendar.getTimeInMillis() - (3 * WORKING_DAY_IN_MILLIS));
            startDate = calendar.getTime();
            Long duration = (endDate.getTime() - startDate.getTime()) / 1000L;// in seconds

            ITaskType design = getTaskType(session, "Design", "#99FE80", "The Design Task Type");

            ITaskType modeling = getTaskType(session, "Modeling", "#009999", "The Modeling Task Type");

            // ITaskType setup =
            getTaskType(session, "Setup", "#00B366", "The Setup Task Type");
            getTaskType(session, "Setup", "#00B366", "The Setup Task Type", projectA);

            // ITaskType texturing =
            getTaskType(session, "Texturing", "#FE8080", "The Texturing Task Type");
            getTaskType(session, "Texturing", "#FE8080", "The Texturing Task Type", projectA);

            // ITaskType shading =
            getTaskType(session, "Shading", "#FEE680", "The Shading Task Type");

            // ITaskType hairs =
            getTaskType(session, "Hairs", "#FE80DF", "The Hairs Task Type");

            // ITaskType mattePainting =
            getTaskType(session, "MattePainting", "#FE9980", "The MattePainting Task Type");

            // ITaskType layout =
            getTaskType(session, "Layout", "#FECC80", "The Layout Task Type");

            // ITaskType export =
            getTaskType(session, "Export", "#990099", "The Export Task Type");

            ITaskType animation = getTaskType(session, "Animation", "#B27D00", "The Animation Task Type");

            // ITaskType tracking =
            getTaskType(session, "Tracking", "#00477D", "The Tracking Task Type");

            // ITaskType rendering =
            getTaskType(session, "Rendering", "#FF9900", "The Rendering Task Type");

            // ITaskType dressing =
            getTaskType(session, "Dressing", "#6BB200", "The Dressing Task Type");

            ITaskType lighting = getTaskType(session, "Lighting", "#FF3300", "The Lighting Task Type");

            // ITaskType compositing =
            getTaskType(session, "Compositing", "#80FEC8", "The Compositing Task Type");

            ITask taskTest = getTask(session, projectA, "Test_Task_00", ETaskStatus.OK, eth, eth, startDate, endDate,
                    duration, null, startDate, endDate, null, null, null, design);
            getTaskChanges(session, eth, startDate, endDate, duration, planning1A, taskTest, null);
            calendar.setTimeInMillis(startDate.getTime() - (3 * WORKING_DAY_IN_MILLIS));
            startDate = calendar.getTime();
            calendar.setTimeInMillis(startDate.getTime() + (7 * WORKING_DAY_IN_MILLIS));
            endDate = calendar.getTime();
            duration = (endDate.getTime() - startDate.getTime()) / 1000L;// in seconds

            ITask task = getTask(session, projectA, "One super task", ETaskStatus.OK, acs, gch, startDate, endDate,
                    duration, null, startDate, endDate, null, null, null, animation);
            getTaskChanges(session, gch, startDate, endDate, duration, planning1A, task, null);
            calendar.setTimeInMillis(startDate.getTime() + (8 * WORKING_DAY_IN_MILLIS));
            startDate = calendar.getTime();
            calendar.setTimeInMillis(startDate.getTime() + (4 * WORKING_DAY_IN_MILLIS));
            endDate = calendar.getTime();
            duration = (endDate.getTime() - startDate.getTime()) / 1000L;// in seconds
            getTask(session, projectA, "No user task", ETaskStatus.OK, eth, null, null, null, duration, null, null,
                    null, null, null, null, modeling);
            getTaskActivity(session, gchDay0, 5L, "Did a few things", task, gch);

            ITask task2 = getTask(session, projectA, "Tracking_Ciel", ETaskStatus.OK, eth, eth, startDate, endDate,
                    duration, null, startDate, endDate, null, null, null, lighting);
            getTaskChanges(session, eth, startDate, endDate, duration, planning1A, task2, null);
            final IPersonDay ethDay0 = getPersonDay(session, new Date(0), eth);
            getTaskActivity(session, ethDay0, 3600L, "Begin tracking", task2, eth);
            getTaskActivity(session, ethDay0, 4800L, "End tracking", task2, eth);
            getTaskActivity(session, ethDay0, 1200L, "Redone tracking", task2, eth);

            // Commit
            session.flush();
        }
    }

    private static class InitDbCmd_ComputersAdmin implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            // init Roles
            // admin
            IRole admin = getRole(session, "computers_admin");
            admin.add("v1:*");
            session.save(admin);
            // regular user
            IRole regular = getRole(session, "computers_regularUser");
            regular.add("v1:computers:*");
            regular.add("v1:devices:*");
            regular.add("v1:screens:*");
            regular.add("v1:licences:*");
            regular.add("v1:softwares:*");
            regular.add("v1:studiomaps:*");
            regular.add("v1:pools:*");
            regular.add("v1:manufacturers:*");
            regular.add("v1:processors:*");
            regular.add("v1:computertypes:*:read");
            regular.add("v1:computermodels:*:read");
            regular.add("v1:devicetypes:*:read");
            regular.add("v1:devicemodels:*:read");
            regular.add("v1:screenmodels:*:read");
            regular.add("v1:sheets:*:read");
            session.save(regular);
            // simple user
            IRole simple = getRole(session, "computers_simpleUser");
            simple.add("v1:computers:*:read");
            simple.add("v1:devices:*:read");
            simple.add("v1:screens:*:read");
            simple.add("v1:softwares:*:read");
            simple.add("v1:licences:*:read");
            simple.add("v1:pools:*:read");
            simple.add("v1:processors:*:read");
            simple.add("v1:computermodels:*:read");
            simple.add("v1:devicemodels:*:read");
            simple.add("v1:screenmodels:*:read");
            simple.add("v1:sheets:*:read");
            session.save(simple);
            session.flush();

            // init Users
            IPerson chef = getPerson(session, "chef", "Conan", "Lebarbare");
            session.save(chef);
            IUserAccount aChef = getAccount(session, "chef");
            aChef.setSecret(FDTAuthUtils.encrypt("chefpass"));
            session.save(aChef);
            IPerson ing = getPerson(session, "ing", "Jean-Alphonse", "Laporte");
            session.save(ing);
            IUserAccount aIng = getAccount(session, "ing");
            aIng.setSecret(FDTAuthUtils.encrypt("ingpass"));
            session.save(aIng);
            IPerson gra = getPerson(session, "graph", "Vassily", "Kandinsky");
            session.save(gra);
            IUserAccount aGraph = getAccount(session, "graph");
            aGraph.setSecret(FDTAuthUtils.encrypt("graphpass"));
            session.save(aGraph);
            session.flush();

            // init Groups
            // admins
            IResourceGroup group = getResourceGroup(session, "administrateur parc machine");
            group.add(admin);
            group.addPerson(chef);
            session.save(group);
            // engineers
            IResourceGroup engineer = getResourceGroup(session, "ingénieur parc machine");
            engineer.add(regular);
            engineer.addPerson(ing);
            session.save(engineer);
            // graph
            IResourceGroup graph = getResourceGroup(session, "graphiste 1D");
            graph.add(simple);
            graph.addPerson(gra);
            session.save(graph);
            session.flush();
        }
    }

    // TL LotA12
    // Need to make another TRANSACTION (to have an autoincrement id)
    // /////////////////////////////
    // CATEGORIES and CONSTITUENTS
    // /////////////////////////////
    /**
     * creates "personnage" CATEGORY (sub category of CONSTITUENT) for project A
     */
    private static class InitDbCmd_2 implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            this.session = session;
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");
            final Long projectAId = projectA.getId();

            ICategory constituent = projectA.rootCategory();
            ICategory personnage = getCategory(this.session, null, null, "Personnage", constituent.getId(), projectAId);
            projectA.addCategory(personnage);
            projectA.doPreUpdate();
        }
    }

    /**
     * creates "animaux" SUB-CATEGORY (sub category of Personnage) for project A
     */
    private static class InitSubCategory implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {

            this.session = session;
            // project A
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");
            final Long projectAId = projectA.getId();
            // project A "Personnage" Category
            Iterator<ICategory> it = projectA.getCategories().iterator();
            ICategory personnageCategory = null;
            while (it.hasNext())
            {
                ICategory c = it.next();
                if (c.getName().equals("Personnage"))
                {
                    personnageCategory = c;
                    break;
                }
            }
            if (personnageCategory != null)
            {
                // add a "animaux" sub category to "Personnage"
                ICategory animaux = getCategory(this.session, null, null, "Animaux", personnageCategory.getId(),
                        projectAId);
                projectA.addCategory(animaux);
                // add a "Martiens" sub category to "Personnage"
                ICategory martiens = getCategory(this.session, null, null, "Martiens", personnageCategory.getId(),
                        projectAId);
                projectA.addCategory(martiens);
                projectA.doPreUpdate();
            }
        }
    }

    /**
     * creates "decor" CATEGORY (sub category of CONSTITUENT) for project A
     */
    private static class InitCategory implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {

            this.session = session;
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");
            final Long projectAId = projectA.getId();
            ICategory constituent = projectA.rootCategory();
            ICategory decor = getCategory(this.session, null, null, "Decor", constituent.getId(), projectAId);
            projectA.addCategory(decor);
            projectA.doPreUpdate();
        }
    }

    /**
     * creates "decor" CATEGORY (sub category of CONSTITUENT) for project B
     */
    private static class InitCategory_1 implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {

            this.session = session;
            final IProject projectB = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectB");
            final Long projectAId = projectB.getId();
            ICategory constituent = projectB.rootCategory();
            ICategory decor = getCategory(this.session, null, null, "Decor", constituent.getId(), projectAId);
            projectB.addCategory(decor);
            projectB.doPreUpdate();
        }
    }

    /**
     * creates CONSTITUENTs for "decor" CATEGORY (sub category of CONSTITUENT) for project A
     */
    private static class InitConstituents implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            // project A
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");
            Iterator<ICategory> it = projectA.getCategories().iterator();
            ICategory decor = null;
            while (it.hasNext())
            {
                ICategory cat = it.next();
                if (cat.getName().equals("Decor"))
                {
                    decor = cat;
                }
            }
            // Constituents
            if (decor != null)
            {
                // IConstituent constituent1 =
                getConstituentById(session, null, decor.getId(), decor.getName() + "_1", 0,
                        "constituent 1 description...&<>!?%@", 1, "hook_1", "const 1 label", 0, null);
                // IConstituent constituent2 =
                getConstituentById(session, null, decor.getId(), decor.getName() + "_2", 0,
                        "constituent 2 une description...&<>!?%@", 1, "hook_2", "const 2 label", 0, null);
                // IConstituent constituent3 =
                getConstituentById(session, null, decor.getId(), decor.getName() + "_3", 0, "le ciel bleu", 1, "CIEL",
                        "ciel", 0, null);
            }

            // project A
            it = projectA.getCategories().iterator();
            ICategory personnage = null;
            ICategory animaux = null;
            while (it.hasNext())
            {
                ICategory cat = it.next();
                if (cat.getName().equals("Personnage"))
                    personnage = cat;
                if (cat.getName().equals("Animaux"))
                    animaux = cat;
            }
            // Constituents
            if (personnage != null)
            {
                // IConstituent constituent3 =
                getConstituentById(session, null, personnage.getId(), personnage.getName(), 0,
                        "constituent 3 description...", 1, "hook_3", "const 1 label", 0, null);
                // IConstituent constituent4 =
                getConstituentById(session, null, personnage.getId(), personnage.getName(), 0,
                        "constituent 4 une description...", 1, "hook_4", "const 2 label", 0, null);
            }
            if (animaux != null)
            {
                // IConstituent constituent5 =
                getConstituentById(session, null, animaux.getId(), animaux.getName(), 0,
                        "constituent 5 description...", 1, "hook_5", "Const_Animaux_5", 0, null);
                // IConstituent constituent6 =
                getConstituentById(session, null, animaux.getId(), animaux.getName(), 0,
                        "constituent 6 une description...", 1, "hook_6", "Const_Animaux_6", 0, null);
            }
        }
    }

    // ///////////////////////
    // SEQUENCES and SHOTS
    // ///////////////////////
    /**
     * creates "Pilote" SEQUENCE (sub sequence of SEQUENCE) for project A <br>
     * 
     * Project A <br>
     * |__"Pilote" sequence
     */
    private static class InitSequence implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {

            this.session = session;
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");
            final Long projectAId = projectA.getId();

            ISequence sequence = projectA.rootSequence();
            ISequence pilote = getSequence(this.session, null, null, "Pilote", "Titre", sequence.getId(), projectAId);
            projectA.addSequence(pilote);
            projectA.doPreUpdate();
        }
    }

    /**
     * creates "il était UNE fois..." SUB-SEQUENCE (sub sequence of Pilote) for project A <br>
     * Project A <br>
     * |__"Pilote" sequence <br>
     * <br>
     * &nbsp;|_"il était UNE fois..." <br>
     * <br>
     * &nbsp;|_"il était DEUX fois..."<br>
     * <br>
     * |__"AAACtion" sequence <br>
     */
    private static class InitSubSequence implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {

            this.session = session;
            // project A
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");
            final Long projectAId = projectA.getId();
            // project A "Pilote" sequence
            Iterator<ISequence> it = projectA.getSequences().iterator();
            ISequence piloteSequence = null;
            while (it.hasNext())
            {
                ISequence s = it.next();
                if (s.getName().equals("Pilote"))
                {
                    piloteSequence = s;
                    break;
                }
            }
            // Add subsequences
            if (piloteSequence != null)
            {
                // add a "il était UNE fois..." sub sequence to "Pilote"
                ISequence iletaitunefois = getSequence(this.session, null, null, "il était UNE fois...", "Titre",
                        piloteSequence.getId(), projectAId);
                projectA.addSequence(iletaitunefois);
                // add a "il était DEUX fois..." sub sequence to "Pilote"
                ISequence iletaitdeuxfois = getSequence(this.session, null, null, "il était DEUX fois...", "Titre",
                        piloteSequence.getId(), projectAId);
                projectA.addSequence(iletaitdeuxfois);
                projectA.doPreUpdate();
            }
        }
    }

    /**
     * creates "Sous-sous sequence 1/2" SUB-SEQUENCE (sub sequence of "il était UNE fois...", sub sequence of "Pilote")
     * for project A <br>
     * Project A <br>
     * |__"Pilote" sequence <br>
     * <br>
     * &nbsp;|_"il était UNE fois..." <br>
     * <br>
     * &nbsp;&nbsp;|_"Sous-sous sequence 1" <br>
     * <br>
     * &nbsp;&nbsp;|_"Sous-sous sequence 2" <br>
     * <br>
     * &nbsp;|_"il était DEUX fois..."<br>
     * <br>
     * |__"AAACtion" sequence <br>
     */
    private static class InitSubSequence_0 implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {

            this.session = session;
            // project A
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");
            final Long projectAId = projectA.getId();
            // project A "Pilote" sequence
            Iterator<ISequence> it = projectA.getSequences().iterator();
            ISequence piloteSequence = null;
            while (it.hasNext())
            {
                ISequence s = it.next();
                if (s.getName().equals("Pilote"))
                {
                    piloteSequence = s;
                    break;
                }
            }
            // Add subsequences
            if (piloteSequence != null)
            {
                ISequence subseq = null;
                Iterator<ISequence> it_0 = piloteSequence.getChildren().iterator();
                while (it_0.hasNext())
                {
                    ISequence s = it_0.next();
                    if (s.getName().equals("il était UNE fois..."))
                    {
                        subseq = s;
                        break;
                    }
                }

                if (subseq != null)
                {
                    // add a "Sous-sous sequence 1" sub sequence to "il était UNE fois..."
                    ISequence subSubSeq = getSequence(this.session, null, null, "Sous-sous sequence 1", "Titre", subseq
                            .getId(), projectAId);
                    projectA.addSequence(subSubSeq);
                    // add a "Sous-sous sequence 2" sub sequence to "il était UNE fois..."
                    ISequence subSubSeq_1 = getSequence(this.session, null, null, "Sous-sous sequence 2", "Titre",
                            subseq.getId(), projectAId);
                    projectA.addSequence(subSubSeq_1);
                    projectA.doPreUpdate();
                }
            }
        }
    }

    /**
     * creates "AAACtion !" SEQUENCE (sub sequence of SEQUENCE) for project A<br>
     * Project A <br>
     * |__"AAACtion" sequence <br>
     */
    private static class InitSequence_1 implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {

            this.session = session;
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");
            final Long projectAId = projectA.getId();
            ISequence sequence = projectA.rootSequence();
            ISequence action = getSequence(this.session, null, null, "AAACtion", "Titre", sequence.getId(), projectAId);
            projectA.addSequence(action);
            projectA.doPreUpdate();
        }
    }

    /**
     * creates "AAACtion !" SEQUENCE (sub sequence of SEQUENCE) for project B<br>
     * Project B <br>
     * |__"AAACtion" sequence <br>
     */
    private static class InitSequence_2 implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {

            this.session = session;
            final IProject projectB = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectB");
            final Long projectBId = projectB.getId();
            ISequence sequence = projectB.rootSequence();
            ISequence action = getSequence(this.session, null, null, "AAACtion", "Titre", sequence.getId(), projectBId);
            projectB.addSequence(action);
            projectB.doPreUpdate();
        }
    }

    /**
     * creates SHOTs for "AAACtion !" SEQUENCE (sub sequence of SEQUENCE) for project A
     */
    private static class InitShots implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            // ///////////
            // project A
            // ///////////
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");
            Iterator<ISequence> it = projectA.getSequences().iterator();
            ISequence action = null;
            while (it.hasNext())
            {
                ISequence s = it.next();
                if (s.getName().equals("AAACtion"))
                {
                    action = s;
                }
            }
            // Shots
            if (action != null)
            {
                // IShot shot1 =
                getShot(session, null, action.getId(), 0, "shot 1 description...", 1, "hook_1", "shot 1 label", 0, null);
                // IShot shot2 =
                getShot(session, null, action.getId(), 0, "shot 2 une description...", 1, "hook_2", "shot 2 label", 0,
                        null);
            }
            //
            it = projectA.getSequences().iterator();
            ISequence pilote = null;
            while (it.hasNext())
            {
                ISequence s = it.next();
                if (s.getName().equals("Pilote"))
                {
                    pilote = s;
                }
            }
            // Shots
            if (pilote != null)
            {
                // IShot shot3 =
                getShot(session, null, pilote.getId(), 0, "shot 3 description...", 1, "hook_3", "shot 3 label", 0, null);
                // IShot shot4 =
                getShot(session, null, pilote.getId(), 0, "shot 4 une description...", 1, "hook_4", "shot 4 label", 0,
                        null);
            }
        }

    }

    /**
     * creates Sheets for project A
     */
    private static class InitSheets implements IHibernateTransaction
    {
        // private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            // ///////////
            // project A
            // ///////////
            // this.session = session;
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");

            // add sheets
            ISheet sheet1 = new SheetH("My Sheet 1", "this is my sheet 1", projectA, "Constituent", null);
            projectA.addSheet(sheet1);
            ISheet sheet2 = new SheetH("My Sheet 2", "this is my sheet 2", projectA, "Constituent", null);
            projectA.addSheet(sheet2);
            ISheet sheet3 = new SheetH("Project Sheet", "my project sheet", projectA, "Project", null);
            projectA.addSheet(sheet3);
            projectA.doPreUpdate();
            session.update(projectA);
            session.flush();

            // add itemGroups
            IItemGroup itemGroup1 = new ItemGroupH("Portlet 1", null, sheet1, "hook 1");
            IItemGroup itemGroup2 = new ItemGroupH("Portlet 2", null, sheet1, "hook 2");
            IItemGroup itemGroup3 = new ItemGroupH("Project Portlet", null, sheet3, "hook 3");

            session.update(sheet1);
            session.update(sheet2);
            session.update(sheet3);
            session.flush();

            itemGroup1.doPreUpdate();
            session.update(itemGroup1);
            itemGroup2.doPreUpdate();
            session.update(itemGroup2);
            itemGroup3.doPreUpdate();
            session.update(itemGroup3);
            session.flush();

            // create items
            IItem item1 = new ItemH("Label", Sheet.ITEMTYPE_ATTRIBUTE, "label", "control content", itemGroup1,
                    "java.lang.String", "fr.hd3d.services.resources.transformer.TestTransformer", null);
            IItem item2 = new ItemH("Complexity", Sheet.ITEMTYPE_ATTRIBUTE, "difficulty", "control content",
                    itemGroup1, "java.lang.Long", "fr.hd3d.services.resources.transformer.TestTransformer", null);
            IItem item3 = new ItemH("Category", Sheet.ITEMTYPE_ATTRIBUTE, "category", "control content", itemGroup1,
                    "Category", "fr.hd3d.services.resources.transformer.TestTransformer", null);
            // IItem item4 = new ItemHibernateImpl("Temps passé", "method", "activitiesDuration", "control content",
            // itemGroup1, "java.lang.Long", "fr.hd3d.services.resources.transformer.TestTransformer", null);
            IItem item5 = new ItemH("Name", Sheet.ITEMTYPE_ATTRIBUTE, "name", "", itemGroup3, "java.lang.String",
                    "String", null);

            final IClassDynMetaDataType classDynType = Persistors.classdynmetadatatype.getObjectByValue("name",
                    "List of numbers");

            IItem item6 = new ItemH("List of numbers", Sheet.ITEMTYPE_METADATA, String.valueOf(classDynType.getId()),
                    "", itemGroup3, "java.lang.Long", "String", null);

            item1.doPrePersist();
            session.save(item1);
            session.flush();

            item2.doPrePersist();
            session.save(item2);
            session.flush();

            item3.doPrePersist();
            session.save(item3);
            session.flush();

            // item4.doPrePersist();
            // session.save(item4);
            // session.flush();

            item5.doPrePersist();
            session.save(item5);
            session.flush();

            item6.doPrePersist();
            session.save(item6);
            session.flush();
        }
    }

    private static class InitTags implements IHibernateTransaction
    {
        // private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {

            // this.session = session;
            // ///////////
            // project A
            // ///////////
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");
            final IProject projectB = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectB");

            // one tag attached to projects A et B
            ITag tag = new TagH("Generic Project tag");
            tag.addParent(projectA);
            tag.addParent(projectB);
            tag.doPrePersist();
            session.save(tag);
            session.flush();

            // one tag attached to projectA
            ITag projectATag = new TagH("Project A tag");
            projectATag.addParent(projectA);
            projectATag.doPrePersist();
            session.save(projectATag);
            session.flush();

            // add different tags to different constituents
            List<IConstituent> constituents = Persistors.constituent.getByValue(null, "label", "const 1 label");

            ITag tag1;
            for (int i = 0; i < constituents.size(); i++)
            {
                tag1 = new TagH("Constituent tag Name" + i);
                tag1.addParent(constituents.get(i));
                tag1.doPrePersist();
                session.save(tag1);
                session.flush();
            }

            // create a tag for all the groups
            ITag groupTag = new TagH("Generic GROUP tag");
            List<IResourceGroup> groups = Persistors.resourcegroup.getAllCollection();
            for (IResourceGroup g : groups)
            {
                groupTag.addParent(g);
            }
            groupTag.doPrePersist();
            session.save(groupTag);
            session.flush();
        }
    }

    private static class InitTagCategories implements IHibernateTransaction
    {
        // private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            // create Tags
            ITag tag_1 = new TagH("Tag 1");
            ITag tag_1_0 = new TagH("Tag 1_0");
            ITag tag_1_1 = new TagH("Tag 1_1");
            ITag tag_2 = new TagH("Tag 2");
            ITag tag_3 = new TagH("Tag 3");
            tag_1.doPrePersist();
            Persistors.tag.persist(tag_1);
            tag_1.doPostPersist();
            tag_1_0.doPrePersist();
            Persistors.tag.persist(tag_1_0);
            tag_1_0.doPostPersist();
            tag_1_1.doPrePersist();
            Persistors.tag.persist(tag_1_1);
            tag_1_1.doPostPersist();

            tag_2.doPrePersist();
            Persistors.tag.persist(tag_2);
            tag_2.doPostPersist();
            tag_3.doPrePersist();
            Persistors.tag.persist(tag_3);
            tag_3.doPostPersist();

            // create Tag Categories
            List<ITag> tagCategory_1_Tags = new ArrayList<ITag>(2);
            tagCategory_1_Tags.add(tag_1);
            tagCategory_1_Tags.add(tag_1_0);

            List<ITag> tagCategory_1_1_Tags = new ArrayList<ITag>(1);
            tagCategory_1_1_Tags.add(tag_1_1);

            List<ITag> tagCategory_2_Tags = new ArrayList<ITag>(1);
            tagCategory_2_Tags.add(tag_2);
            List<ITag> tagCategory_3_Tags = new ArrayList<ITag>(1);
            tagCategory_3_Tags.add(tag_3);

            ITagCategory tagCategory_1 = new TagCategoryH("MAIN TAG CATEGORY 1", null, tagCategory_1_Tags);
            ITagCategory tagCategory_2 = new TagCategoryH("MAIN TAG CATEGORY 2", null, tagCategory_2_Tags);
            ITagCategory tagCategory_3 = new TagCategoryH("MAIN TAG CATEGORY 3", null, tagCategory_3_Tags);

            tagCategory_1.doPrePersist();
            Persistors.tagcategory.persist(tagCategory_1);
            tagCategory_1.doPostPersist();

            tagCategory_2.doPrePersist();
            Persistors.tagcategory.persist(tagCategory_2);
            tagCategory_2.doPostPersist();

            tagCategory_3.doPrePersist();
            Persistors.tagcategory.persist(tagCategory_3);
            tagCategory_3.doPostPersist();

            ITagCategory tagCategory_1_1 = new TagCategoryH("SUB TAG CATEGORY 1-1", tagCategory_1, tagCategory_1_1_Tags);
            ITagCategory tagCategory_1_2 = new TagCategoryH("SUB TAG CATEGORY 1-2", tagCategory_1, null);

            tagCategory_1_1.doPrePersist();
            Persistors.tagcategory.persist(tagCategory_1_1);
            tagCategory_1_1.doPostPersist();

            tagCategory_1_2.doPrePersist();
            Persistors.tagcategory.persist(tagCategory_1_2);
            tagCategory_1_2.doPostPersist();

            // this.session = session;
            // ///////////
            // project A
            // ///////////
            // final IProject projectA = PersistedObjectProviders.project.getByValue("name", "ProjectA");
            // final IProject projectB = PersistedObjectProviders.project.getByValue("name", "ProjectB");
            //
            // // one tag attached to projects A et B
            // ITag tag = new TagHibernateImpl("Generic Project tag");
            // tag.addParent(projectA);
            // tag.addParent(projectB);
            // tag.doPrePersist();
            // session.save(tag);
            // session.flush();
            //
            // // one tag attached to projectA
            // ITag projectATag = new TagHibernateImpl("Project A tag");
            // projectATag.addParent(projectA);
            // projectATag.doPrePersist();
            // session.save(projectATag);
            // session.flush();
            //
            // // add different tags to different constituents
            // List<IConstituent> constituents = PersistedObjectProviders.constituent.getObjectsWithValue(null, "label",
            // "const 1 label");
            //
            // ITag tag1;
            // for (int i = 0; i < constituents.size(); i++)
            // {
            // tag1 = new TagHibernateImpl("Constituent tag Name" + i);
            // tag1.addParent(constituents.get(i));
            // tag1.doPrePersist();
            // session.save(tag1);
            // session.flush();
            // }
            //
            // // create a tag for all the groups
            // ITag groupTag = new TagHibernateImpl("Generic GROUP tag");
            // List<IResourceGroup> groups = PersistedObjectProviders.resourcegroup.getAllCollection();
            // for (IResourceGroup g : groups)
            // {
            // groupTag.addParent(g);
            // }
            // groupTag.doPrePersist();
            // session.save(groupTag);
            // session.flush();
        }
    }

    private static class InitCompos implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            IShot shot1 = Persistors.shot.getObjectByValue_NoPermCheck("label", "shot 1 label");
            IShot shot2 = Persistors.shot.getObjectByValue_NoPermCheck("label", "shot 2 label");

            final IProject projectA = Persistors.project.getObjectByValue("name", "ProjectA");
            IConstituent constituent = Persistors.constituent.getObjectByValue("label", "ciel");
            ISequence action = getSequence(session, null, null, "AAACtion", "Titre", projectA.rootSequence().getId(),
                    projectA.getId());

            IComposition compo1 = new CompositionH("Compo name 1", "Compo Desc 1", constituent, shot1, action);
            constituent.addComposition(compo1);
            compo1.doPrePersist();
            session.save(compo1);
            session.flush();

            IComposition compo2 = new CompositionH("Compo name 2", "Compo Desc 2", constituent, shot2, action);
            constituent.addComposition(compo2);
            compo2.doPrePersist();
            session.save(compo2);
            session.flush();

            constituent.doPreUpdate();
            session.update(constituent);
            session.flush();
        }
    }

    private static class InitDynMetaData implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            // Project A
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");
            String projectAId = String.valueOf(projectA.getId());
            // Constituents
            Iterator<ICategory> it = projectA.getCategories().iterator();
            ICategory decor = null;
            while (it.hasNext())
            {
                ICategory cat = it.next();
                if (cat.getName().equals("Decor"))
                {
                    decor = cat;
                }
            }
            // Constituents
            IConstituent constituent1 = null;
            if (decor != null)
            {
                constituent1 = getConstituent(session, null, decor.getId(), decor.getName() + "_1", 0,
                        "constituent 1 description...", 1, "hook_1", "const 1 label", 0, null);
            }

            /*-------------------------
             * create DynMetaDataTypes
             -------------------------*/
            IDynMetaDataType typeSimple = new DynMetaDataTypeH("boolean", Const.DYNMETADATATYPE_SIMPLE,
                    Const.JAVA_LANG_BOOLEAN, "false");
            typeSimple.doPrePersist();
            session.save(typeSimple);
            session.flush();

            IDynMetaDataType typeSimpleString = new DynMetaDataTypeH("string", Const.DYNMETADATATYPE_SIMPLE,
                    Const.JAVA_LANG_STRING, null);
            typeSimpleString.doPrePersist();
            session.save(typeSimpleString);
            session.flush();

            IDynMetaDataType type0 = new DynMetaDataTypeH("approvalType", Const.DYNMETADATATYPE_SIMPLE,
                    Const.JAVA_LANG_STRING, "TF1");
            type0.doPrePersist();
            session.save(type0);
            session.flush();

            /* DynMetaDataType of type List */
            IListValues listValues = new ListValuesH(null, "1;10;20;42");
            listValues.doPrePersist();
            session.save(listValues);
            session.flush();

            IDynMetaDataType typeList = new DynMetaDataTypeH("List of numbers", Const.JAVA_UTIL_LIST,
                    Const.JAVA_LANG_LONG, "1");
            typeList.setStructName(Entity.LISTVALUES);
            typeList.setStructId(listValues.getId());
            typeList.doPrePersist();
            session.save(typeList);
            session.flush();

            // create DynMetaDataType with type=ApprovalNote
            final IPerson gch = getPerson(session, "gch", FDTAuthUtils.encrypt("gchpass"), "Guillaume", "Chatelet");

            IApprovalNote note = new ApprovalNoteH(new Date(), "OK", "review comment", gch, projectA.getId(), projectA
                    .entityName(), "Réalisateur", null, "http://blabla");
            note.doPrePersist();
            session.save(note);
            session.flush();

            IApprovalNote note1 = new ApprovalNoteH(new Date(), "OK", "review comment blabla", gch, projectA.getId(),
                    projectA.entityName(), "Réalisateur", null, "http://blabla");
            note1.doPrePersist();
            session.save(note1);
            session.flush();

            IApprovalNote note2 = new ApprovalNoteH(new Date(), "OK", "review comment bouh", gch, projectA.getId(),
                    projectA.entityName(), "TF1", null, "http://blabla");
            note2.doPrePersist();
            session.save(note2);
            session.flush();

            IApprovalNote note3 = new ApprovalNoteH(new Date(), "OK", "review comment bouh", gch, projectA.getId(),
                    projectA.entityName(), "BFM", null, "http://blabla");
            note3.doPrePersist();
            session.save(note3);
            session.flush();

            IDynMetaDataType typeApprovalNote = new DynMetaDataTypeH("approval note", Const.DYNMETADATATYPE_SIMPLE,
                    "ApprovalNote", String.valueOf(note.getId()));
            typeApprovalNote.doPrePersist();
            session.save(typeApprovalNote);
            session.flush();

            // DynMetaDataType with type=ApprovalNoteByTypeTransformer
            IDynMetaDataType typeItemTransformer = new DynMetaDataTypeH("approval note filter",
                    Const.DYNMETADATATYPE_SIMPLE,
                    "fr.hd3d.services.resources.transformer.ApprovalNoteByTypeTransformer", "TF1");
            typeItemTransformer.doPrePersist();
            session.save(typeItemTransformer);
            session.flush();

            // =======================================
            // create classDynMetaDataTypes
            // =======================================
            // All constituents with description=.... will have a dynmetadatavalue associated
            String predicate = "description='New constituent Description 日本'";
            IClassDynMetaDataType class_Type = new ClassDynMetaDataTypeH("name 1", typeSimple, predicate,
                    "Constituent", constituent1 == null ? null : String.valueOf(constituent1.getId()));
            class_Type.doPrePersist();
            session.save(class_Type);
            session.flush();
            class_Type.doPostPersist();
            session.flush();

            String predicate0 = "description='XXX'";
            IClassDynMetaDataType class_Type0 = new ClassDynMetaDataTypeH("name 2", typeSimple, predicate0,
                    "Constituent", String.valueOf(constituent1.getId()));
            class_Type0.doPrePersist();
            session.save(class_Type0);
            session.flush();
            class_Type0.doPostPersist();
            session.flush();

            String predicate1 = "*";
            IClassDynMetaDataType class_Type1 = new ClassDynMetaDataTypeH("name 3", typeSimple, predicate1, "Project",
                    projectAId);
            class_Type1.doPrePersist();
            session.save(class_Type1);
            session.flush();
            class_Type1.doPostPersist();
            session.flush();

            String predicate1_1 = "*";
            IClassDynMetaDataType class_Type1_1 = new ClassDynMetaDataTypeH("List of numbers", typeList, predicate1_1,
                    "Project", projectAId);
            class_Type1_1.doPrePersist();
            session.save(class_Type1_1);
            session.flush();
            class_Type1_1.doPostPersist();
            session.flush();

            String predicate2 = "label='const 1 label'";
            IClassDynMetaDataType class_Type2 = new ClassDynMetaDataTypeH("name 4", typeSimple, predicate2,
                    "Constituent", String.valueOf(constituent1.getId()));
            class_Type2.doPrePersist();
            session.save(class_Type2);
            session.flush();
            class_Type2.doPostPersist();// NESTED TRANSACTION

            String predicate3 = "*";
            IClassDynMetaDataType class_Type3 = new ClassDynMetaDataTypeH("name 5", typeApprovalNote, predicate3,
                    "Project", String.valueOf(note.getId()));
            class_Type3.doPrePersist();
            session.save(class_Type3);
            session.flush();
            class_Type3.doPostPersist();
            session.flush();

            String predicate4_0 = "*";
            IClassDynMetaDataType class_Type4_0 = new ClassDynMetaDataTypeH("approvalType", type0, predicate4_0,
                    "Project", null);
            class_Type4_0.doPrePersist();
            session.save(class_Type4_0);
            session.flush();
            class_Type4_0.doPostPersist();
            session.flush();

            String predicate4 = "*";
            IClassDynMetaDataType class_Type4 = new ClassDynMetaDataTypeH("name 6", typeItemTransformer, predicate4,
                    "Project", "dyn:approvalType");
            class_Type4.doPrePersist();
            session.save(class_Type4);
            session.flush();
            class_Type4.doPostPersist();
            session.flush();

        }
    }

    private static class InitScriptExec implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
        // final ScriptH approvalExec = new ScriptH();
        // approvalExec.setTrigger("ApprovalNote.postPersist");
        // approvalExec.setName("Modeling_task_validation");
        // approvalExec.setDescription("Change status to OK on modeling task when approval note is OK");
        // approvalExec.setLocation("/home/www/virtuels/bin/depot/hd3d/scripts_reflex/ApprovalNotePostPersist.py");
        // approvalExec.setExitCode(0);
        // approvalExec.doPrePersist();
        // session.save(approvalExec);
        // session.flush();
        //
        // final ScriptH exec = new ScriptH();
        // exec.setTrigger("Constituent.postPersist");
        // exec.setName("test_script");
        // exec.setDescription("any description");
        // exec.setLocation("d:\\temp\\toto.bat");
        // exec.setExitCode(0);
        // exec.doPrePersist();
        // session.save(exec);
        // session.flush();
        //
        // final ScriptH exec1 = new ScriptH();
        // exec1.setTrigger("Constituent.postPersist");
        // exec1.setName("test_script_1");
        // exec1.setDescription("any description");
        // exec1.setLocation("D:\\temp\\tutu.bat");
        // exec1.setExitCode(0);
        // exec1.doPrePersist();
        // session.save(exec1);
        // session.flush();

        // final ScriptH execPause = new ScriptH();
        // execPause.setTrigger("Constituent.postPersist");
        // execPause.setName("test_script_Pause");
        // execPause.setDescription("fait une pause");
        // execPause.setLocation("D:\\temp\\pause.bat");
        // execPause.setExitCode(0);
        // execPause.doPrePersist();
        // session.save(execPause);
        // session.flush();
        }
    }

    private static class InitAssets implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            final IPerson gch = getPerson(session, "gch", FDTAuthUtils.encrypt("gchpass"), "Guillaume", "Chatelet");
            final IPerson eth = getPerson(session, "eth", FDTAuthUtils.encrypt("ethpass"), "Thomas", "Eskénazi");
            final IProject projectA = Persistors.project.getObjectByValue_NoPermCheck("name", "ProjectA");

            if (projectA == null)
                return;

            Set<ICategory> categories = projectA.getCategories();

            if (categories == null || categories.isEmpty())
                return;

            Iterator<ICategory> it = categories.iterator();
            ICategory animaux = null;
            while (it.hasNext())
            {
                ICategory cat = it.next();
                if (cat.getName().equals("Animaux"))
                    animaux = cat;
            }

            // AssetRevisionGroups
            final AssetRevisionGroupH group1 = new AssetRevisionGroupH();
            group1.setProject(projectA);
            group1.setName("GROUP1");
            group1.setCriteria("Category");
            group1.setValue(String.valueOf(animaux.getId()));
            group1.doPrePersist();
            session.save(group1);
            session.flush();

            final AssetRevisionGroupH group2 = new AssetRevisionGroupH();
            group2.setProject(projectA);
            group2.setName("GROUP2");
            group2.doPrePersist();
            session.save(group2);
            session.flush();

            final AssetRevisionGroupH group3 = new AssetRevisionGroupH();
            group3.setProject(projectA);
            group3.setName("GROUP3");
            group3.doPrePersist();
            session.save(group3);
            session.flush();

            final AssetRevisionGroupH emptygroup = new AssetRevisionGroupH();
            emptygroup.setName("some_group");
            emptygroup.doPrePersist();
            session.save(emptygroup);
            session.flush();

            // AssetRevisions
            ITaskType myTaskType = getTaskType(session, "myTaskType", "#99FE80", "The myTaskType");

            List<IAssetRevision> bsn1_high = createAssetWithRevisions("BSN1", "high", "Build scene 1", myTaskType, 4,
                    projectA, group1, eth, session);

            List<IAssetRevision> rnd1_high = createAssetWithRevisions("RND1", "high", "SC001_RND_CHARS", myTaskType, 2,
                    projectA, group1, eth, session);

            List<IAssetRevision> rnd1_low = createAssetWithRevisions("RND1", "low", "SC001_RND_CHARS", myTaskType, 5,
                    projectA, group2, gch, session);

            List<IAssetRevision> tmod_ = createAssetWithRevisions("TMOD", null, "S001_t_model", myTaskType, 3,
                    projectA, group2, eth, session);

            List<IAssetRevision> tanim = createAssetWithRevisions("TANIM", null, "S001_t_anim", myTaskType, 1,
                    projectA, group2, gch, session);

            // AssetLinks
            linkAssetRevisions(bsn1_high.get(2), rnd1_high.get(0), session);
            linkAssetRevisions(bsn1_high.get(3), rnd1_high.get(1), session);

            linkAssetRevisions(bsn1_high.get(0), rnd1_low.get(0), session);
            linkAssetRevisions(bsn1_high.get(1), rnd1_low.get(1), session);
            linkAssetRevisions(bsn1_high.get(2), rnd1_low.get(2), session);
            linkAssetRevisions(bsn1_high.get(3), rnd1_low.get(3), session);
            linkAssetRevisions(bsn1_high.get(3), rnd1_low.get(4), session);

            linkAssetRevisions(tmod_.get(2), tanim.get(0), session);
            // final AssetRevisionLinkHibernateImpl assetlink = new AssetRevisionLinkHibernateImpl();
            // assetlink.setInputKey(asset.getKey());
            // assetlink.setOutputKey(asset_1.getKey());
            // assetlink.setInputVariation(asset.getVariation());
            // assetlink.setOutputVariation(asset_1.getVariation());
            // assetlink.setInputRevision(1);
            // assetlink.setOutputRevision(2);
            // assetlink.setType("dependancy");
            // assetlink.doPrePersist();
            // session.save(assetlink);
            // session.flush();
            //
            // final AssetRevisionLinkHibernateImpl assetlink_1 = new AssetRevisionLinkHibernateImpl();
            // assetlink_1.setInputKey(asset.getKey());
            // assetlink_1.setOutputKey(asset_2.getKey());
            // assetlink_1.setInputVariation(asset.getVariation());
            // assetlink_1.setOutputVariation(asset_2.getVariation());
            // assetlink_1.setInputRevision(1);
            // assetlink_1.setOutputRevision(2);
            // assetlink_1.setType("dependancy");
            // assetlink_1.doPrePersist();
            // session.save(assetlink_1);
            // session.flush();

            /* Filerevisions */
            MountPointH mountPoint = new MountPointH("V", "Linux", "/test/");
            session.save(mountPoint);
            session.flush();

            FileRevisionH file = new FileRevisionH(new Date(), "SCE_table", "low", 1, gch, "ma", 200L,
                    "d41d8cd98f00b204e9800998ecf8427e", "//server_a/repo/", "file1_001.ma", EFileStatus.COMPRESSED,
                    new Date(), eth, "no comment", "prod", null, EFileState.getDefault(), mountPoint.getName());
            file.doPrePersist();
            session.save(file);
            file.doPostPersist();
            session.flush();
            /* same file with higher revision */
            FileRevisionH file_0 = new FileRevisionH(new Date(), "SCE_table", "low", 2, gch, "ma", 200L,
                    "d41d8cd98f00b204e9800998ecf8427e", "//server_a/repo/", "file1_001.ma", EFileStatus.COMPRESSED,
                    new Date(), eth, "no comment", "prod", null, EFileState.getDefault(), mountPoint.getName());
            file_0.doPrePersist();
            session.save(file_0);
            file_0.doPostPersist();
            session.flush();

            // annex test: add a tag attached to a FileRevision
            ITag tag = new TagH("FileRevision Tag");
            tag.addParent(file);
            tag.doPostPersist();
            session.save(tag);
            tag.doPostPersist();
            session.flush();

            final FileRevisionH file_1 = new FileRevisionH(new Date(), "TEX_bois", "default", 1, gch, "ma", 200L,
                    "d41d8cd98f00b204e9800998ecf8427e", "//server_a/repo/", "file1_001.ma", EFileStatus.COMPRESSED,
                    new Date(), eth, "no comment", "prod", null, EFileState.getDefault(), mountPoint.getName());
            file_1.doPrePersist();
            session.save(file_1);
            file_1.doPostPersist();
            session.flush();

            /* FileRevisionLink */
            final FileRevisionLinkH fileRevisionLink_1 = new FileRevisionLinkH(file, file_0, "TOTO");
            fileRevisionLink_1.doPrePersist();
            session.save(fileRevisionLink_1);
            fileRevisionLink_1.doPostPersist();
            session.flush();

            final FileRevisionLinkH fileRevisionLink_2 = new FileRevisionLinkH(file, file_1, "TOTO");
            fileRevisionLink_2.doPrePersist();
            session.save(fileRevisionLink_2);
            fileRevisionLink_2.doPostPersist();
            session.flush();

            // Files attributes
            final FileAttributeH fileattribute = new FileAttributeH(file, "colorspace", "0");
            final FileAttributeH fileattribute_1 = new FileAttributeH(file, "definition", "crap");
            fileattribute.doPrePersist();
            session.save(fileattribute);
            fileattribute.doPostPersist();
            fileattribute_1.doPrePersist();
            session.save(fileattribute_1);
            fileattribute_1.doPostPersist();
            session.flush();

            final FileAttributeH fileattribute_2 = new FileAttributeH(file_1, "definition", "better");
            fileattribute_2.doPrePersist();
            session.save(fileattribute_2);
            fileattribute_2.doPostPersist();
            session.flush();

            // // AssetRevisionFileRevisions
            // AssetRevisionFileRevisionH assetRevFileRev = new AssetRevisionFileRevisionH();
            // assetRevFileRev.setAssetKey(bsn1_high.get(0).getKey());
            // assetRevFileRev.setAssetVariation(bsn1_high.get(0).getVariation());
            // assetRevFileRev.setAssetRevision(bsn1_high.get(0).getRevision());
            // assetRevFileRev.setFileKey(file.getKey());
            // assetRevFileRev.setFileVariation(file.getVariation());
            // assetRevFileRev.setFileRevision(file.getRevision());
            // assetRevFileRev.doPrePersist();
            // session.save(assetRevFileRev);
            // assetRevFileRev.doPostPersist();
            // session.flush();
            file.setAssetRevision(bsn1_high.get(0));
            session.save(file);
            session.flush();

            // AssetRevisionFileRevisionH assetRevFileRev_1 = new AssetRevisionFileRevisionH();
            // assetRevFileRev_1.setAssetKey(rnd1_high.get(0).getKey());
            // assetRevFileRev_1.setAssetVariation(rnd1_high.get(0).getVariation());
            // assetRevFileRev_1.setAssetRevision(rnd1_high.get(0).getRevision());
            // assetRevFileRev_1.setFileKey(file_1.getKey());
            // assetRevFileRev_1.setFileVariation(file_1.getVariation());
            // assetRevFileRev_1.setFileRevision(file_1.getRevision());
            // assetRevFileRev_1.doPrePersist();
            // session.save(assetRevFileRev_1);
            // assetRevFileRev_1.doPostPersist();
            // session.flush();
            file_1.setAssetRevision(rnd1_high.get(0));
            session.save(file_1);
            session.flush();

        }
    }

    private static class InitEntityTaskLinks implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            ITask task = Persistors.task.getObjectByValue_NoPermCheck("name", "Test_Task_00");
            ISequence sequence = Persistors.sequence.getObjectByValue_NoPermCheck("name", "Pilote");
            IEntityTaskLink link = new EntityTaskLinkH(sequence.getId(), sequence.entityName(), task);
            session.save(link);
            session.flush();
        }
    }

    private static class InitSteps implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            IConstituent constituent = getConstituent(session, null, null, null, 0, "constituent 1 description...", 1,
                    "hook_1", "const 1 label", 0, null);

            ITaskType design = getTaskType(session, "Design", "#99FE80", "The Design Task Type");

            if (constituent != null)
            {
                IStep step = new StepH((IBase) constituent, design, Boolean.FALSE, 10);
                step.doPrePersist();
                Persistors.step.persist(step);
                step.doPostPersist();
            }
        }
    }

    private static class InitAudits implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            Calendar cal = new GregorianCalendar(2009, 01, 01);
            Calendar cal_1 = new GregorianCalendar(2009, 02, 01);
            Calendar cal_2 = new GregorianCalendar(2010, 01, 01);
            Audit audit = new Audit(cal.getTimeInMillis(), null, null, null, null, null);
            Audit audit_1 = new Audit(cal_1.getTimeInMillis(), null, null, null, null, null);
            Audit audit_2 = new Audit(cal_2.getTimeInMillis(), null, null, null, null, null);

            session.save(audit);
            session.save(audit_1);
            session.save(audit_2);
            session.flush();
        }
    }

    private static class InitSettings implements IHibernateTransaction
    {
        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            final IPerson gch = getPerson(session, "gch", "Guillaume", "Chatelet");
            final IPerson eth = getPerson(session, "eth", "Thomas", "Eskénazi");

            Setting setting1 = new Setting(gch.getLogin(), gch.getId(), "explorer.column.toto.length", "20");
            Setting setting2 = new Setting(eth.getLogin(), eth.getId(), "explorer.column.titi.length", "30");

            session.save(setting1);
            session.save(setting2);
            session.flush();
        }
    }

    private static class InitBasicLoadTest implements IHibernateTransaction
    {
        private static final int WORKING_DAY_IN_MILLIS = 24 * 60 * 60 * 1000;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            // create a bunch of Persons
            final int NB_PERSONS = 200;
            List<IPerson> persons = new ArrayList<IPerson>(NB_PERSONS);
            for (int i = 0; i < NB_PERSONS; i++)
            {
                persons.add(getPerson(session, "p" + i, "firstName_" + i, "lastName_" + i));
            }

            // Project
            final IProject projectA = getProject(session, "ProjectA", EProjectStatus.OPEN, "#FF0000", null);

            // ResourceGroup
            final IResourceGroup group = getResourceGroup(session, "Test_group_00");
            for (IPerson p : persons)
            {
                group.addPerson(p);
            }

            Set<IProject> projects = new HashSet<IProject>(1);
            projects.add(projectA);
            group.setProjects(projects);
            group.doPreUpdate();
            session.update(group);
            session.flush();

            // Roles
            IRole role = getRole(session, "Test_role_00");
            IResourceGroup group2 = getResourceGroup(session, "Test_group_00");
            HashSet<IResourceGroup> groups = new HashSet<IResourceGroup>();
            HashSet<String> perms = new HashSet<String>();
            groups.add(group2);
            role.setResourceGroups(groups);
            perms.add("v1:*");
            role.setPermissions(perms);
            role.doPreUpdate();
            session.update(role);
            session.flush();

            // Time
            // int sec = 60000;
            List<IPersonDay> personDays = new ArrayList<IPersonDay>(NB_PERSONS);
            for (IPerson p : persons)
            {
                personDays.add(getPersonDay(session, new Date(0), p));
            }

            // Refresh data
            session.flush();

            // Unplanned Activities
            for (int i = 0; i < NB_PERSONS; i++)
            {
                getSimpleActivity(session, ESimpleActivityType.MEETING, personDays.get(i), 2L, "A simple task 1 " + i,
                        persons.get(i), projectA);
                getSimpleActivity(session, ESimpleActivityType.MEETING, personDays.get(i), 2L, "A simple task 2 " + i,
                        persons.get(i), projectA);
                getSimpleActivity(session, ESimpleActivityType.OTHER, personDays.get(i), 2L, "A simple task 3 " + i,
                        persons.get(i), projectA);
                getSimpleActivity(session, ESimpleActivityType.TECHNICAL_INCIDENT, personDays.get(i), 2L,
                        "A simple task 4 " + i, persons.get(i), projectA);
                getSimpleActivity(session, ESimpleActivityType.TECHNICAL_INCIDENT, personDays.get(i), 2L,
                        "A simple task 5 " + i, persons.get(i), projectA);
                session.flush();
                System.out.println("Simple activities created for person " + persons.get(i));
            }
            // Tasks & TaskGroup & TaskChanges
            Date startDate = new Date();
            Date endDate = null;
            Calendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(startDate.getTime() + (3 * WORKING_DAY_IN_MILLIS));
            endDate = calendar.getTime();

            Long duration = (endDate.getTime() - startDate.getTime()) / 1000L;// in seconds
            ITaskType design = getTaskType(session, "Design", "#99FE80", "The Design Task Type");

            for (int i = 0; i < NB_PERSONS; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    ITask taskTest = getTask(session, projectA, "Test_Task_" + i + "_" + j,
                            ETaskStatus.WORK_IN_PROGRESS, persons.get(i), persons.get(i), startDate, endDate, duration,
                            null, startDate, endDate, null, null, null, design);
                    calendar.setTimeInMillis(startDate.getTime() - (3 * WORKING_DAY_IN_MILLIS));
                    startDate = calendar.getTime();
                    calendar.setTimeInMillis(startDate.getTime() + (7 * WORKING_DAY_IN_MILLIS));
                    endDate = calendar.getTime();
                    duration = (endDate.getTime() - startDate.getTime()) / 1000L;// in seconds
                    getTaskActivity(session, personDays.get(i), 5L, "Did a few things", taskTest, persons.get(i));
                    session.flush();
                }
                System.out.println("Tasks created for person " + persons.get(i));
            }
            // Commit
            session.flush();
        }
    }

    /*
     * ================================================
     * 
     * Convenience common methods
     * 
     * ================================================
     */
    private static ITaskType getTaskType(Session session, String name, String color, String description)
            throws Hd3dPersistenceException
    {
        return getTaskType(session, name, color, description, null);
    }

    private static ITaskType getTaskType(Session session, String name, String color, String description,
            IProject project) throws Hd3dPersistenceException
    {
        final Class<?> persistedClass = Persistors.tasktype.getPersistedClass();
        ITaskType taskType = null;
        Criteria query = session.createCriteria(persistedClass).add(Restrictions.eq("name", name));
        if (StringUtils.isNotBlank(color))
            query.add(Restrictions.eq("color", color));
        if (project != null)
            query.add(Restrictions.eq("project", project));
        if (StringUtils.isNotBlank(description))
            query.add(Restrictions.eq("description", description));

        taskType = (ITaskType) query.uniqueResult();

        if (taskType != null)
        {
            return taskType;
        }
        try
        {
            taskType = new TaskTypeH(null, null, project, color, name, description);

            taskType.doPrePersist();
            session.save(taskType);
            session.flush();
            return taskType;
        }
        catch (Exception e)
        {
            throw new Hd3dPersistenceException(e);
        }
    }

    private static ITaskGroup getTaskGroup(Session session, String color, String name, ITaskGroup parentTaskGroup,
            IPlanning planning, Date startDate, Date endDate) throws Hd3dPersistenceException
    {
        final Class<?> persistedClass = Persistors.taskGroup.getPersistedClass();
        ITaskGroup taskGroup = (ITaskGroup) session.createCriteria(persistedClass).add(Restrictions.eq("color", color))
                .add(Restrictions.eq("name", name)).add(Restrictions.eq("taskGroup", parentTaskGroup)).add(
                        Restrictions.eq("planning", planning)).add(Restrictions.eq("startDate", startDate)).add(
                        Restrictions.eq("endDate", endDate)).uniqueResult();
        if (taskGroup != null)
        {
            return taskGroup;
        }
        try
        {
            taskGroup = new TaskGroupH(null, null, color, endDate, startDate, name, parentTaskGroup, planning, null,
                    null);
            taskGroup.doPrePersist();
            session.save(taskGroup);
            session.flush();
            return taskGroup;
        }
        catch (Exception e)
        {
            throw new Hd3dPersistenceException(e);
        }
    }

    private static ITaskChanges getTaskChanges(Session session, IPerson worker, Date startDate, Date endDate,
            Long duration, IPlanning planning, ITask task, ITaskGroup taskGroup) throws Hd3dPersistenceException,
            Hd3dTranslationException
    {
        final Class<?> persistedClass = Persistors.taskChanges.getPersistedClass();
        ITaskChanges taskChanges = (ITaskChanges) session.createCriteria(persistedClass).add(
                Restrictions.eq("startDate", startDate)).add(Restrictions.eq("endDate", endDate)).add(
                Restrictions.eq("worker", worker)).add(Restrictions.eq("duration", duration)).add(
                Restrictions.eq("planning", planning)).add(Restrictions.eq("task", task)).uniqueResult();
        if (taskChanges != null)
        {
            return taskChanges;
        }
        try
        {
            taskChanges = new TaskChangesH(worker, startDate, endDate, duration, taskGroup, planning, task);
            taskChanges.doPrePersist();
            session.save(taskChanges);
            session.flush();
            return taskChanges;
        }
        catch (Exception e)
        {
            throw new Hd3dPersistenceException(e);
        }
    }

    private static IPlanning getPlanning(Session session, IProject project, String name)
            throws Hd3dPersistenceException
    {
        final Class<?> persistedClass = Persistors.planning.getPersistedClass();
        IPlanning planning = (IPlanning) session.createCriteria(persistedClass).add(Restrictions.eq("name", name)).add(
                Restrictions.eq("project", project)).uniqueResult();
        if (planning != null)
        {
            return planning;
        }
        try
        {
            Calendar now = Calendar.getInstance();
            Date endDate = now.getTime();
            now.add(Calendar.YEAR, -1);
            Date startDate = now.getTime();
            planning = new PlanningH(null, null, name, project, startDate, endDate);
            planning.doPrePersist();
            session.save(planning);
            session.flush();
            return planning;
        }
        catch (Exception e)
        {
            throw new Hd3dPersistenceException(e);
        }
    }

    /**
     * Build a new row in activity table with the parameters given. If the activity already exists in the database, it
     * is returned.
     * 
     * @param type
     *            Activity type
     * @param personDay
     *            Day associated to activity
     * @param duration
     *            Activity duration
     * @param comment
     *            Activity comment
     * @param worker
     *            Worker associated to activity
     * @param project
     *            Project associated to activity
     * @return The simple activity object inserted or found in activity table.
     * @throws Hd3dTranslationException
     *             When error occurs
     */
    private static ISimpleActivity getSimpleActivity(Session session, ESimpleActivityType type, IPersonDay personDay,
            Long duration, String comment, IPerson worker, IProject project) throws Hd3dTranslationException
    {

        // Get Activity class
        final Class<?> persistedClass = Persistors.simpleActivity.getPersistedClass();

        // Try to retrieve object with the constraints given.
        ISimpleActivity object = (ISimpleActivity) session.createCriteria(persistedClass).add(
                Restrictions.eq("type", type)).add(Restrictions.eq("comment", comment)).add(
                Restrictions.eq("duration", duration)).uniqueResult();

        // If database sends something, object is returned
        if (object != null)
            return object;

        // Else a new row is created in the database
        try
        {
            object = new SimpleActivityH(personDay, duration, comment, type, project, worker, worker, personDay
                    .getDate());
            object.doPrePersist();
            session.save(object);
            session.flush();
            return object;
        }
        catch (Exception e)
        {
            throw new Hd3dPersistenceException(e);
        }
    }

    /**
     * Create a new task activity in Activity Table. If task activity already exists, no task is created.
     * 
     * @param personDay
     *            Day associated to task activity
     * @param duration
     *            Task activity duration
     * @param comment
     *            Task activity comment
     * @param task
     *            Task associated to task activity
     * @param filledBy
     *            Person who filled the activity
     * @return Task activity newly created or task activity in table if it already exists.
     * @throws Hd3dTranslationException
     */
    private static ITaskActivity getTaskActivity(Session session, IPersonDay personDay, Long duration, String comment,
            ITask task, IPerson filledBy) throws Hd3dTranslationException
    {

        // Retrieve task activity.
        final Class<?> persistedClass = Persistors.taskActivity.getPersistedClass();
        ITaskActivity object = (ITaskActivity) session.createCriteria(persistedClass).add(
                Restrictions.eq("comment", comment)).add(Restrictions.eq("duration", duration)).add(
                Restrictions.eq("task", task)).uniqueResult();

        // If task activity is in database, return task activity.
        if (object != null)
            return object;

        // Else Save task activity in database.
        try
        {
            object = new TaskActivityH(task, personDay, duration, comment, filledBy, personDay.getDate());
            object.doPrePersist();
            session.save(object);
            session.flush();
            return object;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new Hd3dPersistenceException(e);
        }
    }

    /**
     * Create a new task in Person Table. If task already exists, no task is created.
     * 
     * @param project
     *            Project associated to task
     * @param name
     *            Task name
     * @param status
     *            Task status
     * @param creator
     *            Task creator
     * @param worker
     *            Worker associated to task
     * @return Task newly created or task in table if it already exists.
     * @throws Hd3dPersistenceException
     * @throws Hd3dTranslationException
     */
    private static ITask getTask(Session session, IProject project, String name, ETaskStatus status, IPerson creator,
            IPerson worker, Date startDate, Date endDate, Long duration, Date deadLine, Date actualStartDate,
            Date actualEndDate, Boolean startable, Boolean confirmed, Boolean startup, ITaskType taskType)
            throws Hd3dPersistenceException, Hd3dTranslationException
    {

        // Retrieve task
        final Class<?> persistedClass = Persistors.task.getPersistedClass();
        ITask object = (ITask) session.createCriteria(persistedClass).add(Restrictions.eq("name", name)).uniqueResult();

        // If task is in database, return task.
        if (object != null)
            return object;

        // Else Save task in database.
        try
        {
            object = new TaskH(name, NumberUtils.BYTE_ZERO, null, status, project, creator, worker, startDate, endDate,
                    duration, deadLine, actualStartDate, actualEndDate, startable, confirmed, startup, null, taskType);
            object.doPrePersist();
            session.save(object);
            session.flush();
            return object;

        }
        catch (Exception e)
        {
            // e.printStackTrace();
            throw new Hd3dPersistenceException(e);
        }
    }

    /**
     * Create a new person in Person Table. If task already exists, no person is created.
     * 
     * @param login
     *            Person login
     * @param password
     *            Person Password
     * @param first
     *            Person first name
     * @param last
     *            Person last name
     * @return Person newly created or person in table if it already exists.
     * @throws Hd3dTranslationException
     * @throws Hd3dPersistenceException
     *             When database error occurs
     */
    // private IPerson getPerson(String login, String first, String last) throws Hd3dTranslationException,
    // Hd3dPersistenceException
    // {
    // // Retrieve Person.
    // IPerson object = (IPerson) session.createCriteria(PersistedObjectProviders.person.getPersistedClass()).add(
    // Restrictions.eq("login", login)).uniqueResult();
    //
    // // If person is in database, return person.
    // if (object != null)
    // {
    // return object;
    // }
    //
    // // Else create a new person instance.
    // object = Translators.person.fromLightweight(LPerson.getNewInstance(login, first, last, "null", "null",
    // "null", 0L), null);
    //
    // /*
    // * sets up an user account for that person in order to avoid people's complains about formerly used yet not
    // * working (eth, ethpass)
    // */
    // IUserAccount account = getAccount(login);
    // account.setSecret(FDTAuthUtils.encrypt(login + "pass"));
    // session.save(account);
    //
    // // Save person in database.
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // return object;
    // }
    /**
     * @param date
     * @param person
     * @return
     * @throws Hd3dTranslationException
     * @throws Hd3dPersistenceException
     */
    private static IPersonDay getPersonDay(Session session, Date date, IPerson person) throws Hd3dException
    {
        IPersonDay personDay = new PersonDayH(date, person);

        personDay.doPrePersist();
        session.save(personDay);
        session.flush();

        return personDay;
    }

    /**
     * Create a new project in Project Table. If project already exists, no project is created.
     * 
     * @param name
     *            Project name
     * @param status
     *            Project status
     * @param color
     *            Project color
     * @return Project newly created or project in table if it already exists.
     * @throws Hd3dTranslationException
     * 
     * @throws Hd3dPersistenceException
     *             When database error occurs
     */
    private static IProject getProject(Session session, String name, EProjectStatus status, String color, String hook)
            throws Hd3dException
    {

        // Retrieve project.
        IProject object = (IProject) session.createCriteria(Persistors.project.getPersistedClass()).add(
                Restrictions.eq("name", name)).uniqueResult();

        // If project is in database, return project.
        if (object != null)
        {
            return object;
        }

        // Else create a new project instance.
        object = Translators.project.fromLightweight(LProject.getNewInstance(name, status.toString(), color, hook),
                null);

        // Save project in database.
        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    /**
     * Associate a Persons to a project (Many-to-many relationship).
     * 
     * @param project
     *            Project to be associated with persons
     * @param persons
     *            List of person to associate to a project
     * @throws Hd3dPersistenceException
     */
    private static void appendStaffToProject(Session session, IProject project, IPerson... persons)
            throws Hd3dException
    {

        // Build a new hash set if not initialize
        // if (project.getResourceGroups() == null)
        // project.setStaff(new HashSet<IPerson>());
        //
        // project.getResourceGroups().addAll(Arrays.asList(persons));

        project.doPreUpdate();
    }

    /**
     * Create a new group in Group Table. If group already exists, no group is created.
     * 
     * @param name
     *            Group name
     * @return Group newly created or project in table if it already exists.
     * @throws Hd3dTranslationException
     * 
     * @throws Hd3dPersistenceException
     *             When database error occurs
     */
    private static IResourceGroup getResourceGroup(Session session, String name) throws Hd3dException
    {

        // Retrieve project.
        IResourceGroup object = (IResourceGroup) session.createCriteria(Persistors.resourcegroup.getPersistedClass())
                .add(Restrictions.eq("name", name)).uniqueResult();

        // If project is in database, return project.
        if (object != null)
        {
            return object;
        }

        // Else create a new project instance.
        object = Translators.resourcegroup.fromLightweight(LResourceGroup.getNewInstance(name, null), null);

        // Save project in database.
        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    /**
     * Create a new role in Role Table. If role already exists, no role is created.
     * 
     * @param name
     *            Role name
     * @return Role newly created or role in table if it already exists.
     * @throws Hd3dTranslationException
     * 
     * @throws Hd3dPersistenceException
     *             When database error occurs
     */
    private static IRole getRole(Session session, String name) throws Hd3dException
    {

        // Retrieve role.
        IRole object = (IRole) session.createCriteria(Persistors.role.getPersistedClass()).add(
                Restrictions.eq("name", name)).uniqueResult();

        // If project is in database, return project.
        if (object != null)
        {
            return object;
        }

        // Else create a new role instance.
        object = Translators.role.fromLightweight(LRole.getNewInstance(name), null);

        // Save project in database.
        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    /**
     * Create a new user account in User_Account Table. If it already exists, nothing is created.
     * 
     * @param login
     *            a person's login
     * @return User account newly created or user account in table if it already exists.
     * @throws Hd3dTranslationException
     * @throws Hd3dPersistenceException
     *             When database error occurs
     */
    // private IUserAccount getAccount(String login) throws Hd3dTranslationException, Hd3dPersistenceException
    // {
    // // Retrieve the user account.
    // IUserAccount object = (IUserAccount) session.createCriteria(
    // PersistedObjectProviders.useraccount.getPersistedClass()).add(Restrictions.eq("principal", login))
    // .uniqueResult();
    //
    // // If the user account is in database, return it.
    // if (object != null)
    // {
    // return object;
    // }
    //
    // // Else create a new user account instance.
    // object = new UserAccountHibernateImpl(login);
    //
    // // Save user account in database.
    // object.doPrePersist();
    // session.save(object);
    // session.flush();
    // return object;
    // }
    private static IPerson getPerson(Session session, String login, String first, String last) throws Hd3dException
    {
        // Retrieve Person.
        IPerson object = (IPerson) session.createCriteria(Persistors.person.getPersistedClass()).add(
                Restrictions.eq("login", login)).uniqueResult();

        // If person is in database, return person.
        if (object != null)
        {
            return object;
        }

        // Else create a new person instance.
        object = Translators.person.fromLightweight(LPerson.getNewInstance(login, first, last, "null", "null", "null",
                NumberUtils.LONG_ZERO), null);

        /*
         * sets up an user account for that person in order to avoid people's complains about formerly used yet not
         * working (eth, ethpass)
         */
        IUserAccount account = getAccount(session, login);
        account.setSecret(FDTAuthUtils.encrypt(login + "pass"));
        session.save(account);

        // Save person in database.
        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    private static IPerson getPerson(Session session, String login, String password, String first, String last)
            throws Hd3dException
    {
        // Retrieve Person.
        IPerson object = (IPerson) session.createCriteria(Persistors.person.getPersistedClass()).add(
                Restrictions.eq("login", login)).uniqueResult();

        // If person is in database, return person.
        if (object != null)
        {
            return object;
        }

        // Else create a new person instance.
        object = Translators.person.fromLightweight(LPerson.getNewInstance(login, first, last, null, null, null,
                NumberUtils.LONG_ZERO), null);

        // Save person in database.
        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    /**
     * Create a new user account in User_Account Table. If it already exists, nothing is created.
     * 
     * @param login
     *            a person's login
     * @return User account newly created or user account in table if it already exists.
     * @throws Hd3dTranslationException
     * @throws Hd3dPersistenceException
     *             When database error occurs
     */
    private static IUserAccount getAccount(Session session, String login) throws Hd3dException
    {
        // Retrieve the user account.
        IUserAccount object = (IUserAccount) session.createCriteria(Persistors.useraccount.getPersistedClass()).add(
                Restrictions.eq("principal", login)).uniqueResult();

        // If the user account is in database, return it.
        if (object != null)
        {
            return object;
        }

        // Else create a new user account instance.
        object = new UserAccountH(login);

        // Save user account in database.
        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    private static IConstituent getConstituentById(Session session, Long id, Long categoryID, String categoryName,
            Integer completion, String description, Integer difficulty, String hook, String label, Integer trust,
            String uuid) throws Hd3dException
    {
        // "id" must be property and not column name
        IConstituent object = (IConstituent) session.createCriteria(Persistors.constituent.getPersistedClass()).add(
                Restrictions.eq("id", id)).uniqueResult();
        if (object != null)
            return object;
        object = Translators.constituent.fromLightweight(LConstituent.getNewInstance(categoryID, categoryName,
                completion, description, difficulty, hook, label, trust, true, true, true, false, false, false, null),
                null);
        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    private static IConstituent getConstituent(Session session, Long id, Long categoryID, String categoryName,
            Integer completion, String description, Integer difficulty, String hook, String label, Integer trust,
            String uuid) throws Hd3dException
    {

        IConstituent object = null;

        Criteria query = session.createCriteria(Persistors.constituent.getPersistedClass());
        if (id != null)
            query.add(Restrictions.eq("id", id));
        if (label != null)
            query.add(Restrictions.eq("label", label));
        if (hook != null)
            query.add(Restrictions.eq("hook", hook));

        object = (IConstituent) query.uniqueResult();

        if (object != null)
            return object;

        object = Translators.constituent.fromLightweight(LConstituent
                .getNewInstance(categoryID, categoryName, completion, description, difficulty, hook, label, trust,
                        false, false, false, false, false, false, null), null);
        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    private static ICategory getCategory(Session session, Long id, Long version, String name, Long parent, Long project)
            throws Hd3dException
    {

        ICategory category = (ICategory) session.createCriteria(Persistors.category.getPersistedClass()).add(
                Restrictions.or(Restrictions.eq("id", id), Restrictions.eq("name", name))).uniqueResult();
        if (category != null)
            return category;
        category = Translators.category.fromLightweight(LCategory.getNewInstance(name, parent, project), null);
        category.doPrePersist();
        session.save(category);
        session.flush();
        return category;
    }

    private static IShot getShot(Session session, Long id, Long sequenceID, Integer completion, String description,
            Integer difficulty, String hook, String label, Integer trust, String uuid) throws Hd3dException
    {

        IShot object = (IShot) session.createCriteria(Persistors.shot.getPersistedClass()).add(
                Restrictions.eq("id", id)).uniqueResult();// "id" must be property and not column name
        if (object != null)
            return object;
        object = Translators.shot.fromLightweight(LShot.getNewInstance(sequenceID, completion, description, difficulty,
                hook, label, trust, true, true, false, false, false, false, false, false, false, 0, null, 0, 0), null);
        object.doPrePersist();
        session.save(object);
        session.flush();
        return object;
    }

    private static ISequence getSequence(Session session, Long id, Long version, String name, String title,
            Long parent, Long project) throws Hd3dException
    {
        ISequence sequence = (ISequence) session.createCriteria(Persistors.sequence.getPersistedClass()).add(
                Restrictions.or(Restrictions.eq("id", id), Restrictions.eq("name", name))).uniqueResult();
        if (sequence != null)
            return sequence;

        sequence = Translators.sequence.fromLightweight(LSequence.getNewInstance(name, title, parent, project, null),
                null);
        sequence.doPrePersist();
        session.save(sequence);
        session.flush();
        return sequence;
    }

    private static List<IAssetRevision> createAssetWithRevisions(String key, String variation, String name,
            ITaskType type, int revisions, IProject project, IAssetRevisionGroup group, IPerson owner, Session session)
            throws Hd3dException
    {
        List<IAssetRevision> result = new ArrayList<IAssetRevision>();

        for (int i = 1; i <= revisions; i++)
        {
            final AssetRevisionH asset = new AssetRevisionH();
            asset.setProject(project);
            asset.setCreationDate(new Date());
            asset.setKey(key);
            asset.setVariation(variation);
            asset.setRevision(i);
            asset.setCreator(owner);
            asset.setTaskType(type);
            asset.setName(name);
            asset.setStatus(EAssetStatus.UNLOCKED);
            asset.setLastOperationDate(new Date());
            asset.setLastUser(owner);
            asset.setComment("revision " + String.valueOf(i));
            asset.setValidity("prod");
            asset.doPrePersist();
            session.save(asset);
            session.flush();

            result.add(asset);
        }

        if (group != null)
        {
            // group.addAssetRevision(result.get(0));
            // group.doPrePersist();
            session.save(group);
            session.flush();
        }

        return result;
    }

    private static IAssetRevisionLink linkAssetRevisions(IAssetRevision left, IAssetRevision right, Session session)
    {
        IAssetRevisionLink link = new AssetRevisionLinkH();

        // link.setInputKey(left.getKey());
        // link.setInputVariation(left.getVariation());
        // link.setInputRevision(left.getRevision());
        //
        // link.setOutputKey(right.getKey());
        // link.setOutputVariation(right.getVariation());
        // link.setOutputRevision(right.getRevision());
        link.setInputAsset(left);
        link.setOutputAsset(right);

        session.save(link);
        session.flush();

        return link;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

    private static String format(String pattern, String s)
    {
        return MessageFormat.format(pattern, StringUtils.removeStart(s, "/"));
    }

}
