package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PLAYLISTREVISIONGROUP_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPlayListRevisionGroupChildrenCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.translator.json.JSonFormSerializer;


/**
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des enfants du playlistrevisiongroup indiqué\r\n"
        + "\r\nPUT:Changement de nom d'un playlistrevisiongroup (ou sous-playlistrevisiongroup)\r\n"
        + "\r\nPOST:Ajout d'un playlistrevisiongroup par le nom\r\n"
        + "\r\nDELETE:Suppression d'un playlistrevisiongroup (ou sous-playlistrevisiongroup) et de tous ses fils.")
public class PlayListRevisionGroupChildrenResource extends
        AbstractHd3dCollection<ILPlayListRevisionGroup, IPlayListRevisionGroup>
{
    public PlayListRevisionGroupChildrenResource() throws Hd3dException
    {
        super(Persistors.playlistrevisiongroup, Translators.playlistrevisiongroup);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetPlayListRevisionGroupChildrenCmd(getResourceContext()).wantTotalSize());
    }

    @Override
    protected IPlayListRevisionGroup newObject(Representation entity) throws Hd3dException
    {
        ILPlayListRevisionGroup lPlayListRevisionGroup = JSonFormSerializer
                .<ILPlayListRevisionGroup> deserialize(entity);
        Long playListRevisionGroupId = getResourceContext().getIdFromUrl(PLAYLISTREVISIONGROUP_ID_ATTRIBUTE);
        lPlayListRevisionGroup.setParent(playListRevisionGroupId);
        IPlayListRevisionGroup playListRevisionGroup = getResourceContext().getTranslator().fromLightweight(
                lPlayListRevisionGroup, getResourceContext());

        return playListRevisionGroup;
    }

    @Override
    protected List<IPlayListRevisionGroup> newCollection(Representation entity) throws Hd3dException
    {
        List<ILPlayListRevisionGroup> lsequences = JSonFormSerializer.<ILPlayListRevisionGroup> deserializeList(entity);
        Long sequenceId = getResourceContext().getIdFromUrl(PLAYLISTREVISIONGROUP_ID_ATTRIBUTE);
        List<IPlayListRevisionGroup> objects = new ArrayList<IPlayListRevisionGroup>(lsequences.size());
        for (ILPlayListRevisionGroup s : lsequences)
        {
            s.setParent(sequenceId);
            IPlayListRevisionGroup playListRevisionGroup = getResourceContext().getTranslator().fromLightweight(s,
                    getResourceContext());
            objects.add(playListRevisionGroup);
        }

        return objects;
    }

    // @Override
    // protected List<IPlayListRevisionGroup> doPrePersistCollection(Representation entity, Map<String, String> map)
    // throws Hd3dTranslationException
    // {
    // List<ILPlayListRevisionGroup> lsequences = JSonFormSerializer.<ILPlayListRevisionGroup> deserializeList(entity);
    // Long sequenceId = getIdFromUrl(SEQUENCE_ID_ATTRIBUTE);
    // List<IPlayListRevisionGroup> objects = new ArrayList<IPlayListRevisionGroup>(lsequences.size());
    // for (ILPlayListRevisionGroup s : lsequences)
    // {
    // s.setParent(sequenceId);
    // IPlayListRevisionGroup sequence = translator.fromLightweight(s, map);
    // objects.add(sequence);
    //
    // // PrePersist
    // sequence.doPrePersist();
    // executeScripts(sequence, IPersist.PREPERSIST, getNewIdentifier(sequence));
    // }
    //
    // return objects;
    // }

    // @Override
    // protected IPlayListRevisionGroup doPrePersist(Representation entity, Map<String, String> map) throws
    // Hd3dTranslationException
    // {
    // ILPlayListRevisionGroup lsequence = JSonFormSerializer.<ILPlayListRevisionGroup> deserialize(entity);
    // Long sequenceId = getIdFromUrl(SEQUENCE_ID_ATTRIBUTE);
    // lsequence.setParent(sequenceId);
    // IPlayListRevisionGroup sequence = translator.fromLightweight(lsequence, map);
    //
    // // PrePersist
    // sequence.doPrePersist();
    // executeScripts(sequence, IPersist.PREPERSIST, getNewIdentifier(sequence));
    // return sequence;
    // }
    /**
     * Handle PUT requests. UPDATE
     */
    // @Override
    // public void put(Representation entity)
    // {
    // // Check that the item is not already registered.
    // Response response = getResponse();
    // try
    // {
    // ILPlayListRevisionGroup lsequence = JSonFormSerializer.<ILPlayListRevisionGroup> deserialize(new Form(entity));
    // IPlayListRevisionGroup sequence = objectProvider.getObjectWithId(lsequence.getId());
    // sequence.setName(lsequence.getName()); // only set the name
    //
    // // PreUpdate
    // sequence.doPreUpdate();
    // executeScripts(sequence, IObjectProvider.PREDELETE, getRequest().getResourceRef().toString());
    //
    // // Update
    // objectProvider.mergeObject(sequence);
    //
    // // PostUpdate
    // // Set the response's status and entity
    // response.setStatus(Status.SUCCESS_CREATED);
    // response.setEntity(RessourceSerializer.<ILPlayListRevisionGroup> getRepresentation(entity.getMediaType(),
    // objectTranslator.toLightweight(sequence)));
    // executeScripts(sequence, IObjectProvider.POSTUPDATE, getRequest().getResourceRef().toString());
    // }
    // catch (Exception e)
    // {
    // error(Status.SERVER_ERROR_INTERNAL, e);
    // }
    // finally
    // {
    // cleanup();
    // }
    // }
}
