package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.services.security.utils.AuthenticationUtil;


public class MyActivityDurationResource extends ActivityDurationPerUserResource<ILPersonDay, IPersonDay>
{

    public MyActivityDurationResource() throws Hd3dException
    {
        super();
    }

    protected Long getPersonId()
    {
        return AuthenticationUtil.getCurrentUser().getId();
    }
}
