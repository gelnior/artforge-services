package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectResourceGroupsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("")
public class ProjectResourceGroupsResource extends AbstractHd3dCollection<ILResourceGroup, IResourceGroup>
{
    public ProjectResourceGroupsResource() throws Hd3dException
    {
        super(Persistors.resourcegroup, Translators.resourcegroup);
    }

    @Override
    protected ResultCollection<IResourceGroup> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectResourceGroupsCmd(getResourceContext()).wantTotalSize());
    }

    // @Override
    // protected List<IResourceGroup> doPrePersistCollection(Representation entity, Map<String, String> map)
    // throws Hd3dTranslationException
    // {
    // List<ILCategory> lcategories = JSonFormSerializer.<ILCategory> deserializeList(entity);
    // List<ICategory> objects = new ArrayList<ICategory>(lcategories.size());
    // for (ILCategory c : lcategories)
    // {
    // ICategory category = objectTranslator.fromLightweight(c, map);
    // if (category.getParent() == null)
    // category.setParent(category.getProject().rootCategory());
    // objects.add(category);
    //
    // // PrePersist
    // category.doPrePersist();
    // executeScripts(category, IObjectProvider.PREPERSIST, getNewIdentifier(category));
    // }
    //
    // return objects;
    // }
    //
    // @Override
    // protected ICategory doPrePersist(Representation entity, Map<String, String> map) throws Hd3dTranslationException
    // {
    // ILCategory lcategory = JSonFormSerializer.<ILCategory> deserialize(entity);
    // ICategory category = objectTranslator.fromLightweight(lcategory, map);
    // if (category.getParent() == null)
    // category.setParent(category.getProject().rootCategory());
    //
    // // PrePersist
    // category.doPrePersist();
    // executeScripts(category, IObjectProvider.PREPERSIST, getNewIdentifier(category));
    // return category;
    // }
}
