package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetProjectSimpleActivitiesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * A resource to access to project simple activities. It allows simple activity creation only if activity is linked to a
 * task of this project.
 * 
 * @author HD3D
 */
@Hd3dComment("GET: Retrieve simple activities list for given project. <br />\r\n"
        + "POST: Create a simple activity for given project.<br />\r\n")
public class ProjectSimpleActivitiesResource extends AbstractHd3dCollection<ILSimpleActivity, ISimpleActivity>
{

    public ProjectSimpleActivitiesResource() throws Hd3dException
    {
        super(Persistors.simpleActivity, Translators.simpleActivity);
    }

    @Override
    protected ResultCollection<ISimpleActivity> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetProjectSimpleActivitiesCmd(getResourceContext()).wantTotalSize());
    }

    @Override
    protected void doPrePersist(Representation entity, ISimpleActivity newObject) throws Hd3dException
    {
        IProject project = Persistors.project.getById(getResourceContext().getIdFromUrl(PROJECT_ID_ATTRIBUTE));

        if (project.getId().longValue() == newObject.getProject().getId().longValue())
        {
            super.doPrePersist(entity, newObject);
        }
        else
        {
            throw new Hd3dException("You cannot create activity for another project.");
        }
    }

}
