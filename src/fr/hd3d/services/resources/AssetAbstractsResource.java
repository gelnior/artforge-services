package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILAssetAbstract;
import fr.hd3d.model.persistence.IAssetAbstract;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetAssetAbstractsCmd;
import fr.hd3d.model.translator.Translators;


public class AssetAbstractsResource extends AbstractHd3dCollection<ILAssetAbstract, IAssetAbstract>
{
    public AssetAbstractsResource() throws Hd3dException
    {
        super(Persistors.assetabstract, Translators.assetabstract);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetAssetAbstractsCmd(getResourceContext()).wantTotalSize());
    }
}
