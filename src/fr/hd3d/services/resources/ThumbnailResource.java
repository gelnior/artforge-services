package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.FileUtils;
import org.restlet.Request;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.services.resolver.ThumbnailPathResolver;
import fr.hd3d.utils.Log;


/**
 * Thumbnail resource provides access to thumbnails. It takes as argument folder containing images and id of the object
 * associated to the thumbnail. It also offers the opportunity to upload new thumbnails with the same argument. All
 * thumbnails are proportionally resized to THUMBNAIL_HEIGHT. A preview image (bigger thumbnail) is also generated
 * proportionally to PREVIEW_HEIGHT.
 * 
 * @author HD3D
 */
@Hd3dComment("GET: Retrieve the thumbnail corresponding to folder object id given in URL parameter. <br />"
        + "POST : Create a thumbnail (height of 40px) of the file contained in the post request (variable name : image) in the folder given "
        + "in URL parameter. The id of the object corresponding to thet thumbnail is given in the posted form.")
public class ThumbnailResource extends AbstractHd3dBaseResource
{
    private static final Float THUMBNAIL_HEIGHT = 40.0F;
    private static final Float PREVIEW_HEIGHT = 250.0F;
    private static final String THUMBNAIL_EXTENSION = ".png";
    private static final String PREVIEW_EXTENSION = "_preview.png";

    public static final String THUMBNAILS_PATH = "thumbnails";
    public static final String NO_PICTURE_SEGMENT = "no-picture";

    public static final String PROJECT_PATH = "project-";
    public static final String CONSTITUENTS_SEGMENT = "constituents";
    public static final String CONSTITUENTS_PATH = "constituents";
    public static final String SHOTS_SEGMENT = "shots";
    public static final String SHOTS_PATH = "shots";
    public static final String PERSONS_SEGMENT = "persons";
    public static final String PERSONS_PATH = "persons";
    public static final String PROJECTS_SEGMENT = "projects";
    public static final String PROJECTS_PATH = "projects";

    public static final String IMAGE_FIELD_NAME = "image";
    public static final String ID_FIELD_NAME = "id";
    public static final String NO_PICTURE_FILE = "no_picture.png";

    public static long MAX_FILE_UPLOAD = 150000000;

    /**
     * Default constructor : GET and POST Allowed.
     * 
     * @throws Hd3dException
     */
    public ThumbnailResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("GET,POST");
    }

    /*
     * NOTE: this method override is to fix a bug, do not delete. With the same uri, when the method get(Variant) is
     * called, it succeeds half the time: get(Variant) is called, then get(), then get(Variant), etc...
     */
    public Representation get()
    {
        return get(null);
    }

    /**
     * Return image corresponding to folder and in given in resource parameter (/images/thumbnails/{folder}/{id}.
     * 
     * @see org.restlet.resource.ServerResource#get(org.restlet.representation.Variant)
     */
    @Override
    public Representation get(Variant variant)
    {
        try
        {
            if (getRequest().getOriginalRef().getLastSegment().equals(NO_PICTURE_SEGMENT))
            {
                this.setNoPicFileResponse();
            }
            else if (getRequest().getOriginalRef().getPath().contains(CONSTITUENTS_SEGMENT))
            {
                this.setImageResponse(CONSTITUENTS_PATH, true);
            }
            else if (getRequest().getOriginalRef().getPath().contains(SHOTS_SEGMENT))
            {
                this.setImageResponse(SHOTS_PATH, true);
            }
            else if (getRequest().getOriginalRef().getPath().contains(PERSONS_SEGMENT))
            {
                this.setImageResponse(PERSONS_PATH, false);
            }
            else if (getRequest().getOriginalRef().getPath().contains(PROJECTS_SEGMENT))
            {
                this.setImageResponse(PROJECTS_PATH, false);
            }
            else
            {
                this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            }
            return getResponseEntity();
        }
        finally
        {
            cleanup();
        }
    }

    /**
     * Build a response that contains image as PNG format. Absolute path of the image is constituted of hd3d.images.path
     * property + "thumbnails" + given path.
     * 
     * @param path
     * @param isWorkObject
     */
    private void setImageResponse(String path, boolean isWorkObject)
    {
        String id = getRequest().getOriginalRef().getLastSegment();

        File file = ThumbnailPathResolver.getFile(path, isWorkObject, id);

        if (file != null && file.exists())
        {
            FileRepresentation fileRepresentation = new FileRepresentation(file, MediaType.IMAGE_PNG);
            getResponse().setEntity(fileRepresentation);
            // getResponse().setDate(new Date());
            getResponse().setStatus(Status.SUCCESS_OK);
        }
        else
        {
            setNoPicFileResponse();
        }
    }

    /**
     * Build an image response that returns the no-picture thumbnail.
     */
    private void setNoPicFileResponse()
    {
        File noPicFile = new File(ThumbnailPathResolver.getNoPicPath());
        FileRepresentation fileRepresentation = new FileRepresentation(noPicFile, MediaType.IMAGE_PNG);

        if (noPicFile.exists())
        {
            getResponse().setEntity(fileRepresentation);
            // getResponse().setDate(new Date());
            getResponse().setStatus(Status.SUCCESS_OK);
        }
        else
        {
            getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
        }
    }

    /**
     * Via POST method, this resource let the user upload thumbnail into a specific folder
     * ((/images/thumbnails/{folder}/).
     * 
     * @see org.restlet.resource.ServerResource#post(org.restlet.representation.Representation,
     *      org.restlet.representation.Variant)
     */
    public Representation post(Representation entity, Variant variant)
    {
        if (entity == null)
        {
            getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            return getResponseEntity();
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();
        RestletFileUpload upload = new RestletFileUpload(factory);
        upload.setFileSizeMax(MAX_FILE_UPLOAD);
        upload.setSizeMax(MAX_FILE_UPLOAD);

        List<FileItem> items = null;
        FileItem imageItem = null;
        Request request = this.getRequest();
        try
        {
            items = upload.parseRequest(request);
            String id = null;

            for (FileItem item : items)
            {
                if (item.isFormField() && ID_FIELD_NAME.equals(item.getFieldName()))
                {
                    id = item.getString();
                }
                else if (IMAGE_FIELD_NAME.equals(item.getFieldName()))
                {
                    imageItem = item;
                }
            }

            if (id != null && imageItem != null)
            {
                File folderFile = this.makeFolder(id);
                if (folderFile == null)
                {
                    error(Status.SERVER_ERROR_INTERNAL, null, ERROR_OCCURED);
                }
                else
                {
                    BufferedImage image = ImageIO.read(imageItem.getInputStream());
                    this.createThumbnail(id, image, folderFile, THUMBNAIL_EXTENSION, THUMBNAIL_HEIGHT);
                    this.createThumbnail(id, image, folderFile, PREVIEW_EXTENSION, PREVIEW_HEIGHT);
                }
            }
            else
            {
                error(Status.SERVER_ERROR_INTERNAL, null, ERROR_OCCURED);
            }
        }
        catch (FileUploadException e)
        {
            return this.getErrorResponseEntity(e);
        }
        catch (IOException e)
        {
            return this.getErrorResponseEntity(e);
        }
        catch (Exception e)
        {
            return this.getErrorResponseEntity(e);
        }
        finally
        {
            factory = null;
            upload = null;

            if (items != null)
            {
                items.clear();
                items = null;
            }
            imageItem = null;
        }

        return getResponseEntity();
    }

    private Representation getErrorResponseEntity(Exception e)
    {
        e.printStackTrace();
        error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
        return getResponseEntity();
    }

    /**
     * Create the thumbnail based on imageItem in the file system. Thumbnail is the resize of imageItem (<i>height</i>
     * given in parameter). Thumbnail file name is composed of id + <i>extension</i>.
     * 
     * @param id
     *            The id of the object associated to the thumbnail.
     * @param image
     *            The image used to create the thumbnail.
     * @param folderFile
     *            The folder where to save the thumbnail.
     * @param extension
     *            The extension of the generated file.
     * @param height
     *            The height of the generated file.
     * 
     * @throws Exception
     *             I/O Exceptions
     */
    private void createThumbnail(String id, BufferedImage image, File folderFile, String extension, Float height)
            throws Exception
    {
        if (id == null || folderFile == null)
            return;

        final String fname = ThumbnailPathResolver.getPath(id, folderFile, extension);

        Log.LOGGER.info("trying to create intermediate folders:" + folderFile.getPath());
        FileUtils.forceMkdir(new File(folderFile.getPath()));
        Log.LOGGER.info("trying to create file:" + fname);
        File imageFile = new File(fname);
        if (imageFile.exists())
        {
            imageFile.delete();
        }
        final boolean ok = imageFile.createNewFile();
        if (ok)
        {
            if (image != null)
            {
                this.resizeImageAndWriteThumbnail(image, imageFile, height);
            }
            else
            {
                error(Status.SERVER_ERROR_INTERNAL, null, ERROR_OCCURED);
            }
        }
        else
        {
            error(Status.SERVER_ERROR_INTERNAL, null, ERROR_OCCURED);
            throw new Hd3dException(MessageFormat.format("Cannot create file {0}: already exists", fname));
        }
    }

    /**
     * Resize buffered image and write it to the file system.
     * 
     * @param image
     *            The buffered image.
     * @param imageFile
     *            The target file for the thumbnail creation.
     * @param finalHeight
     *            Height of the new image.
     * @throws IOException
     *             I/O errors.
     */
    private void resizeImageAndWriteThumbnail(BufferedImage image, File imageFile, Float finalHeight)
            throws IOException
    {
        int width = image.getWidth();
        int height = image.getHeight();

        if (width > 0 && height > 0)
        {
            float coeff = (float) (finalHeight / (float) height);

            ImageIO.write(resizeImage(image, coeff, coeff), "png", imageFile);
        }
        else
        {
            error(Status.SERVER_ERROR_INTERNAL, null, ERROR_OCCURED);
        }
    }

    /**
     * @param image
     *            The image to resize.
     * @param coeffLargeur
     *            Coefficient used to resize width.
     * @param coeffHauteur
     *            Coefficient used to resize height.
     * @return A resize a Buffered image depending on coefficient given in parameters. There are one coefficient for the
     *         width and one for the height
     */
    public BufferedImage resizeImage(BufferedImage image, float coeffLargeur, float coeffHauteur)
    {
        AffineTransform tx = new AffineTransform();
        tx.scale(coeffLargeur, coeffHauteur);
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BICUBIC);
        BufferedImage newImage = new BufferedImage((int) (image.getWidth(null) * coeffLargeur), (int) (image
                .getHeight(null) * coeffHauteur), image.getType());
        return op.filter(image, newImage);
    }

    /**
     * @param id
     *            The item id.
     * @return Folder in the file system corresponding to the folder given in the resource URI. The folder is created if
     *         it does not already exist.
     */
    private File makeFolder(String id)
    {
        String type = getRequest().getOriginalRef().getLastSegment();
        if (type == null)
            return null;

        File folderFile = new File(ThumbnailPathResolver.getFolderPath(id, type));

        if (!folderFile.exists())
        {
            folderFile.mkdir();
        }

        return folderFile;
    }

    /**
     * @return null.
     * 
     * @see fr.hd3d.services.resources.AbstractHd3dBaseResource#getNewResourceIdentifier(java.lang.Object)
     */
    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        return null;
    }

}
