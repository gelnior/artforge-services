package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.CATEGORY_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.RESOURCEGROUP_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetGroupChildrenCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.translator.json.JSonFormSerializer;


public class ResourceGroupChildrenResource extends AbstractHd3dCollection<ILResourceGroup, IResourceGroup>
{
    public ResourceGroupChildrenResource() throws Hd3dException
    {
        super(Persistors.resourcegroup, Translators.resourcegroup);
    }

    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetGroupChildrenCmd(getResourceContext()).wantTotalSize());
    }

    @Override
    protected IResourceGroup newObject(Representation entity) throws Hd3dException
    {
        ILResourceGroup lresourceGroup = JSonFormSerializer.<ILResourceGroup> deserialize(entity);
        final Long resourceGroupId = getResourceContext().getIdFromUrl(RESOURCEGROUP_ID_ATTRIBUTE);
        lresourceGroup.setParent(resourceGroupId);
        IResourceGroup resourceGroup = getResourceContext().getTranslator().fromLightweight(lresourceGroup,
                getResourceContext());

        return resourceGroup;
    }

    @Override
    protected List<IResourceGroup> newCollection(Representation entity) throws Hd3dException
    {
        List<ILResourceGroup> lObjects = JSonFormSerializer.<ILResourceGroup> deserializeList(entity);
        final Long resourceGroupId = getResourceContext().getIdFromUrl(CATEGORY_ID_ATTRIBUTE);

        List<IResourceGroup> objects = new ArrayList<IResourceGroup>(lObjects.size());
        for (ILResourceGroup light : lObjects)
        {
            light.setParent(resourceGroupId);
            objects.add(getResourceContext().getTranslator().fromLightweight(light, getResourceContext()));
        }

        return objects;
    }

    // @Override
    // protected IResourceGroup doPrePersist(Representation entity, Map<String, String> map)
    // throws Hd3dTranslationException
    // {
    // ILResourceGroup lresourceGroup = JSonFormSerializer.<ILResourceGroup> deserialize(entity);
    // final Long resourceGroupId = getIdFromUrl(Hd3dApplication.RESOURCEGROUP_ID_ATTRIBUTE);
    // lresourceGroup.setParent(resourceGroupId);
    // IResourceGroup resourceGroup = translator.fromLightweight(lresourceGroup, map);
    //
    // resourceGroup.doPrePersist();
    // executeScripts(resourceGroup, IPersist.PREPERSIST, getNewIdentifier(resourceGroup));
    //
    // return resourceGroup;
    // }

    // @Override
    // protected List<IResourceGroup> doPrePersistCollection(Representation entity, Map<String, String> map)
    // throws Hd3dTranslationException
    // {
    // List<ILResourceGroup> lObjects = JSonFormSerializer.<ILResourceGroup> deserializeList(entity);
    // final Long resourceGroupId = getIdFromUrl(Hd3dApplication.CATEGORY_ID_ATTRIBUTE);
    //
    // List<IResourceGroup> objects = new ArrayList<IResourceGroup>(lObjects.size());
    // for (ILResourceGroup light : lObjects)
    // {
    // light.setParent(resourceGroupId);
    // objects.add(translator.fromLightweight(light, map));
    // }
    //
    // // PrePersist
    // for (IResourceGroup object : objects)
    // object.doPrePersist();
    // executeScripts(objects.get(0), IPersist.PREPERSIST, getNewIdentifier(objects));
    // return objects;
    // }
}
