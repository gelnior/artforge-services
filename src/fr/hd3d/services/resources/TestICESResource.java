package fr.hd3d.services.resources;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;


/**
 * Return all the available business entities
 * 
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class TestICESResource<L, P> extends AbstractHd3dBaseResource
{
    public TestICESResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    @Override
    public Representation get(Variant variant)
    {
        String referreUrl = getRequest().getReferrerRef().toString();

        String page = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">"
                + "<html>"
                + "<head>"
                + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
                + "<title>Insert title here</title>"
                + "</head>"
                + "<body>"
                + "TEST ICES<br>"
                + "<form name='myform' method='POST' action='"
                + referreUrl
                + "ices' enctype='multipart/form-data'>"
                + "<b>name: </b>&nbsp;<input type='text' name='name'><br>"
                + "<b>-ip</b>&nbsp;<input type='file' name='ip'><br>"
                + "<b>-op</b>&nbsp;<input type='file' name='op'><br>"
                + "<b>-bi</b>&nbsp;<input type='text' name='bi'><br>"
                + "<b>-bo</b>&nbsp;<input type='text' name='bo'><br>"
                + "<b>(</b><b>-t</b>&nbsp;<input type='text' name='t'><b>)</b><br>"
                + "<input type='submit' value='GO'>" + "</form> " + "</body>" + "</html>";

        Representation rep = new StringRepresentation(page, MediaType.TEXT_HTML);
        return rep;
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
