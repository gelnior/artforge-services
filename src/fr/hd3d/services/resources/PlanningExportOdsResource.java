/**
 * 
 */
package fr.hd3d.services.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.representation.OutputRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


public class PlanningExportOdsResource extends ServerResource
{

    /**
     * Class which contains the number of days when start the task and the number of days estimed for the task.
     * 
     * @author HD3D
     * 
     */
    static class Duration
    {
        private final int nbToStart;
        private final int nbEstimed;

        public Duration(int nbToStart, int nbEstimed)
        {
            this.nbToStart = nbToStart;
            this.nbEstimed = nbEstimed;
        }

        public int getNbToStart()
        {
            return nbToStart;
        }

        public int getDuration()
        {
            return nbToStart + nbEstimed;
        }
    }

    static class PairDate
    {
        private Date startDate;
        private Date endDate;

        public PairDate(Date startDate, Date endDate)
        {
            this.startDate = startDate;
            this.endDate = endDate;
        }

        public PairDate()
        {}

        public Date getStartDate()
        {
            return startDate;
        }

        public void setStartDate(Date startDate)
        {
            this.startDate = startDate;
        }

        public Date getEndDate()
        {
            return endDate;
        }

        public void setEndDate(Date endDate)
        {
            this.endDate = endDate;
        }

    }

    private static final String INDENTATION_PATTERN = "     ";
    private static final String WEEK_END_DAY_STYLE = "weekEndDay";
    private static final String WEEK_DAY_STYLE = "weekDay";
    private final long ONE_DAY = 24 * 3600 * 1000;
    private String[] days = new String[] { "S", "M", "T", "W", "T", "F", "S" };
    private int currentMonthNumber;
    private Map<String, CellStyle> styleMap;

    /** WorkBook which contains the ods file. */
    private HSSFWorkbook wb = new HSSFWorkbook();
    /** Contains the planning to export. */
    private IPlanning planning;
    /** Contains the project concerning the planning to export. */
    private IProject project;

    /** Map containing the indexed color of task type. */
    private final HashMap<Long, Short> indexColorMapByTaskTypeId = new HashMap<Long, Short>();
    /** Palette containing the indexed colors. */
    private HSSFPalette palette = wb.getCustomPalette();
    /** Map containing the list of task by constituent id. */
    private final HashMap<Long, ArrayList<ITask>> taskListMapByConstituentId = new HashMap<Long, ArrayList<ITask>>();
    /** Map containing the list of task by shot id. */
    private final HashMap<Long, ArrayList<ITask>> taskListMapByShotId = new HashMap<Long, ArrayList<ITask>>();
    /** Map containing the list of task by worker name and by tasktype id. */
    private final HashMap<Long, TreeMap<String, ArrayList<ITask>>> taskListMapByTaskType = new HashMap<Long, TreeMap<String, ArrayList<ITask>>>();
    /** Map containing the list of task type by task type id. */
    private final HashMap<Long, ITaskType> taskTypeMapById = new HashMap<Long, ITaskType>();
    /** Map containing the list of task group by task type id. */
    private final HashMap<Long, ITaskGroup> taskGroupMapByTaskTypeId = new HashMap<Long, ITaskGroup>();

    /** number of days of planning. */
    private int nbDaysPlanning;
    /** level of deep. */
    private int level = 0;

    /** Constant to define the display task type. */
    private static long TASKTYPE = -1;
    /** Constant to define the display constituent. */
    private static long CONSTITUENT = -2;
    /** Constant to define the display shot. */
    private static long SHOT = -3;

    public PlanningExportOdsResource()
    {
        init(Context.getCurrent(), Request.getCurrent(), Response.getCurrent());

    }

    @Override
    protected Representation get() throws ResourceException
    {
        Long projectID = getIdFromUrl(ServicesURI.PROJECT_ID_ATTRIBUTE);
        Long planningID = getIdFromUrl(ServicesURI.PLANNING_ID_ATTRIBUTE);

        OutputRepresentation out = null;
        try
        {
            project = Persistors.project.getById(projectID);
            planning = Persistors.planning.getById(planningID);

            if (planning.getStartDate() == null || planning.getEndDate() == null)
            {
                System.out
                        .println("PlanningExportOdsResource - get -> the planning hasn't no start date or no end date: start date:"
                                + planning.getStartDate() + ", end date:" + planning.getEndDate());
                return out;
            }

            if (planning.getTaskGroups() != null)
            {
                for (ITaskGroup taskGroup : planning.getTaskGroups())
                {
                    taskGroupMapByTaskTypeId.put(taskGroup.getTaskType().getId(), taskGroup);
                }
            }
            else
            {
                Log.LOGGER.error("PlanningExportOdsResource - get -> the task groups are null");
            }

            this.nbDaysPlanning = getDaysBetweenDates(planning.getStartDate(), planning.getEndDate());
            this.getTasks();

            // Write the output to a file
            out = new OutputRepresentation(MediaType.APPLICATION_OPENOFFICE_ODS) {

                @Override
                public void write(OutputStream stream) throws IOException
                {
                    try
                    {
                        styleMap = createStyles();

                        Sheet workerSheet = this.createSheet("Worker");
                        addTreeTaskType(workerSheet);
                    }
                    catch (Exception e)
                    {
                        System.out.println("A error is throwed and stop the creation of the worker sheet : "
                                + e.getMessage());
                    }
                    try
                    {
                        Sheet constituentSheet = this.createSheet("Constituent");
                        addTreeConstituent(constituentSheet);
                    }
                    catch (Exception e)
                    {
                        System.out.println("A error is throwed and stop the creation of the constituent sheet : "
                                + e.getMessage());
                    }
                    try
                    {
                        Sheet shotSheet = this.createSheet("Shot");
                        addTreeShot(shotSheet);
                    }
                    catch (Exception e)
                    {
                        Log.LOGGER.error("A error is throwed and stop the creation of the shot sheet : "
                                + e.getMessage());
                    }

                    wb.write(stream);
                }

                private Sheet createSheet(String string)
                {
                    // create worker Sheet
                    Sheet sheet = wb.createSheet("Worker");
                    sheet.autoSizeColumn(0, true);
                    sheet.setColumnWidth(0, 256 * 33);
                    sheet.createFreezePane(1, 4);

                    // create Timeline
                    createTimeLine(sheet);
                    return sheet;
                }

            };

            String fileName = project.getName() + ".ods";
            Disposition disposition = new Disposition();
            disposition.setFilename(fileName);
            disposition.setType(Disposition.TYPE_INLINE);
            out.setDisposition(disposition);

        }
        catch (Hd3dException e)
        {
            e.printStackTrace();
        }
        return out;

    }

    /**
     * Fill the different Maps to sort the data to display. It get to database to fill Maps. The data are sorted by
     * constituent, task Type and shot. It get the indexed colors to display.
     * 
     * @throws Hd3dPersistenceException
     */
    @SuppressWarnings("unchecked")
    private void getTasks()
    {
        StringBuilder queryStr = new StringBuilder();
        queryStr.append("from ").append(Const.CANONICALNAME_TASK).append(" where project.id =").append(project.getId())
                .append(" and startDate is not null ").append(" and endDate is not null");// .append("and worker is not null");

        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();
        List<ITask> listTask = null;
        listTask = session.createQuery(queryStr.toString()).list();
        if (session.getFlushMode() == FlushMode.MANUAL)
        {
            session.flush();
        }
        tx.commit();

        if (listTask == null || listTask.isEmpty())
        {
            System.out.println("PlanningExportOdsResource - getTasks -> the query return no result");
            return;
        }
        for (ITask task : listTask)
        {
            taskTypeMapById.put(task.getTaskType().getId(), task.getTaskType());
            // arbre des constituents
            try
            {
                setTaskConstituentTree(task);
            }
            catch (Hd3dException e)
            {
                System.out.println("A Error is throwed and stop the creation of the constituent task tree : "
                        + e.getMessage());
            }

            // arbre des tasktypes
            try
            {
                setTaskTaskTypeTree(task);

            }
            catch (Hd3dException e)
            {
                System.out.println("A Error is throwed and stop the creation of the task type task tree : "
                        + e.getMessage());
            }

            // arbre des shots
            try
            {
                setTaskShotTree(task);
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error("A Error is throwed and stop the creation of the shot tasks tree : " + e.getMessage());
            }

        }
        setColorMapByTaskTypeId("#AAFFAA", CONSTITUENT);
        setColorMapByTaskTypeId("#AAAAFF", SHOT);
    }

    /**
     * Set the map of the list of task by shot id.
     * 
     * @param task
     *            the task to set
     * @throws Hd3dPersistenceException
     */
    private void setTaskShotTree(ITask task) throws Hd3dException
    {
        if (task == null)
        {
            throw new Hd3dException("PlanningExportOdsResource - setTaskShotTree -> The task is null");
        }

        IBase boundEntity = task.boundEntity();
        if (IShot.class.isInstance(boundEntity))
        {
            IShot shot = (IShot) boundEntity;
            ArrayList<ITask> taskList = taskListMapByShotId.get(shot.getId());
            if (taskList == null)
            {
                taskList = new ArrayList<ITask>();
            }
            taskList.add(task);
            taskListMapByShotId.put(shot.getId(), taskList);
        }
        if (indexColorMapByTaskTypeId.get(task.getTaskType().getId()) == null)
        {
            setColorMapByTaskTypeId(task.getTaskType().getColor(), task.getTaskType().getId());
        }
    }

    /**
     * Set the map of the list of task by worker name and by task type id.
     * 
     * @param task
     *            the task to set.
     * @throws Hd3dException
     */
    private void setTaskTaskTypeTree(ITask task) throws Hd3dException
    {
        if (task == null || task.getWorker() == null)
        {
            Log.LOGGER
                    .error("PlanningExportOdsResource - setTaskTypeTree -> the task is null or is not assigned : task:"
                            + task + ", worker:" + ((task == null) ? "" : task.getWorker()));
            return;
        }

        TreeMap<String, ArrayList<ITask>> taskListMapByWorker = taskListMapByTaskType.get(task.getTaskType().getId());
        if (taskListMapByWorker == null)
        {
            taskListMapByWorker = new TreeMap<String, ArrayList<ITask>>();
            taskListMapByTaskType.put(task.getTaskType().getId(), taskListMapByWorker);
        }
        ArrayList<ITask> taskList = taskListMapByWorker.get(task.getWorker().fullName());
        if (taskList == null)
        {
            taskList = new ArrayList<ITask>();
            taskListMapByWorker.put(task.getWorker().fullName(), taskList);
        }
        taskList.add(task);
    }

    /**
     * Set the map of the list of task by constituent id.
     * 
     * @param task
     *            the task to set.
     * @throws Hd3dPersistenceException
     */
    private void setTaskConstituentTree(ITask task) throws Hd3dException
    {
        if (task == null)
        {
            throw new Hd3dException("PlanningExportOdsResource - setTaskConstituentTree -> the task is null");
        }
        IBase boundEntity = task.boundEntity();
        if (IConstituent.class.isInstance(boundEntity))
        {

            IConstituent constituent = (IConstituent) boundEntity;
            ArrayList<ITask> taskList = taskListMapByConstituentId.get(constituent.getId());
            if (taskList == null)
            {
                taskList = new ArrayList<ITask>();
            }
            taskList.add(task);
            taskListMapByConstituentId.put(constituent.getId(), taskList);
        }
        if (indexColorMapByTaskTypeId.get(task.getTaskType().getId()) == null)
        {
            setColorMapByTaskTypeId(task.getTaskType().getColor(), task.getTaskType().getId());
        }
    }

    /**
     * Set the color to the maps of indexed colors.
     * 
     * @param color
     *            the color to set.
     * @param taskTypeId
     *            the task type id of the color
     */
    private void setColorMapByTaskTypeId(String color, Long taskTypeId)
    {
        if (color == null || taskTypeId == null)
        {
            Log.LOGGER
                    .error("PlanningExportOdsResource - setColorMapByTaskTypeId -> the color or the task type id is null : color:"
                            + color + ", task type id:" + taskTypeId);
            return;
        }
        String effectiveColor = color;
        int red = hex2decimal(effectiveColor.substring(1, 3));
        int green = hex2decimal(effectiveColor.substring(3, 5));
        int blue = hex2decimal(effectiveColor.substring(5, 7));
        HSSFColor indexedColor = palette.findSimilarColor(red, green, blue);
        indexColorMapByTaskTypeId.put(taskTypeId, indexedColor.getIndex());
    }

    /**
     * Get the number of days between 2 dates.
     * 
     * @param firstDate
     *            the first date
     * @param secondDate
     *            the second date
     * @return the number of the first date and the second date.
     * @throws Hd3dException
     */
    private int getDaysBetweenDates(Date firstDate, Date secondDate) throws Hd3dException
    {
        if (firstDate == null || secondDate == null)
        {
            throw new Hd3dException(
                    "PlanningExportOdsResource - getDaysBetweenDates -> the first date or the second date is null: first date:"
                            + firstDate + ", second date:" + secondDate);
        }
        int nbDays = 0;
        long purgeFirstDateTime = (long) Math.floor(firstDate.getTime() / (double) ONE_DAY);
        long purgeSecondDateTime = (long) Math.floor(secondDate.getTime() / (double) ONE_DAY);
        Date purgeFirstDate = new Date(purgeFirstDateTime * ONE_DAY);
        Date purgeSecondDate = new Date(purgeSecondDateTime * ONE_DAY);
        if (purgeSecondDate.before(purgeFirstDate))
        {
            nbDays = (int) ((purgeFirstDate.getTime() - purgeSecondDate.getTime()) / ONE_DAY);
        }
        else if (purgeFirstDate.before(purgeSecondDate))
        {
            nbDays = (int) ((purgeSecondDate.getTime() - purgeFirstDate.getTime()) / ONE_DAY);
        }
        return Math.max(0, nbDays);
    }

    /**
     * Parse a hexadecimal string to integer.
     * 
     * @param s
     *            the string to convert
     * @return the integer corresponding to the string. -1 if s is null
     */
    public int hex2decimal(String s)
    {
        if (s == null)
        {
            return -1;
        }
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16 * val + d;
        }
        return val;
    }

    /**
     * Create a time line in the given sheet.
     * 
     * @param sheet
     *            the given sheet
     */
    private void createTimeLine(Sheet sheet)
    {
        Row monthRow = sheet.createRow(0);
        Row weekRow = sheet.createRow(1);
        Row dayNameRow = sheet.createRow(2);
        Row dayNumberRow = sheet.createRow(3);

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(planning.getEndDate());
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(planning.getStartDate());

        int index = 1;
        int lastWeekIndex = 1;
        int lastMontIndex = 1;
        currentMonthNumber = -1;

        lastMontIndex = createMonthRow(sheet, monthRow, endCalendar, calendar, index, lastMontIndex);
        String calendarFormatted = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT).format(calendar.getTime());
        String endDateFormatted = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT).format(
                planning.getEndDate().getTime() + ONE_DAY);

        while (!calendarFormatted.equals(endDateFormatted))
        {
            Cell dayNumberCell = dayNumberRow.createCell(index);
            dayNumberCell.setCellValue(calendar.get(Calendar.DAY_OF_MONTH));
            Cell dayNameCell = dayNameRow.createCell(index);
            dayNameCell.setCellValue(days[calendar.get(Calendar.DAY_OF_WEEK) - 1]);
            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
                    || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
            {
                dayNumberCell.setCellStyle(styleMap.get(WEEK_END_DAY_STYLE));
                dayNameCell.setCellStyle(styleMap.get(WEEK_END_DAY_STYLE));
            }
            else
            {
                dayNumberCell.setCellStyle(styleMap.get(WEEK_DAY_STYLE));
                dayNameCell.setCellStyle(styleMap.get(WEEK_DAY_STYLE));
            }

            // create week row
            lastWeekIndex = createWeekRow(sheet, weekRow, calendar, endCalendar, index, lastWeekIndex);
            // create month row
            lastMontIndex = createMonthRow(sheet, monthRow, endCalendar, calendar, index, lastMontIndex);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            index++;
            calendarFormatted = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT).format(calendar.getTime());
            endDateFormatted = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT).format(
                    planning.getEndDate().getTime() + ONE_DAY);
        }
        // createWeekRow(sheet, weekRow, calendar, endCalendar, index, lastWeekIndex);
    }

    /**
     * Create a month row in the given sheet.
     * 
     * @param sheet
     *            the given sheet.
     * @param monthRow
     * @param endCalendar
     * @param calendar
     * @param index
     * @param lastMontIndex
     * @return
     */
    private int createMonthRow(Sheet sheet, Row monthRow, Calendar endCalendar, Calendar calendar, int index,
            int lastMontIndex)
    {
        int monthNumber = calendar.get(Calendar.MONTH);
        int endDateMonthNumber = endCalendar.get(Calendar.MONTH);
        if (currentMonthNumber != monthNumber)
        {
            Cell monthCell = monthRow.createCell(lastMontIndex);
            int lastDayOfMonthNumber = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            int colSpan = lastDayOfMonthNumber - calendar.get(Calendar.DAY_OF_MONTH);
            if (monthNumber == endDateMonthNumber)
            {
                lastDayOfMonthNumber = endCalendar.get(Calendar.DAY_OF_MONTH);
                colSpan = lastDayOfMonthNumber - calendar.get(Calendar.DAY_OF_MONTH);
            }
            monthCell.setCellValue(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH) + " "
                    + calendar.get(Calendar.YEAR));
            monthCell.setCellStyle(styleMap.get(WEEK_DAY_STYLE));
            CellRangeAddress cellRangeAddress = new CellRangeAddress(0, 0, lastMontIndex, lastMontIndex + colSpan);
            sheet.addMergedRegion(cellRangeAddress);
            currentMonthNumber = monthNumber;
            lastMontIndex += colSpan + 1;
        }
        return lastMontIndex;
    }

    /**
     * Create a week row in the given sheet.
     * 
     * @param sheet
     *            the given sheet.
     * @param weekRow
     * @param calendar
     * @param endCalendar
     * @param index
     * @param lastWeekIndex
     * @return
     */
    private int createWeekRow(Sheet sheet, Row weekRow, Calendar calendar, Calendar endCalendar, int index,
            int lastWeekIndex)
    {
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int weekOfYear = 1;
        if (dayOfWeek == Calendar.SUNDAY || calendar.getTime().toString().equals(endCalendar.getTime().toString()))
        {
            Cell weekCell = weekRow.createCell(lastWeekIndex);
            weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
            if (weekOfYear > 53)
            {
                weekOfYear = weekOfYear % 54;
            }
            weekCell.setCellValue(weekOfYear);
            weekCell.setCellStyle(styleMap.get(WEEK_DAY_STYLE));
            CellRangeAddress cellRangeAddress = new CellRangeAddress(1, 1, lastWeekIndex, index);
            sheet.addMergedRegion(cellRangeAddress);
            lastWeekIndex = index + 1;
        }
        return lastWeekIndex;
    }

    /**
     * create a library of cell styles
     */
    private Map<String, CellStyle> createStyles()
    {
        Map<String, CellStyle> styles = new HashMap<String, CellStyle>();

        CellStyle style;
        style = createBorderedStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put(WEEK_DAY_STYLE, style);

        style = createBorderedStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put(WEEK_END_DAY_STYLE, style);

        return styles;
    }

    /**
     * Create a bordered style with a left border and a right border.
     * 
     * @return the created cell style.
     */
    private CellStyle createBorderedStyle()
    {
        CellStyle style = wb.createCellStyle();
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }

    /**
     * Set to the given cell style a bordered style.
     * 
     * @param cellStyle
     *            the given cell style
     */
    private void setAllBorderedStyle(CellStyle cellStyle)
    {

        cellStyle.setBorderRight(CellStyle.BORDER_THIN);
        cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        cellStyle.setBorderTop(CellStyle.BORDER_THIN);
        cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
        cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());

    }

    /**
     * Return the id long from the id string of the url.
     * 
     * @param idString
     * @return the id long
     */
    public Long getIdFromUrl(String idString)
    {
        return NumberUtils.toLong((String) getRequest().getAttributes().get(idString));
    }

    /**
     * Add all the tree of shot with the task in the given shot.
     * 
     * @param sheet
     *            the given sheet
     * @throws Hd3dPersistenceException
     */
    private void addTreeShot(Sheet sheet) throws Hd3dException
    {
        addChildren(sheet, project.rootSequence());
    }

    /**
     * Add the child to the tree of shot.
     * 
     * @param sheet
     *            the given sheet.
     * @param rootSequence
     *            the sequence parent.
     * @return the oldest start date and the newest end date in the list.
     * @throws Hd3dPersistenceException
     */
    private PairDate addChildren(Sheet sheet, ISequence rootSequence) throws Hd3dException
    {
        Collection<ISequence> sequenceCollection = rootSequence.getChildren();
        if (sequenceCollection.size() == 0 && rootSequence.getShots().size() == 0)
        {
            return null;
        }

        PairDate dates = new PairDate();
        for (ISequence sequence : sequenceCollection)
        {
            if (sequence.getChildren().size() == 0 && sequence.getShots().size() == 0)
            {
                continue;
            }
            Row row = createLine(sheet, sequence);
            level++;
            PairDate dateList1 = addShots(sheet, sequence);

            PairDate dateList2 = addChildren(sheet, sequence);
            PairDate dateListTmp = getMinAndMaxDate(dateList1, dateList2);
            level--;
            if (dateListTmp != null)
            {
                createBarTime(sheet, row, dateListTmp, indexColorMapByTaskTypeId.get(SHOT), sequence.getName());
            }
            dates = getMinAndMaxDate(dates, dateListTmp);
        }
        return dates;

    }

    /**
     * Add the shot to the tree of shot.
     * 
     * @param sheet
     *            the given sheet.
     * @param sequence
     *            the sequence containing the shot
     * @return the oldest start date and the newest end date in the list.
     * @throws Hd3dPersistenceException
     */
    private PairDate addShots(Sheet sheet, ISequence sequence) throws Hd3dException
    {
        PairDate dates = new PairDate();
        TreeSet<IShot> sortedshot = new TreeSet<IShot>(new Comparator<IShot>() {
            public int compare(IShot o1, IShot o2)
            {
                return o1.getLabel().compareTo(o2.getLabel());
            }
        });
        sortedshot.addAll(sequence.getShots());
        for (IShot shot : sortedshot)
        {
            ArrayList<ITask> taskList = taskListMapByShotId.get(shot.getId());
            if (taskList == null)
                continue;

            Row firstRow = createLine(sheet, shot);
            level++;
            setMultiLine(sheet, firstRow, taskList, dates, SHOT);
            level--;

        }
        return dates;
    }

    /**
     * Add the tree of task type with the task in the given sheet.
     * 
     * @param sheet
     *            the given sheet.
     * @throws Hd3dPersistenceException
     */
    private void addTreeTaskType(Sheet sheet) throws Hd3dException
    {
        if (sheet == null)
        {
            throw new Hd3dException("PlanningExportOdsResource - addTreeTaskType -> the sheet is null");
        }

        for (Long taskTypeId : taskTypeMapById.keySet())
        {
            Row taskTypeRow = sheet.createRow(sheet.getLastRowNum() + 1);
            Cell cell = createNameCell(taskTypeRow, taskTypeMapById.get(taskTypeId).getName());
            Short color = indexColorMapByTaskTypeId.get(taskTypeId);
            setColorForegroundColor(cell, color == null ? HSSFColor.WHITE.index : color);

            ITaskGroup taskGroup = taskGroupMapByTaskTypeId.get(taskTypeId);
            if (taskGroup != null)
            {
                createBarTime(sheet, taskTypeRow, new PairDate(taskGroup.getStartDate(), taskGroup.getEndDate()),
                        indexColorMapByTaskTypeId.get(taskTypeId), taskGroup.getName());
            }

            TreeMap<String, ArrayList<ITask>> taskListMapByWorker = taskListMapByTaskType.get(taskTypeId);
            level++;
            for (String workerName : taskListMapByWorker.keySet())
            {
                ArrayList<ITask> taskList = taskListMapByWorker.get(workerName);
                if (taskList == null)
                {
                    continue;
                }
                Row workerRow = sheet.createRow(sheet.getLastRowNum() + 1);
                createNameCell(workerRow, workerName);
                addMultiLineTimeBarTask(sheet, workerRow, taskList, TASKTYPE);

            }
            level--;
        }
    }

    /**
     * Add the multiple Bar Time line of task for the same entity. the multiple line are set to the given sheet.
     * 
     * @param sheet
     *            the given sheet.
     * @param firstRow
     *            the fisrt row to start the multiple line
     * @param taskList
     *            the list of task to set in the sheet
     * @param type
     *            the type of tree.
     * 
     * @throws Hd3dPersistenceException
     */
    private void addMultiLineTimeBarTask(Sheet sheet, Row firstRow, ArrayList<ITask> taskList, long type)
            throws Hd3dException
    {
        if (sheet == null || firstRow == null || taskList == null)
        {
            throw new Hd3dException(
                    "PlanningExportOdsResource - addMultiLineTimeBarTask -> the sheet or the first row or the list of task is null");
        }
        Boolean[][] busyCaseList = new Boolean[taskList.size()][nbDaysPlanning];
        ArrayList<Row> rowList = new ArrayList<Row>();

        rowList.add(firstRow);

        for (ITask task : taskList)
        {
            Row freeRow = getRowFree(sheet, busyCaseList, rowList, task);

            String name = null;
            if (type == TASKTYPE)
            {
                IBase bound = task.boundEntity();
                if (IConstituent.class.isInstance(bound))
                {
                    IConstituent constituent = (IConstituent) bound;
                    name = constituent.getPath() + constituent.getLabel();
                }
                else if (IShot.class.isInstance(bound))
                {
                    IShot shot = (IShot) bound;
                    name = shot.getPath() + shot.getLabel();
                }

            }
            else if (type == CONSTITUENT)
            {
                name = task.getTaskType().getName();
            }
            else if (type == SHOT)
            {
                name = task.getTaskType().getName();

            }
            createBarTime(sheet, freeRow, new PairDate(task.getStartDate(), task.getEndDate()),
                    indexColorMapByTaskTypeId.get(task.getTaskType().getId()), name);
        }
    }

    /**
     * Get the free row to set the given task in the given sheet.
     * 
     * @param sheet
     *            the given sheet.
     * @param busyCaseList
     *            the list of busy case by the tasks.
     * @param rowList
     *            the list of row.
     * @param task
     *            the given task.
     * @return
     * @throws Hd3dException
     */
    private Row getRowFree(Sheet sheet, Boolean[][] busyCaseList, ArrayList<Row> rowList, ITask task)
            throws Hd3dException
    {
        Date startDate = task.getStartDate();
        Date endDate = task.getEndDate();

        if (startDate == null || endDate == null)
        {
            throw new Hd3dException(
                    "PlanningExportOdsResource - getRowFree -> the start date or the end date is not correct : start date :"
                            + startDate + ", end date : " + endDate);
        }

        Date planningStart = planning.getStartDate();
        Date planningEnd = planning.getEndDate();

        Duration duration = getNbDaysEstimedAndNbDaysToStart(planningStart, startDate, planningEnd, endDate);

        for (int j = 0; j < busyCaseList.length; j++)
        {
            boolean ok = true;
            for (int i = duration.getNbToStart(); i < duration.getDuration(); i++)
            {
                if (busyCaseList[j][i] != null)
                {
                    ok = false;
                    break;
                }

            }
            if (ok)
            {
                Row row = null;
                if (j >= rowList.size())
                {
                    row = sheet.createRow(sheet.getLastRowNum() + 1);
                    rowList.add(row);

                }
                else
                {
                    row = rowList.get(j);
                }
                for (int k = duration.getNbToStart(); k < duration.getDuration(); k++)
                {
                    busyCaseList[j][k] = true;
                }
                return row;
            }
        }

        return null;
    }

    /**
     * Add the tree of constituent in the given sheet.
     * 
     * @param sheet
     *            the given sheet.
     * @throws Hd3dPersistenceException
     */
    private void addTreeConstituent(Sheet sheet) throws Hd3dException
    {
        addChildren(sheet, project.rootCategory());

    }

    /**
     * Add the children of the tree of constituent.
     * 
     * @param sheet
     *            the given sheet
     * @param rootCategory
     *            the parent of children.
     * @return the oldest start date and the newest end date in the list.
     * @throws Hd3dPersistenceException
     */
    private PairDate addChildren(Sheet sheet, ICategory rootCategory) throws Hd3dException
    {
        Collection<ICategory> categoryCollection = rootCategory.getChildren();
        if (categoryCollection.size() == 0 && rootCategory.getConstituents().size() == 0)
        {
            return null;
        }

        PairDate dateList = null;
        for (ICategory category : categoryCollection)
        {
            if (category.getChildren().size() == 0 && category.getConstituents().size() == 0)
            {
                continue;
            }
            Row row = createLine(sheet, category);
            level++;
            PairDate dateList1 = addConstituents(sheet, category);

            PairDate dateList2 = addChildren(sheet, category);
            PairDate dateListTmp = getMinAndMaxDate(dateList1, dateList2);
            level--;
            if (dateListTmp != null)
            {
                createBarTime(sheet, row, dateListTmp, indexColorMapByTaskTypeId.get(CONSTITUENT), category.getName());
            }
            dateList = getMinAndMaxDate(dateList, dateListTmp);
        }
        return dateList;

    }

    /**
     * Add constituent to the tree of constituent
     * 
     * @param sheet
     *            the given sheet
     * @param category
     *            the category containing the constituent
     * @return the oldest start date and the newest end date in the list.
     * @throws Hd3dPersistenceException
     */
    private PairDate addConstituents(Sheet sheet, ICategory category) throws Hd3dException
    {
        PairDate dates = new PairDate();
        TreeSet<IConstituent> sortedConstituent = new TreeSet<IConstituent>(new Comparator<IConstituent>() {
            public int compare(IConstituent o1, IConstituent o2)
            {
                return o1.getLabel().compareTo(o2.getLabel());
            }
        });
        sortedConstituent.addAll(category.getConstituents());
        for (IConstituent constituent : sortedConstituent)
        {
            ArrayList<ITask> taskList = taskListMapByConstituentId.get(constituent.getId());
            if (taskList == null)
                continue;

            Row firstRow = createLine(sheet, constituent);
            level++;
            setMultiLine(sheet, firstRow, taskList, dates, CONSTITUENT);
            level--;

        }
        return dates;
    }

    /**
     * Set multiline of task and set the start date and the end date.
     * 
     * @param sheet
     *            the current sheet
     * @param taskList
     *            the list of task
     * @param startDate
     *            the actual start date
     * @param endDate
     *            the actual end date
     * @param type
     *            the type of time bar
     * @throws Hd3dException
     */
    private void setMultiLine(Sheet sheet, Row firstRow, ArrayList<ITask> taskList, PairDate dates, Long type)
            throws Hd3dException
    {
        addMultiLineTimeBarTask(sheet, firstRow, taskList, type);

        // Search the oldest start date and the more recent end date.
        for (ITask task : taskList)
        {
            if (dates.getStartDate() == null)
            {
                dates.setStartDate(task.getStartDate());
                dates.setEndDate(task.getEndDate());
            }
            else
            {
                if (dates.getStartDate().after(task.getStartDate()))
                {
                    dates.setStartDate(task.getStartDate());
                }
                if (dates.getEndDate().before(task.getEndDate()))
                {
                    dates.setEndDate(task.getEndDate());
                }
            }
        }
    }

    /**
     * Get the oldest start date and the newest end date in the list from 2 lists.
     * 
     * @param dateList
     *            the first list.
     * @param dateList1
     *            the second list.
     * 
     * @return the oldest start date and the newest end date in the list
     */
    private PairDate getMinAndMaxDate(PairDate date1, PairDate date2)
    {
        PairDate dates = new PairDate();
        if (date1 != null)
        {
            if (date1.getStartDate() != null)
            {
                dates.setStartDate(date1.getStartDate());
            }
            if (date1.getEndDate() != null)
            {
                dates.setEndDate(date1.getEndDate());
            }

        }
        if (date2 != null)
        {
            if (dates.getStartDate() == null)
            {
                dates.setStartDate(date2.getStartDate());
                dates.setEndDate(date2.getEndDate());
            }
            else
            {
                if (date2.getStartDate() != null && dates.getStartDate().after(date2.getStartDate()))
                {
                    dates.setStartDate(date2.getStartDate());
                }
                if (date2.getEndDate() != null && dates.getEndDate().before(date2.getEndDate()))
                {
                    dates.setEndDate(date2.getEndDate());
                }
            }
        }
        return dates;
    }

    /**
     * Create a line to a constituent in a given sheet.
     * 
     * @param sheet
     *            the given sheet.
     * @param constituent
     *            the constituent to add.
     * @return the created row.
     * @throws Hd3dException
     */
    private Row createLine(Sheet sheet, IConstituent constituent) throws Hd3dException
    {
        Row row = sheet.createRow(sheet.getLastRowNum() + 1);
        createNameCell(row, constituent.getLabel());
        return row;
    }

    /**
     * Create a line to a category in a given sheet.
     * 
     * @param sheet
     *            the given sheet.
     * @param category
     *            the category to add.
     * @return the created row.
     * @throws Hd3dException
     */
    private Row createLine(Sheet sheet, ICategory category) throws Hd3dException
    {
        Row row = sheet.createRow(sheet.getLastRowNum() + 1);
        createNameCell(row, "> " + category.getName());
        return row;
    }

    /**
     * Create a line to a shot in a given sheet.
     * 
     * @param sheet
     *            the given sheet.
     * @param shot
     *            the shot to add.
     * @return the created row.
     * @throws Hd3dException
     */
    private Row createLine(Sheet sheet, IShot shot) throws Hd3dException
    {
        Row row = sheet.createRow(sheet.getLastRowNum() + 1);
        createNameCell(row, shot.getLabel());
        return row;
    }

    /**
     * Create a line to a sequence in a given sheet.
     * 
     * @param sheet
     *            the given sheet.
     * @param sequence
     *            the sequence to add.
     * @return the created row.
     * @throws Hd3dException
     */
    private Row createLine(Sheet sheet, ISequence sequence) throws Hd3dException
    {
        Row row = sheet.createRow(sheet.getLastRowNum() + 1);
        createNameCell(row, "> " + sequence.getName());
        return row;
    }

    /**
     * Create a cell bar time to the index, in the color and with the text value.
     * 
     * @param row
     *            the row to create the cell
     * @param indexCell
     *            the index of cell in the row
     * @param color
     *            the color of the cell.
     * @param value
     *            the text value in the cell.
     * @throws Hd3dException
     */
    private void createCellTime(Row row, int indexCell, Short color, String value) throws Hd3dException
    {
        if (row == null || indexCell < 0 || value == null)
        {
            Log.LOGGER
                    .error("PlanningExportOdsResource - createCellTime -> the row is null or the index is negative or the color is null or the value is null: row:"
                            + row + ", indexCell:" + indexCell + ", value:" + value);
            return;
        }
        Cell cell = row.createCell(indexCell);
        CellStyle cellStyle = wb.createCellStyle();
        cell.setCellStyle(cellStyle);
        cell.setCellValue(value);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        cellStyle.setFillForegroundColor(color == null ? HSSFColor.WHITE.index : color);
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

        setAllBorderedStyle(cellStyle);
    }

    /**
     * Create a name cell in the row to set the value text. The value text is indented by level.
     * 
     * @param row
     *            the row to create the name cell.
     * @param value
     *            the value to set in the cell.
     * @return the created cell.
     * @throws Hd3dException
     *             if the row is null
     */
    private Cell createNameCell(Row row, String value) throws Hd3dException
    {
        if (row == null)
        {
            throw new Hd3dException(
                    "PlanningExportOdsResource - createNameCell -> the row is null or the value is null: row:null"
                            + ", value:" + value);
        }
        Cell cell = row.createCell(0);
        value = StringUtils.repeat(INDENTATION_PATTERN, level) + value;
        cell.setCellValue(value);
        return cell;
    }

    /**
     * Create a bar time according the start date and the end date in the indexed color and the value text. It merged
     * the region of start date to the end date.
     * 
     * @param sheet
     *            the given sheet
     * @param row
     *            the given row
     * @param startDate
     *            the start date
     * @param endDate
     *            the end date
     * @param indexColor
     *            the index of color of the foreground
     * @param value
     *            the text value
     * @throws Hd3dException
     *             if the paramaters are incorrect
     */
    private void createBarTime(Sheet sheet, Row row, PairDate dates, Short indexColor, String value)
            throws Hd3dException
    {
        if (row == null || dates == null || dates.getStartDate() == null || dates.getEndDate() == null)
        {
            Log.LOGGER.error("PlanningExportOdsResource - createBarTime -> the start date or the end date is null.");
            return;
        }

        Date planningStart = planning.getStartDate();
        Date planningEnd = planning.getEndDate();

        Duration duration = getNbDaysEstimedAndNbDaysToStart(planningStart, dates.getStartDate(), planningEnd, dates
                .getEndDate());

        CellRangeAddress cellRangeAddress = new CellRangeAddress(row.getRowNum(), row.getRowNum(), duration
                .getNbToStart() + 1, duration.getDuration());

        if (cellRangeAddress.getNumberOfCells() > 0)
        {
            sheet.addMergedRegion(cellRangeAddress);
        }

        createCellTime(row, duration.getNbToStart() + 1, indexColor, value);
    }

    /**
     * Return the duration of the task for the planning
     * 
     * @param planningStart
     * @param startDate
     * @param planningEnd
     * @param endDate
     * @return
     * @throws Hd3dException
     *             if parameter is incorrect
     */
    private Duration getNbDaysEstimedAndNbDaysToStart(Date planningStart, Date startDate, Date planningEnd, Date endDate)
            throws Hd3dException
    {
        if (planningStart == null || startDate == null || planningEnd == null || endDate == null)
        {
            throw new Hd3dException(
                    "PlanningExportOdsResource - getNbDaysEstimedAndNbDaysToStart -> a date is null: planning start:"
                            + planningStart + ", planning end:" + planningEnd + ", start date:" + startDate
                            + ", end date:" + endDate);
        }
        int nbDaysEstimed = getDaysBetweenDates(startDate, endDate) + 1;
        int nbDaysToRemove = 0;
        int nbDaysToStart = 0;
        if (planningStart.before(startDate))
        {
            nbDaysToStart += getDaysBetweenDates(startDate, planningStart);
        }
        else if (planningStart.after(startDate))
        {
            nbDaysToRemove += getDaysBetweenDates(startDate, planningStart);
        }
        if (planningEnd.before(endDate))
        {
            nbDaysToRemove += getDaysBetweenDates(endDate, planningEnd);
        }

        nbDaysEstimed = nbDaysEstimed - nbDaysToRemove;

        return new Duration(nbDaysToStart, nbDaysEstimed);
    }

    /**
     * Set the foreground color style to a given cell.
     * 
     * @param cell
     *            the given cell
     * @param color
     *            the index of color.
     */
    public void setColorForegroundColor(Cell cell, short color)
    {
        CellStyle style = wb.createCellStyle();
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(color);
        cell.setCellStyle(style);
    }
}
