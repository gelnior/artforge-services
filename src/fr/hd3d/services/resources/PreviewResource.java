package fr.hd3d.services.resources;

import java.io.File;

import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.services.resolver.PreviewPathResolver;


/**
 * Preview resource provides access to previews. It takes as argument folder containing images and id of the object
 * associated to the thumbnail. .
 * 
 * @author HD3D
 */
@Hd3dComment("GET: Retrieve the privew corresponding to folder object id given in URL parameter. <br />")
public class PreviewResource extends AbstractHd3dBaseResource
{
    public static String NO_PICTURE_SEGMENT = "no-picture";
    public static long MAX_FILE_UPLOAD = 150000000;

    /**
     * Default constructor : GET and POST Allowed.
     * 
     * @throws Hd3dException
     */
    public PreviewResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("GET");
    }

    /*
     * NOTE: this method override is to fix a bug, do not delete. With the same uri, when the method get(Variant) is
     * called, it succeeds half the time: get(Variant) is called, then get(), then get(Variant), etc...
     */
    public Representation get()
    {
        return get(null);
    }

    /**
     * Return image corresponding to folder and in given in resource parameter (/images/thumbnails/{folder}/{id}.
     * 
     * @see org.restlet.resource.ServerResource#get(org.restlet.representation.Variant)
     */
    @Override
    public Representation get(Variant variant)
    {
        try
        {
            if (getRequest().getOriginalRef().getLastSegment().equals(NO_PICTURE_SEGMENT))
            {
                this.setNoPicFileResponse();
            }
            else if (getRequest().getOriginalRef().getPath().contains(ThumbnailResource.CONSTITUENTS_SEGMENT))
            {
                this.setImageResponse(ThumbnailResource.CONSTITUENTS_PATH, true);
            }
            else if (getRequest().getOriginalRef().getPath().contains(ThumbnailResource.SHOTS_SEGMENT))
            {
                this.setImageResponse(ThumbnailResource.SHOTS_PATH, true);
            }
            else if (getRequest().getOriginalRef().getPath().contains(ThumbnailResource.PERSONS_SEGMENT))
            {
                this.setImageResponse(ThumbnailResource.PERSONS_PATH, false);
            }
            else if (getRequest().getOriginalRef().getPath().contains(ThumbnailResource.PROJECTS_SEGMENT))
            {
                this.setImageResponse(ThumbnailResource.PROJECTS_SEGMENT, false);
            }
            else
            {
                this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
            }
            return getResponseEntity();
        }
        finally
        {
            cleanup();
        }
    }

    /**
     * Build a response that contains image as png format. Absolute path of the image is constitued of hd3d.images.path
     * property + "thumbnails" + given path.
     * 
     * @param path
     * @param isPreview
     */
    private void setImageResponse(String path, boolean isWorkObject)
    {
        String id = getRequest().getOriginalRef().getLastSegment();

        File file = PreviewPathResolver.getFile(path, isWorkObject, id);

        if (file != null && file.exists())
        {
            FileRepresentation fileRepresentation = new FileRepresentation(file, MediaType.IMAGE_PNG);
            getResponse().setEntity(fileRepresentation);
        }
        else
        {
            setNoPicFileResponse();
        }
    }

    /**
     * Build an image response that returns the no-picture thumbnail.
     */
    private void setNoPicFileResponse()
    {
        File noPicFile = new File(PreviewPathResolver.getNoPicPath());
        FileRepresentation fileRepresentation = new FileRepresentation(noPicFile, MediaType.IMAGE_PNG);

        if (noPicFile.exists())
        {
            getResponse().setEntity(fileRepresentation);
        }
        else
        {
            getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
        }
    }

    /**
     * @return null.
     * 
     * @see fr.hd3d.services.resources.AbstractHd3dBaseResource#getNewResourceIdentifier(java.lang.Object)
     */
    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        return null;
    }
}
