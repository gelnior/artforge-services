package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonFilesCmd;
import fr.hd3d.services.security.utils.AuthenticationUtil;


public class MyFilesRevisionResource extends FilesRevisionResource
{

    public MyFilesRevisionResource() throws Hd3dException
    {
        super();
    }

    @Override
    protected ResultCollection<IFileRevision> getCollectionForRepresentation() throws Hd3dException
    {
        IPerson user = AuthenticationUtil.getCurrentUser();
        if (user != null)
        {
            return doGetCollection(new GetPersonFilesCmd(getResourceContext(), user.getId()).wantTotalSize());
        }
        else
        {
            throw new Hd3dException("No user is set for this request, cannot returns asset for current user.");
        }
    }
}
