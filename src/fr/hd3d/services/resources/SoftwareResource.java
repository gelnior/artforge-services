package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.SOFTWARE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSoftware;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single software object (id ford parameter) <br>
 * URI : /softwares/{id}
 * 
 * @author HD3D
 */
@Hd3dComment("GET:Récupération d'un logiciel\r\n" + "\r\nPUT:Modification d'un logiciel\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Désactivation d'un logiciel")
public class SoftwareResource extends AbstractHd3dResource<ILSoftware, ISoftware>
{
    @Override
    protected ISoftware initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(SOFTWARE_ID_ATTRIBUTE, lockMode);
    }

    public SoftwareResource() throws Hd3dException
    {
        super(Persistors.software, Translators.software);
    }
}
