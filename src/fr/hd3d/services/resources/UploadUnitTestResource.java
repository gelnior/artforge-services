package fr.hd3d.services.resources;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.lang.SystemUtils;
import org.restlet.Request;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;


/**
 * @author Nicolas Jauffret
 */

@Hd3dComment("Webservice d'upload de test. Sauvegarde tout fichier reçu dans /tmp.")
public class UploadUnitTestResource<L, P> extends AbstractHd3dBaseResource
{
    protected final static String UPLOAD_DIR = "/tmp";
    protected final static String UPLOAD_FILENAME = "uploadunittest_";

    public UploadUnitTestResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("POST");
    }

    public Representation post(Representation entity, Variant variant)
    {
        if (entity == null)
        {
            return null;
        }

        if (MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true))
        {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            // factory.setSizeThreshold(1000240);

            RestletFileUpload upload = new RestletFileUpload(factory);
            upload.setFileSizeMax(150000000);
            upload.setSizeMax(150000000);

            List<FileItem> items;
            try
            {

                items = upload.parseRequest(Request.getCurrent());

                int cpt = 0;
                for (final Iterator<FileItem> it = items.iterator(); it.hasNext();)
                {
                    System.out.println("Item " + cpt + "\n");

                    FileItem fi = it.next();
                    if (!fi.isFormField())
                    {
                        cpt++;
                        String fileName = UPLOAD_DIR + SystemUtils.FILE_SEPARATOR + UPLOAD_FILENAME + "_"
                                + fi.getFieldName();
                        File file = new File(fileName);
                        fi.write(file);
                        if (file.length() == 0)
                        {
                            file.delete();
                        }
                    }
                }

                Representation rep = null;
                if (cpt != 0)
                {
                    rep = new StringRepresentation("" + cpt + " files successfully uploaded", MediaType.TEXT_PLAIN);
                }
                else
                {
                    // Some problem occurs, sent back a simple line of text.
                    rep = new StringRepresentation("no file uploaded", MediaType.TEXT_PLAIN);
                }
                getResponse().setEntity(rep);
                getResponse().setStatus(Status.SUCCESS_OK);
                return rep;
            }
            catch (Exception e)
            {
                getResponse().setEntity(new StringRepresentation(e.getMessage(), MediaType.TEXT_PLAIN));
                getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
                e.printStackTrace();
                return new StringRepresentation(e.getMessage(), MediaType.TEXT_PLAIN);
            }
        }
        else
        {
            getResponse().setEntity(new StringRepresentation("Bad request", MediaType.TEXT_PLAIN));
            getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            return new StringRepresentation("Bad request", MediaType.TEXT_PLAIN);
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
