package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ASSETREVISIONLINK_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILFileAttribute;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class FileAttributeResource extends AbstractHd3dResource<ILFileAttribute, IFileAttribute>
{

    public FileAttributeResource() throws Hd3dException
    {
        super(Persistors.fileattribute, Translators.fileattribute);
    }

    @Override
    protected IFileAttribute initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ASSETREVISIONLINK_ID_ATTRIBUTE, lockMode);
    }
}
