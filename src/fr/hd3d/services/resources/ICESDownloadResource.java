package fr.hd3d.services.resources;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.restlet.data.MediaType;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.utils.Conf;


/**
 * @author Try LAM
 */
public class ICESDownloadResource<L, P> extends AbstractHd3dBaseResource
{
    protected final static String ICES_OUTPUT_DIR = Conf.getICESOutputDir();

    public ICESDownloadResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("POST");
    }

    /**
     * If id=XXXX is provided in the url, the ICES output file XXXX is returned as a stream. If delete=XXXX is provided
     * in the url, the ICES output file XXXX is deleted.
     * 
     * @param variant
     * @return
     */
    @Override
    public Representation get(Variant variant)
    {
        Representation r = null;

        final String id = getQuery().getValues("id");
        final String delete = getQuery().getValues("delete");

        if (StringUtils.isNotEmpty(id))
        {
            r = new FileRepresentation(new File(ICES_OUTPUT_DIR + SystemUtils.FILE_SEPARATOR + id), new MediaType(
                    "text/3dl"), 0);
            r.setDownloadable(true);
            r.setDownloadName(id);
            return r;
        }
        else if (StringUtils.isNotEmpty(delete))
        {
            File file = new File(ICES_OUTPUT_DIR + SystemUtils.FILE_SEPARATOR + delete);
            file.delete();
            r = new StringRepresentation(delete + " has been removed", MediaType.TEXT_PLAIN);
            return r;
        }
        else
            return null;

    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
