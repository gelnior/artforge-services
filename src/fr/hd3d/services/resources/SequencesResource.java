package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.SEQUENCE_ID_ATTRIBUTE;

import java.util.ArrayList;
import java.util.List;

import org.restlet.representation.Representation;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetSequencesCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.translator.json.JSonFormSerializer;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des sous-sequences de la séquence indiquée\r\n"
        + "\r\nPUT:Changement de nom d'une séquence (ou sous-séquence)\r\n"
        + "\r\nPOST:Ajout d'une SOUS-SEQUENCE par le nom\r\n"
        + "\r\nDELETE:Suppression d'une séquence (ou sous-séquence) et de tous ces fils.")
public class SequencesResource extends AbstractHd3dCollection<ILSequence, ISequence>
{
    public SequencesResource() throws Hd3dException
    {
        super(Persistors.sequence, Translators.sequence);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetSequencesCmd(getResourceContext()).wantTotalSize());
    }

    @Override
    protected ISequence newObject(Representation entity) throws Hd3dException
    {
        ILSequence lsequence = JSonFormSerializer.<ILSequence> deserialize(entity);
        Long sequenceId = getResourceContext().getIdFromUrl(SEQUENCE_ID_ATTRIBUTE);
        lsequence.setParent(sequenceId);
        ISequence sequence = getResourceContext().getTranslator().fromLightweight(lsequence, getResourceContext());

        return sequence;
    }

    @Override
    protected List<ISequence> newCollection(Representation entity) throws Hd3dException
    {
        List<ILSequence> lsequences = JSonFormSerializer.<ILSequence> deserializeList(entity);
        Long sequenceId = getResourceContext().getIdFromUrl(SEQUENCE_ID_ATTRIBUTE);
        List<ISequence> objects = new ArrayList<ISequence>(lsequences.size());
        for (ILSequence s : lsequences)
        {
            s.setParent(sequenceId);
            ISequence sequence = getResourceContext().getTranslator().fromLightweight(s, getResourceContext());
            objects.add(sequence);
        }

        return objects;
    }

    // @Override
    // protected List<ISequence> doPrePersistCollection(Representation entity, Map<String, String> map)
    // throws Hd3dTranslationException
    // {
    // List<ILSequence> lsequences = JSonFormSerializer.<ILSequence> deserializeList(entity);
    // Long sequenceId = getIdFromUrl(SEQUENCE_ID_ATTRIBUTE);
    // List<ISequence> objects = new ArrayList<ISequence>(lsequences.size());
    // for (ILSequence s : lsequences)
    // {
    // s.setParent(sequenceId);
    // ISequence sequence = translator.fromLightweight(s, map);
    // objects.add(sequence);
    //
    // // PrePersist
    // sequence.doPrePersist();
    // executeScripts(sequence, IPersist.PREPERSIST, getNewIdentifier(sequence));
    // }
    //
    // return objects;
    // }

    // @Override
    // protected ISequence doPrePersist(Representation entity, Map<String, String> map) throws Hd3dTranslationException
    // {
    // ILSequence lsequence = JSonFormSerializer.<ILSequence> deserialize(entity);
    // Long sequenceId = getIdFromUrl(SEQUENCE_ID_ATTRIBUTE);
    // lsequence.setParent(sequenceId);
    // ISequence sequence = translator.fromLightweight(lsequence, map);
    //
    // // PrePersist
    // sequence.doPrePersist();
    // executeScripts(sequence, IPersist.PREPERSIST, getNewIdentifier(sequence));
    // return sequence;
    // }
    /**
     * Handle PUT requests. UPDATE
     */
    // @Override
    // public void put(Representation entity)
    // {
    // // Check that the item is not already registered.
    // Response response = getResponse();
    // try
    // {
    // ILSequence lsequence = JSonFormSerializer.<ILSequence> deserialize(new Form(entity));
    // ISequence sequence = objectProvider.getObjectWithId(lsequence.getId());
    // sequence.setName(lsequence.getName()); // only set the name
    //
    // // PreUpdate
    // sequence.doPreUpdate();
    // executeScripts(sequence, IObjectProvider.PREDELETE, getRequest().getResourceRef().toString());
    //
    // // Update
    // objectProvider.mergeObject(sequence);
    //
    // // PostUpdate
    // // Set the response's status and entity
    // response.setStatus(Status.SUCCESS_CREATED);
    // response.setEntity(RessourceSerializer.<ILSequence> getRepresentation(entity.getMediaType(),
    // objectTranslator.toLightweight(sequence)));
    // executeScripts(sequence, IObjectProvider.POSTUPDATE, getRequest().getResourceRef().toString());
    // }
    // catch (Exception e)
    // {
    // error(Status.SERVER_ERROR_INTERNAL, e);
    // }
    // finally
    // {
    // cleanup();
    // }
    // }
}
