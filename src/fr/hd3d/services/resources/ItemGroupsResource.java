package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILItemGroup;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetItemGroupsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des 'itemGroup' qui composent la fiche 'sheetID' \r\n"
        + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'un nouveau 'itemGroup' dans la fiche 'sheetID' (avec, en paramètres, les 'items' qui le composent)\r\n"
        + "\r\nDELETE:N/A")
public class ItemGroupsResource extends AbstractHd3dCollection<ILItemGroup, IItemGroup>
{
    public ItemGroupsResource() throws Hd3dException
    {
        super(Persistors.itemgroup, Translators.itemGroup);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetItemGroupsCmd(getResourceContext()).wantTotalSize());
    }
}
