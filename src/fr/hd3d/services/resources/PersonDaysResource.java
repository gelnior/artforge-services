package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPersonDaysCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author david gauthier
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des jours et les activités du jour d'un utilisateur\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Créer un nouveau jour\r\n" + "\r\nDELETE:N/A")
public class PersonDaysResource extends AbstractHd3dCollection<ILPersonDay, IPersonDay>
{
    public PersonDaysResource() throws Hd3dException
    {
        super(Persistors.personDay, Translators.personDay);
    }

    @Override
    protected ResultCollection<IPersonDay> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetPersonDaysCmd(getResourceContext()).wantTotalSize());
    }
}
