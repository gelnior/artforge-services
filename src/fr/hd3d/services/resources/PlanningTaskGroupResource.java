package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTaskGroup;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPlanningTaskGroupCmd;
import fr.hd3d.model.persistence.impl.hibernate.query.UniqueResultQuery;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.HibernateUtil;


/**
 * @author michael.guiral
 * 
 */
public class PlanningTaskGroupResource extends AbstractHd3dResource<ILTaskGroup, ITaskGroup>
{
    public PlanningTaskGroupResource() throws Hd3dException
    {
        super(Persistors.taskGroup, Translators.taskGroup);
    }

    @Override
    protected void doPreDelete() throws Hd3dException
    {
        super.doPreDelete();
    }

    @Override
    protected ITaskGroup initializeObject(LockMode lockMode) throws Hd3dException
    {
        final UniqueResultQuery<ILTaskGroup, ITaskGroup> cmd = new GetPlanningTaskGroupCmd(getResourceContext());
        HibernateUtil.safeQuery(cmd);
        ITaskGroup obj = (ITaskGroup) cmd.result;
        createDynMetaDataValue(obj);
        return obj;
    }

}
