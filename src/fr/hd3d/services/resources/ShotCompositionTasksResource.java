package fr.hd3d.services.resources;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetWorkObjectCompositionTasksCmd;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.utils.Const;


public class ShotCompositionTasksResource extends AbstractHd3dCollection<ILTask, ITask>
{
    public ShotCompositionTasksResource() throws Hd3dException
    {
        super(Persistors.task, Translators.task);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetWorkObjectCompositionTasksCmd(getResourceContext(),
                ServicesURI.SHOT_ID_ATTRIBUTE, Const.SHOT, Const.CONSTITUENT).wantTotalSize());
    }
}
