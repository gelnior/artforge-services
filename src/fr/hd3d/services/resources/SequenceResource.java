package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.SEQUENCE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste des sous-sequences situées juste en dessous de la séquence indiquée\r\n"
        + "\r\nPUT:Changement de nom d'une séquence (ou sous-séquence)\r\n"
        + "\r\nPOST:Ajout d'une SOUS-SEQUENCE par le nom\r\n"
        + "\r\nDELETE:Suppression d'une séquence (ou sous-séquence) et de tous ses fils.")
public class SequenceResource extends AbstractHd3dResource<ILSequence, ISequence>
{
    public SequenceResource() throws Hd3dException
    {
        super(Persistors.sequence, Translators.sequence);
    }

    @Override
    protected ISequence initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(SEQUENCE_ID_ATTRIBUTE, lockMode);
    }
}
