package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération de la liste de tout les utilisateur\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'un nouvelle utilisateur\r\n" + "\r\nDELETE:N/A")
public class PersonsResource extends AbstractHd3dCollection<ILPerson, IPerson>
{

    public PersonsResource() throws Hd3dException
    {
        super(Persistors.person, Translators.person);
    }
}
