package fr.hd3d.services.resources;

import java.util.List;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILStep;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.visitor.persist.PostPersistVisitor;
import fr.hd3d.model.persistence.impl.hibernate.visitor.update.PostUpdateVisitor;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
public class StepsResource extends AbstractHd3dCollection<ILStep, IStep>
{

    public StepsResource() throws Hd3dException
    {
        super(Persistors.step, Translators.step);
    }

    @Override
    protected void postPersistBulk(List<IStep> objects) throws Hd3dException
    {
        PostPersistVisitor visitor = new PostPersistVisitor();
        visitor.visit(objects);
    }

    @Override
    protected void postUpdateBulk(List<IStep> objects) throws Hd3dException 
    {
        PostUpdateVisitor visitor = new PostUpdateVisitor();
        visitor.visit(objects);
    }
}
