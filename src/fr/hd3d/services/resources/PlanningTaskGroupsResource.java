package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILTaskGroup;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetPlanningTaskGroupsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author michael.guiral
 * 
 */
public class PlanningTaskGroupsResource extends AbstractHd3dCollection<ILTaskGroup, ITaskGroup>
{
    public PlanningTaskGroupsResource() throws Hd3dException
    {
        super(Persistors.taskGroup, Translators.taskGroup);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetPlanningTaskGroupsCmd(getResourceContext()).wantTotalSize());
    }
}
