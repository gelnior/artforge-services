package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.ABSENCE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILAbsence;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET:Récupération d'une absence\r\n" + "\r\nPUT:Changement d'une absence\r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Suppression d'une absence")
public class AbsenceResource extends AbstractHd3dResource<ILAbsence, IAbsence>
{
    public AbsenceResource() throws Hd3dException
    {
        super(Persistors.absence, Translators.absence);
    }

    @Override
    protected IAbsence initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ABSENCE_ID_ATTRIBUTE, lockMode);
    }
}
