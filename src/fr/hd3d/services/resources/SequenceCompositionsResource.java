package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILComposition;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetSequenceCompositionsCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class SequenceCompositionsResource extends AbstractHd3dCollection<ILComposition, IComposition>
{
    public SequenceCompositionsResource() throws Hd3dException
    {
        super(Persistors.composition, Translators.composition);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ResultCollection getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetSequenceCompositionsCmd(getResourceContext()).wantTotalSize());
    }
}
