package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILMileStone;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.HibernatePersist.ResultCollection;
import fr.hd3d.model.persistence.impl.hibernate.query.GetMilestonesCmd;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
@Hd3dComment("GET: Returns milesontes list. \r\n" + "\r\nPUT: N/A\r\n" + "\r\nPOST: Add a milestone. \r\n"
        + "\r\nDELETE: N/A")
public class MileStonesResource extends AbstractHd3dCollection<ILMileStone, IMileStone>
{
    public MileStonesResource() throws Hd3dException
    {
        super(Persistors.mileStone, Translators.mileStone);
    }

    @Override
    protected ResultCollection<IMileStone> getCollectionForRepresentation() throws Hd3dException
    {
        return doGetCollection(new GetMilestonesCmd(getResourceContext()).wantTotalSize());
    }
}
