package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.COMPOSITION_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILComposition;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class CompositionResource extends AbstractHd3dResource<ILComposition, IComposition>
{
    public CompositionResource() throws Hd3dException
    {
        super(Persistors.composition, Translators.composition);
    }

    @Override
    protected IComposition initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(COMPOSITION_ID_ATTRIBUTE, lockMode);
    }
}
