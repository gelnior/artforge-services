package fr.hd3d.services.resources;

import static fr.hd3d.common.client.ServicesURI.SCREEN_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILScreen;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * Restlet resource for accessing to a single software object (id ford parameter) <br>
 * URI : /screens/{id}
 * 
 * @author HD3D
 */
public class ScreenResource extends AbstractHd3dResource<ILScreen, IScreen>
{
    @Override
    protected IScreen initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(SCREEN_ID_ATTRIBUTE, lockMode);
    }

    public ScreenResource() throws Hd3dException
    {
        super(Persistors.screen, Translators.screen);
    }
}
