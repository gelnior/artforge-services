package fr.hd3d.services.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILTag;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * @author Try LAM
 */
@Hd3dComment("GET:Liste des tags existants\r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:Ajout d'un tag\r\n" + "\r\nDELETE:N/A")
public class TagsResource extends AbstractHd3dCollection<ILTag, ITag>
{
    public TagsResource() throws Hd3dException
    {
        super(Persistors.tag, Translators.tag);
    }
}
