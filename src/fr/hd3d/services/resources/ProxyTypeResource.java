package fr.hd3d.services.resources;

import org.hibernate.LockMode;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILProxyType;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;


/**
 * 
 * @author HD3D
 * 
 */
public class ProxyTypeResource extends AbstractHd3dResource<ILProxyType, IProxyType>
{

    public ProxyTypeResource() throws Hd3dException
    {
        super(Persistors.proxytype, Translators.proxytype);
    }

    @Override
    protected IProxyType initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ServicesURI.PROXYTYPE_ID_ATTRIBUTE, lockMode);
    }

}
