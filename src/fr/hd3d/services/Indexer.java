package fr.hd3d.services;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections15.Closure;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.SearchException;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.reader.ReaderProvider;
import org.hibernate.search.store.DirectoryProvider;

import fr.hd3d.common.client.DateFormat;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.Pair;


public class Indexer
{
    private Indexer()
    {}

    public static synchronized void init() throws Hd3dException
    {
        Log.LOGGER.info("INIT INDEXATION...");
        Log.LOGGER.info("BATCH SIZE is:" + HibernateUtil.JDBC_BATCHSIZE);

        /* check if the directory of lucene index does exist... */
        checkDir();

        index();

        Log.LOGGER.info("INDEXATION FINISHED...");
    }

    private static void checkDir() throws Hd3dException
    {
        final String indexDir = HibernateUtil.CFG.getProperty("hibernate.search.default.indexBase");
        final File indexDirFile = new File(indexDir);
        if (!indexDirFile.exists())
        {
            final String error = "Cannot find index directory. Please make sure the directory referenced by "
                    + "hibernate.search.default.indexBase property in hibernate.cfg.xml does exist and has read/write permissions";
            Log.LOGGER.error(error);
            throw new Hd3dException(error);
        }
        if (!indexDirFile.canRead() || !indexDirFile.canWrite())
        {
            final String error = "Index directory " + indexDir + " does not have read/write permissions";
            Log.LOGGER.error(error);
            throw new Hd3dException(error);
        }
    }

    private static void index()
    {
        final Set<String> entities = EntitiesMaps.allEntityClassNames();

        for (String ent : entities)
        {
            /* start indexation */
            try
            {
                final Class<?> classToIndex = getIndexedClass(ent);

                if (classToIndex != null)
                {
                    /* process by slices */
                    final Long countFromDB = getCountFromDB(ent);
                    Log.LOGGER.info("{}: {} occurrences in DB", ent, countFromDB);
                    final int countFromIndex = getCountFromIndex(classToIndex);
                    Log.LOGGER.info("{}: {} occurrences in index", ent, countFromIndex);

                    if (countFromDB < HibernateUtil.JDBC_BATCHSIZE)
                    {
                        process(ent, 0, 0, classToIndex);
                    }
                    else
                    {
                        int i = 0;
                        while (i < countFromDB)
                        {
                            process(ent, i, HibernateUtil.JDBC_BATCHSIZE, classToIndex);
                            i += HibernateUtil.JDBC_BATCHSIZE;
                        }
                        /* remaining */
                        process(ent, i - HibernateUtil.JDBC_BATCHSIZE, HibernateUtil.JDBC_BATCHSIZE, classToIndex);
                    }

                    FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
                    org.hibernate.Transaction tx = fullTextSession.beginTransaction();
                    fullTextSession.getSearchFactory().optimize(classToIndex);
                    fullTextSession.flushToIndexes();
                    tx.commit();
                    tx = null;

                    fullTextSession.close();
                    fullTextSession = null;

                    // updated elements are already handled here above: when an element has changed, its version
                    // is updated. Thus, the old element is removed from the index (the previous version is no more
                    // present in DB) and the new updated element is added in the index just after
                }
            }
            catch (HibernateException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
            catch (IllegalArgumentException e)
            {
                Log.LOGGER.warn(ExceptUtils.format(e));
            }
            catch (SearchException e)
            {
                Log.LOGGER.warn(ExceptUtils.format(e));
            }
        }
    }

    private static void process(final String ent, final Long start, final Long end, final Class<?> classToIndex)
    {
        final List<Pair<Long, String>> entityDB_Id_version = getIdVersionsFromDB(ent, start, end);
        final List<Pair<Long, String>> entityIndex_Id_version = getIdVersionsFromIndex(classToIndex, start, end);

        /* remove from index the elements present in index and not in DB */
        {
            final Collection<Pair<Long, String>> toRemoveFromIndexIds = CollectionUtils.<Pair<Long, String>> subtract(
                    entityIndex_Id_version, entityDB_Id_version);

            Log.LOGGER.info("removing from index...");
            removeFromIndex(toRemoveFromIndexIds, classToIndex);
            toRemoveFromIndexIds.clear();
        }

        /* index the elements present in DB and not in index */
        {
            final Collection<Pair<Long, String>> toAddInIndexIds = CollectionUtils.<Pair<Long, String>> subtract(
                    entityDB_Id_version, entityIndex_Id_version);

            Log.LOGGER.info("indexing DB...");
            indexFromDB(toAddInIndexIds, ent);
            toAddInIndexIds.clear();
        }

        entityDB_Id_version.clear();
        entityIndex_Id_version.clear();
    }

    private static void process(final String ent, final int start, final int nbRow, final Class<?> classToIndex)
    {
        /* collect id, version from DB */
        final List<Pair<Long, String>> dbIdVersion = getIdVersionsFromDB(classToIndex, start, nbRow);

        /* collect id, version from index */
        final List<Pair<Long, String>> indexIdVersion = getIdVersionsFromIndex(classToIndex, collectIds(dbIdVersion));

        /* remove from index the elements present in index and not in DB */
        {
            Collection<Pair<Long, String>> toRemoveFromIndexIds = CollectionUtils.<Pair<Long, String>> subtract(
                    indexIdVersion, dbIdVersion);

            removeFromIndex(toRemoveFromIndexIds, classToIndex);
            toRemoveFromIndexIds.clear();
        }

        /* index the elements present in DB and not in index */
        {
            final Collection<Pair<Long, String>> toAddInIndexIds = CollectionUtils.<Pair<Long, String>> subtract(
                    dbIdVersion, indexIdVersion);

            indexFromDB(toAddInIndexIds, ent);
            toAddInIndexIds.clear();
        }

        dbIdVersion.clear();
        indexIdVersion.clear();
    }

    private static synchronized void purgeAll(final FullTextSession textSession, final String entityClassName)
    {
        if (textSession == null || entityClassName == null)
            return;

        try
        {
            Class<?> clazz = Class.forName(entityClassName);
            textSession.purgeAll(clazz);
            textSession.getSearchFactory().optimize(clazz);
        }
        catch (ClassNotFoundException ce)
        {
            Log.LOGGER.warn(ExceptUtils.format(ce));
        }
    }

    private static synchronized Class<?> getIndexedClass(final String entityClasseName)
    {
        /* if it is not an indexed class, go next */
        Class<?> clazz = null;
        try
        {
            clazz = Class.forName(entityClasseName);
            if (clazz.getAnnotation(Indexed.class) == null)
            {
                return null;
            }
        }
        catch (ClassNotFoundException e)
        {
            return null;
        }

        return clazz;
    }

    private static String getDateString(final Date date)
    {
        /* must use UTC because @DateBridge use to store in the index uses UTC */
        return DateFormatUtils.formatUTC(date, DateFormat.LUCENE_VERSION_DATE_FORMAT);
    }

    @SuppressWarnings( { "serial", "unchecked" })
    private static synchronized List<Pair<Long, String>> getIdVersionsFromDB(final String entity, final Long idFrom,
            final Long idTo)
    {
        String query = "select id, version from " + entity;
        if (idFrom != null && idTo != null)
        {
            List<Long> idsRange = new ArrayList<Long>();
            for (Long i = idFrom; i <= idTo; i++)
            {
                idsRange.add(i);
            }
            query += " where id in (" + StringUtils.join(idsRange, ',') + ")";
        }

        final List<Object[]> entityDB_Id_version = HibernateUtil.currentSession().createQuery(query).list();

        return new ArrayList<Pair<Long, String>>(entityDB_Id_version.size()) {
            {
                for (Object[] obj : entityDB_Id_version)
                    add(new Pair<Long, String>((Long) obj[0], getDateString((Timestamp) obj[1])));
            }
        };
    }

    @SuppressWarnings( { "unchecked", "serial" })
    private static synchronized List<Pair<Long, String>> getIdVersionsFromDB(final Class<?> entity, final int cursor,
            final int nbRow)
    {
        Criteria criteria = HibernateUtil.currentSession().createCriteria(entity);
        criteria.setFirstResult(cursor);
        if (nbRow != 0)
        {
            criteria.setMaxResults(nbRow);
        }
        criteria.setProjection(Projections.projectionList().add(Projections.property(Const.ID)).add(
                Projections.property(Const.VERSION)));

        final List<Object[]> entityDB_Id_version = criteria.list();

        return new ArrayList<Pair<Long, String>>(entityDB_Id_version.size()) {
            {
                for (Object[] obj : entityDB_Id_version)
                    add(new Pair<Long, String>((Long) obj[0], getDateString((Timestamp) obj[1])));
            }
        };
    }

    @SuppressWarnings("unchecked")
    private static Pair<Long, Long> getIdMinMaxFromDB(final String entity)
    {
        final List<Object[]> entityDB_Id_version = HibernateUtil.currentSession().createQuery(
                "select min(id), max(id) from " + entity).list();

        return new Pair<Long, Long>((Long) entityDB_Id_version.get(0)[0], (Long) entityDB_Id_version.get(0)[1]);
    }

    @SuppressWarnings( { "unchecked", "serial" })
    private static synchronized List<Pair<Long, String>> getIdVersionsFromIndex(final Class classToIndex,
            final Long idFrom, final Long idTo)
    {
        Query query;
        if (idFrom != null && idTo != null)
        {
            query = new BooleanQuery();
            for (Long i = idFrom; i <= idTo; i++)
            {
                ((BooleanQuery) query).add(new TermQuery(new Term(Const.ID, String.valueOf(i))),
                        BooleanClause.Occur.SHOULD);
            }
        }
        else
        {
            query = new MatchAllDocsQuery();
        }
        FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
        final List<Object[]> indexIds = fullTextSession.createFullTextQuery(query, classToIndex).setProjection(
                Const.ID, Const.VERSION).list();

        fullTextSession.close();
        fullTextSession = null;

        return new ArrayList<Pair<Long, String>>(indexIds.size()) {
            {
                for (Object[] obj : indexIds)
                    if (obj[1] != null)
                        add(new Pair<Long, String>((Long) obj[0], getDateString((Date) obj[1])));
            }
        };
    }

    @SuppressWarnings( { "unchecked", "serial" })
    private static synchronized List<Pair<Long, String>> getIdVersionsFromIndex(final Class<?> classToIndex,
            final List<Long> ids)
    {
        Query query;

        CollectUtils.removeNull(ids);

        if (CollectUtils.isNotEmpty(ids))
        {
            /* retrieve documents with passed ids */
            query = new BooleanQuery();
            for (Long id : ids)
            {
                ((BooleanQuery) query).add(new TermQuery(new Term(Const.ID, String.valueOf(id))),
                        BooleanClause.Occur.SHOULD);
            }
        }
        else
        {
            /* retrieve all documents */
            query = new MatchAllDocsQuery();
        }

        FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
        final List<Object[]> indexIds = fullTextSession.createFullTextQuery(query, classToIndex).setProjection(
                Const.ID, Const.VERSION).list();

        fullTextSession.close();
        fullTextSession = null;

        return new ArrayList<Pair<Long, String>>(indexIds.size()) {
            {
                for (Object[] obj : indexIds)
                    if (obj[1] != null)
                        add(new Pair<Long, String>((Long) obj[0], getDateString((Date) obj[1])));
            }
        };
    }

    /**
     * return the number of the passed entity in DB
     * 
     * @param entity
     * @return
     */
    private static synchronized Long getCountFromDB(final String entity)
    {
        return (Long) HibernateUtil.currentSession().createQuery("select count(*) from " + entity).uniqueResult();
    }

    /**
     * return the number of the passed entity in lucene index
     * 
     * @param fullTextSession
     * @param classToIndex
     * @return
     */
    @SuppressWarnings("unchecked")
    private static synchronized int getCountFromIndex(final Class classToIndex)
    {
        FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());

        DirectoryProvider entityProvider = fullTextSession.getSearchFactory().getDirectoryProviders(classToIndex)[0];

        ReaderProvider readerProvider = fullTextSession.getSearchFactory().getReaderProvider();
        IndexReader reader = readerProvider.openReader(entityProvider);

        try
        {
            return reader.numDocs();
        }
        finally
        {
            readerProvider.closeReader(reader);
            fullTextSession.close();
            fullTextSession = null;
        }
    }

    private static void removeFromIndex(final Collection<Pair<Long, String>> toRemoveFromIndexIds,
            final Class<?> classToIndex)
    {
        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(toRemoveFromIndexIds))
        {
            FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
            org.hibernate.Transaction tx = fullTextSession.beginTransaction();

            Log.LOGGER.info("Removing elements from lucene index");
            for (Pair<Long, String> tuple : toRemoveFromIndexIds)
            {
                fullTextSession.purge(classToIndex, tuple.getField_1());
            }

            fullTextSession.flushToIndexes();
            tx.commit();
            tx = null;
            fullTextSession.close();
            fullTextSession = null;

            Log.LOGGER.info("Removed {}", StringUtils.join(collectIds(toRemoveFromIndexIds), ','));
        }
    }

    private static void indexFromDB(final Collection<Pair<Long, String>> toAddInIndexIds, final String ent)
    {
        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(toAddInIndexIds))
        {
            CollectUtils.doByPartition(collectIds(toAddInIndexIds), HibernateUtil.JDBC_BATCHSIZE,
                    new IndexFromDBClosure(ent));
        }
    }

    private static class IndexFromDBClosure implements Closure<List<Long>>
    {
        String entityName = null;

        public IndexFromDBClosure(String entityName)
        {
            this.entityName = entityName;
        }

        public void execute(List<Long> idPartitions)
        {
            if (CollectUtils.isNotEmpty(idPartitions))
            {
                /* retrieve entities with given ids */
                String[] query = new String[5];
                query[0] = "from ";
                query[1] = entityName;
                query[2] = " where id in (";
                query[3] = org.apache.commons.lang.StringUtils.join(idPartitions, ',');
                query[4] = ")";

                List<IBase> toAddInIndex = HibernateUtil.currentSession().createQuery(StringUtils.join(query)).list();

                if (CollectUtils.isNotEmpty(toAddInIndex))
                {
                    /* index entities */
                    FullTextSession fullTextSession = Search.getFullTextSession(HibernateUtil.currentSession());
                    org.hibernate.Transaction tx = fullTextSession.beginTransaction();

                    Log.LOGGER.info("Indexing elements from DB");
                    int counter = 0;
                    for (IBase object : toAddInIndex)
                    {
                        fullTextSession.index(object);
                        counter++;
                        if (counter % HibernateUtil.JDBC_BATCHSIZE == 0)
                        {
                            fullTextSession.flushToIndexes();
                        }
                    }

                    fullTextSession.flushToIndexes();
                    tx.commit();
                    tx = null;
                    fullTextSession.close();
                    fullTextSession = null;
                    Log.LOGGER.info("Indexed {}", StringUtils.join(CollectUtils.getIds(toAddInIndex), ','));
                    toAddInIndex.clear();
                }
            }
        }
    }

    @SuppressWarnings("serial")
    private static List<Long> collectIds(final Collection<Pair<Long, String>> toAddInIndexIds)
    {
        return new ArrayList<Long>() {
            {
                for (Pair<Long, String> tuple : toAddInIndexIds)
                    add(tuple.getField_1());
            }
        };
    }
}
