package fr.hd3d.services.security.dao;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.Sha256Hash;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


/**
 * A specification of authorizer that will also play the role of HD3D authentication realm, capable of obtaining all
 * user account informations from the HD3D database.
 * 
 * @author HD3D
 * 
 */
public class Hd3dRealm extends DefaultRealm
{
    private static String lastUsername = null;
    private static AuthenticationInfo lastInfo = null;

    public Hd3dRealm()
    {
        super();
        this.setCredentialsMatcher(new Hd3dCredentialsMatcher());
    }

    public static void resetLastInfo(final String username)
    {
        if (username != null && lastUsername != null && username.equals(lastUsername))
        {
            lastUsername = null;
            lastInfo = null;
        }
    }

    protected Set<String> getPermissionsFromRoles(final Set<IRole> roles)
    {
        final Set<String> permissions = new HashSet<String>();

        CollectUtils.removeNull(roles);
        if (CollectUtils.isNotEmpty(roles))
        {
            for (IRole role : roles)
            {
                permissions.addAll(role.getPermissions());
            }
        }

        return permissions;
    }

    /* Should not be used as Authenticating Realm in the first place (we'll see later) */
    protected AuthenticationInfo doGetAuthenticationInfo(final AuthenticationToken token)
    {
        AuthenticationInfo info = null;

        if (token == null)
        {
            Log.LOGGER.warn("Authentication: token is null.");
        }
        else
        {
            final String login = ((UsernamePasswordToken) token).getUsername();

            /* Mr Nobody? No way! */
            if (login == null)
            {
                Log.LOGGER.info("Authentication: login is null.");
            }
            else
            {
                /* Do not store last connexioninfo: may change (for ex: password change) */
                /* the last guy logged in was you, idiot! */
                if (login.equals(lastUsername))
                {
                    info = lastInfo;
                }
                else
                {
                    info = getAuthenticationInfoFrom(login);

                    lastUsername = login;
                    lastInfo = info;
                }
            }
        }

        return info;
    }

    private AuthenticationInfo getAuthenticationInfoFrom(String login)
    {
        AuthenticationInfo info = null;
        try
        {
            IUserAccount account = Persistors.useraccount.getObjectByValue_NoPermCheck(Const.PRINCIPAL, login);
            if (account == null)
            {
                Log.LOGGER.info("Authentication failed: no user with login=" + login);
                throw new AuthenticationException("Authentication failed: no user with login=" + login);
            }
            /* retrieves user's password */
            final char[] passwordChars = account.getSecret() == null ? null : account.getSecret().toCharArray();
            info = new SimpleAuthenticationInfo(login, passwordChars, getName());
        }
        catch (Hd3dException e1)
        {
            Log.LOGGER.error("Authentication failed: " + ExceptUtils.format(e1));
            throw new AuthenticationException("authentication failed: " + ExceptUtils.format(e1));
        }

        return info;
    }

    /**
     * Default hd3d credentials matcher using -temporary- the <code>FDTAuthUtils.encrypt()</code> method
     * 
     * @author florent-della-valle
     * 
     */
    static class Hd3dCredentialsMatcher extends HashedCredentialsMatcher
    {
        public Hd3dCredentialsMatcher()
        {
            super(Sha256Hash.ALGORITHM_NAME);
            this.setStoredCredentialsHexEncoded(false);
        }
    }

}
