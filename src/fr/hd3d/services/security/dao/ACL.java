package fr.hd3d.services.security.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.search.annotations.Indexed;


/**
 * 
 * @author try.lam
 * 
 */

@Entity
@Indexed
@Table(name = "hd3d_acl")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ACL implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    protected java.sql.Timestamp version;

    private String entityName;
    private String entityIds;
    private String roleAcl;
    private String userAcl;

    public ACL()
    {}

    public ACL(String entityName, String entityIds, String roleAcl, String userAcl)
    {
        super();
        this.entityName = entityName;
        this.entityIds = entityIds;
        this.roleAcl = roleAcl;
        this.userAcl = userAcl;
    }

    @Version
    public java.sql.Timestamp getVersion()
    {
        return version;
    }

    public void setVersion(java.sql.Timestamp version)
    {
        this.version = version;
    }

    @Id
    @Column(name = "acl_entityname")
    public String getEntityName()
    {
        return entityName;
    }

    public void setEntityName(String entityName)
    {
        this.entityName = entityName;
    }

    @Column(name = "acl_entityids")
    public String getEntityIds()
    {
        return entityIds;
    }

    public void setEntityIds(String entityIds)
    {
        this.entityIds = entityIds;
    }

    @Column(name = "acl_roleacl")
    public String getRoleAcl()
    {
        return roleAcl;
    }

    public void setRoleAcl(String roleAcl)
    {
        this.roleAcl = roleAcl;
    }

    @Column(name = "acl_useracl")
    public String getUserAcl()
    {
        return userAcl;
    }

    public void setUserAcl(String userAcl)
    {
        this.userAcl = userAcl;
    }

    public String toString()
    {
        return new ToStringBuilder(this).append("version", version).append("version", version).append("entityName",
                entityName).append("entityIds", entityIds).append("roleAcl", roleAcl).append("userAcl", userAcl)
                .toString();
    }
}
