package fr.hd3d.services.security.dao;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IRole;


@Hd3dEntityName("ACL")
public interface IHd3dACL extends IBase
{
    Set<IRole> getRoles();

    void setRoles(Set<IRole> roles);

    String getEntityName();

    void setEntityName(String entityName);

    String getReadPermitted();

    void setReadPermitted(String read);

    String getCreatePermitted();

    void setCreatePermitted(String create);

    String getUpdatePermitted();

    void setUpdatePermitted(String update);

    String getDeletePermitted();

    void setDeletePermitted(String delete);

    String getReadBans();

    void setReadBans(String readBans);

    String getUpdateBans();

    void setUpdateBans(String updateBans);

    String getDeleteBans();

    void setDeleteBans(String deleteBans);

}
