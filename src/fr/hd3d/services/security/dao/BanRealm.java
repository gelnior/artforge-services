package fr.hd3d.services.security.dao;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;

import fr.hd3d.model.persistence.IRole;
import fr.hd3d.utils.CollectUtils;


/**
 * This realm's purpose is to collect an user's bans.
 * 
 * @author HD3D
 * 
 */
public class BanRealm extends DefaultRealm
{
    protected Set<String> getPermissionsFromRoles(final Set<IRole> roles)
    {
        final Set<String> permissions = new HashSet<String>();

        CollectUtils.removeNull(roles);

        if (CollectUtils.isNotEmpty(roles))
        {
            for (IRole role : roles)
            {
                permissions.addAll(role.getBans());
            }
        }

        return permissions;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(final AuthenticationToken arg0) throws AuthenticationException
    {
        // TODO Auto-generated method stub
        return null;
    }

}
