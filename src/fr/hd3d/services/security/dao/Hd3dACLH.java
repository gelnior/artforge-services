package fr.hd3d.services.security.dao;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.lightweight.ILHd3dACL;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.RoleH;
import fr.hd3d.model.persistence.impl.hibernate.visitor.BasePersistenceVisitor;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.utils.CollectUtils;


@Entity
@Table(name = "hd3d_acl")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class Hd3dACLH extends BaseH implements IHd3dACL
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Set<IRole> roles;
    private String entityName;
    private String readPermitted;
    private String readBans;
    private String createPermitted;
    private String updatePermitted;
    private String updateBans;
    private String deletePermitted;
    private String deleteBans;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_acl"), @Parameter(name = "initial_value", value = "1"),
            @Parameter(name = "increment_size", value = "1"), @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public Hd3dACLH()
    {}

    public Hd3dACLH(Long id, java.sql.Timestamp version, Set<IRole> roles, String entityName, String readPermitted,
            String createPermitted, String updatePermitted, String deletePermitted, String readBans, String updateBans,
            String deleteBans)
    {
        super(id, version);
        this.roles = roles;
        this.entityName = entityName;

        this.readPermitted = readPermitted;
        this.createPermitted = createPermitted;
        this.updatePermitted = updatePermitted;
        this.deletePermitted = deletePermitted;

        this.readBans = readBans;
        this.updateBans = updateBans;
        this.deleteBans = deleteBans;

    }

    public Hd3dACLH(Set<IRole> roles, String entityName, String readPermitted, String createPermitted,
            String updatePermitted, String deletePermitted, String readBans, String updateBans, String deleteBans)
    {
        this(null, null, roles, entityName, readPermitted, createPermitted, updatePermitted, deletePermitted, readBans,
                updateBans, deleteBans);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToMany(targetEntity = RoleH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_acl__role", joinColumns = @JoinColumn(name = "acl_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    @BatchSize(size = 50)
    @Fetch(value = FetchMode.JOIN)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IRole> getRoles()
    {
        if (roles == null)
            roles = new HashSet<IRole>();
        return roles;
    }

    public void setRoles(Set<IRole> roles)
    {
        this.roles = roles;
    }

    @Column(name = "acl_entityname", nullable = false)
    public String getEntityName()
    {
        return entityName;
    }

    public void setEntityName(String entityName)
    {
        this.entityName = entityName;
    }

    @Column(columnDefinition = "TEXT", name = "acl_read_permitted")
    public String getReadPermitted()
    {
        return readPermitted;
    }

    public void setReadPermitted(String allowedToRead)
    {
        this.readPermitted = allowedToRead;
    }

    @Column(columnDefinition = "TEXT", name = "acl_create_permitted")
    public String getCreatePermitted()
    {
        return createPermitted;
    }

    public void setCreatePermitted(String allowedToCreate)
    {
        this.createPermitted = allowedToCreate;
    }

    @Column(columnDefinition = "TEXT", name = "acl_update_permitted")
    public String getUpdatePermitted()
    {
        return updatePermitted;
    }

    public void setUpdatePermitted(String allowedToUpdate)
    {
        this.updatePermitted = allowedToUpdate;
    }

    @Column(columnDefinition = "TEXT", name = "acl_delete_permitted")
    public String getDeletePermitted()
    {
        return deletePermitted;
    }

    public void setDeletePermitted(String allowedToDelete)
    {
        this.deletePermitted = allowedToDelete;
    }

    @Column(columnDefinition = "TEXT", name = "acl_read_bans")
    public String getReadBans()
    {
        return readBans;
    }

    public void setReadBans(String readBans)
    {
        this.readBans = readBans;
    }

    @Column(columnDefinition = "TEXT", name = "acl_update_bans")
    public String getUpdateBans()
    {
        return updateBans;
    }

    public void setUpdateBans(String updateBans)
    {
        this.updateBans = updateBans;
    }

    @Column(columnDefinition = "TEXT", name = "acl_delete_bans")
    public String getDeleteBans()
    {
        return deleteBans;
    }

    public void setDeleteBans(String deleteBans)
    {
        this.deleteBans = deleteBans;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((createPermitted == null) ? 0 : createPermitted.hashCode());
        result = prime * result + ((deleteBans == null) ? 0 : deleteBans.hashCode());
        result = prime * result + ((deletePermitted == null) ? 0 : deletePermitted.hashCode());
        result = prime * result + ((entityName == null) ? 0 : entityName.hashCode());
        result = prime * result + ((readBans == null) ? 0 : readBans.hashCode());
        result = prime * result + ((readPermitted == null) ? 0 : readPermitted.hashCode());
        result = prime * result + ((roles == null) ? 0 : roles.hashCode());
        result = prime * result + ((updateBans == null) ? 0 : updateBans.hashCode());
        result = prime * result + ((updatePermitted == null) ? 0 : updatePermitted.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Hd3dACLH other = (Hd3dACLH) obj;
        if (createPermitted == null)
        {
            if (other.createPermitted != null)
                return false;
        }
        else if (!createPermitted.equals(other.createPermitted))
            return false;
        if (deleteBans == null)
        {
            if (other.deleteBans != null)
                return false;
        }
        else if (!deleteBans.equals(other.deleteBans))
            return false;
        if (deletePermitted == null)
        {
            if (other.deletePermitted != null)
                return false;
        }
        else if (!deletePermitted.equals(other.deletePermitted))
            return false;
        if (entityName == null)
        {
            if (other.entityName != null)
                return false;
        }
        else if (!entityName.equals(other.entityName))
            return false;
        if (readBans == null)
        {
            if (other.readBans != null)
                return false;
        }
        else if (!readBans.equals(other.readBans))
            return false;
        if (readPermitted == null)
        {
            if (other.readPermitted != null)
                return false;
        }
        else if (!readPermitted.equals(other.readPermitted))
            return false;
        if (roles == null)
        {
            if (other.roles != null)
                return false;
        }
        else if (!roles.equals(other.roles))
            return false;
        if (updateBans == null)
        {
            if (other.updateBans != null)
                return false;
        }
        else if (!updateBans.equals(other.updateBans))
            return false;
        if (updatePermitted == null)
        {
            if (other.updatePermitted != null)
                return false;
        }
        else if (!updatePermitted.equals(other.updatePermitted))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    public IBaseTranslator<ILHd3dACL, IHd3dACL> defaultTranslator()
    {
        return Translators.acl;
    }

    @Override
    public void accept(BasePersistenceVisitor visitor) throws Hd3dException
    {
        visitor.visit(this);
    }

    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public String toString()
    {
        return StringUtils.join(CollectUtils.getIds(roles), ',') + ',' + entityName;
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/
}
