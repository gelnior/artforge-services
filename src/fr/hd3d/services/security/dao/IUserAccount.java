package fr.hd3d.services.security.dao;

import fr.hd3d.model.annotation.Hd3dEntityName;
import fr.hd3d.model.persistence.IBase;


/**
 * A user account object for user authentication. It is identified by a <code>principal</code> that is equal to a
 * person's <code>login</code>.
 * 
 * @author HD3D
 * 
 */
@Hd3dEntityName("UserAccount")
public interface IUserAccount extends IBase
{

    public String getSecret();

    public String getPrincipal();

    public Long getLastConnectionDate();

    public void setSecret(String password);

    public void setPrincipal(String login);

    public void setLastConnectionDate(Long dateInMs);

}
