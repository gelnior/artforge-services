package fr.hd3d.services.security.dao;

import java.security.NoSuchAlgorithmException;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.PersonH;
import fr.hd3d.model.persistence.impl.hibernate.ResourceGroupH;
import fr.hd3d.model.persistence.impl.hibernate.RoleH;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.services.security.utils.SecurityProperties;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


/**
 * Contains a query that will initialize either a 'root' user or a 'super_admin' whether the production state is at
 * 'false' or 'true'.
 * 
 * @author florent-della-valle
 * 
 */
public class SuperAccountsInit
{

    private static String ROOT_OLD = SecurityProperties.getRootOldUserName();
    private static String ADMIN_OLD = SecurityProperties.getAdminOldUserName();
    private static String ADMIN_GROUP_OLD = SecurityProperties.getAdminOldGroupName();
    private static String ADMIN_ROLE_OLD = SecurityProperties.getAdminOldRoleName();

    /* copy of the safe hibernate transaction run */
    public static void runRootInit() throws Hd3dPersistenceException
    {
        Session session = HibernateUtil.currentSession();
        Transaction trx = session.beginTransaction();
        HibernateUtil.safeTransaction(new RootUser_Init());
        if (session.getFlushMode() == FlushMode.MANUAL)
        {
            session.flush();
        }
        trx.commit();
        trx = null;
        HibernateUtil.closeSession();
        Log.LOGGER.info("Non-production environment feature: root user initialized");
    }

    private static class RootUser_Init implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            this.session = session;

            /* delete old root or admin account, if any */
            SuperAccountsInit.disableOldUsers(session);

            IUserAccount root = getUserAccount(AuthenticationUtil.ROOT_USER, true);
            root.setSecret(AuthenticationUtil.ROOT_PASS);
            session.save(root);

            IPerson person = getPerson(AuthenticationUtil.ROOT_USER, "Default", "User");
            session.save(person);

            session.flush();
        }

        private IUserAccount getUserAccount(String login, boolean create) throws Hd3dException
        {
            return SuperAccountsInit.getUserAccount(session, login, create);
        }

        private IPerson getPerson(String login, String first, String last) throws Hd3dException
        {
            /* Retrieve Person. */
            IPerson object = (IPerson) Persistors.person.getObjectByValue_NoPermCheck(Const.LOGIN, login);

            /* If person is in database, return person. */
            if (object != null)
            {
                return object;
            }

            /* Else create a new person instance. */
            object = new PersonH(login, first, last, null, null, null, null, null);

            /* Save person in database. */
            object.doPrePersist();
            session.save(object);
            session.flush();
            object.doPostPersist();
            return object;
        }
    }

    // copy of the safe hibernate transaction run
    public static void runSuperAdminInit() throws Hd3dPersistenceException
    {
        Session session = HibernateUtil.currentSession();
        Transaction trx = session.beginTransaction();
        HibernateUtil.safeTransaction(new SuperAdmin_Init());
        if (session.getFlushMode() == FlushMode.MANUAL)
        {
            session.flush();
        }
        trx.commit();
        trx = null;
        HibernateUtil.closeSession();
        Log.LOGGER.info("Production environment feature: admin user initialized");
    }

    private static class SuperAdmin_Init implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            this.session = session;

            /* delete old root or admin account, if any */
            SuperAccountsInit.disableOldUsers(session);

            /* delete old resource group and role, if any */
            disableOldGroupAndRole();

            IPerson admin = getPerson(AuthenticationUtil.ADMIN_USER, "admin", "adminpass");
            admin.setResourceGroups(null);
            session.save(admin);
            IResourceGroup group = getResourceGroup(AuthenticationUtil.ADMIN_GROUP, true);
            group.setChildren(null);
            group.setParent(null);
            group.setProjects(null);
            group.setResources(null);
            group.setRoles(null);
            group.addPerson(admin);
            group.setLeader(admin);
            session.save(group);
            IRole role = getRole(AuthenticationUtil.ADMIN_ROLE, true);
            role.setPermissions(null);
            role.setResourceGroups(null);
            role.add("*");
            role.add(group);
            session.save(role);
            IUserAccount aAdmin = getUserAccount(AuthenticationUtil.ADMIN_USER, true);
            if (aAdmin.getSecret() == null)
            {
                aAdmin.setSecret(AuthenticationUtil.ADMIN_PASS);
            }
            session.save(aAdmin);
            session.flush();
        }

        private IUserAccount getUserAccount(String login, boolean create) throws Hd3dException
        {
            return SuperAccountsInit.getUserAccount(session, login, create);
        }

        private IPerson getPerson(String login, String first, String last) throws Hd3dException
        {
            /* Retrieve Person. */
            IPerson object = (IPerson) Persistors.person.getObjectByValue_NoPermCheck(Const.LOGIN, login);

            /* If person is in database, return person. */
            if (object != null)
            {
                return object;
            }

            /* Else create a new person instance. */
            object = new PersonH(login, first, last, null, null, null, null, null);

            /* Save person in database. */
            object.doPrePersist();
            session.save(object);
            session.flush();
            object.doPostPersist();
            return object;
        }

        private IResourceGroup getResourceGroup(String name, boolean create) throws Hd3dException
        {

            /* Retrieve project. */
            IResourceGroup object = (IResourceGroup) Persistors.resourcegroup.getObjectByValue_NoPermCheck(Const.NAME,
                    name);

            /* If project is in database, return project. */
            if (object != null)
            {
                return object;
            }
            if (create)
            {
                /* Else create a new project instance. */
                object = new ResourceGroupH(name, null, null, null, null, null, null);

                /* Save project in database. */
                object.doPrePersist();
                session.save(object);
                session.flush();
                object.doPostPersist();
            }
            return object;
        }

        private IRole getRole(String name, boolean create) throws Hd3dException
        {

            /* Retrieve role. */
            IRole object = (IRole) Persistors.role.getObjectByValue_NoPermCheck(Const.NAME, name);

            /* If project is in database, return project. */
            if (object != null)
            {
                return object;
            }
            if (create)
            {
                /* Else create a new role instance. */
                object = new RoleH(name, null, null, null);

                /* Save project in database. */
                object.doPrePersist();
                session.save(object);
                session.flush();
                object.doPostPersist();
            }
            return object;
        }

        private void disableOldGroupAndRole() throws Hd3dException
        {
            if (ADMIN_GROUP_OLD != null)
            {
                IResourceGroup gr = getResourceGroup(ADMIN_GROUP_OLD, false);
                if (gr != null)
                {
                    session.delete(gr);
                }
            }
            if (ADMIN_ROLE_OLD != null)
            {
                IRole ro = getRole(ADMIN_ROLE_OLD, false);
                if (ro != null)
                {
                    session.delete(ro);
                }
            }
        }
    }

    private static void disableOldUsers(Session session) throws Hd3dException
    {
        /* delete old root */
        if (ROOT_OLD != null)
        {
            IUserAccount old = getUserAccount(session, ROOT_OLD, false);
            if (old != null)
            {
                session.delete(old);
            }
            IPerson usr = (IPerson) Persistors.person.getObjectByValue_NoPermCheck(Const.LOGIN, ROOT_OLD);
            if (usr != null)
            {
                session.delete(usr);
            }
        }
        /* delete old admin */
        if (ADMIN_OLD != null)
        {
            IUserAccount old = getUserAccount(session, ADMIN_OLD, false);
            if (old != null)
            {
                session.delete(old);
            }
            IPerson usr = (IPerson) Persistors.person.getObjectByValue_NoPermCheck(Const.LOGIN, ADMIN_OLD);
            if (usr != null)
            {
                session.delete(usr);
            }
        }
    }

    // copy of the getPerson() method
    private static IUserAccount getUserAccount(Session session, String login, boolean create) throws Hd3dException
    {
        /* Retrieve the user account. */
        IUserAccount object = (IUserAccount) Persistors.useraccount
                .getObjectByValue_NoPermCheck(Const.PRINCIPAL, login);

        /* If the user account is in database, return it. */
        if (object != null)
        {
            return object;
        }

        if (create)
        {
            /* Else create a new user account instance. */
            object = new UserAccountH(login);

            /* Save user account in database. */
            object.doPrePersist();
            session.save(object);
            session.flush();
            object.doPostPersist();
        }
        return object;
    }
}
