package fr.hd3d.services.security.dao;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.services.security.utils.AuthorizationUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.Log;


/**
 * This realm is managed by Shiro, but has direct access to Hibernate-persisted objects. It should be granted specific
 * rights for accessing only to the "hd3d_person" table and then to the data contained in the groups, roles and
 * permissions in a read-only mode.
 * 
 * @author HD3D
 * 
 */
public abstract class DefaultRealm extends AuthorizingRealm
{
    public static final String NONE = "none";
    public static final String MSG_1 = "authorization cache {0} cleared";
    public static final String MSG_2 = "{0}: user permissions required while no user is identified";
    public static final String MSG_3 = "{0}: user is identified but has no person object attached";
    public static final String MSG_4 = "cache manager set for {0}";
    public static final String MSG_5 = "cache set: {0}";
    public static final String MSG_6 = "authorization failed: principals cannot be null";
    public static final String MSG_7 = "authorization failed: no unique user can be identified";

    public DefaultRealm()
    {
        super();
        setPermissionResolver(AuthorizationUtil.getPermissionResolver());
        /* TODO: set a cachemanager (ehcache) later */
        setCachingEnabled(true);
        setCacheManager(new MemoryConstrainedCacheManager());
    }

    /**
     * We might need to clear the cache upon permission modification. This is quite gross, but might prove cheaper than
     * selectively remove the entries corresponding to the affected roles and users.
     */
    public void clearCache()
    {
        final Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
        if (cache != null)
        {
            cache.clear();
            Log.LOGGER.info(MessageFormat.format(MSG_1, getAuthorizationCacheName()));
        }
    }

    /**
     * Very dirty method that allows to read the permissions of a user from the context.
     * 
     * @param principals
     *            a subjet principal collection
     */
    public Collection<String> getPermissions(final PrincipalCollection principals)
    {
        Collection<String> ret;

        if (principals == null)
        {
            ret = getEmptyInfo();
        }
        else
        {
            try
            {
                final AuthorizationInfo info = getAuthorizationInfo(principals);
                if (info == null)
                {
                    ret = getEmptyInfo();
                }
                else
                {
                    ret = info.getStringPermissions();
                }
            }
            catch (java.lang.NullPointerException e)
            {
                Log.LOGGER.warn(MessageFormat.format(MSG_2, getName()));
                ret = getEmptyInfo();
            }
            catch (AuthorizationException ae)
            {
                Log.LOGGER.warn(MessageFormat.format(MSG_3, getName()));
                ret = getEmptyInfo();
            }
        }

        return ret;
    }

    private Set<String> getEmptyInfo()
    {
        final Set<String> ret = new HashSet<String>();
        ret.add(NONE);
        return ret;
    }

    @Override
    protected void afterCacheManagerSet()
    {
        Log.LOGGER.info(MessageFormat.format(MSG_4, getName()));
        super.afterCacheManagerSet();
    }

    @Override
    public void clearCachedAuthorizationInfo(PrincipalCollection principals)
    {
        Cache<Object, AuthorizationInfo> authCache = getAuthorizationCache();
        if (authCache != null)
        {
            authCache.clear();
        }
    }

    /**
     * Called by super class AuthorizingRealm getAuthorizationInfo. Must return a AuthorizationInfo.
     */
    protected AuthorizationInfo doGetAuthorizationInfo(final PrincipalCollection principals)
    {
        if (principals == null)
        {
            throw new AuthorizationException(MSG_6);
        }

        final String login = (String) getAvailablePrincipal(principals);

        IPerson person;
        try
        {
            person = Persistors.person.getObjectByValue_NoPermCheck(Const.LOGIN, login);
            if (person == null)
            {
                throw new AuthorizationException(MSG_7);
            }
        }
        catch (Hd3dException e)
        {
            throw new AuthorizationException(MSG_7);
        }

        /* retrieves user's roles and roles names */
        final Set<IRole> roles = new HashSet<IRole>();
        final Set<String> rolesNames = new HashSet<String>();

        final Set<IResourceGroup> groups = person.getResourceGroups();
        CollectUtils.removeNull(groups);
        if (CollectUtils.isNotEmpty(groups))
        {
            for (IResourceGroup group : groups)
            {
                final Set<IRole> groupRoles = group.getRoles();
                CollectUtils.removeNull(groupRoles);
                if (CollectUtils.isNotEmpty(groupRoles))
                {
                    for (IRole role : groupRoles)
                    {
                        roles.add(role);
                        rolesNames.add(role.getName());
                    }
                }
            }
        }

        final SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(rolesNames);
        info.setStringPermissions(getPermissionsFromRoles(roles));

        return info;
    }

    /**
     * Gets the proper informations from an user's roles: permissions or bans
     * 
     * @param roles
     *            an user's role
     * @return a set of permissions or bans
     */
    protected abstract Set<String> getPermissionsFromRoles(Set<IRole> roles);

    // default override - ok
    protected abstract AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken arg0)
            throws AuthenticationException;
}
