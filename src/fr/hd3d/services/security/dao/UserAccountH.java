package fr.hd3d.services.security.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.services.security.customs.Customs;


@Entity
@Table(name = "hd3d_user_account")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class UserAccountH extends BaseH implements IUserAccount
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String principal;
    private String secret;
    private Long lastConnectionDate;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_useraccount"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public UserAccountH()
    {}

    public UserAccountH(String login)
    {
        super();
        principal = login;
        lastConnectionDate = NumberUtils.LONG_ZERO;
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @Column(name = "principal")
    /*
     * Note: DO NOT USE NaturalId, as an Id it must be unique and will prevent from having several same login (one
     * disabled and one active)
     */
    // @NaturalId(mutable = true)
    @Index(name = "principal_idx")
    public String getPrincipal()
    {
        return principal;
    }

    @Column(name = "secret")
    public String getSecret()
    {
        return secret;
    }

    @Column(name = "lastConnectionDate")
    public Long getLastConnectionDate()
    {
        return lastConnectionDate;
    }

    public void setPrincipal(String login)
    {
        principal = login;
    }

    public void setSecret(String password)
    {
        secret = password;
    }

    public void setLastConnectionDate(Long dateInMs)
    {
        lastConnectionDate = dateInMs;
    }

    /*------------------
     * equal & hashcode
     ------------------*/
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((principal == null) ? 0 : principal.hashCode());
        result = prime * result + ((secret == null) ? 0 : secret.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserAccountH other = (UserAccountH) obj;
        if (principal == null)
        {
            if (other.principal != null)
                return false;
        }
        else if (!principal.equals(other.principal))
            return false;
        if (secret == null)
        {
            if (other.secret != null)
                return false;
        }
        else if (!secret.equals(other.secret))
            return false;
        return true;
    }

    /*------------------
     * Transient methods
     ------------------*/
    @Override
    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Override
    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    public IBaseTranslator<?, ?> defaultTranslator()
    {
        return null;
    }

    protected boolean accept(Customs customs)
    {
        // TODO find sthg better
        return false;
    }

    public String defaultPath()
    {
        // No lightweight representation
        return null;
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
