package fr.hd3d.services.security.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.pam.FirstSuccessfulStrategy;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.Authorizer;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.CacheManagerAware;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;

import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


/**
 * This is the kind of 'multi-realm' interface we can use in order to plug at the same time several realms handling
 * authentication, ONE handling permission (that could be extended to several realms also, but it would certainly
 * complicate the permission management...) and ONE handling the bans.
 * 
 * @author HD3D
 * 
 */
public class RealmSwitcher extends ModularRealmAuthenticator implements Realm, CacheManagerAware
{

    /** a realm info map to keep track of external authentication realms */
    private static Map<String, AuthenticatingRealm> realmInfo = new ConcurrentHashMap<String, AuthenticatingRealm>();

    /** the permission realm */
    private static Authorizer authorized;
    /** the ban realm */
    private static Authorizer banned;

    public RealmSwitcher()
    {
        super();

        hd3dDefaultInit();
    }

    /**
     * Default initialization in absence of .config file and for testing purpose uses a Hibernate-aware realm to perform
     * both authentication and authorization
     */
    protected void hd3dDefaultInit()
    {
        /*
         * AddonAuthzRealm uses Hibernate to get authc and authz infos. Here it comes without cache nor digester
         */
        final AuthenticatingRealm hd3dRealm = new Hd3dRealm();
        final Authorizer banRealm = new BanRealm();

        /* attaches the realm to the switcher for authc and authz as well */
        // AUTHENTICATING realm.
        setRealms(new ArrayList() {
            {
                add(hd3dRealm);
            }
        });
        Log.LOGGER.info("- authenticating realm initialized");

        authorized = hd3dRealm;// AUTHORIZING realm
        Log.LOGGER.info("- authorizing realm initialized");

        banned = banRealm;// BAN realm
        Log.LOGGER.info("- banning realm initialized");

        setAuthenticationStrategy(new FirstSuccessfulStrategy());
    }

    /* REALM INFO */

    /**
     * Gets the authenticating realm with given name.
     * 
     * @param name
     *            a realm name
     * @return the named realm
     */
    public static AuthenticatingRealm getRealm(String name)
    {
        return realmInfo.get(name);
    }

    /**
     * Gets all the external authenticating realms.
     * 
     * @return the ext. authenticating realms collection
     */
    public static Collection<AuthenticatingRealm> getExternalRealms()
    {
        return realmInfo.values();
    }

    /* CACHE UTILITIES */

    /**
     * Clears the authorizing realm's cache.
     */
    public void clearCache()
    {
        if (authorized instanceof DefaultRealm)
        {
            ((DefaultRealm) authorized).clearCache();
            /* TODO: ugly */
            ((DefaultRealm) authorized).clearCachedAuthorizationInfo(null);
        }
        if (banned instanceof DefaultRealm)
        {
            ((DefaultRealm) banned).clearCache();
            /* TODO: ugly */
            ((DefaultRealm) authorized).clearCachedAuthorizationInfo(null);
        }

    }

    /**
     * Gets an user's permission list
     * 
     * @param subject
     *            the current user
     * @return the current user's permissions
     */
    public Collection<String> getPermissions(Subject subject)
    {
        if (authorized instanceof DefaultRealm)
        {
            return ((DefaultRealm) authorized).getPermissions(subject.getPrincipals());
        }
        return new ArrayList<String>();
    }

    /**
     * Gets an user's ban list
     * 
     * @param subject
     *            the current user
     * @return the current user's bans
     */
    public Collection<String> getBans(Subject subject)
    {
        if (banned instanceof DefaultRealm)
        {
            return ((DefaultRealm) banned).getPermissions(subject.getPrincipals());
        }
        return new ArrayList<String>();
    }

    public void setCacheManager(CacheManager cacheManager)
    {
        if (authorized instanceof CacheManagerAware)
        {
            ((CacheManagerAware) authorized).setCacheManager(cacheManager);
        }
        if (banned instanceof CacheManagerAware)
        {
            ((CacheManagerAware) banned).setCacheManager(cacheManager);
        }
    }

    /* SWAP SUBJECT */

    private int swapSubject()
    {
        // Subject current = SecurityUtils.getSubject();
        //
        // // this isn't a good test, but the only one I could come up with
        // if (current instanceof SpecialSubject)
        // throw new ShiroException("What the foo is goin' on, here?");
        //
        // Integer key = SpecialSubject.addInstance(current);
        // ((DefaultSecurityManager) ThreadContext.getSecurityManager()).getSubjectBinder().bind(
        // SpecialSubject.getInstance(key));
        // return key;
        return 0;
    }

    private void recoverSubject(int key)
    {
        // Subject current = SpecialSubject.removeInstance(key);
        // ((DefaultSecurityManager) ThreadContext.getSecurityManager()).getSubjectBinder().bind(current);
        return;
    }

    /* IMPLEMENTING... */
    /**
     * Adds a secondary authenticating realm and puts it in the realm info map.
     * 
     * @param an
     *            authenticating realm
     */
    public void addExternalAuthenticator(AuthenticatingRealm shiroAuthc)
    {
        getRealms().add(shiroAuthc);
        realmInfo.put(shiroAuthc.getName(), shiroAuthc);
    }

    public AuthenticationInfo getAuthenticationInfo(AuthenticationToken arg0) throws AuthenticationException
    {
        int key = swapSubject();
        try
        {
            return doAuthenticate(arg0);
        }
        catch (UnknownAccountException e)
        {
            Log.LOGGER.info("Authentication failed.");
            return null;
        }
        catch (Exception e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            return null;
        }
        finally
        {
            recoverSubject(key);
        }
    }

    public String getName()
    {
        return getClass().getName();
    }

    public boolean supports(AuthenticationToken token)
    {
        return token != null;
    }

    public void checkPermission(PrincipalCollection arg0, String arg1) throws AuthorizationException
    {
        int key = swapSubject();
        try
        {
            authorized.checkPermission(arg0, arg1);
        }
        catch (AuthorizationException ae)
        {
            throw ae;
        }
        finally
        {
            recoverSubject(key);
        }
    }

    public void checkPermission(PrincipalCollection arg0, Permission arg1) throws AuthorizationException
    {
        int key = swapSubject();
        try
        {
            authorized.checkPermission(arg0, arg1);
        }
        finally
        {
            recoverSubject(key);
        }
    }

    public void checkPermissions(PrincipalCollection arg0, String... arg1) throws AuthorizationException
    {
        int key = swapSubject();
        try
        {
            authorized.checkPermissions(arg0, arg1);
        }
        finally
        {
            recoverSubject(key);
        }
    }

    public void checkPermissions(PrincipalCollection arg0, Collection<Permission> arg1) throws AuthorizationException
    {
        int key = swapSubject();
        try
        {
            authorized.checkPermissions(arg0, arg1);
        }
        finally
        {
            recoverSubject(key);
        }
    }

    public void checkRole(PrincipalCollection arg0, String arg1) throws AuthorizationException
    {
        int key = swapSubject();
        try
        {
            authorized.checkRole(arg0, arg1);
        }
        finally
        {
            recoverSubject(key);
        }
    }

    public void checkRoles(PrincipalCollection arg0, Collection<String> arg1) throws AuthorizationException
    {
        int key = swapSubject();
        try
        {
            authorized.checkRoles(arg0, arg1);
        }
        finally
        {
            recoverSubject(key);
        }
    }

    public void checkRoles(PrincipalCollection arg0, String... arg1) throws AuthorizationException
    {
        for (String role : arg1)
        {
            checkRole(arg0, role);
        }
    }

    public boolean hasAllRoles(PrincipalCollection arg0, Collection<String> arg1)
    {
        int key = swapSubject();
        boolean answer = authorized.hasAllRoles(arg0, arg1);
        recoverSubject(key);
        return answer;
    }

    public boolean hasRole(PrincipalCollection arg0, String arg1)
    {
        int key = swapSubject();
        boolean answer = authorized.hasRole(arg0, arg1);
        recoverSubject(key);
        return answer;
    }

    public boolean[] hasRoles(PrincipalCollection arg0, List<String> arg1)
    {
        int key = swapSubject();
        boolean[] answer = authorized.hasRoles(arg0, arg1);
        recoverSubject(key);
        return answer;
    }

    public boolean isPermitted(PrincipalCollection arg0, String arg1)
    {
        int key = swapSubject();
        boolean isPermitted = authorized.isPermitted(arg0, arg1);
        boolean isBanned = banned.isPermitted(arg0, arg1);
        boolean answer = isPermitted && !isBanned;
        recoverSubject(key);
        return answer;
    }

    public boolean isPermitted(PrincipalCollection arg0, Permission arg1)
    {
        int key = swapSubject();
        boolean isPermitted = authorized.isPermitted(arg0, arg1);
        boolean isBanned = banned.isPermitted(arg0, arg1);
        boolean answer = isPermitted && !isBanned;
        recoverSubject(key);
        return answer;
    }

    public boolean[] isPermitted(PrincipalCollection arg0, String... arg1)
    {
        int key = swapSubject();
        boolean[] answer = authorized.isPermitted(arg0, arg1);
        boolean[] no = banned.isPermitted(arg0, arg1);
        for (int k = 0; k < answer.length; k++)
        {
            answer[k] = answer[k] && !no[k];
        }
        recoverSubject(key);
        return answer;
    }

    public boolean[] isPermitted(PrincipalCollection arg0, List<Permission> arg1)
    {
        int key = swapSubject();
        boolean[] answer = authorized.isPermitted(arg0, arg1);
        boolean[] no = banned.isPermitted(arg0, arg1);
        for (int k = 0; k < answer.length; k++)
        {
            answer[k] = answer[k] && !no[k];
        }
        recoverSubject(key);
        return answer;
    }

    public boolean isPermittedAll(PrincipalCollection arg0, String... arg1)
    {
        int key = swapSubject();
        boolean answer = authorized.isPermittedAll(arg0, arg1) && !banned.isPermittedAll(arg0, arg1);
        recoverSubject(key);
        return answer;
    }

    public boolean isPermittedAll(PrincipalCollection arg0, Collection<Permission> arg1)
    {
        int key = swapSubject();
        boolean answer = authorized.isPermittedAll(arg0, arg1) && !banned.isPermittedAll(arg0, arg1);
        recoverSubject(key);
        return answer;
    }

}
