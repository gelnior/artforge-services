package fr.hd3d.services.security.utils;

import java.util.Collection;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.StringUtils;
import org.restlet.Response;
import org.restlet.data.Cookie;
import org.restlet.data.CookieSetting;

import fr.hd3d.utils.Const;


public class CookieUtil
{
    private final static String COOKIE_PATH = SecurityProperties.getCookiePath();
    private final static String COOKIE_NAME = SecurityProperties.getCookieName();

    // time utilities

    private CookieUtil()
    {}

    public static CookieSetting getNewCookieSetting(final String value, final boolean remember, final Long rememberTTL)
    {
        CookieSetting cookieSetting;
        cookieSetting = new CookieSetting(0, COOKIE_NAME, value);
        if (remember)
        {
            cookieSetting.setMaxAge((int) (rememberTTL / Const.SECOND));
        }
        cookieSetting.setPath(COOKIE_PATH);

        return cookieSetting;
    }

    private static CookieSetting getExpirationCookieSetting()
    {
        CookieSetting cookieSetting;
        cookieSetting = new CookieSetting(0, COOKIE_NAME, "ce message s'auto-détruira dans 5 secondes");
        cookieSetting.setMaxAge(0);
        cookieSetting.setPath(COOKIE_PATH);

        return cookieSetting;
    }

    /**
     * Deletes the existing authentication cookie in client browser.
     * 
     * @param response
     */
    public static void deleteCookie(final Response response)
    {
        if (response != null)
        {
            response.getCookieSettings().add(getExpirationCookieSetting());
        }
    }

    /**
     * Refreshes the specified cookie in client browser.
     * 
     * @param response
     * @param cookie
     *            the client cookie (usually the authentication cookie)
     * @param remember
     *            whether the cookie lasts for a session only or longer
     */
    public static void refreshCookie(final Response response, final Cookie cookie, final boolean remember,
            final Long rememberTTL)
    {
        if (cookie != null && response != null)
        {
            response.getCookieSettings().add(getRefreshedCookieSetting(cookie, remember, rememberTTL));
        }
    }

    private static CookieSetting getRefreshedCookieSetting(final Cookie cookie, final boolean remember,
            final Long rememberTTL)
    {
        CookieSetting cookieSetting = null;

        if (cookie != null)
        {
            cookieSetting = new CookieSetting(cookie.getVersion(), cookie.getName(), cookie.getValue());
            if (remember)
            {
                cookieSetting.setMaxAge((int) (rememberTTL / Const.SECOND));
            }
            cookieSetting.setPath(COOKIE_PATH);
        }

        return cookieSetting;
    }

    public static Cookie getAuthCookie(Collection<Cookie> cookies)
    {
        return CollectionUtils.find(cookies, new Predicate<Cookie>() {
            public boolean evaluate(Cookie cookie)
            {
                return COOKIE_NAME.equals(cookie.getName())
                        && COOKIE_PATH.equals(StringUtils.defaultIfEmpty(cookie.getPath(), "/"));
            }
        });
    }
}
