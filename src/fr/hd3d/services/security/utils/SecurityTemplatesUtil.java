package fr.hd3d.services.security.utils;

import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.BaseH;
import fr.hd3d.model.persistence.impl.hibernate.RoleH;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.services.security.templates.IProjectSecurityTemplate;
import fr.hd3d.services.security.templates.IRoleTemplate;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


/**
 * This class will contain all the hibernate transactions and transformations that will be necessary to create a
 * security template for a newly created project.
 * 
 * @author HD3D
 * 
 */
public class SecurityTemplatesUtil
{

    public final static String PROJECT_ID_MARK = "{projectId}";

    /**
     * Updates the rights of a role from a role model, inserting a project ID into it
     * 
     * @param rights
     * @param projectId
     * @return
     */
    @SuppressWarnings("serial")
    public static Set<String> updateRightsWithProjectId(final Set<String> rights, final Long projectId)
    {
        if (CollectUtils.isEmpty(rights) || BaseH.isNotValidId(projectId))
            return null;

        return new HashSet<String>(rights.size()) {
            {
                for (String right : rights)
                    add(right.replace(PROJECT_ID_MARK, String.valueOf(projectId)));
            }
        };
    }

    /**
     * Apply a project security template to a given project, creating all the roles for that project.
     * 
     * @param project
     * @param template
     */
    public static void applyTemplate(IProject project, IProjectSecurityTemplate template)
    {
        if (project == null || template == null)
            return;

        Long id = project.getId();
        String name = project.getName();

        Session session = HibernateUtil.currentSession();
        Transaction trx = session.beginTransaction();

        try
        {
            HibernateUtil.safeTransaction(new TemplateExecutor(id, name, template));
            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            trx.commit();
            Log.LOGGER.info("Security template " + template.getName() + " successfully applied to project " + name);
        }
        catch (Exception e)
        {
            trx.rollback();
            Log.LOGGER.error("Security template " + template.getName() + " could not be applied to project " + name);
        }
        finally
        {
            trx = null;
            // audit(session, project, "OTHER", name + " has been applied the template: " + template.getName());
            HibernateUtil.closeSession();
        }
    }

    private static class TemplateExecutor implements IHibernateTransaction
    {
        private Session session;

        private Long id;
        private String name;
        private IProjectSecurityTemplate template;

        public TemplateExecutor(Long projectId, String projectName, IProjectSecurityTemplate template)
        {
            id = projectId;
            name = projectName;
            this.template = template;
        }

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            this.session = session;

            IRole role;
            String roleName;
            for (IRoleTemplate roleTemplate : template.getRoles())
            {
                roleName = new StringBuilder(name).append(" - ").append(roleTemplate.getName()).toString();
                role = getRole(roleName);
                roleTemplate.updateFromTemplate(id, role);
                role.doPreUpdate();
                session.save(role);
                role.doPostUpdate();
                // audit(session, role, Audit.UPDATE, role.getName() + " has been applied the template "
                // + roleTemplate.getName());
            }

            session.flush();
        }

        // copy of the getPerson() method
        private IRole getRole(String name) throws Hd3dException
        {
            // Retrieve the security template.
            IRole object = (IRole) session.createCriteria(Persistors.role.getPersistedClass()).add(
                    Restrictions.eq(Const.NAME, name)).uniqueResult();

            // If the security template is in database, return it.
            if (object != null)
            {
                return object;
            }

            // Else create a new security template instance.
            object = new RoleH(name, null, null, null);

            // Save security template in database.
            object.doPrePersist();
            session.save(object);
            // audit(session, object, Audit.CREATE, "applying a security template to project " + this.name);
            session.flush();
            return object;
        }
    }

    // private static void audit(Session session, IBase object, String operation, String message)
    // {
    // Audit audit = new Audit();
    // IPerson person = AuthenticationUtil.getCurrentUser();
    // if (person != null)
    // {
    // audit.setPerson(person.getId());
    // audit.setLogin(person.getLogin());
    // audit.setFirstName(person.getFirstName());
    // audit.setLastName(person.getLastName());
    // }
    // audit.setDateMs((new Date()).getTime());
    // audit.setEntityName(object.entityName());
    // audit.setEntityId(object.getId());
    // audit.setOperation(operation);
    // audit.setModification(message);
    // audit.flushModification();
    // session.save(audit);
    // session.flush();
    // }

}
