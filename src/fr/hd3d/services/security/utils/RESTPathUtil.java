package fr.hd3d.services.security.utils;

import static fr.hd3d.common.client.ServicesURI.ABSENCES;
import static fr.hd3d.common.client.ServicesURI.ACLS;
import static fr.hd3d.common.client.ServicesURI.ACTIVITIES;
import static fr.hd3d.common.client.ServicesURI.APPROVALNOTES;
import static fr.hd3d.common.client.ServicesURI.APPROVALNOTETYPES;
import static fr.hd3d.common.client.ServicesURI.ASSETABSTRACTS;
import static fr.hd3d.common.client.ServicesURI.ASSETREVISIONGROUPS;
import static fr.hd3d.common.client.ServicesURI.ASSETREVISIONLINKS;
import static fr.hd3d.common.client.ServicesURI.ASSETREVISIONS;
import static fr.hd3d.common.client.ServicesURI.CATEGORIES;
import static fr.hd3d.common.client.ServicesURI.CLASSDYNMETADATATYPES;
import static fr.hd3d.common.client.ServicesURI.COMPOSITIONS;
import static fr.hd3d.common.client.ServicesURI.COMPUTERS;
import static fr.hd3d.common.client.ServicesURI.COMPUTER_MODELS;
import static fr.hd3d.common.client.ServicesURI.COMPUTER_TYPES;
import static fr.hd3d.common.client.ServicesURI.CONSTITUENTLINKS;
import static fr.hd3d.common.client.ServicesURI.CONSTITUENTS;
import static fr.hd3d.common.client.ServicesURI.CONTRACTS;
import static fr.hd3d.common.client.ServicesURI.DAYS_AND_ACTIVITIES;
import static fr.hd3d.common.client.ServicesURI.DEVICES;
import static fr.hd3d.common.client.ServicesURI.DEVICE_MODELS;
import static fr.hd3d.common.client.ServicesURI.DEVICE_TYPES;
import static fr.hd3d.common.client.ServicesURI.DYNAMIC_PATHS;
import static fr.hd3d.common.client.ServicesURI.DYNMETADATATYPES;
import static fr.hd3d.common.client.ServicesURI.DYNMETADATAVALUES;
import static fr.hd3d.common.client.ServicesURI.ENTITYTASKLINKS;
import static fr.hd3d.common.client.ServicesURI.EVENTS;
import static fr.hd3d.common.client.ServicesURI.EXTRA_LINES;
import static fr.hd3d.common.client.ServicesURI.FILEREVISIONLINKS;
import static fr.hd3d.common.client.ServicesURI.FILEREVISIONS;
import static fr.hd3d.common.client.ServicesURI.FILEREVISION_ANNOTATIONS;
import static fr.hd3d.common.client.ServicesURI.FILE_ATTRIBUTES;
import static fr.hd3d.common.client.ServicesURI.GRAPHICAL_ANNOTATIONS;
import static fr.hd3d.common.client.ServicesURI.ITEMGROUPS;
import static fr.hd3d.common.client.ServicesURI.ITEMS;
import static fr.hd3d.common.client.ServicesURI.LICENSES;
import static fr.hd3d.common.client.ServicesURI.LISTS_VALUES;
import static fr.hd3d.common.client.ServicesURI.MANUFACTURERS;
import static fr.hd3d.common.client.ServicesURI.MILESTONES;
import static fr.hd3d.common.client.ServicesURI.MOUNTPOINTS;
import static fr.hd3d.common.client.ServicesURI.ORGANIZATIONS;
import static fr.hd3d.common.client.ServicesURI.PERSONS;
import static fr.hd3d.common.client.ServicesURI.PLANNINGS;
import static fr.hd3d.common.client.ServicesURI.PLAYLISTEDITS;
import static fr.hd3d.common.client.ServicesURI.PLAYLISTREVISIONGROUPS;
import static fr.hd3d.common.client.ServicesURI.PLAYLISTREVISIONS;
import static fr.hd3d.common.client.ServicesURI.POOLS;
import static fr.hd3d.common.client.ServicesURI.PROCESSORS;
import static fr.hd3d.common.client.ServicesURI.PROJECTS;
import static fr.hd3d.common.client.ServicesURI.PROJECT_TYPES;
import static fr.hd3d.common.client.ServicesURI.PROXIES;
import static fr.hd3d.common.client.ServicesURI.PROXYTYPES;
import static fr.hd3d.common.client.ServicesURI.QUALIFICATIONS;
import static fr.hd3d.common.client.ServicesURI.RESOURCEGROUPS;
import static fr.hd3d.common.client.ServicesURI.ROLES;
import static fr.hd3d.common.client.ServicesURI.ROOMS;
import static fr.hd3d.common.client.ServicesURI.SCREENS;
import static fr.hd3d.common.client.ServicesURI.SCREEN_MODELS;
import static fr.hd3d.common.client.ServicesURI.SCRIPTS;
import static fr.hd3d.common.client.ServicesURI.SECURITY_TEMPLATES;
import static fr.hd3d.common.client.ServicesURI.SEQUENCES;
import static fr.hd3d.common.client.ServicesURI.SHEETS;
import static fr.hd3d.common.client.ServicesURI.SHOTS;
import static fr.hd3d.common.client.ServicesURI.SKILLLEVELS;
import static fr.hd3d.common.client.ServicesURI.SKILLS;
import static fr.hd3d.common.client.ServicesURI.SOFTWARES;
import static fr.hd3d.common.client.ServicesURI.STEPS;
import static fr.hd3d.common.client.ServicesURI.STUDIOMAPS;
import static fr.hd3d.common.client.ServicesURI.TAGS;
import static fr.hd3d.common.client.ServicesURI.TAG_CATEGORIES;
import static fr.hd3d.common.client.ServicesURI.TASKS;
import static fr.hd3d.common.client.ServicesURI.TASK_CHANGES;
import static fr.hd3d.common.client.ServicesURI.TASK_GROUPS;
import static fr.hd3d.common.client.ServicesURI.TASK_TYPES;
import static fr.hd3d.common.client.ServicesURI.TEMPLATES;
import static fr.hd3d.common.client.ServicesURI.WORKING_TIMES;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IAssetAbstract;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IComputerModel;
import fr.hd3d.model.persistence.IComputerType;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IConstituentLink;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.IDeviceModel;
import fr.hd3d.model.persistence.IDeviceType;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IEvent;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IFact;
import fr.hd3d.model.persistence.IFactType;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IFileRevisionAnnotation;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.IHint;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.IManufacturer;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.IOrganization;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.IProcessor;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.IQualification;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.IRoom;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.IScreenModel;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ISkill;
import fr.hd3d.model.persistence.ISkillLevel;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.services.security.templates.IProjectSecurityTemplate;
import fr.hd3d.services.security.templates.IRoleTemplate;


/**
 * Returns a default resource path for a given object.
 * 
 * @author HD3D
 * 
 */
public class RESTPathUtil
{

    private static <P extends IBase> String buildPath(String s, P object)
    {
        StringBuilder buf = new StringBuilder(100);
        buf.append(s).append('/').append(object.getId()).trimToSize();
        return buf.toString();
    }

    public static String getDefaultPath(IAbsence object)
    {
        return buildPath('/' + ABSENCES, object);
    }

    public static String getDefaultPath(IActivity object)
    {
        return buildPath('/' + ACTIVITIES, object);
    }

    public static String getDefaultPath(IApprovalNote object)
    {
        return buildPath('/' + APPROVALNOTES, object);
    }

    public static String getDefaultPath(IApprovalNoteType object)
    {
        return buildPath('/' + APPROVALNOTETYPES, object);
    }

    public static String getDefaultPath(IAssetAbstract object)
    {
        // TODO find how to complete the path...
        String path = '/' + ASSETABSTRACTS + object.getKey();
        String var = object.getVariation();
        if (var != null)
        {
            path += "+" + object.getVariation();
        }
        return path;
    }

    public static String getDefaultPath(IAssetRevision object)
    {
        return buildPath('/' + ASSETREVISIONS, object);
        // // TODO find how to complete the path...
        // String path = ASSETABSTRACTS + object.getKey() + "/";
        // String var = object.getVariation();
        // if (var != null)
        // {
        // path += "+" + object.getVariation();
        // }
        // path += ASSETREVISIONS + "/" + object.getId();
        // return path;
    }

    // public static String getDefaultPath(IAssetRevisionFileRevision object)
    // {
    // return buildPath('/' + ASSETREVISIONFILEREVISIONS, object);
    // }

    public static String getDefaultPath(IAssetRevisionGroup object)
    {
        String path = buildPath('/' + ASSETREVISIONGROUPS, object);
        IProject project = object.getProject();
        if (project != null)
        {
            String prefix = getDefaultPath(project);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return path;
    }

    public static String getDefaultPath(IAssetRevisionLink object)
    {
        return buildPath('/' + ASSETREVISIONLINKS, object);
    }

    public static String getDefaultPath(ICategory object)
    {
        String path = buildPath('/' + CATEGORIES, object);
        IProject project = object.getProject();
        if (project != null)
        {
            String prefix = getDefaultPath(project);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return null;
    }

    public static String getDefaultPath(IClassDynMetaDataType object)
    {
        return buildPath('/' + CLASSDYNMETADATATYPES, object);
    }

    public static String getDefaultPath(IComposition object)
    {
        String path = buildPath('/' + COMPOSITIONS, object);
        IConstituent constituent = object.getConstituent();
        if (constituent != null)
        {
            String prefix = getDefaultPath(constituent);
            return prefix + path;
        }
        return null;
    }

    public static String getDefaultPath(IComputer object)
    {
        return buildPath('/' + COMPUTERS, object);
    }

    public static String getDefaultPath(IComputerModel object)
    {
        return buildPath('/' + COMPUTER_MODELS, object);
    }

    public static String getDefaultPath(IComputerType object)
    {
        return buildPath('/' + COMPUTER_TYPES, object);
    }

    public static String getDefaultPath(IConstituent object)
    {
        String path = buildPath('/' + CONSTITUENTS, object);
        ICategory category = object.getCategory();
        if (category != null)
        {
            String prefix = getDefaultPath(category);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return path;
    }

    public static String getDefaultPath(IConstituentLink object)
    {
        return buildPath('/' + CONSTITUENTLINKS, object);
    }

    public static String getDefaultPath(IContract object)
    {
        return buildPath('/' + CONTRACTS, object);
    }

    public static String getDefaultPath(IDevice object)
    {
        return buildPath('/' + DEVICES, object);
    }

    public static String getDefaultPath(IDeviceModel object)
    {
        return buildPath('/' + DEVICE_MODELS, object);
    }

    public static String getDefaultPath(IDeviceType object)
    {
        return buildPath('/' + DEVICE_TYPES, object);
    }

    public static String getDefaultPath(IDynMetaDataType object)
    {
        return buildPath('/' + DYNMETADATATYPES, object);
    }

    public static String getDefaultPath(IDynMetaDataValue object)
    {
        return buildPath('/' + DYNMETADATAVALUES, object);
    }

    public static String getDefaultPath(IEntityTaskLink object)
    {
        return buildPath('/' + ENTITYTASKLINKS, object);
    }

    public static String getDefaultPath(IEvent object)
    {
        return buildPath('/' + EVENTS, object);
    }

    public static String getDefaultPath(IExtraLine object)
    {
        // FIXME no specific resource
        String path = buildPath('/' + EXTRA_LINES, object);
        ITaskGroup taskGroup = object.getTaskGroup();
        if (taskGroup != null)
        {
            String prefix = getDefaultPath(taskGroup);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return null;
    }

    public static String getDefaultPath(IFact object)
    {
        // FIXME no resource
        return null;
    }

    public static String getDefaultPath(IFactType object)
    {
        // FIXME no resource
        return null;
    }

    public static String getDefaultPath(IFileAttribute object)
    {
        return buildPath('/' + FILE_ATTRIBUTES, object);
    }

    public static String getDefaultPath(IFileRevision object)
    {
        return buildPath('/' + FILEREVISIONS, object);
    }

    public static String getDefaultPath(IFileRevisionLink object)
    {
        return buildPath('/' + FILEREVISIONLINKS, object);
    }

    public static String getDefaultPath(IHint object)
    {
        // FIXME no resource
        return null;
    }

    public static String getDefaultPath(IItem object)
    {
        String path = buildPath('/' + ITEMS, object);
        IItemGroup itemGroup = object.getItemGroup();
        if (itemGroup != null)
        {
            String prefix = getDefaultPath(itemGroup);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return null;
    }

    public static String getDefaultPath(IItemGroup object)
    {
        String path = buildPath('/' + ITEMGROUPS, object);
        ISheet parent = object.getSheet();
        if (parent != null)
        {
            String prefix = getDefaultPath(parent);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return null;
    }

    public static String getDefaultPath(ILicense object)
    {
        return buildPath('/' + LICENSES, object);
    }

    public static String getDefaultPath(IManufacturer object)
    {
        return buildPath('/' + MANUFACTURERS, object);
    }

    public static String getDefaultPath(IMileStone object)
    {
        return buildPath('/' + MILESTONES, object);
    }

    public static String getDefaultPath(IOrganization object)
    {
        return buildPath('/' + ORGANIZATIONS, object);
    }

    public static String getDefaultPath(IPersonDay object)
    {
        String path = buildPath('/' + DAYS_AND_ACTIVITIES, object);
        IPerson parent = object.getPerson();
        if (parent != null)
        {
            String prefix = getDefaultPath(parent);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return path;
    }

    public static String getDefaultPath(IPerson object)
    {
        return buildPath('/' + PERSONS, object);
    }

    public static String getDefaultPath(IWorkingTime object)
    {
        return buildPath('/' + WORKING_TIMES, object);
    }

    public static String getDefaultPath(IPlanning object)
    {
        String path = buildPath('/' + PLANNINGS, object);

        IProject parent = object.getProject();
        if (parent != null)
        {
            String prefix = getDefaultPath(parent);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return null;
    }

    public static String getDefaultPath(IPool object)
    {
        return buildPath('/' + POOLS, object);
    }

    public static String getDefaultPath(IProcessor object)
    {
        return buildPath('/' + PROCESSORS, object);
    }

    public static String getDefaultPath(IProject object)
    {
        return buildPath('/' + PROJECTS, object);
    }

    public static String getDefaultPath(IQualification object)
    {
        return buildPath('/' + QUALIFICATIONS, object);
    }

    public static String getDefaultPath(IRole object)
    {
        return buildPath('/' + ROLES, object);
    }

    public static String getDefaultPath(IRoleTemplate object)
    {
        return buildPath('/' + SECURITY_TEMPLATES + '/' + ROLES, object);
    }

    public static String getDefaultPath(IRoom object)
    {
        return buildPath('/' + ROOMS, object);
    }

    public static String getDefaultPath(IResourceGroup object)
    {
        return buildPath('/' + RESOURCEGROUPS, object);
    }

    public static String getDefaultPath(IScreen object)
    {
        return buildPath('/' + SCREENS, object);
    }

    public static String getDefaultPath(IScreenModel object)
    {
        return buildPath('/' + SCREEN_MODELS, object);
    }

    public static String getDefaultPath(IScript object)
    {
        return buildPath('/' + SCRIPTS, object);
    }

    public static String getDefaultPath(IProjectSecurityTemplate object)
    {
        return buildPath('/' + SECURITY_TEMPLATES + '/' + TEMPLATES, object);
    }

    public static String getDefaultPath(ISequence object)
    {
        String path = buildPath('/' + SEQUENCES, object);
        IProject parent = object.getProject();
        if (parent != null)
        {
            String prefix = getDefaultPath(parent);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return null;
    }

    public static String getDefaultPath(ISheet object)
    {
        // FIXME fix resource url
        return buildPath('/' + SHEETS, object);
    }

    public static String getDefaultPath(IShot object)
    {
        String path = buildPath('/' + SHOTS, object);
        ISequence parent = object.getSequence();
        if (parent != null)
        {
            String prefix = getDefaultPath(parent);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return null;
    }

    public static String getDefaultPath(ISimpleActivity object)
    {
        String path = buildPath('/' + ACTIVITIES, object);
        IPerson parent = object.getWorker();
        IProject parent2 = object.getProject();
        if (parent != null)
        {
            String prefix = getDefaultPath(parent);
            if (prefix != null)
            {
                path = prefix + path;
                if (parent2 != null)
                {
                    prefix = getDefaultPath(parent2);
                    if (prefix != null)
                    {
                        return prefix + path;
                    }
                }
            }
            return path;
        }
        if (parent2 != null)
        {
            String prefix = getDefaultPath(parent2);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return path;
    }

    public static String getDefaultPath(ISkill object)
    {
        return buildPath('/' + SKILLS, object);
    }

    public static String getDefaultPath(ISkillLevel object)
    {
        return buildPath('/' + SKILLLEVELS, object);
    }

    public static String getDefaultPath(ISoftware object)
    {
        return buildPath('/' + SOFTWARES, object);
    }

    public static String getDefaultPath(IStudioMap object)
    {
        return buildPath('/' + STUDIOMAPS, object);
    }

    public static String getDefaultPath(ITag object)
    {
        return buildPath('/' + TAGS, object);
    }

    public static String getDefaultPath(ITagCategory object)
    {
        return buildPath('/' + TAG_CATEGORIES, object);
    }

    public static String getDefaultPath(ITask object)
    {
        String path = buildPath('/' + TASKS, object);
        IPerson parent = object.getWorker();
        IProject parent2 = object.getProject();
        if (parent != null)
        {
            String prefix = getDefaultPath(parent);
            if (prefix != null)
            {
                path = prefix + path;
                if (parent2 != null)
                {
                    prefix = getDefaultPath(parent2);
                    if (prefix != null)
                    {
                        return prefix + path;
                    }
                }
            }
            return path;
        }
        if (parent2 != null)
        {
            String prefix = getDefaultPath(parent2);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return path;
    }

    public static String getDefaultPath(ITaskActivity object)
    {
        String path = buildPath('/' + ACTIVITIES, object);
        IPerson parent = object.getWorker();
        IProject parent2 = object.getProject();
        if (parent != null)
        {
            String prefix = getDefaultPath(parent);
            if (prefix != null)
            {
                path = prefix + path;
                if (parent2 != null)
                {
                    prefix = getDefaultPath(parent2);
                    if (prefix != null)
                    {
                        return prefix + path;
                    }
                }
            }
            return path;
        }
        if (parent2 != null)
        {
            String prefix = getDefaultPath(parent2);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return path;
    }

    public static String getDefaultPath(ITaskChanges object)
    {
        String path = buildPath('/' + TASK_CHANGES, object);
        IPlanning parent = object.getPlanning();
        if (parent != null)
        {
            String prefix = getDefaultPath(parent);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return null;
    }

    public static String getDefaultPath(ITaskGroup object)
    {
        String path = buildPath('/' + TASK_GROUPS, object);
        IPlanning parent = object.getPlanning();
        if (parent != null)
        {
            String prefix = getDefaultPath(parent);
            if (prefix != null)
            {
                return prefix + path;
            }
        }
        return null;
    }

    public static String getDefaultPath(ITaskType object)
    {
        return buildPath('/' + TASK_TYPES, object);
    }

    public static String getDefaultPath(IProjectType object)
    {
        return buildPath('/' + PROJECT_TYPES, object);
    }

    public static String getDefaultPath(IStep object)
    {
        return buildPath('/' + STEPS, object);
    }

    public static String getDefaultPath(IPlayListRevision object)
    {
        return buildPath('/' + PLAYLISTREVISIONS, object);
    }

    public static String getDefaultPath(IPlayListEdit object)
    {
        return buildPath('/' + PLAYLISTEDITS, object);
    }

    public static String getDefaultPath(IFileRevisionAnnotation object)
    {
        return buildPath('/' + FILEREVISION_ANNOTATIONS, object);
    }

    public static String getDefaultPath(IListValues object)
    {
        return buildPath('/' + LISTS_VALUES, object);
    }

    public static String getDefaultPath(IDynamicPath object)
    {
        return buildPath('/' + DYNAMIC_PATHS, object);
    }

    public static String getDefaultPath(IGraphicalAnnotation object)
    {
        return buildPath('/' + GRAPHICAL_ANNOTATIONS, object);
    }

    public static String getDefaultPath(IMountPoint object)
    {
        return buildPath('/' + MOUNTPOINTS, object);
    }

    public static String getDefaultPath(IProxyType object)
    {
        return buildPath('/' + PROXYTYPES, object);
    }

    public static String getDefaultPath(IProxy object)
    {
        return buildPath('/' + PROXIES, object);
    }

	public static String getDefaultPath(IPlayListRevisionGroup object)
    {
        return buildPath('/' + PLAYLISTREVISIONGROUPS, object);
    }
    
    public static String getDefaultPath(IHd3dACL object)
    {
        return buildPath('/' + ACLS, object);
    }
}
