package fr.hd3d.services.security.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


/**
 * Security properties can be accessed to using this class.
 * 
 * @author HD3D
 * 
 */
public class SecurityProperties
{

    public final static String CONF_FILE = "security.properties";

    // security properties list
    // user accounts
    public final static String DEFAULT_PASS = "default_password";
    // root user initialization
    public final static String ROOT_USER = "root.username";
    public final static String ROOT_PASS = "root.password";
    public final static String ROOT_OLD = "root.old_username";
    // admin user initialization
    public final static String ADMIN_USER = "admin.username";
    public final static String ADMIN_PASS = "admin.password";
    public final static String ADMIN_GROUP = "admin.group.name";
    public final static String ADMIN_ROLE = "admin.role.name";
    public final static String ADMIN_OLD = "admin.old_username";
    public final static String ADMIN_GROUP_OLD = "admin.group.old_name";
    public final static String ADMIN_ROLE_OLD = "admin.role.old_name";
    // cookie properties
    public final static String REMEMBERME_ATTRIBUTE = "cookie.remember_me_attribute";
    public final static String COOKIE_PATH = "cookie.path";
    public final static String COOKIE_NAME = "cookie.name";
    // time to live
    public static final String TTL = "cookie.TTL";
    public static final String REMEMBER_TTL = "cookie.remember_me_TTL";
    // LDAP realm
    public static final String LDAP_ON = "realm.ldap.on";
    public static final String LDAP_NAME = "realm.ldap.name";
    // JDBC realm
    public static final String JDBC_ON = "realm.ldap.on";
    public static final String JDBC_NAME = "realm.ldap.name";

    public final static Properties properties = new Properties();

    static
    {
        init(properties, CONF_FILE);
    }

    public static void init(Properties props, String fileName)
    {
        InputStream stream = null;
        stream = readProperties(fileName);
        if (stream == null)
        {
            stream = readProperties("/" + fileName);
        }
        if (stream == null)
        {
            throw new Hd3dRuntimeException(fileName + " not found");
        }
        try
        {
            props.load(stream);
        }
        catch (IOException exception)
        {
            Log.LOGGER.error(ExceptUtils.format(exception));
        }
        finally
        {
            try
            {
                if (stream != null)
                {
                    stream.close();
                }
            }
            catch (IOException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
        }
    }

    public static InputStream readProperties(String path)
    {
        InputStream stream = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if (classLoader != null)
        {
            stream = classLoader.getResourceAsStream(path);
        }
        if (stream == null)
        {
            stream = Conf.class.getResourceAsStream(path);
        }
        if (stream == null)
        {
            stream = Conf.class.getClassLoader().getResourceAsStream(path);
        }
        return stream;
    }

    public static String getProperty(String propertyName, String defaultValue)
    {
        if (properties == null)
        {
            throw new RuntimeException("No security properties file was found!");
        }
        String result = properties.getProperty(propertyName);
        if (result == null)
        {
            Log.LOGGER.warn("/!\\ No security property was found for '" + propertyName
                    + "', server will use default value '" + defaultValue + "' instead");
            result = defaultValue;
        }
        return result;
    }

    public static String getDefaultPassword()
    {
        return getProperty(DEFAULT_PASS, "1234");
    }

    public static String getRootUserName()
    {
        return getProperty(ROOT_USER, "root");
    }

    public static String getRootPassword()
    {
        return getProperty(ROOT_PASS, "rootpass");
    }

    public static String getRootOldUserName()
    {
        return properties.getProperty(ROOT_OLD);
    }

    public static String getAdminUserName()
    {
        return getProperty(ADMIN_USER, "admin");
    }

    public static String getAdminPassword()
    {
        return getProperty(ADMIN_PASS, "adminpass");
    }

    public static String getAdminGroupName()
    {
        return getProperty(ADMIN_GROUP, "Super Admins");
    }

    public static String getAdminRoleName()
    {
        return getProperty(ADMIN_ROLE, "super_admin");
    }

    public static String getAdminOldUserName()
    {
        return properties.getProperty(ADMIN_OLD);
    }

    public static String getCookieRememberMe()
    {
        return getProperty(REMEMBERME_ATTRIBUTE, "remember_me");
    }

    public static String getCookieName()
    {
        return getProperty(COOKIE_NAME, "hd3d_auth_cookie");
    }

    public static String getCookiePath()
    {
        return getProperty(COOKIE_PATH, "/");
    }

    public static int getTimeToLive()
    {
        String ttl = properties.getProperty(TTL);
        if (ttl == null)
        {
            return 90; // 90 minutes for default ttl
        }
        Long l = Long.valueOf(ttl);
        return l.intValue();
    }

    public static int getRememberTimeToLive()
    {
        String ttl = properties.getProperty(REMEMBER_TTL);
        if (ttl == null)
        {
            return 7; // 7 days for default remember_me_ttl
        }
        Long l = Long.valueOf(ttl);
        return l.intValue();
    }

    public static String getAdminOldGroupName()
    {
        return properties.getProperty(ADMIN_GROUP_OLD);
    }

    public static String getAdminOldRoleName()
    {
        return properties.getProperty(ADMIN_ROLE_OLD);
    }

    /* EXTERNAL DATA REALMS PROPERTIES */

    public static boolean isLDAPRealmOn()
    {
        return "true".equals(properties.getProperty(LDAP_ON));
    }

    public static String getLDAPRealmName()
    {
        return getProperty(LDAP_NAME, "ldap_name");
    }

    public static boolean isJDBCRealmOn()
    {
        return "true".equals(properties.getProperty(JDBC_ON));
    }

    public static String getJDBCRealmName()
    {
        return getProperty(JDBC_NAME, "jdbc_name");
    }

    // TODO Add all other useful properties for external realms...
}
