package fr.hd3d.services.security.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.services.auth.FDTAuthUtils;
import fr.hd3d.services.security.dao.IUserAccount;
import fr.hd3d.services.security.dao.RealmSwitcher;
import fr.hd3d.services.security.dao.UserAccountH;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


/**
 * This utility class is meant to handle database CRUD operations on user accounts, since they are not treated like
 * other HD3D domain objects.
 * 
 * @author HD3D
 * 
 */
public final class UserAccountsUtil
{
    public final static String DEFAULT_PASS = SecurityProperties.getDefaultPassword();
    public final static String DEFAULT_SECRET = FDTAuthUtils.encrypt(DEFAULT_PASS);

    /*
     * the account status. Warning: if changed, do not forget to pass on modifications to the ADMIN's client module
     * AdminConfig class
     */
    public final static Long ACCOUNT_OK = 0L;
    public final static Long ACCOUNT_MISSING = -1L;
    public final static Long ACCOUNT_DEFAULTPASSWORD = -2L;
    public final static Long ACCOUNT_NULLPASSWORD = -3L; // should not happen
    public final static Long ACCOUNT_MULTIPLE = -4L; // should not happen
    public final static Long ACCOUNT_EXTERNAL = -5L;

    public static boolean isDefaultSecret(String password)
    {
        return UserAccountsUtil.DEFAULT_SECRET.equals(password);
    }

    public static boolean isNotDefaultSecret(String password)
    {
        return !isDefaultSecret(password);
    }

    /**
     * Creates an account for specified user name. The new user account's principal will be the user name itself.
     * 
     * @param username
     * @param password
     * @throws Hd3dPersistenceException
     */
    public static void createAccount(String username, String password) throws Hd3dException
    {
        List<IUserAccount> persons = retrieveAccounts(username);
        if (CollectUtils.isNotEmpty(persons))
        {
            throw new Hd3dPersistenceException("user " + username + " already exists");
        }
        IUserAccount user = new UserAccountH(username);
        user.setSecret(FDTAuthUtils.encrypt(password));
        password = null; // to protect memory

        user.doPrePersist();
        Persistors.useraccount.persist(user);
        user.doPostPersist();

        // audit(HibernateUtil.currentSession(), user, Audit.CREATE, ": new user account has been created");
    }

    /**
     * Tries to delete the account with specified user name as principal.
     * 
     * @param username
     * @throws Hd3dPersistenceException
     */
    public static void deleteAccount(String username) throws Hd3dException
    {

        List<IUserAccount> userAccounts = retrieveAccounts(username);
        if (CollectUtils.isEmpty(userAccounts))
        {
            throw new Hd3dPersistenceException("cannot retrieve user " + username);
        }

        Session hibSession = HibernateUtil.currentSession();
        for (IUserAccount user : userAccounts)
        {
            if (AuthenticationUtil.isNotSuperUser(user.getPrincipal()))
            {
                Persistors.useraccount.delete(user);
                AuthenticationUtil.clearUser(username);
                // audit(hibSession, user, Audit.DELETE, ": user account has been deleted");
            }
            else
            {
                throw new Hd3dPersistenceException("It is not allowed to delete a super user.");
            }
        }

        hibSession.flush();
    }

    /**
     * Updates an account by changing old password into specified new password.
     * 
     * @param username
     * @param password
     * @param new_pass
     * @throws Hd3dException
     */
    public static void updateAccount(final String username, final String password, final String new_pass)
            throws Hd3dException
    {
        List<IUserAccount> persons = retrieveAccounts(username);
        if (CollectUtils.isEmpty(persons))
        {
            throw new Hd3dPersistenceException("cannot retrieve user " + username);
        }
        IUserAccount user = persons.get(0);
        String actual_pass = user.getSecret();

        if (actual_pass != null && !actual_pass.equals(DEFAULT_SECRET))
        {
            if (AuthenticationUtil.isCurrentSuperUser())
            {
                // wonder if it is a good choice, but "super_admin" can do anything...
                // especially when a user has lost his/her password !
            }
            else if (password == null || !FDTAuthUtils.encrypt(password).equals(actual_pass))
            {
                throw new Hd3dException("wrong password");
            }
        }
        user.setSecret(FDTAuthUtils.encrypt(new_pass));
        // new_pass = null; // to protect memory

        user.doPrePersist();
        Persistors.useraccount.persist(user);
        user.doPostPersist();

        AuthenticationUtil.clearUser(username);
        // audit(HibernateUtil.currentSession(), user, Audit.UPDATE, ": old password " + password +
        // " has been changed");
    }

    /**
     * Gets the list of accounts with specified user name as principal. Usually the list would consist in one or no
     * element.
     * 
     * @param hibSession
     * @param username
     * @return
     * @throws Hd3dPersistenceException
     */
    private static List<IUserAccount> retrieveAccounts(String username)
    {
        try
        {
            return Persistors.useraccount.getByValue_NoPermCheck(null, Const.PRINCIPAL, username);
        }
        catch (Hd3dException e)
        {
            return new ArrayList<IUserAccount>(0);
        }
    }

    // private static void audit(Session session, IUserAccount user, String operation, String message)
    // {
    // Audit audit = new Audit();
    // IPerson person = AuthenticationUtil.getCurrentUser();
    // if (person != null)
    // {
    // audit.setPerson(person.getId());
    // audit.setLogin(person.getLogin());
    // audit.setFirstName(person.getFirstName());
    // audit.setLastName(person.getLastName());
    // }
    // audit.setDateMs((new Date()).getTime());
    // audit.setEntityName(user.entityName());
    // audit.setEntityId(user.getId());
    // audit.setOperation(operation);
    // audit.setModification(user.getPrincipal() + message);
    // session.save(audit);
    // session.flush();
    // }

    /**
     * Returns the account status : -1 - the user doesn't have any; 0 - user has an account and a proper password is
     * set, but has never been connected; >0 - user has an account and a proper password is set, returns last connection
     * date in ms; -2 - user has an account but the password is set to default; -3 - user has an account, but apparently
     * no secret registered; -4 - user has more than one account ; -5 - account has been retrieved in an external realm.
     * 
     * @param login
     *            the user's login
     * @return the user's account status
     */
    public static Long getUserAccountStatus(String login)
    {
        // check whether user has an external account
        boolean external = false;
        if (hasExternalAccount(login))
        {
            external = true;
        }

        List<IUserAccount> accounts = retrieveAccounts(login);

        if (CollectUtils.isNotEmpty(accounts))
        {
            if (external || accounts.size() != 1)
            {
                return ACCOUNT_MULTIPLE; // HD3D account ought to be deleted
            }
            String secret = accounts.get(0).getSecret();
            if (secret == null)
            {
                return ACCOUNT_NULLPASSWORD;
            }
            else if (UserAccountsUtil.DEFAULT_SECRET.equals(secret))
            {
                return ACCOUNT_DEFAULTPASSWORD;
            }
            return accounts.get(0).getLastConnectionDate();
        }
        else if (external)
        {
            return ACCOUNT_EXTERNAL;
        }
        return ACCOUNT_MISSING;
    }

    /**
     * Deletes all the user accounts non associated with a <code>Person</code> object.
     * 
     * @throws Hd3dPersistenceException
     */
    @SuppressWarnings("unchecked")
    public static void removeOrphanAccounts() throws Hd3dPersistenceException
    {
        Session hibSession = HibernateUtil.currentSession();
        final Criteria personCriteria = hibSession.createCriteria(Persistors.useraccount.getPersistedClass());
        personCriteria.add(Restrictions.ne(Const.PRINCIPAL, AuthenticationUtil.ROOT_USER));
        List<IUserAccount> persons = personCriteria.list();
        String errorMessage = "";
        IPerson owner;
        for (IUserAccount account : persons)
        {
            try
            {
                owner = retrieveAccountOwner(account);
                if (owner == null)
                {
                    hibSession.delete(account);
                    // audit(hibSession, account, Audit.DELETE, ": user account has been deleted");
                    AuthenticationUtil.clearUser(account.getPrincipal());
                }
            }
            catch (Hd3dException he)
            {
                errorMessage = errorMessage.concat(SystemUtils.LINE_SEPARATOR).concat(he.getMessage());
            }
        }
        if (StringUtils.isNotEmpty(errorMessage))
        {
            hibSession.clear();
            throw new Hd3dPersistenceException(errorMessage);
        }
        else
        {
            hibSession.flush();
        }
    }

    /**
     * Tries to find the owner (<code>IPerson</code> object) of an account, by searching for an user whose login equals
     * the specified account's principal.
     * 
     * @param hibSession
     * @param account
     * @return
     * @throws Hd3dPersistenceException
     */
    private static IPerson retrieveAccountOwner(IUserAccount account) throws Hd3dException
    {
        List<IPerson> persons = Persistors.person.getByValue_NoPermCheck(null, Const.LOGIN, account.getPrincipal());

        if (CollectUtils.isEmpty(persons))
            return null;

        if (persons.size() > 1)
        {
            throw new Hd3dPersistenceException("account " + account.getPrincipal()
                    + " belongs to more than one person !");
        }

        return persons.get(0);
    }

    /* EXTERNAL ACCOUNTS & REALMS UTILITIES */

    /**
     * Decides whether the user with given login possesses an external account.
     * 
     * @param login
     *            the user login
     * @return true if a user with this login is found in an external realm
     */
    private static boolean hasExternalAccount(String login)
    {
        for (AuthenticatingRealm realm : RealmSwitcher.getExternalRealms())
        {
            if (userFoundInRealm(login, realm))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks whether user with given login can be found in specified authenticating realm.
     * 
     * @param login
     *            the user login
     * @param realm
     *            the authenticating realm
     * @return true if user is in realm
     */
    private static boolean userFoundInRealm(String login, AuthenticatingRealm realm)
    {
        try
        {
            AuthenticationToken token = new UsernamePasswordToken(login, new char[1]);
            AuthenticationInfo info = realm.getAuthenticationInfo(token);
            if (info != null && info.getCredentials() != null)
            {
                return true;
            }
        }
        catch (Exception e)
        {
            // user is not known
            Log.LOGGER.debug(" -- failed attempt at finding ext. accounts for " + login + " in realm "
                    + realm.getName() + " reason: " + e.getClass().getCanonicalName() + " " + e.getMessage());
        }
        return false;
    }

}
