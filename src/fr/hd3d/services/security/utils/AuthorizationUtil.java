package fr.hd3d.services.security.utils;

import static fr.hd3d.common.client.ServicesURI.VERSION;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.PermissionResolver;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ThreadContext;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.restlet.Request;
import org.restlet.data.Method;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.services.resources.AbstractHd3dBaseResource;
import fr.hd3d.services.resources.AbstractHd3dCollectionResource;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.customs.Hd3dWildCardPermissionResolver;
import fr.hd3d.services.security.dao.RealmSwitcher;
import fr.hd3d.services.security.templates.SecurityTemplatesInit;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


/**
 * Now this class will statically perform all the dirty work around URL-permission and URL-pattern (of a permission). It
 * will map all the correspondences between URLs and permission strings. It is also responsible for cache clearing after
 * rights modification. Finally it checks the permissions of an user if politely asked to.
 * 
 * @author florent-della-valle
 * 
 */
@SuppressWarnings("serial")
public final class AuthorizationUtil
{
    // For the mapping between resource URLs and permission patterns
    protected static ConcurrentHashMap<String, LinkedList<String>> patterns;

    // Constant indicators of id and operation
    public final static String ID = "{id}";
    public final static String OP = "{op}";
    // indicator for data object
    public final static String DATAOBJECT = "DataObject";
    public final static String OTHER = "Other";

    public final static Map<Method, String> METHODSMAP = new HashMap<Method, String>(4) {
        {
            put(Method.GET, Customs.READ);
            put(Method.POST, Customs.CREATE);
            put(Method.PUT, Customs.UPDATE);
            put(Method.DELETE, Customs.DELETE);
        }
    };

    /**
     * This method is launched upon application initialization. It basically initializes the pattern map and the default
     * security template.
     */
    public static void init()
    {
        patterns = new ConcurrentHashMap<String, LinkedList<String>>();

        if (!Conf.isMigration())
        {
            initSecurityTemplates();
        }

        // test
        // setTest();
    }

    /**
     * Initializes a default project security template with some predefined role templates.
     */
    public static void initSecurityTemplates()
    {
        try
        {
            SecurityTemplatesInit.securityTemplatesInit();
        }
        catch (Hd3dPersistenceException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
    }

    /**
     * Matches a ressource's URL with a permission pattern. <br>
     * Ex: url = assetrevisiongroups/{assetrevisiongroupId}/assets/{assetId}/assetrevisions/{assetrevisionId} <br>
     * will have the following pattern:<br>
     * [DataObject, v1, assetrevisiongroups, {id}, assets, {id}, assetrevisions, {id}, {op}]
     * 
     * @param url
     * @param isDataObject
     */
    public static void registerURL(final String url, final boolean isDataObject)
    {
        if (StringUtils.isBlank(url))
            return;

        final String _url = VERSION + url;
        patterns.put(_url, toPermissionPattern(_url, isDataObject));
    }

    /*--------------------------------
     * PERMISSIONS RELATED FUNCTIONS |
     --------------------------------*/

    /**
     * Decides in accordance with Shiro realms whether the user can access to specified resource.
     * 
     * @param request
     *            the authenticated client request
     */
    public static boolean userMayAccessResource(final Request request)
    {
        /* convert the request url into permission string */
        Set<String> permissions = new HashSet<String>() {
            {
                String permissions = AuthorizationUtil.toPermissionString(request);
                if (StringUtils.isNotBlank(permissions))
                    add(permissions);
            }
        };

        Log.LOGGER.info("checks users permission on " + permissions);
        return canOneAtLeast(permissions);
    }

    private static RealmSwitcher getRealmSwitcher()
    {
        RealmSwitcher ret = null;
        org.apache.shiro.mgt.SecurityManager uManager = ThreadContext.getSecurityManager();
        if (uManager instanceof DefaultWebSecurityManager)
        {
            DefaultWebSecurityManager manager = (DefaultWebSecurityManager) uManager;
            for (Realm realm : manager.getRealms())
            {
                if (realm instanceof RealmSwitcher)
                {
                    ret = (RealmSwitcher) realm;
                    break;
                }
            }
        }
        return ret;
    }

    /**
     * For clearing the authz cache after modifying persons, groups, roles or permissions
     */
    public static void clearCache()
    {
        Log.LOGGER.debug("try to clear authorization cache...");

        RealmSwitcher switcher = getRealmSwitcher();

        if (switcher == null)
            return;

        switcher.clearCache();
    }

    /**
     * Directly obtains an user's permissions.
     */
    public static Collection<String> getCurrentUserPermissions()
    {
        Subject subject = SecurityUtils.getSubject();

        /* root user has all rights */
        if (AuthenticationUtil.isSuperUser(subject))
        {
            return Arrays.asList("*");
        }

        RealmSwitcher switcher = getRealmSwitcher();

        if (switcher == null)
            return null;

        return switcher.getPermissions(subject);
    }

    /**
     * Directly obtains an user's bans.
     */
    public static Collection<String> getCurrentUserBans()
    {
        RealmSwitcher switcher = getRealmSwitcher();

        if (switcher == null)
            return null;

        return switcher.getBans(SecurityUtils.getSubject());
    }

    /**
     * Decides whether the subject has at least one of the suggested permissions calling upon Shiro.
     * 
     * @param permissions
     * @return true if the subject is allowed to at least one 'permission' in permissions
     */
    public static boolean canOneAtLeast(Set<String> permissions) throws AuthorizationException
    {
        if (shortCutPermission())
            return true;

        Subject subject = SecurityUtils.getSubject();
        for (String perm : permissions)
        {
            if (subject.isPermitted(perm))
                return true;
        }
        return false;
    }

    /**
     * Checks whether the user was granted all the specified rights.
     * 
     * @param rights
     * @return
     */
    public static boolean canAll(List<String> rights)
    {
        if (shortCutPermission())
            return true;

        Subject subject = SecurityUtils.getSubject();
        return subject.isPermittedAll(rights.toArray(new String[1]));
    }

    public static boolean shortCutPermission()
    {
        /* if authorizations are disabled, short cut the test */
        if (!Conf.isAuthEnable())
        {
            return true;
        }

        // if user is root, short cut the test
        Subject subject = SecurityUtils.getSubject();
        if (AuthenticationUtil.isSuperUser(subject))
        {
            return true;
        }
        return false;
    }

    /*--------------------------
     * URL PATTERN ENGINEERING |
     --------------------------*/
    public static List<LinkedList<String>> getAllURLPatterns()
    {
        return new ArrayList<LinkedList<String>>(patterns.values());
    }

    /**
     * Returns all the URL patterns in the application
     * 
     * @return
     */
    public static Set<String> getPatternURLs()
    {
        return patterns.keySet();
    }

    /**
     * Returns the permission pattern matching the URL without the data object marker
     * 
     * @param url
     * @return
     */
    // public static LinkedList<String> getPermissionPattern(String url)
    // {
    // LinkedList<String> fullPattern = patterns.get(url);
    // if (CollectUtils.isEmpty(fullPattern))
    // return null;
    //
    // return new LinkedList<String>(fullPattern) {
    // {
    // /* remove "isDataObject" marker, which is of no use here */
    // removeFirst();
    // }
    // };
    // }
    /**
     * Returns a permission pattern calculated from a given url | ex : ( /projects/{projectId} will transform into the
     * list ( {isDataObject?}, v1, projects, {id}, {op} )
     * 
     * @param url
     * @param isDataObject
     * @return
     */
    public static LinkedList<String> toPermissionPattern(String url, boolean isDataObject)
    {
        /* Temporary: a full URL-permission mapping should be performed */
        LinkedList<String> pattern = urlToPattern(url);
        pattern.addFirst(isDataObject ? DATAOBJECT : OTHER);
        return pattern;
    }

    /**
     * Quite awkward method whose role is to transform the URL of a resource into a pattern of permission. Very fragile
     * because depending on how the resource are attached to the router and how the resource_ids are specified.
     * 
     * @param url
     *            the server resource URI
     * @return a list of string that denotes an URL pattern
     */
    private static LinkedList<String> urlToPattern(String url)
    {
        // LinkedList<String> pattern = new LinkedList<String>();

        // String[] parts = StringUtils.split(url, '/');
        // String part;
        // for (int k = 0; k < parts.length; k++)
        // {
        // part = parts[k];
        // if (part.length() > 0 && !part.contains("?"))
        // {
        // if (part.charAt(0) == '{') // /!\ fragile
        // {
        // pattern.addLast(AuthorizationUtil.ID);
        // }
        // else
        // {
        // pattern.addLast(part);
        // }
        // }
        // }
        List<String> pattern = new ArrayList<String>();

        List<String> chunks = Arrays.asList(StringUtils.split(url, '/'));
        for (String chunk : chunks)
        {
            if (StringUtils.isBlank(chunk) || chunk.contains("?"))
                continue;

            if (StringUtils.startsWith(chunk, "{")) // /!\ fragile
            {
                pattern.add(AuthorizationUtil.ID);
            }
            else
            {
                pattern.add(chunk);
            }
        }

        pattern.add(AuthorizationUtil.OP);
        // System.out.println(pattern);
        return new LinkedList<String>(pattern);
    }

    /*---------------------------------
     * PERMISSIONS STRING ENGINEERING |
     ---------------------------------*/
    /**
     * A method that returns the permission associated with a given request | ex : ( url=/v1/projects/4532, method=GET )
     * will transform into ( v1:projects:4532:read )
     * 
     * @param request
     * @return
     */
    public static String toPermissionString(Request request)
    {
        if (request == null)
            return null;

        StringBuilder buf = new StringBuilder(getPermissionString(request)).append(':');
        buf.append(METHODSMAP.get(request.getMethod()));// add the suffix

        return buf.toString();
    }

    /**
     * parses the request and transforms it into a shiro permission in other words: applies the mapping between REST
     * URIs and the String representing the data permission
     * 
     * @param request
     * @return
     */
    private static String getPermissionString(Request request)
    {
        // String permission = "";
        // String actual = request.getResourceRef().getPath();// Ex: "/Hd3dServices/v1/filesystem"
        // String origin = request.getRootRef().getPath();// "/Hd3dServices"
        // actual = actual.replaceFirst(origin + "/", "");// "v1/filesystem"
        // actual = actual.split("\\+")[0]; // fragile, yet sufficient for asset hook treatment for now...
        // permission += actual.replace('/', ':');
        // return StringUtils.chomp(permission, ":");

        StringBuilder permission = new StringBuilder();
        final String uri = request.getResourceRef().getPath();/* Ex:"/Hd3dServices/v1/filesystem" */
        final String prefix = request.getRootRef().getPath();/* "/Hd3dServices" = prefix */

        /* remaining string to resolve */
        String toResolve = StringUtils.removeStart(uri, prefix + "/");/* "v1/filesystem" */

        /* In AssetAbstract resources, "\" is used to identity asset abstract. */
        /* fragile, yet sufficient for asset hook treatment for now... */
        toResolve = StringUtils.substringBefore(toResolve, "\\");

        permission.append(StringUtils.replaceChars(toResolve, '/', ':'));
        // TODO ugly workaround to refactor later
        // AbstractHd3dBaseResource res = (AbstractHd3dBaseResource) ThreadContext.get("resource");
        // if (AbstractHd3dCollectionResource.class.isInstance(res))
        // permission.append(":*");

        //
        return StringUtils.chomp(permission.toString(), ":");
    }

    /**
     * TODO Ugly workaround since it takes directly the ServerResource stored previously in ThreadContext.<br>
     * TO BE REFACTORED (removed) when implementing ACL based solution.
     * 
     * @return
     */
    public static <P extends IBase> Set<String> getPermissionStrings(P object)
    {
        Set<String> permissions = new HashSet<String>();
        final String basePermission = getPermissionStringFromUri();
        permissions.add(basePermission);
        permissions.addAll(getImpliedPermissionString(basePermission, object));
        return permissions;
    }

    private static String getPermissionStringFromUri()
    {
        // TODO ugly workaround to refactor later
        AbstractHd3dBaseResource res = (AbstractHd3dBaseResource) ThreadContext.get("resource");

        /* workaround for internal call, to bypass security */
        if (res == null)
            return AuthenticationUtil.ROOT_USER;

        StringBuilder permission = new StringBuilder();

        /* Ex:"/Hd3dServices/v1/filesystem" */
        final String uri = StringUtils.chomp(res.getRequest().getResourceRef().getPath(), "/");

        /* prefix = "/Hd3dServices" */
        final String prefix = res.getRequest().getRootRef().getPath();

        /* remaining string to resolve = "v1/filesystem" */
        String toResolve = StringUtils.removeStart(uri, prefix + "/");

        /* In AssetAbstract resources, "\" is used to identity asset abstract. */
        /* fragile, yet sufficient for asset hook treatment for now... */
        toResolve = StringUtils.substringBefore(toResolve, "\\");

        permission.append(StringUtils.replaceChars(toResolve, '/', ':'));

        // if (AbstractHd3dCollectionResource.class.isInstance(res))
        // permission.append(":*");

        /* suffix = operation (read,create,delete,update) */
        permission.append(':').append(METHODSMAP.get(res.getRequest().getMethod()));// add the suffix

        return StringUtils.chomp(permission.toString(), ":");
    }

    private static <P extends IBase> Set<String> getImpliedPermissionString(String basePermission, P object)
    {
        Set<String> ret = new HashSet<String>();

        AbstractHd3dBaseResource res = (AbstractHd3dBaseResource) ThreadContext.get("resource");
        /*
         * a query for a collection implies every of all the items of the collection. For example:
         * Hd3dServices/projects/ may be forbidden, but Hd3dServices/projects/{a given project id} may be allowed.
         */
        if (AbstractHd3dCollectionResource.class.isInstance(res))
        {
            String[] parts = StringUtils.split(basePermission, ':');
            String[] newParts;

            String lastUriPartBeforeOperator = parts.length >= 2 ? parts[parts.length - 2] : parts[0];
            if (!lastUriPartBeforeOperator.equals("all"))
            {
                String[] defaultPathTokens = StringUtils.split(object.defaultPath(), '/');
                String lastDefaultPartBeforeOperator = defaultPathTokens.length >= 2 ? defaultPathTokens[defaultPathTokens.length - 2]
                        : defaultPathTokens[0];
                if (lastDefaultPartBeforeOperator.equals(lastUriPartBeforeOperator))
                {
                    newParts = (String[]) ArrayUtils.add(parts, parts.length - 1, String.valueOf(object.getId()));
                }
                else
                {
                    newParts = parts;
                }
            }
            else
            {
                newParts = parts;
            }
            final String permission = StringUtils.join(newParts, ':');
            ret.add(permission);
        }

        return ret;
    }

    /**
     * Simple facility for building a permission from a list. Here, if the input is {v1, projects, 14, read}, the output
     * will simply be v1:projects:14:read
     * 
     * @param pattern
     * @return a permission string
     */
    // public static String assemblePermission(LinkedList<String> pattern)
    // {
    // StringBuilder sb = new StringBuilder();
    // for (String elt : pattern)
    // {
    // sb.append(elt + ':');
    // }
    // // String permission = sb.toString();
    // // return permission.substring(0, permission.length() - 1);
    // return StringUtils.chomp(sb.toString(), ":");
    // }
    /*-----------------------------
     * STRING FACTORING UTILITIES |
     -----------------------------*/

    /**
     * Should always be called upon any role's permission modification. It will sometimes concentrate and clean the set
     * after the operation. This for preventing permission sets to grow absurdly enormous.
     */
    public static void onAddingPermissions(Set<String> permissions)
    {
        if (permissions.size() > 6) // /!\ completely arbitrary
        {
            splitOperations(permissions);
            clean(permissions);
            merge(permissions);
        }
    }

    /**
     * First merges the permissions based on same object and operations but different instances. Then merges the same
     * operations. (Finally merges the objects themselves ? Not now.)
     * 
     * @param permissions
     */
    private static void merge(Set<String> permissions)
    {
        Set<String> mergedSet = new LinkedHashSet<String>();
        Set<String> toRemove = new LinkedHashSet<String>();
        String merged;
        String temp;

        // merging on ids
        // for (String perm1 : permissions)
        // {
        // if (!toRemove.contains(perm1))
        // {
        // temp = perm1;
        // for (String perm2 : permissions)
        // {
        // if (perm2 != perm1 && !toRemove.contains(perm2))// INTENTIONAL REFERENCE COMPARISON
        // {
        // merged = mergeIDs(temp, perm2);
        // if (merged != null)
        // {
        // temp = merged;
        // toRemove.add(perm2);
        // }
        // }
        // }
        // toRemove.add(perm1);
        // mergedSet.add(temp);
        // }
        // }
        // permissions.removeAll(toRemove);
        // permissions.addAll(mergedSet);
        // mergedSet = new LinkedHashSet<String>();
        // toRemove = new LinkedHashSet<String>();

        // merging on ops
        for (String perm1 : permissions)
        {
            if (!toRemove.contains(perm1))
            {
                temp = perm1;
                for (String perm2 : permissions)
                {
                    if (perm2 != perm1 && !toRemove.contains(perm2))
                    {
                        merged = mergeOPs(temp, perm2);
                        if (merged != null)
                        {
                            temp = merged;
                            toRemove.add(perm2);
                        }
                    }
                }
                toRemove.add(perm1);
                mergedSet.add(temp);
            }
        }
        permissions.removeAll(toRemove);
        permissions.addAll(mergedSet);

        // class PermissionMerger implements Transformer<String, String>
        // {
        //
        // Set<String> permissions;
        //
        // PermissionMerger(Set<String> permissions)
        // {
        // this.permissions = permissions;
        // }
        //
        // @Override
        // public String transform(String permission)
        // {
        // for (String aPermission : permissions)
        // {
        // if (aPermission == permission)// INTENTIONAL REFERENCE COMPARISON
        // continue;
        // mergeOPs(aPermission, permission);
        //
        // }
        // }
        //
        // }
    }

    // @SuppressWarnings("unused")
    // @Deprecated
    // private static String mergeIDs(String perm1, String perm2)
    // {
    // int last = perm1.lastIndexOf(":") + 1;
    // String postfix1 = perm1.substring(0, last - 1);
    // String suffix1 = perm1.substring(last);
    // int first = postfix1.lastIndexOf(":") + 1;
    // String prefix1 = postfix1.substring(0, first);
    // postfix1 = postfix1.substring(first);
    // last = perm2.lastIndexOf(":") + 1;
    // String postfix2 = perm2.substring(0, last - 1);
    // first = postfix2.lastIndexOf(":") + 1;
    // if (prefix1.equals(postfix2.substring(0, first)) && suffix1.equals(perm2.substring(last)))
    // {
    // postfix2 = postfix2.substring(first);
    //
    // // there perm1 and perm2 could contain the same id, we don't know
    // SortedSet<String> ids = new TreeSet<String>();
    // while (postfix1.contains(","))
    // {
    // last = postfix1.lastIndexOf(",");
    // ids.add(postfix1.substring(last + 1));
    // postfix1 = postfix1.substring(0, last);
    // }
    // ids.add(postfix1);
    // while (postfix2.contains(","))
    // {
    // last = postfix2.lastIndexOf(",");
    // ids.add(postfix2.substring(last + 1));
    // postfix2 = postfix2.substring(0, last);
    // }
    // ids.add(postfix2);
    //
    // // String result = prefix1;
    // // for (String id : ids)
    // // result += id + ",";
    // // result = result.substring(0, result.length() - 1);
    // // result += ":" + suffix1;
    //
    // StringBuilder result = new StringBuilder(prefix1).append(StringUtils.join(ids, ',')).append(':').append(
    // suffix1);
    //
    // return result.toString();
    // }
    // return null;
    // }

    private static String mergeOPs(String perm1, String perm2)
    {
        int last = perm1.lastIndexOf(":") + 1;
        String prefix1 = perm1.substring(0, last);
        if (last < perm2.length() && prefix1.equals(perm2.substring(0, last)))
        {
            // there perm1 and perm2 cannot contain the same OP because of split and clean
            String result = perm1 + "," + perm2.substring(last);

            // dangerous thing here /!\
            // if (result.contains("update") && result.contains("read") && result.contains("delete"))
            // result = "*";
            return result;
        }
        return null;
    }

    public static PermissionResolver getPermissionResolver()
    {
        return new Hd3dWildCardPermissionResolver();
    }

    /**
     * Removes from a set of permissions those which are already implied by another one in the set, thus avoiding
     * redundancy.
     * 
     * @param permissions
     */
    private static void clean(Set<String> permissions)
    {
        PermissionResolver resolver = getPermissionResolver();
        Set<String> toRemove = new HashSet<String>();

        for (String perm1 : permissions)
        {
            if (!toRemove.contains(perm1))
            {
                Permission p1 = resolver.resolvePermission(perm1);
                for (String perm2 : permissions)
                {
                    if (perm1 != perm2)// INTENTIONAL REFERENCE COMPARISON
                    {
                        Permission p2 = resolver.resolvePermission(perm2);
                        if (p1.implies(p2))
                            toRemove.add(perm2);
                    }
                }
            }
        }

        permissions.removeAll(toRemove);

        // CollectionUtils.filter(permissions, new ImplyPermissionPredicate(permissions));
    }

    public static class ImplyPermissionPredicate implements Predicate<String>
    {
        PermissionResolver resolver = getPermissionResolver();
        Set<String> permissions;

        ImplyPermissionPredicate(Set<String> permissions)
        {
            this.permissions = permissions;
        }

        public boolean evaluate(String object)
        {
            for (String p : permissions)
            {
                if (object == p)
                    continue;
                // Keep this order:
                // if the current permission string is implied by the some others, remove it
                if (resolver.resolvePermission(p).implies(resolver.resolvePermission(object)))
                    return false;
            }
            return true;
        }
    }

    /**
     * Preparing for efficient cleaning and merging, splits up aggregated operations at the end of a permission string.
     * Ex: "v1:orange:14:read,update,create" will be split into: <br>
     * v1:orange:14:read<br>
     * v1:orange:14:update<br>
     * v1:orange:14:create<br>
     * and "v1:orange:14:read,update,create" will be removed from the Set of permissions
     * 
     * @param permissions
     */
    private static void splitOperations(Set<String> permissions)
    {
        Collection<Set<String>> simplePermissions = CollectionUtils.collect(permissions, new OperationSplitter());

        permissions.clear();

        for (Set<String> permission : simplePermissions)
        {
            permissions.addAll(permission);
        }

    }

    public static class OperationSplitter implements Transformer<String, Set<String>>
    {
        public Set<String> transform(final String permission)
        {
            Set<String> ret = new HashSet<String>();

            final String prefix = StringUtils.substringBeforeLast(permission, ":");
            String operations = StringUtils.substringAfterLast(permission, ":");

            // if operations string just contains one operation (no "," separator) return as is
            if (!operations.contains(","))
                ret.add(permission);
            else
                // otherwise, split the operations into several simple permission strings
                for (String aOperation : Arrays.asList(StringUtils.split(operations, ",")))
                    ret.add(format(prefix, aOperation));

            return ret;
        }

        private String format(String prefix, String operation)
        {
            return new StringBuilder(prefix).append(':').append(operation).toString();
        }
    }

    // test
    @SuppressWarnings("unused")
    private static void setTest()
    {
        Set<String> test = new HashSet<String>();
        test.add("v1:abricot:12:read");
        test.add("v1:abricot:read");
        test.add("v1:abricot:read");
        test.add("v1:abricot:13,15:read");
        test.add("v1:abricot:13:read,update");
        test.add("v1:abricot:12,13:update");
        test.add("v1:abricot:15:*");
        test.add("v1:pomme:*");
        test.add("v1:pomme:create");
        test.add("v1:orange:14:read");
        test.add("v1:orange:14:read,update");
        test.add("v1:orange:14:delete");
        test.add("v1:orange:*:read");
        test.add("v1:orange:read");
        test.add("v1:melon:11:read,delete");
        test.add("v1:melon:11:update");
        System.out.println("init set: " + SystemUtils.LINE_SEPARATOR + test);
        splitOperations(test);
        System.out.println("split set: " + SystemUtils.LINE_SEPARATOR + test);
        clean(test);
        System.out.println("cleaned set: " + SystemUtils.LINE_SEPARATOR + test);
        merge(test);
        System.out.println("merged set: " + SystemUtils.LINE_SEPARATOR + test);
    }

}
