package fr.hd3d.services.security.utils;

import java.security.Key;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.BlowfishCipherService;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Cookie;
import org.restlet.data.Status;

import fr.hd3d.exception.Hd3dAuthorizationException;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.services.auth.FDTAuthUtils;
import fr.hd3d.services.security.customs.Hd3dAuthcTokenImpl;
import fr.hd3d.services.security.customs.IAuthcToken;
import fr.hd3d.services.security.dao.IUserAccount;
import fr.hd3d.services.security.dao.SuperAccountsInit;
import fr.hd3d.utils.Base64Coder;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.predicate.GetExpiredTokenPredicate;
import fr.hd3d.utils.predicate.GetPrincipalPredicate;


/**
 * Here is the dirty-worker that performs most of authentication. Its roles are severals: 1) to store user tokens in
 * order to recognize further requests; 2) to manage all the cookie stuff; 3) to encode and decode cookie values
 * from/into user marks; 4) to handle the logging in and out of the users.
 * 
 * @author HD3D
 * 
 */
public final class AuthenticationUtil
{
    // authentication feedback
    public final static String OK = "user successfully authenticated";
    public final static String KO_CREDENTIALS = "wrong login or password";
    public final static String KO_OTHER = "unable to authenticate the user";
    public final static String MSG_1 = "try to authenticate {0} (current user) with principal {1}";

    // super user properties
    public final static String ROOT_USER = SecurityProperties.getRootUserName();
    public final static String ROOT_PASS = FDTAuthUtils.encrypt(SecurityProperties.getRootPassword());
    public final static String ADMIN_USER = SecurityProperties.getAdminUserName();
    public final static String ADMIN_GROUP = SecurityProperties.getAdminGroupName();
    public final static String ADMIN_PASS = FDTAuthUtils.encrypt(SecurityProperties.getAdminPassword());
    public final static String ADMIN_ROLE = SecurityProperties.getAdminRoleName();

    // time to live
    public final static long TIME_TO_LIVE = SecurityProperties.getTimeToLive() * Long.valueOf(Const.MINUTE); // test
    public final static long REMEMBER_TIME_TO_LIVE = SecurityProperties.getRememberTimeToLive() * 24L * Const.HOUR;
    public final static String REMEMBERME_ATTRIBUTE = SecurityProperties.getCookieRememberMe();

    private static AtomicInteger login_counter = new AtomicInteger();
    private static ConcurrentHashMap<String, IAuthcToken> registeredTokens = new ConcurrentHashMap<String, IAuthcToken>();

    // cipher
    private final static BlowfishCipherService cipher = new BlowfishCipherService();
    private final static Key cipherKey;

    public static ThreadLocal<IPerson> currentUser = new ThreadLocal<IPerson>();
    public static ThreadLocal<Set<IRole>> currentUserRoles = new ThreadLocal<Set<IRole>>();

    static
    {
        cipherKey = cipher.generateNewKey();
        SecurityUtils.setSecurityManager(new DefaultSecurityManager());
    }

    /**
     * This method is launched upon application initialization. It basically retrieves the cookie configuration,
     * generates a secret key for cookie value ciphering and initializes super users.
     */
    public static void init()
    {
        if (!Conf.isMigration())
        {
            initSuperUser();
        }
    }

    public static int getLoginCounter()
    {
        return login_counter.intValue();
    }

    private static void debug(String message)
    {
        if (!Conf.isInProductionState())
        {
            Log.LOGGER.info(message);
        }
    }

    /**
     * The main method for logging an user in.
     * 
     * @param request
     * @param response
     * @return true if the logging is successful
     * @throws Hd3dAuthorizationException
     */
    public static boolean doLogin(final LoginInfo loginInfo, final Request request, final Response response)
            throws Hd3dAuthorizationException
    {
        try
        {
            loginShiro(loginInfo.getUserName(), loginInfo.getPassword());
            Log.LOGGER.info("login successful");

            /*
             * create a new cookie and send it to client. The cookie value contains the token
             * {rememberMe}:{date}:{address}:{username}:{password} (ciphered)
             */
            IAuthcToken token = new Hd3dAuthcTokenImpl(loginInfo.isRemember(), new Date().getTime(), loginInfo
                    .getAddress(), loginInfo.getUserName(), loginInfo.getPassword());

            emitCookie(response, toCookieValue(token.mark()), loginInfo.isRemember());

            debug("cookie created");

            registerToken(token);

            debug("cookie registered");

            // cleans the register from time to time
            debug("cleanup tokens check");
            login_counter.incrementAndGet();
            if (getLoginCounter() % 30 == 0)
            {
                Log.LOGGER.info("cleaning up token");
                cleanUpExpiredTokens();
            }
            debug("cleanup tokens check");

            // /* sets last connection info if status is OK */
            debug("updating last connection info");
            updateLastConnectionInfo(loginInfo.getUserName());

            Log.LOGGER.info(token.getPrincipal() + " successfuly logged in ");
            response.setStatus(Status.SUCCESS_OK);
            return true;
        }
        catch (AuthenticationException ae)
        {
            Log.LOGGER.info("login failed: " + ae.getMessage());
            doLogout(request, response);
            response.setStatus(Status.SUCCESS_NON_AUTHORITATIVE);
            return false;
        }
        catch (Exception e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            doLogout(request, response);
            response.setStatus(Status.SERVER_ERROR_INTERNAL);
            return false;
        }
    }

    private static void updateLastConnectionInfo(String login)
    {
        IUserAccount account;
        try
        {
            account = Persistors.useraccount.getObjectByValue_NoPermCheck(Const.PRINCIPAL, login);
            Log.LOGGER.info("getting login");
            if (account == null)
            {
                Log.LOGGER.info("login not found");
                return;
            }
        }
        catch (Hd3dException e1)
        {
            Log.LOGGER.error("Authentication failed: " + ExceptUtils.format(e1));
            throw new AuthenticationException("authentication failed: ");
        }

        /* if root user do nothing */
        if (AuthenticationUtil.isRootUser(account.getPrincipal())
                && UserAccountsUtil.isDefaultSecret(account.getSecret()))
            return;

        /* sets last connection info if status is OK */
        Session hibSession = HibernateUtil.currentSession();
        Transaction tx = hibSession.beginTransaction();
        account.setLastConnectionDate(new Date().getTime());

        try
        {
            Log.LOGGER.info("updating last connection info in database");
            account.doPreUpdate();
            Persistors.useraccount.merge(account);
            account.doPostUpdate();
            if (hibSession.getFlushMode() == FlushMode.MANUAL)
            {
                hibSession.flush();
            }
            tx.commit();
            Log.LOGGER.info("last connection info committed");
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            tx.rollback();
        }
        finally
        {
            if (tx != null)
            {
                tx = null;
            }
            HibernateUtil.closeSession();
        }
    }

    public static void refresh(long date, LoginInfo loginInfo, Response resp)
    {
        if (loginInfo != null)
        {
            IAuthcToken token = new Hd3dAuthcTokenImpl(loginInfo.isRemember(), date, loginInfo.getAddress(), loginInfo
                    .getUserName(), loginInfo.getPassword());
            AuthenticationUtil.emitCookie(resp, AuthenticationUtil.toCookieValue(token.mark()), loginInfo.isRemember());
            AuthenticationUtil.registerToken(token);
        }
    }

    /**
     * The main method for logging a user out.
     * 
     * @param request
     * @param response
     * @throws Hd3dAuthorizationException
     */
    public static void doLogout(Request request, Response response) throws Hd3dAuthorizationException
    {
        logoutShiro();
        Cookie cookie = CookieUtil.getAuthCookie(request.getCookies());
        if (cookie != null)
        {
            unregisterToken(getMark(cookie));
        }
        CookieUtil.deleteCookie(response);
        login_counter.decrementAndGet();
    }

    /**
     * Logs the user in in the Shiro engine.
     * 
     * @param username
     *            the requesting user's principal
     * @param password
     *            the alleged user's secret
     * @throws AuthenticationException
     *             when authentication fails
     */
    public static void loginShiro(final String username, final String password) throws AuthenticationException
    {
        Subject subject = SecurityUtils.getSubject();
        if (subject != null)
        {
            Log.LOGGER.info(MessageFormat.format(MSG_1, subject.getPrincipal(), username));
            UsernamePasswordToken shiroToken = new UsernamePasswordToken(username, password.toCharArray());
            subject.login(shiroToken);
            shiroToken.clear();
        }
    }

    /**
     * Logs the user out in the Shiro engine.
     */
    public static void logoutShiro()
    {
        SecurityUtils.getSubject().logout();
        currentUser.set(null);
        currentUser.remove();
        currentUserRoles.set(null);
        currentUserRoles.remove();
    }

    /**
     * Clears the authentication cache, so that no existing cookie can be validated.
     */
    public static void clearRegisteredTokens()
    {
        registeredTokens.clear();
    }

    public static void clearUser(final String principal)
    {
        if (principal != null && AuthenticationUtil.isNotRootUser(principal))
        {
            Map.Entry<String, IAuthcToken> principalEntry = CollectionUtils.find(registeredTokens.entrySet(),
                    new GetPrincipalPredicate(principal));

            if (principalEntry != null && principalEntry.getKey() != null)
            {
                registeredTokens.remove(principalEntry.getKey());
            }
        }
    }

    /*----------------------------
     * COOKIE HANDLING UTILITIES |
     ----------------------------*/

    /**
     * Sets an authentication cookie for the specified value
     * 
     * @param response
     * @param value
     * @param remember
     *            whether the cookie must be remember after session expiration
     */
    private static void emitCookie(final Response response, final String value, final boolean remember)
    {
        response.getCookieSettings().add(CookieUtil.getNewCookieSetting(value, remember, REMEMBER_TIME_TO_LIVE));
    }

    /*----------------------------------
     * CIPHERING AND DECIPHERING UTILS |
     ----------------------------------*/

    /**
     * Gets the mark from a ciphered authentication cookie value
     * 
     * @param cookie
     * @return the mark contained in the cookie
     * @throws Hd3dAuthorizationException
     */
    public static String getMark(final Cookie cookie) throws Hd3dAuthorizationException
    {
        String ret = null;

        if (cookie != null)
        {
            ret = getMarkfromCookieValue(cookie.getValue());
        }

        return ret;
    }

    private static String getMarkfromCookieValue(String value) throws Hd3dAuthorizationException
    {
        String ret = null;

        if (value != null)
        {
            try
            {
                ByteSource mark = cipher.decrypt(Base64Coder.decode(value), cipherKey.getEncoded());
                ret = new String(mark.getBytes());// DO NOT USE: String.valueOf
            }
            catch (org.apache.shiro.crypto.CryptoException e)
            {
                Log.LOGGER.error("Invalid cookie, please delete it.");
                throw new Hd3dAuthorizationException("Invalid cookie, please delete it.");
            }
        }

        return ret;
    }

    /**
     * Ciphers a mark into a valid cookie value
     * 
     * @param mark
     * @return a ciphered value
     */
    private static String toCookieValue(String mark)
    {
        String ret = null;

        if (mark != null)
        {
            ByteSource code = cipher.encrypt(mark.getBytes(), cipherKey.getEncoded());
            ret = new String(code.toBase64());// DO NOT USE: String.valueOf
        }

        return ret;
    }

    /*--------------------------------------------
     * TOKEN REGISTERING AND RECKONING UTILITIES |
     --------------------------------------------*/

    /**
     * Tells whether a mark is found in the cache
     * 
     * @param mark
     * @return true if the mark is in cache
     */
    public static boolean isRegistered(final String mark)
    {
        return registeredTokens.containsKey(FDTAuthUtils.encrypt(mark));
    }

    /**
     * Checks whether the token is valid and not expired. If so, refreshes the token.
     * 
     * @param token
     * @return
     */
    public static boolean isValid(final IAuthcToken token)
    {
        boolean stillAlive = false;

        if (token != null)
        {
            // hash
            final String tokenKey = FDTAuthUtils.encrypt(token.mark());
            final IAuthcToken existingToken = registeredTokens.get(tokenKey);

            // token does not exist
            if (existingToken != null)
            {
                // token already exist
                long ttl = existingToken.rememberMe() ? REMEMBER_TIME_TO_LIVE : TIME_TO_LIVE;
                stillAlive = existingToken.getDate() + ttl - new Date().getTime() > 0;
            }
        }

        return stillAlive;
    }

    public static boolean isNotValid(final IAuthcToken token)
    {
        return !isValid(token);
    }

    /**
     * Registers a token in cache
     * 
     * @param token
     */
    private static void registerToken(IAuthcToken token)
    {
        if (token != null)
        {
            unregisterToken(token);

            String tokenKey = FDTAuthUtils.encrypt(token.mark());
            registeredTokens.put(tokenKey, token);
        }
    }

    /**
     * Removes a token from cache
     * 
     * @param mark
     */
    private static void unregisterToken(String mark)
    {
        // hash
        registeredTokens.remove(FDTAuthUtils.encrypt(mark));
    }

    private static void unregisterToken(IAuthcToken token)
    {
        if (token != null)
        {
            String tokenKey = FDTAuthUtils.encrypt(token.mark());
            registeredTokens.remove(tokenKey);
        }
    }

    /**
     * Clean up cache of expired tokens
     */
    private static void cleanUpExpiredTokens()
    {
        Collection<Map.Entry<String, IAuthcToken>> expired = CollectionUtils.select(registeredTokens.entrySet(),
                new GetExpiredTokenPredicate());

        CollectUtils.removeNull(expired);

        if (CollectUtils.isNotEmpty(expired))
        {
            for (Map.Entry<String, IAuthcToken> entry : expired)
            {
                if (entry.getKey() != null)
                {
                    registeredTokens.remove(entry.getKey());
                }
            }
        }
    }

    /*--------------------------------
     * USER IDENTIFICATION UTILITIES |
     --------------------------------*/
    /**
     * Gets the current request's user's principal.
     * 
     * @return the current user's login
     */
    public static String getCurrentUserPrincipal()
    {
        Object principal = SecurityUtils.getSubject().getPrincipal();

        return (principal == null) ? null : principal.toString();
    }

    /**
     * Retrieves current user's data in the form of a person object.
     * 
     * @return a person object representing the current user
     */
    public static IPerson getCurrentUser()
    {

        IPerson user = currentUser.get();
        if (user != null)
        {
            return user;
        }

        String principal = getCurrentUserPrincipal();
        if (principal != null)
        {
            try
            {
                user = Persistors.person.getObjectByValue_NoPermCheck(Const.LOGIN, principal);
                if (user != null)
                {
                    Set<IRole> roles = new HashSet<IRole>();
                    for (IResourceGroup group : user.getResourceGroups())
                    {
                        roles.addAll(group.parentsRoles(true));
                    }
                    currentUserRoles.set(roles);
                }
            }
            catch (Hd3dException e)
            {
                Log.LOGGER.error(ExceptUtils.format(e));
            }
        }

        return user;
    }

    public static Set<IRole> getCurrentUserRoles()
    {
        Set<IRole> roles = currentUserRoles.get();
        if (roles != null)
        {
            return roles;
        }

        getCurrentUser();

        roles = currentUserRoles.get();

        return roles;
    }

    public static boolean isRootUser(Subject subject)
    {
        return (subject != null) && subject.getPrincipal() != null
                && AuthenticationUtil.ROOT_USER.equals(subject.getPrincipal().toString());
    }

    public static boolean isNotRootUser(Subject subject)
    {
        return !isRootUser(subject);
    }

    public static boolean isRootUser(String name)
    {
        return AuthenticationUtil.ROOT_USER.equals(name);
    }

    public static boolean isNotRootUser(String name)
    {
        return !isRootUser(name);
    }

    /**
     * Tells whether the current user is a 'super admin' or 'root' user.
     * 
     * @return true if it is the case that current user is a 'super user'
     */
    public static boolean isCurrentSuperUser()
    {
        return isSuperUser(SecurityUtils.getSubject());
    }

    public static boolean isCurrentNotNullNormalUser()
    {
        return AuthenticationUtil.getCurrentUserPrincipal() != null && !AuthenticationUtil.isCurrentSuperUser();
    }

    public static boolean isSuperUser(Subject subject)
    {
        return isRootUser(subject) || subject.hasRole(ADMIN_ROLE)
                || AuthenticationUtil.ADMIN_USER.equals(subject.getPrincipal());
    }

    public static boolean isSuperUser(final String name)
    {
        return isRootUser(name) || AuthenticationUtil.ADMIN_USER.equals(name);
    }

    public static boolean isNotSuperUser(final String name)
    {
        return !isSuperUser(name);
    }

    /**
     * Initializes the database so that it contains at least one 'super user', which will be called 'root' in
     * non-production mode and 'super admin' in production mode. The main difference between the two situations is that
     * root is only a single user account whereas 'super admin' will be actually represented in the database by a person
     * object, belonging to a special group, itself related to a special role, allowing other users (i.e different
     * persons and user accounts) to be granted the 'super admin' privileges in normal situation.
     */
    public static void initSuperUser()
    {
        try
        {
            // debug mode
            if (!Conf.isInProductionState())
            {
                SuperAccountsInit.runRootInit();
            }
            // production
            else
            {
                SuperAccountsInit.runSuperAdminInit();
            }
        }
        catch (Hd3dPersistenceException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
    }

    public static void main(String[] args) throws Hd3dAuthorizationException
    {
        init();
        String s = "TOTO";
        String s1 = toCookieValue(s);
        System.out.println("S1=" + s1);
        String s2 = getMarkfromCookieValue(s1);
        System.out.println("S2=" + s2);

    }

}
