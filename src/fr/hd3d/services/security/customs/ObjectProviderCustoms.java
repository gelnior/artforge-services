package fr.hd3d.services.security.customs;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;

import fr.hd3d.model.persistence.IBase;


/**
 * This class provides methods that allow domain object list filtering.
 * 
 * @author florent-della-valle
 * 
 */
public class ObjectProviderCustoms
{

    /**
     * Filters the list in order to keep only the user-readable objects.
     * 
     * @param <E>
     *            a domain object type
     * @param list
     *            the initial list
     * @return the filtered list
     */
    public static <E extends IBase> List<E> filter(Collection<E> list)
    {
        CollectionUtils.filter(list, new Predicate<E>() {
            public boolean evaluate(E object)
            {
                return object != null && object.canRead();
            }
        });
        return (List<E>) list;
    }

    public static <E extends IBase> E filter(E object)
    {
        return (object != null && object.canRead()) ? object : null;
    }
}
