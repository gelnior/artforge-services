package fr.hd3d.services.security.customs;

import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IAssetAbstract;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionFileRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IComputerModel;
import fr.hd3d.model.persistence.IComputerType;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IConstituentLink;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.IDeviceModel;
import fr.hd3d.model.persistence.IDeviceType;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IEvent;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IFact;
import fr.hd3d.model.persistence.IFactType;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IFileRevisionAnnotation;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.IHint;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.IManufacturer;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.IOrganization;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.IProcessor;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.IQualification;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.IRoom;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.IScreenModel;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ISkill;
import fr.hd3d.model.persistence.ISkillLevel;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.services.security.templates.IProjectSecurityTemplate;
import fr.hd3d.services.security.templates.IRoleTemplate;


/**
 * The customs determines if the user can access to a given object. It ensures data-level protection.
 * 
 * @author HD3D
 * 
 */
public interface Customs
{

    // Operations names
    public final static String CREATE = "create";
    public final static String READ = "read";
    public final static String UPDATE = "update";
    public final static String DELETE = "delete";
    public final static String ALL_RIGHTS = "*";

    public String operationName();

    public boolean visit(IAbsence object);

    public boolean visit(IActivity object);

    public boolean visit(IApprovalNote object);

    public boolean visit(IAssetAbstract object);

    public boolean visit(IAssetRevision object);

    public boolean visit(IAssetRevisionFileRevision object);

    public boolean visit(IAssetRevisionGroup object);

    public boolean visit(IAssetRevisionLink object);

    public boolean visit(ICategory object);

    public boolean visit(IClassDynMetaDataType object);

    public boolean visit(IComposition object);

    public boolean visit(IComputer object);

    public boolean visit(IComputerModel object);

    public boolean visit(IComputerType object);

    public boolean visit(IConstituent object);

    public boolean visit(IConstituentLink object);

    public boolean visit(IContract object);

    public boolean visit(IDevice object);

    public boolean visit(IDeviceModel object);

    public boolean visit(IDeviceType object);

    public boolean visit(IDynMetaDataType object);

    public boolean visit(IDynMetaDataValue object);

    public boolean visit(IEntityTaskLink object);

    public boolean visit(IEvent object);

    public boolean visit(IExtraLine object);

    public boolean visit(IFact object);

    public boolean visit(IFactType object);

    public boolean visit(IFileAttribute object);

    public boolean visit(IFileRevision object);

    public boolean visit(IFileRevisionLink object);

    public boolean visit(IHint object);

    public boolean visit(IItem object);

    public boolean visit(IItemGroup object);

    public boolean visit(ILicense object);

    public boolean visit(IManufacturer object);

    public boolean visit(IMileStone object);

    public boolean visit(IOrganization object);

    public boolean visit(IPersonDay object);

    public boolean visit(IPerson object);

    public boolean visit(IPlanning object);

    public boolean visit(IPool object);

    public boolean visit(IProcessor object);

    public boolean visit(IProject object);

    public boolean visit(IQualification object);

    public boolean visit(IRole object);

    public boolean visit(IRoom object);

    public boolean visit(IResourceGroup object);

    public boolean visit(IScreen object);

    public boolean visit(IScreenModel object);

    public boolean visit(IScript object);

    public boolean visit(ISequence object);

    public boolean visit(ISheet object);

    public boolean visit(IShot object);

    public boolean visit(ISimpleActivity object);

    public boolean visit(ISkill object);

    public boolean visit(ISkillLevel object);

    public boolean visit(ISoftware object);

    public boolean visit(IStudioMap object);

    public boolean visit(ITag object);

    public boolean visit(ITagCategory object);

    public boolean visit(ITask object);

    public boolean visit(ITaskActivity object);

    public boolean visit(ITaskChanges object);

    public boolean visit(ITaskGroup object);

    public boolean visit(ITaskType object);

    public boolean visit(IApprovalNoteType object);

    public boolean visit(IRoleTemplate object);

    public boolean visit(IProjectSecurityTemplate object);

    public boolean visit(IProjectType object);

    public boolean visit(IWorkingTime object);

    public boolean visit(IStep object);

    public boolean visit(IPlayListEdit object);

    public boolean visit(IPlayListRevision object);

    public boolean visit(IFileRevisionAnnotation object);

    public boolean visit(IListValues object);

    public boolean visit(IDynamicPath object);

    public boolean visit(IGraphicalAnnotation object);

    public boolean visit(IMountPoint object);

    public boolean visit(IProxyType object);

    public boolean visit(IProxy object);

	public boolean visit(IPlayListRevisionGroup object);
    
    public boolean visit(IHd3dACL object);
}
