package fr.hd3d.services.security.customs;

import java.util.Set;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IAbsence;
import fr.hd3d.model.persistence.IActivity;
import fr.hd3d.model.persistence.IApprovalNote;
import fr.hd3d.model.persistence.IApprovalNoteType;
import fr.hd3d.model.persistence.IAssetAbstract;
import fr.hd3d.model.persistence.IAssetRevision;
import fr.hd3d.model.persistence.IAssetRevisionFileRevision;
import fr.hd3d.model.persistence.IAssetRevisionGroup;
import fr.hd3d.model.persistence.IAssetRevisionLink;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IClassDynMetaDataType;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IComputer;
import fr.hd3d.model.persistence.IComputerModel;
import fr.hd3d.model.persistence.IComputerType;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IConstituentLink;
import fr.hd3d.model.persistence.IContract;
import fr.hd3d.model.persistence.IDevice;
import fr.hd3d.model.persistence.IDeviceModel;
import fr.hd3d.model.persistence.IDeviceType;
import fr.hd3d.model.persistence.IDynMetaDataType;
import fr.hd3d.model.persistence.IDynMetaDataValue;
import fr.hd3d.model.persistence.IDynamicPath;
import fr.hd3d.model.persistence.IEntityTaskLink;
import fr.hd3d.model.persistence.IEvent;
import fr.hd3d.model.persistence.IExtraLine;
import fr.hd3d.model.persistence.IFact;
import fr.hd3d.model.persistence.IFactType;
import fr.hd3d.model.persistence.IFileAttribute;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IFileRevisionAnnotation;
import fr.hd3d.model.persistence.IFileRevisionLink;
import fr.hd3d.model.persistence.IGraphicalAnnotation;
import fr.hd3d.model.persistence.IHint;
import fr.hd3d.model.persistence.IItem;
import fr.hd3d.model.persistence.IItemGroup;
import fr.hd3d.model.persistence.ILicense;
import fr.hd3d.model.persistence.IListValues;
import fr.hd3d.model.persistence.IManufacturer;
import fr.hd3d.model.persistence.IMileStone;
import fr.hd3d.model.persistence.IMountPoint;
import fr.hd3d.model.persistence.IOrganization;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IPersonDay;
import fr.hd3d.model.persistence.IPlanning;
import fr.hd3d.model.persistence.IPlayListEdit;
import fr.hd3d.model.persistence.IPlayListRevision;
import fr.hd3d.model.persistence.IPlayListRevisionGroup;
import fr.hd3d.model.persistence.IPool;
import fr.hd3d.model.persistence.IProcessor;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IProjectType;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.model.persistence.IProxyType;
import fr.hd3d.model.persistence.IQualification;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.IRoom;
import fr.hd3d.model.persistence.IScreen;
import fr.hd3d.model.persistence.IScreenModel;
import fr.hd3d.model.persistence.IScript;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ISimpleActivity;
import fr.hd3d.model.persistence.ISkill;
import fr.hd3d.model.persistence.ISkillLevel;
import fr.hd3d.model.persistence.ISoftware;
import fr.hd3d.model.persistence.IStep;
import fr.hd3d.model.persistence.IStudioMap;
import fr.hd3d.model.persistence.ITag;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.ITaskActivity;
import fr.hd3d.model.persistence.ITaskChanges;
import fr.hd3d.model.persistence.ITaskGroup;
import fr.hd3d.model.persistence.ITaskType;
import fr.hd3d.model.persistence.IWorkingTime;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.services.security.dao.IHd3dACL;
import fr.hd3d.services.security.templates.IProjectSecurityTemplate;
import fr.hd3d.services.security.templates.IRoleTemplate;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.services.security.utils.AuthorizationUtil;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


/**
 * Visitor for data access protection. It checks the user's permission to perform a certain action on a given object.
 * 
 * @author florent-della-valle
 * 
 */
public class CustomsVisitor implements Customs
{
    private String operation;

    public CustomsVisitor(final String operation)
    {
        this.operation = operation;
    }

    public String operationName()
    {
        return operation;
    }

    private <P extends IBase> boolean doVisit(final P object)
    {
        boolean allowed = false;

        if (AuthorizationUtil.shortCutPermission())
        {
            allowed = true;
        }
        else
        {
            final Set<String> permissions = AuthorizationUtil.getPermissionStrings(object);
            /* workaround for internal call, to bypass security */
            allowed = permissions.contains(AuthenticationUtil.ROOT_USER) || checkPermissions(permissions, object);

            permissions.clear();
        }

        return allowed;
    }

    public boolean visit(IAbsence object)
    {
        return doVisit(object);
    }

    public boolean visit(IActivity object)
    {
        return doVisit(object);
    }

    public boolean visit(IApprovalNote object)
    {
        return doVisit(object);
    }

    public boolean visit(IApprovalNoteType object)
    {
        return doVisit(object);
    }

    public boolean visit(final IAssetAbstract object)
    {
        return doVisit(object);
    }

    public boolean visit(final IAssetRevision object)
    {
        return doVisit(object);
    }

    public boolean visit(IAssetRevisionFileRevision object)
    {
        return doVisit(object);
    }

    public boolean visit(final IAssetRevisionGroup object)
    {
        return doVisit(object);
    }

    public boolean visit(IAssetRevisionLink object)
    {
        return doVisit(object);
    }

    public boolean visit(final ICategory object)
    {
        return doVisit(object);
    }

    public boolean visit(IClassDynMetaDataType object)
    {
        // TODO special treatment
        return doVisit(object);
    }

    public boolean visit(final IComposition object)
    {
        return doVisit(object);
    }

    public boolean visit(IComputer object)
    {
        return doVisit(object);
    }

    public boolean visit(IComputerModel object)
    {
        return doVisit(object);
    }

    public boolean visit(IComputerType object)
    {
        return doVisit(object);
    }

    public boolean visit(final IConstituent object)
    {
        return doVisit(object);
    }

    public boolean visit(IConstituentLink object)
    {
        return doVisit(object);
    }

    public boolean visit(IContract object)
    {
        return doVisit(object);
    }

    public boolean visit(IDevice object)
    {
        return doVisit(object);
    }

    public boolean visit(IDeviceModel object)
    {
        return doVisit(object);
    }

    public boolean visit(IDeviceType object)
    {
        return doVisit(object);
    }

    public boolean visit(IDynMetaDataType object)
    {
        // TODO special treatment
        return doVisit(object);
    }

    public boolean visit(IDynMetaDataValue object)
    {
        // TODO special treatment
        return doVisit(object);
    }

    public boolean visit(IEntityTaskLink object)
    {
        return doVisit(object);
    }

    public boolean visit(IEvent object)
    {
        // FIXME no resource
        return true;
    }

    public boolean visit(final IExtraLine object)
    {
        return doVisit(object);
    }

    public boolean visit(IFact object)
    {
        // TODO see if it's correct
        return visit(object.getComposition());
    }

    public boolean visit(IFactType object)
    {
        // FIXME no resource ?
        return true;
    }

    public boolean visit(IFileAttribute object)
    {
        return doVisit(object);
    }

    public boolean visit(IFileRevision object)
    {
        return doVisit(object);
    }

    public boolean visit(IFileRevisionLink object)
    {
        return doVisit(object);
    }

    public boolean visit(IHint object)
    {
        // FIXME no resource ?
        return true;
    }

    public boolean visit(final IItem object)
    {
        return doVisit(object);
    }

    public boolean visit(final IItemGroup object)
    {
        return doVisit(object);
    }

    public boolean visit(ILicense object)
    {
        return doVisit(object);
    }

    public boolean visit(IManufacturer object)
    {
        return doVisit(object);
    }

    public boolean visit(IMileStone object)
    {
        return doVisit(object);
    }

    public boolean visit(IOrganization object)
    {
        return doVisit(object);
    }

    public boolean visit(final IPerson object)
    {
        return doVisit(object);
    }

    public boolean visit(final IPersonDay object)
    {
        return doVisit(object);
    }

    public boolean visit(final IPlanning object)
    {
        return doVisit(object);
    }

    public boolean visit(IPool object)
    {
        return doVisit(object);
    }

    public boolean visit(IProcessor object)
    {
        return doVisit(object);
    }

    public boolean visit(IProject object)
    {
        return doVisit(object);
    }

    public boolean visit(IQualification object)
    {
        return doVisit(object);
    }

    public boolean visit(IResourceGroup object)
    {
        return doVisit(object);
    }

    public boolean visit(final IRole object)
    {
        return doVisit(object);
    }

    public boolean visit(IRoom object)
    {
        return doVisit(object);
    }

    public boolean visit(IScreen object)
    {
        return doVisit(object);
    }

    public boolean visit(IScreenModel object)
    {
        return doVisit(object);
    }

    public boolean visit(IScript object)
    {
        return doVisit(object);
    }

    public boolean visit(final ISequence object)
    {
        return doVisit(object);
    }

    public boolean visit(final ISheet object)
    {
        return doVisit(object);
    }

    public boolean visit(final IShot object)
    {
        return doVisit(object);
    }

    public boolean visit(final ISimpleActivity object)
    {
        return doVisit(object);
    }

    public boolean visit(ISkill object)
    {
        return doVisit(object);
    }

    public boolean visit(ISkillLevel object)
    {
        return doVisit(object);
    }

    public boolean visit(ISoftware object)
    {
        return doVisit(object);
    }

    public boolean visit(IStudioMap object)
    {
        return doVisit(object);
    }

    public boolean visit(ITag object)
    {
        return doVisit(object);
    }

    public boolean visit(ITagCategory object)
    {
        return doVisit(object);
    }

    public boolean visit(final ITask object)
    {
        return doVisit(object);
    }

    public boolean visit(final ITaskActivity object)
    {
        return doVisit(object);
    }

    public boolean visit(final ITaskChanges object)
    {
        return doVisit(object);
    }

    public boolean visit(final ITaskGroup object)
    {
        return doVisit(object);
    }

    public boolean visit(ITaskType object)
    {
        return doVisit(object);
    }

    public boolean visit(IRoleTemplate object)
    {
        // SecurityTemplate / RoleTemplate
        return doVisit(object);
    }

    public boolean visit(IProjectSecurityTemplate object)
    {
        // SecurityTemplate / Template
        return doVisit(object);
    }

    public boolean visit(final IProjectType object)
    {
        return doVisit(object);
    }

    public boolean visit(final IWorkingTime object)
    {
        return doVisit(object);
    }

    public boolean visit(final IStep object)
    {
        return doVisit(object);
    }

    public boolean visit(final IPlayListEdit object)
    {
        return doVisit(object);
    }

    public boolean visit(final IPlayListRevision object)
    {
        return doVisit(object);
    }

    public boolean visit(final IFileRevisionAnnotation object)
    {
        return doVisit(object);
    }

    public boolean visit(final IListValues object)
    {
        return doVisit(object);
    }

    public boolean visit(IDynamicPath object)
    {
        return doVisit(object);
    }

    public boolean visit(final IGraphicalAnnotation object)
    {
        return doVisit(object);
    }

    public boolean visit(final IMountPoint object)
    {
        return doVisit(object);
    }

    public boolean visit(IProxyType object)
    {
        return doVisit(object);
    }

    public boolean visit(IProxy object)
    {
        return doVisit(object);
    }

	public boolean visit(IPlayListRevisionGroup object)
    {
        return doVisit(object);
    }
    
    public boolean visit(IHd3dACL object)
    {
        return doVisit(object);
    }

    /*-------------------
     * SPECIFIC METHODS |
     -------------------*/

    /**
     * Allows final operation to be modified by subclasses
     * 
     * @param permissions
     * @param modifier
     * @return
     */
    protected boolean checkPermissions(Set<String> permissions, IBase object)
    {
        if (isReadOnly(object))
        {
            return false;
        }
        try
        {
            return isAllowed(permissions);
        }
        catch (Exception e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            return false;
        }
    }

    /**
     * Checks whether the object belongs to the 'super admin' user, in which case it can only be read and not modified
     * nor deleted.
     * 
     * @param object
     *            the object to test
     * @return true if the object blongs to the 'super admin' user
     */
    protected boolean isReadOnly(IBase object)
    {
        boolean result = false;
        if (object instanceof IPerson)
        {
            result = AuthenticationUtil.ADMIN_USER.equals(((IPerson) object).getLogin());
        }
        if (object instanceof IResourceGroup)
        {
            result = AuthenticationUtil.ADMIN_GROUP.equals(((IResourceGroup) object).getName());
        }
        if (object instanceof IRole)
        {
            result = AuthenticationUtil.ADMIN_ROLE.equals(((IRole) object).getName());
        }
        result &= !(Customs.READ.equals(operationName()));
        return result;
    }

    /*-------------------------
     * STATIC UTILITY METHODS |
     -------------------------*/
    /**
     * Decides whether the user is allowed to access the object: for that, he must have at least one of the permissions.
     * 
     * @param permissions
     *            a list of permissions
     * @return true if the user has one of the permissions
     */
    private static boolean isAllowed(Set<String> permissions)
    {
        return AuthorizationUtil.canOneAtLeast(permissions);
    }

    /* TEST METHODS */

    public static void test()
    {
        CustomsVisitor visitor1 = new CustomsVisitor(CREATE);
        CustomsVisitor visitor2 = new CustomsVisitor(READ);
        CustomsVisitor visitor3 = new CustomsVisitor(DELETE);
        tr("----TEST OF PERMISSION RUNNING-------------------");
        IConstituent ex = null;
        IPerson lam = null;
        ITask l03 = null;
        try
        {
            // constituent
            ex = Persistors.constituent.getById(71121L);
            tr("label: " + ex.getLabel());
            tr("associated urls and perms : ");
            // tr(AuthorizationUtil.getAllPatterns(IConstituent.class));
            tr(visitor1.visit(ex));
            // person
            lam = Persistors.person.getById(71018L);
            tr("name: " + lam.getFirstName() + " " + lam.getLastName());
            tr("associated urls and perms: ");
            // tr(AuthorizationUtil.getAllPatterns(IPerson.class));
            tr(visitor2.visit(lam));
            // task
            l03 = Persistors.task.getById(71175L);
            tr("task: " + l03.getName() + " starts " + l03.getActualStartDate());
            tr("associated urls and perms: ");
            // tr(AuthorizationUtil.getAllPatterns(ITask.class));
            tr(visitor3.visit(l03));
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
        tr("----END OF TEST----------------------------------");
    }

    private static void tr(Object o)
    {
        try
        {
            Log.LOGGER.debug("----    " + o);
        }
        catch (Exception e)
        {
            Log.LOGGER.debug("----    " + o.toString());
        }
    }

}
