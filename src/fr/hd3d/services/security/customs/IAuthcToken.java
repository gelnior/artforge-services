package fr.hd3d.services.security.customs;

/**
 * The <code>IToken<code> specification for cookie-based authentication.
 * 
 * @author HD3D
 * 
 */
public interface IAuthcToken
{

    /**
     * Computes a future cookie mark with the token data. Token must not have been cleared in order to obtain an
     * accurate result. The token mark contains all there is to know about the token and its ciphered version will serve
     * as a cookie value.
     * 
     * @return the token mark
     */
    public String mark();

    /**
     * On the opposite of the {@link #mark()} method, this methods initializes all the token fields from a properly
     * specified mark. This mark will usually be retrieved from a deciphered cookie value.
     * 
     * @param mark
     */
    public void load(String mark);

    /**
     * @return whether the token is to be remembered more than one day.
     */
    public boolean rememberMe();

    public void setRememberMe(boolean rememberMe);

    /**
     * @return the login of the user to whom the token belongs to.
     */
    public String getPrincipal();

    public void setPrincipal(String principal);

    /**
     * @return the actual password of the user to whom the token belongs to.
     */
    public String getSecret();

    public void setSecret(String secret);

    /**
     * @return the date of token attribution.
     */
    public long getDate();

    public void setDate(long date);

    /**
     * @return the host address to which the token has been attributed.
     */
    public String getAddress();

    public void setAddress(String address);

    /**
     * Clears sensible informations, such as the secret.
     */
    public void clear();

    /**
     * Sets the token's attribution date to now.
     */
    public void refresh();

}
