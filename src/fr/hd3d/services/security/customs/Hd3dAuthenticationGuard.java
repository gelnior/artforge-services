package fr.hd3d.services.security.customs;

import static fr.hd3d.common.client.ServicesURI.LOGIN_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PASSWORD_ID_ATTRIBUTE;

import java.text.MessageFormat;
import java.util.Date;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.impl.ChainBase;
import org.apache.commons.chain.impl.ContextBase;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.shiro.SecurityUtils;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.ChallengeResponse;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.ClientInfo;
import org.restlet.data.Cookie;
import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.security.ChallengeAuthenticator;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.services.security.utils.CookieUtil;
import fr.hd3d.services.security.utils.LoginInfo;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


/**
 * The authenticating guard class. In order to authenticate the request, the server emits a cookie carrying a 'mark'
 * token. This mark follows the syntax : "{rememberMe?}/{dateOfDelivery}/{address}/{username}/{password}". It is encoded
 * into the cookie value and decoded for validation and check. The mark is also kept encrypted in memory, so that the
 * 'value' token traveling aboard the cookie can be recognized upon further submission.
 * 
 * @author HD3D
 * 
 */
public class Hd3dAuthenticationGuard extends ChallengeAuthenticator
{
    private final static String REALM = "HD3D Authentication Service";
    private final static String REQUEST = "request";
    private final static String RESPONSE = "response";
    private final static String AUTH_COOKIE = "authCookie";
    private final static String TOKEN = "token";

    private final static String MSG_0 = "cookie validated from {0}";
    private final static String MSG_1 = "cookie from {0} is stale!";
    private final static String MSG_2 = "cookie from {0} is not valid!";
    private final static String MSG_3 = "token validated from {0}";

    public Hd3dAuthenticationGuard(final Context context)
    {
        super(context, ChallengeScheme.HTTP_BASIC, REALM);
    }

    @Override
    public boolean authenticate(final Request request, final Response response)
    {
        boolean authenticated;

        /* find out the authentication cookie */
        final Cookie authCookie = CookieUtil.getAuthCookie(request.getCookies());

        /* authentication Cookie NOT FOUND */
        if (authCookie == null)
        {
            authenticated = doBasicAuthentication(request, response);
        }
        /* authentication Cookie FOUND */
        else
        {
            authenticated = doAuthentication(request, response, authCookie);
        }

        return authenticated;
    }

    private boolean doBasicAuthentication(final Request request, final Response response)
    {
        boolean authenticated = false;

        final ChallengeResponse challengeResponse = request.getChallengeResponse();

        if (challengeResponse != null && authenticateBasic(challengeResponse))
        {
            LoginInfo loginInfo = getLoginInfoFromChallengeResponse(request);

            AuthenticationUtil.refresh(new Date().getTime(), loginInfo, response);

            loginInfo.clear();
            challengeResponse.setSecret(new char[1]);
            authenticated = true;
        }
        else
        {
            /* send a challenge to the user */
            challenge(response, false);
            /* logs out the user (he indeed possesses no valid cookie) */
            AuthenticationUtil.logoutShiro();
        }

        return authenticated;
    }

    private static LoginInfo getLoginInfoFromChallengeResponse(final ChallengeResponse challengeResponse)
    {
        LoginInfo loginInfo = null;

        if (challengeResponse != null)
        {
            loginInfo = new LoginInfo();
            loginInfo.setUserName(challengeResponse.getIdentifier());
            loginInfo.setPassword(String.valueOf(challengeResponse.getSecret()));
        }
        return loginInfo;
    }

    public static LoginInfo getLoginInfoFromForm(final Form form, final Request request)
    {
        LoginInfo loginInfo = null;

        if (form != null && request != null)
        {
            loginInfo = new LoginInfo();
            loginInfo.setUserName(form.getFirstValue(LOGIN_ID_ATTRIBUTE));
            loginInfo.setPassword(form.getFirstValue(PASSWORD_ID_ATTRIBUTE)); // HTTPS needed
            loginInfo.setRemember(BooleanUtils.toBoolean(form.getFirstValue(AuthenticationUtil.REMEMBERME_ATTRIBUTE)));
            loginInfo.setAddress(request.getClientInfo().getAddress());
        }
        return loginInfo;
    }

    public static LoginInfo getLoginInfoFromModificationForm(final Form form, final Request request)
    {
        LoginInfo loginInfo = null;

        if (form != null && request != null)
        {
            loginInfo = new LoginInfo();
            loginInfo.setPassword(form.getFirstValue(ServicesURI.SECRET_ATTRIBUTE)); // HTTPS needed
            loginInfo.setNewPassword(form.getFirstValue(ServicesURI.NEW_PASS_ATTRIBUTE)); // HTTPS needed
            loginInfo.setAddress(request.getClientInfo().getAddress());
        }
        return loginInfo;
    }

    public LoginInfo getLoginInfoFromChallengeResponse(final Request request)
    {
        LoginInfo loginInfo = null;

        if (request != null)
        {
            loginInfo = getLoginInfoFromChallengeResponse(request.getChallengeResponse());
            loginInfo.setAddress(request.getClientInfo().getAddress());
        }

        return loginInfo;
    }

    // public static LoginInfo getLoginInfoFromForm(final Request request)
    // {
    // LoginInfo loginInfo = getLoginInfoFromForm(form);
    // loginInfo.setAddress(request.getClientInfo().getAddress());
    //
    // return loginInfo;
    // }

    /**
     * Logs a subject from a HTTP basic challenge response. It is used when the client has no cookie ability (like in
     * the case of scripts).
     * 
     * @param challengeResponse
     * @return
     */
    protected boolean authenticateBasic(final ChallengeResponse challengeResponse)
    {
        boolean authenticated = false;

        final String login = challengeResponse.getIdentifier();
        final String password = String.valueOf(challengeResponse.getSecret());
        final Object username = SecurityUtils.getSubject().getPrincipal();
        if (username == null || username.toString().equals(login))
        {
            try
            {
                AuthenticationUtil.loginShiro(login, password);
                authenticated = true;
            }
            catch (Exception e)
            {
                AuthenticationUtil.logoutShiro();
                Log.LOGGER.error(ExceptUtils.format(e));
            }
        }

        return authenticated;
    }

    @SuppressWarnings("serial")
    private boolean doAuthentication(final Request request, final Response response, final Cookie authCookie)
    {
        boolean authenticated;

        IAuthcToken token = null;
        try
        {
            /* create a token from cookie and initialize it */
            token = new Hd3dAuthcTokenImpl(AuthenticationUtil.getMark(authCookie));

            /* Build the context for chain authentication steps */

            final ContextBase context = new ContextBase();
            context.put(REQUEST, request);
            context.put(RESPONSE, response);
            context.put(AUTH_COOKIE, authCookie);
            context.put(TOKEN, token);

            final ChainBase chain = new ChainBase(new Command[] { new TokenCheckCmd(), new TokenAuthenticationCmd() });
            authenticated = chain.execute(context);
        }
        catch (Exception e)
        {
            Log.LOGGER.warn(ExceptUtils.format(e));
            authenticationFailed(request, response);
            authenticated = false;
        }

        if (token != null)
        {
            token.refresh();
        }

        return authenticated;
    }

    private static class TokenValidationCmd implements Command
    {
        public boolean execute(final org.apache.commons.chain.Context ctx) throws Hd3dException
        {
            final Request request = (Request) ctx.get(REQUEST);
            final IAuthcToken token = (IAuthcToken) ctx.get(TOKEN);

            if (isValid(request.getClientInfo(), token))
            {
                Log.LOGGER.info(MessageFormat.format(MSG_0, token.getAddress()));
                return CONTINUE_PROCESSING;
            }

            /* cookie not valid => AUTHENTICATION FAILED */
            throw new Hd3dException(MessageFormat.format(MSG_2, request.getClientInfo().getAddress()));
        }

        /**
         * Customized implementation for deciding if a cookie is worth being examined or not. Here I just check if the
         * given address and the given mark are recognized. I could go further and check user info to compare username,
         * date of delivery etc. Note that the 'time to live' check will be performed by the
         * <code>AddonAuthcUtils</code> class.
         * 
         * @param info
         * @param mark
         * @return returns true if the cookie can be accepted
         */
        protected boolean isValid(final ClientInfo info, final IAuthcToken token)
        {
            boolean valid = false;

            if (info != null && token != null)
            {
                /* FIXME find a way to check the address and validate local script requests */
                valid = ObjectUtils.equals(info.getAddress(), token.getAddress());

                valid &= AuthenticationUtil.isRegistered(token.mark());
            }

            return valid;
        }

        protected boolean isNotValid(final ClientInfo info, final IAuthcToken token)
        {
            return !isValid(info, token);
        }
    }

    private static class TokenCheckCmd implements Command
    {
        public boolean execute(final org.apache.commons.chain.Context ctx) throws Hd3dException
        {
            final IAuthcToken token = (IAuthcToken) ctx.get(TOKEN);

            /* token valid => CONTINUE */
            if (AuthenticationUtil.isValid(token))
            {
                Log.LOGGER.info(MessageFormat.format(MSG_3, token.getAddress()));
                return CONTINUE_PROCESSING;
            }

            /* token not valid => AUTHENTICATION FAILED */
            throw new Hd3dException(MessageFormat.format(MSG_1, token.getAddress()));
        }
    }

    private static class TokenAuthenticationCmd implements Command
    {
        public boolean execute(final org.apache.commons.chain.Context ctx) throws Hd3dException
        {
            final Response response = (Response) ctx.get(RESPONSE);
            final IAuthcToken token = (IAuthcToken) ctx.get(TOKEN);
            final Cookie authCookie = (Cookie) ctx.get(AUTH_COOKIE);

            /* SUCESSFUL Authentication */
            authenticateFromMark(token);
            CookieUtil
                    .refreshCookie(response, authCookie, token.rememberMe(), AuthenticationUtil.REMEMBER_TIME_TO_LIVE);
            token.clear(); // to protect memory
            return PROCESSING_COMPLETE;
        }

        /**
         * This function is an exception, meaning that it calls upon Shiro in spite of the fact it is not included in a
         * Utils final class. It logs the subject in if it is not already the case.
         * 
         * @param token
         */
        protected void authenticateFromMark(final IAuthcToken token)
        {
            /* already authenticated? bye! */
            if (!SecurityUtils.getSubject().isAuthenticated())
            {
                /* who are you? */
                /* token.getPrincipal() = Username */
                /* token.getSecret() = Password */
                AuthenticationUtil.loginShiro(token.getPrincipal(), token.getSecret());
            }
        }
    }

    protected void authenticationFailed(final Request request, final Response response)
    {
        /* Authentication FAILED */
        CookieUtil.deleteCookie(response);
        Log.LOGGER.warn(MessageFormat.format(MSG_2, request.getClientInfo().getAddress()));
        response.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
    }
}
