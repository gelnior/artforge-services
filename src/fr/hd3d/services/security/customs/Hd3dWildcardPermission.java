package fr.hd3d.services.security.customs;

import java.util.List;
import java.util.Set;

import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.WildcardPermission;

import fr.hd3d.utils.CollectUtils;


@SuppressWarnings("serial")
public class Hd3dWildcardPermission extends WildcardPermission
{
    protected Hd3dWildcardPermission()
    {}

    public Hd3dWildcardPermission(String wildcardString)
    {
        super(wildcardString, DEFAULT_CASE_SENSITIVE);
    }

    public Hd3dWildcardPermission(String wildcardString, boolean caseSensitive)
    {
        setParts(wildcardString, caseSensitive);
    }

    @Override
    public boolean implies(Permission p)
    {
        // By default only supports comparisons with other WildcardPermissions
        if (!(p instanceof WildcardPermission))
        {
            return false;
        }

        Hd3dWildcardPermission wp = (Hd3dWildcardPermission) p;

        List<Set<String>> otherParts = wp.getParts();

        /**
         * a hd3d permission string = [URI]:[OPERATOR] check permissions on [URI] first.
         */
        List<Set<String>> otherUriParts = CollectUtils.getAllExceptLast(otherParts);
        Set<String> otherOperatorPart = CollectUtils.getLastOrNull(otherParts);
        List<Set<String>> uriParts = CollectUtils.getAllExceptLast(getParts());
        Set<String> operatorPart = CollectUtils.getLastOrNull(getParts());
        // System.out.println("----------------------------");
        // System.out.println("otherUriParts=" + otherUriParts);
        // System.out.println("otherOperatorPart=" + otherOperatorPart);
        // System.out.println("uriParts=" + uriParts);
        // System.out.println("operatorPart=" + operatorPart);

        int i = 0;
        for (Set<String> otherPart : otherUriParts)
        {
            // If this permission has less parts than the other permission, everything after the number of parts
            // contained
            // in this permission is automatically implied, so return true
            if (uriParts.size() - 1 < i)
            {
                if (!uriParts.isEmpty())
                {
                    int index = (uriParts.size() - 1 < 0) ? 0 : uriParts.size() - 1;
                    Set<String> part = uriParts.get(index);
                    if (!part.contains(WILDCARD_TOKEN) && !part.containsAll(otherPart)
                            && !operatorPart.contains(WILDCARD_TOKEN) && !operatorPart.containsAll(otherOperatorPart))
                    {
                        return false;
                    }
                }

                return true;
            }
            else
            {
                Set<String> part = uriParts.get(i);
                if (!part.contains(WILDCARD_TOKEN) && !part.containsAll(otherPart))
                {
                    return false;
                }
                i++;
            }
        }

        // If this permission has more parts than the other parts, only imply it if all of the other parts are wildcards
        for (; i < uriParts.size(); i++)
        {
            Set<String> part = uriParts.get(i);
            if (!part.contains(WILDCARD_TOKEN))
            {
                return false;
            }
        }

        /**
         * check permission on [OPERATOR]
         */
        if (!operatorPart.contains(WILDCARD_TOKEN) && !operatorPart.containsAll(otherOperatorPart))
        {
            return false;
        }

        return true;
    }
}
