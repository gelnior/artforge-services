package fr.hd3d.services.security.customs;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.security.Authorizer;

import fr.hd3d.services.security.utils.AuthorizationUtil;


/**
 * A simple authorizer based on Shiro permissions that will restrict all requests. It will call upon the
 * {@link fr.hd3d.services.security.util.AuthorizationUtil} methods to verify the user's rights.
 * 
 * @author florent-della-valle
 * 
 */
public class Hd3dAuthorizer extends Authorizer
{
    protected boolean authorize(Request request, Response response)
    {
        return AuthorizationUtil.userMayAccessResource(request);
    }
}
