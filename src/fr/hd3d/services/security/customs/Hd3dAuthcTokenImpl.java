package fr.hd3d.services.security.customs;

import java.util.Date;

import org.apache.commons.lang.StringUtils;


/**
 * A simple <code>IToken</code> implementation.
 * 
 * @author HD3D
 * 
 */
public class Hd3dAuthcTokenImpl implements IAuthcToken
{

    /** the separator in mark */
    private static char SeP = '/';

    private boolean rememberMe;
    private long date;
    private String address;
    private String principal;
    private String secret;

    private String markBuffer;

    public Hd3dAuthcTokenImpl()
    {}

    public Hd3dAuthcTokenImpl(boolean rememberMe, long date, String address, String principal, String secret)
    {
        this.rememberMe = rememberMe;
        this.date = date;
        this.address = address;
        this.principal = principal;
        this.secret = secret;
    }

    public Hd3dAuthcTokenImpl(String mark)
    {
        load(mark);
    }

    public void clear()
    {
        secret = null;
        address = null;
        markBuffer = null;
    }

    public String getAddress()
    {
        return address;
    }

    public long getDate()
    {
        return date;
    }

    public String getPrincipal()
    {
        return principal;
    }

    public String getSecret()
    {
        return secret;
    }

    public String mark()
    {
        if (markBuffer == null)
        {
            // mark pattern = <rememberMe>/<date>/<adress>/<principal>/<secret>
            markBuffer = StringUtils.join(new Object[] { rememberMe, date, address, principal, secret }, SeP);
        }
        return markBuffer;
    }

    public boolean rememberMe()
    {
        return rememberMe;
    }

    public void setAddress(String address)
    {
        this.address = address;
        markBuffer = null;
    }

    public void setDate(long date)
    {
        this.date = date;
        markBuffer = null;
    }

    public void setPrincipal(String principal)
    {
        this.principal = principal;
        markBuffer = null;
    }

    public void setRememberMe(boolean rememberMe)
    {
        this.rememberMe = rememberMe;
        markBuffer = null;
    }

    public void setSecret(String secret)
    {
        this.secret = secret;
        markBuffer = null;
    }

    public void load(String mark)
    {
        if (mark == null)
        {
            return;
        }

        String[] parts = mark.split(String.valueOf(SeP));

        int size = parts.length;
        if (size >= 1)
        {
            rememberMe = Boolean.valueOf(parts[0]);
        }
        if (size >= 2)
        {
            date = Long.valueOf(parts[1]);
        }
        if (size >= 3)
        {
            address = parts[2];
        }
        if (size >= 4)
        {
            principal = parts[3];
        }
        if (size >= 5)
        {
            secret = parts[4];
        }
        // for (int k = 5; k < parts.length; k++)
        // {
        // secret += SeP + parts[k];
        // }

        markBuffer = mark;

    }

    public void refresh()
    {
        date = new Date().getTime();
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + (int) (date ^ (date >>> 32));
        result = prime * result + ((principal == null) ? 0 : principal.hashCode());
        result = prime * result + (rememberMe ? 1231 : 1237);
        result = prime * result + ((secret == null) ? 0 : secret.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Hd3dAuthcTokenImpl other = (Hd3dAuthcTokenImpl) obj;
        if (address == null)
        {
            if (other.address != null)
                return false;
        }
        else if (!address.equals(other.address))
            return false;
        if (date != other.date)
            return false;
        if (principal == null)
        {
            if (other.principal != null)
                return false;
        }
        else if (!principal.equals(other.principal))
            return false;
        if (rememberMe != other.rememberMe)
            return false;
        if (secret == null)
        {
            if (other.secret != null)
                return false;
        }
        else if (!secret.equals(other.secret))
            return false;
        return true;
    }

}
