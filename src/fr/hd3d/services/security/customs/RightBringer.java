package fr.hd3d.services.security.customs;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.PermissionResolver;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.ISheet;
import fr.hd3d.model.persistence.ITask;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.services.security.utils.AuthorizationUtil;
import fr.hd3d.utils.ExceptUtils;
import fr.hd3d.utils.Log;


/**
 * The purpose of this class is to add on creation of an object the permission to access it to the creator's roles.
 * Should be used only for certain object types like persons, projects, sheets, resource groups...
 * 
 * @author HD3D
 * 
 */
public class RightBringer extends CustomsVisitor
{

    public final static String MIN_RIGHTS = READ + "," + UPDATE + "," + DELETE;

    public RightBringer()
    {
        super(CREATE);
    }

    protected boolean checkPermissions(Set<String> permissions, IBase object)
    {
        String suffix;
        // find all instances for which we want the creator to be a "super user"
        if (object instanceof IProject || object instanceof ISheet)
        {
            suffix = ALL_RIGHTS;
        }
        else
        {
            suffix = MIN_RIGHTS;
        }
        try
        {
            // format = object.getId:suffix
            StringBuilder buf = new StringBuilder(50);
            buf.append(String.valueOf(object.getId())).append(':').append(suffix).trimToSize();
            addRightsToCurrentUser(permissions, buf.toString());
        }
        catch (Exception e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
            return false;
        }
        return true;
    }

    /* STATIC UTILITIES */

    @SuppressWarnings("serial")
    private static void addRightsToCurrentUser(Set<String> permBases, String permSuffix) throws Hd3dException
    {
        final IPerson user = AuthenticationUtil.getCurrentUser();

        if (user == null)
            return;

        // get all the current user's roles, bound to the groups he belongs to
        Set<IRole> roles = new HashSet<IRole>() {
            {
                for (IResourceGroup group : user.getResourceGroups())
                    addAll(group.getRoles());
            }
        };

        for (IRole role : roles)
        {
            addRightToRole(role, permBases, permSuffix);
        }
    }

    private static void addRightToRole(IRole role, Set<String> permBases, String permSuffix) throws Hd3dException
    {
        for (String permPrefix : permBases)
        {
            if (isWorth(role, permPrefix))
            {
                Set<String> permissions = role.getPermissions();
                permissions.add(formatPermission(permPrefix) + ":" + permSuffix);

                AuthorizationUtil.onAddingPermissions(permissions);
                role.setPermissions(permissions);

                try
                {
                    Persistors.role.merge(role);
                }
                catch (Hd3dPersistenceException e)
                {
                    throw new Hd3dException(permPrefix + " will not be added to " + role.getName(), e);
                }
                return;
            }
        }
    }

    private static boolean isWorth(IRole role, String permPrefix)
    {
        PermissionResolver resolver = AuthorizationUtil.getPermissionResolver();
        Permission creation = resolver.resolvePermission(permPrefix);
        Permission other;
        for (String perm : role.getPermissions())
        {
            other = resolver.resolvePermission(perm);
            if (other.implies(creation))
            {
                return true;
            }
        }
        return false;
    }

    private static String formatPermission(String permPrefix)
    {
        int last = permPrefix.lastIndexOf(":");
        String result = permPrefix.substring(0, last);
        last = permPrefix.lastIndexOf(":");
        result = permPrefix.substring(last + 1);
        result = ALL_RIGHTS + ":" + result;
        return result;
    }

    /* TEST METHODS */

    public static void test()
    {
        RightBringer bringer = new RightBringer();
        tr("----TEST OF PERMISSION RUNNING-------------------");
        IConstituent ex = null;
        IPerson lam = null;
        ITask l03 = null;
        try
        {
            // constituent
            ex = Persistors.constituent.getById(71121L);
            tr("label: " + ex.getLabel());
            tr(bringer.visit(ex));
            // person
            lam = Persistors.person.getById(71018L);
            tr("name: " + lam.getFirstName() + " " + lam.getLastName());
            tr(bringer.visit(lam));
            // task
            l03 = Persistors.task.getById(71175L);
            tr("task: " + l03.getName() + " starts " + l03.getActualStartDate());
            tr(bringer.visit(l03));
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error(ExceptUtils.format(e));
        }
        tr("----END OF TEST----------------------------------");
    }

    private static void tr(Object o)
    {
        try
        {
            Log.LOGGER.debug("----    " + o);
        }
        catch (Exception e)
        {
            Log.LOGGER.debug("----    " + o.toString());
        }
    }

}
