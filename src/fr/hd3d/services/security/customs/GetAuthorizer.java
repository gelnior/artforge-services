package fr.hd3d.services.security.customs;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Method;


/**
 * A simple authorizer based on Shiro authorizations that allows all GET requests.
 * 
 * @author HD3D
 * 
 */
public class GetAuthorizer extends Hd3dAuthorizer
{

    public boolean authorize(Request request, Response response)
    {
        return request.getMethod().equals(Method.GET) || super.authorize(request, response);
    }

}
