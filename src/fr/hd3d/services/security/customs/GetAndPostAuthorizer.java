package fr.hd3d.services.security.customs;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Method;


/**
 * A simple authorizer based on Shiro authorizations that allows all GET and PUT requests.
 * 
 * @author HD3D
 * 
 */
public class GetAndPostAuthorizer extends GetAuthorizer
{
    public boolean authorize(Request request, Response response)
    {
        return request.getMethod().equals(Method.POST) || super.authorize(request, response);
    }

}
