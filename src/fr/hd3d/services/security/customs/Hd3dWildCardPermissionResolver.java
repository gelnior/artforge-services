package fr.hd3d.services.security.customs;

import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.WildcardPermissionResolver;


public class Hd3dWildCardPermissionResolver extends WildcardPermissionResolver
{

    public Hd3dWildCardPermissionResolver()
    {
        super();
    }

    public Permission resolvePermission(String permissionString)
    {
        return new Hd3dWildcardPermission(permissionString);
    }

}
