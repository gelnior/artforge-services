package fr.hd3d.services.security.templates;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.model.lightweight.ILProjectSecurityTemplate;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.impl.hibernate.SimpleH;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.services.security.utils.SecurityTemplatesUtil;


@Entity
@Table(name = "hd3d_security_template")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class ProjectSecurityTemplateH extends SimpleH implements IProjectSecurityTemplate
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Set<IRoleTemplate> roles;
    private String comment;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_projectsecuritytemplate"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public ProjectSecurityTemplateH()
    {}

    public ProjectSecurityTemplateH(Long id, Timestamp version, String name, Set<IRoleTemplate> roles)
    {
        super(id, version, name);
        this.roles = roles;
    }

    public ProjectSecurityTemplateH(String name, Set<IRoleTemplate> roles)
    {
        this(null, null, name, roles);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @ManyToMany(targetEntity = RoleTemplateH.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    @JoinTable(name = "hd3d_security_template__role", joinColumns = @JoinColumn(name = "security_template_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    @BatchSize(size = 50)
    @Fetch(FetchMode.SELECT)
    @Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
    public Set<IRoleTemplate> getRoles()
    {
        if (roles == null)
            roles = new HashSet<IRoleTemplate>();
        return roles;
    }

    public void setRoles(Set<IRoleTemplate> roles)
    {
        this.roles = roles;
    }

    @Column(name = "comment", length = 1000)
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        String comm;
        if (comment != null && comment.length() > 999)
        {
            comm = comment.substring(0, 999);
        }
        else
        {
            comm = comment;
        }
        this.comment = comm;
    }

    /*------------------
     * equal & hashcode
     ------------------*/

    /*------------------
     * Transient methods
     ------------------*/
    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    public void applyTemplate(IProject project)
    {
        SecurityTemplatesUtil.applyTemplate(project, this);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public IBaseTranslator<ILProjectSecurityTemplate, IProjectSecurityTemplate> defaultTranslator()
    {
        return Translators.securitytemplate;
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
