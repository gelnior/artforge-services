package fr.hd3d.services.security.templates;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.ISimple;


@Hd3dEntityName("RoleTemplate")
public interface IRoleTemplate extends ISimple
{

    /**
     * Changes a role's attributes so that it be a copy of the role template. In particular, permissions and bans shall
     * be modified in order to refer to the specified project Id. The resource groups the role is related to, on the
     * other hand, are not modified.
     * 
     * @param projectId
     *            the covering project's Id
     * @param role
     *            the role to update
     */
    public void updateFromTemplate(Long projectId, IRole role);

    // get methods

    public Set<String> getPermissions();

    public Set<String> getBans();

    // set methods

    public void setName(String name);

    public void setPermissions(Set<String> permissions);

    public void setBans(Set<String> bans);

    // others

    public void addPermission(String permission);

    public void addBan(String ban);

}
