package fr.hd3d.services.security.templates;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;

import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.persistence.IRole;
import fr.hd3d.model.persistence.impl.hibernate.SimpleH;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.security.customs.Customs;
import fr.hd3d.services.security.utils.RESTPathUtil;
import fr.hd3d.services.security.utils.SecurityTemplatesUtil;


@javax.persistence.Entity
@javax.persistence.Table(name = "hd3d_role_template")
@FilterDef(name = "internalStatus", defaultCondition = ":internalStatus = internalStatus", parameters = @ParamDef(name = "internalStatus", type = "java.lang.Integer"))
@Filter(name = "internalStatus", condition = ":internalStatus = internalStatus")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.VERSION)
public class RoleTemplateH extends SimpleH implements IRoleTemplate
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Set<String> permissions;
    private Set<String> bans;

    /*-------------
     * id override
     -------------*/
    @Id
    @GeneratedValue(generator = "seq_gen")
    @GenericGenerator(name = "seq_gen", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "seq_roletemplate"),
            @Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1"),
            @Parameter(name = "optimizer", value = "pooled") })
    public Long getId()
    {
        return id;
    }

    /*-------------
     * Constructors
     -------------*/
    public RoleTemplateH()
    {}

    public RoleTemplateH(Long id, java.sql.Timestamp version, String name, Set<String> permissions, Set<String> bans)
    {
        super(id, version, name);
        this.permissions = permissions;
        this.bans = bans;
    }

    public RoleTemplateH(String name, Set<String> permissions, Set<String> bans)
    {
        this(null, null, name, permissions, bans);
    }

    /*------------------
     * Getters & Setters
     ------------------*/
    @CollectionOfElements
    @Column(name = "role_template_permissions")
    public Set<String> getPermissions()
    {
        if (permissions == null)
            permissions = new LinkedHashSet<String>();
        return permissions;
    }

    public void setPermissions(Set<String> permissions)
    {
        this.permissions = permissions;
    }

    @CollectionOfElements
    @Column(name = "role_template_bans")
    public Set<String> getBans()
    {
        if (bans == null)
            bans = new LinkedHashSet<String>();
        return bans;
    }

    public void setBans(Set<String> bans)
    {
        this.bans = bans;
    }

    /*------------------
     * equal & hashcode
     ------------------*/

    /*------------------
     * Transient methods
     ------------------*/
    protected boolean accept(Customs customs)
    {
        return customs.visit(this);
    }

    @Transient
    public boolean isApprovable()
    {
        return false;
    }

    @Transient
    public boolean isTaskable()
    {
        return false;
    }

    public void addPermission(String permission)
    {
        getPermissions().add(permission);
    }

    public void addBan(String ban)
    {
        getBans().add(ban);
    }

    public String defaultPath()
    {
        return RESTPathUtil.getDefaultPath(this);
    }

    public IBaseTranslator<ILRole, IRoleTemplate> defaultTranslator()
    {
        return Translators.roletemplates;
    }

    public void updateFromTemplate(Long projectId, IRole role)
    {
        role.setPermissions(SecurityTemplatesUtil.updateRightsWithProjectId(getPermissions(), projectId));
        role.setBans(SecurityTemplatesUtil.updateRightsWithProjectId(getBans(), projectId));
    }

    /*-------------------------------------------
     * ComplexTypeProvider (only for Enum & Date)
     -------------------------------------------*/

}
