package fr.hd3d.services.security.templates;

import static fr.hd3d.common.client.ServicesURI.ABSENCES;
import static fr.hd3d.common.client.ServicesURI.ACTIVITIES;
import static fr.hd3d.common.client.ServicesURI.APPROVALNOTES;
import static fr.hd3d.common.client.ServicesURI.ASSETABSTRACTS;
import static fr.hd3d.common.client.ServicesURI.ASSETREVISIONGROUPS;
import static fr.hd3d.common.client.ServicesURI.ASSETREVISIONLINKS;
import static fr.hd3d.common.client.ServicesURI.ASSETREVISIONS;
import static fr.hd3d.common.client.ServicesURI.ASSETS;
import static fr.hd3d.common.client.ServicesURI.CATEGORIES;
import static fr.hd3d.common.client.ServicesURI.CLASSDYNMETADATATYPES;
import static fr.hd3d.common.client.ServicesURI.CLEANUP;
import static fr.hd3d.common.client.ServicesURI.COMPUTERS;
import static fr.hd3d.common.client.ServicesURI.COMPUTER_MODELS;
import static fr.hd3d.common.client.ServicesURI.COMPUTER_TYPES;
import static fr.hd3d.common.client.ServicesURI.CONSTITUENTS;
import static fr.hd3d.common.client.ServicesURI.CONTRACTS;
import static fr.hd3d.common.client.ServicesURI.DAYS_AND_ACTIVITIES;
import static fr.hd3d.common.client.ServicesURI.DELETE;
import static fr.hd3d.common.client.ServicesURI.DEVICES;
import static fr.hd3d.common.client.ServicesURI.DEVICE_MODELS;
import static fr.hd3d.common.client.ServicesURI.DEVICE_TYPES;
import static fr.hd3d.common.client.ServicesURI.DOWNLOAD;
import static fr.hd3d.common.client.ServicesURI.DYNMETADATATYPES;
import static fr.hd3d.common.client.ServicesURI.DYNMETADATAVALUES;
import static fr.hd3d.common.client.ServicesURI.ENTITIES;
import static fr.hd3d.common.client.ServicesURI.ENTITYTASKLINKS;
import static fr.hd3d.common.client.ServicesURI.EVENTS;
import static fr.hd3d.common.client.ServicesURI.FILEREVISIONLINKS;
import static fr.hd3d.common.client.ServicesURI.FILEREVISIONS;
import static fr.hd3d.common.client.ServicesURI.FILE_ATTRIBUTES;
import static fr.hd3d.common.client.ServicesURI.FREE_QUERY;
import static fr.hd3d.common.client.ServicesURI.LICENSES;
import static fr.hd3d.common.client.ServicesURI.MANUFACTURERS;
import static fr.hd3d.common.client.ServicesURI.MILESTONES;
import static fr.hd3d.common.client.ServicesURI.ORGANIZATIONS;
import static fr.hd3d.common.client.ServicesURI.PERSONS;
import static fr.hd3d.common.client.ServicesURI.PLANNINGS;
import static fr.hd3d.common.client.ServicesURI.POOLS;
import static fr.hd3d.common.client.ServicesURI.PROCESSORS;
import static fr.hd3d.common.client.ServicesURI.PROJECTS;
import static fr.hd3d.common.client.ServicesURI.PROJECT_TYPES;
import static fr.hd3d.common.client.ServicesURI.QUALIFICATIONS;
import static fr.hd3d.common.client.ServicesURI.RESOURCEGROUPS;
import static fr.hd3d.common.client.ServicesURI.ROOMS;
import static fr.hd3d.common.client.ServicesURI.SCREENS;
import static fr.hd3d.common.client.ServicesURI.SCREEN_MODELS;
import static fr.hd3d.common.client.ServicesURI.SEARCH_FILES;
import static fr.hd3d.common.client.ServicesURI.SEQUENCES;
import static fr.hd3d.common.client.ServicesURI.SETTINGS;
import static fr.hd3d.common.client.ServicesURI.SHOTS;
import static fr.hd3d.common.client.ServicesURI.SIMPLE_ACTIVITIES;
import static fr.hd3d.common.client.ServicesURI.SKILLLEVELS;
import static fr.hd3d.common.client.ServicesURI.SKILLS;
import static fr.hd3d.common.client.ServicesURI.SOFTWARES;
import static fr.hd3d.common.client.ServicesURI.STUDIOMAPS;
import static fr.hd3d.common.client.ServicesURI.TASKS;
import static fr.hd3d.common.client.ServicesURI.TASK_ACTIVITIES;
import static fr.hd3d.common.client.ServicesURI.TASK_TYPES;
import static fr.hd3d.common.client.ServicesURI.UPLOAD;

import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.persistence.impl.hibernate.query.IHibernateTransaction;
import fr.hd3d.services.security.utils.SecurityTemplatesUtil;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.HibernateUtil;
import fr.hd3d.utils.Log;


/**
 * Contains a query that will initialize a default security template.
 * 
 * @author HD3D
 * 
 */
public class SecurityTemplatesInit
{

    // copy of the safe hibernate transaction run
    public static void securityTemplatesInit() throws Hd3dPersistenceException
    {
        Session session = HibernateUtil.currentSession();
        Transaction trx = session.beginTransaction();
        HibernateUtil.safeTransaction(new DefaultTemplate_Init());
        if (session.getFlushMode() == FlushMode.MANUAL)
        {
            session.flush();
        }
        trx.commit();
        trx = null;
        HibernateUtil.closeSession();
        Log.LOGGER.info("Default security template initialized");
    }

    private static String format(String pattern, String s)
    {
        return MessageFormat.format(pattern, StringUtils.removeStart(s, "/"));
    }

    private static class DefaultTemplate_Init implements IHibernateTransaction
    {
        private Session session;

        public void execute(Session session) throws Hd3dException, NoSuchAlgorithmException
        {
            this.session = session;

            /* create a default security template */
            IProjectSecurityTemplate template = getProjectTemplate("Default Security Template");

            if (StringUtils.isBlank(template.getComment()))
            {
                template.setComment("This template is the default security template, only provided for the "
                        + "purpose of performing tests and demos.");
            }

            /* create four default roles */
            IRoleTemplate supervisor = getRoleTemplate("Production Manager");
            IRoleTemplate graph = getRoleTemplate("Graphic Designer");
            IRoleTemplate comp = getRoleTemplate("Graphic Composer");
            IRoleTemplate RH = getRoleTemplate("Human Resources Manager");
            IRoleTemplate basic = getRoleTemplate("Default Security Template: Basic Rights");
            IRoleTemplate basicGraphiste = getRoleTemplate("Graphiste Basic");

            /* apply permissions to supervisor */
            if (supervisor != null && CollectUtils.isEmpty(supervisor.getPermissions()))
            {
                addProjectRight(supervisor, "*");
                supervisor.addPermission(format("*:{0}:*", '/' + EVENTS));
                supervisor.addPermission(format("*:{0}:*", '/' + MILESTONES));
                supervisor.addPermission(format("*:{0}:*:read", '/' + RESOURCEGROUPS));
                session.save(supervisor);
            }

            /* apply permissions to graph */
            if (graph != null && CollectUtils.isEmpty(graph.getPermissions()))
            {
                addProjectRight(graph, "read");
                addProjectRight(graph, format("{0}:*", '/' + ASSETABSTRACTS));
                addProjectRight(graph, format("{0}:*", '/' + ASSETREVISIONGROUPS));
                addProjectRight(graph, format("{0}:*", '/' + CATEGORIES));
                graph.addPermission(format("*:{0}:*", '/' + ASSETREVISIONLINKS));
                // graph.addPermission(format("*:{0}:*", '/' + ASSETREVISIONFILEREVISIONS));
                graph.addPermission(format("*:{0}:*", '/' + FILEREVISIONLINKS));
                graph.addPermission(format("*:{0}:*", '/' + FILE_ATTRIBUTES));
                session.save(graph);
            }

            /* apply permissions to comp */
            if (comp != null && CollectUtils.isEmpty(comp.getPermissions()))
            {
                comp.setPermissions(new HashSet<String>());
                addProjectRight(comp, format("{0}:*:read", '/' + ASSETABSTRACTS));
                addProjectRight(comp, format("{0}:*:read", '/' + ASSETREVISIONGROUPS));
                addProjectRight(comp, format("{0}:*", '/' + SEQUENCES));
                session.save(comp);
            }

            /* apply permissions to RH */
            if (RH != null && CollectUtils.isEmpty(RH.getPermissions()))
            {
                RH.addPermission(format("*:{0}:*:read", '/' + PLANNINGS));
                RH.addPermission(format("*:{0}:*:read", '/' + SKILLLEVELS));
                RH.addPermission(format("*:{0}:*:read", '/' + SKILLS));
                RH.addPermission(format("*:{0}:*:read", '/' + QUALIFICATIONS));
                RH.addPermission(format("*:{0}:*:read", '/' + RESOURCEGROUPS));
                RH.addPermission(format("*:{0}:*:read", '/' + ABSENCES));
                RH.addPermission(format("*:{0}:*:read", '/' + PERSONS));
                RH.addPermission(format("*:{0}:*:read", '/' + CONTRACTS));

                addProjectRight(RH, "read");

                RH.addPermission(format("*:{0}:*:read", '/' + ORGANIZATIONS));
                session.save(RH);
            }

            /* define basic rights */
            if (basic != null && CollectUtils.isEmpty(basic.getPermissions()))
            {
                basic.addPermission(format("*:{0}:*:read", '/' + ENTITIES));
                basic.addPermission(format("*:{0}:*:read", '/' + CLASSDYNMETADATATYPES));
                basic.addPermission(format("*:{0}:*:read", '/' + DYNMETADATATYPES));
                basic.addPermission(format("*:{0}:*:read", '/' + DYNMETADATAVALUES));
                basic.addPermission(format("*:{0}:*:read", '/' + ENTITYTASKLINKS));
                basic.addPermission(format("*:{0}:self:*", '/' + SETTINGS));
                basic.addPermission(format("*:{0}:*", '/' + FREE_QUERY));
                basic.addPermission(format("*:{0}:*", '/' + UPLOAD));
                basic.addPermission(format("*:{0}:*", '/' + DOWNLOAD));
                basic.addPermission(format("*:{0}:*", '/' + DELETE));
                basic.addPermission(format("*:{0}:*", '/' + CLEANUP));
                basic.addPermission(format("*:{0}:*", '/' + SEARCH_FILES));
                basic.addPermission(format("*:{0}:*:read", '/' + COMPUTERS));
                basic.addPermission(format("*:{0}:*:read", '/' + DEVICES));
                basic.addPermission(format("*:{0}:*:read", '/' + COMPUTER_TYPES));
                basic.addPermission(format("*:{0}:*:read", '/' + DEVICE_MODELS));
                basic.addPermission(format("*:{0}:*:read", '/' + DEVICES));
                basic.addPermission(format("*:{0}:*:read", '/' + DEVICE_TYPES));
                basic.addPermission(format("*:{0}:*:read", '/' + COMPUTER_MODELS));
                basic.addPermission(format("*:{0}:*:read", '/' + LICENSES));
                basic.addPermission(format("*:{0}:*:read", '/' + MANUFACTURERS));
                basic.addPermission(format("*:{0}:*:read", '/' + POOLS));
                basic.addPermission(format("*:{0}:*:read", '/' + PROCESSORS));
                basic.addPermission(format("*:{0}:*:read", '/' + ROOMS));
                basic.addPermission(format("*:{0}:*:read", '/' + SCREEN_MODELS));
                basic.addPermission(format("*:{0}:*:read", '/' + SCREENS));
                basic.addPermission(format("*:{0}:*:read", '/' + SOFTWARES));
                basic.addPermission(format("*:{0}:*:read", '/' + STUDIOMAPS));
                session.save(basic);
            }

            /* Graphiste role for basic application */
            if (basicGraphiste != null && CollectUtils.isEmpty(basicGraphiste.getPermissions()))
            {
                basicGraphiste.addPermission(format("*:{0}:*", '/' + TASK_ACTIVITIES));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + ASSETS));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + ASSETREVISIONS));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + TASK_TYPES));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + SETTINGS));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + TASKS));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + DAYS_AND_ACTIVITIES));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + ACTIVITIES));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + SHOTS));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + CONSTITUENTS));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + FILEREVISIONS));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + PROJECT_TYPES));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + APPROVALNOTES));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + SIMPLE_ACTIVITIES));
                basicGraphiste.addPermission(format("*:{0}:*", '/' + PERSONS));
                basicGraphiste.addPermission(format("*:{0}:*:read", '/' + PROJECTS));
                session.save(basicGraphiste);
            }

            /* add role templates to project security template */

            Set<IRoleTemplate> existingRoles = template.getRoles();

            Set<IRoleTemplate> roles = new HashSet<IRoleTemplate>();
            if (!existingRoles.contains(supervisor))
            {
                roles.add(supervisor);
            }
            if (!existingRoles.contains(graph))
            {
                roles.add(graph);
            }
            if (!existingRoles.contains(comp))
            {
                roles.add(comp);
            }
            if (!existingRoles.contains(RH))
            {
                roles.add(RH);
            }
            if (!existingRoles.contains(basicGraphiste))
            {
                roles.add(basicGraphiste);
            }
            if (CollectUtils.isNotEmpty(roles))
            {
                template.setRoles(roles);
                session.save(template);
                session.flush();
            }
        }

        // copy of the getPerson() method
        private IProjectSecurityTemplate getProjectTemplate(String name) throws Hd3dTranslationException, Hd3dException
        {
            /* Retrieve the security template. */
            IProjectSecurityTemplate object = (IProjectSecurityTemplate) Persistors.securitytemplate
                    .getObjectByValue_NoPermCheck(Const.NAME, name);

            /* If the security template is in database, return it. */
            if (object != null)
            {
                return object;
            }

            /* Else create a new security template instance. */
            object = new ProjectSecurityTemplateH(name, null);

            /* Save security template in database. */
            object.doPrePersist();
            session.save(object);
            session.flush();
            return object;
        }

        // copy of the getPerson() method
        private IRoleTemplate getRoleTemplate(String name) throws Hd3dException
        {
            /* Retrieve the role template. */
            IRoleTemplate object = (IRoleTemplate) Persistors.roletemplates.getObjectByValue_NoPermCheck(Const.NAME,
                    name);

            /* If the role template is in database, return it. */
            if (object != null)
            {
                return object;
            }

            /* Else create a new role template instance. */
            object = new RoleTemplateH(name, null, null);

            /* Save role template in database. */
            object.doPrePersist();
            session.save(object);
            session.flush();
            return object;
        }

        private void addProjectRight(IRoleTemplate role, String suffix)
        {
            role.addPermission(format("*:{0}:", '/' + PROJECTS) + SecurityTemplatesUtil.PROJECT_ID_MARK + ":" + suffix);
        }
    }

}
