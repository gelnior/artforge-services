package fr.hd3d.services.security.templates;

import java.util.Set;

import fr.hd3d.model.annotation.Hd3dEntityName;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISimple;


/**
 * A project security template model object
 * 
 * @author HD3D
 * 
 */
@Hd3dEntityName("ProjectSecurityTemplate")
public interface IProjectSecurityTemplate extends ISimple
{

    public Set<IRoleTemplate> getRoles();

    public void setRoles(Set<IRoleTemplate> roles);

    /**
     * Applies a security template to a project, creating all the project's specifically designed roles contained in the
     * template.
     * 
     * @param project
     *            the project to which the template will be applied
     */
    public void applyTemplate(IProject project);

    public String getComment();

    public void setComment(String comment);

}
