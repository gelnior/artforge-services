package fr.hd3d.services.security.resources;

import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.services.resources.AbstractHd3dBaseResource;


/**
 * A resource for testing one's authentication. It also works when server authentication is not activated, for it is
 * directly protected by Shiro filter.
 * 
 * @author HD3D
 * 
 * @param <L>
 * @param <P>
 */
public class Ping<L, P> extends AbstractHd3dBaseResource
{

    public Ping() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    public Representation get(Variant v)
    {
        return new StringRepresentation("pong");
    }

    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
