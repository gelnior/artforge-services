package fr.hd3d.services.security.resources;

import static fr.hd3d.common.client.ServicesURI.TEMPLATE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILProjectSecurityTemplate;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.AbstractHd3dResource;
import fr.hd3d.services.security.templates.IProjectSecurityTemplate;


/**
 * Resource for handling project security templates collectively.
 * 
 * URI : /security_templates/projects
 * 
 * @author HD3D
 * 
 */
@Hd3dComment("GET:Get a project security template \r\n" + "\r\nPUT:Update a project security template \r\n"
        + "\r\nPOST:N/A\r\n" + "\r\nDELETE:Delete project security template")
public class SecurityTemplateResource extends AbstractHd3dResource<ILProjectSecurityTemplate, IProjectSecurityTemplate>
{

    protected IProjectSecurityTemplate initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(TEMPLATE_ID_ATTRIBUTE, lockMode);
    }

    public SecurityTemplateResource() throws Hd3dException
    {
        super(Persistors.securitytemplate, Translators.securitytemplate);
    }

}
