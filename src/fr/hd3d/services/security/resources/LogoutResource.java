package fr.hd3d.services.security.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.services.resources.AbstractHd3dBaseResource;
import fr.hd3d.services.security.utils.AuthenticationUtil;


/**
 * Very simple resource for user logging out.
 * 
 * @author HD3D
 * 
 */
public class LogoutResource<L, P> extends AbstractHd3dBaseResource
{

    public LogoutResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("GET");
    }

    public Representation get(Variant arg0) throws ResourceException
    {
        try
        {
            AuthenticationUtil.doLogout(getRequest(), getResponse());
            String jsonCode = "{\"result\":\"ok\",\"message\":\"user logged out\"}";
            return new StringRepresentation(jsonCode, MediaType.APPLICATION_JSON);
        }
        catch (Exception e)
        {
            error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
            return getResponseEntity();
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
