package fr.hd3d.services.security.resources;

import java.util.HashSet;
import java.util.Set;

import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.services.resources.AbstractHd3dBaseResource;
import fr.hd3d.services.security.utils.AuthorizationUtil;


/**
 * A resource that returns connected user's permission list.
 * 
 * @author HD3D
 * 
 * @param <L>
 * @param <P>
 */
public class PermissionsResource<L, P> extends AbstractHd3dBaseResource
{

    public PermissionsResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    public Representation get(Variant variant) throws ResourceException
    {
        try
        {
            Set<String> permissions = new HashSet<String>(AuthorizationUtil.getCurrentUserPermissions());
            Representation r = new StringRepresentation(permissions.toString(), variant.getMediaType());
            r.setCharacterSet(CharacterSet.UTF_8);
            return r;
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
