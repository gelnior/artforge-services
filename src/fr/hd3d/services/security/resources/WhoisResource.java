package fr.hd3d.services.security.resources;

import static fr.hd3d.common.client.ResourceConst.ERROR_OCCURED;

import org.restlet.data.CharacterSet;
import org.restlet.data.Cookie;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.AbstractHd3dBaseResource;
import fr.hd3d.services.resources.serializer.RessourceSerializer;
import fr.hd3d.services.security.customs.Hd3dAuthcTokenImpl;
import fr.hd3d.services.security.customs.IAuthcToken;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.services.security.utils.CookieUtil;
import fr.hd3d.utils.Const;
import fr.hd3d.utils.Log;
import fr.hd3d.utils.ServerResourceUtils;


/**
 * When calling this resource you will be returned the current user as a 'Person' object.
 * 
 * @author HD3D
 * 
 */
@Hd3dComment("GET:Récupération de la liste de tout les utilisateur\r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Ajout d'un nouvelle utilisateur\r\n" + "\r\nDELETE:N/A")
public class WhoisResource<L, P> extends AbstractHd3dBaseResource
{

    public WhoisResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    public Representation get(Variant variant)
    {
        try
        {
            /* create a token from cookie and initialize it */
            final Cookie authCookie = CookieUtil.getAuthCookie(getRequest().getCookies());
            final IAuthcToken token = new Hd3dAuthcTokenImpl(AuthenticationUtil.getMark(authCookie));

            IPerson user = Persistors.person.getObjectByValue_NoPermCheck(Const.LOGIN, token.getPrincipal());
            if (user != null)
            {
                ILPerson lUser = Translators.person.toLightweight(user, getResourceContext());
                Representation r = RessourceSerializer.getRepresentation(variant, lUser);
                r.setCharacterSet(CharacterSet.UTF_8);

                getResponse().setStatus(Status.SUCCESS_OK);

                Log.LOGGER.info(responseInfo());

                getResponse().setEntity(ServerResourceUtils.encodeRepresentation(r));

                return getResponseEntity();

            }
            else
            {
                error(Status.CLIENT_ERROR_NOT_FOUND, null, "user cannot be indentified");
                return getResponseEntity();
            }
        }
        catch (Exception e)
        {
            error(Status.SERVER_ERROR_INTERNAL, e, ERROR_OCCURED);
            return getResponseEntity();
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
