package fr.hd3d.services.security.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.AbstractHd3dCollection;
import fr.hd3d.services.security.templates.IRoleTemplate;


/**
 * Resource for handling role templates collectively.
 * 
 * URI : /security_templates/roles
 * 
 * @author HD3D
 * 
 */
@Hd3dComment("GET:Gat all the role templates \r\n" + "\r\nPUT:N/A\r\n" + "\r\nPOST:Create a role template \r\n"
        + "\r\nDELETE:N/A")
public class RoleTemplatesResource extends AbstractHd3dCollection<ILRole, IRoleTemplate>
{

    public RoleTemplatesResource() throws Hd3dException
    {
        super(Persistors.roletemplates, Translators.roletemplates);
    }

}
