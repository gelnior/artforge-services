package fr.hd3d.services.security.resources;

import org.restlet.data.CharacterSet;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.services.resources.AbstractHd3dBaseResource;
import fr.hd3d.services.security.utils.AuthorizationUtil;


/**
 * A resource that returns all the server URI patterns.
 * 
 * @author HD3D
 * 
 * @param <L>
 * @param <P>
 */
public class ServicesURLsResource<L, P> extends AbstractHd3dBaseResource
{

    public ServicesURLsResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods(null);
    }

    public Representation get(Variant variant) throws ResourceException
    {
        try
        {
            String result = AuthorizationUtil.getAllURLPatterns().toString();
            Representation r = new StringRepresentation(result, variant.getMediaType());
            r.setCharacterSet(CharacterSet.UTF_8);
            return r;
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
