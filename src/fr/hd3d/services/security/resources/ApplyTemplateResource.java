package fr.hd3d.services.security.resources;

import static fr.hd3d.common.client.ServicesURI.PROJECT_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.TEMPLATE_ID_ATTRIBUTE;

import org.hibernate.LockMode;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILProjectSecurityTemplate;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.AbstractHd3dResource;
import fr.hd3d.services.resources.ResourceContext;
import fr.hd3d.services.security.templates.IProjectSecurityTemplate;
import fr.hd3d.utils.Log;


/**
 * Resource for applying a security template to a given project.
 * 
 * URI : /security_templates/apply_template/{templateId}/{projectId}
 * 
 * @author HD3D
 * 
 */
@Hd3dComment("GET:N/A \r\n"
        + "\r\nPOST:Apply a template to a project, creating all the specific roles defined in the template\r\n"
        + "\r\nPUT:N/A\r\n" + "\r\nDELETE:N/A")
public class ApplyTemplateResource extends AbstractHd3dResource<ILProject, IProject>
{
    public ApplyTemplateResource() throws Hd3dException
    {
        super(Persistors.project, Translators.project);
        allowMethods("POST");
    }

    protected IProject initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(PROJECT_ID_ATTRIBUTE, lockMode);
    }

    public Representation get(Variant variant)
    {
        getResponse().setStatus(Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
        return null;
    }

    public Representation post(Representation entity, Variant variant)
    {
        final ResourceContext<ILProject, IProject> context = getResourceContext();
        if (false == initObject())
        {
            return getResponseEntity();
        }

        Long templateId = null;
        IProjectSecurityTemplate template;
        try
        {
            templateId = context.getIdFromUrl(TEMPLATE_ID_ATTRIBUTE);

            ResourceContext<ILProjectSecurityTemplate, IProjectSecurityTemplate> secuTemplateCtx = new ResourceContext<ILProjectSecurityTemplate, IProjectSecurityTemplate>();
            template = Persistors.securitytemplate.getById(secuTemplateCtx, templateId, LockMode.PESSIMISTIC_WRITE);
            template.applyTemplate(object);
            getResponse().setStatus(Status.SUCCESS_ACCEPTED);
            return getResponseEntity();
        }
        catch (Hd3dException e)
        {
            Log.LOGGER.error("Template with id " + templateId + " could not be retrieved");
            getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
            return getResponseEntity();
        }
    }

}
