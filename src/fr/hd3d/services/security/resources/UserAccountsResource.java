package fr.hd3d.services.security.resources;

import static fr.hd3d.common.client.ServicesURI.PRINCIPAL_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.SECRET_ATTRIBUTE;

import java.util.Date;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.services.resources.AbstractHd3dBaseResource;
import fr.hd3d.services.security.customs.Hd3dAuthenticationGuard;
import fr.hd3d.services.security.dao.Hd3dRealm;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.services.security.utils.LoginInfo;
import fr.hd3d.services.security.utils.UserAccountsUtil;
import fr.hd3d.utils.HibernateUtil;


/**
 * Resource for handling user accounts.
 * 
 * URI : /useraccounts/{userAccountId}
 * 
 * @author HD3D
 * 
 */
@Hd3dComment("GET:N/A \r\n" + "\r\nPUT:Change user password \r\n" + "\r\nPOST:Create account for user \r\n"
        + "\r\nDELETE:Delete user account")
public class UserAccountsResource<L, P> extends AbstractHd3dBaseResource
{

    private String username;

    public UserAccountsResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("POST,PUT,DELETE");

        // init user
        username = (String) getRequest().getAttributes().get(PRINCIPAL_ATTRIBUTE);
        if (username == null || AuthenticationUtil.isRootUser(username))
        {
            getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
            getResponse().setEntity(new StringRepresentation("must specify a valid user principal"));
        }
    }

    public Representation get(Variant variant)
    {
        try
        {
            getResponse().setStatus(Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
            return null;
        }
        finally
        {
            username = null;
            cleanup();
        }
    }

    public Representation post(Representation entity, Variant variant) throws ResourceException
    {
        // if (!AuthenticationUtil.isSuperUser())
        // {
        // // wonder if it is a good choice, but only "super_admin" could create new user accounts
        // throw new ResourceException(Status.CLIENT_ERROR_FORBIDDEN,
        // "only super_admin role possessing users can create or delete user accounts");
        // }
        Form form = new Form(entity);
        String password = form.getFirstValue(SECRET_ATTRIBUTE); // HTTPS needed
        if (password == null)
        {
            password = UserAccountsUtil.DEFAULT_PASS;
        }

        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();
        try
        {
            UserAccountsUtil.createAccount(username, password);
            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            tx.commit();
        }
        catch (Hd3dException he)
        {
            throw new ResourceException(Status.CLIENT_ERROR_CONFLICT, he.getMessage());
        }
        finally
        {
            if (tx != null)
            {
                tx = null;
            }
            cleanup();
        }

        password = null; // to protect memory
        return getResponse().getEntity();
    }

    public Representation delete(Variant variant) throws ResourceException
    {
        // if (!AuthenticationUtil.isSuperUser())
        // {
        // // wonder if it is a good choice, but only "super_admin" could create new user accounts
        // throw new ResourceException(Status.CLIENT_ERROR_FORBIDDEN,
        // "only super_admin role possessing users can create or delete user accounts");
        // }
        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();
        try
        {
            UserAccountsUtil.deleteAccount(username);
            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            tx.commit();
            Hd3dRealm.resetLastInfo(username);
        }
        catch (Hd3dException he)
        {
            throw new ResourceException(Status.CLIENT_ERROR_CONFLICT, he.getMessage());
        }
        finally
        {
            if (tx != null)
            {
                tx = null;
            }
            cleanup();
        }

        return getResponse().getEntity();
    }

    public Representation put(Representation entity, Variant variant) throws ResourceException
    {
        final Form form = new Form(entity);

        final LoginInfo loginInfo = Hd3dAuthenticationGuard.getLoginInfoFromModificationForm(form, getRequest());
        loginInfo.setUserName(username);

        if (loginInfo.getNewPassword() == null)
        {
            throw new ResourceException(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY, "must specifiy new password");
        }

        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();
        try
        {
            UserAccountsUtil.updateAccount(username, loginInfo.getPassword(), loginInfo.getNewPassword());
            if (session.getFlushMode() == FlushMode.MANUAL)
            {
                session.flush();
            }
            tx.commit();
            AuthenticationUtil.refresh(new Date().getTime(), loginInfo, getResponse());
            Hd3dRealm.resetLastInfo(username);
        }
        catch (Hd3dPersistenceException he)
        {
            throw new ResourceException(Status.CLIENT_ERROR_CONFLICT, he.getMessage());
        }
        catch (Hd3dException he)
        {
            throw new ResourceException(Status.CLIENT_ERROR_FORBIDDEN, he.getMessage());
        }
        finally
        {
            if (tx != null)
            {
                tx = null;
            }
            cleanup();
        }

        loginInfo.clear();
        return getResponse().getEntity();
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

    // public Representation represent(Variant arg0) throws ResourceException
    // {
    // String form = "<h3>You must enter the user credentials and a new password</h3>";
    // form += "<form action='/Hd3dServices/v1/passwords' method='POST'>";
    // form += "<table align='left' border='0' cellspacing='0' cellpadding='3'>" + "<tr>";
    // form += "<td>Login :</td>";
    // form += "<td><input type='text' name='" + PRINCIPAL_ATTRIBUTE + "' maxlength='50'/></td>"
    // + "</tr>" + "<tr>";
    // form += "<td>Mot de passe :</td>";
    // form += "<td><input type='password' name='" + SECRET_ATTRIBUTE + "' maxlength='50'/></td>"
    // + "</tr>" + "<tr>";
    // form += "<td>Nouveau mot de passe :</td>";
    // form += "<td><input type='password' name='" + NEW_PASS_ATTRIBUTE + "' maxlength='50'/></td>" + "</tr>" + "<tr>";
    // form +=
    // "<td colspan='2' align='right'><input type='submit' name='submit' value='Changer'/></td></tr></table></form>";
    // return new StringRepresentation(form, MediaType.TEXT_HTML);
    // }

}
