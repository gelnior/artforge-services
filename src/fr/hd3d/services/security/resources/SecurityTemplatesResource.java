package fr.hd3d.services.security.resources;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILProjectSecurityTemplate;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.AbstractHd3dCollection;
import fr.hd3d.services.security.templates.IProjectSecurityTemplate;


/**
 * Resource for handling the project security template with specified Id.
 * 
 * URI : /security_templates/projects/{projectId}
 * 
 * @author HD3D
 * 
 */
@Hd3dComment("GET:Get all project security templates \r\n" + "\r\nPUT:N/A\r\n"
        + "\r\nPOST:Create a project security template \r\n" + "\r\nDELETE:N/A")
public class SecurityTemplatesResource extends
        AbstractHd3dCollection<ILProjectSecurityTemplate, IProjectSecurityTemplate>
{

    public SecurityTemplatesResource() throws Hd3dException
    {
        super(Persistors.securitytemplate, Translators.securitytemplate);
    }

}
