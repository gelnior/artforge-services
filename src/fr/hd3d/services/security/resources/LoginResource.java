package fr.hd3d.services.security.resources;

import static fr.hd3d.common.client.ServicesURI.LOGIN;
import static fr.hd3d.common.client.ServicesURI.LOGIN_ID_ATTRIBUTE;
import static fr.hd3d.common.client.ServicesURI.PASSWORD_ID_ATTRIBUTE;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;

import fr.hd3d.exception.Hd3dAuthorizationException;
import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.persistence.IPerson;
import fr.hd3d.services.resources.AbstractHd3dBaseResource;
import fr.hd3d.services.security.customs.Hd3dAuthenticationGuard;
import fr.hd3d.services.security.utils.AuthenticationUtil;
import fr.hd3d.services.security.utils.LoginInfo;
import fr.hd3d.utils.Log;


/**
 * A resource for handling proper user connection
 * 
 * @author HD3D
 * 
 */
public class LoginResource<L, P> extends AbstractHd3dBaseResource
{

    private static Long LAPSE_TIME = -10000L;// 10 seconds
    private static ConcurrentMap<String, Date> authenticationBuffer = new ConcurrentHashMap<String, Date>();

    public LoginResource() throws Hd3dException
    {
        super();
        getResourceContext().complete();
        allowMethods("POST");
    }

    /**
     * Checks if another similar request from the same client is on its way before processing the current request.
     */
    public Representation post(Representation entity, Variant variant)
    {
        try
        {
            final Form form = new Form(entity);

            final LoginInfo loginInfo = Hd3dAuthenticationGuard.getLoginInfoFromForm(form, getRequest());

            Date now = new Date();
            if (!authenticationBuffer.containsKey(loginInfo.getUserName())
                    || authenticationBuffer.get(loginInfo.getUserName()).getTime() - now.getTime() < LAPSE_TIME)
            {
                authenticationBuffer.put(loginInfo.getUserName(), now);
                performLogin(loginInfo);
                authenticationBuffer.remove(loginInfo.getUserName());
                getResponse().setStatus(Status.SUCCESS_ACCEPTED);
            }
            else
            {
                Log.LOGGER.error("User " + loginInfo.getUserName() + " tried to log twice in a too short span");
                getResponse().setStatus(Status.CLIENT_ERROR_REQUEST_TIMEOUT);
            }
            return getResponseEntity();
        }
        catch (Hd3dAuthorizationException e)
        {
            return getResponseEntity();
        }
        finally
        {
            cleanup();
        }
    }

    public void performLogin(final LoginInfo loginInfo) throws Hd3dAuthorizationException
    {
        String jsonCode;
        boolean ok = AuthenticationUtil.doLogin(loginInfo, getRequest(), getResponse());
        if (ok)
        {
            jsonCode = "{\"result\":\"ok\",\"message\":\"" + AuthenticationUtil.OK + "\"";
            IPerson user = AuthenticationUtil.getCurrentUser();
            if (user != null)
            {
                jsonCode += ",\"userId\":" + user.getId();
            }
            jsonCode += "}";
        }
        else
        {
            jsonCode = "{\"result\":\"ko\",\"cause\":\"" + AuthenticationUtil.KO_CREDENTIALS + "\"}";
        }
        getResponse().setEntity(new StringRepresentation(jsonCode, MediaType.APPLICATION_JSON));
    }

    /* temporary */
    public Representation get(Variant arg0) throws ResourceException
    {
        try
        {
            String form = "<h3>You must log in in order to proceed</h3>";
            form += "<form action='/Hd3dServices" + '/' + LOGIN + "' method='POST'>";
            form += "<table align='left' border='0' cellspacing='0' cellpadding='3'>" + "<tr>";
            form += "<td>Login :</td>";
            form += "<td><input type='text' name='" + LOGIN_ID_ATTRIBUTE + "' maxlength='50'/></td>" + "</tr>" + "<tr>";
            form += "<td>Mot de passe :</td>";
            form += "<td><input type='password' name='" + PASSWORD_ID_ATTRIBUTE + "' maxlength='50'/></td>" + "</tr>"
                    + "<tr>";
            form += "<td><input type='checkbox' name='" + AuthenticationUtil.REMEMBERME_ATTRIBUTE
                    + "'/>Remember me?</td>" + "</tr>" + "<tr>";
            form += "<td colspan='2' align='right'><input type='submit' name='submit' value='Login'/></td></tr></table></form>";
            return new StringRepresentation(form, MediaType.TEXT_HTML);
        }
        finally
        {
            cleanup();
        }
    }

    @Override
    protected String getNewResourceIdentifier(Object object)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
