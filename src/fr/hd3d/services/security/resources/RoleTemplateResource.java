package fr.hd3d.services.security.resources;

import static fr.hd3d.common.client.ServicesURI.ROLE_ID_ATTRIBUTE;

import org.hibernate.LockMode;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.model.annotation.Hd3dComment;
import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.persistence.Persistors;
import fr.hd3d.model.translator.Translators;
import fr.hd3d.services.resources.AbstractHd3dResource;
import fr.hd3d.services.security.templates.IRoleTemplate;


/**
 * Resource for handling the role template with specified Id.
 * 
 * URI : /security_templates/roles/{roleId}
 * 
 * @author HD3D
 * 
 */
@Hd3dComment("GET:Get a role template \r\n" + "\r\nPUT:Update a role template \r\n" + "\r\nPOST:N/A\r\n"
        + "\r\nDELETE:Disable a role template")
public class RoleTemplateResource extends AbstractHd3dResource<ILRole, IRoleTemplate>
{

    protected IRoleTemplate initializeObject(LockMode lockMode) throws Hd3dException
    {
        return super.initializeObject(ROLE_ID_ATTRIBUTE, lockMode);
    }

    public RoleTemplateResource() throws Hd3dException
    {
        super(Persistors.roletemplates, Translators.roletemplates);
    }

}
