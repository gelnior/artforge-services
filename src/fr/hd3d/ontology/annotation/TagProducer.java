package fr.hd3d.ontology.annotation;

import java.util.Collection;

import org.hd3d2.sp4.common.upload.Annotation;
import org.hd3d2.sp4.common.upload.Asset;
import org.hd3d2.sp4.common.upload.AssetPart;

import fr.hd3d.model.persistence.ITag;
import fr.hd3d.utils.CollectUtils;


public class TagProducer extends BaseProducer<ITag>
{
    public TagProducer(ITag tag)
    {
        super(tag);
    }

    public TagProducer(Collection<ITag> tags)
    {
        super(tags);
    }

    protected String annotationDescription()
    {
        return "Reference Annotation for Tags";
    }

    protected void populateAssetPart(AssetPart assetPart, ITag tag)
    {
        assetPart.setSrc(BaseProducer.BASEURI + "tags/" + tag.getId());
        assetPart.getAnnotations().add(name(tag));
        assetPart.getAnnotations().add(tagCategory(tag));
    }

    @Override
    protected void populateAsset(Asset asset, ITag tag)
    {
        asset.setSrc(BaseProducer.BASEURI + "tags/" + tag.getId());
        asset.getAnnotations().add(name(tag));
        asset.getAnnotations().add(tagCategory(tag));
    }

    private Annotation name(ITag tag)
    {
        Annotation anno = getBaseAnnotation(tag);
        anno.setTypeRef(PREFIX_HD3D + "name");
        anno.setValue(nullSafeToString(tag.getName()));

        return anno;
    }

    private Annotation tagCategory(ITag tag)
    {
        Annotation anno = getBaseAnnotation(tag);
        anno.setTypeRef(PREFIX_HD3D + "tagCategory");
        anno.setValue("TagCategory/" + CollectUtils.getIds(tag.getCategories()));

        return anno;
    }

}
