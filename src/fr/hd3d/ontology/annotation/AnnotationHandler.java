package fr.hd3d.ontology.annotation;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import org.hd3d2.sp4.common.upload.Upload;

import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.model.persistence.IShot;
import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.model.persistence.Persistent;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;


public class AnnotationHandler
{
    Set<Persistent> entitiesForAnnotation;
    List<org.hd3d2.sp4.common.upload.Upload> uploads = new CopyOnWriteArrayList<Upload>();

    public AnnotationHandler(Set<Persistent> entitiesForAnnotation)
    {
        this.entitiesForAnnotation = entitiesForAnnotation;
    }

    public List<org.hd3d2.sp4.common.upload.Upload> getUploads()
    {
        return uploads;
    }

    @SuppressWarnings("unchecked")
    public void buildAnnotations()
    {
        if (Conf.isAnnotationOn())
        {
            /* produce annotations for the following entities */
            Collection<IFileRevision> fileRevisions = (Collection<IFileRevision>) CollectUtils.select(
                    entitiesForAnnotation, IFileRevision.class);
            if (CollectUtils.isNotEmpty(fileRevisions))
                uploads.addAll(new FileRevisionProducer(fileRevisions).getUploads());

            Collection<IProject> projects = (Collection<IProject>) CollectUtils.select(entitiesForAnnotation,
                    IProject.class);
            if (CollectUtils.isNotEmpty(projects))
                uploads.addAll(new ProjectProducer(projects).getUploads());

            Collection<ICategory> categories = (Collection<ICategory>) CollectUtils.select(entitiesForAnnotation,
                    ICategory.class);
            if (CollectUtils.isNotEmpty(categories))
                uploads.addAll(new CategoryProducer(categories).getUploads());

            Collection<IComposition> compositions = (Collection<IComposition>) CollectUtils.select(
                    entitiesForAnnotation, IComposition.class);
            if (CollectUtils.isNotEmpty(compositions))
                uploads.addAll(new CompositionProducer(compositions).getUploads());

            Collection<IConstituent> constituents = (Collection<IConstituent>) CollectUtils.select(
                    entitiesForAnnotation, IConstituent.class);
            if (CollectUtils.isNotEmpty(constituents))
                uploads.addAll(new ConstituentProducer(constituents).getUploads());

            Collection<IShot> shots = (Collection<IShot>) CollectUtils.select(entitiesForAnnotation, IShot.class);
            if (CollectUtils.isNotEmpty(shots))
                uploads.addAll(new ShotProducer(shots).getUploads());

            Collection<ISequence> sequences = (Collection<ISequence>) CollectUtils.select(entitiesForAnnotation,
                    ISequence.class);
            if (CollectUtils.isNotEmpty(sequences))
                uploads.addAll(new SequenceProducer(sequences).getUploads());

            Collection<ITagCategory> tagCategories = (Collection<ITagCategory>) CollectUtils.select(
                    entitiesForAnnotation, ITagCategory.class);
            if (CollectUtils.isNotEmpty(tagCategories))
                uploads.addAll(new TagCategoryProducer(tagCategories).getUploads());
        }
    }

    public void sendAnnotations() throws IOException
    {
        if (CollectUtils.isNotEmpty(uploads))
        {
            new AnnotationSender(BaseProducer.ANNOTATION_SERVER_URI, uploads).send();
        }
    }

}
