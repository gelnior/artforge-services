package fr.hd3d.ontology.annotation;

import java.util.Collection;

import org.hd3d2.sp4.common.upload.Annotation;
import org.hd3d2.sp4.common.upload.Asset;
import org.hd3d2.sp4.common.upload.AssetPart;

import fr.hd3d.model.persistence.ITagCategory;
import fr.hd3d.utils.CollectUtils;


public class TagCategoryProducer extends BaseProducer<ITagCategory>
{
    public TagCategoryProducer(ITagCategory tagCategory)
    {
        super(tagCategory);
    }

    public TagCategoryProducer(Collection<ITagCategory> tagCategories)
    {
        super(tagCategories);
    }

    protected String annotationDescription()
    {
        return "Reference Annotation for TagCategories";
    }

    protected void populateAssetPart(AssetPart assetPart, ITagCategory tagCategory)
    {
        assetPart.setSrc(BaseProducer.BASEURI + "tag-categories/" + tagCategory.getId());
        assetPart.getAnnotations().add(name(tagCategory));
        assetPart.getAnnotations().add(parent(tagCategory));
        if (CollectUtils.isNotEmpty(tagCategory.getBoundTags()))
        {
            assetPart.getAnnotations().add(boundTags(tagCategory));
        }
    }

    @Override
    protected void populateAsset(Asset asset, ITagCategory tagCategory)
    {
        asset.setSrc(BaseProducer.BASEURI + "tag-categories/" + tagCategory.getId());
        asset.getAnnotations().add(name(tagCategory));
        asset.getAnnotations().add(parent(tagCategory));
        if (CollectUtils.isNotEmpty(tagCategory.getBoundTags()))
        {
            asset.getAnnotations().add(boundTags(tagCategory));
        }
    }

    private Annotation name(ITagCategory tagCategory)
    {
        Annotation anno = getBaseAnnotation(tagCategory);
        anno.setTypeRef(PREFIX_HD3D + "name");
        anno.setValue(nullSafeToString(tagCategory.getName()));

        return anno;
    }

    private Annotation parent(ITagCategory tagCategory)
    {
        Annotation anno = getBaseAnnotation(tagCategory);
        anno.setTypeRef(PREFIX_HD3D + "parentTagCategory");
        anno.setValue("TagCategory/" + CollectUtils.nullSafeId(tagCategory.getParent()));

        return anno;
    }

    private Annotation boundTags(ITagCategory tagCategory)
    {
        Annotation anno = getBaseAnnotation(tagCategory);
        anno.setTypeRef(PREFIX_HD3D + "boundTags");
        anno.setValue("BoundTags/" + CollectUtils.getIds(tagCategory.getBoundTags()));

        return anno;
    }

}
