package fr.hd3d.ontology.annotation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hd3d2.sp4.common.upload.Annotation;
import org.hd3d2.sp4.common.upload.Asset;
import org.hd3d2.sp4.common.upload.AssetPart;
import org.restlet.data.MediaType;
import org.restlet.service.MetadataService;

import fr.hd3d.model.persistence.IFileRevision;
import fr.hd3d.model.persistence.IProxy;
import fr.hd3d.utils.StringUtils;


public class FileRevisionProducer extends BaseProducer<IFileRevision>
{
    final static String NAMESPACE = PREFIX_HD3D + "SearchableAsset/";

    public FileRevisionProducer(IFileRevision fileRevision)
    {
        super(fileRevision);
    }

    public FileRevisionProducer(Collection<IFileRevision> fileRevisions)
    {
        super(fileRevisions);
    }

    protected String annotationDescription()
    {
        return "Reference Annotation for FileRevisions";
    }

    // protected List<Upload> prepareUploads(Collection<IFileRevision> objects)
    // {
    // List<Upload> uploads = new ArrayList<Upload>();
    //
    // /* create different Uploads for different kind of File types */
    // MultiMap<String, IFileRevision> fileRevisionsByType = new MultiHashMap<String, IFileRevision>();
    // String format;
    // MetadataService md = new MetadataService();
    // MediaType type;
    // for (IFileRevision object : objects)
    // {
    // format = object.getFormat();
    // if (format == null)
    // {
    // fileRevisionsByType.put("Document", object);
    // }
    // else
    // {
    // type = md.getMediaType(format);
    // if (MediaType.IMAGE_ALL.includes(type))
    // {
    // fileRevisionsByType.put("StillImage", object);
    // }
    // else if (MediaType.VIDEO_ALL.includes(type))
    // {
    // fileRevisionsByType.put("MovingImage", object);
    // }
    // else if (isDocumentType(type))
    // {
    // fileRevisionsByType.put("Document", object);
    // }
    // else
    // { /* TODO : handle other types here */
    // fileRevisionsByType.put("Document", object);
    // }
    // }
    // }
    //
    // /* StillImage Upload */
    // collectUploads(uploads, fileRevisionsByType.get("StillImage"), "StillImage");
    // collectUploads(uploads, fileRevisionsByType.get("MovingImage"), "MovingImage");
    // collectUploads(uploads, fileRevisionsByType.get("Document"), "Document");
    //
    // return uploads;
    // }

    private String getRefTypeString(IFileRevision fileRevision)
    {
        if (fileRevision == null)
            return "";

        MetadataService md = new MetadataService();
        MediaType type;
        String format = fileRevision.getFormat();
        if (format == null)
        {
            return "Document";
        }
        else
        {
            type = md.getMediaType(format);
            if (MediaType.IMAGE_ALL.includes(type))
            {
                return "StillImage";
            }
            else if (MediaType.VIDEO_ALL.includes(type))
            {
                return "MovingImage";
            }
            else if (isDocumentType(type))
            {
                return "Document";
            }
            else
            { /* TODO : handle other types here */
                return "Document";
            }
        }
    }

    // private void collectUploads(List<Upload> uploads, Collection<IFileRevision> fileRevisions, String type)
    // {
    // Upload upload = null;
    // if (CollectUtils.isNotEmpty(fileRevisions))
    // {
    // upload = new Upload();
    // if ("StillImage".equalsIgnoreCase(type))
    // {
    // upload.getNamespaces().put("h2sa", PREFIX_HD3D + "StillImage");
    // }
    // else if ("MovingImage".equalsIgnoreCase(type))
    // {
    // upload.getNamespaces().put("h2sa", PREFIX_HD3D + "MovingImage");
    // }
    // else if ("Document".equalsIgnoreCase(type))
    // {
    // upload.getNamespaces().put("h2sa", PREFIX_HD3D + "Document");
    // }
    // else
    // {
    // /* TODO: handle other types here */
    // upload.getNamespaces().put("h2sa", PREFIX_HD3D + "Document");
    // }
    //
    // for (IFileRevision f : fileRevisions)
    // {
    // Asset asset = new Asset();
    // populateAsset(asset, f);
    // upload.getAssets().add(asset);
    // }
    // if (!upload.getAssets().isEmpty())
    // {
    // uploads.add(upload);
    // }
    // }
    // }

    protected void populateAsset(Asset asset, IFileRevision fileRevision)
    {
        // Date validationDate;
        // name = filename
        asset.setSrc(BaseProducer.BASEURI + "file-revisions/" + fileRevision.getId());
        asset.getAnnotations().add(rdfType(fileRevision));
        asset.getAnnotations().add(name(fileRevision));
        asset.getAnnotations().add(creationDate(fileRevision));
        asset.getAnnotations().add(byteSize(fileRevision));
        asset.getAnnotations().add(description(fileRevision));
        asset.getAnnotations().addAll(author(fileRevision));
        // asset.getAnnotations().add(validationStatus(fileRevision));
        // asset.getAnnotations().add(key(fileRevision));
        // asset.getAnnotations().add(variation(fileRevision));
        // asset.getAnnotations().add(revision(fileRevision));
        asset.getAnnotations().addAll(project(fileRevision));
        asset.getAnnotations().add(format(fileRevision));
        asset.getAnnotations().add(proxy(fileRevision));
    }

    protected void populateAssetPart(AssetPart assetPart, IFileRevision fileRevision)
    {}

    private List<Annotation> project(IFileRevision fileRevision)
    {
        List<Annotation> annos = new ArrayList<Annotation>();

        Annotation uriAnno = getBaseAnnotation(fileRevision);
        uriAnno.setTypeRef(NAMESPACE + "project");

        Annotation nameAnno = getBaseAnnotation(fileRevision);
        nameAnno.setTypeRef(NAMESPACE + "projectName");

        if (fileRevision.getAssetRevision() != null && fileRevision.getAssetRevision().getProject() != null)
        {
            uriAnno.setValue(_BASEURI + String.valueOf(fileRevision.getAssetRevision().getProject().defaultPath()));
            nameAnno.setValue(fileRevision.getAssetRevision().getProject().getName());
        }
        else
        {
            uriAnno.setValue("");// do not use null, makes annotation server crash
            nameAnno.setValue("");
        }

        annos.add(uriAnno);
        annos.add(nameAnno);

        return annos;
    }

    private Annotation byteSize(IFileRevision fileRevision)
    {
        Annotation anno = getBaseAnnotation(fileRevision);
        anno.setTypeRef(NAMESPACE + "byteSize");
        anno.setValue(String.valueOf(fileRevision.getSize()));

        return anno;
    }

    private Annotation format(IFileRevision fileRevision)
    {
        MetadataService md = new MetadataService();
        MediaType type;
        String format = fileRevision.getFormat();
        if (format == null)
        {
            type = MediaType.APPLICATION_OCTET_STREAM;
        }
        else
        {
            type = md.getMediaType(format);
        }

        Annotation anno = getBaseAnnotation(fileRevision);
        anno.setTypeRef(NAMESPACE + "format");
        if (type == null)
        {
            type = MediaType.APPLICATION_OCTET_STREAM;

        }
        anno.setValue(type.toString());
        // anno.setValue(org.apache.commons.lang.StringUtils.defaultString(fileRevision.getFormat()));

        return anno;
    }

    private Annotation proxy(IFileRevision fileRevision)
    {
        Annotation anno = getBaseAnnotation(fileRevision);
        anno.setTypeRef(NAMESPACE + "preview");

        /* lookup thumbnail proxy */
        IProxy thumbnailProxy = null;
        for (IProxy proxy : fileRevision.getProxies())
        {
            if (proxy.getProxyType() == null)
                continue;

            if ("thumbnail".equalsIgnoreCase(proxy.getProxyType().getName())
                    && proxy.getProxyType().getOpenType() != null
                    && "image".equalsIgnoreCase(proxy.getProxyType().getOpenType()))
            {
                thumbnailProxy = proxy;
                break;
            }
        }

        if (thumbnailProxy == null)
        {
            anno.setValue("");
        }
        else
        {
            String fullPath = thumbnailProxy.getMountPoint() + '/' + thumbnailProxy.getLocation() + '/'
                    + thumbnailProxy.getFilename();
            anno.setValue(org.apache.commons.lang.StringUtils.defaultString(fullPath));
        }

        return anno;
    }

    private List<Annotation> author(IFileRevision fileRevision)
    {
        List<Annotation> annos = new ArrayList<Annotation>();

        Annotation uriAnno = getBaseAnnotation(fileRevision);
        uriAnno.setTypeRef(NAMESPACE + "author");

        Annotation nameAnno = getBaseAnnotation(fileRevision);
        nameAnno.setTypeRef(NAMESPACE + "authorName");

        if (fileRevision.getCreator() != null)
        {
            uriAnno.setValue(_BASEURI + fileRevision.getCreator().defaultPath());
            nameAnno.setValue(fileRevision.getCreator().fullName());
        }
        else
        {
            uriAnno.setValue("");
            nameAnno.setValue("");
        }

        annos.add(uriAnno);
        annos.add(nameAnno);

        return annos;
    }

    // private Annotation validationStatus(IFileRevision fileRevision)
    // {
    // Annotation anno = getBaseAnnotation(fileRevision);
    // anno.setTypeRef(PREFIX_HD3D + "validationStatus");
    // if (fileRevision.getState() != null)
    // {
    // anno.setValue(fileRevision.getState().toString());
    // }
    // else
    // {
    // anno.setValue("");
    // }
    //
    // return anno;
    // }

    private Annotation creationDate(IFileRevision fileRevision)
    {
        Annotation anno = getBaseAnnotation(fileRevision);
        anno.setTypeRef(NAMESPACE + "creationDate");
        if (fileRevision.getCreationDate() != null)
        {
            anno.setValue(StringUtils.formatIso(fileRevision.getCreationDate()));
        }
        else
        {
            anno.setValue("");
        }

        return anno;
    }

    private Annotation rdfType(IFileRevision fileRevision)
    {
        Annotation anno = getBaseAnnotation(fileRevision);
        anno.setTypeRef("rdf:type");
        anno.setValue(PREFIX_HD3D + getRefTypeString(fileRevision));

        return anno;
    }

    private Annotation name(IFileRevision fileRevision)
    {
        Annotation anno = getBaseAnnotation(fileRevision);
        anno.setTypeRef(NAMESPACE + "name");
        anno.setValue(org.apache.commons.lang.StringUtils.defaultString(fileRevision.getRelativePath()));

        return anno;
    }

    private Annotation description(IFileRevision fileRevision)
    {
        Annotation anno = getBaseAnnotation(fileRevision);
        anno.setTypeRef(NAMESPACE + "description");
        anno.setValue(org.apache.commons.lang.StringUtils.defaultString(fileRevision.getComment()));

        return anno;
    }

    // private Annotation key(IFileRevision fileRevision)
    // {
    // Annotation anno = getBaseAnnotation(fileRevision);
    // anno.setTypeRef(PREFIX_HD3D + ":Key");
    // anno.setValue(org.apache.commons.lang.StringUtils.defaultString(fileRevision.getKey()));
    //
    // return anno;
    // }
    //
    // private Annotation variation(IFileRevision fileRevision)
    // {
    // Annotation anno = getBaseAnnotation(fileRevision);
    // anno.setTypeRef(PREFIX_HD3D + ":Variation");
    // anno.setValue(org.apache.commons.lang.StringUtils.defaultString(fileRevision.getVariation()));
    //
    // return anno;
    // }
    //
    // private Annotation revision(IFileRevision fileRevision)
    // {
    // Annotation anno = getBaseAnnotation(fileRevision);
    // anno.setTypeRef(PREFIX_HD3D + ":Revision");
    // anno.setValue(org.apache.commons.lang.StringUtils.defaultString(String.valueOf(fileRevision.getRevision())));
    //
    // return anno;
    // }

    // private Annotation fileRevisionCategory(IFileRevision fileRevision)
    // {
    // Annotation anno = getBaseAnnotation(fileRevision);
    // anno.setTypeRef(PREFIX_REF + ":fileRevisionCategory");
    // anno.setValue("fileRevisionCategory/" + CollectUtils.getIds(fileRevision.getCategories()));
    //
    // return anno;
    // }
    //
    // // TODO
    // private Annotation fileRevisiongedEntities(IFileRevision fileRevision)
    // {
    // Annotation anno = getBaseAnnotation(fileRevision);
    // anno.setTypeRef(PREFIX_REF + ":fileRevisiongedEntities");
    // anno.setValue("fileRevisiongedEntity/" + CollectUtils.getIds(fileRevision.getCategories()));
    //
    // return anno;
    // }

}
