package fr.hd3d.ontology.annotation;

import java.util.Collection;

import org.hd3d2.sp4.common.upload.Annotation;
import org.hd3d2.sp4.common.upload.Asset;
import org.hd3d2.sp4.common.upload.AssetPart;

import fr.hd3d.model.persistence.IShot;
import fr.hd3d.utils.CollectUtils;


public class ShotProducer extends BaseProducer<IShot>
{
    public ShotProducer(IShot object)
    {
        super(object);
    }

    public ShotProducer(Collection<IShot> objects)
    {
        super(objects);
    }

    protected String annotationDescription()
    {
        return "Reference Annotation for Shots";
    }

    protected void populateAssetPart(AssetPart assetPart, IShot shot)
    {
        assetPart.setSrc(BaseProducer.BASEURI + "projects/" + shot.getSequence().getProject().getId() + "/sequences/"
                + shot.getSequence().getId() + "/shots" + shot.getId());
        assetPart.getAnnotations().add(sequence(shot));
        assetPart.getAnnotations().add(label(shot));
        assetPart.getAnnotations().add(description(shot));
        assetPart.getAnnotations().add(nbFrame(shot));
    }

    @Override
    protected void populateAsset(Asset asset, IShot shot)
    {
        asset.setSrc(BaseProducer.BASEURI + "projects/" + shot.getSequence().getProject().getId() + "/sequences/"
                + shot.getSequence().getId() + "/shots" + shot.getId());
        asset.getAnnotations().add(sequence(shot));
        asset.getAnnotations().add(label(shot));
        asset.getAnnotations().add(description(shot));
        asset.getAnnotations().add(nbFrame(shot));
    }

    private Annotation sequence(IShot shot)
    {
        Annotation anno = getBaseAnnotation(shot);
        anno.setTypeRef(PREFIX_HD3D + "sequence");
        anno.setValue("Sequence/" + zeroIfNull(CollectUtils.nullSafeId(shot.getSequence())));
        return anno;
    }

    private Annotation label(IShot shot)
    {
        Annotation anno = getBaseAnnotation(shot);
        anno.setTypeRef(PREFIX_HD3D + "label");
        anno.setValue(nullSafeToString(shot.getLabel()));
        return anno;
    }

    private Annotation description(IShot shot)
    {
        Annotation anno = getBaseAnnotation(shot);
        anno.setTypeRef(PREFIX_HD3D + "description");
        anno.setValue(nullSafeToString(shot.getDescription()));
        return anno;
    }

    private Annotation nbFrame(IShot shot)
    {
        Annotation anno = getBaseAnnotation(shot);
        anno.setTypeRef(PREFIX_HD3D + ":NbFrame");
        anno.setValue(String.valueOf(shot.getNbFrame()));
        return anno;
    }

}
