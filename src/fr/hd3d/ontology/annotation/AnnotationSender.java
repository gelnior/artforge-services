package fr.hd3d.ontology.annotation;

import java.io.IOException;
import java.util.List;

import org.hd3d2.sp4.common.upload.Upload;
import org.hd3d2.sp4.common.upload.UploadResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;
import org.restlet.resource.ClientResource;
import org.restlet.service.ConverterService;

import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;
import fr.hd3d.utils.Log;


public class AnnotationSender
{
    private String annotationServerUri;
    private List<Upload> uploads;

    public AnnotationSender(String annotationServerUri, List<Upload> uploads)
    {
        this.annotationServerUri = annotationServerUri;
        this.uploads = uploads;
    }

    public void send() throws IOException
    {
        for (Upload upload : CollectUtils.nullSafe(uploads))
        {
            /* trace */
            ConverterService cs = new ConverterService();
            Representation in = cs.toRepresentation(upload, new Variant(MediaType.TEXT_XML), null);
            if (!Conf.isInProductionState())
            {
                in.write(System.out);
            }

            // 3) Effectively send the upload
            final ClientResource client = new ClientResource(new Context(), Method.POST, annotationServerUri);

            UploadResource uploadsResource = client.getChild("/upload/", UploadResource.class);
            Log.LOGGER.info("UPLOADING ANNOTATION...");
            String uploadRef = uploadsResource.send(upload);
        }
    }
}
