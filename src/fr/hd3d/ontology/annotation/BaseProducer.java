package fr.hd3d.ontology.annotation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hd3d2.sp4.common.upload.Annotation;
import org.hd3d2.sp4.common.upload.Asset;
import org.hd3d2.sp4.common.upload.AssetPart;
import org.hd3d2.sp4.common.upload.Upload;
import org.restlet.data.MediaType;
import org.restlet.data.Metadata;

import fr.hd3d.model.persistence.IBase;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Conf;


public abstract class BaseProducer<T extends IBase>
{
    public final static String BASEURI;
    public final static String _BASEURI;
    public final static String ANNOTATION_SERVER_URI;

    public final static String PREFIX_HD3D = "http://ontology.hd3d.fr/2011/09/";
    // public final static String PREFIX = PREFIX_HD3D + ":2minutes/reference/";
    // public final static String PREFIX_REF = PREFIX_HD3D + ":reference/";

    static
    {
        BASEURI = Conf.getBaseUri();
        _BASEURI = StringUtils.chomp(BASEURI, "/");
        ANNOTATION_SERVER_URI = Conf.getAnnotationServerUri();
    }

    public T object = null;
    public Collection<T> objects = null;

    public BaseProducer(T object)
    {
        this.object = object;
    }

    protected Annotation getBaseAnnotation(T object)
    {
        Annotation anno = new Annotation();
        return anno;
    }

    public BaseProducer(Collection<T> objects)
    {
        this.objects = objects;
    }

    public List<Upload> getUploads()
    {
        List<Upload> uploads = new ArrayList<Upload>();
        if (object != null)
        {
            Upload upload = getBaseUpload();
            populateUpload(upload, object);
            uploads.add(upload);
        }

        else if (CollectUtils.isNotEmpty(objects))
        {
            uploads.addAll(prepareUploads(objects));
            // populateUpload(upload, objects);
        }

        return uploads;
    }

    protected void populateUpload(Upload upload, T object)
    {
        if (upload == null)
            return;

        Asset asset = new Asset();

        populateAsset(asset, object);

        upload.getAssets().add(asset);
    }

    protected List<Upload> prepareUploads(Collection<T> objects)
    {
        List<Upload> uploads = new ArrayList<Upload>();

        Upload upload = getBaseUpload();
        for (T object : objects)
        {
            Asset asset = new Asset();
            populateAsset(asset, object);
            upload.getAssets().add(asset);
        }

        uploads.add(upload);
        return uploads;
    }

    protected void populateUpload(Upload upload, Collection<T> objects)
    {
        if (upload == null)
            return;

        for (T object : objects)
        {
            Asset asset = new Asset();
            populateAsset(asset, object);
            upload.getAssets().add(asset);
        }
    }

    protected abstract String annotationDescription();

    protected abstract void populateAssetPart(AssetPart assetPart, T object);

    protected abstract void populateAsset(Asset asset, T object);

    protected Upload getBaseUpload()
    {
        // 1) Prepare the upload
        Upload upload = new Upload();
        upload.setBaseRef(BASEURI);
        upload.getNamespaces().put("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        upload.getNamespaces().put("h2sa", "http://ontology.hd3d.fr/2011/09/SearchableAsset/");
        return upload;
    }

    public static String nullSafeToString(final String string)
    {
        return string == null ? "null" : string;
    }

    public static Long zeroIfNull(final Long l)
    {
        return l == null ? 0L : l;
    }

    public static boolean isDocumentType(Metadata type)
    {
        return MediaType.TEXT_ALL.includes(type) || MediaType.APPLICATION_MSOFFICE_DOCM.includes(type)
                || MediaType.APPLICATION_MSOFFICE_DOCX.includes(type)
                || MediaType.APPLICATION_MSOFFICE_DOTM.includes(type)
                || MediaType.APPLICATION_MSOFFICE_DOTX.includes(type)
                || MediaType.APPLICATION_MSOFFICE_ONETOC.includes(type)
                || MediaType.APPLICATION_MSOFFICE_ONETOC2.includes(type)
                || MediaType.APPLICATION_MSOFFICE_POTM.includes(type)
                || MediaType.APPLICATION_MSOFFICE_POTX.includes(type)
                || MediaType.APPLICATION_MSOFFICE_PPAM.includes(type)
                || MediaType.APPLICATION_MSOFFICE_PPSM.includes(type)
                || MediaType.APPLICATION_MSOFFICE_PPSM.includes(type)
                || MediaType.APPLICATION_MSOFFICE_PPSX.includes(type)
                || MediaType.APPLICATION_MSOFFICE_PPTM.includes(type)
                || MediaType.APPLICATION_MSOFFICE_PPTX.includes(type)
                || MediaType.APPLICATION_MSOFFICE_SLDM.includes(type)
                || MediaType.APPLICATION_MSOFFICE_SLDX.includes(type)
                || MediaType.APPLICATION_MSOFFICE_XLAM.includes(type)
                || MediaType.APPLICATION_MSOFFICE_XLSB.includes(type)
                || MediaType.APPLICATION_MSOFFICE_XLSM.includes(type)
                || MediaType.APPLICATION_MSOFFICE_XLSX.includes(type)
                || MediaType.APPLICATION_MSOFFICE_XLTM.includes(type)
                || MediaType.APPLICATION_MSOFFICE_XLTX.includes(type) || MediaType.APPLICATION_PDF.includes(type)
                || MediaType.APPLICATION_RTF.includes(type);

    }
}
