package fr.hd3d.ontology.annotation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hd3d2.sp4.common.upload.Annotation;
import org.hd3d2.sp4.common.upload.Asset;
import org.hd3d2.sp4.common.upload.AssetPart;

import fr.hd3d.model.persistence.IComposition;
import fr.hd3d.utils.CollectUtils;


public class CompositionProducer extends BaseProducer<IComposition>
{
    public CompositionProducer(IComposition object)
    {
        super(object);
    }

    public CompositionProducer(Collection<IComposition> objects)
    {
        super(objects);
    }

    protected String annotationDescription()
    {
        return "Reference Annotation for Casting";
    }

    protected void populateAssetPart(AssetPart assetPart, IComposition compo)
    {
        assetPart.setSrc(BaseProducer.BASEURI + "compositions/" + compo.getId());
        assetPart.getAnnotations().addAll(casting(compo));
    }

    @Override
    protected void populateAsset(Asset asset, IComposition compo)
    {
        asset.setSrc(BaseProducer.BASEURI + "compositions/" + compo.getId());
        asset.getAnnotations().addAll(casting(compo));
    }

    private List<Annotation> casting(IComposition compo)
    {
        List<Annotation> ret = new ArrayList<Annotation>();

        Annotation anno = new Annotation();

        if (compo.getShot() != null)
        {
            anno.setTypeRef(PREFIX_HD3D + "Shot/Casting");
            anno.setValue("Shot/" + compo.getShot().getId());
            ret.add(anno);

            /* reverse annotation */
            Annotation reverse = new Annotation(PREFIX_HD3D + "Shot/" + compo.getShot().getId(), "");
            reverse.setTypeRef(PREFIX_HD3D + "Constituent/Casting");
            reverse.setValue("Constituent/" + CollectUtils.nullSafeId(compo.getConstituent()));
            ret.add(reverse);
        }
        else if (compo.getSequence() != null)
        {
            anno.setTypeRef(PREFIX_HD3D + "Sequence/Casting");
            anno.setValue("Sequence/" + compo.getSequence().getId());
            ret.add(anno);

            /* reverse annotation */
            Annotation reverse = new Annotation(PREFIX_HD3D + "Sequence/" + compo.getSequence().getId(), "");
            reverse.setTypeRef(PREFIX_HD3D + "Constituent/Casting");
            reverse.setValue("Constituent/" + CollectUtils.nullSafeId(compo.getConstituent()));
            ret.add(reverse);
        }
        return ret;
    }
}
