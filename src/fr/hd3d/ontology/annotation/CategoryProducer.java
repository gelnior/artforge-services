package fr.hd3d.ontology.annotation;

import java.util.Collection;

import org.hd3d2.sp4.common.upload.Annotation;
import org.hd3d2.sp4.common.upload.Asset;
import org.hd3d2.sp4.common.upload.AssetPart;

import fr.hd3d.model.persistence.ICategory;
import fr.hd3d.utils.CollectUtils;


public class CategoryProducer extends BaseProducer<ICategory>
{

    public CategoryProducer(ICategory object)
    {
        super(object);
    }

    public CategoryProducer(Collection<ICategory> objects)
    {
        super(objects);
    }

    protected String annotationDescription()
    {
        return "Reference Annotation for Categories";
    }

    protected void populateAssetPart(AssetPart assetPart, ICategory category)
    {
        assetPart.setSrc(BaseProducer.BASEURI + "projects/" + category.getProject().getId() + "/categories/"
                + category.getId());

        assetPart.getAnnotations().add(project(category));
        assetPart.getAnnotations().add(label(category));
        assetPart.getAnnotations().add(parent(category));
    }

    protected void populateAsset(Asset asset, ICategory category)
    {
        asset.setSrc(BaseProducer.BASEURI + "projects/" + category.getProject().getId() + "/categories/"
                + category.getId());

        asset.getAnnotations().add(project(category));
        asset.getAnnotations().add(label(category));
        asset.getAnnotations().add(parent(category));
    }

    private Annotation project(ICategory category)
    {
        Annotation anno = getBaseAnnotation(category);
        anno.setTypeRef(PREFIX_HD3D + "project");
        anno.setValue("Project/" + zeroIfNull(CollectUtils.nullSafeId(category.getProject())));
        return anno;
    }

    private Annotation label(ICategory category)
    {
        Annotation anno = getBaseAnnotation(category);
        anno.setTypeRef(PREFIX_HD3D + "label");
        anno.setValue(nullSafeToString(category.getName()));
        return anno;
    }

    private Annotation parent(ICategory category)
    {
        Annotation anno = getBaseAnnotation(category);
        anno.setTypeRef(PREFIX_HD3D + "parentCategory");
        anno.setValue("Category/" + zeroIfNull(CollectUtils.nullSafeId(category.getParent())));
        return anno;
    }

}
