package fr.hd3d.ontology.annotation;

import java.util.Collection;

import org.hd3d2.sp4.common.upload.Annotation;
import org.hd3d2.sp4.common.upload.Asset;
import org.hd3d2.sp4.common.upload.AssetPart;

import fr.hd3d.model.persistence.IProject;


public class ProjectProducer extends BaseProducer<IProject>
{
    public ProjectProducer(IProject project)
    {
        super(project);
    }

    public ProjectProducer(Collection<IProject> projects)
    {
        super(projects);
    }

    protected String annotationDescription()
    {
        return "Reference Annotation for Projects";
    }

    protected void populateAssetPart(AssetPart assetPart, IProject project)
    {
        assetPart.setSrc(BaseProducer.BASEURI + "projects/" + project.getId());
        assetPart.getAnnotations().add(description(project));
        assetPart.getAnnotations().add(label(project));
    }

    @Override
    protected void populateAsset(Asset asset, IProject object)
    {
        asset.setSrc(BaseProducer.BASEURI + "projects/" + object.getId());
        asset.getAnnotations().add(description(object));
        asset.getAnnotations().add(label(object));
    }

    private Annotation description(IProject project)
    {
        Annotation anno = getBaseAnnotation(project);
        anno.setTypeRef(PREFIX_HD3D + "description");
        anno.setValue(nullSafeToString(project.getName()));// le champs description n'existe pas pour Projet

        return anno;
    }

    private Annotation label(IProject project)
    {
        Annotation anno = getBaseAnnotation(project);
        anno.setTypeRef(PREFIX_HD3D + "label");
        anno.setValue(nullSafeToString(project.getName()));

        return anno;
    }

}
