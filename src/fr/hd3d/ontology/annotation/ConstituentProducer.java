package fr.hd3d.ontology.annotation;

import java.util.Collection;

import org.hd3d2.sp4.common.upload.Annotation;
import org.hd3d2.sp4.common.upload.Asset;
import org.hd3d2.sp4.common.upload.AssetPart;

import fr.hd3d.model.persistence.IConstituent;
import fr.hd3d.utils.CollectUtils;


public class ConstituentProducer extends BaseProducer<IConstituent>
{

    public ConstituentProducer(IConstituent object)
    {
        super(object);
    }

    public ConstituentProducer(Collection<IConstituent> objects)
    {
        super(objects);
    }

    protected String annotationDescription()
    {
        return "Reference Annotation for Constituents";
    }

    protected void populateAssetPart(AssetPart assetPart, IConstituent constituent)
    {
        assetPart.setSrc(BaseProducer.BASEURI + "constituents/" + constituent.getId());
        assetPart.getAnnotations().add(category(constituent));
        assetPart.getAnnotations().add(label(constituent));
        assetPart.getAnnotations().add(difficulty(constituent));
        assetPart.getAnnotations().add(risk(constituent));
        assetPart.getAnnotations().add(version(constituent));
        assetPart.getAnnotations().add(description(constituent));
    }

    @Override
    protected void populateAsset(Asset asset, IConstituent constituent)
    {
        asset.setSrc(BaseProducer.BASEURI + "constituents/" + constituent.getId());
        asset.getAnnotations().add(category(constituent));
        asset.getAnnotations().add(label(constituent));
        asset.getAnnotations().add(difficulty(constituent));
        asset.getAnnotations().add(risk(constituent));
        asset.getAnnotations().add(version(constituent));
        asset.getAnnotations().add(description(constituent));
    }

    private Annotation category(IConstituent constituent)
    {
        Annotation anno = getBaseAnnotation(constituent);
        anno.setTypeRef(PREFIX_HD3D + "category");
        anno.setValue("Category/" + zeroIfNull(CollectUtils.nullSafeId(constituent.getCategory())));
        return anno;
    }

    private Annotation label(IConstituent constituent)
    {
        Annotation anno = getBaseAnnotation(constituent);
        anno.setTypeRef(PREFIX_HD3D + "label");
        anno.setValue(nullSafeToString(constituent.getLabel()));
        return anno;
    }

    private Annotation difficulty(IConstituent constituent)
    {
        Annotation anno = getBaseAnnotation(constituent);
        anno.setTypeRef(PREFIX_HD3D + ":Difficulty");
        anno.setValue(String.valueOf(constituent.getDifficulty()));
        return anno;
    }

    private Annotation risk(IConstituent constituent)
    {
        Annotation anno = getBaseAnnotation(constituent);
        anno.setTypeRef(PREFIX_HD3D + ":Risk");
        anno.setValue(String.valueOf(constituent.getTrust()));
        return anno;
    }

    private Annotation version(IConstituent constituent)
    {
        Annotation anno = getBaseAnnotation(constituent);
        anno.setTypeRef(PREFIX_HD3D + ":Version");
        anno.setValue(String.valueOf(constituent.getVersion()));
        return anno;
    }

    private Annotation description(IConstituent constituent)
    {
        Annotation anno = getBaseAnnotation(constituent);
        anno.setTypeRef(PREFIX_HD3D + ":Description");
        anno.setValue(nullSafeToString(constituent.getDescription()));
        return anno;
    }

}
