package fr.hd3d.ontology.annotation;

import java.util.Collection;

import org.hd3d2.sp4.common.upload.Annotation;
import org.hd3d2.sp4.common.upload.Asset;
import org.hd3d2.sp4.common.upload.AssetPart;

import fr.hd3d.model.persistence.ISequence;
import fr.hd3d.utils.CollectUtils;


public class SequenceProducer extends BaseProducer<ISequence>
{
    public SequenceProducer(ISequence object)
    {
        super(object);
    }

    public SequenceProducer(Collection<ISequence> objects)
    {
        super(objects);
    }

    protected String annotationDescription()
    {
        return "Reference Annotation for Sequences";
    }

    protected void populateAssetPart(AssetPart assetPart, ISequence sequence)
    {
        assetPart.setSrc(BaseProducer.BASEURI + "projects/" + sequence.getProject().getId() + "/sequences/"
                + sequence.getId());
        assetPart.getAnnotations().add(project(sequence));
        assetPart.getAnnotations().add(label(sequence));
        assetPart.getAnnotations().add(parent(sequence));
    }

    @Override
    protected void populateAsset(Asset asset, ISequence sequence)
    {
        asset.setSrc(BaseProducer.BASEURI + "projects/" + sequence.getProject().getId() + "/sequences/"
                + sequence.getId());
        asset.getAnnotations().add(project(sequence));
        asset.getAnnotations().add(label(sequence));
        asset.getAnnotations().add(parent(sequence));
    }

    private Annotation project(ISequence sequence)
    {
        Annotation anno = getBaseAnnotation(sequence);
        anno.setTypeRef(PREFIX_HD3D + "project");
        anno.setValue("Project/" + zeroIfNull(CollectUtils.nullSafeId(sequence.getProject())));
        return anno;
    }

    private Annotation label(ISequence sequence)
    {
        Annotation anno = getBaseAnnotation(sequence);
        anno.setTypeRef(PREFIX_HD3D + "label");
        anno.setValue(nullSafeToString(sequence.getName()));
        return anno;
    }

    private Annotation parent(ISequence sequence)
    {
        Annotation anno = getBaseAnnotation(sequence);
        anno.setTypeRef(PREFIX_HD3D + "parentSequence");
        anno.setValue("Sequence/" + zeroIfNull(CollectUtils.nullSafeId(sequence.getParent())));
        return anno;
    }

}
